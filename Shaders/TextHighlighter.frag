uniform sampler2D SDBaseTexture;

/* If true, then the highlighting detection and functionality will run.  Otherwise, this shader will do nothing. */
uniform bool bIsHighlighting;

/* Absolute position at which the highlight begins (top left corner).  This is in SFML coordinate system where positive Y is down. */
uniform vec2 FirstHighlightPos;

/* Absolute position at which the highlight ends (top right corner).  This is in SFML coordinate system where positive Y is down. */
uniform vec2 LastHighlightPos;

uniform float LineHeight;

/**
 * Returns true if the current fragment coordinates should be highlighted.
 * NOTE:  glsl's coordinate system is inverted from Sand Dune's, where y=0 is the bottom of the window in glsl.
 */
bool ShouldBeHighlighted ()
{
	//Handle first line
	if (gl_FragCoord.y <= FirstHighlightPos.y && gl_FragCoord.y >= FirstHighlightPos.y - LineHeight)
	{
		if (gl_FragCoord.x < FirstHighlightPos.x)
		{
			return false;
		}

		//Handle case where last line is also the first line
		if (FirstHighlightPos.y - LastHighlightPos.y < LineHeight)
		{
			return (gl_FragCoord.x <= LastHighlightPos.x);
		}

		return true;
	}

	//Handle last line
	if (gl_FragCoord.y <= LastHighlightPos.y && gl_FragCoord.y >= LastHighlightPos.y - LineHeight)
	{
		return (gl_FragCoord.x <= LastHighlightPos.x);
	}

	//Otherwise must be between first and last line
	return (gl_FragCoord.y <= FirstHighlightPos.y && gl_FragCoord.y >= LastHighlightPos.y - LineHeight);
}

void main (void)
{
	gl_FragColor = texture2D(SDBaseTexture, gl_TexCoord[0].xy);
	if (!bIsHighlighting)
	{
		return; //Do nothing
	}

	if (!ShouldBeHighlighted())
	{
		return;
	}

	//Invert colors
	gl_FragColor = texture2D(SDBaseTexture, gl_TexCoord[0].xy) - vec4(1,1,1,0);
}