uniform sampler2D SDBaseTexture;

void main (void)
{
	vec4 curColor = texture2D(SDBaseTexture, gl_TexCoord[0].xy);
	gl_FragColor = vec4(1.0 - curColor.x, 1.0 - curColor.y, 1.0 - curColor.z, 1.0);
}