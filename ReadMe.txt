Sand Dune is a 2D Entity Component based engine built in C++ that's planned to be a platform for simple games. It uses the SFML library (http://www.sfml-dev.org/) to interface with the OS window handle, input, and graphics.

This engine is young in its development cycle, and it is not planned for public use any time soon. This is not intended for large scale projects yet since the software is unstable and lacks functionality.


====================================================
Prerequisites
====================================================
Windows 10
CMake version 3.12.4

====================================================
Setup
====================================================
1. To Generate the solution, execute the RunCMake.bat file found in the <root>/Source directory.
2. The batch script will prompt you to choose a 32 bit or a 64 bit project.
3. The solution file will be found at <root>/Source/ProjectFiles/Win*/SandDune.sln
4. Compiling the solution will generate executable binaries and DLLs in the <root>/Binaries/Win*/ directory.


====================================================
License
====================================================
Sand Dune's source code is released under the MIT license. Please see LICENSE.txt for complete licensing information.
SFML libraries and source code are distributed under the zlib/png license.  Please see http://www.sfml-dev.org/license.php for their license details.
Various art assets (such as Images found in Open Game Art) have a credits document which specifies under what licenses the assets are released under.