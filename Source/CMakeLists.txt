cmake_minimum_required (VERSION 3.12.4)
project (SandDune)


#########################################################################
# Apply Configuration
#########################################################################

option(CMakeGenerateZeroCheck "If true then CMake's ZERO_CHECK project will be generated." false)
option(GenerateMinSizeRel "If true then CMake will generate a project with MinSizeRel configuration." false)
option(GenerateRelWithDebInfo "If true then CMake will generate a project with Release with Debug Info configuration." false)

if (NOT CMakeGenerateZeroCheck)
	#disables ZeroCheck
	set (CMAKE_SUPPRESS_REGENERATION true)
endif (NOT CMakeGenerateZeroCheck)

set (AdditionalConfiguration "")
if (GenerateMinSizeRel)
	set (AdditionalConfiguration ${AdditionalConfiguration} "MinSizeRel")
endif (GenerateMinSizeRel)

if (GenerateRelWithDebInfo)
	set (AdditionalConfiguration ${AdditionalConfiguration} "RelWithDebInfo")
endif (GenerateRelWithDebInfo)

#Only have Debug and Release build configurations.  Add MinSizeRel and RelWithDebInfo if user specified
set (CMAKE_CONFIGURATION_TYPES Debug Release ${AdditionalConfiguration})

#Enable USE_FOLDERS to allow modules to be placed in Visual Studio filters
set_property (GLOBAL PROPERTY USE_FOLDERS ON)


#########################################################################
# Configure Platform
#########################################################################

#Are we using 64 bit or 32 bit architecture?
if (${CMAKE_SIZEOF_VOID_P} EQUAL "8")
	set (ArchitectureName "x64")
else ()
	set (ArchitectureName "x86")
endif ()
message ("Setting build configuration to use ${ArchitectureName}")

#Platform-specific configurations
if (MSVC)
	#using Microsoft Visual C
	set (ExecGenerationFlags WIN32) #Notify compiler to use Window's subsystem
	set (SharedLibraryExtension ".dll")
	
	if (${ArchitectureName} STREQUAL "x64")
		#Configure x64 solution
		set (ArchitectureFolderName "Win64")
	else ()
		#Configure x86 solution
		set (ArchitectureFolderName "Win32")
	endif ()
else (MSVC)
	set (ExecGenerationFlags )
	set (SharedLibraryExtension ".so")
	message (FATAL_ERROR "Please configure CMakeLists for your platform.")
endif(MSVC)


#########################################################################
# Establish Common Variables
#########################################################################

#Store SandDune directories in variables
set (SourceDirectory "${PROJECT_SOURCE_DIR}")
set (RootDirectory "${SourceDirectory}/..")
set (BinaryDirectory "${RootDirectory}/Binaries/${ArchitectureFolderName}")
set (ExternalDirectory "${SourceDirectory}/External")

#Names to append to the end of libs and exes to differentiate debug and release versions
set (DebugSuffix "-Debug")
set (ReleaseSuffix "")

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY "${BinaryDirectory}")
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG "${BinaryDirectory}")
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE "${BinaryDirectory}")

set (CMAKE_LIBRARY_OUTPUT_DIRECTORY "${BinaryDirectory}")
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG "${BinaryDirectory}")
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE "${BinaryDirectory}")

set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${BinaryDirectory}")
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG "${BinaryDirectory}")
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE "${BinaryDirectory}")

include_directories (${SourceDirectory})
link_directories (${BinaryDirectory})


#########################################################################
# Define Shared Functions
#########################################################################

function (ConfigureProjectFilters FileList)
	#This function configures the FileList so that it aligns with the project's folder list.
	foreach(CurFile IN LISTS FileList)
		get_filename_component(FilePath "${CurFile}" PATH)
		string(REPLACE "/" "\\" ProjectSourceFilter "${FilePath}")
		source_group("${ProjectSourceFilter}" FILES "${CurFile}")
	endforeach()
endfunction (ConfigureProjectFilters)

#Converts the given file list to C++ include statements.  Converts FileList to an include list.
function (GenerateIncludeList FileList)
	set (Results "")
	foreach(CurFile IN LISTS ${FileList})
		#Remove the "Headers/" prefix from FileList.
		string(REPLACE "Headers/" "" CurFile "${CurFile}")
		list (APPEND Results "#include \"${CurFile}\"\n")
	endforeach()
	
	#Convert from list to a long string by removing all semicolons (Prevent extra semi colons from being listed in generated source file).
	string(REPLACE ";" "" Results "${Results}")
	
	#Replace FileList with formatted include statements
	set (${FileList} ${Results} PARENT_SCOPE)
endfunction (GenerateIncludeList)

function (ApplyTargetConfiguration BaseName)
	set_target_properties(${BaseName} PROPERTIES OUTPUT_NAME_DEBUG ${BaseName}${DebugSuffix})
	set_target_properties(${BaseName} PROPERTIES OUTPUT_NAME_RELEASE ${BaseName}${ReleaseSuffix})
endfunction (ApplyTargetConfiguration)

#Simply links the specified module to a dependency that's assuming SandDune configuration where library has the 'SD-' prefix and the dependency suffix is based on configuration.
function (AddSandDuneLibDependency ModuleName DependencyName)
	target_link_libraries(SD-${ModuleName}
		PUBLIC optimized SD-${DependencyName}${ReleaseSuffix}
		PUBLIC debug SD-${DependencyName}${DebugSuffix})
endfunction (AddSandDuneLibDependency)

#Same as SandDuneLibDependency but the names do not assume the 'SD-' prefix
function (AddSandDuneLibDependencyUnformatted ModuleName DependencyName)
	target_link_libraries(${ModuleName}
		PUBLIC optimized ${DependencyName}${ReleaseSuffix}
		PUBLIC debug ${DependencyName}${DebugSuffix})
endfunction (AddSandDuneLibDependencyUnformatted)


#########################################################################
# Generate Project Files for Modules and Applications
#########################################################################

#Sand Dune modules
option(BuildModuleCommand "Build SandDune's Command module?" true)
option(BuildModuleConsole "Build SandDune's Console module?" true)
option(BuildModuleCore "Build SandDune's Core module?" true)
option(BuildModuleFile "Build SandDune's File module?" true)
option(BuildModuleGeometry "Build SandDune's Geometry module?" false)
option(BuildModuleGraphics "Build SandDune's Graphics module?" true)
option(BuildModuleGui "Build SandDune's GUI module?" true)
option(BuildModuleInput "Build SandDune's Input module?" true)
option(BuildModuleLocalization "Build SandDune's Localization module?" true)
option(BuildModuleLogger "Build SandDune's Logger module?" true)
option(BuildModuleMultiThread "Build SandDune's Multi Thread module?" true)
option(BuildModuleRandom "Build SandDune's Random module?" true)
option(BuildModuleSfmlGraphics "Build SandDune's SFML Graphics module?" true)
option(BuildModuleTime "Build SandDune's Time module?" true)
option(BuildModuleWorld "Build SandDune's World module?" true)

#Sand Dune tools
option(BuildEmptyProject "Build SandDune's EmptyProject?" true)
option(BuildSandDuneTester "Build SandDune's tester?" false)

#Sand Dune Sample Games
option(BuildSolitaire "Build SandDune's Solitaire sample game?" false)
option(BuildDroneDestroyer "Build Drone Destroyer game?" true)

#Sand Dune modules
if (BuildModuleCommand)
	add_subdirectory(Engine/Command)
endif (BuildModuleCommand)

if (BuildModuleConsole)
	add_subdirectory(Engine/Console)
endif (BuildModuleConsole)

if (BuildModuleCore)
	add_subdirectory(Engine/Core)
endif (BuildModuleCore)

if (BuildModuleFile)
	add_subdirectory(Engine/File)
endif (BuildModuleFile)

if (BuildModuleGeometry)
	add_subdirectory(Engine/Geometry)
endif (BuildModuleGeometry)

if (BuildModuleGraphics)
	add_subdirectory(Engine/Graphics)
endif (BuildModuleGraphics)

if (BuildModuleGui)
	add_subdirectory(Engine/Gui)
endif (BuildModuleGui)

if (BuildModuleInput)
	add_subdirectory(Engine/Input)
endif (BuildModuleInput)

if (BuildModuleLocalization)
	add_subdirectory(Engine/Localization)
endif (BuildModuleLocalization)

if (BuildModuleLogger)
	add_subdirectory(Engine/Logger)
endif (BuildModuleLogger)

if (BuildModuleMultiThread)
	add_subdirectory(Engine/MultiThread)
endif (BuildModuleMultiThread)

if (BuildModuleRandom)
	add_subdirectory(Engine/Random)
endif (BuildModuleRandom)

if (BuildModuleSfmlGraphics)
	add_subdirectory(Engine/SfmlGraphics)
endif (BuildModuleSfmlGraphics)

if (BuildModuleTime)
	add_subdirectory(Engine/Time)
endif (BuildModuleTime)

if (BuildModuleWorld)
	add_subdirectory(Engine/World)
endif (BuildModuleWorld)

#Sand Dune tools
if (BuildEmptyProject)
	add_subdirectory(Tools/EmptyProject)
endif (BuildEmptyProject)

if (BuildSandDuneTester)
	add_subdirectory(Tools/SandDuneTester)
endif (BuildSandDuneTester)

#Sample Games
if (BuildSolitaire)
	add_subdirectory(SampleGames/Solitaire)
endif (BuildSolitaire)

if (BuildDroneDestroyer)
	add_subdirectory(Games/DroneDestroyer)
endif (BuildDroneDestroyer)


#########################################################################
# Copy files to binary directory
#########################################################################

#List of shared libraries to copy over to binary directory.
set (LibsToCopy
	#Copy SFML shared libs
	"${ExternalDirectory}/SFML/bin/${ArchitectureFolderName}/openal32.dll"
	"${ExternalDirectory}/SFML/bin/${ArchitectureFolderName}/sfml-audio-2${SharedLibraryExtension}"
	"${ExternalDirectory}/SFML/bin/${ArchitectureFolderName}/sfml-audio-d-2${SharedLibraryExtension}"
	"${ExternalDirectory}/SFML/bin/${ArchitectureFolderName}/sfml-system-2${SharedLibraryExtension}"
	"${ExternalDirectory}/SFML/bin/${ArchitectureFolderName}/sfml-system-d-2${SharedLibraryExtension}"
	"${ExternalDirectory}/SFML/bin/${ArchitectureFolderName}/sfml-graphics-2${SharedLibraryExtension}"
	"${ExternalDirectory}/SFML/bin/${ArchitectureFolderName}/sfml-graphics-d-2${SharedLibraryExtension}"
	"${ExternalDirectory}/SFML/bin/${ArchitectureFolderName}/sfml-window-2${SharedLibraryExtension}"
	"${ExternalDirectory}/SFML/bin/${ArchitectureFolderName}/sfml-window-d-2${SharedLibraryExtension}"

	#Copy Box2D shared libs
	"${ExternalDirectory}/Box2D/lib/${ArchitectureFolderName}/box2d${SharedLibraryExtension}"
	"${ExternalDirectory}/Box2D/lib/${ArchitectureFolderName}/box2d-d${SharedLibraryExtension}")
	
file (COPY ${LibsToCopy} DESTINATION ${BinaryDirectory})