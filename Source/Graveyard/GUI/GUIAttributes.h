/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIAttributes.h
  A datatype responsible for calculating absolute and relative position
  and size for any GUIComponent.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUIATTRIBUTES_H
#define GUIATTRIBUTES_H

#include "GUI.h"

#if INCLUDE_GUI

namespace SD
{
	class GUIComponent;

	class GUIAttributes
	{


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* This class instance is storing attributes for this GUIComponent. */
		GUIComponent* OwningComponent;

		/* If true, then this attributes will automatically adjust the max position/size values whenever its position/size changed.
		   In detail:  this will automatically set the max position so that the right border of this UI
		   component does not pass the right border of its owner.  This also ensures that the attributes
		   does not overlap the borders of UI component's owner attributes.*/
		bool bAutoClamp;

		/* Absolute coordinates where this component is positioned.  X = 0 implies to the left. */
		INT AbsPositionX;

		/* Absolute coordinates where this component is position.  Y = 0 implies on top. */
		INT AbsPositionY;

		/* Component's relative position to its owning component.  If there is no
		owner, then it's the relative position to the window handle, itself. */
		FLOAT RelPositionX;
		FLOAT RelPositionY;

		/* Absolute size of this component (in pixels) */
		INT AbsSizeX;
		INT AbsSizeY;

		/* Component's relative size to its owning component.  If there is no
		owner, then it's the relative size to the window handle, itself. */
		FLOAT RelSizeX;
		FLOAT RelSizeY;

		/* Min/Max values the attribute's size/position may achieve. */
		INT MinPositionX;
		INT MaxPositionX;
		INT MinPositionY;
		INT MaxPositionY;
		INT MinSizeX;
		INT MaxSizeX;
		INT MinSizeY;
		INT MaxSizeY;

		/* Clamp values automatically set from bAutoClamp */
		INT AutoMinPositionX;
		INT AutoMaxPositionX;
		INT AutoMinPositionY;
		INT AutoMaxPositionY;
		INT AutoMaxSizeX;
		INT AutoMaxSizeY;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		GUIAttributes (bool bNewAutoClamp = true);
		GUIAttributes (GUIComponent* newOwningComponent);
		GUIAttributes (const GUIAttributes& copyObject);
		virtual ~GUIAttributes ();


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Links these Attributes to a particular GUIComponent.
		 */
		virtual void SetOwningComponent (GUIComponent* newOwningComponent);

		virtual void SetMinPositionX (const INT newMinPos);
		virtual void SetMaxPositionX (const INT newMaxPos);
		virtual void SetMinPositionY (const INT newMinPos);
		virtual void SetMaxPositionY (const INT newMaxPos);
		virtual void SetMinSizeX (const INT newMinSize);
		virtual void SetMaxSizeX (const INT newMaxSize);
		virtual void SetMinSizeY (const INT newMinSize);
		virtual void SetMaxSizeY (const INT newMaxSize);

		/**
		  Recomputes what the auto clamp values should be based on the current condition of this attributes.
		 */
		virtual void ApplyAutoClampValues ();

		/**
		  Method used to recalculate relative values to handle any change in the component's owner.
		 */
		virtual void RecalculateRelativeValues ();

		virtual void SetAutoClamp (bool bNewAutoClamp);

		/**
		  Returns a copy of the owning component's owner's attributes.  If the component does not have
		  an owner, then it generates attributes from the application's window handle.
		 */
		virtual void GetComponentsOwnerAttributes (GUIAttributes& outAttributesCopy) const;

		/**
		  Gets the absolute right border in pixels.
		 */
		virtual INT GetRight () const;

		/**
		  Gets the absolute bottom border in pixels.
		 */
		virtual INT GetBottom () const;


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual bool GetAutoClamp () const;

		virtual INT GetAbsPositionX () const;
		virtual INT GetAbsPositionY () const;
		virtual FLOAT GetRelPositionX () const;
		virtual FLOAT GetRelPositionY () const;

		virtual INT GetAbsSizeX () const;
		virtual INT GetAbsSizeY () const;
		virtual FLOAT GetRelSizeX () const;
		virtual FLOAT GetRelSizeY () const;

		virtual INT GetMinPositionX () const;
		virtual INT GetMaxPositionX () const;
		virtual INT GetMinPositionY () const;
		virtual INT GetMaxPositionY () const;
		virtual INT GetMinSizeX () const;
		virtual INT GetMaxSizeX () const;
		virtual INT GetMinSizeY () const;
		virtual INT GetMaxSizeY () const;

		virtual const GUIComponent* GetOwningComponent () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual void SetAbsolutePosition (INT newPosX, INT newPosY);
		virtual void SetRelativePosition (FLOAT newPosX, FLOAT newPosY);

		virtual void SetAbsoluteSize (INT newSizeX, INT newSizeY);
		virtual void SetRelativeSize (FLOAT newSizeX, FLOAT newSizeY);

		/**
		  Calculates the absolute position based on the current relative position.
		 */
		virtual void CalculateAbsPosition ();

		/**
		  Calculates the relative position based on the current absolute position.
		 */
		virtual void CalculateRelPosition ();

		/**
		  Calculates the absolute size based on the current relative size.
		 */
		virtual void CalculateAbsSize ();

		/**
		  Calculates the relative size based on the current absolute size.
		 */
		virtual void CalculateRelSize ();

	friend GUIComponent;
	};

#pragma region "External Operators"
	bool operator== (const GUIAttributes& left, const GUIAttributes& right);
	bool operator!= (const GUIAttributes& left, const GUIAttributes& right);
#pragma endregion
}

#endif
#endif