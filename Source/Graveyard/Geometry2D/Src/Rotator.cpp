/*
=====================================================================
  MIT License

  Copyright (c) 2016-2018 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Rotator.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "Geometry2DClasses.h"

#if INCLUDE_GEOMETRY2D

using namespace std;

SD_BEGIN
Rotator::Rotator ()
{
	Value = 0.f;
}

Rotator::Rotator (FLOAT initialAngle)
{
	Value = initialAngle;
	ClampRotation();
}

Rotator::Rotator (float initialAngle)
{
	Value = FLOAT(initialAngle);
	ClampRotation();
}

Rotator::Rotator (const Rotator& copyRotator)
{
	Value = copyRotator.Value;
}

void Rotator::operator= (const Rotator& copyRotator)
{
	Value = copyRotator.Value;
}
		
DString Rotator::ToString () const
{
#if ROTATOR_UNIT_DEGREES
	return Value.ToString() + TXT(" degrees");
#else
	return Value.ToString() + TXT(" radians");
#endif
}

void Rotator::Serialize (DataBuffer& dataBuffer) const
{
	dataBuffer << Value;
}

void Rotator::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> Value;
}

void Rotator::SetRotation (FLOAT newRotation)
{
	Value = newRotation;
	ClampRotation();
}

void Rotator::SetRotation (Vector2 axis)
{
	if (axis.IsEmpty())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Can't update rotators through axis without specifying a direction."));
		return;
	}

	axis.SetLengthTo(1.f);
	Value = acos(axis.X.Value);

#if ROTATOR_UNIT_DEGREES
	Value *= RADIAN_TO_DEGREE;
#endif

	//Cover range (180, 360).  This mirrors to the 1st and 2nd quadrant (if you were to graph axis on cartesian plan).
	if (axis.Y > 0)
	{
		//If you were to draw this on a wheel, mirror the result about the x-axis
		Value += ((ROTATOR_RIGHT_ANGLE * 2) - Value) * 2;
	}
}

bool Rotator::IsPerpendicular (const Rotator& otherRotator) const
{
	FLOAT deltaAngle = FLOAT::Abs(Value - otherRotator.Value);

	return (deltaAngle == ROTATOR_RIGHT_ANGLE || deltaAngle == ROTATOR_RIGHT_ANGLE * 3);
}

bool Rotator::IsParallel (const Rotator& otherRotator) const
{
	FLOAT deltaAngle = FLOAT::Abs(Value - otherRotator.Value);

	//Return true for equal directions or directions travelling in opposite directions.
	return (deltaAngle == 0.f || deltaAngle == ROTATOR_RIGHT_ANGLE * 2);
}

Vector2 Rotator::GetAxis () const
{
	FLOAT radians = GetRadians();

	Vector2 result;
	result.X = cos(radians.Value);
	result.Y = sin(radians.Value) * -1; //-1 because we rotate clockwise

	return result;
}

FLOAT Rotator::GetRotation () const
{
	return Value;
}

FLOAT Rotator::GetDegrees () const
{
#if ROTATOR_UNIT_DEGREES
	return Value;
#else
	return Value * RADIAN_TO_DEGREE;
#endif
}

FLOAT Rotator::GetRadians () const
{
#if ROTATOR_UNIT_DEGREES
	return Value * DEGREE_TO_RADIAN;
#else
	return Value;
#endif
}

void Rotator::ClampRotation ()
{
	FLOAT revolution = ROTATOR_RIGHT_ANGLE * 4;

	//unwind
	while (Value < 0)
	{
		Value += revolution;
	}

	while (Value >= revolution)
	{
		Value -= revolution;
	}
}

#pragma region "External Operators"
bool operator== (const Rotator& left, const Rotator& right)
{
	return (left.GetRotation() == right.GetRotation());
}

bool operator!= (const Rotator& left, const Rotator& right)
{
	return !(left == right);
}

Rotator operator+ (const Rotator& left, const Rotator& right)
{
	return Rotator(left.GetRotation() + right.GetRotation());
}

Rotator operator+ (const Rotator& left, const FLOAT& right)
{
	return Rotator(left.GetRotation() + right);
}

Rotator operator+ (const FLOAT &left, const Rotator& right)
{
	return Rotator(left + right.GetRotation());
}

Rotator& operator+= (Rotator& left, const Rotator& right)
{
	left.SetRotation(left.GetRotation() + right.GetRotation());
	return left;
}

Rotator& operator+= (Rotator& left, const FLOAT& right)
{
	left.SetRotation(left.GetRotation() + right);
	return left;
}

Rotator operator- (const Rotator& left, const Rotator& right)
{
	return Rotator(left.GetRotation() - right.GetRotation());
}

Rotator operator- (const Rotator& left, const FLOAT& right)
{
	return Rotator(left.GetRotation() - right);
}

Rotator operator- (const FLOAT& left, const Rotator& right)
{
	return Rotator(left - right.GetRotation());
}

Rotator& operator-= (Rotator& left, const Rotator& right)
{
	left.SetRotation(left.GetRotation() - right.GetRotation());
	return left;
}

Rotator& operator-= (Rotator& left, const FLOAT& right)
{
	left.SetRotation(left.GetRotation() - right);
	return left;
}
#pragma endregion
SD_END

#endif