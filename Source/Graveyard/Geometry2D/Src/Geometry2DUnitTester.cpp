/*
=====================================================================
  MIT License

  Copyright (c) 2016-2018 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Geometry2DUnitTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "Geometry2DClasses.h"

#if INCLUDE_GEOMETRY2D

#ifdef DEBUG_MODE

using namespace std;

SD_BEGIN
IMPLEMENT_CLASS(SD, Geometry2DUnitTester, SD, UnitTester)

bool Geometry2DUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		return (TestRotator(testFlags));
	}

	return true;
}

bool Geometry2DUnitTester::TestRotator (EUnitTestFlags testFlags) const
{
#if 0
	BeginTestSequence(testFlags, TXT("Rotator"));
#if ROTATOR_UNIT_DEGREES
	TestLog(testFlags, TXT("Rotators are in degrees."));
#else
	TestLog(testFlags, TXT("Rotators are in radians."));
#endif

	Rotator blankRotator;
	Rotator initializedRotator(ROTATOR_RIGHT_ANGLE * 0.5f);
	Rotator copiedRotator(initializedRotator);

	TestLog(testFlags, TXT("Constructor tests:  BlankRotator=%s"), blankRotator);
	TestLog(testFlags, TXT("    initializedRotator(%s)= %s"), FLOAT(ROTATOR_RIGHT_ANGLE * 0.5f), initializedRotator);
	TestLog(testFlags, TXT("    copiedRotator from initializedRotator= %s"), copiedRotator);

	SetTestCategory(testFlags, TXT("Comparison"));
	TestLog(testFlags, TXT("initializedRotator != copiedRotator ? %s"), BOOL(initializedRotator != copiedRotator));
	if (initializedRotator != copiedRotator)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedRotator != copiedRotator.  Values are:  %s and %s"), {initializedRotator.ToString(), copiedRotator.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	Rotator testRotator = Rotator(ROTATOR_RIGHT_ANGLE * 1.5f) + Rotator(ROTATOR_RIGHT_ANGLE * 0.5f);
	TestLog(testFlags, TXT("%s + %s = %s"), Rotator(ROTATOR_RIGHT_ANGLE * 1.5f), Rotator(ROTATOR_RIGHT_ANGLE * 0.5f), testRotator);
	if (testRotator != Rotator(ROTATOR_RIGHT_ANGLE * 2.f))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRotator is %s.  It's currently:  %s"), {Rotator(ROTATOR_RIGHT_ANGLE * 0.5f).ToString(), testRotator.ToString()});
		return false;
	}

	const Rotator nineDegrees = ROTATOR_RIGHT_ANGLE * 0.1f;
	const Rotator twentySevenDegrees = ROTATOR_RIGHT_ANGLE * 0.3f;
	const Rotator fourtyFiveDegrees = ROTATOR_RIGHT_ANGLE * 0.5f;
	const Rotator ninetyDegrees = ROTATOR_RIGHT_ANGLE;
	const Rotator oneEightyDegrees = ROTATOR_RIGHT_ANGLE * 2.f;
	const Rotator twoSeventyDegrees = ROTATOR_RIGHT_ANGLE * 3.f;
	const Rotator threeFifteen = (fourtyFiveDegrees.GetRotation() * 7.f);
	const FLOAT tolerance = 0.000001f;

	testRotator = ninetyDegrees - twentySevenDegrees;
	TestLog(testFlags, TXT("%s - %s = %s"), ninetyDegrees, twentySevenDegrees, testRotator);
#if ROTATOR_UNIT_DEGREES
	if (testRotator != Rotator(63.f))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRotator is %s.  It's currently:  %s"), {Rotator(63).ToString(), testRotator.ToString()});
		return false;
	}
#else
	if (FLOAT::Abs((testRotator - Rotator(1.099557459f)).GetRotation()) > tolerance)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRotator is %s.  It's currently:  %s"), {Rotator(1.099557459f).ToString(), testRotator.ToString()});
		return false;
	}
#endif

	Rotator oldTestRotator = testRotator;
	testRotator += fourtyFiveDegrees;
	TestLog(testFlags, TXT("%s += %s  ---> %s"), oldTestRotator, fourtyFiveDegrees, testRotator);
#if ROTATOR_UNIT_DEGREES
	if (testRotator != Rotator(108.f))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRotator is %s.  It's currently:  %s"), {Rotator(108.f).ToString(), testRotator.ToString()});
		return false;
	}
#else
	if (FLOAT::Abs((testRotator - Rotator(1.884955644f)).GetRotation()) > tolerance)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRotator is %s.  It's currently:  %s"), {Rotator(1.884955644f).ToString(), testRotator.ToString()});
		return false;
	}
#endif
	
	testRotator = Rotator();
	TestLog(testFlags, TXT("Incrementing %s every %s for 2 revolutions"), testRotator, nineDegrees);
	for (UINT_TYPE i = 0; i < 80; i++)
	{
		testRotator += nineDegrees;
		TestLog(testFlags, TXT("    testRotator=%s"), testRotator);

#if ROTATOR_UNIT_DEGREES
		if (testRotator.GetRotation() < 0.f || testRotator.GetRotation() >= 360.f)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  The value of testRotator (%s) should be within 0-359 degrees."), {testRotator.ToString()});
			return false;
		}
#else
		if (testRotator.GetRotation() < 0.f || testRotator.GetRotation() >= 6.283185307f)
		{
			UnitTestError(testFlags, TXT("Arithmetic failed.  The value of testRotator (%s) should be within 0-2 pi."), {testRotator.ToString()});
			return false;
		}
#endif
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Rotator firstRotation(10.f);
		Rotator secondRotation(12.5f);
		Rotator readFirstRotation;
		Rotator readSecondRotation;
		DataBuffer rotatorBuffer;

		rotatorBuffer << firstRotation;
		rotatorBuffer << secondRotation;
		rotatorBuffer >> readFirstRotation;
		rotatorBuffer >> readSecondRotation;

		if (firstRotation != readFirstRotation)
		{
			UnitTestError(testFlags, TXT("Streaming rotator test failed.  After pushing the rotator %s to data buffer, it pulled %s from that data buffer."), {firstRotation.ToString(), readFirstRotation.ToString()});
			return true;
		}

		if (secondRotation != readSecondRotation)
		{
			UnitTestError(testFlags, TXT("Streaming rotator test failed.  After pushing the second rotator %s to data buffer, it pulled %s from that data buffer."), {secondRotation.ToString(), readSecondRotation.ToString()});
			return true;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utilities"));
	testRotator = Rotator();
	TestLog(testFlags, TXT("Is %s perpendicular to %s ?  %s"), testRotator, ninetyDegrees, BOOL(testRotator.IsPerpendicular(ninetyDegrees)));
	if (!testRotator.IsPerpendicular(ninetyDegrees))
	{
		UnitTestError(testFlags, TXT("IsPerpendicular function test failed.  %s should be perpendicular to %s."), {testRotator.ToString(), ninetyDegrees.ToString()});
		return false;
	}

	testRotator = fourtyFiveDegrees;
	TestLog(testFlags, TXT("Is %s perpendicular to %s ?  %s"), testRotator, threeFifteen, BOOL(testRotator.IsPerpendicular(threeFifteen)));
	if (!testRotator.IsPerpendicular(threeFifteen))
	{
		UnitTestError(testFlags, TXT("IsPerpendicular function test failed.  %s should be perpendicular to %s."), {testRotator.ToString(), threeFifteen.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Is %s perpendicular to %s ?  %s"), testRotator, ninetyDegrees, BOOL(testRotator.IsPerpendicular(ninetyDegrees)));
	if (testRotator.IsPerpendicular(ninetyDegrees))
	{
		UnitTestError(testFlags, TXT("IsPerpendicular function test failed.  %s should not be perpendicular to %s."), {testRotator.ToString(), ninetyDegrees.ToString()});
		return false;
	}

	testRotator = Rotator();
	TestLog(testFlags, TXT("Is %s parallel to %s ?  %s"), testRotator, ninetyDegrees, BOOL(testRotator.IsParallel(ninetyDegrees)));
	if (testRotator.IsParallel(ninetyDegrees))
	{
		UnitTestError(testFlags, TXT("IsParallel function test failed.  %s should not be parallel to %s."), {testRotator.ToString(), ninetyDegrees.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Is %s parallel to %s ?  %s"), testRotator, oneEightyDegrees, BOOL(testRotator.IsParallel(oneEightyDegrees)));
	if (!testRotator.IsParallel(oneEightyDegrees))
	{
		UnitTestError(testFlags, TXT("IsParallel function test failed.  %s should be parallel to %s."), {testRotator.ToString(), oneEightyDegrees.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Is %s parallel to itself ?  %s"), testRotator, BOOL(testRotator.IsParallel(testRotator)));
	if (!testRotator.IsParallel(testRotator))
	{
		UnitTestError(testFlags, TXT("IsParallel function test failed.  %s should be parallel to itself."), {testRotator.ToString()});
		return false;
	}

	testRotator = fourtyFiveDegrees;
	TestLog(testFlags, TXT("The degree value of %s is %s."), testRotator, testRotator.GetDegrees());
	if (testRotator.GetDegrees() != 45.f)
	{
		UnitTestError(testFlags, TXT("GetDegrees function test failed.  The degree value of %s should have returned 45 degrees.  Instead it returned %s degrees."), {testRotator.ToString(), testRotator.GetDegrees().ToString()});
		return false;
	}

	TestLog(testFlags, TXT("The radian value of %s is %s."), testRotator, testRotator.GetRadians());
	if ((testRotator.GetRadians() - 0.785398163f) >= tolerance)
	{
		UnitTestError(testFlags, TXT("GetRadians function test failed.  The radian value of %s should have returned about 0.785398163.  Instead it returned %s radians."), {testRotator.ToString(), testRotator.GetRadians().ToString()});
		return false;
	}

	Vector2 testRotatorAxis = testRotator.GetAxis();
	TestLog(testFlags, TXT("The axis values of %s is %s."), testRotator, testRotatorAxis);
	if ((testRotatorAxis - Vector2(0.707106781f, -0.707106781f)).VSize() >= tolerance)
	{
		UnitTestError(testFlags, TXT("GetAxis function test failed.  The axis values of %s should have returned (0.707106781, -0.707106781).  Instead it returned %s."), {testRotator.ToString(), testRotatorAxis.ToString()});
		return false;
	}

	testRotatorAxis = Vector2(0.f, 1024.f);
	testRotator.SetRotation(testRotatorAxis);
	TestLog(testFlags, TXT("Setting axis %s to testRotator resulted in %s"), testRotatorAxis, testRotator);
	if (testRotator != twoSeventyDegrees)
	{
		UnitTestError(testFlags, TXT("SetRotation via axis function test failed.  Setting Axises %s to a rotator should have resulted in %s.  Instead it returned %s."), {testRotatorAxis.ToString(), twoSeventyDegrees.ToString(), testRotator.ToString()});
		return false;
	}

	testRotatorAxis = Vector2(50.f, 50.f);
	testRotator.SetRotation(testRotatorAxis);
	TestLog(testFlags, TXT("Setting axis %s to testRotator resulted in %s"), testRotatorAxis, testRotator);
	if (testRotator != threeFifteen)
	{
		UnitTestError(testFlags, TXT("SetRotation via axis function test failed.  Setting Axises %s to a rotator should have resulted in %s.  Instead it returned %s."), {testRotatorAxis.ToString(), threeFifteen.ToString(), testRotator.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Rotator"));
#endif
	return true;
}
SD_END

#endif
#endif