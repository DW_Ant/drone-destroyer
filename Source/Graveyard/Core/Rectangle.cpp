/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Rectangle.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	Rectangle::Rectangle ()
	{
		Left = 0;
		Top = 0;
		Right = 0;
		Bottom = 0;
	}

	Rectangle::Rectangle (const FLOAT inLeft, const FLOAT inTop, const FLOAT inRight, const FLOAT inBottom)
	{
		Left = inLeft;
		Top = inTop;
		Right = inRight;
		Bottom = inBottom;
	}

	Rectangle::Rectangle (const Rectangle& copyRectangle)
	{
		Left = copyRectangle.Left;
		Top = copyRectangle.Top;
		Right = copyRectangle.Right;
		Bottom = copyRectangle.Bottom;
	}

	void Rectangle::operator= (const Rectangle& copyRectangle)
	{
		Left = copyRectangle.Left;
		Top = copyRectangle.Top;
		Right = copyRectangle.Right;
		Bottom = copyRectangle.Bottom;
	}

	DString Rectangle::ToString () const
	{
		return (TXT("Left(") + Left.ToString() + TXT(") Top(") + Top.ToString() + TXT(") Right(") + Right.ToString() + TXT(") Bottom(") + Bottom.ToString() + TXT(")"));
	}

	FLOAT Rectangle::CalculateArea () const
	{
		return GetWidth() * GetHeight();
	}

	FLOAT Rectangle::CalculatePerimeter () const
	{
		return (GetWidth() * 2) + (GetHeight() * 2);
	}

	bool Rectangle::IsSquare () const
	{
		return (GetWidth() == GetHeight());
	}

	bool Rectangle::IsEmpty () const
	{
		return (GetWidth() == 0 && GetHeight() == 0);
	}

	Rectangle Rectangle::GetOverlappingRectangle (const Rectangle& otherRectangle) const
	{
		if (!Overlaps(otherRectangle))
		{
			return Rectangle();
		}

		Rectangle result;
		result.Left = Utils::Max(Left, otherRectangle.Left);
		result.Top = Utils::Max(Top, otherRectangle.Top);
		result.Right = Utils::Min(Right, otherRectangle.Right);
		result.Bottom = Utils::Min(Bottom, otherRectangle.Bottom);

		return result;
	}

	bool Rectangle::Overlaps (const Rectangle& otherRectangle) const
	{
		if (Top >= otherRectangle.Bottom || Bottom <= otherRectangle.Top)
		{
			//Rectangle is below/above the other rectangle.
			return false;
		}

		if (Left >= otherRectangle.Right || Right <= otherRectangle.Left)
		{
			//Rectangle is left/right of other rectangle.
			return false;
		}

		return true;
	}

	bool Rectangle::EncompassesPoint (const Vector2& targetPoint) const
	{
		return (targetPoint.X >= Left && targetPoint.X <= Right && targetPoint.Y <= Top && targetPoint.Y >= Bottom);
	}

	bool Rectangle::ContainsInvalidBorders () const
	{
		return (Left > Right || Top > Bottom);
	}

	FLOAT Rectangle::GetWidth () const
	{
		return (Right - Left).Abs();
	}

	FLOAT Rectangle::GetHeight () const
	{
		return (Bottom - Top).Abs();
	}

#pragma region "External Operators"
	bool operator== (const Rectangle& left, const Rectangle& right)
	{
		return (left.Top == right.Top && left.Right == right.Right && left.Bottom == right.Bottom && left.Left == right.Left);
	}

	bool operator!= (const Rectangle& left, const Rectangle& right)
	{
		return !(left == right);
	}

	Rectangle operator+ (const Rectangle& left, const Rectangle& right)
	{
		return Rectangle(left.Left + right.Left, left.Top + right.Top, left.Right + right.Right, left.Bottom + right.Bottom);
	}

	Rectangle& operator+= (Rectangle& left, const Rectangle& right)
	{
		left.Left += right.Left;
		left.Top += right.Top;
		left.Right += right.Right;
		left.Bottom += right.Bottom;
		return left;
	}

	Rectangle operator- (const Rectangle& left, const Rectangle& right)
	{
		return Rectangle(left.Left - right.Left, left.Top - right.Top, left.Right - right.Right, left.Bottom - right.Bottom);
	}

	Rectangle& operator-= (Rectangle& left, const Rectangle& right)
	{
		left.Left -= right.Left;
		left.Top -= right.Top;
		left.Right -= right.Right;
		left.Bottom -= right.Bottom;
		return left;
	}

	Rectangle operator* (const Rectangle& left, const Rectangle& right)
	{
		return Rectangle(left.Left * right.Left, left.Top * right.Top, left.Right * right.Right, left.Bottom * right.Bottom);
	}

	Rectangle& operator*= (Rectangle& left, const Rectangle& right)
	{
		left.Left *= right.Left;
		left.Top *= right.Top;
		left.Right *= right.Right;
		left.Bottom *= right.Bottom;
		return left;
	}

	Rectangle operator/ (const Rectangle& left, const Rectangle& right)
	{
		Rectangle result(left);
		result /= right;
		return result;
	}

	Rectangle& operator/= (Rectangle& left, const Rectangle& right)
	{
		if (right.Left != 0.f)
		{
			left.Left /= right.Left;
		}

		if (right.Top != 0.f)
		{
			left.Top /= right.Top;
		}

		if (right.Right != 0.f)
		{
			left.Right /= right.Right;
		}

		if (right.Bottom != 0.f)
		{
			left.Bottom /= right.Bottom;
		}

		return left;
	}
#pragma endregion
}

#endif