/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Rotator.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

#if 0

using namespace std;

namespace SD
{
	Rotator::Rotator ()
	{
		SetAxis(1.f, 0.f);
	}

	Rotator::Rotator (float initialAngle)
	{
		SetAxisFromFloat(initialAngle);
	}

	Rotator::Rotator (const float x, const float y)
	{
		SetAxis(x, y);
	}

	Rotator::Rotator (const Rotator& copyRotator)
	{
		SetAxis(copyRotator.X, copyRotator.Y);
	}

	void Rotator::operator= (const Rotator& copyRotator)
	{
		SetAxis(copyRotator.X, copyRotator.Y);
	}

	void Rotator::operator= (float newRotation)
	{
		SetAxisFromFloat(newRotation);
	}

	bool Rotator::operator== (const Rotator& otherRotator)
	{
		return (Vector2(X - otherRotator.X, Y - otherRotator.Y).VSize() <= ROTATOR_PRECISION_TOLERANCE);
	}

	bool Rotator::operator!= (const Rotator& otherRotator)
	{
		return (Vector2(X - otherRotator.X, Y - otherRotator.Y).VSize() > ROTATOR_PRECISION_TOLERANCE);
	}

	Rotator Rotator::operator+ (const Rotator& otherRotator)
	{
		Rotator result(X, Y);
		float theta = otherRotator.ToFloat();

#if ROTATOR_UNIT_DEGREES
		theta *= DEGREE_TO_RADIAN;
#endif

		/*
		Using matrix multiplication
		[x'] = [cos(theta)		-sin(theta)][x]
		[y'] = [sin(theta)		cos(theta) ][y]
		*/
		float savedX = result.X; //cache X to prevent accumulated precision error
		result.X = ((result.X * cos(theta)) - (result.Y * sin(theta)));
		result.Y = ((savedX * sin(theta)) + (result.Y * cos(theta)));

		//Increment counter three times since there were three trig functions applied per axis
		result.IncrementCounter(3);
		return result;
	}

	Rotator Rotator::operator+ (float adjustment)
	{
		float finalAngle = ToFloat() + adjustment;

		Rotator result(finalAngle);
		result.OperationCounter = OperationCounter;
		result.IncrementCounter();

		return result;
	}

	void Rotator::operator+= (const Rotator& otherRotator)
	{
		float theta = atan(otherRotator.X/otherRotator.Y);

		/*
		Using matrix multiplication
		[x'] = [cos(theta)		-sin(theta)][x]
		[y'] = [sin(theta)		cos(theta) ][y]
		*/
		float savedX = X; //cache X to prevent accumulated precision error
		X = ((savedX * cos(theta)) - (Y * sin(theta)));
		Y = ((savedX * sin(theta)) + (Y * cos(theta)));

		//Increment counter three times since there were three trig functions applied per axis
		IncrementCounter(3);
	}

	void Rotator::operator+= (float adjustment)
	{
		float finalAngle = ToFloat() + adjustment;
		SetAxisFromFloat(finalAngle);
	}

	Rotator Rotator::operator- (const Rotator& otherRotator)
	{
		Rotator result(X, Y);
		float theta = otherRotator.ToFloat();

#if ROTATOR_UNIT_DEGREES
		theta *= DEGREE_TO_RADIAN;
#endif

		/*
		Using matrix multiplication
		[x'] = [cos(theta)		sin(theta)][x]
		[y'] = [-sin(theta)		cos(theta) ][y]
		*/
		float savedX = result.X; //cache X to prevent accumulated precision error
		result.X = ((result.X * cos(theta)) + (result.Y * sin(theta)));
		result.Y = ((-1 * savedX * sin(theta)) + (result.Y * cos(theta)));

		//Increment counter three times since there were three trig functions applied per axis
		result.IncrementCounter(3);
		return result;
	}

	Rotator Rotator::operator- (float adjustment)
	{
		float finalAngle = ToFloat() - adjustment;

		Rotator result(finalAngle);
		result.OperationCounter = OperationCounter;
		result.IncrementCounter();

		return result;
	}

	void Rotator::operator-= (const Rotator& otherRotator)
	{
		float theta = atan(otherRotator.X/otherRotator.Y);

		/*
		Using matrix multiplication
		[x'] = [cos(theta)		-sin(theta)][x]
		[y'] = [sin(theta)		cos(theta) ][y]
		*/
		float savedX = X; //cache X to prevent accumulated precision error
		X = ((savedX * cos(theta)) + (Y * sin(theta)));
		Y = ((-1 * savedX * sin(theta)) + (Y * cos(theta)));

		//Increment counter three times since there were three trig functions applied per axis
		IncrementCounter(3);
	}

	void Rotator::operator-= (float adjustment)
	{
		float finalAngle = ToFloat() - adjustment;
		SetAxisFromFloat(finalAngle);
	}

	DString Rotator::ToString () const
	{
		return (TXT("(") + Utils::ToString(X) + TXT(",") + Utils::ToString(Y) + TXT(")"));
	}

	void Rotator::SetAxis (float newX, float newY)
	{
		X = newX;
		Y = newY;
		Normalize();
	}

	float Rotator::Dot (const Rotator& otherRotator) const
	{
		return ((X + otherRotator.X) * (Y + otherRotator.Y));
	}

	void Rotator::Normalize ()
	{
		//Precision errors removed.  Reset the precision error counter.
		OperationCounter = 0;

		float magnitude = (sqrt(pow(X, 2) + pow(Y, 2)));

		if (magnitude == 0)
		{
#ifdef DEBUG_MODE
			LOG(LOG_WARNING, TXT("A Rotator was detected where its magnitude is length 0!  Resetting this rotator to defaults."));
#endif
			//Reset Rotator
			SetAxis(1.f, 0.f);
			return;
		}

		X /= magnitude;
		Y /= magnitude;
	}

	float Rotator::ToFloat () const
	{
		float result = acos(X);

#if ROTATOR_UNIT_DEGREES
		result *= RADIAN_TO_DEGREE;
#endif

		//Cover range (180, 360).  This mirrors to the 3rd and 4th quadrant.
		if (Y < 0)
		{
			//If you were to draw this on a wheel, mirror the result about the x-axis
			result += ((ROTATOR_90 * 2) - result) * 2;
		}

		return result;
	}

	void Rotator::GetAxis (float& outX, float& outY) const
	{
		outX = X;
		outY = Y;
	}

	void Rotator::IncrementCounter (int numTimesToIncrement)
	{
		OperationCounter += numTimesToIncrement;

		if (OperationCounter >= NormalizingFrequency)
		{
			Normalize();
		}
	}

	void Rotator::SetAxisFromFloat (float newRotation)
	{
#if ROTATOR_UNIT_DEGREES
		X = cos(newRotation * DEGREE_TO_RADIAN);
		Y = sin(newRotation * DEGREE_TO_RADIAN);
#else
		X = cos(newRotation);
		Y = sin(newRotation);
#endif
		IncrementCounter();
	}
}

#endif
#endif