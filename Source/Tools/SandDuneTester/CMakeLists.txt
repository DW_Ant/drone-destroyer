#########################################################################
# Establish directories
#########################################################################
include_directories ("Headers"
	"${ExternalDirectory}/SFML/Include"
	"${ExternalDirectory}/Box2D/include")
	
link_directories(${ExternalDirectory}/Box2D/lib/${ArchitectureFolderName})
link_directories(${ExternalDirectory}/SFML/lib/${ArchitectureFolderName})

SET (SandDuneTesterDirectory "${SourceDirectory}/Tools/SandDuneTester")
SET (WorkingDirectory ${SandDuneTesterDirectory})


#########################################################################
# Establish source files
#########################################################################

SET (HeaderFiles
	"Headers/Configuration.h"
	"Headers/CppUnitTest.h"
	"Headers/Definitions.h"
	"Headers/FeatureTestLauncher.h"
	"Headers/PhysicsTester.h"
	"Headers/PhysicsTesterTeleporter.h"
	"Headers/PhysicsTesterUi.h"
	"Headers/Player.h"
	"Headers/QaUnitTester.h"
	"Headers/SandDune.h"
	"Headers/SandDuneTester.h"
	"Headers/SandDuneTesterEngineComponent.h"
	"Headers/SandDuneTesterTheme.h"
	"Headers/SceneTester.h"
	"Headers/SceneTesterHud.h"
	"Headers/ShaderTester.h"
	"Headers/SpriteInstanceTester.h"
	"Headers/SpriteInstanceTesterUi.h"
	"Headers/WindowedEntityManager.h"
	"Headers/stdafx.h"
)

SET (SourceFiles
	"Source/CppUnitTest.cpp"
	"Source/FeatureTestLauncher.cpp"
	"Source/PhysicsTester.cpp"
	"Source/PhysicsTesterTeleporter.cpp"
	"Source/PhysicsTesterUi.cpp"
	"Source/Player.cpp"
	"Source/QaUnitTester.cpp"
	"Source/SandDuneTester.cpp"
	"Source/SandDuneTesterEngineComponent.cpp"
	"Source/SandDuneTesterTheme.cpp"
	"Source/SceneTester.cpp"
	"Source/SceneTesterHud.cpp"
	"Source/ShaderTester.cpp"
	"Source/SpriteInstanceTester.cpp"
	"Source/SpriteInstanceTesterUi.cpp"
	"Source/stdafx.cpp"
	"Source/WindowedEntityManager.cpp"
)

#Set definitions used in generated header file.
SET (MODULE_NAME "SandDuneTester")
SET (PREPROCESSOR_MODULE_NAME "SAND_DUNE_TESTER")

	
#########################################################################
# Build the module
#########################################################################

SET (AllFiles ${HeaderFiles} ${SourceFiles})

#Generate project filters that align with the source file's directory location.
ConfigureProjectFilters("${AllFiles}")

add_executable(${MODULE_NAME} ${ExecGenerationFlags} ${AllFiles})
ApplyTargetConfiguration(${MODULE_NAME})

set_target_properties (${MODULE_NAME} PROPERTIES FOLDER "Tools")

AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Core)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Command)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Time)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-File)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-SfmlGraphics)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Graphics)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Logger)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Console)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Localization)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-MultiThread)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Input)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Gui)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Geometry)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-Random)
AddSandDuneLibDependencyUnformatted(${MODULE_NAME} SD-World)

target_link_libraries(${MODULE_NAME}
	PRIVATE optimized Box2D
	PRIVATE debug Box2D-d
	PRIVATE optimized Box2D
	PRIVATE debug Box2D-d
	PRIVATE optimized sfml-system
	PRIVATE debug sfml-system-d
	PRIVATE optimized sfml-graphics
	PRIVATE debug sfml-graphics-d
	PRIVATE optimized sfml-window
	PRIVATE debug sfml-window-d)