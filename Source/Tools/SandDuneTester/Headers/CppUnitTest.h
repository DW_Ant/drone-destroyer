/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CppUnitTest.h
  Constructs several simple C++ classes, and conducts unit tests to see if the current platform
  can reproduce the expected C++ behavior.
=====================================================================
*/

#pragma once

#include <cassert>

#define CHECK_TRUE(condition) \
	assert(condition);

#define CHECK_FALSE(condition) \
	assert(!(condition));

class CppUnitTestLauncher
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	static void LaunchCppUnitTests ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	static void TestComparisons ();
	static void TestSizes ();
	static void TestOperations ();
	static void TestInheritance ();
	static void TestCasting ();
};

class CppUnitTest_BaseClass
{
public:
	virtual int GetNum () const;
};

class CppUnitTest_ProtectedInterface
{
protected:
	virtual int GetProtectedInterfaceNum () const = 0;
};

class CppUnitTest_PublicInterface
{
public:
	virtual int GetPublicInterfaceNum () const = 0;
};

class CppUnitTest_SubClass : public CppUnitTest_BaseClass, public CppUnitTest_PublicInterface, protected CppUnitTest_ProtectedInterface
{
private:
	int PrivateVar;

public:
	virtual int GetNum () const override;
	virtual int GetPublicInterfaceNum () const override;

protected:
	virtual int GetProtectedInterfaceNum () const override;
};