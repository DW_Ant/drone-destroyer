/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FeatureTestLauncher.h
  Pops up a window that allows the user to launch individual unit tests that typically
  have their own display or something that requires attention to run.

  This is for tests that are not running in an automated way. Yet it prevents the
  SandDuneTester from launching dozens of windows at start.
=====================================================================
*/

#pragma once

#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class QaUnitTester;
class ShaderTester;
class SpriteInstanceTester;
class SceneTester;
class PhysicsTester;

class FeatureTestLauncher : public WindowedEntityManager
{
	DECLARE_CLASS(FeatureTestLauncher)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Various tests this object has launched. */
	ShaderTester* ShaderTester;
	SpriteInstanceTester* SpriteTester;
	SceneTester* SceneTester;
	PhysicsTester* PhysicsTester;

	/* Simple UI that has individual buttons for launching a single test.
	If this GuiEntity becomes complicated enough, may want to consider using a custom class. */
	SD::GuiEntity* LauncherUi;

	SD::ButtonComponent* ShaderTesterButton;
	SD::ButtonComponent* SpriteInstanceButton;
	SD::ButtonComponent* SceneTestButton;
	SD::ButtonComponent* PhysicsTestButton;

	/* Object instance that kicked off this object. It's read only since usually CDOs initialize this test. */
	const QaUnitTester* QaTester;

	/* Unit test flags that were used when initializing the launcher. */
	SD::UnitTester::EUnitTestFlags TestFlags;

	/* If true, then this launcher will shutdown the local engine as soon as this is destroyed. */
	bool ShutdownEngineOnDestruction;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes all Entities that are needed to setup the launcher.
	 */
	virtual void InitializeLauncher (const QaUnitTester* launcher, SD::UnitTester::EUnitTestFlags testFlags, bool shutdownOnDestruction);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates and initializes all components for the LauncherUi.
	 */
	virtual void InitializeLauncherUi ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleLaunchShaderTester (SD::ButtonComponent* uiComponent);
	virtual void HandleShaderTesterTerminated ();

	virtual void HandleLaunchSpriteInstanceTester (SD::ButtonComponent* uiComponent);
	virtual void HandleSpriteInstanceTesterTerminated ();
	
	virtual void HandleLaunchSceneTester (SD::ButtonComponent* uiComponent);
	virtual void HandleSceneTesterTerminated ();

	virtual void HandleLaunchPhysicsTester (SD::ButtonComponent* uiComponent);
	virtual void HandlePhysicsTesterTerminated ();

	virtual void HandleCloseButtonReleased (SD::ButtonComponent* uiComponent);
};
SD_TESTER_END

#endif