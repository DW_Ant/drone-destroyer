/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PhysicsTesterTeleporter.h
  A simple Entity associated with the PhysicsTester that simply teleports all dynamic
  Physics Entities to a destination.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN

class PhysicsTesterTeleporter : public SD::SceneEntity
{
	DECLARE_CLASS(PhysicsTesterTeleporter)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then the teleporting Entities will preserve their X/Y coordinate. */
	bool PreserveXCoordinates;
	bool PreserveYCoordinates;

	/* Point in 3D space entities will teleport to once they overlap with this Entity. */
	SD::Vector3 TeleportDestination;

protected:
	SD::PhysicsComponent* PhysComp;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual SD::PhysicsComponent* GetPhysComp () const
	{
		return PhysComp;
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleBeginContact (SD::PhysicsComponent* otherComp, b2Contact* contactData);
};
SD_TESTER_END

#endif