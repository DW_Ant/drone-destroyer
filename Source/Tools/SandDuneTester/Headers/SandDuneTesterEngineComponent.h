/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SandDuneTesterEngineComponent.h
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

SD_TESTER_BEGIN
class SandDuneTesterEngineComponent : public SD::EngineComponent
{
	DECLARE_ENGINE_COMPONENT(SandDuneTesterEngineComponent)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool CanRunPreInitializeComponent (const std::vector<const SD::DClass*> executedEngineComponents) const override;
	virtual bool CanRunInitializeComponent (const std::vector<const SD::DClass*> executedEngineComponents) const override;
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Imports various utilized assets (like textures) to their corresponding asset pools.
	 */
	virtual void ImportAssets ();
};
SD_TESTER_END