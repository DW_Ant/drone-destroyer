/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SandDuneTester.h
=====================================================================
*/

#pragma once

#include "SandDune.h"

#define SD_TESTER_BEGIN namespace SDTester {
#define SD_TESTER_END }

#define TICK_GROUP_SD_TESTER "SdTester"
#define TICK_GROUP_PRIORITY_SD_TESTER 1200

#define FEATURE_ENGINE_IDX 4

SD_TESTER_BEGIN
extern SD::LogCategory SdTesterLog;
SD_TESTER_END

/**
 * Instantiates and returns a list of every Engine Component that'll be used for testing.
 * NOTE: This function actually creates engine components on the heap. If these aren't used to
 * initialize Engine instances, then these will have to be manually deleted.
 */
void GetEngineComponentList (std::vector<SD::EngineComponent*>& outEngineComponents);