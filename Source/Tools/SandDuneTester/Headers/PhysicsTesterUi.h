/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PhysicsTesterUi.h

  A simple GuiEntity that allows the end user to launch various physics tests.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class PhysicsTester;

class PhysicsTesterUi : public SD::GuiEntity
{
	DECLARE_CLASS(PhysicsTesterUi)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::ScrollbarComponent* MenuList;
	SD::GuiEntity* LaunchButtonsUi;
	std::vector<SD::ButtonComponent*> LaunchButtons;

	/* Entity responsible for managing this UI. */
	PhysicsTester* OwningTester;

protected:
	SD::Color StaticColor;
	SD::Color DynamicColor;
	SD::Color OverlapColor;
	SD::Color TeleporterColor;

private:
	/* All PhysicsComponents instantiated from this test will be using this number
	to ensure these entities will not collide against other objects instantiated from other systems. */
	int16 CollisionGroup;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetOwningTester (PhysicsTester* newOwningTester)
	{
		OwningTester = newOwningTester;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates and initializes the Render Entity for the given Entity.
	 * The Render component will attach itself to the Entity. It'll initialize its color, and it'll register to the tester's draw layer.
	 */
	virtual void InitializeRenderComponent (SD::SceneEntity* entity, SD::Color renderColor) const;

	/**
	 * Creates and initializes an Entity with a static Physics component that has a rectangular collision box.
	 * The Position specifies the center point of the rectangle.
	 */
	virtual SD::SceneEntity* CreateStaticRectangle (const SD::Vector3& position, const SD::Rotator& rotation, const SD::Rectangle<SD::FLOAT>& dimensions) const;

	/**
	 * Creates four static rectangles that form a perimeter matching the specified dimensions. Units are in SD units.
	 */
	virtual void CreateStaticPerimeter (std::vector<Entity*>& outEntityList, SD::FLOAT width, SD::FLOAT height) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleLaunchBouncyBallTest (SD::ButtonComponent* uiComponent);
	virtual void HandleLaunchMultiBallTest (SD::ButtonComponent* uiComponent);
	virtual void HandleLaunchOverlapTest (SD::ButtonComponent* uiComponent);
	virtual void HandleLaunchTumbleTest (SD::ButtonComponent* uiComponent);
};
SD_TESTER_END

#endif