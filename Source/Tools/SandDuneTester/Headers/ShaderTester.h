/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ShaderTester.h
  Object that'll be testing the ShaderComponent.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class ShaderTester : public SD::GuiEntity
{
	DECLARE_CLASS(ShaderTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Called whenever the test is terminated/destroyed. */
	SD::SDFunction<void> OnTestTerminated;

protected:
	SD::DPointer<SD::FrameComponent> Borders;
	SD::DPointer<SD::DropdownComponent> TextureDropdown;
	SD::DPointer<SD::DropdownComponent> SpriteShaderDropdown;
	SD::DPointer<SD::DropdownComponent> FontDropdown;
	SD::DPointer<SD::DropdownComponent> TextShaderDropdown;

	SD::DPointer<SD::LabelComponent> TextureDropdownLabel;
	SD::DPointer<SD::LabelComponent> SpriteShaderDropdownLabel;
	SD::DPointer<SD::LabelComponent> FontDropdownLabel;
	SD::DPointer<SD::LabelComponent> TextShaderDropdownLabel;

	SD::DPointer<SD::ButtonComponent> CloseButton;

	/* Various components that could be rendered with shaders. */
	SD::DPointer<SD::SpriteComponent> SpriteDemo;
	SD::DPointer<SD::LabelComponent> TextDemo;

	SD::DPointer<SD::GuiComponent> SpriteOwner;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through the Shader directory, and reports a list of all available shaders.
	 */
	virtual void GetShaderList (std::vector<SD::DString>& outShaderList) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTextureSelected (SD::INT dropdownIdx);
	virtual void HandleSpriteShaderSelected (SD::INT dropdownIdx);
	virtual void HandleFontSelected (SD::INT dropdownIdx);
	virtual void HandleTextShaderSelected (SD::INT dropdownIdx);
	virtual void HandleCloseButtonPressed (SD::ButtonComponent* uiComponent);
};
SD_TESTER_END

#endif