/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Player.h
  An Entity in 3D space that represents something that the player may manipulate.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class Player : public SD::Entity, public SD::SceneTransform
{
	DECLARE_CLASS(Player)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Various vectors that adjusts the player transform continuously. Values should range between -1 to 1 since they act like multipliers to the base speeds. */
	SD::Vector3 VelocityInput;
	SD::Vector3 GrowthInput;


protected:
	SD::DPointer<SD::SolidColorRenderComponent> RenderComp;
	SD::DPointer<SD::InputComponent> InputComp;
	SD::TickComponent* Tick;

	/* Determines how quickly this player may traverse through an axis. */
	SD::FLOAT MoveSpeed;

	/* Determines how quickly this player may grow/shrink in size. */
	SD::FLOAT GrowSpeed;

private:
	/* Various input flags for player movement commands. */
	bool InputLeft : 1;
	bool InputUp : 1;
	bool InputRight : 1;
	bool InputDown : 1;
	bool InputGrow : 1;
	bool InputShrink : 1;
	unsigned int InputUnused : 2;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void BeginObject () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::SolidColorRenderComponent* GetRenderComp () const
	{
		return RenderComp.Get();
	}

	inline SD::InputComponent* GetInputComp () const
	{
		return InputComp.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleKeyboardInput (const sf::Event& evnt);
	virtual void HandleTick (SD::FLOAT deltaSec);
};
SD_TESTER_END

#endif