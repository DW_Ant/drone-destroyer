/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SandDune.h
  This file includes all enabled modules that makes up the SandDune engine.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Command\Headers\CommandClasses.h"
#include "Engine\Time\Headers\TimeClasses.h"
#include "Engine\File\Headers\FileClasses.h"
#include "Engine\SfmlGraphics\Headers\SfmlGraphicsClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
//#include "Engine\BaseEditor\Headers\BaseEditorClasses.h"
#include "Engine\Logger\Headers\LoggerClasses.h"
#include "Engine\Console\Headers\ConsoleClasses.h"
#include "Engine\Localization\Headers\LocalizationClasses.h"
#include "Engine\MultiThread\Headers\MultiThreadClasses.h"
#include "Engine\Input\Headers\InputClasses.h"
#include "Engine\Gui\Headers\GuiClasses.h"
//#include "Engine\GuiEditor\Headers\GuiEditorClasses.h"
#include "Engine\Geometry\Headers\GeometryClasses.h"
#include "Engine\Random\Headers\RandomClasses.h"
#include "Engine\World\Headers\WorldClasses.h"