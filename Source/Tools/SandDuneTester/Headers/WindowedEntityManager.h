/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  WindowedEntityManager.h
  The SandDuneTester application has numerous unit tests that needs to launch in their
  own isolated windows with their own input handlers and draw layers.

  This Entity is responsible for managing these objects and properly shutting them down through
  destruction or through window closures.
=====================================================================
*/

#pragma once

#include "SandDuneTester.h"

#ifdef DEBUG_MODE
SD_TESTER_BEGIN
class WindowedEntityManager : public SD::Entity
{
	DECLARE_CLASS(WindowedEntityManager)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::Window* WindowHandle;
	SD::GuiDrawLayer* GuiDrawLayer;
	SD::PlanarDrawLayer* MainDrawLayer;
	SD::MousePointer* Mouse;
	SD::InputBroadcaster* Input;

private:
	bool isInitialized;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes all base Entities for this manager.
	 */
	virtual void InitializeEntityManager (const SD::Vector2& windowSize, const SD::DString& windowName, SD::FLOAT windowUpdateRate = 0.0333f /*~30 fps*/);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsInitialized () const
	{
		return isInitialized;
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleWindowEvent (const sf::Event& windowEvent);
};
SD_TESTER_END

#endif