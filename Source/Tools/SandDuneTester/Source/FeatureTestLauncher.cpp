/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FeatureTestLauncher.cpp
=====================================================================
*/

#include "SandDuneTester.h"
#include "FeatureTestLauncher.h"
#include "ShaderTester.h"
#include "SpriteInstanceTester.h"
#include "SceneTester.h"
#include "PhysicsTester.h"
#include "QaUnitTester.h"

#ifdef DEBUG_MODE

SD_TESTER_BEGIN
IMPLEMENT_CLASS(SDTester, FeatureTestLauncher, SDTester, WindowedEntityManager)

void FeatureTestLauncher::InitProps ()
{
	Super::InitProps();

	ShaderTester = nullptr;
	SpriteTester = nullptr;
	SceneTester = nullptr;
	PhysicsTester = nullptr;

	LauncherUi = nullptr;
	ShaderTesterButton = nullptr;
	SpriteInstanceButton = nullptr;
	SceneTestButton = nullptr;
	PhysicsTestButton = nullptr;
	ShutdownEngineOnDestruction = false;
}

void FeatureTestLauncher::Destroyed ()
{
	if (ShaderTester != nullptr)
	{
		ShaderTester->Destroy();
		ShaderTester = nullptr;
	}

	if (SpriteTester != nullptr)
	{
		SpriteTester->Destroy();
		SpriteTester = nullptr;
	}

	if (SceneTester != nullptr)
	{
		SceneTester->Destroy();
		SceneTester = nullptr;
	}

	if (PhysicsTester != nullptr)
	{
		PhysicsTester->Destroy();
		PhysicsTester = nullptr;
	}

	if (LauncherUi != nullptr)
	{
		LauncherUi->Destroy();
		LauncherUi = nullptr;

		//Components within a destroyed Entity are also destroyed. Simply set them to null.
		ShaderTesterButton = nullptr;
		SpriteInstanceButton = nullptr;
	}

	if (ShutdownEngineOnDestruction)
	{
		SD::Engine* localEngine = SD::Engine::FindEngine();
		CHECK(localEngine != nullptr)
		localEngine->Shutdown();
	}

	Super::Destroyed();
}

void FeatureTestLauncher::InitializeLauncher (const QaUnitTester* tester, SD::UnitTester::EUnitTestFlags testFlags, bool shutdownOnDestruction)
{
	CHECK(!IsInitialized())
	QaTester = tester;
	TestFlags = testFlags;
	ShutdownEngineOnDestruction = shutdownOnDestruction;

	const float numButtons = 5.f;
	const float buttonSpace = 32.f;
	const SD::Vector2 windowSize(256.f, (buttonSpace * numButtons) + buttonSpace);
	InitializeEntityManager(windowSize, TXT("Unit Test Launcher"));

	InitializeLauncherUi();
}

void FeatureTestLauncher::InitializeLauncherUi ()
{
	LauncherUi = SD::GuiEntity::CreateObject();
	GuiDrawLayer->RegisterMenu(LauncherUi);
	LauncherUi->SetupInputComponent(Input, 100);
	LauncherUi->SetPosition(SD::Vector2::ZeroVector);
	LauncherUi->SetSize(SD::Vector2(1.f, 1.f));

	SD::FocusComponent* focusComp = LauncherUi->GetFocus();
	SD::ContainerUtils::Empty(focusComp->TabOrder);

	SD::FLOAT buttonPadding = 12.f;
	SD::FLOAT buttonHeight = 24.f;
	SD::FLOAT buttonPos = buttonPadding;

	ShaderTesterButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(ShaderTesterButton))
	{
		ShaderTesterButton->SetSize(0.f, buttonHeight);
		ShaderTesterButton->SetPosition(0.f, buttonPos);
		ShaderTesterButton->SetAnchorLeft(buttonPadding);
		ShaderTesterButton->SetAnchorRight(buttonPadding);
		buttonPos += buttonHeight + buttonPadding;
		ShaderTesterButton->SetCaptionText(TXT("Launch Shader Test"));
		ShaderTesterButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchShaderTester, void, SD::ButtonComponent*));
		focusComp->TabOrder.push_back(ShaderTesterButton);
	}

	SpriteInstanceButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(SpriteInstanceButton))
	{
		SpriteInstanceButton->SetSize(0.f, buttonHeight);
		SpriteInstanceButton->SetPosition(0.f, buttonPos);
		SpriteInstanceButton->SetAnchorLeft(buttonPadding);
		SpriteInstanceButton->SetAnchorRight(buttonPadding);
		buttonPos += buttonHeight + buttonPadding;
		SpriteInstanceButton->SetCaptionText(TXT("Launch Sprite Instance Test"));
		SpriteInstanceButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchSpriteInstanceTester, void, SD::ButtonComponent*));
		focusComp->TabOrder.push_back(SpriteInstanceButton);
	}

	SceneTestButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(SceneTestButton))
	{
		SceneTestButton->SetSize(0.f, buttonHeight);
		SceneTestButton->SetPosition(0.f, buttonPos);
		SceneTestButton->SetAnchorLeft(buttonPadding);
		SceneTestButton->SetAnchorRight(buttonPadding);
		buttonPos += buttonHeight + buttonPadding;
		SceneTestButton->SetCaptionText(TXT("Launch Scene Test"));
		SceneTestButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchSceneTester, void, SD::ButtonComponent*));
		focusComp->TabOrder.push_back(SceneTestButton);
	}

	PhysicsTestButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(PhysicsTestButton))
	{
		PhysicsTestButton->SetSize(0.f, buttonHeight);
		PhysicsTestButton->SetPosition(0.f, buttonPos);
		PhysicsTestButton->SetAnchorLeft(buttonPadding);
		PhysicsTestButton->SetAnchorRight(buttonPadding);
		buttonPos += buttonHeight + buttonPadding;
		PhysicsTestButton->SetCaptionText(TXT("Launch Physics Test"));
		PhysicsTestButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleLaunchPhysicsTester, void, SD::ButtonComponent*));
		focusComp->TabOrder.push_back(PhysicsTestButton);
	}

	SD::ButtonComponent* closeButton = SD::ButtonComponent::CreateObject();
	if (LauncherUi->AddComponent(closeButton))
	{
		closeButton->SetSize(0.f, buttonHeight);
		closeButton->SetPosition(0.f, buttonPos);
		closeButton->SetAnchorLeft(buttonPadding);
		closeButton->SetAnchorRight(buttonPadding);
		buttonPos += buttonHeight + buttonPadding;
		closeButton->SetCaptionText(TXT("Close"));
		closeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, FeatureTestLauncher, HandleCloseButtonReleased, void, SD::ButtonComponent*));
		focusComp->TabOrder.push_back(closeButton);
	}
}

void FeatureTestLauncher::HandleLaunchShaderTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	ShaderTester = ShaderTester::CreateObject();
	CHECK(ShaderTester != nullptr)
	ShaderTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleShaderTesterTerminated, void);
	QaTester->TestLog(TestFlags, TXT("ShaderTester created.  Entity will automatically perish when user clicks on close button."));

	if (ShaderTesterButton != nullptr)
	{
		ShaderTesterButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleShaderTesterTerminated ()
{
	ShaderTester = nullptr;
	if (ShaderTesterButton != nullptr)
	{
		ShaderTesterButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchSpriteInstanceTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)
	
	SpriteTester = SpriteInstanceTester::CreateObject();
	CHECK(SpriteTester != nullptr)
	SpriteTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleSpriteInstanceTesterTerminated, void);
	SpriteTester->InitializeTest(QaTester, TestFlags);
	QaTester->TestLog(TestFlags, TXT("Sprite Instance Tester created.  Entity will automatically perish when user clicks on close button."));

	if (SpriteInstanceButton != nullptr)
	{
		SpriteInstanceButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleSpriteInstanceTesterTerminated ()
{
	SpriteTester = nullptr;
	if (SpriteInstanceButton != nullptr)
	{
		SpriteInstanceButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchSceneTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	SceneTester = SceneTester::CreateObject();
	CHECK(SceneTester != nullptr)
	SceneTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandleSceneTesterTerminated, void);
	SceneTester->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("SceneTester is created. This test will automatically terminate based on user input."));

	if (SceneTestButton != nullptr)
	{
		SceneTestButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandleSceneTesterTerminated ()
{
	SceneTester = nullptr;
	if (SceneTestButton != nullptr)
	{
		SceneTestButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleLaunchPhysicsTester (SD::ButtonComponent* uiComponent)
{
	CHECK(QaTester != nullptr)

	PhysicsTester = PhysicsTester::CreateObject();
	CHECK(PhysicsTester != nullptr)
	PhysicsTester->OnTestTerminated = SDFUNCTION(this, FeatureTestLauncher, HandlePhysicsTesterTerminated, void);
	PhysicsTester->LaunchTest(TestFlags);
	QaTester->TestLog(TestFlags, TXT("PhysicsTester is created. This test will automatically terminate based on user input."));

	if (PhysicsTestButton != nullptr)
	{
		PhysicsTestButton->SetEnabled(false);
	}
}

void FeatureTestLauncher::HandlePhysicsTesterTerminated ()
{
	PhysicsTester = nullptr;
	if (PhysicsTestButton != nullptr)
	{
		PhysicsTestButton->SetEnabled(true);
	}
}

void FeatureTestLauncher::HandleCloseButtonReleased (SD::ButtonComponent* uiComponent)
{
	if (WindowHandle != nullptr)
	{
		//No need to invoke closed event since this Entity is already aware of it.
		WindowHandle->InvokeCloseOnDestruction = false;
	}

	Destroy();
}
SD_TESTER_END
#endif