/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PhysicsTesterUi.cpp
=====================================================================
*/

#include "PhysicsTesterUi.h"
#include "PhysicsTester.h"
#include "PhysicsTesterTeleporter.h"

#ifdef DEBUG_MODE

SD_TESTER_BEGIN
IMPLEMENT_CLASS(SDTester, PhysicsTesterUi, SD, GuiEntity)

void PhysicsTesterUi::InitProps ()
{
	Super::InitProps();

	MenuList = nullptr;
	LaunchButtonsUi = nullptr;
	OwningTester = nullptr;

	StaticColor = SD::Color::GREEN;
	DynamicColor = SD::Color::CYAN;
	OverlapColor = SD::Color::RED;
	TeleporterColor = SD::Color::MAGENTA;

	CollisionGroup = 29859; //Some hard-coded magic number that should be unique to any other collision systems.
}

void PhysicsTesterUi::ConstructUI ()
{
	Super::ConstructUI();

	MenuList = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(MenuList))
	{
		MenuList->SetSize(SD::Vector2(0.15f, 0.33f));
		MenuList->SetHideControlsWhenFull(true);
		MenuList->SetZoomEnabled(false);
		CHECK(MenuList->GetFrameTexture() != nullptr)
		MenuList->GetFrameTexture()->ResetColor = SD::Color(32, 32, 32);

		LaunchButtonsUi = SD::GuiEntity::CreateObject();
		CHECK(LaunchButtonsUi != nullptr)

		LaunchButtonsUi->SetAutoSizeVertical(true);
		LaunchButtonsUi->SetEnableFractionScaling(false);
		LaunchButtonsUi->SetGuiSizeToOwningScrollbar(SD::Vector2(1.f, -1.f));
		MenuList->SetViewedObject(LaunchButtonsUi);

		SD::FLOAT margins = 4.f;
		SD::FLOAT buttonHeight = 16.f;
		SD::FLOAT verticalButtonPos = margins;

		std::function<void(SD::ButtonComponent*)> initButton([&](SD::ButtonComponent* button)
		{
			button->SetPosition(SD::Vector2(0.f, verticalButtonPos));
			button->SetSize(SD::Vector2(1.f, buttonHeight));
			button->SetAnchorLeft(margins);
			button->SetAnchorRight(margins);
			LaunchButtons.push_back(button);
			verticalButtonPos += buttonHeight + margins;
		});

		SD::ButtonComponent* bouncingBallButton = SD::ButtonComponent::CreateObject();
		if (LaunchButtonsUi->AddComponent(bouncingBallButton))
		{
			initButton(bouncingBallButton);
			bouncingBallButton->SetCaptionText(TXT("Bouncy Ball Test"));
			bouncingBallButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PhysicsTesterUi, HandleLaunchBouncyBallTest, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* multiBallButton = SD::ButtonComponent::CreateObject();
		if (LaunchButtonsUi->AddComponent(multiBallButton))
		{
			initButton(multiBallButton);
			multiBallButton->SetCaptionText(TXT("Multi Balls Test"));
			multiBallButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PhysicsTesterUi, HandleLaunchMultiBallTest, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* overlapButton = SD::ButtonComponent::CreateObject();
		if (LaunchButtonsUi->AddComponent(overlapButton))
		{
			initButton(overlapButton);
			overlapButton->SetCaptionText(TXT("Overlap Test"));
			overlapButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PhysicsTesterUi, HandleLaunchOverlapTest, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* tumbleButton = SD::ButtonComponent::CreateObject();
		if (LaunchButtonsUi->AddComponent(tumbleButton))
		{
			initButton(tumbleButton);
			tumbleButton->SetCaptionText(TXT("Tumble Test"));
			tumbleButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PhysicsTesterUi, HandleLaunchTumbleTest, void, SD::ButtonComponent*));
		}
	}
}

void PhysicsTesterUi::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();

	CHECK(Focus != nullptr)

	for (size_t i = 0; i < LaunchButtons.size(); ++i)
	{
		Focus->TabOrder.push_back(LaunchButtons.at(i));
	}
}

void PhysicsTesterUi::InitializeRenderComponent (SD::SceneEntity* entity, SD::Color renderColor) const
{
	CHECK(entity != nullptr)

	if (OwningTester != nullptr && OwningTester->GetSceneDrawLayer() != nullptr)
	{
		SD::PhysicsRenderer* renderer = SD::PhysicsRenderer::CreateObject();
		if (entity->AddComponent(renderer))
		{
			renderer->FillColor = renderColor;
			OwningTester->GetSceneDrawLayer()->RegisterSingleComponent(renderer);
		}
	}
	else
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to initialize Render Entity for %s since either the owning tester or its draw layer is not initialized."), entity->DebugName);
	}
}

SD::SceneEntity* PhysicsTesterUi::CreateStaticRectangle (const SD::Vector3& position, const SD::Rotator& rotation, const SD::Rectangle<SD::FLOAT>& dimensions) const
{
	SD::SceneEntity* entity = SD::SceneEntity::CreateObject();
	entity->SetTranslation(position);
	entity->SetRotation(rotation);

	SD::PhysicsComponent* comp = SD::PhysicsComponent::CreateObject();
	if (entity->AddComponent(comp))
	{
		SD::PhysicsComponent::SPhysicsInitData initData;
		initData.PhysicsType = SD::PhysicsComponent::PT_Static;
		comp->InitializePhysics(initData);
		if (b2Body* body = comp->GetPhysicsBody())
		{
			b2FixtureDef fixtureInit;
			b2PolygonShape shape = SD::Box2dUtils::GetBox2dRectangle(dimensions);
			fixtureInit.shape = &shape;
			fixtureInit.density = 0.f;

			body->CreateFixture(&fixtureInit);
		}
		else
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to retrieve static rectangle's physics component's body."));
		}

		comp->SetCollisionGroup(CollisionGroup);
		InitializeRenderComponent(entity, StaticColor);
	}

	return entity;
}

void PhysicsTesterUi::CreateStaticPerimeter (std::vector<Entity*>& outEntityList, SD::FLOAT width, SD::FLOAT height) const
{
	SD::FLOAT halfWidth = width * 0.5f;
	SD::FLOAT halfHeight = height * 0.5f;
	SD::FLOAT halfRectWidth = 10.f;

	SD::SceneEntity* rightWall = CreateStaticRectangle(SD::Vector3(halfWidth, 0.f, 0.f), SD::Rotator::ZERO_ROTATOR, SD::Rectangle<SD::FLOAT>(-halfRectWidth, halfHeight, halfRectWidth, -halfHeight));
	rightWall->DebugName = TXT("Right Wall");
	outEntityList.push_back(rightWall);

	SD::SceneEntity* bottomWall = CreateStaticRectangle(SD::Vector3(0.f, halfHeight, 0.f), SD::Rotator::ZERO_ROTATOR, SD::Rectangle<SD::FLOAT>(-halfWidth, halfRectWidth, halfWidth, -halfRectWidth));
	bottomWall->DebugName = TXT("Bottom Wall");
	outEntityList.push_back(bottomWall);

	SD::SceneEntity* leftWall = CreateStaticRectangle(SD::Vector3(-halfWidth, 0.f, 0.f), SD::Rotator::ZERO_ROTATOR, SD::Rectangle<SD::FLOAT>(-halfRectWidth, halfHeight, halfRectWidth, -halfHeight));
	leftWall->DebugName = TXT("Left Wall");
	outEntityList.push_back(leftWall);

	SD::SceneEntity* topWall = CreateStaticRectangle(SD::Vector3(0.f, -halfHeight, 0.f), SD::Rotator::ZERO_ROTATOR, SD::Rectangle<SD::FLOAT>(-halfWidth, halfRectWidth, halfWidth, -halfRectWidth));
	topWall->DebugName = TXT("Top Wall");
	outEntityList.push_back(topWall);
}

void PhysicsTesterUi::HandleLaunchBouncyBallTest (SD::ButtonComponent* uiComponent)
{
	std::vector<SD::Entity*> entityList;

	CreateStaticPerimeter(OUT entityList, 1000.f, 1000.f);

	SD::SceneEntity* tester = SD::SceneEntity::CreateObject();
	tester->DebugName = TXT("Bouncing Circle");
	entityList.push_back(tester);

	SD::PhysicsComponent::SComponentInitData compInit;
	compInit.BodyInit.PhysicsType = SD::PhysicsComponent::PT_Dynamic;
	compInit.BodyInit.LinearDamping = 0.f;
	compInit.BodyInit.AngularDamping = 0.f;
	compInit.Friction = 0.f;
	compInit.Restitution = 1.f;
	compInit.StarterVelocity = SD::Vector3(500.f, 300.f, 0.f); //5 and 3 meters per second
	compInit.CollisionGroup = CollisionGroup;

	b2CircleShape circle;
	circle.m_radius = 1.f; //1 meter radius
	compInit.CollisionShape = &circle;

	SD::PhysicsComponent* comp = SD::PhysicsComponent::CreateAndInitializeComponent<SD::PhysicsComponent>(tester, compInit);
	InitializeRenderComponent(tester, DynamicColor);
	
	if (OwningTester != nullptr)
	{
		OwningTester->LaunchNewPhysicsTest(entityList);
	}
}

void PhysicsTesterUi::HandleLaunchMultiBallTest (SD::ButtonComponent* uiComponent)
{
	std::vector<SD::Entity*> entityList;

	SD::FLOAT arenaWidth = 2000.f;
	SD::FLOAT arenaHeight = 1000.f;
	CreateStaticPerimeter(OUT entityList, arenaWidth, arenaHeight);

	SD::PhysicsComponent::SComponentInitData compInit;
	compInit.BodyInit.PhysicsType = SD::PhysicsComponent::PT_Dynamic;
	compInit.BodyInit.LinearDamping = 0.f;
	compInit.BodyInit.AngularDamping = 0.f;
	compInit.Friction = 0.f;
	compInit.Restitution = 1.f;
	compInit.CollisionGroup = CollisionGroup;

	b2CircleShape circle;
	compInit.CollisionShape = &circle;

	SD::Vector3 startPosition(arenaWidth * -0.5f, arenaHeight * -0.5f, 0.f);
	SD::FLOAT maxCircleDiameter = 200.f;
	startPosition.Y += maxCircleDiameter;

	for (size_t i = 0; i < 32; ++i)
	{
		startPosition.X += maxCircleDiameter;
		if (startPosition.X + maxCircleDiameter >= arenaWidth * 0.5f)
		{
			startPosition.Y += maxCircleDiameter;
			startPosition.X = (arenaWidth * -0.5f) + maxCircleDiameter;
		}

		SD::SceneEntity* ball = SD::SceneEntity::CreateObject();
		ball->DebugName = TXT("Ball ") + SD::DString::MakeString(i);
		ball->SetTranslation(startPosition);
		entityList.push_back(ball);

		compInit.StarterVelocity = SD::Vector3(SD::RandomUtils::RandRange(-600.f, 600.f), SD::RandomUtils::RandRange(-600.f, 600.f), 0.f);
		circle.m_radius = SD::Box2dUtils::ToBox2dDist(SD::RandomUtils::RandRange(10.f, maxCircleDiameter * 0.5f)); //10cm to 1 meter radii

		SD::PhysicsComponent* comp = SD::PhysicsComponent::CreateAndInitializeComponent<SD::PhysicsComponent>(ball, compInit);
		InitializeRenderComponent(ball, DynamicColor);
	}
	
	if (OwningTester != nullptr)
	{
		OwningTester->LaunchNewPhysicsTest(entityList);
	}
}

void PhysicsTesterUi::HandleLaunchOverlapTest (SD::ButtonComponent* uiComponent)
{
	std::vector<SD::Entity*> entityList;

	SD::FLOAT arenaWidth = 1500.f;
	SD::FLOAT arenaHeight = 1000.f;
	CreateStaticPerimeter(OUT entityList, arenaWidth, arenaHeight);

	SD::PhysicsComponent::SComponentInitData compInit;
	compInit.BodyInit.PhysicsType = SD::PhysicsComponent::PT_Dynamic;
	compInit.BodyInit.LinearDamping = 0.f;
	compInit.BodyInit.AngularDamping = 0.f;
	compInit.Friction = 0.f;
	compInit.Restitution = 1.f;
	compInit.CollisionGroup = CollisionGroup;

	b2CircleShape circle;
	compInit.CollisionShape = &circle;

	//Left ball
	{
		SD::SceneEntity* leftBall = SD::SceneEntity::CreateObject();
		leftBall->DebugName = TXT("Left Ball");
		leftBall->SetTranslation(SD::Vector3(-650.f, 0.f, 0.f));
		entityList.push_back(leftBall);

		compInit.StarterVelocity = SD::Vector3(600.f, 500.f, 0.f);
		circle.m_radius = SD::Box2dUtils::ToBox2dDist(SD::RandomUtils::RandRange(10.f, 100.f)); //10cm to 1 meter radii

		SD::PhysicsComponent* comp = SD::PhysicsComponent::CreateAndInitializeComponent<SD::PhysicsComponent>(leftBall, compInit);
		InitializeRenderComponent(leftBall, DynamicColor);
	}

	//Right ball
	{
		SD::SceneEntity* rightBall = SD::SceneEntity::CreateObject();
		rightBall->DebugName = TXT("Right Ball");
		rightBall->SetTranslation(SD::Vector3(650.f, 0.f, 0.f));
		entityList.push_back(rightBall);

		compInit.StarterVelocity = SD::Vector3(-650.f, -400.f, 0.f);
		circle.m_radius = SD::Box2dUtils::ToBox2dDist(SD::RandomUtils::RandRange(10.f, 100.f)); //10cm to 1 meter radii

		SD::PhysicsComponent* comp = SD::PhysicsComponent::CreateAndInitializeComponent<SD::PhysicsComponent>(rightBall, compInit);
		InitializeRenderComponent(rightBall, DynamicColor);
	}

	//Dynamic Overlap ball
	{
		SD::SceneEntity* overlapBall = SD::SceneEntity::CreateObject();
		overlapBall->DebugName = TXT("Moving Overlap Ball");
		overlapBall->SetTranslation(SD::Vector3(0.f, 0.f, -1));
		entityList.push_back(overlapBall);

		compInit.StarterVelocity = SD::Vector3(250.f, 0.f, 0.f);
		compInit.IsBlockingComponent = false;
		circle.m_radius = SD::Box2dUtils::ToBox2dDist(150.f); //1.5 meter radius

		SD::PhysicsComponent* comp = SD::PhysicsComponent::CreateAndInitializeComponent<SD::PhysicsComponent>(overlapBall, compInit);
		InitializeRenderComponent(overlapBall, OverlapColor);
	}

	//Static Overlap ball
	{
		SD::SceneEntity* overlapBall = SD::SceneEntity::CreateObject();
		overlapBall->DebugName = TXT("Stationary Overlap Ball");
		overlapBall->SetTranslation(SD::Vector3(0.f, 0.f, -1.f));
		entityList.push_back(overlapBall);

		compInit.StarterVelocity = SD::Vector3(1.f, 1.f, 1.f);
		compInit.IsBlockingComponent = false;
		compInit.BodyInit.PhysicsType = SD::PhysicsComponent::ePhysicsType::PT_Static;
		circle.m_radius = SD::Box2dUtils::ToBox2dDist(150.f); //1.5 meter radius

		SD::PhysicsComponent* comp = SD::PhysicsComponent::CreateAndInitializeComponent<SD::PhysicsComponent>(overlapBall, compInit);
		InitializeRenderComponent(overlapBall, OverlapColor);
	}

	if (OwningTester != nullptr)
	{
		OwningTester->LaunchNewPhysicsTest(entityList);
	}
}

void PhysicsTesterUi::HandleLaunchTumbleTest (SD::ButtonComponent* uiComponent)
{
	std::vector<SD::Entity*> entityList;

	SD::FLOAT arenaWidth = 2000.f;
	SD::FLOAT arenaHeight = 1250.f;
	CreateStaticPerimeter(OUT entityList, arenaWidth, arenaHeight);

	SD::PhysicsComponent::SComponentInitData compInit;
	compInit.BodyInit.PhysicsType = SD::PhysicsComponent::PT_Static;
	compInit.BodyInit.LinearDamping = 0.f;
	compInit.BodyInit.AngularDamping = 0.f;
	compInit.Friction = 0.f;
	compInit.Restitution = 1.f;
	compInit.CollisionGroup = CollisionGroup;

	b2CircleShape circle;
	compInit.CollisionShape = &circle;

	SD::Rectangle<SD::FLOAT> rectangleDimensions(-200.f, 10.f, 200.f, -10.f);
	SD::SceneEntity* topRight = CreateStaticRectangle(SD::Vector3(512.f, -512.f, 0.f), SD::Rotator(-30.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* topCenter = CreateStaticRectangle(SD::Vector3(128.f, -200.f, 0.f), SD::Rotator(20.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* bottomRight = CreateStaticRectangle(SD::Vector3(512.f, 512.f, 0.f), SD::Rotator(30.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* bottomCenter = CreateStaticRectangle(SD::Vector3(128.f, 200.f, 0.f), SD::Rotator(-20.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* centerTopHalf = CreateStaticRectangle(SD::Vector3(-64.f, 96.f, 0.f), SD::Rotator(-30.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* centerBottomHalf = CreateStaticRectangle(SD::Vector3(-64.f, -96.f, 0.f), SD::Rotator(30.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* topLeft = CreateStaticRectangle(SD::Vector3(-512.f, -300.f, 0.f), SD::Rotator(-45.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);
	SD::SceneEntity* bottomLeft = CreateStaticRectangle(SD::Vector3(-512.f, 300.f, 0.f), SD::Rotator(45.f, 0.f, 0.f, SD::Rotator::RU_Degrees), rectangleDimensions);

	PhysicsTesterTeleporter* teleporter = PhysicsTesterTeleporter::CreateObject();
	//Initialize teleporter
	{
		teleporter->SetTranslation(SD::Vector3(arenaWidth * -0.485f, 0.f, 0.f));
		teleporter->TeleportDestination = SD::Vector3(arenaWidth * 0.475f, 0.f, 0.f);
		teleporter->PreserveYCoordinates = true;

		SD::PhysicsComponent* physComp = teleporter->GetPhysComp();
		CHECK(physComp != nullptr)
		SD::PhysicsComponent::SPhysicsInitData teleportInit;
		teleportInit.PhysicsType = SD::PhysicsComponent::PT_Static;
		physComp->InitializePhysics(teleportInit);
		if (b2Body* body = physComp->GetPhysicsBody())
		{
			b2FixtureDef fixtureInit;
			b2PolygonShape shape = SD::Box2dUtils::GetBox2dRectangle(SD::Rectangle<SD::FLOAT>(-32.f, arenaHeight * 0.5f, 32.f, arenaHeight * -0.5f));
			fixtureInit.shape = &shape;
			fixtureInit.isSensor = true;

			body->CreateFixture(&fixtureInit);
		}
		
		physComp->SetCollisionGroup(CollisionGroup);
		InitializeRenderComponent(teleporter, TeleporterColor);
		entityList.push_back(teleporter);
	}

	//Create numerous moving/tumbling crosses
	for (int i = 0; i < 32; ++i)
	{
		SD::SceneEntity* cross = SD::SceneEntity::CreateObject();
		cross->DebugName = TXT("Tumbling Cross");
		cross->SetTranslation(SD::Vector3(arenaWidth * 0.45f + SD::RandomUtils::RandRange(-64.f, 0.f), SD::RandomUtils::RandRange(arenaHeight * -0.45f, arenaHeight * 0.45f), SD::FLOAT::MakeFloat(i)));
		entityList.push_back(cross);

		SD::PhysicsComponent* crossPhys = SD::PhysicsComponent::CreateObject();
		if (cross->AddComponent(crossPhys))
		{
			crossPhys->SetAcceleration(SD::Vector3(-32.f, 0.f, 0.f));

			SD::PhysicsComponent::SPhysicsInitData crossInit;
			crossInit.AngularDamping = 0.f;
			crossInit.IsFixedRotation = false;
			crossInit.LinearDamping = 0.f;
			crossInit.PhysicsType = SD::PhysicsComponent::PT_Dynamic;
			crossPhys->InitializePhysics(crossInit);
			if (b2Body* crossBody = crossPhys->GetPhysicsBody())
			{
				b2FixtureDef fixtureInit;
				b2PolygonShape shape = SD::Box2dUtils::GetBox2dRectangle(SD::Rectangle<SD::FLOAT>(-4.f, 24.f, 4.f, -24.f));
				shape.m_type = b2Shape::e_polygon;
				fixtureInit.shape = &shape;
				fixtureInit.isSensor = false;
				fixtureInit.restitution = 0.5f;
				fixtureInit.friction = 0.25f;
				fixtureInit.density = 1.f;
				crossBody->CreateFixture(&fixtureInit);

				shape = SD::Box2dUtils::GetBox2dRectangle(SD::Rectangle<SD::FLOAT>(-24.f, 4.f, 24.f, -4.f));
				crossBody->CreateFixture(&fixtureInit);
			}

			crossPhys->SetCollisionGroup(CollisionGroup);
			InitializeRenderComponent(cross, DynamicColor);
		}
		else
		{
			SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to attached PhysicsComponent to a SceneEntity when initializing the tumbling crosses."));
		}
	}

	if (OwningTester != nullptr)
	{
		OwningTester->LaunchNewPhysicsTest(entityList);
	}
}

SD_TESTER_END

#endif