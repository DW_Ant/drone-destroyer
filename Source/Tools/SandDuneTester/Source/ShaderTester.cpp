/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ShaderTester.cpp
=====================================================================
*/

#include "ShaderTester.h"

#ifdef DEBUG_MODE

SD_TESTER_BEGIN
IMPLEMENT_CLASS(SDTester, ShaderTester, SD, GuiEntity)

void ShaderTester::InitProps ()
{
	Super::InitProps();

	Borders = nullptr;
	TextureDropdown = nullptr;
	SpriteShaderDropdown = nullptr;
	FontDropdown = nullptr;
	TextShaderDropdown = nullptr;

	TextureDropdownLabel = nullptr;
	SpriteShaderDropdownLabel = nullptr;
	FontDropdownLabel = nullptr;
	TextShaderDropdownLabel = nullptr;

	CloseButton = nullptr;

	SpriteDemo = nullptr;
	TextDemo = nullptr;
	SpriteOwner = nullptr;
}

void ShaderTester::ConstructUI ()
{
	SetSize(SD::Vector2(512.f, 384.f));
	SetPosition(SD::Vector2(32.f, 64.f));

	Borders = SD::FrameComponent::CreateObject();
	if (!AddComponent(Borders.Get()))
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to construct ShaderTester UI since it could not attach the Borders component to GuiEntity."));
		Borders = nullptr;
		return;
	}
	else
	{
		Borders->SetSize(ReadCachedAbsSize() - SD::Vector2(Borders->GetBorderThickness(), Borders->GetBorderThickness()));
		Borders->SetBorderThickness(1.f);
		Borders->SetLockedFrame(true);
	}

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	SD::Texture* defaultSpriteTexture = localTexturePool->FindTexture(TXT("DebuggingTexture"));
	std::vector<SD::DString> shaderList;
	shaderList.push_back("None");
	GetShaderList(shaderList);

	std::vector<SD::GuiDataElement<SD::DString>> shaderDropdownList;
	for (UINT_TYPE i = 0; i < shaderList.size(); i++)
	{
		shaderDropdownList.push_back(SD::GuiDataElement<SD::DString>(shaderList.at(i), shaderList.at(i)));
	}

	TextureDropdown = SD::DropdownComponent::CreateObject();
	if (Borders->AddComponent(TextureDropdown.Get()))
	{
		std::vector<SD::Texture*> importedTextures = localTexturePool->GetImportedResources();
		std::vector<SD::GuiDataElement<SD::Texture*>> textureList;
		for (UINT_TYPE i = 0; i < importedTextures.size(); i++)
		{
			textureList.push_back(SD::GuiDataElement<SD::Texture*>(importedTextures.at(i), importedTextures.at(i)->GetFriendlyName()));
		}
		TextureDropdown->GetExpandMenu()->SetList(textureList);
		TextureDropdown->SetSelectedItem(defaultSpriteTexture);
		TextureDropdown->SetSize(SD::Vector2(350, 256));
		TextureDropdown->SetPosition(SD::Vector2(Borders->ReadCachedAbsSize().X - TextureDropdown->ReadCachedAbsSize().X, 96));
		TextureDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, ShaderTester, HandleTextureSelected, void, SD::INT);
	}

	SpriteShaderDropdown = SD::DropdownComponent::CreateObject();
	if (Borders->AddComponent(SpriteShaderDropdown.Get()))
	{
		SpriteShaderDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, ShaderTester, HandleSpriteShaderSelected, void, SD::INT);
		SpriteShaderDropdown->GetExpandMenu()->SetList(shaderDropdownList);
		SpriteShaderDropdown->SetSize(SD::Vector2(350, 168));
		SpriteShaderDropdown->SetPosition(SD::Vector2(Borders->ReadCachedAbsSize().X - SpriteShaderDropdown->ReadCachedAbsSize().X, 24));
	}

	FontDropdown = SD::DropdownComponent::CreateObject();
	if (Borders->AddComponent(FontDropdown.Get()))
	{
		FontDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, ShaderTester, HandleFontSelected, void, SD::INT);

		std::vector<SD::Font*> importedFonts = SD::FontPool::FindFontPool()->GetImportedResources();
		std::vector<SD::GuiDataElement<SD::Font*>> fontList;
		for (UINT_TYPE i = 0; i < importedFonts.size(); i++)
		{
			fontList.push_back(SD::GuiDataElement<SD::Font*>(importedFonts.at(i), importedFonts.at(i)->GetFriendlyName()));
		}
		FontDropdown->GetExpandMenu()->SetList(fontList);
		FontDropdown->SetSize(SD::Vector2(350, 256));
		FontDropdown->SetPosition(SD::Vector2(Borders->ReadCachedAbsSize().X - FontDropdown->ReadCachedAbsSize().X, 240));
	}

	TextShaderDropdown = SD::DropdownComponent::CreateObject();
	if (Borders->AddComponent(TextShaderDropdown.Get()))
	{
		TextShaderDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, ShaderTester, HandleTextShaderSelected, void, SD::INT);
		TextShaderDropdown->GetExpandMenu()->SetList(shaderDropdownList);
		TextShaderDropdown->SetSize(SD::Vector2(350, 256));
		TextShaderDropdown->SetPosition(SD::Vector2(Borders->ReadCachedAbsPosition().X - TextShaderDropdown->ReadCachedAbsPosition().X, 312));
	}

	TextureDropdownLabel = SD::LabelComponent::CreateObject();
	if (Borders->AddComponent(TextureDropdownLabel.Get()))
	{
		TextureDropdownLabel->SetCharacterSize(16);
		TextureDropdownLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
		TextureDropdownLabel->SetWrapText(false);
		TextureDropdownLabel->SetText(TXT("Base Texture:"));
		TextureDropdownLabel->SetSize(SD::Vector2(TextureDropdown->ReadCachedAbsSize().X, 24));
		TextureDropdownLabel->SetPosition(TextureDropdown->ReadPosition() + SD::Vector2(0, -24));
	}

	SpriteOwner = SD::GuiComponent::CreateObject();
	if (Borders->AddComponent(SpriteOwner.Get()))
	{
		SpriteOwner->SetPosition(SD::Vector2(Borders->GetBorderThickness(), TextureDropdownLabel->ReadPosition().Y));
		SpriteOwner->SetSize(SD::Vector2(64, 64));

		SpriteDemo = SD::SpriteComponent::CreateObject();
		SpriteOwner->AddComponent(SpriteDemo.Get());
	}

	SpriteShaderDropdownLabel = SD::LabelComponent::CreateObject();
	if (Borders->AddComponent(SpriteShaderDropdownLabel.Get()))
	{
		SpriteShaderDropdownLabel->SetCharacterSize(16);
		SpriteShaderDropdownLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
		SpriteShaderDropdownLabel->SetWrapText(false);
		SpriteShaderDropdownLabel->SetText(TXT("Sprite Shader:"));
		SpriteShaderDropdownLabel->SetSize(SD::Vector2(SpriteShaderDropdown->ReadCachedAbsSize().X, 24));
		SpriteShaderDropdownLabel->SetPosition(SpriteShaderDropdown->ReadPosition() + SD::Vector2(0, -24));
	}

	FontDropdownLabel = SD::LabelComponent::CreateObject();
	if (Borders->AddComponent(FontDropdownLabel.Get()))
	{
		FontDropdownLabel->SetCharacterSize(16);
		FontDropdownLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
		FontDropdownLabel->SetWrapText(false);
		FontDropdownLabel->SetText(TXT("Font:"));
		FontDropdownLabel->SetSize(SD::Vector2(FontDropdown->ReadCachedAbsSize().X, 24));
		FontDropdownLabel->SetPosition(FontDropdown->ReadPosition() + SD::Vector2(0, -24));
	}

	TextDemo = SD::LabelComponent::CreateObject();
	if (Borders->AddComponent(TextDemo.Get()))
	{
		TextDemo->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
		TextDemo->SetWrapText(true);
		TextDemo->SetCharacterSize(24);
		TextDemo->SetText(PROJECT_NAME);
		TextDemo->SetPosition(SpriteOwner->ReadPosition() + SD::Vector2(0, FontDropdownLabel->ReadPosition().Y));
		TextDemo->SetSize(SD::Vector2(128, 64));
	}

	TextShaderDropdownLabel = SD::LabelComponent::CreateObject();
	if (Borders->AddComponent(TextShaderDropdownLabel.Get()))
	{
		TextShaderDropdownLabel->SetCharacterSize(16);
		TextShaderDropdownLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
		TextShaderDropdownLabel->SetWrapText(false);
		TextShaderDropdownLabel->SetText(TXT("Text Shader:"));
		TextShaderDropdownLabel->SetSize(SD::Vector2(TextShaderDropdown->ReadCachedAbsSize().X, 24));
		TextShaderDropdownLabel->SetPosition(TextShaderDropdown->ReadPosition() + SD::Vector2(0, -24));
	}

	CloseButton = SD::ButtonComponent::CreateObject();
	if (Borders->AddComponent(CloseButton.Get()))
	{
		CloseButton->SetCaptionText(TXT("Close"));
		CloseButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ShaderTester, HandleCloseButtonPressed, void, SD::ButtonComponent*));
		SD::Texture* textureStates = SD::GuiTheme::GetGuiTheme()->ButtonSingleSpriteBackground.Get();
		if (textureStates != nullptr && CloseButton->GetRenderComponent() != nullptr)
		{
			CloseButton->GetRenderComponent()->SetSpriteTexture(textureStates);
		}

		CloseButton->ReplaceStateComponent(SD::SingleSpriteButtonState::SStaticClass());
		CloseButton->SetSize(SD::Vector2(48, 24));
		CloseButton->SetPosition(SD::Vector2((Borders->ReadCachedAbsSize().X - (CloseButton->ReadCachedAbsSize().X/2)) / 2, 0.95f));
	}
}

void ShaderTester::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();
	CHECK(Focus != nullptr);

	Focus->TabOrder.push_back(TextureDropdown.Get());
	Focus->TabOrder.push_back(SpriteShaderDropdown.Get());
	Focus->TabOrder.push_back(FontDropdown.Get());
	Focus->TabOrder.push_back(TextShaderDropdown.Get());
}

void ShaderTester::Destroyed ()
{
	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated();
	}

	Super::Destroyed();
}

void ShaderTester::GetShaderList (std::vector<SD::DString>& outShaderList) const
{
	for (SD::FileIterator iter(SD::Shader::SHADER_DIRECTORY, SD::FileIterator::INCLUDE_FILES | SD::FileIterator::ITERATE_SUBDIRECTORIES); !iter.FinishedIterating(); iter++)
	{
		SD::PrimitiveFileAttributes attributes = iter.GetSelectedAttributes();
		outShaderList.push_back(attributes.GetName(false, true));
	}
}

void ShaderTester::HandleTextureSelected (SD::INT dropdownIdx)
{
	CHECK(SpriteDemo != nullptr)
	SpriteDemo->SetSpriteTexture(TextureDropdown->GetSelectedItem<SD::Texture*>());
}

void ShaderTester::HandleSpriteShaderSelected (SD::INT dropdownIdx)
{
	CHECK(SpriteDemo != nullptr)
	if (dropdownIdx == 0) //None
	{
		SpriteDemo->SetSpriteShader(nullptr);
		return;
	}

	SD::DString fullFileName = SpriteShaderDropdown->GetSelectedItem<SD::DString>();
	SD::DString fileName;
	SD::DString fileExtension;
	SD::FileUtils::ExtractFileExtension(fullFileName, fileName, fileExtension);

	sf::Shader::Type shaderType = (fileExtension.Compare(FRAGMENT_SHADER_EXTENSION, SD::DString::CC_IgnoreCase) == 0) ? sf::Shader::Type::Fragment : sf::Shader::Type::Vertex;
	SD::Shader* newShader = SD::Shader::CreateShader(fullFileName, shaderType);
	if (newShader == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to apply shader %s to sprite since it failed to create and import a shader instance."), fullFileName);
		return;
	}

	SpriteDemo->SetSpriteShader(newShader);
}

void ShaderTester::HandleFontSelected (SD::INT dropdownIdx)
{
	CHECK(TextDemo != nullptr)
	TextDemo->SetFont(FontDropdown->GetSelectedItem<SD::Font*>());
}

void ShaderTester::HandleTextShaderSelected (SD::INT dropdownIdx)
{
	CHECK(TextDemo != nullptr)
	if (dropdownIdx == 0) //None
	{
		TextDemo->GetRenderComponent()->SetTextShader(nullptr);
		return;
	}

	SD::DString fullFileName = TextShaderDropdown->GetSelectedItem<SD::DString>();
	SD::DString fileName;
	SD::DString fileExtension;
	SD::FileUtils::ExtractFileExtension(fullFileName, fileName, fileExtension);

	sf::Shader::Type shaderType = (fileExtension.Compare(FRAGMENT_SHADER_EXTENSION, SD::DString::CC_IgnoreCase) == 0) ? sf::Shader::Type::Fragment : sf::Shader::Type::Vertex;
	SD::Shader* newShader = SD::Shader::CreateShader(fullFileName, shaderType);
	if (newShader == nullptr)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to apply shader %s to text since it failed to create and import a shader instance."), fullFileName);
		return;
	}

	TextDemo->GetRenderComponent()->SetTextShader(newShader);
}

void ShaderTester::HandleCloseButtonPressed (SD::ButtonComponent* uiComponent)
{
	Destroy();
}
SD_TESTER_END

#endif