/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SandDuneTesterEngineComponent.cpp
=====================================================================
*/

#include "SandDuneTesterEngineComponent.h"
#include "SandDuneTesterTheme.h"

SD_TESTER_BEGIN

IMPLEMENT_ENGINE_COMPONENT(SDTester, SandDuneTesterEngineComponent)

bool SandDuneTesterEngineComponent::CanRunPreInitializeComponent (const std::vector<const SD::DClass*> executedEngineComponents) const
{
	if (!Super::CanRunPreInitializeComponent(executedEngineComponents))
	{
		return false;
	}

	for (size_t i = 0; i < executedEngineComponents.size(); ++i)
	{
		if (executedEngineComponents.at(i) == SD::GuiEngineComponent::SStaticClass())
		{
			return true;
		}
	}

	return false;
}

bool SandDuneTesterEngineComponent::CanRunInitializeComponent (const std::vector<const SD::DClass*> executedEngineComponents) const
{
	if (!Super::CanRunInitializeComponent(executedEngineComponents))
	{
		return false;
	}

	for (size_t i = 0; i < executedEngineComponents.size(); ++i)
	{
		if (executedEngineComponents.at(i) == SD::GuiEngineComponent::SStaticClass())
		{
			return true;
		}
	}

	return false;
}

void SandDuneTesterEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	SD::GuiEngineComponent* guiEngine = SD::GuiEngineComponent::Find();
	CHECK(guiEngine != nullptr)
	guiEngine->SetDefaultThemeClass(SandDuneTesterTheme::SStaticClass());
}

void SandDuneTesterEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_SD_TESTER, TICK_GROUP_PRIORITY_SD_TESTER);

	ImportAssets();
}

void SandDuneTesterEngineComponent::ImportAssets ()
{
	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("Ground") / TXT("Environmental") / TXT("Terrain"), TXT("GreenGrass.jpg")), TXT("Ground.Environmental.Terrain.GreenGrass"));
	localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("Ground") / TXT("Environmental") / TXT("Foliage"), TXT("TopDownFerns.png")), TXT("Ground.Environmental.Foliage.TopDownFerns"));
	localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("Ground") / TXT("Indoor"), TXT("Tile4.png")), TXT("Ground.Indoor.Tile4"));
}

SD_TESTER_END