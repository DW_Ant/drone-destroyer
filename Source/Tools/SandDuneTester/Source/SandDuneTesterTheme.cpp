/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SandDuneTesterTheme.cpp
=====================================================================
*/

#include "SandDuneTesterTheme.h"

SD_TESTER_BEGIN

IMPLEMENT_CLASS(SDTester, SandDuneTesterTheme, SD, GuiTheme)

const SD::DString SandDuneTesterTheme::SAND_DUNE_TESTER_STYLE_NAME = TXT("SandDuneTesterTheme");

void SandDuneTesterTheme::InitializeStyles ()
{
	Super::InitializeStyles();

	const SStyleInfo* defaultStyle = FindStyle(DEFAULT_STYLE_NAME);
	if (defaultStyle != nullptr)
	{
		SStyleInfo testerTheme = CreateStyleFrom(*defaultStyle);
		testerTheme.Name = SAND_DUNE_TESTER_STYLE_NAME;
		Styles.push_back(testerTheme);

		SD::ButtonComponent* buttonTemplate = dynamic_cast<SD::ButtonComponent*>(FindTemplate(&testerTheme, SD::ButtonComponent::SStaticClass()));
		CHECK(buttonTemplate != nullptr)

		SD::BorderedSpriteComponent* buttonSprite = buttonTemplate->GetRenderComponent();
		if (buttonSprite != nullptr)
		{
			buttonSprite->ClearSpriteTexture();
		}

		SD::ColorButtonState* colorState = dynamic_cast<SD::ColorButtonState*>(buttonTemplate->ReplaceStateComponent(SD::ColorButtonState::SStaticClass()));
		CHECK(colorState != nullptr)
		colorState->SetDefaultColor(SD::Color(128, 128, 128, 200));
		colorState->SetHoverColor(SD::Color(145, 145, 145, 200));
		colorState->SetDownColor(SD::Color(96, 96, 96, 200));
		colorState->SetDisabledColor(SD::Color(32, 32, 32, 200));
	}
}

SD_TESTER_END