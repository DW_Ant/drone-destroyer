/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PhysicsTester.cpp
=====================================================================
*/

#include "PhysicsTester.h"
#include "PhysicsTesterUi.h"

#ifdef DEBUG_MODE

SD_TESTER_BEGIN
IMPLEMENT_CLASS(SDTester, PhysicsTester, SDTester, WindowedEntityManager)

const SD::Vector2 PhysicsTester::WINDOW_SIZE(1024.f, 576.f);

void PhysicsTester::InitProps ()
{
	Super::InitProps();

	SceneLayer = nullptr;
	SceneCamera = nullptr;
	Ui = nullptr;
}

void PhysicsTester::Destroyed ()
{
	for (size_t i = 0; i < ActivePhysicsEntities.size(); ++i)
	{
		ActivePhysicsEntities.at(i)->Destroy();
	}
	SD::ContainerUtils::Empty(ActivePhysicsEntities);

	if (SceneLayer != nullptr)
	{
		SceneLayer->Destroy();
		SceneLayer = nullptr;
	}

	if (SceneCamera != nullptr)
	{
		SceneCamera->Destroy();
		SceneCamera = nullptr;
	}

	if (Ui != nullptr)
	{
		Ui->Destroy();
		Ui = nullptr;
	}

	if (OnTestTerminated.IsBounded())
	{
		OnTestTerminated();
	}

	Super::Destroyed();
}

void PhysicsTester::LaunchTest (SD::UnitTester::EUnitTestFlags testFlags)
{
	if (IsInitialized())
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to launch the PhysicsTester since the test has already launched."));
		return;
	}
	TestFlags = testFlags;

	InitializeEntityManager(WINDOW_SIZE, TXT("Physics Test"), 0.0166f /*~60 fps*/);

	SceneCamera = SD::TopDownCamera::CreateObject();
	SceneCamera->SetTranslation(SD::Vector3(-196.f, 0.f, 100.f)); //Slightly shift the camera since the UI is going to take up the left side of the window.
	SceneLayer = SD::SceneDrawLayer::CreateObject();
	SceneLayer->SetDrawPriority(GuiDrawLayer->GetDrawPriority() - 100); //Lower priority compared to the GuiDrawLayer so it draws behind it.
	WindowHandle->RegisterDrawLayer(SceneLayer, SceneCamera);

	Ui = PhysicsTesterUi::CreateObject();
	Ui->SetupInputComponent(Input, 1000);
	GuiDrawLayer->RegisterMenu(Ui);
	Ui->SetOwningTester(this);

	SD::UnitTester::TestLog(TestFlags, TXT("The PhysicsTester unit test has launched. It will terminate when the window closes."));
}

void PhysicsTester::LaunchNewPhysicsTest (const std::vector<Entity*>& newEntities)
{
	//Clear old test
	for (size_t i = 0; i < ActivePhysicsEntities.size(); ++i)
	{
		ActivePhysicsEntities.at(i)->Destroy();
	}
	SD::ContainerUtils::Empty(ActivePhysicsEntities);

	//Add new Entities to the ActivePhysicsEntities vector.
	ActivePhysicsEntities.insert(ActivePhysicsEntities.end(), newEntities.begin(), newEntities.end());

	//Register PhysicsComponents' callbacks
	for (size_t i = 0; i < ActivePhysicsEntities.size(); ++i)
	{
		SD::PhysicsComponent* physComp = dynamic_cast<SD::PhysicsComponent*>(ActivePhysicsEntities.at(i)->FindSubComponent(SD::PhysicsComponent::SStaticClass()));
		if (physComp != nullptr)
		{
			if (!physComp->OnBeginContact.IsBounded())
			{
				physComp->OnBeginContact = SDFUNCTION_2PARAM(this, PhysicsTester, HandleBeginContact, void, SD::PhysicsComponent*, b2Contact*);
			}

			if (!physComp->OnEndContact.IsBounded())
			{
				physComp->OnEndContact = SDFUNCTION_2PARAM(this, PhysicsTester, HandleEndContact, void, SD::PhysicsComponent*, b2Contact*);
			}

			if (!physComp->OnCollision.IsBounded())
			{
				physComp->OnCollision = SDFUNCTION_3PARAM(this, PhysicsTester, HandleCollision, void, SD::PhysicsComponent*, b2Contact*, const b2ContactImpulse*);
			}
		}
	}
}

void PhysicsTester::RefreshRenderCompColors (SD::PhysicsComponent* physComp, bool incrementBrightness) const
{
	bool foundSensor = false;
	for (const b2Fixture* fixture = physComp->GetPhysicsBody()->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		if (fixture->IsSensor())
		{
			foundSensor = true;
			break;
		}
	}

	if (!foundSensor)
	{
		//Only apply if this component is a sensor (ignore blocking components)
		return;
	}

	Entity* owner = physComp->GetOwner();
	if (owner == nullptr)
	{
		return;
	}

	SD::PhysicsRenderer* renderer = dynamic_cast<SD::PhysicsRenderer*>(owner->FindSubComponent(SD::PhysicsRenderer::SStaticClass(), true));
	if (renderer == nullptr)
	{
		//Nothing to do here if there's no render component
		return;
	}

	sf::Uint8 colorIncrement = 32;
	//Not protecting against number overload to hint there's an error with the sensor counter.
	if (incrementBrightness)
	{
		renderer->FillColor.Source.r += colorIncrement;	
	}
	else
	{
		renderer->FillColor.Source.r -= colorIncrement;	
	}
}

void PhysicsTester::HandleBeginContact (SD::PhysicsComponent* otherComp, b2Contact* contactData)
{
	RefreshRenderCompColors(otherComp, false);
}

void PhysicsTester::HandleEndContact (SD::PhysicsComponent* otherComp, b2Contact* contactData)
{
	RefreshRenderCompColors(otherComp, true);
}

void PhysicsTester::HandleCollision (SD::PhysicsComponent* otherComp, b2Contact* contactData, const b2ContactImpulse* impulse)
{
	Entity* owner = otherComp->GetOwner();
	CHECK(owner != nullptr)

	const sf::Uint8 minAlpha = 64;
	const sf::Uint8 alphaLossPerHit = 32;

	SD::PhysicsRenderer* renderer = dynamic_cast<SD::PhysicsRenderer*>(owner->FindSubComponent(SD::PhysicsRenderer::SStaticClass(), false));
	if (renderer != nullptr && renderer->FillColor.Source.a > minAlpha)
	{
		//Darken the color
		renderer->FillColor.Source.a -= alphaLossPerHit;
	}
}
SD_TESTER_END

#endif