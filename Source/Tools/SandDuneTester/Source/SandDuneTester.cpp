/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SandDuneTester.cpp

  Defines the application's entry point.  Launches the engine and its loop.
=====================================================================
*/

#ifdef _WIN32
#include "stdafx.h"
#endif

#include "SandDuneTester.h"
#include "SandDuneTesterEngineComponent.h"
#include "CppUnitTest.h"

SD_TESTER_BEGIN
SD::LogCategory SdTesterLog(TXT("Sand Dune Tester"), SD::LogCategory::VERBOSITY_DEFAULT,
	SD::LogCategory::FLAG_LOG_FILE |
	SD::LogCategory::FLAG_OS_OUTPUT |
	SD::LogCategory::FLAG_OUTPUT_WINDOW |
	SD::LogCategory::FLAG_STANDARD_OUTPUT);
SD_TESTER_END

void GetEngineComponentList (std::vector<SD::EngineComponent*>& outEngineComponents)
{
	outEngineComponents.push_back(new SD::CommandEngineComponent());
	outEngineComponents.push_back(new SD::GraphicsEngineComponent());
	outEngineComponents.push_back(new SD::LoggerEngineComponent());
	outEngineComponents.push_back(new SD::ConsoleEngineComponent());
	outEngineComponents.push_back(new SD::LocalizationEngineComponent());
	outEngineComponents.push_back(new SD::MultiThreadEngineComponent());
	outEngineComponents.push_back(new SD::InputEngineComponent());
	outEngineComponents.push_back(new SD::GuiEngineComponent());
	outEngineComponents.push_back(new SD::RandomEngineComponent());
	outEngineComponents.push_back(new SDTester::SandDuneTesterEngineComponent());
	outEngineComponents.push_back(new SD::TimerEngineComponent());
	outEngineComponents.push_back(new SD::WorldEngineComponent());
}

//Forward function declarations
int BeginSandDune (int numArgs, TCHAR* args[]);
SD::DString ConvertArgsToStr (int numArgs, TCHAR* args[]);
int RunMainLoop ();

/**
 * Entry points based on operating system.
 */
#ifdef PLATFORM_WINDOWS
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int numArgs;
	//Only using this to get numArgs because of reasons
	LPWSTR *notUsed = CommandLineToArgvW(GetCommandLineW(), &numArgs);
	LocalFree(notUsed);

	TCHAR* args = GetCommandLine();
	return BeginSandDune(numArgs, &args);
}
#else //Not PLATFORM_WINDOWS
int main (int numArgs, char* args[])
{
	return BeginSandDune(numArgs, args);
}
#endif

/**
 * Entry point of the engine
 */
int BeginSandDune (int numArg, TCHAR* args[])
{
	//Ensure the current platform can perform primitive operations
	CppUnitTestLauncher::LaunchCppUnitTests();

	SD::Engine* engine = new SD::Engine();
	if (engine == nullptr)
	{
		std::cout << "Failed to instantiate Sand Dune Engine!\n";
		throw std::exception("Failed to instantiate Sand Dune Engine!");
		return -1;
	}

	engine->SetCommandLineArgs(ConvertArgsToStr(numArg, args));

	if (!SD::DClassAssembler::AssembleDClasses())
	{
		std::string errorMsg = "Failed to initialize Sand Dune.  The DClassAssembler could not link DClasses.";
		std::cout << errorMsg << "\n";
		throw std::exception(errorMsg.c_str());
		return -1;
	}

	//Generate a list of engine components for the main engine.  The Engine will destroy these components on shutdown.
	std::vector<SD::EngineComponent*> engineComponents;
	GetEngineComponentList(OUT engineComponents);

	//Disable the max frame rate
	engine->SetMinDeltaTime(-1.f);

	//Set at least a min frame rate to prevent time sensitive tests from failing after hitting a debug break point.
	engine->SetMaxDeltaTime(1.f);

	engine->InitializeEngine(SD::Engine::MAIN_ENGINE_IDX, engineComponents); //Kickoff the Main Engine

#ifdef DEBUG_MODE
	SD::CoreLog.Log(SD::LogCategory::LL_Debug, TXT("Running %s in Debug mode."), SD::DString(PROJECT_NAME));

	SD::UnitTester::EUnitTestFlags testFlags;
	testFlags = SD::UnitTester::EUnitTestFlags::UTF_SmokeTest |
					SD::UnitTester::EUnitTestFlags::UTF_FeatureTest |
					SD::UnitTester::EUnitTestFlags::UTF_StressTest |
					SD::UnitTester::EUnitTestFlags::UTF_Automatic |
					SD::UnitTester::EUnitTestFlags::UTF_Manual |
					SD::UnitTester::EUnitTestFlags::UTF_CanDetectErrors |
					SD::UnitTester::EUnitTestFlags::UTF_NeverFails |
					SD::UnitTester::EUnitTestFlags::UTF_Synchronous |
					SD::UnitTester::EUnitTestFlags::UTF_Asynchronous |
					//SD::UnitTester::EUnitTestFlags::UTF_ErrorsOnly |
					SD::UnitTester::EUnitTestFlags::UTF_Verbose |
					SD::UnitTester::EUnitTestFlags::UTF_CrashOnError;

	if (!SD::UnitTestLauncher::RunAllTests(testFlags))
	{
		SD::Engine::GetEngine(SD::Engine::MAIN_ENGINE_IDX)->FatalError(TXT("One of the unit tests failed.  See the logs for details."));
	}
#endif

	return RunMainLoop();
}

/**
 * Converts the given args into a single string variable.
 */
SD::DString ConvertArgsToStr (int numArgs, TCHAR* args[])
{
	SD::DString allArgs = SD::DString::EmptyString;

	for (int i = 0; i < numArgs; ++i)
	{
		allArgs += args[i];
	}

	return allArgs;
}

int RunMainLoop ()
{
	SD::Engine* sandDuneEngine = SD::Engine::GetEngine(SD::Engine::MAIN_ENGINE_IDX);
	while (sandDuneEngine != nullptr)
	{
		sandDuneEngine->Tick();

		if (sandDuneEngine->IsShuttingDown())
		{
			//At the end of Tick, the Engine should have cleaned up its resources.  It's safe to delete Engine here.
			delete sandDuneEngine;
			sandDuneEngine = nullptr;
			break;
		}
	}

	return 0;
}