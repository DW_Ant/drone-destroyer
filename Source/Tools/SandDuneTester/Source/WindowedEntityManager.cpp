/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  WindowedEntityManager.cpp
=====================================================================
*/

#include "SandDuneTester.h"
#include "WindowedEntityManager.h"

#ifdef DEBUG_MODE

SD_TESTER_BEGIN
IMPLEMENT_CLASS(SDTester, WindowedEntityManager, SD, Entity)

void WindowedEntityManager::InitProps ()
{
	Super::InitProps();

	WindowHandle = nullptr;
	GuiDrawLayer = nullptr;
	MainDrawLayer = nullptr;
	Mouse = nullptr;
	Input = nullptr;

	isInitialized;
}

void WindowedEntityManager::Destroyed ()
{
	if (Mouse != nullptr)
	{
		Mouse->Destroy();
		Mouse = nullptr;
	}

	if (Input != nullptr)
	{
		Input->Destroy();
		Input = nullptr;
	}

	if (GuiDrawLayer != nullptr)
	{
		GuiDrawLayer->Destroy();
		GuiDrawLayer = nullptr;
	}

	if (MainDrawLayer != nullptr)
	{
		MainDrawLayer->Destroy();
		MainDrawLayer = nullptr;
	}

	if (WindowHandle != nullptr)
	{
		WindowHandle->Destroy();
		WindowHandle = nullptr;
	}

	Super::Destroyed();
}

void WindowedEntityManager::InitializeEntityManager (const SD::Vector2& windowSize, const SD::DString& windowName, SD::FLOAT windowUpdateRate)
{
	if (isInitialized)
	{
		SdTesterLog.Log(SD::LogCategory::LL_Warning, TXT("%s is already initialized."), ToString());
		return;
	}
	isInitialized = true;

	WindowHandle = SD::Window::CreateObject();
	WindowHandle->SetResource(new sf::RenderWindow(sf::VideoMode(SD::INT(windowSize.X).ToUnsignedInt32(), SD::INT(windowSize.Y).ToUnsignedInt32()), windowName.ReadString()));
	WindowHandle->GetTick()->SetTickInterval(windowUpdateRate);
	WindowHandle->GetPollingTickComponent()->SetTickInterval(windowUpdateRate);
	WindowHandle->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, WindowedEntityManager, HandleWindowEvent, void, const sf::Event&));
	WindowHandle->DefaultColor = SD::Color(0, 0, 0);
	WindowHandle->bClearsHandleOnDelete = true;
	WindowHandle->SetSize(windowSize);

	MainDrawLayer = SD::PlanarDrawLayer::CreateObject();
	MainDrawLayer->SetDrawPriority(100);
	WindowHandle->RegisterDrawLayer(MainDrawLayer);

	GuiDrawLayer = SD::GuiDrawLayer::CreateObject();
	GuiDrawLayer->SetDrawPriority(1000);
	WindowHandle->RegisterDrawLayer(GuiDrawLayer);

	Input = SD::InputBroadcaster::CreateObject();
	Mouse = SD::MousePointer::CreateObject();
	SD::InputUtils::SetupInputForWindow(WindowHandle, Input, Mouse);
	GuiDrawLayer->RegisterSingleComponent(Mouse->GetMouseIcon());
}

void WindowedEntityManager::HandleWindowEvent (const sf::Event& windowEvent)
{
	if (windowEvent.type == sf::Event::Closed)
	{
		//Window is already getting destroyed. Clear pointer to prevent destroying it twice.
		WindowHandle = nullptr;
		Destroy();
	}
}
SD_TESTER_END
#endif