/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Player.cpp
=====================================================================
*/

#include "Player.h"

#ifdef DEBUG_MODE

SD_TESTER_BEGIN
IMPLEMENT_CLASS(SDTester, Player, SD, Entity)

void Player::InitProps ()
{
	Super::InitProps();

	PivotPoint = SD::Vector3(0.5f, 0.5f, 0.5f); //Move the origin to the center.

	VelocityInput = SD::Vector3::ZeroVector;
	GrowthInput = SD::Vector3::ZeroVector;

	RenderComp = nullptr;
	InputComp = nullptr;
	Tick = nullptr;

	MoveSpeed = 500.f; //5 meters a second
	GrowSpeed = 0.5f;

	InputLeft = false;
	InputUp = false;
	InputRight = false;
	InputDown = false;
	InputGrow = false;
	InputShrink = false;
}

void Player::BeginObject ()
{
	Super::BeginObject();

	RenderComp = SD::SolidColorRenderComponent::CreateObject();
	if (AddComponent(RenderComp.Get()))
	{
		RenderComp->SolidColor = SD::Color(255, 128, 64);
		RenderComp->Shape = SD::SolidColorRenderComponent::ST_Circle;
		RenderComp->BaseSize = 100.f; //1 meter wide. Increase the player size without affecting Entity positions relative to this Entity.
	}

	InputComp = SD::InputComponent::CreateObject();
	if (AddComponent(InputComp.Get()))
	{
		InputComp->SetInputEnabled(true);
		InputComp->CaptureInputDelegate = SDFUNCTION_1PARAM(this, Player, HandleKeyboardInput, bool, const sf::Event&);
	}

	Tick = SD::TickComponent::CreateObject(TICK_GROUP_SD_TESTER);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, Player, HandleTick, void, SD::FLOAT));
	}
}

bool Player::HandleKeyboardInput (const sf::Event& evnt)
{
	if (evnt.type != sf::Event::KeyPressed && evnt.type  != sf::Event::KeyReleased)
	{
		return false;
	}

	bool newInputFlag = (evnt.type == sf::Event::KeyPressed); //Move on true (pressed). Stop on false (released).
	bool consumeEvent = false;
	switch (evnt.key.code)
	{
		case(sf::Keyboard::Up):
		case(sf::Keyboard::W):
			InputUp = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::Right):
		case(sf::Keyboard::D):
			InputRight = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::Down):
		case(sf::Keyboard::S):
			InputDown = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::Left):
		case(sf::Keyboard::A):
			InputLeft = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::Z):
			InputGrow = newInputFlag;
			consumeEvent = true;
			break;

		case(sf::Keyboard::X):
			InputShrink = newInputFlag;
			consumeEvent = true;
			break;
	}

	VelocityInput = SD::Vector3::ZeroVector;
	if (InputUp)
	{
		VelocityInput.Y -= 1.f;
	}

	if (InputRight)
	{
		VelocityInput.X += 1.f;
	}

	if (InputDown)
	{
		VelocityInput.Y += 1.f;
	}

	if (InputLeft)
	{
		VelocityInput.X -= 1.f;
	}

	GrowthInput = SD::Vector3::ZeroVector;
	if (InputGrow)
	{
		GrowthInput += SD::Vector3(1.f, 1.f, 1.f);
	}

	if (InputShrink)
	{
		GrowthInput -= SD::Vector3(1.f, 1.f, 1.f);
	}

	return consumeEvent;
}

void Player::HandleTick (SD::FLOAT deltaSec)
{
	if (!VelocityInput.IsEmpty())
	{
		SD::Vector3 newPos = ReadTranslation() + (VelocityInput * deltaSec * MoveSpeed);
		SetTranslation(newPos);
	}

	if (!GrowthInput.IsEmpty())
	{
		SD::Vector3 newSize = ReadScale() + (GrowthInput * deltaSec * GrowSpeed);
		SetScale(newSize);
	}
}
SD_TESTER_END

#endif