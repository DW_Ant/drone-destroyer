/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PhysicsTesterTeleporter.cpp
=====================================================================
*/

#include "PhysicsTesterTeleporter.h"

#ifdef DEBUG_MODE

SD_TESTER_BEGIN
IMPLEMENT_CLASS(SDTester, PhysicsTesterTeleporter, SD, SceneEntity)

void PhysicsTesterTeleporter::InitProps ()
{
	Super::InitProps();

	PreserveXCoordinates = false;
	PreserveYCoordinates = false;
	TeleportDestination = SD::Vector3::ZeroVector;

	PhysComp = nullptr;
}

void PhysicsTesterTeleporter::BeginObject ()
{
	Super::BeginObject();

	PhysComp = SD::PhysicsComponent::CreateObject();
	if (AddComponent(PhysComp))
	{
		PhysComp->OnBeginContact = SDFUNCTION_2PARAM(this, PhysicsTesterTeleporter, HandleBeginContact, void, SD::PhysicsComponent*, b2Contact*);
	}
	else
	{
		PhysComp = nullptr;
	}
}

void PhysicsTesterTeleporter::HandleBeginContact (SD::PhysicsComponent* otherComp, b2Contact* contactData)
{
	SD::SceneTransform* transform = dynamic_cast<SD::SceneTransform*>(otherComp->GetOwner());
	CHECK(transform != nullptr) //PhysicsComponents should always be attached to a SceneTransform.

	SD::Vector3 destination(TeleportDestination);
	if (PreserveXCoordinates)
	{
		destination.X = transform->ReadTranslation().X;
	}

	if (PreserveYCoordinates)
	{
		destination.Y = transform->ReadTranslation().Y;
	}

	transform->SetTranslation(destination);
	otherComp->OverwritePosition = true;
}

SD_TESTER_END

#endif