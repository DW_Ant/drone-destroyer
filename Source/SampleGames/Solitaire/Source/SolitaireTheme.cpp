/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolitaireTheme.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
IMPLEMENT_CLASS(Solitaire, SolitaireTheme, SD, GuiTheme)

const DString SolitaireTheme::SOLITAIRE_STYLE_NAME = TXT("SolitaireTheme");

void SolitaireTheme::InitializeStyles ()
{
	Super::InitializeStyles();

	const SStyleInfo* defaultStyle = FindStyle(DEFAULT_STYLE_NAME);
	if (defaultStyle != nullptr)
	{
		SStyleInfo solitaireStyle = CreateStyleFrom(*defaultStyle);
		solitaireStyle.Name = SOLITAIRE_STYLE_NAME;
		Styles.push_back(solitaireStyle);

		ButtonComponent* buttonTemplate = dynamic_cast<ButtonComponent*>(FindTemplate(&solitaireStyle, ButtonComponent::SStaticClass()));
		CHECK(buttonTemplate != nullptr)

		BorderedSpriteComponent* buttonSprite = buttonTemplate->GetRenderComponent();
		if (buttonSprite != nullptr)
		{
			buttonSprite->ClearSpriteTexture();
		}

		LabelComponent* buttonLabel = buttonTemplate->CaptionComponent.Get();
		if (buttonLabel != nullptr)
		{
			buttonLabel->SetCharacterSize(24);
		}

		ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(buttonTemplate->ReplaceStateComponent(ColorButtonState::SStaticClass()));
		CHECK(colorState != nullptr)
		colorState->SetDefaultColor(Color(128, 128, 128, 200));
		colorState->SetHoverColor(Color(145, 145, 145, 200));
		colorState->SetDownColor(Color(96, 96, 96, 200));
		colorState->SetDisabledColor(Color(32, 32, 32, 200));
	}
}
SOLITAIRE_END