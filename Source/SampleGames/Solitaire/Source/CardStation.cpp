/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CardStation.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
IMPLEMENT_CLASS(Solitaire, CardStation, SD, Entity)

const INT CardStation::STATION_INPUT_PRIORITY = 10;

void CardStation::InitProps ()
{
	Super::InitProps();

	SetSize(Card::GetCardSize());

	StationOutline = nullptr;
	Input = nullptr;
}

void CardStation::BeginObject ()
{
	Super::BeginObject();

	StationOutline = SpriteComponent::CreateObject();
	if (AddComponent(StationOutline.Get()))
	{
		TexturePool* localTexturePool = TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)
		StationOutline->SetSpriteTexture(localTexturePool->FindTexture(TXT("SampleGames.Solitaire.CardOutline")));
	}

	Input = InputComponent::CreateObject();
	if (AddComponent(Input.Get()))
	{
		InputBroadcaster* mainBroadcaster = InputEngineComponent::Find()->GetMainBroadcaster();
		CHECK(mainBroadcaster != nullptr)

		mainBroadcaster->AddInputComponent(Input.Get(), STATION_INPUT_PRIORITY);
		Input->MouseClickDelegate = SDFUNCTION_3PARAM(this, CardStation, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
	}

	SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
	if (solitaireEngine == nullptr)
	{
		SolitaireLog.Log(LogCategory::LL_Warning, TXT("Cannot register CardStation to a SolitaireDrawLayer since it could not find a SolitaireEngineComponent in curren thread."));
		return;
	}

	SolitaireDrawLayer* drawLayer = solitaireEngine->GetCardLayer();
	if (drawLayer == nullptr)
	{
		SolitaireLog.Log(LogCategory::LL_Warning, TXT("Cannot register CardStation to a SolitaireDrawLayer since the local Solitaire Engine does not have a SolitaireDrawLayer."));
		return;
	}

	drawLayer->RegisterCardStation(this);
}

void CardStation::ReturnCards (CardColumn* columnToReturn)
{
	AddColumn(columnToReturn);
}

void CardStation::AddCard (Card* newCard)
{
	CardStack.push_back(newCard);
	newCard->SetRelativeTo(this);
	newCard->SetPositionNear(Vector2::ZeroVector); //shift card a bit

	if (OnCardAdded.IsBounded())
	{
		OnCardAdded(newCard);
	}
}

void CardStation::AddColumn (CardColumn* columnToAdd)
{
	for (INT i = INT(columnToAdd->ReadCardList().size() - 1); i >= 0; --i)
	{
		AddCard(columnToAdd->ReadCardList().at(i.ToUnsignedInt()));
	}

	columnToAdd->Destroy();
}

void CardStation::RemoveTopCard ()
{
	if (ContainerUtils::IsEmpty(CardStack))
	{
		SolitaireLog.Log(LogCategory::LL_Warning, TXT("Attempting to remove a card from an empty stack."));
		return;
	}

	CardStack.erase(CardStack.begin() + CardStack.size() - 1);
}

void CardStation::RemoveAllCards ()
{
	ContainerUtils::Empty(CardStack);
}

bool CardStation::CanAddCard (Card* newCard) const
{
	return false;
}

bool CardStation::CanAddColumn (CardColumn* newColumn) const
{
	return false;
}

void CardStation::RenderCardsInStation (RenderTarget* renderTarget, Camera* camera)
{
	ComputeAbsTransform();
	MarkProjectionDataDirty();

	if (ContainerUtils::IsEmpty(CardStack))
	{
		//Render the outline
		if (StationOutline != nullptr && StationOutline->IsVisible())
		{
			StationOutline->Render(renderTarget, camera);
		}
	}
	else
	{
		//Although we can get away with just rendering the top card, still render all cards underneathe due to rotation differences
		for (UINT_TYPE i = 0; i < CardStack.size(); ++i)
		{
			CardStack.at(i)->ComputeAbsTransform();

			SpriteComponent* cardSprite = CardStack.at(i)->GetSprite();
			if (cardSprite != nullptr && cardSprite->IsVisible())
			{
				cardSprite->Render(renderTarget, camera);
			}
		}
	}
}

Card* CardStation::GetTopCard () const
{
	if (ContainerUtils::IsEmpty(CardStack))
	{
		return nullptr;
	}

	return CardStack.at(CardStack.size() - 1);
}

bool CardStation::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& buttonEvent, sf::Event::EventType eventType)
{
	if (buttonEvent.button != sf::Mouse::Left)
	{
		return false;
	}

	if (!IsWithinBounds(Vector2(FLOAT::MakeFloat(buttonEvent.x), FLOAT::MakeFloat(buttonEvent.y))))
	{
		return false;
	}

	if (ContainerUtils::IsEmpty(CardStack))
	{
		if (OnEmptyClicked.IsBounded())
		{
			OnEmptyClicked();
		}
		return true;
	}

	Card* topCard = GetTopCard();
	CHECK(topCard != nullptr)

	if (topCard->IsFacingUp())
	{
		if (eventType == sf::Event::MouseButtonPressed)
		{
			Vector2 clickOffset(ReadCachedAbsPosition() - mouse->GetPosition());
			CardColumn* floatingColumn = CardColumn::CreateObject();
			floatingColumn->SetDragging(true, this);
			floatingColumn->SetPosition(clickOffset);
			floatingColumn->SetRelativeTo(mouse);
			floatingColumn->SetCardList({GetTopCard()});
			floatingColumn->GetInput()->SetInputPriority(CardColumn::COLUMN_INPUT_PRIORITY + 5);
			RemoveTopCard();
		}

		return true;
	}
	else
	{
		if (eventType == sf::Event::MouseButtonReleased)
		{
			//Reveal top card
			topCard->SetFacingUp(true);
			if (OnCardReveal.IsBounded())
			{
				OnCardReveal(this, topCard);
			}
		}

		return true;
	}
}
SOLITAIRE_END