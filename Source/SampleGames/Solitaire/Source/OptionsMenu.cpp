/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  OptionsMenu.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
IMPLEMENT_CLASS(Solitaire, OptionsMenu, SD, GuiEntity)

void OptionsMenu::InitProps ()
{
	Super::InitProps();

	MaxAngleRange = Range<FLOAT>(0.f, 15.f);
	MaxShiftRange = Range<FLOAT>(0.f, 5.f);
	IncrementStep = 0.5f;

	OriginalMaxAngle = Card::GetMaxAngle();
	OriginalMaxShift = Card::GetMaxShift();

	FadeOut = nullptr;
	Background = nullptr;
	Title = nullptr;
	MaxAngleIncrement = nullptr;
	MaxAngleDecrement = nullptr;
	MaxShiftIncrement = nullptr;
	MaxShiftDecrement = nullptr;
	MaxAngleText = nullptr;
	MaxShiftText = nullptr;
	AcceptButton = nullptr;
	CancelButton = nullptr;
}

void OptionsMenu::ConstructUI ()
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	FadeOut = FrameComponent::CreateObject();
	if (AddComponent(FadeOut.Get()))
	{
		FadeOut->SetPosition(Vector2::ZeroVector);
		FadeOut->SetSize(Vector2(1.f, 1.f));
		FadeOut->SetLockedFrame(true);
		FadeOut->ConsumesInput = true;
		BorderedSpriteComponent* fadeSprite = FadeOut->RenderComponent.Get();
		if (fadeSprite != nullptr)
		{
			fadeSprite->ClearSpriteTexture();
			fadeSprite->FillColor = Color(0, 0, 0, 64); //darken
			fadeSprite->TopBorder = nullptr;
			fadeSprite->RightBorder = nullptr;
			fadeSprite->BottomBorder = nullptr;
			fadeSprite->LeftBorder = nullptr;
			fadeSprite->TopRightCorner = nullptr;
			fadeSprite->BottomRightCorner = nullptr;
			fadeSprite->BottomLeftCorner = nullptr;
			fadeSprite->TopLeftCorner = nullptr;
		}

		Background = FrameComponent::CreateObject();
		if (FadeOut->AddComponent(Background.Get()))
		{
			Background->SetPosition(Vector2(0.275f, 0.165f));
			Background->SetSize(Vector2(0.45f, 0.67f));
			Background->SetMinDragSize(Vector2(200.f, 400.f));
			Background->SetMaxDragSize(Vector2(600.f, 550.f));
			Background->SetLockedFrame(false);
			Background->SetBorderThickness(5.f);
			BorderedSpriteComponent* backgroundSprite = Background->RenderComponent.Get();
			if (backgroundSprite != nullptr)
			{
				backgroundSprite->ClearSpriteTexture();
				backgroundSprite->FillColor = Color(96, 128, 96);
			}

			FLOAT yPos = 0.f;
			Title = LabelComponent::CreateObject();
			if (Background->AddComponent(Title.Get()))
			{
				Title->SetAutoRefresh(false);
				Title->SetPosition(Vector2(0.05f, 0.05f));
				Title->SetSize(Vector2(0.9f, 0.1f));
				yPos += Title->ReadPosition().Y + Title->ReadSize().Y;
				Title->SetVerticalAlignment(LabelComponent::VA_Center);
				Title->SetHorizontalAlignment(LabelComponent::HA_Center);
				Title->SetCharacterSize(32);
				Title->SetText(translator->TranslateText(TXT("Title"), TXT("Solitaire"), TXT("OptionsMenu")));
				Title->SetAutoRefresh(true);
			}

			const FLOAT incrementButtonWidth = 0.1f;
			const INT incrementCharSize = 14;
			yPos += 0.1f;

			MaxAngleText = LabelComponent::CreateObject();
			if (Background->AddComponent(MaxAngleText.Get()))
			{
				MaxAngleText->SetAutoRefresh(false);
				MaxAngleText->SetPosition(Vector2(0.05f, 0.05f + yPos));
				MaxAngleText->SetSize(Vector2(0.7f, 0.15f));
				MaxAngleText->SetHorizontalAlignment(LabelComponent::HA_Left);
				RefreshMaxAngleText();
				MaxAngleText->SetClampText(true);
				MaxAngleText->SetAutoRefresh(true);
			}

			MaxAngleIncrement = ButtonComponent::CreateObject();
			if (Background->AddComponent(MaxAngleIncrement.Get()))
			{
				MaxAngleIncrement->SetPosition(Vector2(0.8f, 0.05f + yPos));
				MaxAngleIncrement->SetSize(Vector2(incrementButtonWidth, 0.05f));
				MaxAngleIncrement->SetCaptionText(TXT("+"));
				MaxAngleIncrement->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, OptionsMenu, HandleMaxAngleIncrement, void, ButtonComponent*));

				if (MaxAngleIncrement->CaptionComponent != nullptr)
				{
					MaxAngleIncrement->CaptionComponent->SetCharacterSize(incrementCharSize);
				}
			}

			MaxAngleDecrement = ButtonComponent::CreateObject();
			if (Background->AddComponent(MaxAngleDecrement.Get()))
			{
				MaxAngleDecrement->SetPosition(Vector2(0.8f, 0.105f + yPos));
				MaxAngleDecrement->SetSize(Vector2(incrementButtonWidth, 0.05f));
				MaxAngleDecrement->SetCaptionText(TXT("-"));
				MaxAngleDecrement->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, OptionsMenu, HandleMaxAngleDecrement, void, ButtonComponent*));

				if (MaxAngleDecrement->CaptionComponent != nullptr)
				{
					MaxAngleDecrement->CaptionComponent->SetCharacterSize(incrementCharSize);
				}
			}

			yPos += 0.2f;
			MaxShiftText = LabelComponent::CreateObject();
			if (Background->AddComponent(MaxShiftText.Get()))
			{
				MaxShiftText->SetAutoRefresh(false);
				MaxShiftText->SetPosition(Vector2(0.05f, 0.1f + yPos));
				MaxShiftText->SetSize(Vector2(0.7f, 0.25f));
				MaxShiftText->SetHorizontalAlignment(LabelComponent::HA_Left);
				RefreshMaxShiftText();
				MaxShiftText->SetClampText(true);
				MaxShiftText->SetAutoRefresh(true);
			}

			MaxShiftIncrement = ButtonComponent::CreateObject();
			if (Background->AddComponent(MaxShiftIncrement.Get()))
			{
				MaxShiftIncrement->SetPosition(Vector2(0.8f, 0.1f + yPos));
				MaxShiftIncrement->SetSize(Vector2(incrementButtonWidth, 0.05f));
				MaxShiftIncrement->SetCaptionText(TXT("+"));
				MaxShiftIncrement->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, OptionsMenu, HandleMaxShiftIncrement, void, ButtonComponent*));

				if (MaxShiftIncrement->CaptionComponent != nullptr)
				{
					MaxShiftIncrement->CaptionComponent->SetCharacterSize(incrementCharSize);
				}
			}

			MaxShiftDecrement = ButtonComponent::CreateObject();
			if (Background->AddComponent(MaxShiftDecrement.Get()))
			{
				MaxShiftDecrement->SetPosition(Vector2(0.8f, 0.155f + yPos));
				MaxShiftDecrement->SetSize(Vector2(incrementButtonWidth, 0.05f));
				MaxShiftDecrement->SetCaptionText(TXT("-"));
				MaxShiftDecrement->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, OptionsMenu, HandleMaxShiftDecrement, void, ButtonComponent*));

				if (MaxShiftDecrement->CaptionComponent != nullptr)
				{
					MaxShiftDecrement->CaptionComponent->SetCharacterSize(incrementCharSize);
				}
			}

			AcceptButton = ButtonComponent::CreateObject();
			if (Background->AddComponent(AcceptButton.Get()))
			{
				AcceptButton->SetPosition(Vector2(0.05f, 0.85f));
				AcceptButton->SetSize(Vector2(0.4f, 0.1f));
				AcceptButton->SetCaptionText(translator->TranslateText(TXT("Accept"), TXT("Solitaire"), TXT("OptionsMenu")));
				AcceptButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, OptionsMenu, HandleAcceptClicked, void, ButtonComponent*));
			}

			CancelButton = ButtonComponent::CreateObject();
			if (Background->AddComponent(CancelButton.Get()))
			{
				CancelButton->SetPosition(Vector2(0.55f, 0.85f));
				CancelButton->SetSize(Vector2(0.4f, 0.1f));
				CancelButton->SetCaptionText(translator->TranslateText(TXT("Cancel"), TXT("Solitaire"), TXT("OptionsMenu")));
				CancelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, OptionsMenu, HandleCancelClicked, void, ButtonComponent*));
			}
		}
	}
}

DString OptionsMenu::FormatFloat (FLOAT value)
{
	DString result = value.ToString();

	//Trim trailing zeroes
	std::string& str = result.EditString();
	str.erase(str.find_last_not_of('0') + 1, std::string::npos);
	result.MarkNumCharactersDirty();

	if (result.At(result.Length() - 1) == '.')
	{
		//Ensure there's at least one decimal.
		result += TXT("0");
	}

	return result;
}

void OptionsMenu::RefreshMaxAngleText ()
{
	CHECK(MaxAngleText != nullptr)

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	const DString translationKey = (Card::GetMaxAngle() == 1.f) ? TXT("MaxRotationSingular") : TXT("MaxRotationPlural");
	DString text = translator->TranslateText(translationKey, TXT("Solitaire"), TXT("OptionsMenu"));
	DString::FormatString(text, FormatFloat(Card::GetMaxAngle()));

	MaxAngleText->SetText(text);
}

void OptionsMenu::RefreshMaxShiftText ()
{
	CHECK(MaxShiftText != nullptr)

	TextTranslator* translator = TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	const DString translationKey = (Card::GetMaxShift() == 1.f) ? TXT("MaxShiftSingular") : TXT("MaxShiftPlural");
	DString text = translator->TranslateText(translationKey, TXT("Solitaire"), TXT("OptionsMenu"));

	DString::FormatString(text, FormatFloat(Card::GetMaxShift()));

	MaxShiftText->SetText(text);
}

void OptionsMenu::HandleMaxAngleIncrement (ButtonComponent* uiComponent)
{
	if (Card::GetMaxAngle() < MaxAngleRange.Max)
	{
		Card::SetMaxAngle(Card::GetMaxAngle() + IncrementStep);
		RefreshMaxAngleText();
	}
}

void OptionsMenu::HandleMaxAngleDecrement (ButtonComponent* uiComponent)
{
	if (Card::GetMaxAngle() > MaxAngleRange.Min)
	{
		Card::SetMaxAngle(Card::GetMaxAngle() - IncrementStep);
		RefreshMaxAngleText();
	}
}

void OptionsMenu::HandleMaxShiftIncrement (ButtonComponent* uiComponent)
{
	if (Card::GetMaxShift() < MaxShiftRange.Max)
	{
		Card::SetMaxShift(Card::GetMaxShift() + IncrementStep);
		RefreshMaxShiftText();
	}
}

void OptionsMenu::HandleMaxShiftDecrement (ButtonComponent* uiComponent)
{
	if (Card::GetMaxShift() > MaxShiftRange.Min)
	{
		Card::SetMaxShift(Card::GetMaxShift() - IncrementStep);
		RefreshMaxShiftText();
	}
}

void OptionsMenu::HandleAcceptClicked (ButtonComponent* uiComponent)
{
	//Iterate through all cards to apply new settings
	SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
	CHECK(solitaireEngine != nullptr)
	SolitaireGame* game = solitaireEngine->GetActiveGame();
	if (game != nullptr)
	{
		const std::vector<Card*> allCards = game->ReadAllCards();
		for (UINT_TYPE i = 0; i < allCards.size(); ++i)
		{
			allCards.at(i)->ReapplyRandomShifting();
		}
	}

	Destroy();
}

void OptionsMenu::HandleCancelClicked (ButtonComponent* uiComponent)
{
	//Restore settings
	Card::SetMaxAngle(OriginalMaxAngle);
	Card::SetMaxShift(OriginalMaxShift);
	Destroy();
}
SOLITAIRE_END