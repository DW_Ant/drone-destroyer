/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Table.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
IMPLEMENT_CLASS(Solitaire, Table, SD, Entity)

void Table::InitProps ()
{
	Super::InitProps();

	Sprite = nullptr;

	//Set transform to fill entire screen
	Position = Vector2::ZeroVector;
	Size = Vector2(1.f, 1.f);
}

void Table::BeginObject ()
{
	Super::BeginObject();

	Sprite = SpriteComponent::CreateObject();
	if (AddComponent(Sprite.Get()))
	{
		Texture* tableTexture = TexturePool::FindTexturePool()->FindTexture(TXT("SampleGames.Solitaire.Background"));
		CHECK(tableTexture != nullptr)

		Sprite->SetSpriteTexture(tableTexture);
		RegisterToDrawLayer();
	}
}

void Table::RegisterToDrawLayer ()
{
	SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
	if (solitaireEngine != nullptr)
	{
		SolitaireDrawLayer* drawLayer = solitaireEngine->GetCardLayer();
		CHECK(drawLayer != nullptr)

		drawLayer->Background = Sprite;
	}
}
SOLITAIRE_END