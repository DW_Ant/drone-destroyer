/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  VictoryStation.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
IMPLEMENT_CLASS(Solitaire, VictoryStation, Solitaire, CardStation)
void VictoryStation::BeginObject ()
{
	Super::BeginObject();

	//Player can never drag a card away from victory station.
	if (Input != nullptr)
	{
		Input->Destroy();
	}
}

bool VictoryStation::CanAddCard (Card* newCard) const
{
	Card* topCard = GetTopCard();
	if (topCard != nullptr)
	{
		//Must be in ascending order and match suit
		return (newCard->GetSuit() == topCard->GetSuit() &&
			static_cast<int>(newCard->GetValue()) - static_cast<int>(topCard->GetValue()) == 1);
	}
	else
	{
		return (newCard->GetValue() == Card::CV_Ace);
	}
}

bool VictoryStation::CanAddColumn (CardColumn* newColumn) const
{
	//Only a single card can be added one at a time
	if (newColumn->ReadCardList().size() != 1)
	{
		return false;
	}

	return CanAddCard(newColumn->ReadCardList().at(0));
}

bool VictoryStation::HasCompleteSet () const
{
	const int numCardsInSuit = 13;
	if (CardStack.size() != numCardsInSuit)
	{
		return false;
	}

	for (UINT_TYPE i = 0; i < numCardsInSuit; ++i)
	{
		if (static_cast<int>(CardStack.at(i)->GetValue()) != (i+1))
		{
			return false;
		}
	}

	return true;
}
SOLITAIRE_END