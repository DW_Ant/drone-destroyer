/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CardTransform.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
CardTransform::CardTransform () : PlanarTransform(),
	Rotation(0.f)
{

}

CardTransform::CardTransform (const Vector2& inPosition, const Vector2& inSize, const Vector2& inPivot, FLOAT inDepth, const PlanarTransform* inRelativeTo) : PlanarTransform(inPosition, inSize, inPivot, inDepth, inRelativeTo),
	Rotation(0.f)
{

}

CardTransform::CardTransform (const CardTransform& copyObj) : PlanarTransform(copyObj),
	Rotation(copyObj.Rotation)
{

}

CardTransform::~CardTransform ()
{

}

void CardTransform::CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const
{
	PlanarTransform::CalculateScreenSpaceTransform(target, cam, OUT outProjectionData);

	outProjectionData.Rotation = Rotation.Value;
}

void CardTransform::SetRotation (FLOAT newRotation)
{
	Rotation = newRotation;
}
SOLITAIRE_END