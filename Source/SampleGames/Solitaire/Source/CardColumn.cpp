/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CardColumn.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
IMPLEMENT_CLASS(Solitaire, CardColumn, SD, Entity)

const INT CardColumn::COLUMN_INPUT_PRIORITY = 20;
const FLOAT CardColumn::VERTICAL_SPACING = 24.f;

void CardColumn::InitProps ()
{
	Super::InitProps();

	bIsDragging = false;
	CardReturn = nullptr;
	Input = nullptr;
}

void CardColumn::BeginObject ()
{
	Super::BeginObject();

	Input = InputComponent::CreateObject();
	if (AddComponent(Input.Get()))
	{
		InputBroadcaster* mainBroadcaster = InputEngineComponent::Find()->GetMainBroadcaster();
		CHECK(mainBroadcaster != nullptr)

		mainBroadcaster->AddInputComponent(Input.Get(), COLUMN_INPUT_PRIORITY);
		Input->MouseClickDelegate = SDFUNCTION_3PARAM(this, CardColumn, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
	}

	SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
	if (solitaireEngine == nullptr)
	{
		SolitaireLog.Log(LogCategory::LL_Warning, TXT("Cannot register CardColumn to a SolitaireDrawLayer since it could not find a SolitaireEngineComponent in curren thread."));
		return;
	}

	SolitaireDrawLayer* drawLayer = solitaireEngine->GetCardLayer();
	if (drawLayer == nullptr)
	{
		SolitaireLog.Log(LogCategory::LL_Warning, TXT("Cannot register CardColumn to a SolitaireDrawLayer since the local Solitaire Engine does not have a SolitaireDrawLayer."));
		return;
	}

	drawLayer->RegisterCardColumn(this);
}

bool CardColumn::IsWithinBounds (const Vector2& point) const
{
	//Add a slight buffer around the position to prevent players from clicking on the stations underneathe columns.
	const FLOAT sizeBuffer = 12.f;
	if (point.X > ReadCachedAbsPosition().X - sizeBuffer && point.X < ReadCachedAbsPosition().X + ReadCachedAbsSize().X + sizeBuffer &&
		point.Y > ReadCachedAbsPosition().Y - sizeBuffer && point.Y < ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y + sizeBuffer)
	{
		return true;
	}

	for (Card* card : CardList)
	{
		if (card->IsWithinBounds(point))
		{
			return true;
		}
	}

	return false;
}

void CardColumn::ReturnCards (CardColumn* columnToReturn)
{
	AppendColumn(columnToReturn);
}

void CardColumn::AppendColumn (CardColumn* bottomColumn)
{
	UINT_TYPE originalCardSize = CardList.size();
	for (UINT_TYPE i = 0; i < bottomColumn->CardList.size(); ++i)
	{
		bottomColumn->CardList.at(i)->SetRelativeTo(this);
		bottomColumn->CardList.at(i)->SetPositionNear(Vector2(0.f, VERTICAL_SPACING * FLOAT::MakeFloat(i + originalCardSize)));
		CardList.push_back(bottomColumn->CardList.at(i));
	}

	bottomColumn->Destroy();
}

void CardColumn::AppendCard (Card* newBottomCard)
{
	CHECK(newBottomCard != nullptr)

	newBottomCard->SetRelativeTo(this);
	newBottomCard->SetPositionNear(Vector2(0.f, VERTICAL_SPACING * FLOAT::MakeFloat(CardList.size())));
	CardList.push_back(newBottomCard);
}

void CardColumn::RemoveAllCards ()
{
	ContainerUtils::Empty(CardList);
}

CardColumn* CardColumn::DetachAt (UINT_TYPE cardIdx)
{
	CHECK(ContainerUtils::IsValidIndex(CardList, cardIdx))

	CardColumn* newColumn = CardColumn::CreateObject();
	for (UINT_TYPE i = cardIdx; i < CardList.size(); ++i)
	{
		CardList.at(i)->SetRelativeTo(newColumn);
		CardList.at(i)->SetPositionNear(Vector2(0.f, VERTICAL_SPACING * FLOAT::MakeFloat(newColumn->CardList.size())));
		newColumn->CardList.push_back(CardList.at(i));
	}

	CardList.erase(CardList.begin() + cardIdx, CardList.end());
	return newColumn;
}

bool CardColumn::CanCombine (CardColumn* bottomColumn) const
{
	return CanCombine(bottomColumn->CardList.at(0));
}

bool CardColumn::CanCombine (Card* newCard) const
{
	if (newCard == nullptr)
	{
		return false;
	}

	if (ContainerUtils::IsEmpty(CardList))
	{
		return (newCard->GetValue() == Card::CV_King);
	}

	//Must decrement a number and must be opposite color
	Card* lastCard = CardList.at(CardList.size() - 1);
	CHECK(lastCard != nullptr)

	return (lastCard->IsRedCard() != newCard->IsRedCard() &&
		static_cast<int>(lastCard->GetValue()) - static_cast<int>(newCard->GetValue()) == 1);
}

void CardColumn::RenderCards (RenderTarget* renderTarget, Camera* camera)
{
	ComputeAbsTransform();
	MarkProjectionDataDirty();

	for (Card* card : CardList)
	{
		card->ComputeAbsTransform();

		SpriteComponent* cardSprite = card->GetSprite();
		if (cardSprite != nullptr && cardSprite->IsVisible())
		{
			cardSprite->Render(renderTarget, camera);
		}
	}
}

void CardColumn::SetCardList (const std::vector<Card*> newCardList)
{
	CardList = newCardList;

	for (UINT_TYPE i = 0; i < CardList.size(); ++i)
	{
		CardList.at(i)->SetRelativeTo(this);
		CardList.at(i)->SetPositionNear(Vector2(0.f, VERTICAL_SPACING * FLOAT::MakeFloat(i)));
	}
}

void CardColumn::SetDragging (bool bNewDragging, ReturnCardInterface* returnPlace)
{
	bIsDragging = (bNewDragging && returnPlace != nullptr);
	CardReturn = returnPlace;
}

INT CardColumn::FindCardFromCoordinates (const Vector2& mousePos) const
{
	for (INT i = INT(CardList.size()) - 1; i >= 0; --i)
	{
		if (CardList.at(i.ToUnsignedInt())->IsWithinBounds(mousePos))
		{
			return i;
		}
	}

	return INT_INDEX_NONE;
}

bool CardColumn::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& buttonEvent, sf::Event::EventType eventType)
{
	if (buttonEvent.button != sf::Mouse::Left)
	{
		return false;
	}

	Vector2 mousePos(FLOAT::MakeFloat(buttonEvent.x), FLOAT::MakeFloat(buttonEvent.y));
	if (bIsDragging)
	{
		//Find the station/column the mouse is over
		for (ObjectIterator iter(Engine::FindEngine()->GetEntityHashNumber()); iter.SelectedObject; iter++)
		{
			if (CardColumn* col = dynamic_cast<CardColumn*>(iter.SelectedObject.Get()))
			{
				if (col != this && col->CanCombine(this) && col->IsWithinBounds(mousePos))
				{
					col->AppendColumn(this);
					return true;
				}
			}
		}

		SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
		CHECK(solitaireEngine != nullptr)

		//The game has a static list of all stations.  It'll be faster to iterate through that list.
		SolitaireGame* game = solitaireEngine->GetActiveGame();
		if (game != nullptr)
		{
			for (CardStation* station : game->ReadAllStations())
			{
				if (station->CanAddColumn(this) && station->IsWithinBounds(mousePos))
				{
					station->AddColumn(this);
					return true;
				}
			}
		}

		//Nothing will accept this column.  Return it to previous location.
		CHECK(CardReturn != nullptr)
		CardReturn->ReturnCards(this);
		Destroy();
		return true;
	}
	else //Column that isn't dragging - player is attempting to pickup a card
	{
		if (ContainerUtils::IsEmpty(CardList) || !IsWithinBounds(mousePos))
		{
			return false;
		}

		INT pickedCardIdx = FindCardFromCoordinates(mousePos);
		if (pickedCardIdx == INT_INDEX_NONE)
		{
			return true; //Consume input but don't do anything with it (player must have clicked on edge of column but the card shift offset is not under mouse).
		}

		if (eventType != sf::Event::MouseButtonPressed)
		{
			return true; //Consume input but don't do anything with it.
		}

		Vector2 clickOffset(CardList.at(pickedCardIdx.ToUnsignedInt())->ReadCachedAbsPosition() - CardList.at(pickedCardIdx.ToUnsignedInt())->GetPivot() - mousePos);
		CardColumn* newCol = DetachAt(pickedCardIdx.ToUnsignedInt());
		CHECK(newCol != nullptr)

		newCol->SetDragging(true, this);
		newCol->SetRelativeTo(mouse);
		newCol->Input->SetInputPriority(COLUMN_INPUT_PRIORITY + 5);

		newCol->SetPosition(clickOffset + (Card::GetCardSize() * 0.5f));
		return true;
	}
}
SOLITAIRE_END