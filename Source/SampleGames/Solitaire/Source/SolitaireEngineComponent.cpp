/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolitaireEngineComponent.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
IMPLEMENT_ENGINE_COMPONENT(Solitaire, SolitaireEngineComponent)

SolitaireEngineComponent::SolitaireEngineComponent () : EngineComponent(),
	NumPlays(0),
	NumVictories(0),
	FastestTime(-1.f)
{

}

void SolitaireEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_SOLITAIRE, TICK_GROUP_PRIORITY_SOLITAIRE);

	CardDrawLayer = SolitaireDrawLayer::CreateObject();
	GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
	CHECK(graphicsEngine != nullptr)
	Window* mainWindow = graphicsEngine->GetPrimaryWindow();
	if (mainWindow != nullptr)
	{
		mainWindow->RegisterDrawLayer(CardDrawLayer.Get());
	}

	GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
	CHECK(guiEngine != nullptr)
	guiEngine->SetDefaultThemeClass(SolitaireTheme::SStaticClass());
}

void SolitaireEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	ImportSolitaireResources();

	GuiTheme* theme = GuiTheme::GetGuiTheme();
	CHECK(theme != nullptr)
	theme->SetDefaultStyle(SolitaireTheme::SOLITAIRE_STYLE_NAME);
}

void SolitaireEngineComponent::PostInitializeComponent ()
{
	Background = Table::CreateObject();

	MainMenu = SolitaireMenu::CreateObject();
	MainMenu->RegisterToMainWindow();

	Bot = SolitaireBot::CreateObject();
}

void SolitaireEngineComponent::ShutdownComponent ()
{
	if (Bot != nullptr)
	{
		Bot->Destroy();
	}

	if (CardDrawLayer != nullptr)
	{
		CardDrawLayer->Destroy();
	}

	if (MainMenu != nullptr)
	{
		MainMenu->Destroy();
	}

	if (ActiveGame != nullptr)
	{
		ActiveGame->Destroy();
	}

	Super::ShutdownComponent();
}

std::vector<const DClass*> SolitaireEngineComponent::GetPreInitializeDependencies () const
{
	return std::vector<const DClass*>({GraphicsEngineComponent::SStaticClass(), GuiEngineComponent::SStaticClass()});
}

std::vector<const DClass*> SolitaireEngineComponent::GetInitializeDependencies () const
{
	return std::vector<const DClass*>{GuiEngineComponent::SStaticClass()};
}

void SolitaireEngineComponent::StartNewGame (const DClass* gameClass)
{
	NumPlays++;

	if (ActiveGame != nullptr)
	{
		ActiveGame->ClearGame();
	}

	if (const SolitaireGame* gameCdo = dynamic_cast<const SolitaireGame*>(gameClass->GetDefaultObject()))
	{
		ActiveGame = Object::ReplaceTargetWithObjOfMatchingClass<SolitaireGame>(ActiveGame.Get(), gameCdo);
		if (ActiveGame != nullptr)
		{
			ActiveGame->SetupNewGame();
		}
	}
	else
	{
		SolitaireLog.Log(LogCategory::LL_Warning, TXT("Cannot start a new game since the specified class (%s) is not a SolitaireGame."), gameClass->ToString());
	}
}

void SolitaireEngineComponent::BeatenGame (FLOAT elapsedTime)
{
	NumVictories++;
	if (FastestTime <= 0.f || elapsedTime < FastestTime)
	{
		FastestTime = elapsedTime;
	}

	if (MainMenu != nullptr)
	{
		MainMenu->RefreshScoreboard();
	}
}

void SolitaireEngineComponent::ImportSolitaireResources ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("SampleGames") / TXT("Solitaire"), TXT("Background.jpg")), TXT("SampleGames.Solitaire.Background"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("SampleGames") / TXT("Solitaire"), TXT("CardOutline.png")), TXT("SampleGames.Solitaire.CardOutline"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("SampleGames") / TXT("Solitaire"), TXT("CardBack.png")), TXT("SampleGames.Solitaire.CardBack"));

	std::vector<DString> suits;
	suits.push_back(TXT("Clubs"));
	suits.push_back(TXT("Diamonds"));
	suits.push_back(TXT("Hearts"));
	suits.push_back(TXT("Spades"));

	for (UINT_TYPE suitIdx = 0; suitIdx < suits.size(); ++suitIdx)
	{
		for (int i = 1; i <= 13; ++i)
		{
			localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("SampleGames") / TXT("Solitaire") / suits.at(suitIdx), Card::CardValueToString(i) + TXT(".png")), TXT("SampleGames.Solitaire.") + suits.at(suitIdx) + TXT(".") + Card::CardValueToString(i));
		}
	}
}
SOLITAIRE_END