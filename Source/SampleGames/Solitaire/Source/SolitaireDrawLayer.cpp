/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolitaireDrawLayer.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
IMPLEMENT_CLASS(Solitaire, SolitaireDrawLayer, SD, PlanarDrawLayer)
const INT SolitaireDrawLayer::SOLITAIRE_DRAW_LAYER_PRIORITY = 100;

void SolitaireDrawLayer::InitProps ()
{
	Super::InitProps();

	Background = nullptr;
}

void SolitaireDrawLayer::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, SolitaireDrawLayer, HandleGarbageCollection, void));
	}
}

void SolitaireDrawLayer::RenderDrawLayer (RenderTarget* renderTarget, Camera* cam)
{
	//Render background first to be drawn behind everything else
	if (Background != nullptr)
	{
		if (PlanarTransform* transform = dynamic_cast<PlanarTransform*>(Background->GetOwner()))
		{
			transform->ComputeAbsTransform();
			transform->MarkProjectionDataDirty();
			Background->Render(renderTarget, cam);
		}
		else
		{
			SolitaireLog.Log(LogCategory::LL_Warning, TXT("The Background (%s) component's owner does not have a PlanarTransformation."), Background);
		}
	}

	Super::RenderDrawLayer(renderTarget, cam);

	SortRenderedObjects();

	//Render stations first since they should be drawn under columns
	RenderCardStations(renderTarget, cam);
	RenderCardColumns(renderTarget, cam);
}

void SolitaireDrawLayer::Destroyed ()
{
	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, SolitaireDrawLayer, HandleGarbageCollection, void));
	}

	Super::Destroyed();
}

void SolitaireDrawLayer::RegisterCardColumn (CardColumn* newColumn)
{
	Columns.push_back(newColumn);
}

void SolitaireDrawLayer::RegisterCardStation (CardStation* newStation)
{
	Stations.push_back(newStation);
}

void SolitaireDrawLayer::SortRenderedObjects ()
{
	PurgeExpiredObjects();

	std::sort(Columns.begin(), Columns.end(), [](CardColumn* a, CardColumn* b)
	{
		return a->GetDepth() > b->GetDepth();
	});

	//Don't bother sorting stations since no station overlaps another station.
}

void SolitaireDrawLayer::RenderCardStations (RenderTarget* renderTarget, Camera* cam)
{
	for (UINT_TYPE i = 0; i < Stations.size(); ++i)
	{
		if (Stations.at(i)->IsVisible())
		{
			Stations.at(i)->RenderCardsInStation(renderTarget, cam);
		}
	}
}

void SolitaireDrawLayer::RenderCardColumns (RenderTarget* renderTarget, Camera* cam)
{
	for (UINT_TYPE i = 0; i < Columns.size(); ++i)
	{
		if (Columns.at(i)->IsVisible())
		{
			Columns.at(i)->RenderCards(renderTarget, cam);
		}
	}
}

void SolitaireDrawLayer::PurgeExpiredObjects ()
{
	UINT_TYPE i = 0;
	while (i < Columns.size())
	{
		if (!VALID_OBJECT(Columns.at(i)))
		{
			Columns.erase(Columns.begin() + i);
			continue;
		}

		++i;
	}

	i = 0;
	while (i < Stations.size())
	{
		if (!VALID_OBJECT(Stations.at(i)))
		{
			Stations.erase(Stations.begin() + i);
		}

		++i;
	}
}

void SolitaireDrawLayer::HandleGarbageCollection ()
{
	PurgeExpiredObjects();
}
SOLITAIRE_END