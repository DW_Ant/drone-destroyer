/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolitaireBot.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
IMPLEMENT_CLASS(Solitaire, SolitaireBot, SD, Entity)

void SolitaireBot::InitProps ()
{
	Super::InitProps();

	MouseDestination = Vector2(-1.f, -1.f);
	MouseReleaseDestination = Vector2(-1.f, -1.f);
	MouseDistTolerance = 2.f;
	MouseSpeed = 2250.f; //pixels per second
	ActionDelay = Range<FLOAT>(0.2f, 0.4f);
	IsDraggingCard = false;
	Tick = nullptr;
	IsPaused = true;

	SleepTime = -1.f;
	ActionDoneThisCycle = false;
}

void SolitaireBot::BeginObject ()
{
	Super::BeginObject();

	Tick = TickComponent::CreateObject(TICK_GROUP_SOLITAIRE);
	if (AddComponent(Tick.Get()))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, SolitaireBot, HandleTick, void, FLOAT));
	}

	Input = InputComponent::CreateObject();
	if (AddComponent(Input.Get()))
	{
		Input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, SolitaireBot, HandleInput, bool, const sf::Event&);
		Input->MouseWheelScrollDelegate = SDFUNCTION_2PARAM(this, SolitaireBot, HandleWheelInput, bool, MousePointer*, const sf::Event::MouseWheelScrollEvent&);
		Input->MouseClickDelegate = SDFUNCTION_3PARAM(this, SolitaireBot, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
		Input->SetInputPriority(1000);
	}
}

void SolitaireBot::SetPaused (bool newIsPaused)
{
	IsPaused = newIsPaused;
	if (!IsPaused)
	{
		MousePointer* mouse = MousePointer::GetMousePointer();
		CHECK(mouse != nullptr)

		GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
		CHECK(graphicsEngine != nullptr)
		Window* mainWindow = graphicsEngine->GetPrimaryWindow();
		Vector2 lockPos = mouse->ReadCachedAbsPosition();
		if (mainWindow != nullptr)
		{
			INT posX;
			INT posY;
			INT sizeX;
			INT sizeY;
			mainWindow->GetWindowPosition(OUT posX, OUT posY);
			mainWindow->GetWindowSize(OUT sizeX, OUT sizeY);
			lockPos.X += posX.ToFLOAT();
			lockPos.Y += posY.ToFLOAT();

			//clamp the mouse at its current position to prevent from dragging the mouse outside of window.  Protection against very large mouse deltas.
			OS_ClampMousePointer(Rectangle<INT>(posX, posY, posX + sizeX, posY + sizeY, Rectangle<INT>::CS_XRightYDown));
		}

		CalculateNextAction();
	}
	else
	{
		//Relinquish clamped mouse
		OS_ClampMousePointer(Rectangle<INT>(0, 0, 0, 0));
	}
}

void SolitaireBot::CalculateNextAction ()
{
	SolitaireEngineComponent* solitaireEngine = SolitaireEngineComponent::Find();
	CHECK(solitaireEngine != nullptr)
	SolitaireGame* game = solitaireEngine->GetActiveGame();
	if (game == nullptr)
	{
		return;
	}

	const Vector2 clickRange(Card::GetCardSize() * 0.5f);
	const Vector2 randomClickLocation(RandomUtils::RandRange(clickRange.X * -0.4f, clickRange.X * 0.4f),
		RandomUtils::RandRange(clickRange.Y * -0.4f, clickRange.Y * 0.4f));

	//The bot assumes the number of center stations and center columns are the same.
	CHECK(game->ReadCentralColumns().size() == game->ReadCentralStations().size())

	/*
	List of priorities
	1. Reveal face down cards in central stations.
	2. Combine one central column with another column.  Potentially move kings to empty columns.
	3. Place any cards in victory stations.
	4. Place card from draw station to central stations.
	5. Draw a new card.
	6. Restart Game.
	*/
		
	//1. Find any face down central stations
	for (UINT_TYPE i = 0; i < game->ReadCentralColumns().size(); ++i)
	{
		if (game->ReadCentralStations().at(i)->GetTopCard() != nullptr &&
			ContainerUtils::IsEmpty(game->ReadCentralColumns().at(i)->ReadCardList()))
		{
			const Vector2 baseLocation = game->ReadCentralStations().at(i)->GetTopCard()->ReadCachedAbsPosition();

			//Found a card the bot can reveal
			MouseDestination = Vector2(randomClickLocation + game->ReadCentralStations().at(i)->GetTopCard()->ReadCachedAbsPosition());
			ActionDoneThisCycle = true;
			return;
		}
	}

	//2. Combine central columns
	for (UINT_TYPE i = 0; i < game->ReadCentralColumns().size(); ++i)
	{
		if (!ContainerUtils::IsEmpty(game->ReadCentralColumns().at(i)->ReadCardList()))
		{
			Card* topCard = game->ReadCentralColumns().at(i)->ReadCardList().at(0);
			if (topCard == nullptr)
			{
				continue;
			}

			//Iterate through the central columns to see if this column can combine with others.
			for (UINT_TYPE colIdx = 0; colIdx < game->ReadCentralColumns().size(); ++colIdx)
			{
				if (colIdx == i)
				{
					continue; //don't combine with self
				}

				//Don't bother combining to an empty column if the current column (king) is already at an empty stack.
				if (game->ReadCentralStations().at(i)->GetTopCard() == nullptr && topCard->GetValue() == Card::CV_King)
				{
					continue;
				}

				CardColumn* column = game->ReadCentralColumns().at(colIdx);
				if (column->CanCombine(topCard))
				{
					//Click somewhere on top of the TopCard since there may be cards partially covering its bottom.
					MouseDestination = Vector2(randomClickLocation.X + topCard->ReadCachedAbsPosition().X, topCard->ReadCachedAbsPosition().Y - (Card::GetCardSize().Y * 0.5f) + 8.f);
					MouseReleaseDestination = Vector2(randomClickLocation + column->ReadCachedAbsPosition() + (column->ReadCachedAbsSize() * 0.5f));
					ActionDoneThisCycle = true;
					return;
				}
			}
		}
	}

	//3. Look for a card to place in victory station
	const std::function<VictoryStation*(Card*)> FindAvailableVictoryStation = [&game](Card* targetCard) -> VictoryStation*
	{
		for (UINT_TYPE i = 0; i < game->ReadVictoryStations().size(); ++i)
		{
			if (game->ReadVictoryStations().at(i)->CanAddCard(targetCard))
			{
				return game->ReadVictoryStations().at(i);
			}
		}

		return nullptr;
	};

	for (UINT_TYPE i = 0; i < game->ReadCentralColumns().size(); ++i)
	{
		if (ContainerUtils::IsEmpty(game->ReadCentralColumns().at(i)->ReadCardList()))
		{
			continue;
		}

		Card* bottomCard = game->ReadCentralColumns().at(i)->ReadCardList().at(game->ReadCentralColumns().at(i)->ReadCardList().size() - 1);
		VictoryStation* availableStation = FindAvailableVictoryStation(bottomCard);
		if (availableStation != nullptr)
		{
			MouseDestination = Vector2(randomClickLocation + bottomCard->ReadCachedAbsPosition());
			MouseReleaseDestination = Vector2(randomClickLocation + availableStation->ReadCachedAbsPosition() + (availableStation->ReadCachedAbsSize() * 0.5f));
			ActionDoneThisCycle = true;
			return;
		}
	}

	//Check card at staging area
	Card* stageCard = game->GetStageStation()->GetTopCard();
	if (stageCard != nullptr)
	{
		//4. Find a location for staging Card
		VictoryStation* availableStation = FindAvailableVictoryStation(stageCard);
		if (availableStation != nullptr)
		{
			MouseDestination = Vector2(randomClickLocation + stageCard->ReadCachedAbsPosition());
			MouseReleaseDestination = Vector2(randomClickLocation + availableStation->ReadCachedAbsPosition() + (availableStation->ReadCachedAbsSize() * 0.5f));
			ActionDoneThisCycle = true;
			return;
		}

		//Look for available central column
		for (UINT_TYPE i = 0; i < game->ReadCentralColumns().size(); ++i)
		{
			CardColumn* column = game->ReadCentralColumns().at(i);
			if (column->CanCombine(stageCard))
			{
				MouseDestination = Vector2(randomClickLocation + stageCard->ReadCachedAbsPosition());
				MouseReleaseDestination = Vector2(randomClickLocation + column->ReadCachedAbsPosition() + (column->ReadCachedAbsSize() * 0.5f));
				ActionDoneThisCycle = true;
				return;
			}
		}
	}

	//5. Draw a new card
	if (game->GetDrawStation()->GetTopCard() == nullptr)
	{
		//Draw station is empty.  If an action hasn't been conducted within this cycle, then terminate game.
		if (!ActionDoneThisCycle)
		{
			//6. Terminate game
			SolitaireMenu* menu = solitaireEngine->GetMainMenu();
			CHECK(menu != nullptr)
			ButtonComponent* startButton = menu->GetStartButton();
			CHECK(startButton != nullptr)
			MouseDestination = Vector2(startButton->ReadCachedAbsPosition() + (startButton->ReadCachedAbsSize() * 0.5f));
			return;
		}

		ActionDoneThisCycle = false; //Clear cycle flag to detect if this new cycle has any actions
	}

	MouseDestination = Vector2(randomClickLocation + game->GetDrawStation()->ReadCachedAbsPosition() + (game->GetDrawStation()->ReadCachedAbsSize() * 0.5f));
}

void SolitaireBot::MoveMouse (FLOAT deltaSec)
{
	MousePointer* mouse = MousePointer::GetMousePointer();
	CHECK(mouse != nullptr)

	if (MouseDestination.X >= 0.f && MouseDestination.Y >= 0.f)
	{
		Vector2 remainingPath = MouseDestination - mouse->ReadCachedAbsPosition();
		FLOAT distToTravel = remainingPath.VSize();
		if (distToTravel <= MouseDistTolerance)
		{
			InputEngineComponent* inputEngine = InputEngineComponent::Find();
			CHECK(inputEngine != nullptr)
			InputBroadcaster* mainBroadcaster = inputEngine->GetMainBroadcaster();
			CHECK(mainBroadcaster != nullptr)

			//Create a new mouse event
			sf::Event newEvent;
			newEvent.mouseButton.button = sf::Mouse::Button::Left;
			newEvent.mouseButton.x = INT(mouse->ReadCachedAbsPosition().X).ToInt32();
			newEvent.mouseButton.y = INT(mouse->ReadCachedAbsPosition().Y).ToInt32();

			//Clicking on new card
			if (!IsDraggingCard)
			{
				newEvent.type = sf::Event::MouseButtonPressed;
				mainBroadcaster->BroadcastNewEvent(newEvent);
				if (MouseReleaseDestination.X < 0.f || MouseReleaseDestination.Y < 0.f)
				{
					//Not dragging a card anywhere.  Immediately send a mouse up event
					newEvent.type = sf::Event::MouseButtonReleased;
					mainBroadcaster->BroadcastNewEvent(newEvent);
					MouseDestination = Vector2(-1.f, -1.f);
					//Add the bot 'reaction' time
					SleepTime = RandomUtils::RandRange(ActionDelay.Min, ActionDelay.Max);
				}
				else
				{
					//Move the mouse to release destination before the mouse up event
					MouseDestination = MouseReleaseDestination;
					MouseReleaseDestination = Vector2(-1.f, -1.f);
					IsDraggingCard = true;
				}
			}
			else
			{
				//Send a mouse up event instead
				newEvent.type = sf::Event::MouseButtonReleased;
				mainBroadcaster->BroadcastNewEvent(newEvent);
				MouseDestination = Vector2(-1.f, -1.f);
				IsDraggingCard = false;

				//Add the bot 'reaction' time
				SleepTime = RandomUtils::RandRange(ActionDelay.Min, ActionDelay.Max);
			}
		}
		else
		{
			//Move the mouse closer to destination
			Vector2 newMousePos(remainingPath);
			newMousePos.Normalize();
			FLOAT deltaMove = MouseSpeed * deltaSec;
			newMousePos *= Utils::Min(distToTravel, deltaMove);
			newMousePos += mouse->ReadCachedAbsPosition();

			GraphicsEngineComponent* graphics = GraphicsEngineComponent::Find();
			CHECK(graphics != nullptr)
			Window* mainWindow = graphics->GetPrimaryWindow();
			CHECK(mainWindow != nullptr)
			mouse->SetRelativeMousePosition(mainWindow, newMousePos);

			mouse->SetPosition(newMousePos);
		}
	}
}

bool SolitaireBot::HandleInput (const sf::Event& evnt)
{
	if (evnt.type == sf::Event::KeyReleased && evnt.key.code == sf::Keyboard::Numpad0)
	{
		SetPaused(!IsPaused);
		return true;
	}

	return false;
}

bool SolitaireBot::HandleWheelInput (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& wheelEvent)
{
	if (!IsPaused)
	{
		MouseSpeed = Utils::Max<FLOAT>(0.01f, MouseSpeed + (MouseSpeed * 0.1f * wheelEvent.delta));

		if (MouseSpeed != 0.f)
		{
			if (const SolitaireBot* cdo = dynamic_cast<const SolitaireBot*>(GetDefaultObject()))
			{
				FLOAT speedMod = MouseSpeed / cdo->MouseSpeed;
				ActionDelay.Min = cdo->ActionDelay.Min / speedMod;
				ActionDelay.Max = cdo->ActionDelay.Max / speedMod;
			}
		}

		return true;
	}

	return false;
}

bool SolitaireBot::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& mouseEvent, sf::Event::EventType type)
{
	if (mouseEvent.button == sf::Mouse::Button::Middle && type == sf::Event::MouseButtonReleased)
	{
		//Toggles AI
		SetPaused(!IsPaused);
		return true;
	}

	return false;
}

void SolitaireBot::HandleTick (FLOAT deltaSec)
{
	if (SleepTime > 0.f)
	{
		SleepTime -= deltaSec;
		return;
	}

	if (IsPaused)
	{
		return;
	}

	if (MouseDestination.X < 0.f && MouseDestination.Y < 0.f)
	{
		CalculateNextAction();
	}

	MoveMouse(deltaSec);
}
SOLITAIRE_END