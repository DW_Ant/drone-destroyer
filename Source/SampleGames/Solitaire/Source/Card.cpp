/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Card.cpp
=====================================================================
*/

#include "SolitaireClasses.h"

SOLITAIRE_BEGIN
IMPLEMENT_CLASS(Solitaire, Card, SD, Entity)

const Vector2 Card::SIZE_RATIO(1.f, 1.4f);
FLOAT Card::CardSize = 90.f;
FLOAT Card::MaxAngle = 3.f;
FLOAT Card::MaxShift = 3.f;

void Card::InitProps ()
{
	Super::InitProps();

	SetSize(GetCardSize());
	SetPivot(GetCardSize()); //Set pivot point at center so rotation spins about the center.  Note:  Cards' sprite scaling is 0.5.
	SetBoundingBoxTranslation(GetCardSize() * 0.5f);
	SetEnableFractionScaling(false);

	Value = CV_Ace;
	Suit = S_Hearts;
	bFacingUp = false;
	Sprite = nullptr;
}

void Card::BeginObject ()
{
	Super::BeginObject();

	Sprite = SpriteComponent::CreateObject();
	AddComponent(Sprite.Get());
}

DString Card::CardValueToString (int cardValue)
{
	switch (cardValue)
	{
		case (1): return TXT("Ace");
		case (11): return TXT("Jack");
		case (12): return TXT("Queen");
		case (13): return TXT("King");
	}

	return DString::MakeString(cardValue);
}

void Card::SetPositionNear (const Vector2& position)
{
	BasePosition = position;

	FLOAT randX = RandomUtils::RandRange(-MaxShift, MaxShift);
	FLOAT randY = RandomUtils::RandRange(-MaxShift, MaxShift);

	Vector2 newPosition(position);
	newPosition.X += randX;
	newPosition.Y += randY;

	//The positions specified in this function is relative to top left corner.  Offset the position so that
	//The origin is at the center of desired placement.
	newPosition += (GetCardSize() * 0.5f);

	SetPosition(newPosition);

	//Add a random spin
	SetRotation(RandomUtils::RandRange(-MaxAngle, MaxAngle));
}

void Card::ReapplyRandomShifting ()
{
	SetPositionNear(BasePosition);
}

void Card::InitCard (ECardValue inValue, ESuit inSuit)
{
	Value = inValue;
	Suit = inSuit;

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)
	Sprite->SetSpriteTexture(localTexturePool->FindTexture(TXT("SampleGames.Solitaire.CardBack"), true));
}

bool Card::IsRedCard () const
{
	return (Suit == S_Hearts || Suit == S_Diamonds);
}

void Card::SetCardSize (FLOAT newCardSize)
{
	CardSize = newCardSize;
}

void Card::SetMaxAngle (FLOAT newMaxAngle)
{
	MaxAngle = newMaxAngle;
}

void Card::SetMaxShift (FLOAT newMaxShift)
{
	MaxShift = newMaxShift;
}

void Card::SetFacingUp (bool bNewFacingUp)
{
	if (bFacingUp == bNewFacingUp)
	{
		return;
	}

	bFacingUp = bNewFacingUp;

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	if (Sprite != nullptr && localTexturePool != nullptr)
	{
		if (!bFacingUp)
		{
			Sprite->SetSpriteTexture(localTexturePool->FindTexture(TXT("SampleGames.Solitaire.CardBack"), true));
		}
		else
		{
			DString suitName;
			switch (Suit)
			{
				case (S_Hearts):
					suitName = TXT("Hearts");
					break;

				case (S_Clubs):
					suitName = TXT("Clubs");
					break;

				case (S_Diamonds):
					suitName = TXT("Diamonds");
					break;

				case (S_Spades):
					suitName = TXT("Spades");
					break;
			}

			Sprite->SetSpriteTexture(localTexturePool->FindTexture(TXT("SampleGames.Solitaire.") + suitName + TXT(".") + CardValueToString(static_cast<int>(Value)), true));
		}
	}
}
SOLITAIRE_END