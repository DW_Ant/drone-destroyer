/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolitaireBot.h
  An Entity that can play the Solitaire Game by analyzing the game board, and sending
  mouse events to play the game.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class SolitaireBot : public Entity
{
	DECLARE_CLASS(SolitaireBot)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The location the bot is sending the mouse pointer to. */
	Vector2 MouseDestination;

	/* Location the mouse release button is suppose to go. */
	Vector2 MouseReleaseDestination;

	/* The distance the mouse must be from destination before the mouse event is sent. */
	FLOAT MouseDistTolerance;

	/* How many pixels per second this bot is traversing through the board. */
	FLOAT MouseSpeed;

	/* Delay between each action (in seconds). */
	Range<FLOAT> ActionDelay;

	/* If true, then this bot is dragging a card around. */
	bool IsDraggingCard;

	DPointer<TickComponent> Tick;
	DPointer<InputComponent> Input;

	/* Becomes true if the bot is currently suspended due to user override. */
	bool IsPaused;

private:
	/* Amount of time to wait before processing Tick.  If negative, then this bot is not sleeping. */
	FLOAT SleepTime;

	/* Becomes true if an action was taken place within this drawing cycle.
	This is primarily used to detect a game over, and the bot needs to decide to start a new game. */
	bool ActionDoneThisCycle;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	void SetPaused (bool newIsPaused);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Evaluates the board to figure out where the bot should take the mouse pointer.
	 */
	virtual void CalculateNextAction ();

	/**
	 * Moves the mouse closer to destination.
	 */
	virtual void MoveMouse (FLOAT deltaSec);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleInput (const sf::Event& evnt);
	virtual bool HandleWheelInput (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& wheelEvent);
	virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& mouseEvent, sf::Event::EventType type);
	virtual void HandleTick (FLOAT deltaSec);
};
SOLITAIRE_END