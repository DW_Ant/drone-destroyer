/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolitaireGame.h
  Object that defines the game rules (win conditions, initial placements, and monitors
  player activity.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class Card;
class CardStation;
class VictoryStation;
class CardColumn;

class SolitaireGame : public Object
{
	DECLARE_CLASS(SolitaireGame)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of all cards on the table (no joker cards). */
	std::vector<Card*> AllCards;

	std::vector<CardStation*> AllStations;

	/* The station the player may draw cards from. */
	CardStation* DrawStation;

	/* The station the drawn cards are placed. */
	CardStation* StageStation;

	/* The seven locations that make up the center row. */
	std::vector<CardStation*> CentralStations;

	/* The seven columns over the central stations. */
	std::vector<CardColumn*> CentralColumns;

	/* The 4 foundations in the top right corner that the user must place the cards in ascending order to win the game. */
	std::vector<VictoryStation*> VictoryStations;

	/* Timestamp when this game has begun (in seconds). */
	FLOAT StartTime;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SetupNewGame ();
	virtual void ClearGame ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(CardStation*, DrawStation)
	DEFINE_ACCESSOR(CardStation*, StageStation)
	inline const std::vector<CardStation*>& ReadAllStations () const
	{
		return AllStations;
	}

	inline const std::vector<Card*>& ReadAllCards () const
	{
		return AllCards;
	}

	inline const std::vector<CardStation*>& ReadCentralStations () const
	{
		return CentralStations;
	}

	inline const std::vector<CardColumn*>& ReadCentralColumns () const
	{
		return CentralColumns;
	}
	
	inline const std::vector<VictoryStation*>& ReadVictoryStations () const
	{
		return VictoryStations;
	}

	DEFINE_ACCESSOR(FLOAT, StartTime);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if the cards are in the correct state to win the game.
	 */
	virtual bool HasWonGame () const;

	virtual void ExecuteVictorySequence ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleEmptyDrawStationClicked ();
	virtual void HandleDrawReveal (CardStation* station, Card* revealedCard);
	virtual void HandleCentralCardReveal (CardStation* station, Card* revealedCard);
	virtual void HandleVictoryCardPlaced (Card* addedCard);
};
SOLITAIRE_END