/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolitaireEngineComponent.h
  The EngineComponent that spawns the UI that launches Solitaire game instances.
  This EngineComponent is also responsible for setting up the custom DrawLayer.
=====================================================================
*/

#pragma once

#include "Solitaire.h"
#include "SolitaireMenu.h"
#include "SolitaireDrawLayer.h"
#include "SolitaireGame.h"
#include "SolitaireBot.h"

SOLITAIRE_BEGIN

class SolitaireEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(SolitaireEngineComponent)
		

	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The UI menu that's responsible for choosing a Solitaire game mode. */
	DPointer<SolitaireMenu> MainMenu;

	/* Draw Layer that handles rendering card objects on the table. */
	DPointer<SolitaireDrawLayer> CardDrawLayer;

	/* Game Instance that's defines the game rules in how to play Solitaire. */
	DPointer<SolitaireGame> ActiveGame;

	/* Entity that renders the table in the background. */
	DPointer<Entity> Background;

	/* The bot that could take control to play the Solitaire games. */
	DPointer<SolitaireBot> Bot;

	/* Number of times the player started a new game. */
	INT NumPlays;

	/* Number of times the player beaten the game. */
	INT NumVictories;

	/* Fastest game completion time (in seconds).  Becomes negative if the player has not completed a game yet. */
	FLOAT FastestTime;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SolitaireEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void PostInitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual std::vector<const DClass*> GetPreInitializeDependencies () const override;
	virtual std::vector<const DClass*> GetInitializeDependencies () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Instantiates/replaces a new game instance if the class is different from what's sepcified.
	 * Otherwise, the current game simply restarts.
	 */
	virtual void StartNewGame (const DClass* gameClass);

	virtual void BeatenGame (FLOAT elapsedTime);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SolitaireDrawLayer* GetCardLayer () const
	{
		return CardDrawLayer.Get();
	}

	inline SolitaireGame* GetActiveGame () const
	{
		return ActiveGame.Get();
	}

	inline SolitaireMenu* GetMainMenu ()
	{
		return MainMenu.Get();
	}

	DEFINE_ACCESSOR(INT, NumPlays)
	DEFINE_ACCESSOR(INT, NumVictories)
	DEFINE_ACCESSOR(FLOAT, FastestTime)


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ImportSolitaireResources ();
};
SOLITAIRE_END