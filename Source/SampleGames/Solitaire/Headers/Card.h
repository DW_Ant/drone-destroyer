/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Card.h
  Object that represents a single card.  It defines the card's number, suit, and color.
=====================================================================
*/

#pragma once

#include "Solitaire.h"
#include "CardTransform.h"

SOLITAIRE_BEGIN
class Card : public Entity, public CardTransform
{
	DECLARE_CLASS(Card)


	/*
	=====================
	  Data types
	=====================
	*/

public:
	enum ECardValue
	{
		CV_Ace = 1,
		CV_Two = 2,
		CV_Three = 3,
		CV_Four = 4,
		CV_Five = 5,
		CV_Six = 6,
		CV_Seven = 7,
		CV_Eight = 8,
		CV_Nine = 9,
		CV_Ten = 10,
		CV_Jack = 11,
		CV_Queen = 12,
		CV_King = 13
	};

	enum ESuit
	{
		S_Hearts,
		S_Clubs,
		S_Diamonds,
		S_Spades
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The ratios that determine all cards' size dimension multipliers. */
	static const Vector2 SIZE_RATIO;

protected:
	/* Base size of all cards.  Typically this is adjusted based on window size.  The card's resulting size is
	Width = CardSize * SIZE_RATIO,  Height = CardSize * SIZE_RATIO */
	static FLOAT CardSize;

	/* Determines the maximum angle the card is allowed to spin randomly (in degrees). */
	static FLOAT MaxAngle;

	/* Determines the maximum units the card is allowed to shift from specified coordinates (in pixels). */
	static FLOAT MaxShift;

	ECardValue Value;
	ESuit Suit;

	/* If true, then the value of the card is visible. */
	bool bFacingUp;

	DPointer<SpriteComponent> Sprite;

	/* Base position this card is placed near (position it would have been without any random shifting). */
	Vector2 BasePosition;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Translates a card value (1-13) to a string name.  This isn't meant for localization since this may influence
	 * directory locations and texture names.
	 */
	static DString CardValueToString (int cardValue);

	/**
	 * Sets the card's position somewhere around the specified location.
	 * This will also give the card a new random spin based on the MaxAngle property.
	 */
	virtual void SetPositionNear (const Vector2& position);

	virtual void ReapplyRandomShifting ();

	virtual void InitCard (ECardValue inValue, ESuit inSuit);
	bool IsRedCard () const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	static void SetCardSize (FLOAT newCardSize);
	static void SetMaxAngle (FLOAT newMaxAngle);
	static void SetMaxShift (FLOAT newMaxShift);

	void SetFacingUp (bool bNewFacingUp);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	static inline Vector2 GetCardSize ()
	{
		return Vector2(CardSize * SIZE_RATIO.X, CardSize * SIZE_RATIO.Y);
	}

	static inline FLOAT GetMaxAngle ()
	{
		return MaxAngle;
	}

	static inline FLOAT GetMaxShift ()
	{
		return MaxShift;
	}

	inline bool IsFacingUp () const
	{
		return bFacingUp;
	}

	inline ECardValue GetValue () const
	{
		return Value;
	}

	inline ESuit GetSuit () const
	{
		return Suit;
	}

	SpriteComponent* GetSprite () const
	{
		return Sprite.Get();
	}
};
SOLITAIRE_END