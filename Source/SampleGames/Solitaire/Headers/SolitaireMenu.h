/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolitaireMenu.h
  Menu that provides the player the option to start a new game or quit.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class SolitaireMenu : public GuiEntity
{
	DECLARE_CLASS(SolitaireMenu)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<ButtonComponent> StartGameButton;
	DPointer<ButtonComponent> ExitGameButton;
	DPointer<ButtonComponent> OptionsButton;
	DPointer<LabelComponent> Title;

	//Scoreboard bar
	DPointer<FrameComponent> ScoreboardBackground;
	DPointer<LabelComponent> NumPlaysLabel;
	DPointer<LabelComponent> NumVictoriesLabel;
	DPointer<LabelComponent> SpeedRecordLabel;
	DPointer<LabelComponent> CurTimeLabel;
	DPointer<TickComponent> Tick;

private:
	/* Becomes true if the buttons are placed out of the way since there's an active game running. */
	bool IsGameActive;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	
protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Refreshes the values on the scoreboard to match with the game's score.
	 */
	virtual void RefreshScoreboard ();

	/**
	 * Pauses/resumes the timer portion of the scoreboard.
	 */
	virtual void PauseScoreboard ();
	virtual void ResumeScoreboard ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ButtonComponent* GetStartButton () const
	{
		return StartGameButton.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Repositions UI elements so it's out of the way from the game board.
	 */
	virtual void SetMenuToGameMode ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleStartGameClicked (ButtonComponent* uiComponent);
	virtual void HandleExitClicked (ButtonComponent* uiComponent);
	virtual void HandleOptionsClicked (ButtonComponent* uiComponent);
	virtual void HandleTick (FLOAT deltaSec);
};
SOLITAIRE_END