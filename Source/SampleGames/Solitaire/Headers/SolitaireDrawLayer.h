/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolitaireDrawLayer.h
  Specialized DrawLayer that draws cards in order based on their stack positions.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class CardColumn;
class CardStation;

class SolitaireDrawLayer : public PlanarDrawLayer
{
	DECLARE_CLASS(SolitaireDrawLayer)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const INT SOLITAIRE_DRAW_LAYER_PRIORITY;

	/* Background that renders behind the cards. */
	DPointer<SpriteComponent> Background;

protected:
	std::vector<CardColumn*> Columns;
	std::vector<CardStation*> Stations;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void RenderDrawLayer (RenderTarget* renderTarget, Camera* cam) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void RegisterCardColumn (CardColumn* newColumn);
	virtual void RegisterCardStation (CardStation* newStation);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Sorts the Rendered cards based on their depth values.
	 */
	virtual void SortRenderedObjects ();

	virtual void RenderCardStations (RenderTarget* renderTarget, Camera* cam);
	virtual void RenderCardColumns (RenderTarget* renderTarget, Camera* cam);

	virtual void PurgeExpiredObjects ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleGarbageCollection ();
};
SOLITAIRE_END