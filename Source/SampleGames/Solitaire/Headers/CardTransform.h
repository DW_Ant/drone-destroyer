/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CardTransform.h
  Nearly identical to the parent class PlanarTransform, this transform allows cards
  to rotate.  The reason the rotation functionality is not added to Engine's PlanarTransform
  is because certain functions (such as IsWithinBounds) do not support Rotations.

  This class is internallized to Solitaire game since the rotations used are subtle, and to
  hint that rotations in planar transforms are not supported.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class CardTransform : public PlanarTransform
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The amount of spin about the origin/pivot point in degrees. */
	FLOAT Rotation;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CardTransform ();
	CardTransform (const Vector2& inPosition, const Vector2& inSize, const Vector2& inPivot = Vector2::ZeroVector, FLOAT inDepth = 0.f, const PlanarTransform* inRelativeTo = nullptr);
	CardTransform (const CardTransform& copyObj);
	virtual ~CardTransform ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	void SetRotation (FLOAT newRotation);
	

	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(FLOAT, Rotation)
};
SOLITAIRE_END