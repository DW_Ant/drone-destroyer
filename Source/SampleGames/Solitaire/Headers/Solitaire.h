/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Solitaire.h
  This application allows the user to play Solitaire.

  This game's primary purpose is to test the Graphics Engine rendering pipeline in
  particular on draw layers and draw order.
=====================================================================
*/

#pragma once

#include "Definitions.h"

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\SfmlGraphics\Headers\SfmlGraphicsClasses.h"
#include "Engine\Input\Headers\InputClasses.h"
#include "Engine\Localization\Headers\LocalizationClasses.h"
#include "Engine\Gui\Headers\GuiClasses.h"
#include "Engine\Random\Headers\RandomClasses.h"

//Solitaire namespace is within the SD namespace
#define SOLITAIRE_BEGIN SD_BEGIN namespace Solitaire {
#define SOLITAIRE_END } SD_END

#define TICK_GROUP_SOLITAIRE "Solitaire"
#define TICK_GROUP_PRIORITY_SOLITAIRE 500000

SOLITAIRE_BEGIN
extern LogCategory SolitaireLog;
SOLITAIRE_END