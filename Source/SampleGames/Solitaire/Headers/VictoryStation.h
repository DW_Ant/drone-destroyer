/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  VictoryStation.h
  A station where the player must place all cards in the same suit in this station in ascending
  order.  When all victory stations are full, the player has won the game.
=====================================================================
*/

#pragma once

#include "CardStation.h"

SOLITAIRE_BEGIN
class VictoryStation : public CardStation
{
	DECLARE_CLASS(VictoryStation)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void BeginObject () override;
	virtual bool CanAddCard (Card* newCard) const override;
	virtual bool CanAddColumn (CardColumn* newColumn) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this stack contains all cards in the suit.
	 */
	virtual bool HasCompleteSet () const;
};
SOLITAIRE_END