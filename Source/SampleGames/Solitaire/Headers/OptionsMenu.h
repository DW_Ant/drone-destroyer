/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  OptionsMenu.h
  Tiny menu that enables card customization.
=====================================================================
*/

#pragma once

#include "Solitaire.h"

SOLITAIRE_BEGIN
class OptionsMenu : public GuiEntity
{
	DECLARE_CLASS(OptionsMenu)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	Range<FLOAT> MaxAngleRange;
	Range<FLOAT> MaxShiftRange;

	/* Determines the strength behind each increment click. */
	FLOAT IncrementStep;

	/* The original settings when this menu was first created (for restoration if cancelled). */
	FLOAT OriginalMaxAngle;
	FLOAT OriginalMaxShift;

	/* Background behind the menu that darkens everything else, and consumes mouse input. */
	DPointer<FrameComponent> FadeOut;
	DPointer<FrameComponent> Background;
	DPointer<LabelComponent> Title;
	DPointer<ButtonComponent> MaxAngleIncrement;
	DPointer<ButtonComponent> MaxAngleDecrement;
	DPointer<ButtonComponent> MaxShiftIncrement;
	DPointer<ButtonComponent> MaxShiftDecrement;
	DPointer<LabelComponent> MaxAngleText;
	DPointer<LabelComponent> MaxShiftText;
	DPointer<ButtonComponent> AcceptButton;
	DPointer<ButtonComponent> CancelButton;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	
protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	static DString FormatFloat (FLOAT value);
	virtual void RefreshMaxAngleText ();
	virtual void RefreshMaxShiftText ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleMaxAngleIncrement (ButtonComponent* uiComponent);
	virtual void HandleMaxAngleDecrement (ButtonComponent* uiComponent);
	virtual void HandleMaxShiftIncrement (ButtonComponent* uiComponent);
	virtual void HandleMaxShiftDecrement (ButtonComponent* uiComponent);
	virtual void HandleAcceptClicked (ButtonComponent* uiComponent);
	virtual void HandleCancelClicked (ButtonComponent* uiComponent);
};
SOLITAIRE_END