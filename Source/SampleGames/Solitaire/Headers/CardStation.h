/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CardStation.h
  A place where multiple cards may stack.
=====================================================================
*/

#pragma once

#include "Solitaire.h"
#include "ReturnCardInterface.h"

SOLITAIRE_BEGIN
class Card;

class CardStation : public Entity, public PlanarTransform, public ReturnCardInterface
{
	DECLARE_CLASS(CardStation)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const INT STATION_INPUT_PRIORITY;

	/* Callback invoked whenever a face down top card is revealed. */
	SDFunction<void, CardStation*, Card*> OnCardReveal;
	
	/* Callback whenever the player clicked on this station while there aren't any cards on it. */
	SDFunction<void> OnEmptyClicked;

	/* Callback whenever a card is added to this station. */
	SDFunction<void, Card*> OnCardAdded;

protected:
	/* List of cards resting on this station.  The last card is the one on top of the pile. */
	std::vector<Card*> CardStack;

	DPointer<SpriteComponent> StationOutline;
	DPointer<InputComponent> Input;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void ReturnCards (CardColumn* columnToReturn) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void AddCard (Card* newCard);
	virtual void AddColumn (CardColumn* columnToAdd);
	virtual void RemoveTopCard ();
	virtual void RemoveAllCards ();

	/**
	 * Returns true if the given card can be added to this stack.
	 */
	virtual bool CanAddCard (Card* newCard) const;
	virtual bool CanAddColumn (CardColumn* newColumn) const;

	/**
	 * Iterates through the cards in this station to render them in the specified render target.
	 */
	virtual void RenderCardsInStation (RenderTarget* renderTarget, Camera* camera);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns the top card the user may interact with.
	 */
	virtual Card* GetTopCard () const;

	inline SpriteComponent* GetStationOutline () const
	{
		return StationOutline.Get();
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& buttonEvent, sf::Event::EventType eventType);
};
SOLITAIRE_END