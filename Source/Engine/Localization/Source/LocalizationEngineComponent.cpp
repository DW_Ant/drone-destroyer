/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LocalizationEngineComponent.cpp
=====================================================================
*/

#include "LocalizationClasses.h"

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(SD, LocalizationEngineComponent)

const Directory LocalizationEngineComponent::LOCALIZATION_DIRECTORY(Directory::BASE_DIRECTORY / TXT("Localization"));

void LocalizationEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	Translator = TextTranslator::CreateObject();
	if (Translator == nullptr)
	{
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to instantiate a text translator object.  Text will not be translated."));
	}
}

void LocalizationEngineComponent::ShutdownComponent ()
{
	if (Translator != nullptr)
	{
		Translator->Destroy();
		Translator = nullptr;
	}

	Super::ShutdownComponent();
}

TextTranslator* LocalizationEngineComponent::GetTranslator () const
{
	return Translator.Get();
}
SD_END