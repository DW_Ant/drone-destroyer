/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Localization.cpp

  Implements various functions from other modules since the those modules
  (such as Core) also depended some of its functionality to be localized.
=====================================================================
*/

#include "LocalizationClasses.h"

SD_BEGIN
LogCategory LocalizationLog(TXT("Localization"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT);

#if 0
//TODO:  Reenable
bool DString::ToBool () const
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	if (translator == nullptr)
	{
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to convert %s to bool since it was unable to get a handle to the text translator."), ToString());
		return false;
	}

	vector<DString> translations;
	translator->GetTranslations(TXT("TrueText"), translations, TXT("Core"), TXT("DString"));
	if (translations.size() <= 0)
	{
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("String To Bool is not localized to the current language:  %s"), translator->GetSelectedLanguageName());
		return false;
	}

	DString trimmedString(String);
	trimmedString.TrimSpaces();

	//Return true if any of the localized text matches current string
	for (UINT_TYPE i = 0; i < translations.size(); i++)
	{
		if (trimmedString.Compare(translations.at(i), DString::CC_IgnoreCase) == 0)
		{
			return true;
		}
	}

	return false;
}

DString BOOL::ToString () const
{
	TextTranslator* translator = TextTranslator::GetTranslator();
	if (translator == nullptr)
	{
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to convert %s to string since it was unable to get a handle to the text translator."), ToString());
		return (Value) ? TXT("True") : TXT("False");
	}

	std::lock_guard<std::mutex> guard(LocalizationMutex);
	if (translator->GetSelectedLanguage() != LanguageID)
	{
		//Selected language changed.  Update translations
		LanguageID = translator->GetSelectedLanguage();
		ToTrueText = translator->TranslateText(TXT("TBoolToString"), TXT("Core"), TXT("BOOL"));
		ToFalseText = translator->TranslateText(TXT("FBoolToString"), TXT("Core"), TXT("BOOL"));

		if (ToTrueText.IsEmpty() || ToFalseText.IsEmpty())
		{
			LocalizationLog.Log(LogCategory::LL_Warning, TXT("Bool to String is not localized to the current language:  %s"), translator->GetSelectedLanguageName());
			ToTrueText = TXT("True");
			ToFalseText = TXT("False");
		}
	}

	return ((Value) ? ToTrueText : ToFalseText);
}
#endif
SD_END