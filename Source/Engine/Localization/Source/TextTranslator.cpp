/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextTranslator.cpp
=====================================================================
*/

#include "LocalizationClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, TextTranslator, SD, Object)

void TextTranslator::InitProps ()
{
	Super::InitProps();

	SelectedLanguage = English;

	SLanguageMapping newLanguageMapping;
	newLanguageMapping.Language = English;
	newLanguageMapping.TextSuffix = TXT("-ENG");
	newLanguageMapping.FriendlyName = TXT("English");
	LanguageMappings.push_back(newLanguageMapping);

	newLanguageMapping.Language = Debugging;
	newLanguageMapping.TextSuffix = TXT("-XXX");
	newLanguageMapping.FriendlyName = TXT("Debugging");
	LanguageMappings.push_back(newLanguageMapping);
}

void TextTranslator::BeginObject ()
{
	Super::BeginObject();

	DefaultReader = ConfigWriter::CreateObject();
	if (DefaultReader == nullptr)
	{
		Engine::FindEngine()->FatalError(TXT("Failed to initialize TextTranslator.  Unable to create a ConfigWriter for the DefaultReader variable."));
		Destroy();
		return;
	}

	SelectDefaultLanguage();		
#ifndef PROJECT_NAME
	DString fileName = GetFullFileFormat(TXT("TextTranslator"));
#else
	DString fileName = GetFullFileFormat(PROJECT_NAME);
#endif

	DefaultReader->SetReadOnly(true);
	DefaultReader->OpenFile(fileName, false);
	if (!DefaultReader->IsFileOpened())
	{
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Failed to initialize TextTranslator.  Unable to open or find a file named %s"), fileName);
		DefaultReader->Destroy();
		DefaultReader = nullptr;
	}
}

void TextTranslator::Destroyed ()
{
	for (size_t i = 0; i < TranslationFiles.size(); ++i)
	{
		TranslationFiles.at(i)->Destroy();
	}
	ContainerUtils::Empty(TranslationFiles);

	if (DefaultReader != nullptr)
	{
		DefaultReader->Destroy();
		DefaultReader = nullptr;
	}

	Super::Destroyed();
}

TextTranslator* TextTranslator::GetTranslator ()
{
	return LocalizationEngineComponent::Find()->GetTranslator();
}

DString TextTranslator::TranslateText (const DString& localizationKey, const DString& baseFileName, const DString& sectionName) const
{
	DString fileName = GetFullFileFormat(baseFileName);
	ConfigWriter* curFile = GetFileReader(fileName);
	DString result = DString::EmptyString;

	if (curFile == nullptr || !curFile->IsFileOpened())
	{
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to translate \"%s\" since it could not find an available file named \"%s\""), localizationKey, fileName);
	}
	else
	{
		result = curFile->GetPropertyText(sectionName, localizationKey);
	}

	if (result.IsEmpty())
	{
		//Key is not translated for current language.
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to find translated text for key (%s) within section (%s) within file (%s)."), localizationKey, sectionName, baseFileName);
#ifdef DEBUG_MODE
		//Manipulate the resulting string to display the error message since there's a good chance this string will appear on screen in some way.
		result = DString::CreateFormattedString(TXT("Missing Translation in file (%s), section (%s), key (%s)"), baseFileName, sectionName, localizationKey);
#endif
	}

	return result;
}

void TextTranslator::GetTranslations (const DString& localizationKey, std::vector<DString>& outTranslations, const DString& baseFileName, const DString& sectionName) const
{
	DString fileName = GetFullFileFormat(baseFileName);
	ConfigWriter* curFile = GetFileReader(fileName);

	if (curFile == nullptr || !curFile->IsFileOpened())
	{
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to translate \"%s\" since it could not find an available file named \"%s\""), localizationKey, fileName);
	}
	else
	{
		curFile->GetArrayValues<DString>(sectionName, localizationKey, outTranslations);
	}

	if (outTranslations.size() <= 0)
	{
		//Key is not translated for current language.
		LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to find translated text for key (%s) within section (%s) within file (%s)."), localizationKey, sectionName, baseFileName);
	}

	if (curFile != nullptr && curFile != DefaultReader)
	{
		curFile->Destroy();
	}
}

void TextTranslator::SelectLanguage (ELanguages newLanguage)
{
	if (newLanguage == SelectedLanguage)
	{
		return;
	}

	for (ConfigWriter* writer : TranslationFiles)
	{
		writer->Destroy();
	}
	ContainerUtils::Empty(TranslationFiles);

	for (UINT_TYPE i = 0; i < LanguageMappings.size(); i++)
	{
		if (LanguageMappings.at(i).Language == newLanguage)
		{
			SelectedLanguageMappingIdx = i;
			SelectedLanguage = newLanguage;
			return;
		}
	}

	LocalizationLog.Log(LogCategory::LL_Warning, TXT("Unable to set language since there isn't a language mapping associated with specified language."));
}

TextTranslator::ELanguages TextTranslator::GetSelectedLanguage () const
{
	return SelectedLanguage;
}

DString TextTranslator::GetSelectedLanguageName () const
{
	return LanguageMappings[SelectedLanguageMappingIdx].FriendlyName;
}

void TextTranslator::GetLanguageMappings (std::vector<SLanguageMapping>& outLanguageMappings) const
{
	outLanguageMappings = LanguageMappings;
}

TextTranslator::SLanguageMapping TextTranslator::GetSelectedLanguageData () const
{
	if (SelectedLanguageMappingIdx != UINT_INDEX_NONE)
	{
		return LanguageMappings[SelectedLanguageMappingIdx];
	}

	LocalizationLog.Log(LogCategory::LL_Warning, TXT("Failed to get the selected language data since SelectedLanguageMappingIdx variable is not yet set."));
	return SLanguageMapping();
}

const ConfigWriter* TextTranslator::GetDefaultReader () const
{
	return DefaultReader.Get();
}

DString TextTranslator::GetFullFileFormat (const DString& baseFileName) const
{
	if (baseFileName.IsEmpty())
	{
		return LocalizationEngineComponent::LOCALIZATION_DIRECTORY.ReadDirectoryPath() + PROJECT_NAME + GetSelectedLanguageData().TextSuffix + LOCALIZATION_EXT;
	}

	return LocalizationEngineComponent::LOCALIZATION_DIRECTORY.ReadDirectoryPath() + baseFileName + GetSelectedLanguageData().TextSuffix + LOCALIZATION_EXT;
}

void TextTranslator::SelectDefaultLanguage ()
{
	SelectLanguage(DEFAULT_LANGUAGE);
}

ConfigWriter* TextTranslator::GetFileReader (const DString& fullFileName) const
{
	//Use default reader if the file name is not specified
	if (fullFileName == GetFullFileFormat(TXT("")))
	{
		return DefaultReader.Get();
	}

	for (ConfigWriter* writer : TranslationFiles)
	{
		if (writer->GetCurrentFileName().Compare(fullFileName, DString::CC_CaseSensitive) == 0)
		{
			return writer;
		}
	}

	ConfigWriter* newReader = ConfigWriter::CreateObject();
	newReader->SetReadOnly(true);
	newReader->OpenFile(fullFileName, false);
	TranslationFiles.push_back(newReader);

	return newReader;
}
SD_END