/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileUtils.cpp
=====================================================================
*/

#include "FileClasses.h"

SD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(SD, FileUtils, SD, BaseUtils)

void FileUtils::ExtractFileName (const DString& fullAddress, DString& outPath, DString& outFullFileName)
{
	INT idx = fullAddress.ReadString().find_last_of(*Directory::DIRECTORY_SEPARATOR.ToCString());
	if (idx == std::string::npos)
	{
		//No path was given.
		outPath.Clear();
		outFullFileName = fullAddress;
		return;
	}

	outPath = fullAddress.SubString(0, idx);
	outFullFileName = fullAddress.SubString(idx + 1);
}

void FileUtils::ExtractFileExtension (const DString& fullFileName, DString& outFileName, DString& outExtension)
{
	size_t idx = fullFileName.ReadString().find_last_of('.');
	if (idx == std::string::npos)
	{
		//No extension was given.
		outExtension.Clear();
		outFileName = fullFileName;
		return;
	}

	fullFileName.SplitString(idx, outFileName, outExtension);
}

void FileUtils::CopyFileContents (const DString& source, const DString& destination)
{
	//This seems to be the most simplistic way to copy one file to another:  http://stackoverflow.com/questions/10195343/copy-a-file-in-a-sane-safe-and-efficient-way
	std::ifstream sourceStream(source.ToCString());
	std::ofstream destinationStream(destination.ToCString());

	destinationStream << sourceStream.rdbuf();

	sourceStream.close();
	destinationStream.close();
}
SD_END