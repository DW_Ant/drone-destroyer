/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PrimitiveFileAttributes.cpp
=====================================================================
*/

#include "FileClasses.h"

SD_BEGIN
PrimitiveFileAttributes::PrimitiveFileAttributes ()
{
	FileName = DString::EmptyString;
	FileExtension = DString::EmptyString;
}

PrimitiveFileAttributes::PrimitiveFileAttributes (const FilePtr& fileHandle)
{
	DString fullFileName;
	DString pathName;
	OS_GetFileName(fileHandle, OUT pathName, OUT fullFileName);
	Path = pathName;
	FileUtils::ExtractFileExtension(fullFileName, OUT FileName, OUT FileExtension);
}

PrimitiveFileAttributes::PrimitiveFileAttributes (const DString& inPath, const DString& fullFileName)
{
	FileUtils::ExtractFileExtension(fullFileName, FileName, FileExtension);
	Path = inPath;
}

PrimitiveFileAttributes::PrimitiveFileAttributes (const Directory& dir, const DString& fullFileName)
{
	FileUtils::ExtractFileExtension(fullFileName, FileName, FileExtension);
	Path = dir;
}

PrimitiveFileAttributes::PrimitiveFileAttributes (const PrimitiveFileAttributes& attributesCopy)
{
	FileName = attributesCopy.FileName;
	FileExtension = attributesCopy.FileExtension;
	Path = attributesCopy.Path;
}

PrimitiveFileAttributes::PrimitiveFileAttributes (const FileAttributes& attributesCopy)
{
	FileName = attributesCopy.FileName;
	FileExtension = attributesCopy.FileExtension;
	Path = attributesCopy.Path;
}

PrimitiveFileAttributes::~PrimitiveFileAttributes ()
{
	//Noop
}

bool PrimitiveFileAttributes::operator== (const PrimitiveFileAttributes& other) const
{
	//Use absolute paths since there are infinitely many ways to represent a particular path using relative paths.
	return ((Path.GetAbsPath().ReadDirectoryPath() + FileName + FileExtension) == (other.Path.GetAbsPath().ReadDirectoryPath() + other.FileName + other.FileExtension));
}

bool PrimitiveFileAttributes::operator!= (const PrimitiveFileAttributes& other) const
{
	return !(*this == other);
}

DString PrimitiveFileAttributes::ToString () const
{
	return GetName(true, true);
}

void PrimitiveFileAttributes::Serialize (DataBuffer& dataBuffer) const
{
	dataBuffer << Path;

	DString fullFileName = GetName(false, true);
	dataBuffer << fullFileName;
}

void PrimitiveFileAttributes::Deserialize (const DataBuffer& dataBuffer)
{
	DString path;
	dataBuffer >> path;

	DString fullFileName;
	dataBuffer >> fullFileName;

	//This will initialize all other properties in this class and subclasses (due to virtual PopulateFileInfo function).
	SetFileName(path, fullFileName);
}

DString PrimitiveFileAttributes::GetName (bool includePath, bool includeExtension) const
{
	DString results = DString::EmptyString;
	if (includePath)
	{
		results = Path.GetDirectoryPath();
	}

	results += FileName;

	if (includeExtension && !FileName.IsEmpty() && !FileExtension.IsEmpty())
	{
		results += TXT(".") + FileExtension;
	}

	return results;
}

void PrimitiveFileAttributes::ClearValues ()
{
	FileName = DString::EmptyString;
	FileExtension = DString::EmptyString;
	Path = DString::EmptyString;
}

bool PrimitiveFileAttributes::SetFileName (const DString& path, const DString& fullFileName)
{
	FileUtils::ExtractFileExtension(fullFileName, FileName, FileExtension);
	Path = path;
	PopulateFileInfo();

	return OS_CheckIfFileExists(*this);
}

DString PrimitiveFileAttributes::GetFileName () const
{
	return FileName;
}

DString PrimitiveFileAttributes::GetFileExtension () const
{
	return FileExtension;
}

bool PrimitiveFileAttributes::GetFileExists () const
{
	return OS_CheckIfFileExists(*this);
}

void PrimitiveFileAttributes::PopulateFileInfo ()
{
	if (!OS_CheckIfFileExists(Path.ReadDirectoryPath(), GetName(false, true)))
	{
		HandleMissingFile();
	}
}

void PrimitiveFileAttributes::PopulateFileInfoFromHandle (FilePtr fileHandle)
{
	DString fullFileName;
	DString absPath;
	OS_GetFileName(fileHandle, OUT absPath, OUT fullFileName);
	Path = absPath;
	FileUtils::ExtractFileExtension(fullFileName, OUT FileName, OUT FileExtension);
}

void PrimitiveFileAttributes::HandleMissingFile ()
{
	FileLog.Log(LogCategory::LL_Log, FileName + TXT(" does not exist."));
}
SD_END