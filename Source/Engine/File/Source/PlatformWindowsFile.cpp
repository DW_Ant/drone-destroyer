/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlatformWindowsFile.cpp
=====================================================================
*/

#include "FileClasses.h"

#ifdef PLATFORM_WINDOWS

SD_BEGIN
DString OS_FullPath (const DString& relativePath)
{
	TCHAR fullPath[_MAX_PATH];

#if USE_WIDE_STRINGS
	if (_wfullpath(fullPath, relativePath.ToCString(), _MAX_PATH) != NULL)
#else
	if (_fullpath(fullPath, relativePath.ToCString(), _MAX_PATH) != NULL)
#endif
	{
		return DString(fullPath);
	}

#ifdef DEBUG_MODE
	FileLog.Log(LogCategory::LL_Warning, TXT("Unable to find absolute path from the given relativePath (%s)."), relativePath);
#endif
	return DString::EmptyString;
}

bool OS_SetWorkingDirectory (const Directory& newWorkingDirectory)
{
	LPCTSTR winWorkingDirectory = newWorkingDirectory.ReadDirectoryPath().ToCString();

	return (SetCurrentDirectory(winWorkingDirectory) != 0);
}

FilePtr OS_GetFileHandle (const PrimitiveFileAttributes& attributes)
{
	if (!attributes.GetFileExists())
	{
		return nullptr;
	}

	return OS_GetFileHandle(attributes.ReadPath().ReadDirectoryPath(), attributes.GetName(false, true));
}

FilePtr OS_GetFileHandle (const DString& absDirectory, const DString& fullFileName)
{
	// Do I seriously have to call CreateFile to get a handle of an existing file!?

	DString scopedString(absDirectory + fullFileName);
	LPCTSTR lpFileName = (scopedString).ToCString();
	HANDLE fileHandle = CreateFile(lpFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to open file named %s.  Error code:  %s"), scopedString, DString::MakeString(GetLastError()));
		return nullptr;
	}

	return fileHandle;
}

FilePtr OS_CreateFile (const Directory& path, const DString& fullFileName)
{
	DString scopedString(path.GetAbsPath().ReadDirectoryPath() + fullFileName);
	LPCTSTR lpFileName = (scopedString).ToCString();
	HANDLE fileHandle = CreateFile(lpFileName, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to create file named:  %s.  Error code:  %s"), scopedString, DString::MakeString(GetLastError()));
		return nullptr;
	}

	return fileHandle;
}

bool OS_WriteToFile (FilePtr fileHandle, void* values, int numBytes)
{
	DWORD bytesWritten = 0; //not used
	return (WriteFile(fileHandle, values, numBytes, &bytesWritten, NULL) != 0);
}

bool OS_CopyFile (const PrimitiveFileAttributes& fileSource, const Directory& destinationPath, const DString& destName, bool bOverwriteExisting)
{
	if (!fileSource.GetFileExists())
	{
		return false;
	}

	bool bFailIfExist = !bOverwriteExisting;
	//Use MS's implementation to ensure we're not overriding existing file.
	DString scopedStringSrc = fileSource.GetName(true, true);
	DString scopedStringDest = destinationPath.ReadDirectoryPath() + destName;
	LPCTSTR source = scopedStringSrc.ToCString();
	LPCTSTR destination = scopedStringDest.ToCString();
	if (!CopyFile(source, destination, bFailIfExist))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to copy %s to %s.  Error code:  %s"), scopedStringSrc, scopedStringDest, DString::MakeString(GetLastError()));
		return false;
	}

	return true;
}

bool FILE_API OS_DeleteFile (const PrimitiveFileAttributes& target)
{
	return OS_DeleteFile(target.ReadPath().GetAbsPath().ReadDirectoryPath(), target.GetName(false, true));
}

bool OS_DeleteFile (const DString& absPath, const DString& fullFileName)
{
	DString scopedFile(absPath + fullFileName);
	LPCTSTR fullAddress = (scopedFile).ToCString();

	if (DeleteFile(fullAddress))
	{
		FileLog.Log(LogCategory::LL_Log, TXT("Deleted file:  %s"), absPath + fullFileName);
		return true;
	}

	FileLog.Log(LogCategory::LL_Warning, TXT("Attempted to delete file:  %s.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
	return false;
}

void OS_CloseFile (FilePtr& outHandle)
{
	if (outHandle != nullptr)
	{
		CloseHandle(outHandle);
		outHandle = nullptr;
	}
}

bool OS_CheckIfFileExists (const PrimitiveFileAttributes& file)
{
	return OS_CheckIfFileExists(file.ReadPath().GetAbsPath().ReadDirectoryPath(), file.GetName(false, true));
}

bool OS_CheckIfFileExists (const DString& absDirectory, const DString& fullFileName)
{
	if (fullFileName.IsEmpty())
	{
		//Early out.  Also if the directory is given without a file name returns FILE_ATTRIBUTE_DIRECTORY.  This function only cares if a file exists or not.
		return false;
	}

	DString scopedString(absDirectory + fullFileName);
	LPCTSTR microsoftPath = (scopedString).ToCString();
	DWORD fileAttributes = GetFileAttributes(microsoftPath);

	return (fileAttributes != INVALID_FILE_ATTRIBUTES);
}

unsigned long long OS_GetFileSize (const PrimitiveFileAttributes& file)
{
	return OS_GetFileSize(file.ReadPath().GetAbsPath().ReadDirectoryPath(), file.GetName(false, true));
}

unsigned long long OS_GetFileSize (const DString& absPath, const DString& fullFileName)
{
	if (!OS_CheckIfFileExists(absPath, fullFileName))
	{
		return 0;
	}

	HANDLE fileHandle = OS_GetFileHandle(absPath, fullFileName);
	if (fileHandle == nullptr)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to obtain file size for:  %s.  Could not gain a handle to file.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		return 0;
	}

	LARGE_INTEGER fileSize;
	if (!GetFileSizeEx(fileHandle, &fileSize))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to obtain file size for:  %s.  Could not read file size.   Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		CloseHandle(fileHandle);
		return 0;
	}

	OS_CloseFile(fileHandle);

	return fileSize.QuadPart;
}

bool OS_IsFileReadOnly (const PrimitiveFileAttributes& file)
{
	return OS_IsFileReadOnly(file.ReadPath().GetAbsPath().ReadDirectoryPath(), file.GetName(false, true));
}

bool FILE_API OS_IsFileReadOnly (const DString& absPath, const DString& fullFileName)
{
	DString scopedFile(absPath + fullFileName);
	LPCTSTR lpFileName = (scopedFile).ToCString();
	DWORD fileAttributes = GetFileAttributes(lpFileName);

	if (fileAttributes == INVALID_FILE_ATTRIBUTES)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to check if the file (%s) is read only or not.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		return false;
	}

	return ((fileAttributes & FILE_ATTRIBUTE_READONLY) > 0);
}

bool OS_GetFileTimestamps (const PrimitiveFileAttributes& file, DateTime& outCreatedDate, DateTime& outModifiedDate)
{
	return OS_GetFileTimestamps(file.ReadPath().GetAbsPath().ReadDirectoryPath(), file.GetName(false, true), OUT outCreatedDate, OUT outModifiedDate);
}

bool FILE_API OS_GetFileTimestamps (const DString& absPath, const DString& fullFileName, DateTime& outCreatedDate, DateTime& outModifiedDate)
{
	FILETIME createdTime;
	FILETIME accessedTime;
	FILETIME writeTime;
	HANDLE fileHandle = OS_GetFileHandle(absPath, fullFileName);
	if (fileHandle == nullptr)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to get timestamps from %s since GetFileTimestamps could not get a handle to the file."), absPath + fullFileName);
		return false;
	}

	if (!GetFileTime(fileHandle, &createdTime, &accessedTime, &writeTime))
	{
		OS_CloseFile(fileHandle);
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to get timestamps from %s.  System could not get file time.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		return false;
	}

	OS_CloseFile(fileHandle);

	//break apart the file time
	SYSTEMTIME createdSystemTime;
	SYSTEMTIME writeSystemTime;

	if (!FileTimeToSystemTime(&createdTime, &createdSystemTime))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to get timestamps from %s since system could not convert file time to system time.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		return false;
	}

	if (!FileTimeToSystemTime(&writeTime, &writeSystemTime))
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Unable to get timestamps from %s since system could not convert file time to system time.  Error code:  %s"), absPath + fullFileName, DString::MakeString(GetLastError()));
		return false;
	}

	//Convert LPSYSTEMTIME to DateTime
	outCreatedDate.Second = createdSystemTime.wSecond;
	outCreatedDate.Minute = createdSystemTime.wMinute;
	outCreatedDate.Hour = createdSystemTime.wHour;
	outCreatedDate.Day = createdSystemTime.wDay - 1; //SystemTime range from 1-31.  DateTime range from 0-30.
	outCreatedDate.Month = createdSystemTime.wMonth - 1; //SystemTime range from 1-12.  DateTime range from 0-11.
	outCreatedDate.Year = createdSystemTime.wYear;

	outModifiedDate.Second = writeSystemTime.wSecond;
	outModifiedDate.Minute = writeSystemTime.wMinute;
	outModifiedDate.Hour = writeSystemTime.wHour;
	outModifiedDate.Day = writeSystemTime.wDay - 1;
	outModifiedDate.Month = writeSystemTime.wMonth - 1;
	outModifiedDate.Year = createdSystemTime.wYear;

	return true;
}

void OS_GetFileName (FilePtr fileHandle, DString& outAbsPath, DString& outFullFileName)
{
	if (fileHandle == nullptr)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Attempted to get a file name from an invalid file handle."));
		return;
	}

	TCHAR msPath[BUFSIZ];
	DWORD resultLength;
	//resultLength = GetFinalPathNameByHandle(fileHandle, msPath, BUFSIZ, FILE_NAME_NORMALIZED);
	resultLength = GetFinalPathNameByHandle(fileHandle, OUT msPath, BUFSIZ, VOLUME_NAME_NONE);
	if (resultLength >= BUFSIZ)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Could not get file name from a handle.  The required buffer size is:  %s"), DString::MakeString(resultLength));
		return;
	}

	if (resultLength <= 0)
	{
		FileLog.Log(LogCategory::LL_Warning, TXT("Could not get file name from handle.  Error code:  %s"), DString::MakeString(GetLastError())); 
		return;
	}

	DString fullAddress = msPath;
	FileUtils::ExtractFileName(fullAddress, OUT outAbsPath, OUT outFullFileName);
}

DString OS_GetProcessLocation ()
{
	DWORD bufferSize = MAX_PATH;
	TCHAR fileName[MAX_PATH];
	DWORD strLength = GetModuleFileName(NULL, fileName, bufferSize);
		
	return DString(TString(fileName, strLength));
}

bool OS_IsDirectoryReflection (const Directory& dir)
{
	return (dir.ReadDirectoryPath() == TXT(".") || dir.ReadDirectoryPath() == TXT(".."));
}
SD_END

#endif //PLATFORM_WINDOWS