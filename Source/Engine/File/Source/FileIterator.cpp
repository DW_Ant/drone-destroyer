/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileIterator.cpp
=====================================================================
*/

#include "FileClasses.h"

SD_BEGIN
const INT FileIterator::INCLUDE_FILES = 1;
const INT FileIterator::INCLUDE_DIRECTORIES = 2;
const INT FileIterator::ITERATE_SUBDIRECTORIES = 4;
const INT FileIterator::INCLUDE_REFLECTIVE_DIRECTORIES = 8;
const INT FileIterator::INCLUDE_EVERYTHING = INCLUDE_FILES | INCLUDE_DIRECTORIES | ITERATE_SUBDIRECTORIES | INCLUDE_REFLECTIVE_DIRECTORIES;

FileIterator::FileIterator ()
{
	PreviousExpandedDirectory = DString::EmptyString;
	SearchFlags = INCLUDE_FILES | INCLUDE_DIRECTORIES | ITERATE_SUBDIRECTORIES;
	SelectFirstFile();
}

FileIterator::FileIterator (const Directory& newDirectory)
{
	PreviousExpandedDirectory = DString::EmptyString;
	BaseDirectory = newDirectory;
	BaseDirectory.ConvertToAbsPath();
	SearchFlags = INCLUDE_FILES | INCLUDE_DIRECTORIES | ITERATE_SUBDIRECTORIES;
	SelectFirstFile();
}

FileIterator::FileIterator (const Directory& newDirectory, INT searchFlags)
{
	PreviousExpandedDirectory = DString::EmptyString;
	BaseDirectory = newDirectory;
	BaseDirectory.ConvertToAbsPath();
	SearchFlags = searchFlags;
	SelectFirstFile();
}

FileIterator::FileIterator (const FileIterator& otherFileIterator)
{
	PreviousExpandedDirectory = DString::EmptyString;
	BaseDirectory = otherFileIterator.BaseDirectory;
	SearchFlags = otherFileIterator.SearchFlags;
	SelectFirstFile();
}

FileIterator::~FileIterator ()
{
	if (SubIterator != nullptr)
	{
		delete SubIterator;
		SubIterator = nullptr;
	}

#ifdef PLATFORM_WINDOWS
	if (InitialFile != nullptr)
	{
		FindClose(InitialFile);
		InitialFile = nullptr;
	}
#else
	OS_CloseFile(InitialFile);
#endif
}

void FileIterator::operator++ ()
{
	SelectNextFile();
}

void FileIterator::operator++ (int)
{
	SelectNextFile();
}

FileAttributes FileIterator::RetrieveFileAttributes () const
{
	return FileAttributes(GetSelectedAttributes());
}

bool FileIterator::IsDirectorySelected () const
{
	return (SubIterator != nullptr) ? SubIterator->IsDirectorySelected() : !SelectedDirectory.ReadDirectoryPath().IsEmpty();
}

bool FileIterator::FinishedIterating () const
{
	return (SubIterator == nullptr && SelectedDirectory.ReadDirectoryPath().IsEmpty() && !SelectedAttributes.GetFileExists());
}

void FileIterator::SetBaseDirectory (const Directory& newBaseDirectory)
{
	BaseDirectory = newBaseDirectory;
	BaseDirectory.ConvertToAbsPath();
}

PrimitiveFileAttributes FileIterator::GetSelectedAttributes () const
{
	return (SubIterator != nullptr) ? SubIterator->GetSelectedAttributes() : SelectedAttributes;
}

Directory FileIterator::GetSelectedDirectory () const
{
	return (SubIterator != nullptr) ? SubIterator->GetSelectedDirectory() : BaseDirectory;
}

const Directory& FileIterator::ReadSelectedDirectory () const
{
	return (SubIterator != nullptr) ? SubIterator->GetSelectedDirectory() : BaseDirectory;
}

void FileIterator::SelectFirstFile ()
{
	SelectedDirectory = DString::EmptyString;
	SelectedAttributes = PrimitiveFileAttributes();
	SubIterator = nullptr;

#ifdef PLATFORM_WINDOWS
	/**
	Since Windows has a terrible implementation for file iterators (ie:  Inconsistent params and return
	values for FindFirstFile and FindNextFile, and being overly dependant on MS-specific structs and
	flags); it's cleaner to have the iterator handle the file selection than extracting functionality
	to platform independent utility functions, which was implemented earlier (but was messy).
	*/
	//Don't use Directory objects.  It would strip the * and add a trailing slash.  We can't have that for FindFirstFile function.
	DString searchQuery = BaseDirectory.ReadDirectoryPath() + TXT("*");
	InitialFile = FindFirstFile(searchQuery.ToCString(), &WindowsFindData);
	if (InitialFile == INVALID_HANDLE_VALUE)
	{
		return; //Nothing was found
	}

	if (WindowsFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		if ((SearchFlags & INCLUDE_DIRECTORIES) == 0)
		{
			SelectNextFile(); //Skip directories
			return;
		}

		if ((SearchFlags & INCLUDE_REFLECTIVE_DIRECTORIES) == 0 && OS_IsDirectoryReflection(Directory(DString(WindowsFindData.cFileName), false)))
		{
			SelectNextFile(); //Skip reflective directories
			return;
		}

		SelectedDirectory = WindowsFindData.cFileName;
		return;
	}
	else
	{
		if ((SearchFlags & INCLUDE_FILES) == 0)
		{
			SelectNextFile(); //Skip files
			return;
		}

		SelectedAttributes = PrimitiveFileAttributes(InitialFile);
	}
#else
#error Please implement SelectFirstFile for your platform.
#endif
}

void FileIterator::SelectNextFile ()
{ 
	if (SubIterator != nullptr)
	{
		SubIterator->SelectNextFile();
		if (!SubIterator->FinishedIterating())
		{
			return;
		}

		//finished iterating through sub directory.  Carry on with this iterator
		delete SubIterator;
		SubIterator = nullptr;
	}

	//Check if it needs to search sub directory.  This is implemented outside of loop to have the parent directory listed before subdirectories.
	if ((SearchFlags & ITERATE_SUBDIRECTORIES) > 0 && !SelectedDirectory.ReadDirectoryPath().IsEmpty() && SelectedDirectory != PreviousExpandedDirectory && !OS_IsDirectoryReflection(SelectedDirectory))
	{
		//File iterator found a directory that wasn't investigated yet.
		SubIterator = new FileIterator(BaseDirectory / SelectedDirectory, SearchFlags);
		PreviousExpandedDirectory = SelectedDirectory;
		return; //Using sub iterator until it finishes
	}

#ifdef PLATFORM_WINDOWS
	if (FindNextFile(InitialFile, &WindowsFindData))
	{
		if (WindowsFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if ((SearchFlags & INCLUDE_DIRECTORIES) == 0)
			{
				if ((SearchFlags & ITERATE_SUBDIRECTORIES) > 0 && !OS_IsDirectoryReflection(Directory(DString(WindowsFindData.cFileName), false)))
				{
					SubIterator = new FileIterator(BaseDirectory / WindowsFindData.cFileName, SearchFlags);
					return;
				}

				SelectNextFile(); //Skip directories
				return;
			}

			if ((SearchFlags & INCLUDE_REFLECTIVE_DIRECTORIES) == 0 && OS_IsDirectoryReflection(Directory(DString(WindowsFindData.cFileName), false)))
			{
				SelectNextFile(); //Skip reflective directories
				return;
			}

			SelectedAttributes = PrimitiveFileAttributes();
			SelectedDirectory = WindowsFindData.cFileName;
		}
		else
		{
			if ((SearchFlags & INCLUDE_FILES) == 0)
			{
				SelectNextFile(); //Skip files
				return;
			}

			SelectedAttributes = PrimitiveFileAttributes(BaseDirectory, WindowsFindData.cFileName);
			SelectedDirectory = DString::EmptyString;
		}
	}
	else //Nothing is found
	{
		SelectedDirectory = DString::EmptyString;
		SelectedAttributes = PrimitiveFileAttributes();
	}
#else
	#error Please implement SelectNextFile for your platform.
#endif
}
SD_END