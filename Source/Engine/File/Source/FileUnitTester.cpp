/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileUnitTester.cpp
=====================================================================
*/

#include "FileClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
const Directory FileUnitTester::UnitTestLocation(Directory::BASE_DIRECTORY.ReadDirectoryPath() + TXT("/Source/Engine/File/UnitTest/"), true);

IMPLEMENT_CLASS(SD, FileUnitTester, SD, UnitTester)

bool FileUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		return (TestDirectories(testFlags) && TestOSCalls(testFlags) && TestFileAttributes(testFlags) && TestFileIterator(testFlags) && TestFileWriter(testFlags) && TestConfig(testFlags));
	}

	return true;
}

bool FileUnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
{
	if (!Super::MetRequirements(completedTests))
	{
		return false;
	}

	//The FileUnitTester depends on the TimeUnitTester
	for (UINT_TYPE i = 0; i < completedTests.size(); i++)
	{
		if (completedTests.at(i) == TimeUnitTester::SStaticClass()->GetDefaultObject())
		{
			return true;
		}
	}

	//TimeUnitTester is not completed yet.  Try again later.
	return false;
}

bool FileUnitTester::TestDirectories (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Directories"));

	TestLog(testFlags, TXT("The engine's base directory is:  %s."), Directory::BASE_DIRECTORY);
	TestLog(testFlags, TXT("The engine's binary directory is:  %s."), Directory::BINARY_DIRECTORY);
	TestLog(testFlags, TXT("The engine's document directory is:  %s."), Directory::DOCUMENT_DIRECTORY);
	TestLog(testFlags, TXT("The engine's source directory is:  %s."), Directory::SOURCE_DIRECTORY);
	TestLog(testFlags, TXT("The current working directory is:  %s."), Directory::GetWorkingDirectory());

	SetTestCategory(testFlags, TXT("Fundamental Operations"));
	{
		const DString subFolder(TXT("TestSubDir"));
		Directory testDir(subFolder, true);
		TestLog(testFlags, TXT("Adding a subfolder %s relative from the current working directory.  Resulting directory object is:  %s"), subFolder);
		testDir.ConvertToAbsPath();
		TestLog(testFlags, TXT("Converting the directory to absolute path:  %s"), testDir);
		if (testDir.ToString().Find(Directory::GetWorkingDirectory().ReadDirectoryPath(), 0, DString::CC_CaseSensitive) < 0)
		{
			UnitTestError(testFlags, TXT("Appending subfolder to current working directory failed since the current working directory is not found in resulting directory object converted to abs path:  %s"), {testDir.ToString()});
			return false;
		}

		Directory copyDir(testDir);
		if (copyDir != testDir)
		{
			UnitTestError(testFlags, TXT("Directory test failed since creating a copy of %s does not create a duplicate object.  The directory copy is %s"), {testDir.ToString(), copyDir.ToString()});
			return false;
		}

		Directory workingDir(Directory::GetWorkingDirectory());
		workingDir.ConvertToAbsPath();
		if (testDir == Directory(Directory::GetWorkingDirectory()))
		{
			UnitTestError(testFlags, TXT("Directory test failed since the working directory + subfolder (%s) is the same as simply the working directory (%s)."), {testDir.ToString(), workingDir.ToString()});
			return false;
		}

		Directory absPathTest(TXT("SubDir/InnerMostDir"), true);
		if (absPathTest.IsAbsPath())
		{
			UnitTestError(testFlags, TXT("Directory test failed since it believes %s is an absolute path."), {absPathTest.ToString()});
			return false;
		}

		absPathTest.ConvertToAbsPath();
		if (!absPathTest.IsAbsPath())
		{
			UnitTestError(testFlags, TXT("Directory test failed since it believes %s is a relative path."), {absPathTest.ToString()});
			return false;
		}

		TestLog(testFlags, TXT("Retriving inner-most directory."));
		const DString expectedInnerDir(TXT("InnerDir"));
		Directory innerDirTest(DString(TXT("SomeDir")) / TXT("MiddleDir") / expectedInnerDir);
		if (innerDirTest.GetInnerMostDirectory() != expectedInnerDir)
		{
			UnitTestError(testFlags, TXT("Directory test failed since getting the inner most directory of %s returned %s instead of %s."), {innerDirTest.ToString(), innerDirTest.GetInnerMostDirectory(), expectedInnerDir});
			return false;
		}

#ifdef PLATFORM_WINDOWS
		TestLog(testFlags, TXT("Retrieving drive letter."));
		Directory driveLetterTest(DString(TXT("D:")) / TXT("BaseDir") / TXT("SubDir") / TXT("InnerDir"));
		const DString expectedDriveLetter = TXT("D");

		if (driveLetterTest.GetDriveLetter() != expectedDriveLetter)
		{
			UnitTestError(testFlags, TXT("Directory test failed since it was not able to properly pull the drive letter from %s.  It pulled %s instead of %s."), {driveLetterTest.ToString(), driveLetterTest.GetDriveLetter(), expectedDriveLetter});
			return false;
		}
#endif
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Interface to Vectors"));
	{
		const std::vector<DString> folderList({TXT("Base Dir"), TXT("Outer Dir"), TXT("Branch Dir"), TXT(".."), TXT("Some other Dir"), TXT("Inner Dir")});
		Directory appendedDir(folderList.at(0), true);
		DString expectedList = folderList.at(0) + Directory::DIRECTORY_SEPARATOR;
		for (UINT_TYPE i = 1; i < folderList.size(); ++i)
		{
			TestLog(testFlags, TXT("Appending %s to %s"), folderList.at(i), appendedDir);

			appendedDir /= folderList.at(i);
			expectedList += folderList.at(i) + Directory::DIRECTORY_SEPARATOR;

			if (appendedDir.ToString() != expectedList)
			{
				UnitTestError(testFlags, TXT("Directory test failed since appending %s to the directory list should have resulted in %s.  Instead it's %s."), {folderList.at(i), expectedList, appendedDir.ToString()});
				return false;
			}
		}

		TestLog(testFlags, TXT("Resulting directory list is %s"), appendedDir.ToString());
		std::vector<DString> convertedDirList;
		std::vector<DString> expectedDirList;
		expectedList.ParseString(*Directory::DIRECTORY_SEPARATOR.ToCString(), expectedDirList, true);
		convertedDirList = appendedDir.SplitDirectoryToList();
		if (expectedDirList.size() != folderList.size() || convertedDirList.size() != expectedDirList.size())
		{
			UnitTestError(testFlags, TXT("Directory to vector of strings conversion error.  There's a mismatch in vector sizes.  Original list size (%s), expected list size (%s), converted list size (%s)."), {DString::MakeString(folderList.size()), DString::MakeString(expectedDirList.size()), DString::MakeString(convertedDirList.size())});
			return false;
		}

		for (UINT_TYPE i = 0; i < expectedDirList.size(); ++i)
		{
			if (expectedDirList.at(i) != folderList.at(i))
			{
				UnitTestError(testFlags, TXT("Directory test failed since the expected folder list is different from the original folder list.  At index %s, the original folder name is \"%s\" but it's expecting \"%s\"."), {DString::MakeString(i), folderList.at(i), expectedDirList.at(i)});
				return false;
			}

			if (expectedDirList.at(i) != convertedDirList.at(i))
			{
				UnitTestError(testFlags, TXT("Directory test failed since the expected folder list is different from the converted folder list.  At index %s, the expected folder name is \"%s\" but the converted folder name is \"%s\"."), {DString::MakeString(i), expectedDirList.at(i), convertedDirList.at(i)});
				return false;
			}
		}

		TestLog(testFlags, TXT("Successfully converted Directory object (%s) to a list of strings (%s)."), appendedDir, DString::ListToString(convertedDirList));
		Directory convertedDir = Directory::ListToDirectory(convertedDirList);
		TestLog(testFlags, TXT("Converting list (%s) back to Directory object (%s)."), DString::ListToString(convertedDirList), convertedDir);
		if (convertedDir != appendedDir)
		{
			UnitTestError(testFlags, TXT("Directory test failed.  After converting directory (%s) to a list and converting the list back to a directory object, the directory object (%s) is not the same as the original."), {appendedDir.ToString(), convertedDir.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Directory Streaming"));
	{
		Directory dir1(TXT("RelativeDir"), true);
		dir1 /= TXT("SubDir");

		Directory dir2(TXT("SomeAbsPath"), true);
		dir2.ConvertToAbsPath();

		Directory dir3(dir2);
		dir3 /= dir1 / TXT("SubDirWithinAbsPath");

		DataBuffer directoryBuffer;

		directoryBuffer << dir1;
		directoryBuffer << dir2;
		directoryBuffer << dir3;

		Directory readDir1;
		Directory readDir2;
		Directory readDir3;
		directoryBuffer >> readDir1;
		directoryBuffer >> readDir2;
		directoryBuffer >> readDir3;

		if (dir1 != readDir1)
		{
			UnitTestError(testFlags, TXT("Failed to stream Directories to data buffer.  The original directory is \"%s\".  The streamed directory is \"%s\""), {dir1.ToString(), readDir1.ToString()});
			return false;
		}

		if (dir2 != readDir2)
		{
			UnitTestError(testFlags, TXT("Failed to stream Directories to data buffer.  The original directory is \"%s\".  The streamed directory is \"%s\""), {dir2.ToString(), readDir2.ToString()});
			return false;
		}

		if (dir3 != readDir3)
		{
			UnitTestError(testFlags, TXT("Failed to stream Directories to data buffer.  The original directory is \"%s\".  The streamed directory is \"%s\""), {dir3.ToString(), readDir3.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Directory Sanitation"));
	{
		DString input = TXT("\\Something/else\\Testing Directory/Slashes");
		Directory testDir(input, true);
		DString expected = TXT("%sSomething%selse%sTesting Directory%sSlashes%s");
		expected.ReplaceInline(TXT("%s"), Directory::DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);
		if (testDir.ToString() != expected)
		{
			UnitTestError(testFlags, TXT("Directory sanitation test failed.  Directory characters are not normalized to use %s.  Expected:  %s  |  Received:  %s"), {Directory::DIRECTORY_SEPARATOR, expected, testDir.ToString()});
			return false;
		}
		
		input = TXT("/BaseDir:\\Something.Else/Some Other @ Dir : With \"Bad Chars\" \\/..\\/ Unreal > VBS \\ VBS < Unity / Pipe | Star * Confusion ??\\Inner most directory.WithPeriod");
		testDir.SetDirectory(input);
#ifdef PLATFORM_WINDOWS
		expected = TXT("%sBaseDir%sSomething.Else%sSome Other @ Dir  With Bad Chars %s..%s Unreal  VBS %s VBS  Unity %s Pipe  Star  Confusion %sInner most directory.WithPeriod%s");
#else
		expected = TXT("%sBaseDir:%sSomething.Else%sSome Other @ Dir : With \"Bad Chars\" %s..%s Unreal > VBS %s VBS < Unity %s Pipe | Star * Confusion ??%sInner most directory.WithPeriod%s");
#endif
		expected.ReplaceInline(TXT("%s"), Directory::DIRECTORY_SEPARATOR, DString::CC_CaseSensitive);
		if (testDir.ToString() != expected)
		{
			UnitTestError(testFlags, TXT("Directory sanitation test failed.  Directories with invalid characters do not match expected format.  Expected:  %s  |  Received:  %s"), {expected, testDir.ToString()});
			return false;
		}

		//Ensure Window's absolute path format is fine
		input = TXT("D:") + input;
		expected = TXT("D:") + expected;
		testDir.SetDirectory(input);
		if (testDir.ToString() != expected)
		{
			UnitTestError(testFlags, TXT("Directory sanitation test failed.  Directories with invalid characters do not match expected format.  Expected:  %s  |  Received:  %s"), {expected, testDir.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Directories"));
	return true;
}

bool FileUnitTester::TestOSCalls (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("File OS Calls"));

	SetTestCategory(testFlags, TXT("Relative to Absolute Path"));
	TestLog(testFlags, TXT("Testing conversion from relative path to absolute path."));

	Directory absUnitTestLocation(UnitTestLocation);
	absUnitTestLocation.ConvertToAbsPath();

	TestLog(testFlags, TXT("Testing if system can check if a file exists or not."));
	if (!OS_CheckIfFileExists(PrimitiveFileAttributes(absUnitTestLocation, TXT("FileA"))))
	{
		UnitTestError(testFlags, TXT("Check if file test failed.  Could not find FileA within %s.  FileA should exist within that location."), {absUnitTestLocation.ToString()});
		return false;
	}
	TestLog(testFlags, TXT("Unit test was able find the file."));

	DString nonExistingFile(TXT("FileUnitTesterExpectsThisFileNotToExist"));
	TestLog(testFlags, TXT("Testing if system does not get a handle to a nonexisting file:  %s%s"), UnitTestLocation, nonExistingFile);
	if (OS_CheckIfFileExists(PrimitiveFileAttributes(absUnitTestLocation, nonExistingFile)))
	{
		UnitTestError(testFlags, TXT("Check if file does not exist test failed.  Unit test obtained a handle to a file that should not exist:  %s%s"), {absUnitTestLocation.ToString(), nonExistingFile.ToString()});
		return false;
	}
	TestLog(testFlags, TXT("Unit test could not find a non existing file [this is good!]."));

	TestLog(testFlags, TXT("Testing if system could get a file handle."));
	FilePtr fileHandle = OS_GetFileHandle(PrimitiveFileAttributes(absUnitTestLocation, TXT("FileA")));
	if (fileHandle == nullptr)
	{
		UnitTestError(testFlags, TXT("File handle test failed.  Could not obtain a handle to %sFileA."), {absUnitTestLocation.ToString()});
		return false;
	}

	DString handlePath;
	DString handleName;
	OS_GetFileName(fileHandle, handlePath, handleName);
	if (handlePath.IsEmpty())
	{
		OS_CloseFile(fileHandle);
		UnitTestError(testFlags, TXT("File handle test failed.   Handle path to the file 'FileA' is empty."));
		return false;
	}

	if (handleName != TXT("FileA"))
	{
		OS_CloseFile(fileHandle);
		UnitTestError(testFlags, TXT("File handle test failed.  Unable to get file name from handle.  Found name is:  ") + handleName + TXT(".  The expected value is:  FileA."));
		return false;
	}

	OS_CloseFile(fileHandle);
	if (fileHandle != nullptr)
	{
		UnitTestError(testFlags, TXT("File handle test failed.  Was not able to close the file handle to FileA"));
		return false;
	}
	TestLog(testFlags, TXT("System was able to obtain File handles!"));

	TestLog(testFlags, TXT("Testing file size."));
	INT fileSize = static_cast<UINT_TYPE>(OS_GetFileSize(PrimitiveFileAttributes(absUnitTestLocation, TXT("FileA"))));
	if (fileSize != 75)
	{
		UnitTestError(testFlags, TXT("File size test failed.  File size is:  ") + fileSize.ToString() + TXT(" bytes.  The expected value is:  75 bytes."));
		return false;
	}
	TestLog(testFlags, TXT("The file size of FileA is ") + fileSize.ToString() + TXT(" bytes."));

	TestLog(testFlags, TXT("Testing obtaining Read Only attribute."));
	PrimitiveFileAttributes testFile(absUnitTestLocation / TXT("SubDirectoryTest"), TXT("SubFileB"));
	if (!OS_IsFileReadOnly(testFile))
	{
		UnitTestError(testFlags, TXT("Read only test failed.  SubFileB within %s should be read only."), {testFile.GetName(true, true)});
		return false;
	}
	TestLog(testFlags, TXT("System was able to check the Read Only attribute."));

	TestLog(testFlags, TXT("Testing file timestamps."));
	DateTime createdDate;
	DateTime modifiedDate;
	DateTime minDate;
	minDate.Year = 2015;
	OS_GetFileTimestamps(PrimitiveFileAttributes(absUnitTestLocation, TXT("FileA")), createdDate, modifiedDate);
	TestLog(testFlags, TXT("Created date for FileA is:  ") + createdDate.ToString() + TXT("."));
	TestLog(testFlags, TXT("Last modified date for FileA is:  ") + createdDate.ToString() + TXT("."));
	if (createdDate == DateTime() || modifiedDate == DateTime())
	{
		UnitTestError(testFlags, TXT("Timestamp test failed.  Either created date [") + createdDate.ToString() + TXT("] or last modified date [") + modifiedDate.ToString() + TXT("] is empty."));
		return false;
	}

	if (createdDate.Year < minDate.Year || modifiedDate.Year < minDate.Year)
	{
		UnitTestError(testFlags, TXT("Timestamp test failed.  The created date [") + createdDate.ToString() + TXT("] and the last modified date [") + modifiedDate.ToString() + TXT("] must be later than ") + minDate.ToString() + TXT("."));
		return false;
	}

	TestLog(testFlags, TXT("Testing file creation."));
	DString tempFileName = TXT("TemporaryFileTest");
	FilePtr createdFile = OS_CreateFile(absUnitTestLocation, tempFileName);
	if (createdFile == nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to create file named:  %s.  Make sure the file does not already exist."), {absUnitTestLocation.ToString() + tempFileName});
		return false;
	}
	TestLog(testFlags, TXT("System was able to create files."));

	TestLog(testFlags, TXT("Testing file writing."));
	TCHAR* dataBuffer = TXT("The FileUnitTester wrote this data to this file.\n");
	int bufferSize = static_cast<int>(strlen(dataBuffer));
	if (!OS_WriteToFile(createdFile, dataBuffer, bufferSize))
	{
		OS_CloseFile(createdFile);
		UnitTestError(testFlags, TXT("Failed to write ") + DString(dataBuffer) + TXT(" to ") + tempFileName + TXT(".  Make sure application has write access to this file."));
		UnitTestError(testFlags, TXT("You'll need to manually delete %s since the unit test will be attempting to create a new file with the same name."), {absUnitTestLocation.ToString() + tempFileName});
		return false;
	}
	TestLog(testFlags, TXT("Write to file test passed!"));
	OS_CloseFile(createdFile);

	TestLog(testFlags, TXT("Testing file copy."));
	DString fileCopy = tempFileName + TXT("_Copy");
	PrimitiveFileAttributes tempFile(absUnitTestLocation, tempFileName);
	if (!OS_CopyFile(tempFile, absUnitTestLocation, fileCopy, true))
	{
		UnitTestError(testFlags, TXT("File copy test failed.  Failed to copy %s to %s."), {tempFileName, fileCopy});
		return false;
	}

	TestLog(testFlags, TXT("Testing file deletion."));
	if (!OS_DeleteFile(tempFile))
	{
		UnitTestError(testFlags, TXT("Failed to delete %s.  Make sure application has write access to this file."), {tempFile.GetName(true, true)});
		TestLog(testFlags, TXT("You'll need to manually delete this file since the unt test will be attempting to create a new file with the same name."));
		return false;
	}

	if (!OS_DeleteFile(PrimitiveFileAttributes(absUnitTestLocation, fileCopy)))
	{
		UnitTestError(testFlags, TXT("Failed to delete %s%s.  Make sure application has write access to this file."), {absUnitTestLocation.ToString(), fileCopy});
		//No need to notify user to manually delete fileCopy since this file will be replaced next time this unit tester runs.
		return false;
	}
	TestLog(testFlags, TXT("System was able to delete files."));

	ExecuteSuccessSequence(testFlags, TXT("File OS Calls"));
	return true;
}

bool FileUnitTester::TestFileAttributes (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("File Attributes"));

	Directory absUnitTestLocation(UnitTestLocation);
	absUnitTestLocation.ConvertToAbsPath();

	TestLog(testFlags, TXT("Constructing File Attributes from handle."));
	FilePtr fileHandle = OS_GetFileHandle(absUnitTestLocation.ReadDirectoryPath(), TXT("FileA"));
	if (fileHandle == nullptr)
	{
		UnitTestError(testFlags, TXT("File handle constructor test failed.  Unable to get a handle to FileA"));
		return false;
	}

	FileAttributes attributes(fileHandle);
	OS_CloseFile(fileHandle);
	if (attributes.GetFileName() != TXT("FileA"))
	{
		UnitTestError(testFlags, TXT("File handle constructor test failed.  The attributes file name (") + attributes.GetFileName() + TXT(") is not equal to FileA."));
		return false;
	}

	if (attributes.GetFileSize() != 75)
	{
		UnitTestError(testFlags, TXT("File handle constructor test failed.  The attributes describing FileA is ") + attributes.GetFileSize().ToString() + TXT(" bytes.  The expected size is 75 bytes."));
		return false;
	}

	TestLog(testFlags, TXT("Constructing File Attributes from string."));
	FileAttributes stringAttributes(UnitTestLocation, TXT("FileA"));
	if (!stringAttributes.GetFileExists())
	{
		UnitTestError(testFlags, TXT("Constructing file attributes from string test failed.  Unable to locate FileA from %s."), {absUnitTestLocation.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("The created date for FileA is %s.  The last modified date is %s."), stringAttributes.GetCreatedDate(), stringAttributes.GetLastModifiedDate());
	if (stringAttributes.GetCreatedDate().Year < 2015 || stringAttributes.GetLastModifiedDate().Year < 2015)
	{
		UnitTestError(testFlags, TXT("Unable to obtain correct created date for FileA.  The year should at least be 2015.  The created date is [%s]."), {stringAttributes.GetCreatedDate().ToString()});
		TestLog(testFlags, TXT("    The last modified date for FileA reported [%s]."), stringAttributes.GetLastModifiedDate());
		return false;
	}

	if (stringAttributes.GetFileSize() != 75)
	{
		UnitTestError(testFlags, TXT("Constructing file attributes from string test failed.  The attributes describing FileA suggests it's ") + stringAttributes.GetFileSize().ToString() + TXT(" bytes.  The expected file size is 75 bytes."));
		return false;
	}

	PrimitiveFileAttributes fileACopy(stringAttributes);
	if (fileACopy != stringAttributes)
	{
		UnitTestError(testFlags, TXT("After constructing a file attributes using a copy constructor, the file attributes %s is still not equal to %s."), {fileACopy.ToString(), stringAttributes.ToString()});
		return false;
	}

	//Change the extension
	fileACopy.SetFileName(fileACopy.GetPath().GetDirectoryPath(), fileACopy.GetFileName() + TXT(".jnk"));
	if (fileACopy.GetFileExists())
	{
		UnitTestError(testFlags, TXT("After renaming the file attributes to an unknown extension, the file attributes still believes the file exists:  %s"), {fileACopy.ToString()});
		return false;
	}

	if (fileACopy == stringAttributes)
	{
		UnitTestError(testFlags, TXT("After changing the file extension, a file attributes comparison believes that %s is equal to %s."), {fileACopy.ToString(), stringAttributes.ToString()});
		return false;
	}

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		FileAttributes attr(absUnitTestLocation, TXT("FileA"));
		PrimitiveFileAttributes primAttr(absUnitTestLocation, TXT("ConfigTest.ini"));

		//Reversing file attribute classes is intended.  The classes should be agnostic to whichever version was saved to buffer.
		PrimitiveFileAttributes readAttr;
		FileAttributes readPrimAttr;

		DataBuffer fileDataBuffer;

		fileDataBuffer << attr;
		fileDataBuffer << primAttr;
		fileDataBuffer >> readAttr;
		fileDataBuffer >> readPrimAttr;

		if (attr != readAttr)
		{
			UnitTestError(testFlags, TXT("File streaming test failed.  After pushing %s to a data buffer, %s was pulled from that data buffer."), {attr.ToString(), readAttr.ToString()});
			return false;
		}

		if (primAttr != readPrimAttr)
		{
			UnitTestError(testFlags, TXT("File streaming test failed.  After pushing the second file attributes %s to a data buffer, %s was pulled from that data buffer."), {primAttr.ToString(), readPrimAttr.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("File Attributes"));
	return true;
}

bool FileUnitTester::TestFileIterator (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("File Iterator"));

	Directory absUnitTestLocation(UnitTestLocation);
	absUnitTestLocation.ConvertToAbsPath();

	//List all files within the unit test directory that are expected to be captured from the file iterator.
	std::vector<DString> expectedFileNames;
	expectedFileNames.push_back(TXT("FileA"));
	expectedFileNames.push_back(TXT("ReadMe.txt"));
	expectedFileNames.push_back(TXT("SubFileA"));
	expectedFileNames.push_back(TXT("SubFileB"));
	INT fileCounter = 0;

	for (FileIterator fileIter(absUnitTestLocation, FileIterator::INCLUDE_EVERYTHING); !fileIter.FinishedIterating(); fileIter++)
	{
		if (fileIter.IsDirectorySelected())
		{
			TestLog(testFlags, TXT("File iterator found a directory named:  %s"), fileIter.GetSelectedDirectory());
			continue;
		}

		DString curFileName = fileIter.GetSelectedAttributes().GetName(false, true);
		TestLog(testFlags, TXT("File iterator found an file named:  %s"), curFileName);
		fileCounter++;

		for (UINT_TYPE i = 0; i < expectedFileNames.size(); i++)
		{
			if (expectedFileNames.at(i) == curFileName)
			{
				expectedFileNames.erase(expectedFileNames.begin() + i);
				break;
			}
		}
	}

	if (expectedFileNames.size() > 0)
	{
		UnitTestError(testFlags, TXT("File iterator test failed.  Not all expected file names were found.  See the logs for details."));

		TestLog(testFlags, TXT("The file iterator did not find the following files within the %s directory. . ."), absUnitTestLocation.ReadDirectoryPath());
		for (UINT_TYPE i = 0; i < expectedFileNames.size(); i++)
		{
			TestLog(testFlags, TXT("    ") + expectedFileNames.at(i));
		}

		return false;
	}
		
	TestLog(testFlags, TXT("The file iterator found a total of %s files."), fileCounter);

	ExecuteSuccessSequence(testFlags, TXT("File Iterator"));
	return true;
}

bool FileUnitTester::TestFileWriter (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("File Writer"));

	DString fullFileName = UnitTestLocation.ReadDirectoryPath() + TXT("FileWriterTest.txt");
	TextFileWriter* testWriter = TextFileWriter::CreateObject();
	if (testWriter == nullptr)
	{
		UnitTestError(testFlags, TXT("File writer test failed.  Unable to instantiate a TextFileWriter."));
		return false;
	}

	if (!testWriter->OpenFile(fullFileName, true))
	{
		UnitTestError(testFlags, TXT("File writer test failed.  Failed to open file named:  %s"), {fullFileName});
		return false;
	}
		
	std::vector<DString> contentToWrite;
	contentToWrite.push_back(TXT("First Line"));
	contentToWrite.push_back(TXT("Second Line"));
	contentToWrite.push_back(TXT("Third Line"));
	contentToWrite.push_back(TXT(""));
	contentToWrite.push_back(TXT("Last Line"));

	for (UINT_TYPE i = 0; i < contentToWrite.size(); i++)
	{
		TestLog(testFlags, TXT("Writing %s to the file."), contentToWrite.at(i));
		testWriter->AddTextEntry(contentToWrite.at(i));
	}

	if (!testWriter->WriteToFile())
	{
		UnitTestError(testFlags, TXT("File writer test failed.  Failed to flush buffer to the file."));
		return false;
	}

	TestLog(testFlags, TXT("Finished writing to file.  Now reading all text from %s"), fullFileName);
	DString currentLine;
	testWriter->ResetSeekPosition();
	while(testWriter->ReadLine(currentLine))
	{
		TestLog(testFlags, TXT("Recalled \"%s\" from text file."), currentLine);

		for (UINT_TYPE i = 0; i < contentToWrite.size(); i++)
		{
			if (contentToWrite.at(i) == currentLine)
			{
				contentToWrite.erase(contentToWrite.begin() + i);
				break;
			}
		}
	}

	TestLog(testFlags, TXT("Finished reading from file."));

	if (contentToWrite.size() > 0)
	{
		UnitTestError(testFlags, TXT("File writer test failed.  Some of the written text was not recalled when reading from file.  See the logs for details."));
		TestLog(testFlags, TXT("File writer test failed.  The following written text was not recalled when reading file."));
		for (UINT_TYPE i = 0; i < contentToWrite.size(); i++)
		{
			TestLog(testFlags, TXT("    %s"), contentToWrite.at(i));
		}

		return false;
	}

	TestLog(testFlags, TXT("Deleting file writer test file."));
	testWriter->Destroy(); //closes file

	DString path;
	DString fileName;
	FileUtils::ExtractFileName(fullFileName, OUT path, OUT fileName);
	if (!OS_DeleteFile(PrimitiveFileAttributes(path, fileName)))
	{
		UnitTestError(testFlags, TXT("File writer test failed.  Unable to clean up resources.  Attempted to delete file using %s"), {fullFileName});
		return false;
	}

	ExecuteSuccessSequence(testFlags, TXT("File Writer"));
	return true;
}

bool FileUnitTester::TestConfig (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Config"));

	Directory absUnitTestLocation(UnitTestLocation);
	absUnitTestLocation.ConvertToAbsPath();

	DString fileName = absUnitTestLocation.ReadDirectoryPath() + TXT("ConfigTest.ini");
	ConfigWriter* testConfig = ConfigWriter::CreateObject();
	if (testConfig == nullptr)
	{
		UnitTestError(testFlags, TXT("Config test failed.  Unable to instantiate a ConfigWriter."));
		return false;
	}

	if (!OS_CheckIfFileExists(PrimitiveFileAttributes(absUnitTestLocation, TXT("ConfigTest.ini"))))
	{
		//File doesn't exist, restore file using backup
		TestLog(testFlags, TXT("Unable to find ConfigTest.ini.  Attempting to restore unit test config file by copying its backup file."));
		OS_CopyFile(PrimitiveFileAttributes(absUnitTestLocation, TEXT("ConfigTestBackup.ini")), absUnitTestLocation, TEXT("ConfigTest.ini"));

		if (!OS_CheckIfFileExists(PrimitiveFileAttributes(absUnitTestLocation, TXT("ConfigTest.ini"))))
		{
			UnitTestError(testFlags, TXT("Config test failed.  Failed to restore missing file named %s."), {fileName});
			testConfig->Destroy();
			return false;
		}
	}

	if (!testConfig->OpenFile(fileName, false))
	{
		UnitTestError(testFlags, TXT("Config test failed.  Failed to open file named:  %s"), {fileName});
		testConfig->Destroy();
		return false;
	}
		
	TestLog(testFlags, TXT("Begin reading from config test."));

	DString unitTestSection = TXT("UnitTest");
	if (!testConfig->ContainsSection(unitTestSection))
	{
		UnitTestError(testFlags, TXT("Config test failed.  The test config writer does not have the %s section defined."), {unitTestSection});
		return false;
	}

	DString falseSectionName = TXT("FalseSectionName");
	if (testConfig->ContainsSection(falseSectionName))
	{
		UnitTestError(testFlags, TXT("Config test failed.  The test config contains a section named %s when it should not have that section."), {falseSectionName});
		return false;
	}

	DString oldTimeStampStr = testConfig->GetProperty<DString>(unitTestSection, TXT("FullTimeStamp"));
	if (oldTimeStampStr.IsEmpty())
	{
		UnitTestError(testFlags, TXT("Config test failed.  Unable to get FullTimeStamp property from %s section within %s file."), {unitTestSection, fileName});
		testConfig->Destroy();
		return false;
	}

	//Retrieve the same variable without specifying a section
	DString oldTimeStampStrNoSection = testConfig->GetProperty<DString>(TXT(""), TXT("FullTimeStamp"));
	if (oldTimeStampStrNoSection == oldTimeStampStr)
	{
		UnitTestError(testFlags, TXT("Config test failed.  Retrieving the FullTimeStamp property without specifying a section within %s file should have failed to retrieve that property."), {fileName});
		testConfig->Destroy();
		return false;
	}

	//Retrieving a property value that's not within a section
	DString sectionlessValue = testConfig->GetProperty<DString>(TXT(""), TXT("PropertyWithoutSection"));
	if (sectionlessValue != TXT("Some Value"))
	{
		UnitTestError(testFlags, TXT("Config test failed.  The expected value of PropertyWithoutSection is \"Some Value\" from config file %s.  Instead it returned \"%s\"."), {fileName, sectionlessValue});
		testConfig->Destroy();
		return false;
	}
	TestLog(testFlags, TXT("Successfully extracted FullTimeStamp from %s section.  The value is:  %s"), unitTestSection, oldTimeStampStr);

	DateTime oldTimeStamp;
	oldTimeStamp.Year = testConfig->GetProperty<INT>(unitTestSection, TXT("Year"));
	oldTimeStamp.Month = testConfig->GetProperty<INT>(unitTestSection, TXT("Month"));
	oldTimeStamp.Day = testConfig->GetProperty<INT>(unitTestSection, TXT("Day"));
	oldTimeStamp.Hour = testConfig->GetProperty<INT>(unitTestSection, TXT("Hour"));
	oldTimeStamp.Minute = testConfig->GetProperty<INT>(unitTestSection, TXT("Minute"));
	oldTimeStamp.Second = testConfig->GetProperty<INT>(unitTestSection, TXT("Second"));
	TestLog(testFlags, TXT("Extracted the following INTs from Config:  Year=%s, Month=%s, Day=%s, Hour=%s, Minute=%s, Second=%s."), oldTimeStamp.Year, oldTimeStamp.Month, oldTimeStamp.Day, oldTimeStamp.Hour, oldTimeStamp.Minute, oldTimeStamp.Second);
	if (oldTimeStamp.CalculateTotalSeconds() <= 0.f)
	{
		UnitTestError(testFlags, TXT("Config test failed.  The time stamp from %s is not positive (%s seconds).  The unit test should have been able to extract the previous unit time's time stamp."), {oldTimeStamp.ToString(), oldTimeStamp.CalculateTotalSeconds().ToString()});
		testConfig->Destroy();
		return false;
	}

	if (oldTimeStamp.ToString() != oldTimeStampStr)
	{
		UnitTestError(testFlags, TXT("Config test failed.  The components of the time stamp in config should have been equal to the string representation.  Instead the components string representation is %s.  The string representation in config is %s."), {oldTimeStamp.ToString(), oldTimeStampStr.ToString()});
		testConfig->Destroy();
		return false;
	}
	TestLog(testFlags, TXT("Reading variables from config successful.  The components of the config's time stamp's string representation is equal to the string representation in config.  %s"), oldTimeStamp);

	DateTime current;
	current.SetToCurrentTime();
	TestLog(testFlags, TXT("Testing saving data to config.  Updating time stamp variables to current time:  %s"), current);

	testConfig->SaveProperty<DString>(unitTestSection, TXT("FullTimeStamp"), current.ToString());
	testConfig->SaveProperty<INT>(unitTestSection, TXT("Year"), current.Year);
	testConfig->SaveProperty<INT>(unitTestSection, TXT("Month"), current.Month);
	testConfig->SaveProperty<INT>(unitTestSection, TXT("Day"), current.Day);
	testConfig->SaveProperty<INT>(unitTestSection, TXT("Hour"), current.Hour);
	testConfig->SaveProperty<INT>(unitTestSection, TXT("Minute"), current.Minute);
	testConfig->SaveProperty<INT>(unitTestSection, TXT("Second"), current.Second);
	testConfig->SaveConfig();

#if 0
	//This was removed since this will most likely cause a sharing violation.
	//Check if data was actually saved to the file (not just in buffer)
	if (OS_GetFileSize(UnitTestLocation, TXT("ConfigTest.ini")) <= 0)
	{
		UnitTestError(testFlags, TXT("Save config test failed.  %s file size is zero after calling SaveConfig()."), {fileName});
		testConfig->Destroy();
		return false;
	}
#endif

	TestLog(testFlags, TXT("Retrieving newly saved data from config."));

	DString currentReaderStr = testConfig->GetProperty<DString>(unitTestSection, TXT("FullTimeStamp"));
	if (currentReaderStr != current.ToString())
	{
		UnitTestError(testFlags, TXT("Config test failed.  The retrieved value of FullTimeStamp should be equal to %s.  Instead it returned %s."), {current.ToString(), currentReaderStr});
		return false;
	}

	DateTime currentReader;
	currentReader.Year = testConfig->GetProperty<INT>(unitTestSection, TXT("Year"));
	currentReader.Month = testConfig->GetProperty<INT>(unitTestSection, TXT("Month"));
	currentReader.Day = testConfig->GetProperty<INT>(unitTestSection, TXT("Day"));
	currentReader.Hour = testConfig->GetProperty<INT>(unitTestSection, TXT("Hour"));
	currentReader.Minute = testConfig->GetProperty<INT>(unitTestSection, TXT("Minute"));
	currentReader.Second = testConfig->GetProperty<INT>(unitTestSection, TXT("Second"));

	if (currentReader != current)
	{
		UnitTestError(testFlags, TXT("Config test failed.  The retrieved DateTime should be equal to %s.  Instead it retrieved %s."), {current.ToString(), currentReader.ToString()});
		testConfig->Destroy();
		return false;
	}
	TestLog(testFlags, TXT("Saving data to config test passed!  Was able to retrieve the current time from config:  %s"), currentReader);

	SetTestCategory(testFlags, TXT("Multi Line String"));
	{
		const DString expectedMultiLineStringValue(TXT("The value of this string is covered in multiple lines \n\
The line must end with a \\ in order to continue onward to the next string. \n\
\n\
FakeProperty=This line including \"FakeProperty=\" should be included in MultiLineString. \n\
\n\
[FakeSection]"));
		const DString fakeSectionStr = TXT("FakeSection");
		const DString fakePropertyKey = TXT("FakeProperty");

		TestLog(testFlags, TXT("Verifying that the %s section is not created."), fakeSectionStr);
		if (testConfig->ContainsSection(fakeSectionStr))
		{
			UnitTestError(testFlags, TXT("Config test failed. The ConfigWriter object created a section [%s] from a multi-line string. That section should not exist since it's part of a property value."), {fakeSectionStr});
			testConfig->Destroy();
			return false;
		}

		TestLog(testFlags, TXT("Verifying that the %s property key doesn't exist in ConfigWriter."), fakePropertyKey);
		DString fakeValue = testConfig->GetPropertyText(unitTestSection, fakePropertyKey);
		if (!fakeValue.IsEmpty())
		{
			UnitTestError(testFlags, TXT("Config test failed. The ConfigWriter object created a property line %s within the section %s when it shouldn't have since that line is part of a multi line string value."), {fakePropertyKey, unitTestSection});
			testConfig->Destroy();
			return false;
		}

		TestLog(testFlags, TXT("Verifying the contents of the multi line string."));
		DString multiLineStringValue = testConfig->GetPropertyText(unitTestSection, TXT("MultiLineString"));
		if (multiLineStringValue.Compare(expectedMultiLineStringValue, DString::CC_CaseSensitive) != 0)
		{
			FileLog.Log(LogCategory::LL_Critical, TXT("Config unit tester failed. Expected multi line string value:  %s"), expectedMultiLineStringValue);
			FileLog.Log(LogCategory::LL_Critical, TXT("Config unit tester failed. Actual multi line string value:  %s"), multiLineStringValue);
			UnitTestError(testFlags, TXT("Config test failed. The value of the MultiLineString value does not match the expected value. See logs for expected and actual values."));
			testConfig->Destroy();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Config Array"));
	{
		TestLog(testFlags, TXT("Reading the counter from the config array."));
		std::vector<INT> counterKey;
		for (UINT_TYPE i = 1; i < 11; i++)
		{
			counterKey.push_back(INT(i));
		}

		std::vector<INT> counter;
		testConfig->GetArrayValues<INT>(TXT("ArrayTest"), TXT("CounterArray"), counter);

		if (counter.size() != counterKey.size())
		{
			UnitTestError(testFlags, TXT("Config array tests failed.  There's a size mismatch from the expected counter array (size: %s) and the counter array read from config file (size: %s)."), {INT(counterKey.size()).ToString(), INT(counter.size()).ToString()});
			testConfig->Destroy();
			return false;
		}

		for (UINT_TYPE i = 0; i < counter.size(); i++)
		{
			TestLog(testFlags, TXT("    Counter[%s]=%s"), INT(i), counter.at(i));
			if (counter.at(i) != counterKey.at(i))
			{
				UnitTestError(testFlags, TXT("Config array tests failed.  Reading the array from config does not match the expected array.  At index %s, the expected value is %s.  Instead it found %s."), {INT(i).ToString(), counterKey.at(i).ToString(), counter.at(i).ToString()});
				testConfig->Destroy();
				return false;
			}
		}

		//Test saving array
		counter.clear();
		testConfig->SaveArray<INT>(TXT("ArrayTest"), TXT("CounterArray"), counter);

		//Retrieve that array to see if the result is empty
		testConfig->GetArrayValues<INT>(TXT("ArrayTest"), TXT("CounterArray"), counter);
		if (!counter.empty())
		{
			UnitTestError(testFlags, TXT("Config array tests failed.  After clearing the counter array, and updating the config file array with an empty one, retrieving that same array should have also returned an empty array.  Instead the size of the array is %s"), {INT(counter.size()).ToString()});
			testConfig->Destroy();
			return false;
		}

		//Repopulate array
		testConfig->SaveArray<INT>(TXT("ArrayTest"), TXT("CounterArray"), counterKey);
		testConfig->GetArrayValues<INT>(TXT("ArrayTest"), TXT("CounterArray"), counter);

		if (counter.size() != counterKey.size())
		{
			UnitTestError(testFlags, TXT("Config array tests failed.  After updating the array in config file, there's a size mismatch from the expected counter array (size: %s) and the counter array read from config file (size: %s)."), {INT(counterKey.size()).ToString(), INT(counter.size()).ToString()});
			testConfig->Destroy();
			return false;
		}

		TestLog(testFlags, TXT("Rereading the counter array after updating the config file."));
		for (UINT_TYPE i = 0; i < counter.size(); i++)
		{
			TestLog(testFlags, TXT("    Counter[%s]=%s"), INT(i), counter.at(i));
			if (counter.at(i) != counterKey.at(i))
			{
				UnitTestError(testFlags, TXT("Config array tests failed.  After updating the array in config file, reading the array from config does not match the expected array.  At index %s, the expected value is %s.  Instead it found %s."), {INT(i).ToString(), counterKey.at(i).ToString(), counter.at(i).ToString()});
				testConfig->Destroy();
				return false;
			}
		}
		testConfig->Destroy();
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Config"));
	return true;
}
SD_END

#endif