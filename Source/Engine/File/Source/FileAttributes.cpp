/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileAttributes.cpp
=====================================================================
*/

#include "FileClasses.h"

SD_BEGIN
FileAttributes::FileAttributes () : PrimitiveFileAttributes()
{
	bReadOnly = false;
	FileSize = 0;
}

FileAttributes::FileAttributes (const FilePtr& fileHandle) : PrimitiveFileAttributes(fileHandle)
{
	FileSize = OS_GetFileSize(Path.ReadDirectoryPath(), GetName(false, true));
	bReadOnly = OS_IsFileReadOnly(Path.ReadDirectoryPath(), GetName(false, true));
	OS_GetFileTimestamps(Path.ReadDirectoryPath(), GetName(false, true), OUT CreatedDate, OUT LastModifiedDate);
}

FileAttributes::FileAttributes (const DString& relativeDirectory, const DString& fullFileName) : PrimitiveFileAttributes(relativeDirectory, fullFileName)
{
	if (OS_CheckIfFileExists(Path.ReadDirectoryPath(), FileName + TXT(".") + FileExtension))
	{
		FileSize = OS_GetFileSize(Path.ReadDirectoryPath(), GetName(false, true));
		bReadOnly = OS_IsFileReadOnly(Path.ReadDirectoryPath(), GetName(false, true));
		OS_GetFileTimestamps(Path.ReadDirectoryPath(), GetName(false, true), OUT CreatedDate, OUT LastModifiedDate);
	}
}

FileAttributes::FileAttributes (const Directory& dir, const DString& fileName) : PrimitiveFileAttributes(dir, fileName)
{
	if (OS_CheckIfFileExists(Path.ReadDirectoryPath(), FileName + TXT(".") + FileExtension))
	{
		FileSize = OS_GetFileSize(Path.ReadDirectoryPath(), GetName(false, true));
		bReadOnly = OS_IsFileReadOnly(Path.ReadDirectoryPath(), GetName(false, true));
		OS_GetFileTimestamps(Path.ReadDirectoryPath(), GetName(false, true), OUT CreatedDate, OUT LastModifiedDate);
	}
}

FileAttributes::FileAttributes (const PrimitiveFileAttributes& attributesCopy) : PrimitiveFileAttributes(attributesCopy)
{
	bReadOnly = false;
	FileSize = 0;
	PopulateFileInfo();
}

FileAttributes::FileAttributes (const FileAttributes& attributesCopy) : PrimitiveFileAttributes(attributesCopy)
{
	bReadOnly = attributesCopy.bReadOnly;
	FileSize = attributesCopy.FileSize;
	LastModifiedDate = attributesCopy.LastModifiedDate;
	CreatedDate = attributesCopy.CreatedDate;
}

FileAttributes::~FileAttributes ()
{

}

void FileAttributes::ClearValues ()
{
	PrimitiveFileAttributes::ClearValues();

	bReadOnly = false;
	FileSize = 0;
}

void FileAttributes::PopulateFileInfo ()
{
	PrimitiveFileAttributes::PopulateFileInfo();

	if (OS_CheckIfFileExists(Path.ReadDirectoryPath(), GetName(false, true)))
	{
		FileSize = OS_GetFileSize(Path.ReadDirectoryPath(), GetName(false, true));
		bReadOnly = OS_IsFileReadOnly(Path.ReadDirectoryPath(), GetName(false, true));
		OS_GetFileTimestamps(Path.ReadDirectoryPath(), GetName(false, true), OUT CreatedDate, OUT LastModifiedDate);
	}
}

void FileAttributes::PopulateFileInfoFromHandle (FilePtr handle)
{
	PrimitiveFileAttributes::PopulateFileInfoFromHandle(handle);

	FileSize = OS_GetFileSize(Path.ReadDirectoryPath(), GetName(false, true));
	bReadOnly = OS_IsFileReadOnly(Path.ReadDirectoryPath(), GetName(false, true));
	OS_GetFileTimestamps(Path.ReadDirectoryPath(), GetName(false, true), OUT CreatedDate, OUT LastModifiedDate);
}

bool FileAttributes::GetReadOnly () const
{
	return bReadOnly;
}

INT FileAttributes::GetFileSize () const
{
	return INT(static_cast<UINT_TYPE>(FileSize));
}

unsigned long long FileAttributes::GetFileSizeLarge () const
{
	return FileSize;
}

DateTime FileAttributes::GetLastModifiedDate () const
{
	return LastModifiedDate;
}

DateTime FileAttributes::GetCreatedDate () const
{
	return CreatedDate;
}
SD_END