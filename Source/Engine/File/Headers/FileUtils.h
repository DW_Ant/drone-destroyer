/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileUtils.h
  Abstract class containing generic utility functions related to file
  systems.
=====================================================================
*/

#pragma once

#include "File.h"

SD_BEGIN
class FILE_API FileUtils : public BaseUtils
{
	DECLARE_CLASS(FileUtils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Splits the full file name from the path.
	 */
	static void ExtractFileName (const DString& fullAddress, DString& outPath, DString& outFileName);

	/**
	 * Extracts the file name from its extension (the period is trimmed).
	 */
	static void ExtractFileExtension (const DString& fullFileName, DString& outFileName, DString& outExtension);

	/**
	 * Copies the contents of one file to another (regardless if destination already exists or not).
	 * The source and destination expects path and file extensions.
	 */
	static void CopyFileContents (const DString& source, const DString& destination);
};
SD_END