/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileAttributes.h
  Cross-platform file object that contains detailed information about
  a file.
=====================================================================
*/

#pragma once

#include "PrimitiveFileAttributes.h"

SD_BEGIN
class FILE_API FileAttributes : public PrimitiveFileAttributes
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Becomes true if this file is read only. */
	bool bReadOnly = false;

	/* Number of bytes this file contains. */
	unsigned long long FileSize = 0;

	/* Timestamp this file was last modified. */
	DateTime LastModifiedDate;

	/* Timestamp this file was created. */
	DateTime CreatedDate;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	FileAttributes ();
	explicit FileAttributes (const FilePtr& fileHandle);
	explicit FileAttributes (const DString& relativeDirectory, const DString& fileName);
	explicit FileAttributes (const Directory& dir, const DString& fileName);
	FileAttributes (const PrimitiveFileAttributes& attributesCopy);
	FileAttributes (const FileAttributes& attributesCopy);
	virtual ~FileAttributes ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void ClearValues () override;

protected:
	virtual void PopulateFileInfo () override;
	virtual void PopulateFileInfoFromHandle (FilePtr handle) override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool GetReadOnly () const;
	virtual INT GetFileSize () const;
	virtual unsigned long long GetFileSizeLarge () const;
	virtual DateTime GetLastModifiedDate () const;
	virtual DateTime GetCreatedDate () const;
};
SD_END