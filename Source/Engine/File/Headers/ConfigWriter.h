/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ConfigWriter.h
  A Text File Writer that's responsible for serializing configurable properties
  to a specified ini file.  This class is also responsible for retrieving saved
  properties from an ini file.
=====================================================================
*/

#pragma once

#include "TextFileWriter.h"

SD_BEGIN
class FILE_API ConfigWriter : public TextFileWriter
{
	DECLARE_CLASS(ConfigWriter)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SDataSection
	{
	public:
		DString SectionName = DString::EmptyString;

		/* List of each property value in this section. Multiline entries are automatically collapsed into one element, but when they're saved to a file, they will be written to multiple lines.
		 Example:
		 SectionData Element (runtime):  "MyMultiLineString=Line 1\nLine 2"
		 How it appears in file: "MyMultiLineString=Line 1\
		 Line 2
		 */
		std::vector<DString> SectionData;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Character used to signal that the property value will carry on to the next line. */
	static const TCHAR MULTI_LINE_CHARACTER;

protected:
	std::vector<SDataSection> Sections;

	/* List of strings that were manually written to the configuration file before the first property was listed. */
	std::vector<DString> HeaderData;

	/* True if a property changed and this config file needs to be saved. */
	bool bNeedsSave;

private:
	/* Number of lines on the file that makes up the HeaderData. NOTE: This isn't always the vector size of HeaderData since
	multiple lines in the file could be consolidated to one element in the vector for multi lined properties. */
	INT NumLinesInFileHeader;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool OpenFile (const DString& fileName, bool bReplaceFile) override;

protected:
	virtual DString TextToWriteAfterFlush () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Saves all TextEntries (from AddTextEntry function) and properties to the configuration file.
	 */
	virtual void SaveConfig ();

	/**
	 * Saves the string property to the config file within the specified section name.
	 */
	virtual void SavePropertyText (const DString& sectionName, const DString& propertyName, const DString& propertyValue);

	/**
	 * Automatically instantiates a ConfigWriter object, and saves the PropertyText.  The instantiated object is then immediately destroyed.
	 * Returns true if the property successfully saved to the ini file.
	 */
	static bool SavePropertyText (const DString& fileName, const DString& sectionName, const DString& propertyName, const DString& propertyValue);

	/**
	 * Returns the string representation of the property value that's associated with the propertyName within the specified section name.
	 * If section name is not specified, it'll only search through the header data.
	 */
	virtual DString GetPropertyText (const DString& sectionName, const DString& propertyName) const;

	/**
	 * Returns true if this config file contains the specified section regardless if it's empty or not.
	 * SectionName comparison is case insensitive.
	 */
	virtual bool ContainsSection (const DString& sectionName) const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Reads all text and populates it to the Header vector until the first section is encountered.
	 */
	virtual void PopulateHeaderData ();

	/**
	 * Reads the config file and populates the section data as it goes.
	 */
	virtual void PopulateSectionData ();

	/**
	 * Returns true if the current line is a section header format (ie:  "[SectionName]")
	 */
	virtual bool IsSectionHeaderFormat (const DString& text) const;

	/**
	 * Returns true if the given text is a comment.
	 */
	virtual bool IsCommentFormat (const DString& line) const;

	/**
	 * Searches through all sections until the section name matches the parameter name.  Returns the index of that section.
	 */
	virtual UINT_TYPE GetSectionIndex (const DString& sectionIndex) const;

	/**
	 * Searches and returns property value that's associated with the propertyName within the specified section.
	 * PropertyIndex is the index value where the property value resides within the section.
	 */
	virtual DString FindPropertyWithinSection (UINT_TYPE sectionIndex, const DString& propertyName) const;
	virtual DString FindPropertyWithinSection (UINT_TYPE sectionIndex, const DString& propertyName, UINT_TYPE& outPropertyIndex) const;

	/**
	 * Attempts to extract the property value of the given line and records the value to parameter.
	 * Returns false if the given line is not in correct format (ie:  comment), or if the propertyName does not match.
	 */
	virtual bool ReadPropertyValue (const DString& fullLine, const DString& targetPropertyName, DString& outPropertyValue) const;

	/**
	 * Returns true if the given line matches the target property name.
	 * @Param:  outEqualPosIdx is the character index the equal sign is found within fullLine.
	 */
	virtual bool ContainsPropertyValue (const DString& fullLine, const DString& targetPropertyName, INT& outEqualPosIdx) const;


	/*
	=====================
	  Templates
	=====================
	*/

public:
	template <class Type>
	void SaveProperty (const DString& sectionName, const DString& propertyName, Type propertyValue)
	{
		SavePropertyText(sectionName, propertyName, propertyValue.ToString());
	}

	template <class Type>
	Type GetProperty (const DString& sectionName, const DString& propertyName) const
	{
		DString txtResult = GetPropertyText(sectionName, propertyName);
		return Type(txtResult);
	}

	/**
	 * Populates the array with every match of propertyName found within the given section.
	 * If the sectionName is not specified, then it'll populate the array with matches within the header.
	 */
	template <class Type>
	void GetArrayValues (const DString& sectionName, const DString& propertyName, std::vector<Type>& outValues) const
	{
		if (!sectionName.IsEmpty())
		{
			UINT_TYPE sectionIdx = GetSectionIndex(sectionName);

			if (sectionIdx < Sections.size())
			{
				FindArrayWithinSection<Type>(sectionIdx, propertyName, outValues);
			}
		}
		else
		{
			//Populate values from header since section is not specified
			for (UINT_TYPE i = 0; i < HeaderData.size(); i++)
			{
				DString propValue;
				if (ReadPropertyValue(HeaderData.at(i), propertyName, propValue))
				{
					outValues.push_back(Type(propValue));
				}
			}
		}
	}

	/**
	 * Updates the array values of the specified section to the new values.
	 * If sectionName is not found, then it'll apply for the header data.
	 */
	template <class Type>
	void SaveArray (const DString& sectionName, const DString& arrayName, const std::vector<Type>& arrayValues)
	{
		//Index at which the array appears within the section or header section
		UINT_TYPE arrayIdx = UINT_INDEX_NONE;

		if (!sectionName.IsEmpty())
		{
			UINT_TYPE sectionIdx = GetSectionIndex(sectionName);
				
			if (sectionIdx >= Sections.size())
			{
				//Section not found.  Append new section
				sectionIdx = Sections.size();
				SDataSection newSection;
				newSection.SectionName = sectionName;
				Sections.push_back(newSection);
				arrayIdx = 0;
			}
			else
			{
				//Iterate through the current section and clear all property values with matching arrayName
				UINT_TYPE i = 0;
				while (i < Sections.at(sectionIdx).SectionData.size())
				{
					INT equalPos;
					if (ContainsPropertyValue(Sections.at(sectionIdx).SectionData.at(i), arrayName, equalPos))
					{
						if (arrayIdx == UINT_INDEX_NONE)
						{
							arrayIdx = i; //the array should appear here.
						}

						//Clear line
						Sections.at(sectionIdx).SectionData.erase(Sections.at(sectionIdx).SectionData.begin() + i);
						continue;
					}

					i++;
				}
			}

			if (arrayIdx == UINT_INDEX_NONE)
			{
				//Didn't find where the array was previously located.  Append to end
				arrayIdx = Sections.at(sectionIdx).SectionData.size();
			}

			//Add array to this section (traverse backwards to not mess up the array order)
			for (INT i = arrayValues.size() - 1; i >= 0; i--)
			{
				Sections.at(sectionIdx).SectionData.insert(Sections.at(sectionIdx).SectionData.begin() + arrayIdx, arrayName + TXT("=") + arrayValues.at(i.ToUnsignedInt()).ToString());
			}
		}
		else //Update header data
		{
			//Iterate through the header data and clear all property values with matching arrayName
			UINT_TYPE i = 0;
			while (i < HeaderData.size())
			{
				INT equalPos;
				if (ContainsPropertyValue(HeaderData.at(i), arrayName, equalPos))
				{
					if (arrayIdx == UINT_INDEX_NONE)
					{
						arrayIdx = i; //the array should appear here.
					}

					//Clear header line
					HeaderData.erase(HeaderData.begin() + i);
					continue;
				}

				i++;
			}

			if (arrayIdx == UINT_INDEX_NONE)
			{
				//Didn't find where the array was previously located.  Append to end
				arrayIdx = HeaderData.size();
			}

			//Add array to this section (traverse backwards to not mess up the array order)
			for (INT i = arrayValues.size() - 1; i >= 0; i--)
			{
				HeaderData.insert(HeaderData.begin() + arrayIdx, arrayName + TXT("=") + arrayValues.at(i.ToUnsignedInt()).ToString());
			}
		}

		bNeedsSave = true;
	}

protected:
	/**
	 * Appends all matching property values to the array.
	 */
	template <class Type>
	void FindArrayWithinSection (UINT_TYPE sectionIdx, const DString& propertyName, std::vector<Type>& outValues) const
	{
		for (UINT_TYPE i = 0; i < Sections.at(sectionIdx).SectionData.size(); i++)
		{
			DString propValue;
			if (ReadPropertyValue(Sections.at(sectionIdx).SectionData.at(i), propertyName, propValue))
			{
				outValues.push_back(Type(propValue));
			}
		}
	}
};
SD_END