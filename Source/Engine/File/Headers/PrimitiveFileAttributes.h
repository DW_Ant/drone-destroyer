/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PrimitiveFileAttributes.h
  Cross-platform file object that essentially contains basic
  attributes about the OS's file.
=====================================================================
*/

#pragma once

#include "File.h"
#include "Directory.h"

SD_BEGIN
class FileAttributes;

class FILE_API PrimitiveFileAttributes : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Name of the file without the extension. */
	DString FileName;

	/* Location this file resides. Path could either be
	relative to the engine's location or absolute.
	This should be converted to absolute path for faster performance though. */
	Directory Path;

	/* File name extension without the '.' character. */
	DString FileExtension;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	PrimitiveFileAttributes ();
	explicit PrimitiveFileAttributes (const FilePtr& fileHandle);
	explicit PrimitiveFileAttributes (const DString& path, const DString& fullFileName);
	explicit PrimitiveFileAttributes (const Directory& dir, const DString& fullFileName);
	PrimitiveFileAttributes (const PrimitiveFileAttributes& attributesCopy);
	PrimitiveFileAttributes (const FileAttributes& attributesCopy);

	virtual ~PrimitiveFileAttributes ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	bool operator== (const PrimitiveFileAttributes& other) const;
	bool operator!= (const PrimitiveFileAttributes& other) const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual DString ToString () const override;
	virtual void Serialize (DataBuffer& dataBuffer) const override;
	virtual void Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a string representation of this object.
	 */
	virtual DString GetName (bool includePath, bool includeExtension) const;

	/**
	 * Clears all property values for these attributes.
	 */
	virtual void ClearValues ();

	/**
	 * Updates attributes to describe the file that matches the newly specified address.
	 * Returns true if the file exists.
	 */
	virtual bool SetFileName (const DString& path, const DString& fullFileName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual DString GetFileName () const;
	virtual DString GetFileExtension () const;
	inline Directory GetPath () const
	{
		return Path;
	}

	inline const Directory& ReadPath () const
	{
		return Path;
	}

	inline Directory& EditPath ()
	{
		return Path;
	}

	virtual bool GetFileExists () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the target directory to see if the file exists.  If the file exists, then the properties
	 * of this object will be filled accordingly.  This does not create a file.
	 * Returns true if the file exists.
	 */
	virtual void PopulateFileInfo (); 

	/**
	 * Populates property values from the given file pointer.
	 */
	virtual void PopulateFileInfoFromHandle (FilePtr handle);

	/**
	 * Handle that's invoked when it failed to find a file.
	 */
	virtual void HandleMissingFile ();
};
SD_END