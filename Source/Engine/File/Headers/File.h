/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  File.h
  Contains important file includes and definitions for the File module.

  The File module interfaces with the operating system's file and
  directory structures.  This module includes file-related utilities.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Time\Headers\TimeClasses.h"

#if !(MODULE_CORE)
#error The File module requires the Core module.
#endif

#if !(MODULE_TIME)
#error The File module requires the Time module.
#endif

#include <fstream>
#include <sstream>
#include <iterator>

typedef void* FilePtr;

#ifdef PLATFORM_WINDOWS
	#ifdef FILE_EXPORT
		#define FILE_API __declspec(dllexport)
	#else
		#define FILE_API __declspec(dllimport)
	#endif
#else
	#define FILE_API
#endif

//Platform-specific includes
#ifdef PLATFORM_WINDOWS
#include "PlatformWindowsFile.h"
#elif defined(PLATFORM_MAC)
#include "PlatformMacFile.h"
#endif

SD_BEGIN
extern FILE_API LogCategory FileLog;
SD_END