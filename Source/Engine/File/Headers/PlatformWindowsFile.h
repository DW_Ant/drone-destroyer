/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlatformWindowsFile.h
  Utilities containing Windows-specific file system utilities.
=====================================================================
*/

#pragma once

#include "PrimitiveFileAttributes.h"

#ifdef PLATFORM_WINDOWS

//Windows-specific includes related to file structures
#include <direct.h>

#ifdef PLATFORM_64BIT
#define OUTPUT_LOCATION TXT("Win64")
#else
#define OUTPUT_LOCATION TXT("Win32")
#endif

/* Extension used for the owning process.  For Windows, it would be SandDune(.exe) */
#define OUTPUT_EXTENSION TXT(".exe")

SD_BEGIN
/**
 * Converts the relative path to absolute path.  Path is relative to the base directory.
 * A word of caution:  Some operating systems such as Windows does not have a unique name for
 * a full path (meaning, it's not a good idea to use string comparison for directories).
 * 
 * Example:
 * Sometimes it includes the driver and working directory.
 * "test" -> C:\Documents and Settings\user\My Documents\test
 * 
 * Sometimes it includes only the driver
 * "\\test" -> C:\test
 * 
 * And I simply don't have an explanation for this one mentioned in MSDN...
 * "..\\test" -> C:\Documents and Settings\user\test
 * 
 * Returns empty string if there was an error.
 */
DString FILE_API OS_FullPath (const DString& relativePath);

/**
 * Sets this process's current working directory to the specified location.
 * Returns true on success.
 */
bool FILE_API OS_SetWorkingDirectory (const Directory& newWorkingDirectory);

/**
 * Retrieves a handle to a file from attributes.  You must remember to call OS_CloseFile to release resources.
 */
FilePtr FILE_API OS_GetFileHandle (const PrimitiveFileAttributes& attributes);
FilePtr FILE_API OS_GetFileHandle (const DString& absDirectory, const DString& fullFileName);

/**
 * Attempts to create a file, and returns a handle to that file.
 */
FilePtr FILE_API OS_CreateFile (const Directory& path, const DString& fullFileName);

/**
 * Writes data to the file handle.  Returns true on success.
 */
bool FILE_API OS_WriteToFile (FilePtr fileHandle, void* values, int numBytes);

/**
 * Makes a copy from source to destination.  If bOverwriteExisting is true and if there's a file already
 * named destAddress, then it'll replace that file.  Address contains absolute path and the full file name.
 * Returns true on success.
 */
bool FILE_API OS_CopyFile (const PrimitiveFileAttributes& fileSource, const Directory& destinationPath, const DString& destName, bool bOverwriteExisting = false);

/**
 * Attempts to delete a file with the given name.
 * Returns false if it was unable to delete file.
 */
bool FILE_API OS_DeleteFile (const PrimitiveFileAttributes& target);
bool FILE_API OS_DeleteFile (const DString& absPath, const DString& fullFileName);

void FILE_API OS_CloseFile (FilePtr& outHandle);

/**
 * Returns if the given file object points to an existing file.
 */
bool FILE_API OS_CheckIfFileExists (const PrimitiveFileAttributes& file);
bool FILE_API OS_CheckIfFileExists (const DString& absDirectory, const DString& fullFileName);

/**
 * Retrieves the file size in bytes.
 */
unsigned long long FILE_API OS_GetFileSize (const PrimitiveFileAttributes& file);
unsigned long long FILE_API OS_GetFileSize (const DString& absPath, const DString& fullFileName);

/**
 * Returns true if the file is ReadOnly.
 */
bool FILE_API OS_IsFileReadOnly (const PrimitiveFileAttributes& file);
bool FILE_API OS_IsFileReadOnly (const DString& absPath, const DString& fullFileName);

/**
 * Sets the timestamps with the file's creation date and last modified date.
 * Returns true on success.
 */
bool FILE_API OS_GetFileTimestamps (const PrimitiveFileAttributes& file, DateTime& outCreatedDate, DateTime& outModifiedDate);
bool FILE_API OS_GetFileTimestamps (const DString& absPath, const DString& fullFileName, DateTime& outCreatedDate, DateTime& outModifiedDate);

/**
 * Retrieves the file name and directory of a given handle.
 */
void FILE_API OS_GetFileName (FilePtr fileHandle, DString& outAbsPath, DString& outFullFileName);

/**
 * Returns the absolute location the executable location.
 */
DString FILE_API OS_GetProcessLocation ();

/**
 * Returns true if the given characters could represent a parent or current directory.
 * For example:  returns true for '.' (current directory), or '..' (parent directory) for Windows.
 */
bool FILE_API OS_IsDirectoryReflection (const Directory& dir);
SD_END

#endif //PLATFORM_WINDOWS