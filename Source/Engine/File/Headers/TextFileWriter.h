/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFileWriter.h
  The TextFileWriter class is responsible for writing text/strings
  to a file.
=====================================================================
*/

#pragma once

#include "File.h"

SD_BEGIN
class FILE_API TextFileWriter : public Object
{
	DECLARE_CLASS(TextFileWriter)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Buffer that is pending to be written to the file. */
	std::vector<DString> TextBuffer;

	/* Maximmum size of TextBuffer before the text is automatically flushed to the text file. */
	UINT_TYPE BufferLimit;

	std::fstream File;

	/* Name of the file that's currently opened. */
	DString CurrentFile;

	/* If true, then data cannot be written to this file during run time. */
	bool bReadOnly;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual DString GetFriendlyName () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Reassigns the File variable to write to.  It will close the previously opened file if already opened.
	 * If the file already exists, the file will be replaced with this new file if bReplaceFile is true
	 * fileName parameter should include the path.
	 */
	virtual bool OpenFile (const DString& fileName, bool bReplaceFile);

	/**
	 * Adds a new entry to the TextBuffer
	 */
	virtual void AddTextEntry (DString newEntry);

	/**
	 * Writes the entire TextBuffer at the end of the file.  Returns true if successful and clears the TextBuffer.
	 * If it fails, the TextBuffer will clear only if bClearBufferIfFailed is true.
	 */
	virtual bool WriteToFile (bool bClearBufferIfFailed = false);

	/**
	 * Clears all text from the file.
	 */
	virtual bool EmptyFile ();

	/**
	 * Reads the next line from File stream.  Returns true if it found text.  To read all
	 * lines, reset the seek position, and continuously call this function until it returns false.
	 */
	virtual bool ReadLine (DString& outCurrentLine);

	virtual bool IsFileOpened () const;

	virtual void SetBufferLimit (UINT_TYPE newBufferLimit);

	virtual void SetReadOnly (bool bNewReadOnly);


	/*
	=====================
	  Accessors
	=====================
	*/

	virtual UINT_TYPE GetBufferLimit () const;
	virtual DString GetCurrentFileName () const;
	virtual bool GetReadOnly () const;


	/*
	=====================
	  Implementation
	=====================
	*/

	/**
	 * Writes any special characters at the end of each 'WriteToFile' command.
	 */
	virtual DString TextToWriteAfterFlush () const;

	/**
	 * Resets seeking position for ifile stream.
	 */
	virtual void ResetSeekPosition ();
};
SD_END