/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LatentTimerTester.cpp
=====================================================================
*/

#include "TimeClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, LatentTimerTester, SD, Entity)

void LatentTimerTester::InitProps ()
{
	Super::InitProps();

	OwningUnitTester = nullptr;
	NumEventsTriggered = 0;
	MaxNumRepeatedEvents = 3;
	EventInterval = 1.f;
	StartTime = 0.f;
	bFailedTest = false;
	UnitTestStopwatch = nullptr;
}

void LatentTimerTester::Destroyed ()
{
	if (!bFailedTest)
	{
		UnitTester::TestLog(TestFlags, TXT("Timer Unit Test:  Unit test is destroyed properly because its LifeSpanComponent timed out."));
		UnitTester::TestLog(TestFlags, TXT("End timer test."));
		UnitTester::TestLog(TestFlags, TXT(""));
	}

	RemoveStopwatch();

	Super::Destroyed();
}

void LatentTimerTester::LaunchTest (UnitTester::EUnitTestFlags testFlags, const UnitTester* owningUnitTester)
{
	TestFlags = testFlags;
	OwningUnitTester = owningUnitTester;
	UnitTester::TestLog(TestFlags, TXT("Begin timer unit test. . ."));
	UnitTester::TestLog(TestFlags, TXT("Timer Unit Test:  This test should trigger one standard event and %s repeated events within %s second(s)."), MaxNumRepeatedEvents, (MaxNumRepeatedEvents.ToFLOAT() * EventInterval));

	StartTime = Engine::FindEngine()->GetElapsedTime();

	RemoveStopwatch(); //Replace stopwatch if it already existed.
	UnitTestStopwatch = new Stopwatch(TXT("Timer unit test stopwatch"));

	TickComponent* ticker = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (ticker == nullptr)
	{
		RemoveStopwatch();
		OwningUnitTester->UnitTestError(TestFlags, TXT("Failed to initialize LatentTimerTester since it was unable to instantiate a tick component!"));
		bFailedTest = true;
		Destroy();
		return;
	}

	ticker->SetTickHandler(SDFUNCTION_1PARAM(this, LatentTimerTester, HandleTick, void, FLOAT));
	AddComponent(ticker);

	SET_TIMER(this, EventInterval, false, LatentTimerTester::HandleSingleEvent);
	SET_TIMER(this, EventInterval, true, LatentTimerTester::HandleRepeatedEvent);
	SET_TIMER(this, EventInterval, false, LatentTimerTester::HandleFalseEvent);
	REMOVE_TIMER(this, LatentTimerTester::HandleFalseEvent);

	LifeSpanComponent* lifeSpan = LifeSpanComponent::CreateObject();
	if (lifeSpan == nullptr)
	{
		RemoveStopwatch();
		OwningUnitTester->UnitTestError(TestFlags, TXT("Failed to initialize LatentTimerTester since it was unable to instantiate a life span component!"));
		bFailedTest = true;
		Destroy();
		return;
	}

	//Ensure that this unit tester expires one second after the last repeated event
	lifeSpan->LifeSpan = FLOAT(round((MaxNumRepeatedEvents.ToFLOAT() * EventInterval).Value / 1.f)) + 1.f;
	AddComponent(lifeSpan);
}

void LatentTimerTester::VerifyEvents ()
{
	//Should be at most one per repeated event plus one from single event
	if (NumEventsTriggered > MaxNumRepeatedEvents + 1)
	{
		RemoveStopwatch();
		OwningUnitTester->UnitTestError(TestFlags, TXT("Timer Unit Test failed!  Too many events were triggered.\n") + NumEventsTriggered.ToString() + TXT(" > ") + (MaxNumRepeatedEvents + 1).ToString());
		bFailedTest = true;
		Destroy();
	}
}

void LatentTimerTester::RemoveStopwatch ()
{
	if (UnitTestStopwatch != nullptr)
	{
		delete UnitTestStopwatch;
		UnitTestStopwatch = nullptr;
	}
}

bool LatentTimerTester::HandleSingleEvent ()
{
	NumEventsTriggered++;
	UnitTester::TestLog(TestFlags, TXT("Timer Unit Test:  single timer event was called.  Total number of events is now:  %s"), NumEventsTriggered.ToString());
	VerifyEvents();
	return true;
}

bool LatentTimerTester::HandleRepeatedEvent ()
{
	static INT repeatedCounter = 0;

	repeatedCounter++;
	NumEventsTriggered++;

	UnitTester::TestLog(TestFlags, TXT("Timer Unit Test:  repeated timer event was called (%s).  Total number number of events is now:  %s"), repeatedCounter, NumEventsTriggered);
	VerifyEvents();

	if (repeatedCounter >= MaxNumRepeatedEvents)
	{
		UnitTester::TestLog(TestFlags, TXT("Timer Unit Test:  MaxNumRepeatedEvents (%s) reached; stopping repeated timer."), MaxNumRepeatedEvents);
		REMOVE_TIMER(this, LatentTimerTester::HandleRepeatedEvent);
		return false;
	}

	return true;
}

bool LatentTimerTester::HandleFalseEvent ()
{
	RemoveStopwatch();
	OwningUnitTester->UnitTestError(TestFlags, TXT("Timer Unit Test failed!  The false event was still triggered when it should have been removed from the Timer Manager before triggering this event."));
	bFailedTest = true;
	Destroy();
	return false;
}

void LatentTimerTester::HandleTick (FLOAT deltaSec)
{
	// The time elapsed condition must fail twice in a row to handle cases such as (Very large delta times, or the tick component was ticked before timer manager)
	static bool bExpiredPreviously = false;

	//Check if too much time elapsed
	if (bExpiredPreviously && Engine::FindEngine()->GetElapsedTime() - StartTime > (EventInterval * MaxNumRepeatedEvents.ToFLOAT()) + 2) //add 2 seconds to time limit to ensure it's considered after the LifeSpanComponent's expiration time
	{
		RemoveStopwatch();
		OwningUnitTester->UnitTestError(TestFlags, TXT("Timer Unit Test failed!  Not enough events (") + NumEventsTriggered.ToString() + TXT(") were triggered within the time limit.\nThis test should have finished in ") + (EventInterval * MaxNumRepeatedEvents.ToFLOAT()).ToString() + TXT(" seconds."));
		bFailedTest = true;
		Destroy();
	}

	bExpiredPreviously = (Engine::FindEngine()->GetElapsedTime() - StartTime > (EventInterval * MaxNumRepeatedEvents.ToFLOAT()) + 2);
}
SD_END

#endif