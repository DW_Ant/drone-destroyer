/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DateTime.cpp
=====================================================================
*/

#include "TimeClasses.h"

SD_BEGIN
DateTime::DateTime ()
{
	Year = 0;
	Month = 0;
	Day = 0;
	Hour = 0;
	Minute = 0;
	Second = 0;
}

DateTime::DateTime (const INT& inYear, const INT& inMonth, const INT& inDay, const INT& inHour, const INT& inMinute, const INT& inSecond)
{
	Year = inYear;
	Month = inMonth;
	Day = inDay;
	Hour = inHour;
	Minute = inMinute;
	Second = inSecond;
}

DateTime::DateTime (const DateTime& otherDateTime)
{
	Year = otherDateTime.Year;
	Month = otherDateTime.Month;
	Day = otherDateTime.Day;
	Hour = otherDateTime.Hour;
	Minute = otherDateTime.Minute;
	Second = otherDateTime.Second;
}

DateTime::~DateTime ()
{
	
}

void DateTime::operator= (const DateTime& copyDateTime)
{
	Year = copyDateTime.Year;
	Month = copyDateTime.Month;
	Day = copyDateTime.Day;
	Hour = copyDateTime.Hour;
	Minute = copyDateTime.Minute;
	Second = copyDateTime.Second;
}

DString DateTime::ToString () const
{
	//ISO format
	return FormatStringPadded(TXT("%year-%month-%day %hour:%minute:%second"));
}

void DateTime::Serialize (DataBuffer& dataBuffer) const
{
	dataBuffer << Year;
	dataBuffer << Month;
	dataBuffer << Day;
	dataBuffer << Hour;
	dataBuffer << Minute;
	dataBuffer << Second;
}

void DateTime::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> Year;
	dataBuffer >> Month;
	dataBuffer >> Day;
	dataBuffer >> Hour;
	dataBuffer >> Minute;
	dataBuffer >> Second;
}

void DateTime::SetToCurrentTime ()
{
	time_t rawTime = time(0); //Get time from system
	struct tm timeInfo;
	localtime_s(&timeInfo, &rawTime);

	Year = timeInfo.tm_year + 1900; //tm_year starts since 1900
	Month = timeInfo.tm_mon; //Months range from 0-11, no need for an offset here
	Day = timeInfo.tm_mday - 1; //days range from 1-31, offset days by its range offset
	Hour = timeInfo.tm_hour;
	Minute = timeInfo.tm_min;
	Second = timeInfo.tm_sec;
}

DString DateTime::FormatString (const DString& format) const
{
	DString result = format;

	result.ReplaceInline(TXT("%year"), Year.ToString(), DString::CC_IgnoreCase);

	//When formatting date, we start from 1.  January is displayed as 1 (not 0).
	result.ReplaceInline(TXT("%month"), (Month + 1).ToString(), DString::CC_IgnoreCase);

	//When formatting time, we start from 0.  Midnight is displayed as 0 (not 1).
	result.ReplaceInline(TXT("%day"), (Day + 1).ToString(), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%hour"), Hour.ToString(), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%minute"), Minute.ToString(), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%second"), Second.ToString(), DString::CC_IgnoreCase);

	return result;
}

DString DateTime::FormatStringPadded (const DString& format) const
{
	//When formatting date, we start from 1.  January is displayed as 1 (not 0).
	//When formatting time, we start from 0.  Midnight is displayed as 0 (not 1).
	INT dayDisplay = Day + 1;
	INT monthDisplay = Month + 1;
	INT yearDisplay = Year;
	DString result = format;

	result.ReplaceInline(TXT("%year"), yearDisplay.ToFormattedString(4), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%month"), monthDisplay.ToFormattedString(2), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%day"), dayDisplay.ToFormattedString(2), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%hour"), Hour.ToFormattedString(2), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%minute"), Minute.ToFormattedString(2), DString::CC_IgnoreCase);
	result.ReplaceInline(TXT("%second"), Second.ToFormattedString(2), DString::CC_IgnoreCase);

	return result;
}

void DateTime::WrapSeconds ()
{
	INT numMin = Second/60;

	Minute += numMin;
	Second -= numMin * 60;
}

void DateTime::WrapMinutes ()
{
	INT numHours = Minute/60;

	Hour += numHours;
	Minute -= numHours * 60;
}

void DateTime::WrapHours ()
{
	INT numDays = Hour/24;

	Day += numDays;
	Hour -= numDays * 24;
}

void DateTime::WrapDays ()
{
	if (Month < 0 || Month > 11)
	{
		//Must wrap months first to avoid infinite loop (NumDaysWithinMonths expects months to range from 0-11).
		WrapMonths();
	}

	while (true)
	{
		INT daysWithinMonth = TimeUtils::NumDaysWithinMonth(Month, Year);
		if (daysWithinMonth > Day)
		{
			break;
		}

		Day -= daysWithinMonth;
		if (++Month > 11)
		{
			//Wrap months since NumDaysWithinMonth expects Month to range from 0-11
			Month = 0;
			Year++;
		}
	}
}

void DateTime::WrapMonths ()
{
	INT numYears = Month/12;

	Year += numYears;
	Month -= numYears * 12;
}

void DateTime::WrapDateTime ()
{
	WrapSeconds();
	WrapMinutes();
	WrapHours();
	WrapDays();
	WrapMonths();
}

FLOAT DateTime::CalculateTotalSeconds () const
{
	DateTime daysOnly;
	daysOnly.Day = Day;
	daysOnly.Month = Month;
	daysOnly.Year = Year;

	//Short-cut to count total number of days without second, minute, hour precision.
	//CalculateTotalDays will unwrap months and years to consider leap years and various days of the month.
	FLOAT totalDays = daysOnly.CalculateTotalDays();

	return Second.ToFLOAT() + (Minute.ToFLOAT() * 60) + (Hour.ToFLOAT() * 3600) + (totalDays * 86400);
}

FLOAT DateTime::CalculateTotalMinutes () const
{
	DateTime daysOnly;
	daysOnly.Day = Day;
	daysOnly.Month = Month;
	daysOnly.Year = Year;

	//Short-cut to count total number of days without second, minute, hour precision.
	//CalculateTotalDays will unwrap months and years to consider leap years and various days of the month.
	FLOAT totalDays = daysOnly.CalculateTotalDays();

	return (Second.ToFLOAT()/60.f) + Minute.ToFLOAT() + (Hour.ToFLOAT() * 60.f) + (totalDays * 1440.f);
}

FLOAT DateTime::CalculateTotalHours () const
{
	DateTime daysOnly;
	daysOnly.Day = Day;
	daysOnly.Month = Month;
	daysOnly.Year = Year;

	//Short-cut to count total number of days without second, minute, hour precision.
	//CalculateTotalDays will unwrap months and years to consider leap years and various days of the month.
	FLOAT totalDays = daysOnly.CalculateTotalDays();

	return (Second.ToFLOAT()/3600.f) + (Minute.ToFLOAT()/60.f) + Hour.ToFLOAT() + (totalDays * 24.f);
}

FLOAT DateTime::CalculateTotalDays () const
{
	//Unwrap years to handle leap years
	INT yearsRemaining = Year;
	INT totalDays = Day;
	while (yearsRemaining > 0)
	{
		/*
		Note:  When calculating total days, only consider years prior to current year.
		If current year is 2015, only count years from 0-2014.
		*/
		if (TimeUtils::IsLeapYear(yearsRemaining - 1))
		{
			totalDays += 366;
		}
		else
		{
			totalDays += 365;
		}

		yearsRemaining--;
	}

	INT monthsRemaining = Month;
	while (monthsRemaining > 0)
	{
		/*
		note:  When calculating total days, only add months prior to the current month.
		If current Month is March, only add January and February.
		*/
		totalDays += TimeUtils::NumDaysWithinMonth(monthsRemaining - 1, Year);
		monthsRemaining--;
	}

	return (Second.ToFLOAT()/86400.f) + (Minute.ToFLOAT()/1440.f) + (Hour.ToFLOAT()/24.f) + totalDays.ToFLOAT();
}

FLOAT DateTime::CalculateTotalMonths () const
{
	FLOAT daysWithinCurMonth = TimeUtils::NumDaysWithinMonth(Month, Year).ToFLOAT();

	FLOAT result = Second.ToFLOAT()/(86400.f * daysWithinCurMonth);
	result += Minute.ToFLOAT()/(1440.f * daysWithinCurMonth);
	result += Hour.ToFLOAT()/(24.f * daysWithinCurMonth);
	result += Day.ToFLOAT()/daysWithinCurMonth;
	result += Month.ToFLOAT();
	result += Year.ToFLOAT() * 12.f;

	return result;
}

FLOAT DateTime::CalculateTotalYears () const
{
	FLOAT daysWithinCurYear = (TimeUtils::IsLeapYear(Year)) ? 366.f : 365.f;

	FLOAT result = Second.ToFLOAT()/(86400.f * daysWithinCurYear);
	result += Minute.ToFLOAT()/(1440.f * daysWithinCurYear);
	result += Hour.ToFLOAT()/(24.f * daysWithinCurYear);
	result += Day.ToFLOAT() / daysWithinCurYear;
	result += (Month.ToFLOAT()/12.f);
	result += Year.ToFLOAT();

	return result;
}

#pragma region "External Operators"
bool operator== (const DateTime& left, const DateTime& right)
{
	return (left.Second == right.Second && left.Minute == right.Minute && left.Hour == right.Hour &&
			left.Day == right.Day && left.Month == right.Month && left.Year == right.Year);
}

bool operator!= (const DateTime& left, const DateTime& right)
{
	return !(left == right);
}

DateTime operator+ (const DateTime& left, const DateTime& right)
{
	return DateTime(left.Year + right.Year, left.Month + right.Month, left.Day + right.Day, left.Hour + right.Hour, left.Minute + right.Minute, left.Second + right.Second);
}

DateTime& operator+= (DateTime& left, const DateTime& right)
{
	left.Year += right.Year;
	left.Month += right.Month;
	left.Day += right.Day;
	left.Hour += right.Hour;
	left.Minute += right.Minute;
	left.Second += right.Second;
	return left;
}

DateTime operator- (const DateTime& left, const DateTime& right)
{
	return DateTime(left.Year - right.Year, left.Month - right.Month, left.Day - right.Day, left.Hour - right.Hour, left.Minute - right.Minute, left.Second - right.Second);
}

DateTime& operator-= (DateTime& left, const DateTime& right)
{
	left.Year -= right.Year;
	left.Month -= right.Month;
	left.Day -= right.Day;
	left.Hour -= right.Hour;
	left.Minute -= right.Minute;
	left.Second -= right.Second;
	return left;
}

DateTime operator* (const DateTime& left, const DateTime& right)
{
	return DateTime(left.Year * right.Year, left.Month * right.Year, left.Day * right.Day, left.Hour * right.Hour, left.Minute * right.Minute, left.Second * right.Second);
}

DateTime& operator*= (DateTime& left, const DateTime& right)
{
	left.Year *= right.Year;
	left.Month *= right.Month;
	left.Day *= right.Day;
	left.Hour *= right.Hour;
	left.Minute *= right.Minute;
	left.Second *= right.Second;
	return left;
}

DateTime operator/ (const DateTime& left, const DateTime& right)
{
	DateTime result(left);
	result /= right;
	return result;
}

DateTime& operator/= (DateTime& left, const DateTime& right)
{
	if (right.Year != 0)
	{
		left.Year /= right.Year;
	}

	if (right.Hour != 0)
	{
		left.Hour /= right.Hour;
	}

	if (right.Day != 0)
	{
		left.Day /= right.Day;
	}

	if (right.Hour != 0)
	{
		left.Hour /= right.Hour;
	}

	if (right.Minute != 0)
	{
		left.Minute /= right.Minute;
	}

	if (right.Second != 0)
	{
		left.Second /= right.Second;
	}

	return left;
}
#pragma endregion
SD_END