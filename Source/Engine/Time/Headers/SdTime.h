/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SdTime.h
  Contains important file includes and definitions for the SD_Time module.

  The Time module contains utilities and data types related to time and
  date.

  This file was renamed to SdTime.h to avoid name clash with MS's time.h
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"

#if !(MODULE_CORE)
#error The Time module requires the Core module.
#endif

#include <time.h>

#ifdef PLATFORM_WINDOWS
	#ifdef TIME_EXPORT
		#define TIME_API __declspec(dllexport)
	#else
		#define TIME_API __declspec(dllimport)
	#endif
#else
	#define TIME_API
#endif

SD_BEGIN
extern TIME_API LogCategory TimeLog;
SD_END