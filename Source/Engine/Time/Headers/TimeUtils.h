/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TimeUtils.h
  Abstract class containing generic utility functions related to date
  and time.
=====================================================================
*/

#pragma once

#include "SdTime.h"

SD_BEGIN
class TIME_API TimeUtils : public BaseUtils
{
	DECLARE_CLASS(TimeUtils)


	/*
	=====================
	  Data types
	=====================
	*/

public:
	struct SMonthInfo
	{
		/* Number of days for this month (ignoring leap year). */
		INT NumDays;

		/* Short-hand version to write the month. */
		DString MonthNameAbbreviated;

		DString FullMonthName;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	static std::vector<SMonthInfo> MonthInfo;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns number of days within the given month.
	 * If a year is positive, then it considers if February has an additional day or not.
	 */
	static INT NumDaysWithinMonth (INT month, INT year = -1);

	/**
	 * Returns true if the given year is a leap year.
	 */
	static bool IsLeapYear (INT year);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	static void GetMonthInfo (INT monthNumber, SMonthInfo& outMonthInfo);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Populates the month info vector with data.
	 */
	static std::vector<SMonthInfo> PopulateMonthInfo ();
};
SD_END