/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DateTime.h
  Data type responsible for storing time and the date.
=====================================================================
*/

#pragma once

#include "SdTime.h"

SD_BEGIN
class TIME_API DateTime : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Year 0 is 0AD. */
	INT Year;

	/* The actual value range from 0-11 (when wrapped), but it's displayed as Month + 1 from FormatString. */
	INT Month;

	/* The actual value range from 0-30 (when wrapped), but it's displayed as Day + 1 from FormatString. */
	INT Day;
	INT Hour;
	INT Minute;
	INT Second;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	DateTime ();
	DateTime (const INT& inYear, const INT& inMonth, const INT& inDay, const INT& inHour, const INT& inMinute, const INT& inSecond);
	DateTime (const DateTime& otherDateTime);
	virtual ~DateTime ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const DateTime& copyDateTime);


	/*
	=====================
	  Inherited
	=====================
	*/

public:

	/**
	 * Converts data type to string that'll display all values (used mostly for logging).
	 * See FormatString to customize appearance.
	 */
	virtual DString ToString () const override;

	virtual void Serialize (DataBuffer& dataBuffer) const override;
	virtual void Deserialize (const DataBuffer& dataBuffer) override;

		
	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets attributes to current system time.
	 */
	virtual void SetToCurrentTime ();

	/**
	 * Returns a formatted string where string macros are replaced with values.
	 * %year, %month, %day, %hour, %minute, %second
	 */
	virtual DString FormatString (const DString& format) const;

	/**
	 * Same as FormatString, but it pads the values with leading zeroes if the values don't have
	 * enough digits.  Years=4 digits, Months=2, Days=2, Hours=2, Minutes=2, Seconds=2.
	 */
	virtual DString FormatStringPadded (const DString& format) const;

	/**
	 * Increments 1 minute for every 60 seconds.
	 */
	virtual void WrapSeconds ();

	/**
	 * Increments 1 hour for every 60 minutes.
	 */
	virtual void WrapMinutes ();

	/**
	 * Increments 1 day for every 24 hours.
	 */
	virtual void WrapHours ();

	/**
	 * Increments 1 month for every 28-31 days (based on the year and current month).
	 */
	virtual void WrapDays ();

	/**
	 * Increments 1 year for every 12 months.
	 */
	virtual void WrapMonths ();

	/**
	 * Wraps seconds, minutes, hours, days, and months.
	 */
	virtual void WrapDateTime ();

	virtual FLOAT CalculateTotalSeconds () const;
	virtual FLOAT CalculateTotalMinutes () const;
	virtual FLOAT CalculateTotalHours () const;
	virtual FLOAT CalculateTotalDays () const;
	virtual FLOAT CalculateTotalMonths () const;
	virtual FLOAT CalculateTotalYears () const;
};

#pragma region "External Operators"
TIME_API bool operator== (const DateTime& left, const DateTime& right);
TIME_API bool operator!= (const DateTime& left, const DateTime& right);
TIME_API DateTime operator+ (const DateTime& left, const DateTime& right);
TIME_API DateTime& operator+= (DateTime& left, const DateTime& right);
TIME_API DateTime operator- (const DateTime& left, const DateTime& right);
TIME_API DateTime& operator-= (DateTime& left, const DateTime& right);
TIME_API DateTime operator* (const DateTime& left, const DateTime& right);
TIME_API DateTime& operator*= (DateTime& left, const DateTime& right);
TIME_API DateTime operator/ (const DateTime& left, const DateTime& right);
TIME_API DateTime& operator/= (DateTime& left, const DateTime& right);
#pragma endregion
SD_END