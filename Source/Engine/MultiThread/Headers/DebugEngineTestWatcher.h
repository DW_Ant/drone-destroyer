/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DebugEngineTestWatcher.h

  Object that tracks various debugging objects.  Whenever all objects are destroyed, this
  object will shutdown the local (Debugging) engine.
=====================================================================
*/

#pragma once

#include "MultiThread.h"

#ifdef DEBUG_MODE

SD_BEGIN
class MULTI_THREAD_API DebugEngineTestWatcher : public Object
{
	DECLARE_CLASS(DebugEngineTestWatcher)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of objects this object is watching.  Whenever this list clears during garbage collection, this object will shutdown the engine. */
	std::vector<Object*> WatchedObjects;

private:
	/* Becomes true when a watch object was added to the watch list. */
	bool bIsWatching;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Searches through the local Engine's object list to find this instance, and returns the
	 * DebugEngineTestWatcher instance if found.
	 */
	static DebugEngineTestWatcher* FindTestWatcher ();

	/**
	 * Adds the given object to the watch list.  This object will not shutdown the engine until
	 * the given object and all other objects in watch list is destroyed.
	 */
	virtual void AddWatchObject (Object* newWatchObject);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleGarbageCollection ();
};
SD_END

#endif