/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ThreadUtils.h
  A small utility class that defines a few thread-related helper functions.
=====================================================================
*/

#pragma once

#include "MultiThread.h"

SD_BEGIN
class MULTI_THREAD_API ThreadUtils : public BaseUtils
{
	DECLARE_CLASS(ThreadUtils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Attempts to lock the mutex (only if it's available).  If locked, then this function will
	 * execute the function that's passed in.  The mutex is automatically unlocked after the function's
	 * execution.  This function will not block the current thread if the mutex is already locked.
	 * Behavior is undefined if this function is called while the current thread already locked the mutex.
	 *
	 * @param outMutex The object containing the semaphore.  This mutex will be locked during the execution of the lambda.
	 * @param codeToExecute The method to execute if the local thread obtained ownership over the mutex.
	 * @return Returns true if the mutex was locked, and the codeToExecute parameter was executed.
	 */
	static bool TryLock (std::mutex& outMutex, std::function<void()> codeToExecute);
};
SD_END