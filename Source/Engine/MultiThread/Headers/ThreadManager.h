/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ThreadManager.h
  The ThreadManager listens for requests from any ThreadedComponent within the same thread.
  Should there be sufficient amount of requests for data transfer, then the manager will
  halt the current thread, and notify the external engine's ThreadManager to stop their thread.

  When both Engines are frozen, then all pending ThreadedComponents may transfer their data.
=====================================================================
*/

#pragma once

#include "MultiThread.h"

SD_BEGIN
class MultiThreadEngineComponent;
class ThreadedComponent;

class MULTI_THREAD_API ThreadManager
{


	/*
	=====================
	  Data Types
	=====================
	*/

protected:
	/**
	 * Struct containing data related for requests to execute data transfers.
	 */
	struct SPendingRequestData
	{
	public:
		/* The Engine Index of Engine::EngineInstances that will freeze before copying data. */
		UINT_TYPE ExternalEngineIdx;

		/* List of ThreadedComponents that are waiting for both Engines to freeze before transfering data. */
		std::vector<ThreadedComponent*> PendingComponents;

		/* If true, then the manager will freeze both Engines as soon as it can. */
		bool bFreezingEngines;

		/* Maximum number of time remaining before transfering data.  When negative, then this is disabled. */
		FLOAT TimeRemaining;

		SPendingRequestData ()
		{
			ExternalEngineIdx = UINT_INDEX_NONE;
			bFreezingEngines = false;
			TimeRemaining = -1.f;
		}

		/**
		 * Iterates through each PendingComponent to invoke their CopyMethods.
		 * Clears pending activity after execution.
		 */
		void FlushPendingComponents ();

		/**
		 * Returns true if the conditions are sufficient enough to lock both threads.
		 * This method will decrement time remaining if enabled.
		 *
		 * @param manager The local manager that owns this thread request.
		 * @param deltaSec Time elapsed since last check if it should lock this thread.
		 */
		bool ShouldLockThreads (ThreadManager* manager, FLOAT deltaSec);
	};

	/**
	 * Struct containing data related to a ThreadedComponent instantiation request.
	 */
	struct SComponentInstantiationData
	{
		/* External component the newly instanced local component will link to. */
		ThreadedComponent* ExternalRequester;

		/* Method used to instantiate the local component.  The function returns the newly instantiated object. */
		std::function<ThreadedComponent*()> InstantiationMethod;

		SComponentInstantiationData ()
		{
			ExternalRequester = nullptr;
			InstantiationMethod = nullptr;
		}

		SComponentInstantiationData (ThreadedComponent* inExternalRequester, std::function<ThreadedComponent*()> inInstantiationMethod)
		{
			ExternalRequester = inExternalRequester;
			InstantiationMethod = inInstantiationMethod;
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Determines how often does the ThreadManager check the NumFreezeRequests variable while
	waiting for other threads to transfer data.  This is in milliseconds. */
	static const INT FreezeCheckFrequency;

	/* Maximum number of thread requests for a single Engine that are allowed to queue up before
	the ThreadManager forces the engine lock.
	Requests from external Engines to lock the local engine are excluded.
	If not positive, then the ThreadManager will not automatically flush requests based on number of requests. */
	INT MaxNumRequests;

	/* List of requests to instantiate ThreadedComponents on this thread.
	A single request will force an engine freeze as soon as possible.
	This vector is referenced in multiple threads that maybe outside the freezing framework.  Lock its associated mutex when accessing. */
	std::vector<SComponentInstantiationData> ComponentInstantiationRequests;

	/* Mutex for accessing ComponentInstantiationRequests. */
	std::mutex ComponentInstantiationRequestMutex;

	/* List of Requests of ThreadedComponents waiting for an external Engine to freeze.
	Each struct must point to an external Engine.  Each ExternalEngineIdx must be unique. */
	std::vector<SPendingRequestData> PendingRequestData;

	/* Increments everytime a ThreadManager from an external thread requests the local Engine to freeze.
	The local ThreadManager will then halt the current thread to allow the external manager to copy data.  The
	external manager will then decrement this value when it's finished.  The local thread manager will
	resume local thread execution when this value reaches zero. */
	INT NumExternalFreezeRequests;

	/* Mutex used to access NumExternalFreezeRequest variable. */
	std::mutex FreezeRequestMutex;

	/* Becomes true if the local thread is frozen, and it's safe to copy data to this thread. */
	bool bThreadFrozen;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ThreadManager ();
	virtual ~ThreadManager ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds a callback to be invoked on the external thread where the function will instantiate
	 * a threaded component object that links to the specified ThreadedComponent.
	 * The ThreadManager will wait until both engines are locked before instantiation.
	 * After instantiation, the manager will link the localRequester with the newly instantiated ThreadedComponent.
	 */
	virtual void InstantiateExternalThreadComponent (ThreadedComponent* localRequester, std::function<ThreadedComponent*()> instantiationMethod);

	/**
	 * Checks its lists of PendingRequestData.  This method will halt the current thread should it need
	 * to copy data across thread boundaries.

	 * @param deltaSec Number of seconds that elapsed since last frame.
	 * @return true if the ThreadManager halted this thread.
	 */
	virtual bool EvaluateLockRequests (FLOAT deltaSec);

	/**
	 * Adds a request to the data transfer queue.  The requester and ThreadManager must reside on the same thread.
	 *
	 * @param localRequester - The ThreadComponent that would like to transfer data from this thread to the external thread.
	 * @param bForeLock - If true, then this request is urgent.  The ThreadManager will lock both Engines as soon as possible.
	 * @param maxWaitTime - The maximum wait time before the manager locks both threads.  The thread manager may flush the 
	 * requests before the max wait time (based on urgency and amount of other requests).  If nonpositive, then the request
	 * may wait indefinitely (no time limit).
	 */
	virtual void AddThreadRequest (ThreadedComponent* localRequester, bool bForceLock, FLOAT maxWaitTime);

	/**
	 * Removes all thread component requests that were instigated from the specified requester.
	 */
	virtual void CancelComponentInstantiation (ThreadedComponent* externalRequester);

	/**
	 * Removes the specified ThreadedComponent from the pending data list.
	 */
	virtual void RemoveThreadRequest (ThreadedComponent* localThreadComp, UINT_TYPE engineIdx);

	virtual void SetMaxNumRequests (INT newMaxNumRequests);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(INT, MaxNumRequests);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Finds the MultiThreadEngineComponent that's attached to the specified engine.
	 *
	 * @param engineIdx Target engine to retrieve found in the index of Engine::EngineInstances 
	 * @return If valid, it'll return the threaded engine component attached to the engine.
	 * It'll return nullptr if the engine is not found or is shutting down.
	 */
	virtual MultiThreadEngineComponent* GetThreadedEngine (UINT_TYPE engineIdx) const;

	/**
	 * Retrieves the engine's friendly name (if any).  Otherwise, it'll return the given engine index.
	 *
	 * @param engineIdx The target engine to retrieve found in the index of Engine::Instances.
	 */
	virtual DString GetEngineName (UINT_TYPE engineIdx) const;

	/**
	 * Retrieves the local engine's friendly name (if any).  Otherwise, it'll return the index of the local engine.
	 */
	virtual DString GetEngineName () const;

	/**
	 * Iterates through the pending thread component instantiation list, and links up the instantiated component
	 * with the requester.  Only components matching engines that are currently lock are processed.
	 * This function assumes that the local engine is frozen.
	 */
	virtual void ProcessComponentInstantationRequests ();

	/**
	 * Notifies the external engine to freeze when its time is right.  This function will not return
	 * until the external engine is frozen and all pending components transfered their data to the external thread.
	 *
	 * @param outPendingRequestData struct that's waiting for the external engine to freeze.
	 */
	virtual void TransferDataToExternalThread (SPendingRequestData& outPendingRequestData);

	/**
	 * Notifies this ThreadManager to freeze its local Engine to allow another ThreadManager to transfer data.
	 * The ThreadManager will halt the current thread when the Engine permits it to.
	 */
	virtual void ExternalFreezeRequest ();

	/**
	 * Notifies this ThreadManager that the data transfer has finished.  This decrements the NumFreezeRequests.
	 */
	virtual void ExternalUnfreezeRequest ();
};
SD_END