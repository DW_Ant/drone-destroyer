/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ThreadTester.h
  A debugging Entity that'll be testing Sand Dune's multithreaded framework.

  There are two tests where each test consist of 2 ThreadTester instances.

  One way test
  This will be testing the ThreadTester's capability of sending data from the MainThread to the DebugThread.
  The test validates the following:
  * Instantiates and links a ThreadTester on the DebugThread.
  * Sends all integers in the correct order to the DebugThread.
  * Tests ThreadComponent disconnection whenever the ThreadTester on main thread is destroyed.
  * Validates that the ThreadTester in DebugThread received all data in correct order.
  * The ThreadTester on the main thread will automatically destroy after sending the last integer.
  * The ThreadTester on the debug thread will automatically destroy after losing connection with main thread.

  Two way test
  This will be testing the ThreadTester's ability to send and receive data between the Main and Debug threads.
  This test validates the following:
  * Instantiates and links a ThreadTester on the DebugThread.
  * Sends all integers in the correct order to the DebugThread.  The ThreadTester on debug thread will also
  be sending data to the main thread at different intervals.
  * Tests ThreadComponent disconnection whenever the ThreadTester on the main thread is destroyed.
  * Validates that both ThreadTesters received all data in correct order.
  * The ThreadTester on the main thread will automatically destroy after receiving the last integer.
  It may also time out if it didn't receive any data within 10 seconds.
  * The ThreadTester on the debug thread will automatically destroy after losing connection with the main thread.
=====================================================================
*/

#pragma once

#include "MultiThread.h"

#ifdef DEBUG_MODE

SD_BEGIN
class ThreadedComponent;

class MULTI_THREAD_API ThreadTester : public Entity
{
	DECLARE_CLASS(ThreadTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Series of expected variable updates all ThreadTesters are required to send and receive. */
	static const std::vector<INT> ExpectedData;

	/* Determines the min/max interval range at which the tester sends data across boundaries (in seconds). */
	static const Range<FLOAT> DataIntervalRange;

protected:
	UnitTester::EUnitTestFlags TestFlags;

	/* Becomes true if this tester detected that it resides in the main thread. */
	bool bIsOnMainThread;

	/* Becomes true if this ThreadTester is part of the two-way communication test. */
	bool bIsTwoWayTest;

	/* Time that must elapse before sending the next data value (simulating work/async operations). */
	FLOAT DataWriteTime;

	/* Timestamp when data was last transfered. */
	FLOAT LastDataWriteTime;

	/* Component that'll be passing and retrieving variables across thread boundaries. */
	DPointer<ThreadedComponent> ThreadComp;

	DPointer<TickComponent> Tick;

	/* List of variable updates received from external component.  To pass the test, the received updates must
	align with ExpectedUpdates by the end of the test.
	Normally, we would ignore updates if the thread component was updated twice before its owner (ThreadTester) retrieves the latest value,
	but this test records the history to ensure all messages were sent. */
	std::vector<INT> ReceivedData;

	/* Time that must elapse before reading data from the thread component's data buffer (simulating work/async operations). */
	FLOAT DataReadTime;

	/* Timestamp when data was last read. */
	FLOAT LastDataReadTime;

	/* Maximum time allowed to elapse before the ThreadTester assumes that the external thread component is unable to send data (in seconds). */
	FLOAT TimeReceiveTimeout;

	/* Timestamp when this tester received data from external thread component. */
	FLOAT LastReceivedTime;

private:
	/* Index of ThreadTesterClient::ExpectedData vector's value that was most recently send across thread boundaries. */
	UINT_TYPE SentDataIndex;

	/* Becomes true if the tester sent the first data set.  This flag switches before SentDataIndex since this flips before the data is transfered. */
	bool bSentInitialData;

	/* Becomes true if this object kicked off any of its tests. */
	bool bStartedTest;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Initializes and kicks off the one-way communication unit test where this ThreadTester will only
	 * send messages to the component in external thread.
	 *
	 * @param testFlags unit test flags that specifies verbosity and error handling.
	 * @return Returns true if the test was kicked off.  Otherwise, there was an error in initializing the test.
	 */
	virtual bool BeginOneWayTest (UnitTester::EUnitTestFlags testFlags);

	/**
	 * Initializes and kicks off the two-way communication unit test where another ThreadTester will spawn in
	 * external thread, and this thread tester will communicate with external thread tester.
	 *
	 * @param testFlags unit test flags that specifies the verbosity and error handling.
	 * @return Returns true if the test was kicked off.  Otherwise, there was an error in initializing the test.
	 */
	virtual bool BeginTwoWayTest (UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline bool IsTwoWayTester () const
	{
		return bIsTwoWayTest;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Initializes and kicks off the test.
	 * Returns true if the test successfully started.
	 */
	virtual bool ExecuteUnitTest (UnitTester::EUnitTestFlags testFlags, bool bInTwoWayTest);

	/**
	 * Compares this Entity's ReceivedData against its expected data.  Should there be a mismatch,
	 * it'll notify the unit tester that an error has occured.
	 */
	virtual void EvaluateReceivedData () const;

	virtual bool ShouldTerminateTest () const;

	/**
	 * Notifies the local ThreadManager to lock up the engines so the tester could send data.
	 */
	virtual void SendDataRequest ();

	/**
	 * Reads from the local ThreadComponent's data buffer, and
	 * records all data to the ReceivedData vector.
	 */
	virtual void ReadReceivedData ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	/**
	 * The external engine is frozen.  It's currently safe to write data to the given data buffer.
	 * This event transfers data from local thread to the component in external thread.
	 */
	virtual void HandleDataTransfer (DataBuffer& outExternalData);

	virtual void HandleTick (FLOAT deltaSec);
};
SD_END

#endif