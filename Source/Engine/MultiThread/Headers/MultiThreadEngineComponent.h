/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MultiThreadEngineComponent.h
  This component is responsible maintaining and ticking the ThreadManager.
  An Engine with this component assumes that it may freeze occasionally to enable cross engine communication.
=====================================================================
*/

#pragma once

#include "MultiThread.h"
#include "ThreadManager.h"

SD_BEGIN
class MULTI_THREAD_API MultiThreadEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(MultiThreadEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* ThreadManager that may freeze this engine to allow external engines to transfer data across thread boundaries. */
	ThreadManager ThreadController;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	MultiThreadEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PostTick (FLOAT deltaSec) override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual ThreadManager* GetThreadManager ();
};
SD_END