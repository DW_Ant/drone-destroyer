/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlatformMacMultiThread.h
  Utility functions containing Mac-specific multi threaded operations.
=====================================================================
*/

#pragma once

#include "MultiThread.h"
#include "SDThread.h"

#ifdef PLATFORM_MAC

SD_BEGIN

/*
=====================
  Methods
=====================
*/

/**
 * Creates a new thread, and invokes the specified function on that thread.
 * @param outThread The thread object that'll uniquely identify the newly created thread.
 * @param startRoutine The function address that will be invoked first on the new thread.  This parameter varies based on the platform.
 * @param routineArgs The parameters passed into startRoutine.
 * @param initData A struct containing various optional parameters used to initialize the thread.
 * @return Returns an error code should an issue happen with thread initialization.  Returns 0 if there aren't any errors.
 */
int MULTI_THREAD_API OS_CreateThread (SDThread& outThread, void** startRoutine, void* routineArgs, SDThread::SThreadInitData* initData);
SD_END

#endif