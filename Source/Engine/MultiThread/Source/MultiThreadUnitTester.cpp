/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MultiThreadUnitTester.cpp
=====================================================================
*/

#include "MultiThreadClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
/**
 * Creates and initializes a debug Engine object.  This function will then continuously loop until the engine shuts down.
 * This function will not return until the Debug Engine completely shuts down.
 */
SD_THREAD_FUNCTION(InitializeDebugEngine);

IMPLEMENT_CLASS(SD, MultiThreadUnitTester, SD, UnitTester)

bool MultiThreadUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool bPassed = true;

	if ((testFlags & UTF_FeatureTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Asynchronous) > 0)
	{
		//Note:  bPassed becomes true when the test was kicked off.  The async test may fail moments later.  Based on testFlags, it may throw an assertion if it fails.
		bPassed = LaunchThreadTester(testFlags);
	}

	return bPassed;
}

bool MultiThreadUnitTester::LaunchThreadTester (EUnitTestFlags testFlags) const
{
	//Create a thread that launches the Debug Engine
	SDThread newThread;
	THREAD_INIT_TYPE threadInitData;
	threadInitData.ThreadName = TXT("Debug Thread");
	int exitCode = OS_CreateThread(newThread, InitializeDebugEngine, nullptr, &threadInitData);
	if (exitCode != 0)
	{
		UnitTestError(testFlags, TXT("Failed to launch thread tester.  Could not kick off a new thread that'll host the debug Engine.  Exit code:  %s"), {DString::MakeString(exitCode).ToCString()});
		return false;
	}

	ThreadTester* oneWayTester = ThreadTester::CreateObject();
	if (oneWayTester == nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to launch thread Tester.  Could not instantiate the one-way thread test object."));
		return false;
	}

	ThreadTester* twoWayTester = ThreadTester::CreateObject();
	if (twoWayTester == nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to launch thread Tester.  Could not instantiate the two-way thread test object."));
		return false;
	}

	//In milliseconds
	INT timeoutTime = 5000;
	INT totalWaitTime = 0;
	INT sleepInterval = 500;
	//Wait for the DebugEngine to finish initializing before kicking off the ThreadTester test
	while (Engine::GetEngine(Engine::DEBUG_ENGINE_IDX) == nullptr || !Engine::GetEngine(Engine::DEBUG_ENGINE_IDX)->IsInitialized())
	{
		totalWaitTime += sleepInterval;
		if (totalWaitTime >= timeoutTime)
		{
			UnitTestError(testFlags, TXT("Failed to launch thread tester since the DebugEngine in the other thread was not instantiated.  The wait loop timed-out."));
			oneWayTester->Destroy();
			twoWayTester->Destroy();
			return false;
		}

		OS_Sleep(sleepInterval);
	}

	//The thread testers will destroy themselves automatically after their test completed.
	oneWayTester->BeginOneWayTest(testFlags);
	TestLog(testFlags, TXT("One-way thread tester test has begun."));
	twoWayTester->BeginTwoWayTest(testFlags);
	TestLog(testFlags, TXT("Two-way thread tester test has begun."));
	return true;
}

/**
 * Entry function the debug thread will start in.
 * This function simply sets up the DebugEngine instance with a MultiThreadEngineComponent.
 */
SD_THREAD_FUNCTION(InitializeDebugEngine)
{
	Engine* debugEngine = new Engine();
	CHECK(debugEngine != nullptr)

	std::vector<EngineComponent*> engineComponents; //This Engine will only have the MultiThreadEngineComponent
	engineComponents.push_back(MultiThreadEngineComponent::SStaticClass()->GetDefaultEngineComponent()->CreateObjectOfMatchingClass());
	debugEngine->InitializeEngine(Engine::DEBUG_ENGINE_IDX, engineComponents);
	debugEngine->SetDebugName(TXT("Debug Engine"));

	//Instantiate the watcher that'll determine when it's time to shutdown the DebugEngine.
	DebugEngineTestWatcher* testWatcher = DebugEngineTestWatcher::CreateObject();

	while (debugEngine != nullptr)
	{
		debugEngine->Tick();

		if (debugEngine->IsShuttingDown())
		{
			delete debugEngine;
			debugEngine = nullptr;
		}
	}

	SD_EXIT_THREAD
}
SD_END

#endif