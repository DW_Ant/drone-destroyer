/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DebugEngineTestWatcher.cpp
=====================================================================
*/

#include "MultiThreadClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, DebugEngineTestWatcher, SD, Object)

void DebugEngineTestWatcher::InitProps ()
{
	Super::InitProps();

	bIsWatching = false;
}

void DebugEngineTestWatcher::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, DebugEngineTestWatcher, HandleGarbageCollection, void));
	}
}

void DebugEngineTestWatcher::Destroyed ()
{
	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, DebugEngineTestWatcher, HandleGarbageCollection, void));
	}

	Super::Destroyed();
}

DebugEngineTestWatcher* DebugEngineTestWatcher::FindTestWatcher ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	for(ObjectIterator iter(localEngine->GetObjectHashNumber(), false); iter.SelectedObject != nullptr; iter++)
	{
		DebugEngineTestWatcher* watcher = dynamic_cast<DebugEngineTestWatcher*>(iter.SelectedObject.Get());
		if (watcher != nullptr)
		{
			return watcher;
		}
	}

	MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Unable to find an instance of the DebugEngineTestWatcher."));
	return nullptr;
}

void DebugEngineTestWatcher::AddWatchObject (Object* newWatchObject)
{
	//Ensure the object isn't already added
	for (Object* watchedObj : WatchedObjects)
	{
		if (watchedObj == newWatchObject)
		{
			MultiThreadLog.Log(LogCategory::LL_Warning, TXT("%s refused to add %s to the watch list since that object is already in the watch list."), ToString(), newWatchObject->ToString());
			return;
		}
	}

	WatchedObjects.push_back(newWatchObject);
	bIsWatching = true;
}

void DebugEngineTestWatcher::HandleGarbageCollection ()
{
	if (!bIsWatching)
	{
		return;
	}

	UINT_TYPE i = 0;
	while (i < WatchedObjects.size())
	{
		if (WatchedObjects.at(i)->GetPendingDelete())
		{
			WatchedObjects.erase(WatchedObjects.begin() + i);
			continue;
		}

		++i;
	}

	if (ContainerUtils::IsEmpty(WatchedObjects))
	{
		MultiThreadLog.Log(LogCategory::LL_Debug, TXT("Shutting down debug engine since all objects registered in the watcher were destroyed."));

		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)
		localEngine->Shutdown();
		Destroy();
	}
}
SD_END

#endif