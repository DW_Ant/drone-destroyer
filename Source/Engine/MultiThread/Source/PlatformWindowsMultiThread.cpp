/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlatformWindowsMultiThread.cpp
=====================================================================
*/


#include "MultiThreadClasses.h"

#ifdef PLATFORM_WINDOWS

SD_BEGIN
namespace PWI //PlatformWindowsImplementation - Namespace used reduce name conflicts with generic function names such as SetThreadName.
{
	//Needed to extract this function from OS_CreateThread due to use of __try __except block.
	void SetThreadName (const char* threadName, DWORD threadId)
	{
		//Window's threads do not have thread names.  Raise an exception to allow debuggers to capture a thread name.
		SDThread::SWinThreadNameData nameData;
		nameData.ThreadName = threadName;
		nameData.ThreadId = threadId;
		const DWORD MS_VC_EXCEPTION = 0x406D1388; //Some magic number for Windows.

		__try
		{  
			RaiseException(MS_VC_EXCEPTION, 0, sizeof(nameData) / sizeof(ULONG_PTR), (ULONG_PTR*)&nameData);
		}  
		__except(EXCEPTION_EXECUTE_HANDLER)
		{
		}
	}
}

int OS_CreateThread (SDThread& outThread, LPTHREAD_START_ROUTINE startRoutine, void* routineArgs, SDThread::SThreadInitData* initData)
{
	SDThread::SWinThreadInitData* winInitData = dynamic_cast<SDThread::SWinThreadInitData*>(initData);
	bool bCreatedInitData = false;
	if (winInitData == nullptr)
	{
		winInitData = new SDThread::SWinThreadInitData();
		bCreatedInitData = true;
	}

	HANDLE threadHandle = CreateThread(winInitData->SecurityAttributes, winInitData->StackSize, startRoutine, routineArgs, winInitData->CreationFlags, nullptr);
	if (threadHandle == nullptr)
	{
		if (bCreatedInitData)
		{
			delete winInitData;
		}

		return -1;
	}

	outThread = SDThread(threadHandle);
	if (!winInitData->ThreadName.IsEmpty())
	{
		DWORD winThreadId = GetThreadId(const_cast<const HANDLE>(outThread.GetThread()));
		if (winThreadId != 0)
		{
			PWI::SetThreadName(winInitData->ThreadName.ToCString(), winThreadId);
		}
		else
		{
			MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Unable to name newly created thread to %s since it wasn't able to retrieve the thread ID.  Error code:  %s"), winInitData->ThreadName, DString::MakeString(GetLastError()));
		}
	}

	if (bCreatedInitData)
	{
		delete winInitData;
	}

	return 0;
}
SD_END

#endif