/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ThreadedComponent.cpp
=====================================================================
*/

#include "MultiThreadClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, ThreadedComponent, SD, EntityComponent)

void ThreadedComponent::InitProps ()
{
	Super::InitProps();

	ExternalEngineIdx = UINT_INDEX_NONE;
	InternalEngineIdx = UINT_INDEX_NONE;

	{
		std::lock_guard<std::mutex> guard(ExternalComponentMutex);
		ExternalComponent = nullptr;
	}

	CopyMethod.ClearFunction();

	LocalThreadManager = nullptr;
	bEstablishedConnection = false;
}

void ThreadedComponent::BeginObject () 
{
	Super::BeginObject();

	InternalEngineIdx = Engine::FindEngine()->GetEngineIndex();

	MultiThreadEngineComponent* localThreadEngine = MultiThreadEngineComponent::Find();
	CHECK(localThreadEngine != nullptr);

	LocalThreadManager = localThreadEngine->GetThreadManager();
	CHECK(LocalThreadManager != nullptr);
}

void ThreadedComponent::Destroyed ()
{
	//Cancel any pending actions
	if (LocalThreadManager != nullptr)
	{
		if (ExternalEngineIdx != UINT_INDEX_NONE)
		{
			LocalThreadManager->RemoveThreadRequest(this, ExternalEngineIdx);
		}

		LocalThreadManager->CancelComponentInstantiation(this);
		LocalThreadManager = nullptr;
	}

	if (!IsConnectionTerminated())
	{
		DetachThreadedConnection();
	}

	Super::Destroyed();
}

bool ThreadedComponent::IsConnectionTerminated () const
{
	return (ExternalComponent == nullptr && bEstablishedConnection);
}

void ThreadedComponent::RequestDataTransfer (bool bForced)
{
	CHECK(LocalThreadManager != nullptr)
	if (!IsConnectionTerminated())
	{
		LocalThreadManager->AddThreadRequest(this, bForced, -1.f);
	}
}

void ThreadedComponent::RequestDataTransfer (FLOAT maxWaitTime)
{
	//Ensure the developer has invoked InstantiateListener before making requests
	CHECK(ExternalEngineIdx != UINT_INDEX_NONE)

	CHECK(LocalThreadManager != nullptr)
	if (!IsConnectionTerminated())
	{
		LocalThreadManager->AddThreadRequest(this, false, maxWaitTime);
	}
}

void ThreadedComponent::InstantiateExternalComp (UINT_TYPE inExternalEngineIdx, std::function<ThreadedComponent*()> instantiationMethod)
{
	if (inExternalEngineIdx > Engine::GetMaxNumPossibleEngines() - 1)
	{
		MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Failed to instantiate external ThreadedComponent since the given external engine index (%s) is not a valid index.  Valid indices range from 0-%s."), INT(inExternalEngineIdx), Engine::GetMaxNumPossibleEngines());
		return;
	}

	//The external engine index must be an external engine
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	if (localEngine->GetEngineIndex() == inExternalEngineIdx)
	{
		MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Failed to instantiate external ThreadedComponent since the given external engine index (%s) is the index of the engine that's in the same thread.  The index must be an index to an engine in a separate thread."), INT(inExternalEngineIdx));
		return;
	}

	ExternalEngineIdx = inExternalEngineIdx;

	//Find the external thread ID through the external engine
	Engine* externalEngine = Engine::GetEngine(ExternalEngineIdx);
	CHECK(externalEngine != nullptr)
	MultiThreadEngineComponent* externalEngineComp = MultiThreadEngineComponent::Find(externalEngine->GetThreadID());
	if (externalEngineComp == nullptr)
	{
		MultiThreadLog.Log(LogCategory::LL_Warning, TXT("Failed to instantiate external ThreadedComponent since the specified engine (of index %s) does not contain a MultiThreadEngineComponent."), INT(ExternalEngineIdx));
		return;
	}

	ThreadManager* externalThreadManager = externalEngineComp->GetThreadManager();
	CHECK(externalThreadManager != nullptr)

	//The external ThreadedComponent must reside in other thread.  Notify external thread manager about the instantiation request.
	externalThreadManager->InstantiateExternalThreadComponent(this, instantiationMethod);
}

void ThreadedComponent::SetCopyMethod (SDFunction<void, DataBuffer&> newCopyMethod)
{
	CopyMethod = newCopyMethod;
}

bool ThreadedComponent::IsCopyMethodBounded () const
{
	return CopyMethod.IsBounded();
}

void ThreadedComponent::ExecuteCopyMethod ()
{
	CHECK(CopyMethod.IsBounded())
	if (ExternalComponent != nullptr)
	{
		//Execute method that'll populate the data buffer.
		CopyMethod(ExternalComponent->ExternalData);
	}
}

void ThreadedComponent::DetachThreadedConnection ()
{
	//Sever connection between the two threaded components
	std::lock_guard<std::mutex> guard(ExternalComponentMutex);
	if (ExternalComponent != nullptr)
	{
		ExternalComponent->ExternalComponent = nullptr;
		ExternalComponent = nullptr;
	}
}

void ThreadedComponent::SetExternalComponent (ThreadedComponent* newExternalComponent)
{
	std::lock_guard<std::mutex> guard(ExternalComponentMutex);
	ExternalComponent = newExternalComponent;
	bEstablishedConnection = true;
}
SD_END