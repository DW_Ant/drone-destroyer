/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SDThread.cpp
=====================================================================
*/

#include "MultiThreadClasses.h"

SD_BEGIN
SDThread::SThreadInitData::SThreadInitData ()
{
	ThreadName = DString::EmptyString;
}

SDThread::SThreadInitData::SThreadInitData (const SThreadInitData& copyObject)
{
	ThreadName = copyObject.ThreadName;
}

SDThread::SThreadInitData::~SThreadInitData ()
{
	//Noop
}

#ifdef PLATFORM_WINDOWS
SDThread::SWinThreadInitData::SWinThreadInitData () : SThreadInitData()
{
	SecurityAttributes = nullptr;
	StackSize = 0;
	CreationFlags = 0;
}

SDThread::SWinThreadInitData::SWinThreadInitData (const SWinThreadInitData& copyObject) : SThreadInitData()
{
	SecurityAttributes = copyObject.SecurityAttributes;
	StackSize = copyObject.StackSize;
	CreationFlags = copyObject.CreationFlags;
}

SDThread::SWinThreadInitData::~SWinThreadInitData ()
{
	//Noop
}
#endif

#ifdef PLATFORM_LINUX
SDThread::SpThreadInitData::SpThreadInitData () : SThreadInitData()
{
	int error = pthread_attr_init(&InitAttributes);
	CHECK(error == 0) //POSIX.1-2001 documents a rare error.  This error shouldn't happen unless there's a compatibility issue.
}

SDThread::SpThreadInitData::SpThreadInitData (const SpThreadInitData& copyObject) : SThreadInitData()
{
	InitAttributes = copyObject.InitAttributes;
}

SDThread::SpThreadInitData::~SpThreadInitData ()
{
	//Noop
}
#endif

SDThread::SDThread ()
{
	Thread = nullptr;
}

SDThread::SDThread (THREAD_OBJECT* inThread)
{
	Thread = inThread;
}

SDThread::SDThread (const SDThread& copyObject)
{
	Thread = copyObject.Thread;
}

SDThread::~SDThread ()
{
	//Noop
}
SD_END