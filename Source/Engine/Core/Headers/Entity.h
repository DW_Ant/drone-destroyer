/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Entity.h
  Entity is an object that does not have a definition by itself.  Entities
  are essentially defined through their components.
=====================================================================
*/

#pragma once

#include "Object.h"
#include "ComponentIterator.h"

SD_BEGIN
class EntityComponent;

class CORE_API Entity : public Object
{
	DECLARE_CLASS(Entity)


	/*
	=====================
	  Properties
	=====================
	*/

private:
	/* List of all immediate entity components this Entity owns.  This does not
	reference entity components that are owned by this entity's entity components. */
	std::vector<DPointer<EntityComponent>> Components;

	/* If true, then this Entity is visible.  Typically if the Owner is invisible, all sub components are invisible, too. */
	bool bVisible;

	/* An number that represents the "sum" of components' hash IDs.  This provides a quick glance
	in what components are affecting this entity without having to iterate through them all.
	Components within components also affect this value. */
	unsigned int ComponentsModifier;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual unsigned int CalculateHashID () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds the new component to the entity's component array.
	 * If bLogOnFail is true, then a log message will display if this function returns false.
	 * If bDeleteOnFail is true, then the newComponent entity will delete itself if it failed to attach itself.
	 */
	virtual bool AddComponent (EntityComponent* newComponent, bool bLogOnFail = true, bool bDeleteOnFail = true);

	/**
	 * Attempts to remove an component from this entity.
	 * Returns true if component was found and detached form component list.
	 * This does NOT destroy the component.
	 */
	virtual bool RemoveComponent (EntityComponent* target);

	/**
	 * Adjusts the ComponentsModifier value based on the new component's hash.
	 */
	virtual void AddComponentModifier (EntityComponent* newComponent);

	/**
	 * Adjusts the ComponentsModifier value based on the new component's hash.
	 * The value will not be adjusted if this entity possesses another component with the same hash.
	 * Returns true if the ComponentModifier was modified.
	 */
	virtual bool RemoveComponentModifier (EntityComponent* oldComponent);

	/**
	 * Recursively searches through this entity's component list to find the first
	 * component whose hash matches the given parameters.
	 * if bRecursive is false, then this function will not search components within components.
	 * This will only return the first component it finds.  Use a ComponentIterator to find multiple components.
	 */
	virtual EntityComponent* FindSubComponent (unsigned int targetComponentHash, bool bRecursive = true) const;

	/**
	 * Recursively searches through this entity's component list to find the first
	 * component of matching static class.
	 * If bRecursive is false, then this function will not search components within components.
	 * This will only return the first component it finds.  Use a ComponentIterator to find multiple components.
	 */
	virtual EntityComponent* FindSubComponent (const DClass* targetClass, bool bRecursive = true) const;

	virtual void SetVisibility (bool newVisibility);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<DPointer<EntityComponent>>& ReadComponents () const
	{
		return Components;
	}

	virtual bool IsVisible () const;
	virtual unsigned int GetComponentsModifier () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
#ifdef DEBUG_MODE
	/**
	 * For debug builds, this function runs every frame.
	 * It's a quick utility for checking status on this entity without having to create a TickComponent.
	 */
	virtual void DebugTick (FLOAT deltaTime);
#endif

	friend class Engine;
	friend class ComponentIterator;
	friend class EntityComponent;
};
SD_END