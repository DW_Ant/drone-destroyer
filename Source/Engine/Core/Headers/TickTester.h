/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TickTester.h
  Debugging Entity used to ensure its various TickComponents ticks as expected.
=====================================================================
*/

#pragma once

#include "Entity.h"
#include "UnitTester.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API TickTester : public Entity
{
	DECLARE_CLASS(TickTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	UnitTester::EUnitTestFlags TestFlags;
	DPointer<const UnitTester> OwningTester;

	/* When all TickCounters meets or exceeds this value, then the UnitTest completes, and the entity destroys itself. */
	INT MaxTickCounter;

protected:
	/* Becomes true if the Misc TickGroup should tick before the Debug TickGroup. */
	bool bMiscTickFirst;

	/* Various counters that increments on each tick. */
	INT MiscTickCounter1;
	INT MiscTickCounter2;
	INT DebugTickCounter1;
	INT DebugTickCounter2;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void BeginTest ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the tick counters to ensure the tick order is working as expected.
	 * This conditionally terminates the test if a failure was detected, or if all conditions passed.
	 */
	virtual void ValidateTickTest ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleMiscTick1 (FLOAT deltaSec);
	virtual void HandleMiscTick2 (FLOAT deltaSec);
	virtual void HandleDebugTick1 (FLOAT deltaSec);
	virtual void HandleDebugTick2 (FLOAT deltaSec);
	virtual void HandleDisabledTick (FLOAT deltaSec);
};
SD_END

#endif //debug_mode