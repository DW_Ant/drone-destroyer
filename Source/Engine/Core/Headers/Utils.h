/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Utils.h
  Contains multiple commonly used static utility functions.
=====================================================================
*/

#pragma once

#include "BaseUtils.h"

#define PI 3.14159265358
#define PI_FLOAT 3.141593f

SD_BEGIN
class CORE_API Utils : public BaseUtils
{
	DECLARE_CLASS(Utils)


	/*
	=====================
	  Templates
	=====================
	*/

public:
#pragma region "Templates"
	/**
	 * Returns the larger number of the two numbers.
	 */
	template <class Type>
	static inline const Type Max (const Type& a, const Type& b)
	{
		return (a > b) ? a : b;
	}

	/**
	 * Returns the smaller number of the two numbers.
	 */
	template <class Type>
	static inline const Type Min (const Type& a, const Type& b)
	{
		return (a < b) ? a : b;
	}

	/**
	 * Returns a copy of input that is bounded to min and max.
	 */
	template <class Type>
	static inline const Type Clamp (const Type& input, const Type& min, const Type& max)
	{
		return Max(Min(input, max), min);
	}

	/**
	 * Linear interpolation  between minBounds and maxBounds based on the given ratio.
	 * Ratio ranges from [0,1] to return a value between min and max bounds.  0 being equal to min bounds, and 1 being equal to max bounds.
	 * @tparam Type The datatype used for minBounds and maxBounds.
	 * @tparam A The datatype used for alpha/ratio.  Only decimal datatypes (such as floats, doubles, and FLOATs) are supported.
	 */
	template <class Type, class A>
	static inline const Type Lerp (const A& ratio, const Type& minBounds, const Type& maxBounds)
	{
		return static_cast<Type>((ratio * static_cast<A>(maxBounds - minBounds)) + static_cast<A>(minBounds));
	}

	/**
	 * Rounds the given number to the nearest whole value.
	 * [0-0.5) rounds to 0.
	 * [0.5-1.0) rounds to 1.
	 * Only decimal template arguments (such as float, double, and FLOAT) are supported.
	 */
	template <class Type>
	static inline const Type Round (const Type& a)
	{
		return std::floor(a + 0.5f);
	}

	/**
	 * Returns true if the two numbers are approximately equal to each other
	 * within the specified tolerance.
	 */
	template <class Type>
	static inline bool IsAboutEqual (const Type& a, const Type& b, const Type& tolerance)
	{
		return (std::abs(a - b) <= tolerance);
	}

	/**
	 * Returns true if the given is in powers of 2.
	 * Only integers are supported.
	 */
	template <class Type>
	static inline bool IsPowerOf2 (const Type& num)
	{
		return (num > 0 && !(num & (num - 1)));
	}
#pragma endregion Templates
};
SD_END