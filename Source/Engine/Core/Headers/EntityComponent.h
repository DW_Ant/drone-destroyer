/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EntityComponent.h
  EntityComponent is the base class of all Entity Components.
  EntityComponents extend its owning entity's functionality since the component
  may influence how the entity behaves.
=====================================================================
*/

#pragma once

#include "Entity.h"

SD_BEGIN
class CORE_API EntityComponent : public Entity
{
	DECLARE_CLASS(EntityComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Entity this component is attached to. */
	DPointer<Entity> Owner;

	/* Reference counter to the number of Owning Entities that are invisible.  0 means
	no Entities in the ownership chain are invisible. */
	INT NumInvisibleOwners;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void AddComponentModifier (EntityComponent* newComponent) override;
	virtual bool RemoveComponentModifier (EntityComponent* oldComponent) override;
	virtual bool IsVisible () const override;

protected:
	virtual unsigned int CalculateHashID () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Retrieves the root entity that this component influences.
	 * Climbs up the attached component chain until it finds a non-component entity.
	 */
	virtual Entity* GetRootEntity () const;

	/**
	 * Returns true if this component is allowed to be attached to the specified entity.
	 */
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const;

	/**
	 * Notifies the owning entity to RemoveComponent(this)
	 */
	void DetachSelfFromOwner ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns the immediate owner of this component.
	 */
	virtual Entity* GetOwner () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Notifies this component to attach itself to the specified entity.
	 * Refer to Entity::AddComponent to call this function.
	 */
	virtual void AttachTo (Entity* newOwner);

	/**
	 * Method invoked whenever this component is detached from owning entity.
	 */
	virtual void ComponentDetached ();

	friend class Entity;
};
SD_END