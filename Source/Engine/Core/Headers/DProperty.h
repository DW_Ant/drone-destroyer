/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DProperty.h
  Short for Dune Property, this abstract class is the parent class of
  all Sand Dune data types.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class DString;
class DataBuffer;

class CORE_API DProperty
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a string representation of this data type.
	 */
	virtual DString ToString () const = 0;

	/**
	 * Saves this variable to the given dataBuffer.
	 */
	virtual void Serialize (DataBuffer& dataBuffer) const = 0;

	/**
	 * Reads from the given dataBuffer, and records the byte contents to this variable.
	 */
	virtual void Deserialize (const DataBuffer& dataBuffer) = 0;
};
SD_END