/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Vector2.h
  Self-contained 2D vector class with some utilities.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#include "SDFLOAT.h"

SD_BEGIN
class Vector3;

class CORE_API Vector2 : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	FLOAT X;
	FLOAT Y;

	/* A 2D vector that has all of its components equal to 0. */
	static const Vector2 ZeroVector;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Vector2 ();

	Vector2 (const float x, const float y);

	Vector2 (const FLOAT x, const FLOAT y);

	Vector2 (const Vector2& copyVector);

		
	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Vector2& copyVector);
	FLOAT operator[] (UINT_TYPE axisIdx);
	const FLOAT operator[] (UINT_TYPE axisIdx) const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	DString ToString () const override;
	virtual void Serialize (DataBuffer& dataBuffer) const override;
	virtual void Deserialize (const DataBuffer& dataBuffer) override;

		
	/*
	=====================
	  Methods
	=====================
	*/

public:
	static Vector2 SFMLtoSD (const sf::Vector2f sfVector);
	static sf::Vector2f SDtoSFML (const Vector2 sdVector);

	bool IsNearlyEqual (const Vector2& compareTo, FLOAT tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns the magnitude of this vector.
	 */
	FLOAT VSize () const;

	/**
	 * Adjusts this vector's size so the length of the vector is equal to the specified length.
	 */
	void SetLengthTo (FLOAT targetLength);

	Vector2 static Min (Vector2 input, const Vector2& min);
	void MinInline (const Vector2& min);
	Vector2 static Max (Vector2 input, const Vector2& max);
	void MaxInline (const Vector2& max);

	/**
	 * Clamps both axis to be within the min/max range. Both axis acts independently from each other.
	 */
	Vector2 static Clamp (Vector2 input, const Vector2& min, const Vector2& max);
	void ClampInline (const Vector2& min, const Vector2& max);

	FLOAT Dot (const Vector2& otherVector) const;

	void Normalize ();

	/**
	 * Returns true of all axis of this vector are the equal.
	 */
	bool IsUniformed () const;

	/**
	 * Returns true if all axis are 0.
	 */
	bool IsEmpty () const;

	/**
	 * Returns a Vector3 from this vector's components.  Z axis is 0.
	 */
	Vector3 ToVector3 () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Retrieves one of the vector dimensions based on specified axis index.
	 * [0] = X axis
	 * [1] = Y axis
	 */
	FLOAT GetAxis (UINT_TYPE vectorComponent) const;


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Executes a linear interpolation between minBounds and maxBounds based on the given ratio for all axis.
	 * Ratio ranges from [0,1] to return a value between min and max bounds.  0 being equal to min bounds, and 1 being equal to max bounds.
	 * @tparam T The datatype used for alpha/ratio.  Only decimal datatypes (such as floats, doubles, and FLOATs) are supported.
	 */
	template <class T>
	static Vector2 Lerp(T ratio, const Vector2& minBounds, const Vector2& maxBounds)
	{
		Vector2 result;

		result.X = Utils::Lerp(ratio, minBounds.X, maxBounds.X);
		result.Y = Utils::Lerp(ratio, minBounds.Y, maxBounds.Y);

		return result;
	}
};

#pragma region "External Operators"
	CORE_API bool operator== (const Vector2& left, const Vector2& right);
	CORE_API bool operator!= (const Vector2& left, const Vector2& right);
	CORE_API Vector2 operator+ (const Vector2& left, const Vector2& right);
	CORE_API Vector2 operator+ (const Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f operator+ (const sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2& operator+= (Vector2& left, const Vector2& right);
	CORE_API Vector2& operator+= (Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f& operator+= (sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2 operator- (const Vector2& left, const Vector2& right);
	CORE_API Vector2 operator- (const Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f operator- (const sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2& operator-= (Vector2& left, const Vector2& right);
	CORE_API Vector2& operator-= (Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f& operator-= (sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2 operator* (const Vector2& left, const Vector2& right);
	CORE_API Vector2 operator* (const Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f operator* (const sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2& operator*= (Vector2& left, const Vector2& right);
	CORE_API Vector2& operator*= (Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f& operator*= (sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2 operator/ (const Vector2& left, const Vector2& right);
	CORE_API Vector2 operator/ (const Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f operator/ (const sf::Vector2f& left, const Vector2& right);
	CORE_API Vector2& operator/= (Vector2& left, const Vector2& right);
	CORE_API Vector2& operator/= (Vector2& left, const sf::Vector2f& right);
	CORE_API sf::Vector2f& operator/= (sf::Vector2f& left, const Vector2& right);

	template<class T>
	Vector2 operator* (const Vector2& left, const T& right)
	{
		return Vector2(left.X * right, left.Y * right);
	}

	template<class T>
	Vector2 operator* (const T& left, const Vector2& right)
	{
		return Vector2(left * right.X, left * right.Y);
	}

	template<class T>
	Vector2& operator*= (Vector2& left, const T& right)
	{
		left.X *= right;
		left.Y *= right;

		return left;
	}

	template<class T>
	Vector2 operator/ (const Vector2& left, const T& right)
	{
		if (right != 0)
		{
			return Vector2(left.X / right, left.Y / right);
		}

		return left;
	}

	template<class T>
	Vector2& operator/= (Vector2& left, const T& right)
	{
		if (right != 0)
		{
			left.X /= right;
			left.Y /= right;
		}

		return left;
	}
#pragma endregion
SD_END