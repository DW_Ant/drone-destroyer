/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlatformWindows.h
  Includes any Windows-specific libraries and definitions.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#ifdef PLATFORM_WINDOWS

//HACK:  Need to redirect INT so the typedef INT in minwindef.h doesn't override SD INT when working with external namespaces
#define INT MS_INT

//Do the same for other datatypes
#define FLOAT MS_FLOAT

//Platform-specific includes
#include <windows.h>
#include <shellapi.h>

#undef FLOAT
#undef INT

#if _WIN32
#include "..\Windows\resource.h"
#endif


/*
=====================
  Defines
=====================
*/

// Get 32/64 bit defines
#if _WIN64
#define PLATFORM_64BIT
#elif _WIN32
#define PLATFORM_32BIT
#else
#error "Please define your platform."
#endif

#include "Core.h"
#include "CoreMacros.h"
#include "Rectangle.h"
#include "INT.h"

SD_BEGIN
class DString;


/*
=====================
  Methods
=====================
*/

/**
 * Setup any platform-specific initialization.
 */
void CORE_API InitializePlatform ();

void CORE_API PlatformOpenWindow (const DString& windowMsg, const DString& windowTitle);

/**
 * Informs the operating system to freeze this current thread for the specified amount.
 * This function does not return until enough time elapsed.
 */
void CORE_API OS_Sleep (INT milliseconds);

/**
 * Copies the copyContent to the OS clipboard.  Returns true if successful.
 */
bool CORE_API OS_CopyToClipboard (const DString& copyContent);

/**
 * Retrieves the text buffer from the OS clipboard, and returns as result as string.
 * Returns an empty string if clipboard is empty, or if application was unable to paste from clipboard.
 */
DString CORE_API OS_PasteFromClipboard ();

/**
 * Sends a signal to allow an IDE to break the program for debugging.
 * Execution should halt if the signal was captured.
 */
void CORE_API OS_BreakExecution ();
SD_END

#endif //PLATFORM_WINDOWS