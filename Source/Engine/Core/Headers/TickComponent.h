/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TickComponent.h
  TickComponent contains a delegate that is continuously called from the Engine every frame.
=====================================================================
*/

#pragma once

#include "EntityComponent.h"

SD_BEGIN
class TickGroup;

class CORE_API TickComponent : public EntityComponent
{
	DECLARE_CLASS(TickComponent)


	/*
	=====================
	  Properties
	=====================
	*/
		
protected:
	/* Determines how frequently this TickComponent will invoke OnTick.  If negative, it'll update every frame,
	otherwise, it'll tick at this given interval. */
	FLOAT TickInterval;

	/* Function to callback on every update.  DeltaSec is the amount of time elapsed since last OnTick. */
	SDFunction<void, FLOAT /*deltaSec*/> OnTick;

	/* TickGroup this component is registered to.  This TickComponent may not be registered to the TickGroup if the component
	is not ticking. */
	TickGroup* OwningTickGroup;

	/* Only used at init, this is the TickGroup this TickComponent is about to register to. */
	DString PendingTickGroupName;

	/* If true, then the Tick function is active and is being called every frame. */
	bool bTicking;

private:
	/* Accumulated deltaTime since last OnTick execution. */
	FLOAT AccumulatedDeltaTime;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	TickComponent ();
	TickComponent (const DString& inTickGroup);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual unsigned int CalculateHashID () const override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Overload method to specify which Tick group this component should register to.
	 */
	static TickComponent* CreateObject (const DString& tickGroupName);

	/**
	 * This function runs every update call where deltaTime is the time since the last update call.
	 */
	virtual void Tick (FLOAT deltaSec);

	/**
	 * For TickComponents with TickIntervals, this resets the accumulated Tick time so that the Tick function
	 * will not be invoked until enough time elapsed.  The accumulated tick time resets automatically on Tick,
	 * this function only needs to be invoked to reset the time before Tick is invoked.
	 */
	virtual void ResetAccumulatedTickTime ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetTickInterval (FLOAT newTickInterval);

	/**
	 * Assigns the OnTick delegate to the given parameter.
	 */
	virtual void SetTickHandler (SDFunction<void, FLOAT> newHandler);

	virtual void SetTicking (bool bNewTicking);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool IsTicking () const;

	DEFINE_ACCESSOR(FLOAT, TickInterval)
};
SD_END