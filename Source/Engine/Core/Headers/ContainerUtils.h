/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ContainerUtils.h
  Various utility functions supporting std vector containers.
=====================================================================
*/

#pragma once

#include "BaseUtils.h"

SD_BEGIN
class CORE_API ContainerUtils : public BaseUtils
{
	DECLARE_CLASS(ContainerUtils)


	/*
	=====================
	  Templates
	=====================
	*/

public:
#pragma region "Templates"
	/**
	 * Returns true if the vector does not contain any elements (length 0).
	 */
	template <class Type>
	static inline bool IsEmpty (const std::vector<Type>& vectorToCheck)
	{
		return vectorToCheck.empty(); //'empty' is my least favorite function name from the std library.
	}

	/**
	 * Removes all elements within the vector.
	 */
	template <class Type>
	static inline void Empty (std::vector<Type>& outVectorToClear)
	{
		outVectorToClear.clear();
	}

	/**
	 * Returns true if the specified index is within the range of the vector's indices.
	 * Checks to see if it's greater than or equal to 0, and it's less than the vector size.
	 */
	template <class Type>
	static inline bool IsValidIndex(const std::vector<Type>& vector, INT index)
	{
		return (index < vector.size() && index >= 0);
	}

	/**
	 * Returns the last element of the given vector.  Asserts if the vector is empty.
	 */
	template <class Type>
	static inline Type GetLast (const std::vector<Type>& vector)
	{
		CHECK(!IsEmpty(vector));
		return vector.at(vector.size() - 1);
	}

	/**
	 * Returns the first index that matches the specified target.
	 * Complexity is linear.
	 */
	template <class Type>
	static UINT_TYPE FindInVector (const std::vector<Type>& vector, Type searchFor)
	{
		for (UINT_TYPE i = 0; i < vector.size(); i++)
		{
			if (vector.at(i) == searchFor)
			{
				return i;
			}
		}

		return UINT_INDEX_NONE;
	}

	/**
	 * Removes the specified item from the vector.  Complexity is linear.
	 * @return The index the item was removed from, or returns INT_INDEX_NONE if the item is not found.
	 */
	template <class Type>
	static UINT_TYPE RemoveItem (std::vector<Type>& outVector, Type target)
	{
		UINT_TYPE itemIdx = FindInVector(outVector, target);
		if (itemIdx != UINT_INDEX_NONE)
		{
			outVector.erase(outVector.begin() + itemIdx);
		}

		return itemIdx;
	}

	/**
	 * Removes all items that match the target.  Complexity is linear.
	 * @return The number of items that were removed from the vector.
	 */
	template <class Type>
	static INT RemoveItems (std::vector<Type>& outVector, Type target)
	{
		if (IsEmpty(outVector))
		{
			return 0;
		}

		INT numRemoved = 0;
		//Iterate backwards so that the erases do not interfere with indices.
		UINT_TYPE i = outVector.size() - 1;
		while (true)
		{
			if (outVector.at(i) == target)
			{
				outVector.erase(outVector.begin() + i);
				++numRemoved;
			}

			if (i == 0)
			{
				break;
			}
			--i;
		}

		return numRemoved;
	}

	/**
	 * Adds a unique element to the specified vector to avoid duplicate entries.
	 * Returns the index the item is found in vector.
	 * Complexity is linear.
	 */
	template <class Type>
	static UINT_TYPE AddUnique (std::vector<Type>& outVector, Type itemToAdd)
	{
		UINT_TYPE itemIdx = FindInVector(outVector, itemToAdd);
		if (itemIdx == UINT_INDEX_NONE)
		{
			outVector.push_back(itemToAdd);
			return (outVector.size() - 1);
		}

		return itemIdx;
	}

	/**
	 * Counts and returns the number of times this value appears in the specified vector.
	 * Complexity is linear.
	 */
	template <class Type>
	static UINT_TYPE CountNumItems (const std::vector<Type>& vector, Type itemTarget)
	{
		UINT_TYPE count = 0;
		for (UINT_TYPE i = 0; i < vector.size(); ++i)
		{
			if (vector.at(i) == itemTarget)
			{
				count++;
			}
		}

		return count;
	}
#pragma endregion Templates
};
SD_END