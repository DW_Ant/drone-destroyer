/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CoreClasses.h
  Contains all header includes for the Core module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the CoreClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#define MODULE_CORE 1

#include "BaseUtils.h"
#include "BOOL.h"
#include "ClassIterator.h"
#include "ComponentIterator.h"
#include "Configuration.h"
#include "ContainerUtils.h"
#include "CopiableObjectInterface.h"
#include "Core.h"
#include "CoreDatatypes.h"
#include "CoreMacros.h"
#include "CoreUtilUnitTester.h"
#include "DataBuffer.h"
#include "DatatypeUnitTester.h"
#include "DClass.h"
#include "DClassAssembler.h"
#include "Definitions.h"
#include "DPointer.h"
#include "DPointerBase.h"
#include "DPointerInterface.h"
#include "DPointerTester.h"
#include "DProperty.h"
#include "DPropertyExtension.h"
#include "DString.h"
#include "Engine.h"
#include "EngineComponent.h"
#include "EngineIntegrityTester.h"
#include "EngineIntegrityUnitTester.h"
#include "Entity.h"
#include "EntityComponent.h"
#include "INT.h"
#include "LifeSpanComponent.h"
#include "LogCategory.h"
#include "Matrix.h"
#include "MulticastDelegate.h"
#include "Object.h"
#include "ObjectIterator.h"
#include "PlatformMac.h"
#include "PlatformWindows.h"
#include "PropertyIterator.h"
#include "Range.h"
#include "Rectangle.h"
#include "Rotator.h"
#include "SDFLOAT.h"
#include "SDFunction.h"
#include "SDFunctionTester.h"
#include "Stopwatch.h"
#include "StringIterator.h"
#include "TickComponent.h"
#include "TickGroup.h"
#include "TickTester.h"
#include "TransformMatrix.h"
#include "UnitTester.h"
#include "UnitTestLauncher.h"
#include "Utils.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Version.h"

