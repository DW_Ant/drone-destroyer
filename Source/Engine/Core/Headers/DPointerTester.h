/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DPointerTester.h
  Entity that simply references other DPointerTesters.

  This class is intended to test the DPointer property, and is only available in debug builds.
=====================================================================
*/

#pragma once

#include "Entity.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API DPointerTester : public Entity
{
	DECLARE_CLASS(DPointerTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The primary test pointer.  This is used for testing accessing, assignment, and clearing. */
	DPointer<DPointerTester> OtherTester;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
};

/**
 * Simple pointer tester that can be copied and compared to other testers.
 * This is similar to the PointerTester with the exception that this object can be copied and compared against other instances.
 */
class PrimitivePointerTester : public DPointerInterface
{
	
	
	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The primary test pointer.  This is used for testing accessing, assignment, and clearing. */
	DPointer<DPointerTester> OtherTester;

private:
	mutable DPointerBase* LeadingPointer;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool CanBePointed () const override;
	virtual void SetLeadingDPointer (DPointerBase* newLeadingPointer) const override;
	virtual DPointerBase* GetLeadingDPointer () const override;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	PrimitivePointerTester ();
	PrimitivePointerTester (const PrimitivePointerTester& other);
	virtual ~PrimitivePointerTester ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	bool operator== (const PrimitivePointerTester& other) const;
	bool operator!= (const PrimitivePointerTester& other) const;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual DString ToString () const;
};
SD_END

#endif