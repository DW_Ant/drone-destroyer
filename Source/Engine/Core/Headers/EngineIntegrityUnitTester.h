/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EngineIntegrityUnitTester.h
  This class is responsible for conducting unit tests for object handling.
  It'll test object instantiation, object iterators, class iterators,
  object clean up, object references, and the object hash table.
=====================================================================
*/

#pragma once

#include "UnitTester.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API EngineIntegrityUnitTester : public UnitTester
{
	DECLARE_CLASS(EngineIntegrityUnitTester)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool RunTests (EUnitTestFlags testFlags) const override;
	virtual bool MetRequirements (const std::vector<const UnitTester*>& completedTests) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Instantiates and kicks off the tick tester.  Returns true if the test launched.
	 */
	virtual bool LaunchTickTester (EUnitTestFlags testFlags) const;
};
SD_END

#endif //debug_mode