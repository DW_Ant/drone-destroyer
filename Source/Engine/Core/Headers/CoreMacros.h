/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CoreMacros.h
  Contains essential macros to be used for many objects.
=====================================================================
*/

#pragma once

#define STRINGIFY_IMPL(x) #x
#define STRINGIFY(x) STRINGIFY_IMPL(x)

#define SD_BEGIN namespace SD {
#define SD_END }

//Quick identifier to display that a particular parameter is supposed to be initialized by the time the function returns.
#ifndef OUT
#define OUT
#endif

/**
 * Short-cut to declare an inlined accessor function for a particular member variable.
 */
#define DEFINE_ACCESSOR(datatype, variableName) \
	inline datatype Get##variableName () const \
	{ \
		return variableName##; \
	}

#if (CHECK_CONFIG == 2)
/**
 * Macro used to cause an assertion if the condition fails, and displays helpful debugging information.
 */
#define CHECK(condition) \
	CHECK_INFO(##condition##, );

/**
 * Same as CHECK but provides an additional parameter to display a custom message.
 */
#define CHECK_INFO(condition, message) \
	if (!(##condition##)) \
	{ \
		SD::OS_BreakExecution(); \
		SD::Engine* engine = SD::Engine::FindEngine(); \
		if (engine != nullptr) \
		{ \
			engine->FatalError("Assertion failed (" #condition ")\nFile:" STRINGIFY(__FILE__) "\nLine:" STRINGIFY(__LINE__) "\n" #message ); \
		} \
		assert(false); \
	} \
	else {}
#elif (CHECK_CONFIG == 1)
//Log it instead of forcing a crash.
#define CHECK(condition) \
	CHECK_INFO(##condition##, );

#define CHECK_INFO(condition, message) \
	if (!(##condition##)) \
	{ \
		SD::CoreLog.Log(SD::LogCategory::LL_Critical, "Check condition failed (" #condition ")\nFile:" STRINGIFY(__FILE__) "\nLine:" STRINGIFY(__LINE__) "\n" #message); \
	} \
	else {}
#else
//Disable CHECK macros
#define CHECK(condition)
#define CHECK_INFO(condition, message)
#endif

#if ENABLE_COMPLEX_CHECKING
#define CHECK_SLOW(condition) CHECK(##condition##)
#else
#define CHECK_SLOW(condition)
#endif

/**
 * Macro that'll generate bitwise functions for the specified enumerator.
 */
#define DEFINE_ENUM_FUNCTIONS(enumName) \
/* Bitwise AND */ \
inline enumName operator& (##enumName left, enumName right) \
{ \
	return static_cast<##enumName##>(static_cast<int>(left) & static_cast<int>(right)); \
} \
/* Bitwise OR */ \
inline enumName operator| (##enumName left, enumName right) \
{ \
	return static_cast<##enumName##>(static_cast<int>(left) | static_cast<int>(right)); \
} \
/* Bitwise XOR */ \
inline enumName operator^ (##enumName left, enumName right) \
{ \
	return static_cast<##enumName##>(static_cast<int>(left) ^ static_cast<int>(right)); \
} \
/* Bitwise NOT */ \
inline enumName operator~ (##enumName flags) \
{ \
	return static_cast<##enumName##>(~static_cast<int>(flags)); \
} \
inline enumName##& operator&= (##enumName##& left, enumName right) \
{ \
	return left = (left & right); \
} \
inline enumName##& operator|= (##enumName##& left, enumName right) \
{ \
	return left = (left | right); \
} \
inline enumName##& operator^= (##enumName##& left, enumName right) \
{ \
	return left = (left ^ right); \
}

/**
 * Short-cut to declare public methods for a class (this goes in header files).
 */
#define DECLARE_CLASS(className) \
	private: \
		/** \
		  Instantiates and registers a DClass object that'll represent this class. \
		 */ \
		static const SD::DClass* InitializeStaticClass (); \
		\
	public: \
		static const SD::DClass* className##Class; \
		\
		/** \
		 * Obtains the associated DClass object with this object. \
		 * This object is available in all threads, but it's only editable in main thread. \
		 */ \
		virtual inline const SD::DClass* StaticClass () const \
		{ \
			return className##Class; \
		} \
		\
		/** \
		  Static method to obtain the DClass. \
		 */ \
		static inline const SD::DClass* SStaticClass () \
		{ \
			return className##Class; \
		} \
		\
		/** \
		  Static version:  Returns the object instance registered to this class's DClass. \
		  Abstract classes will nullptr. \
		 */ \
		static inline const SD::Object* SGetDefaultObject () \
		{ \
			return className##Class->GetDefaultObject(); \
		} \
		\
		/** \
		  Instantiates an object of this class. \
		 */ \
		static className##* CreateObject (); \
		\
		/** \
		  Instantiates a class that matches this object's class.  This is useful if you have a pointer \
		  to an object, but you do not know its class at design time. \
		 */ \
		virtual inline className##* CreateObjectOfMatchingClass () const \
		{ \
			return className##::CreateObject(); \
		} \
	/* Restore C++ class access-specifier defaults. */ \
	private:

/**
 * Inserts general properties and functions for an Engine Component.
 * All subclasses of EngineComponents are expected to be singleton objects.
 */
#define DECLARE_ENGINE_COMPONENT(className) \
	private: \
		/* Static class reference of this Engine Component. */ \
		static const SD::DClass* className##Class; \
		\
		/* Unique name that distinguishes this component from other components without having a reference to an instance. */ \
		static SD::DString className##Name; \
	\
		/* List of Registered Engine Components that are registered a Engine.  Each EngineComponent is mapped to a thread id they were instantiated from.  Any thread may access this map. */ \
		static std::map<std::thread::id, className##*> className##RegisteredComponents; \
	\
		/* Mutex associated with the RegisteredComponents vector.  This is used when the vector is accessed or edited. */ \
		static std::mutex className##RegisteredComponentsMutex; \
	\
		static const SD::DClass* InitializeStaticClass (); \
	\
		/** Registers this EngineComponent instance to the class-specific RegisteredComponents map. */ \
		virtual void RegisterEngineComponentInstance (); \
	\
		/** Removes this EngineComponent instance from the class-specific RegisteredComponents map. */ \
		virtual void RemoveEngineComponentInstance (); \
	\
	public: \
		static SD::DString GetComponentName (); \
		\
		/** \
		  Returns the EngineComponent registered to the Engine in the current thread. \
		 */ \
		static className##* Find (); \
		static className##* Find (std::thread::id threadId); \
		static className##* FindRegisteredComponent (std::thread::id threadId); \
		\
		virtual SD::DString GetName () const; \
		\
		/** \
		 * Obtains the associated DClass object with this object. \
		 * This object is available in all threads, but it's only editable in main thread. \
		 */ \
		virtual inline const SD::DClass* StaticClass () const \
		{ \
			return className##Class; \
		} \
		\
		/** \
		  Retrieves the DClass. \
		 */ \
		static inline const SD::DClass* SStaticClass () \
		{ \
			return className##Class; \
		} \
		\
		virtual inline className##* CreateObjectOfMatchingClass () const \
		{ \
			return new className##(); \
		} \
	/* Restore C++ class access-specifier defaults. */ \
	private:



/**
 * Short-cut to associate a DClass with this class (this goes in the cpp file)
 * These macros expect that your class contains a function called InitProps for the DClass::DefaultObject pointer
 * These macros also expect that your parent class to contain a function called InitializeObject.
 * Use IMPLEMENT_CLASS to define a standard class with a parent.
 * Use IMPLEMENT_ABSTRACT_CLASS to define an abstract class with a parent.
 * Use IMPLEMENT_CLASS_NO_PARENT to define a standard class without a parent.
 * Use IMPLEMENT_ABSTRACT_CLASS_NO_PARENT to define an abstract class without a parent.
 */
#define IMPLEMENT_CLASS(classNamespace, className, parentNamespace, parentClass) \
	/* Ensure this class is actually a subclass of parentClass. */ \
	static_assert(std::is_base_of<##parentNamespace##::##parentClass, classNamespace##::##className##>::value, #classNamespace "::" #className " does not derive from " #parentNamespace "::" #parentClass ".  Please advise the parentNamespace and parentClass parameters for the IMPLEMENT_CLASS macro."); \
	const SD::DClass* className##::##className##Class = InitializeStaticClass(); \
	\
	typedef parentNamespace##::##parentClass Super; \
	\
	IMPLEMENT_ANY_CLASS(className) \
	\
	IMPLEMENT_STATIC_CLASS(##classNamespace##, className##, parentNamespace##, parentClass##)


#define IMPLEMENT_ABSTRACT_CLASS(classNamespace, className, parentNamespace, parentClass) \
	/* Ensure this class is actually a subclass of parentClass. */ \
	static_assert(std::is_base_of<##parentNamespace##::##parentClass, classNamespace##::##className##>::value, #classNamespace "::" #className " does not derive from " #parentNamespace "::" #parentClass ".  Please advise the parentNamespace and parentClass parameters for the IMPLEMENT_ABSTRACT_CLASS macro."); \
	const SD::DClass* className##::##className##Class = InitializeStaticClass(); \
	\
	typedef parentNamespace##::##parentClass Super; \
	\
	IMPLEMENT_ANY_CLASS(className) \
	\
	IMPLEMENT_STATIC_ABSTRACT_CLASS(##classNamespace##, className##, parentNamespace##, parentClass##)
	

//short-cut to create an instance of DClass without a parent class
#define IMPLEMENT_CLASS_NO_PARENT(classNamespace, className) \
	const SD::DClass* className##::##className##Class = InitializeStaticClass(); \
	\
	IMPLEMENT_ANY_CLASS(className) \
	\
	IMPLEMENT_STATIC_CLASS(##classNamespace, className##, , )


#define IMPLEMENT_ABSTRACT_CLASS_NO_PARENT(classNamespace, className) \
	const SD::DClass* className##::##className##Class = InitializeStaticClass(); \
	\
	IMPLEMENT_ANY_CLASS(className) \
	\
	IMPLEMENT_STATIC_ABSTRACT_CLASS(##classNamespace##, className##, , )

/**
 * Macro used in all IMPLEMENT_CLASS macros.
 * Do NOT call this directly.  Use IMPLEMENT_CLASS... macros instead.
 */
#define IMPLEMENT_ANY_CLASS(className)


/**
 * These macros implement the InitializeStaticClass function.  There are two versions.
 * The standard one registers an instanced DClass to the class tree, and instantiates a default object.
 * The other one is for abstract classes where it registers a DClass to the tree, but does not create a default object.
 * Do NOT call this directly.  Use IMPLEMENT_CLASS... macros instead.
 */
#define IMPLEMENT_STATIC_CLASS(classNamespace, className, parentNamespace, parentClass) \
	const SD::DClass* className##::InitializeStaticClass () \
	{ \
		SD::DClass* result = SD::DClassAssembler::LoadClass( #classNamespace, #className, #parentNamespace, #parentClass); \
		\
		if (result != nullptr) \
		{ \
			/* Instantiate a defaultObject.  It's intentional to exclude this object from the Engine's hash table so that the */ \
			/* object iterators will not include these objects since we don't want these objects to tick, render, or directly impact the game. */ \
			/* This will generate a compiler error if an abstract class uses this macro.  Use IMPLEMENT_ABSTRACT_CLASS instead. */ \
			className##* defaultObject = new className##(); \
			\
			if (defaultObject != nullptr) \
			{ \
				defaultObject->InitProps(); \
				result->SetCDO(defaultObject); \
			} \
		} \
		\
		return result; \
	} \
	\
	className##* className##::CreateObject () \
	{ \
		className##* newObject = new className##(); \
		CHECK_INFO(newObject != nullptr, "Failed to allocate memory to instantiate a " STRINGIFY(#className) ".  Line:" STRINGIFY(__LINE__) ", File:" STRINGIFY(__FILE__)) \
		\
		newObject->InitializeObject(); \
		\
		return newObject; \
	}


/**
 * Since abstract classes cannot be instantiated, we must have a separate macro to skip the defaultObject implementation.
 * Do NOT call this directly.  Use IMPLEMENT_CLASS... macros instead.
 */
#define IMPLEMENT_STATIC_ABSTRACT_CLASS(classNamespace, className, parentNamespace, parentClass) \
	const SD::DClass* className##::InitializeStaticClass () \
	{ \
		/* Insert a compile error if the nonabstract class is using a macro for abstract classes. */ \
		/* If you receive a compile error stating "is_abstract undeclared identifier", then you'll need to place the macro within std blocks. */ \
		static_assert(std::is_abstract<##classNamespace##::##className##>::value, #classNamespace "::" #className " is not an abstract class but IMPLMENT_ABSTRACT_CLASS macro was used."); \
		return SD::DClassAssembler::LoadClass( #classNamespace, #className, #parentNamespace, #parentClass); \
	} \
	\
	className##* className##::CreateObject () \
	{ \
		SD::CoreLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot instantiate an abstract class:  " #classNamespace "::" #className)); \
		/* Cannot instantiate an abstract class. */ \
		return nullptr; \
	}


/**
 * This macro defines boiler-plate code for the specified EngineComponent.
 */
#define IMPLEMENT_ENGINE_COMPONENT(classNamespace, className) \
	typedef SD::EngineComponent Super; \
	IMPLEMENT_ENGINE_COMPONENT_PARENT(##classNamespace##, className##, SD##, EngineComponent##)

#define IMPLEMENT_ENGINE_COMPONENT_PARENT(classNamespace, className, parentNamespace, parentClass) \
	SD::DString className##::##className##Name = #className; \
	const SD::DClass* className##::##className##Class = InitializeStaticClass(); \
	\
	std::map<std::thread::id, className##*> className##::##className##RegisteredComponents = std::map<std::thread::id, className##*>(); \
	std::mutex className##::##className##RegisteredComponentsMutex; \
	\
	const SD::DClass* className##::InitializeStaticClass () \
	{ \
		SD::DClass* result = SD::DClassAssembler::LoadClass( #classNamespace, #className, #parentNamespace, #parentClass); \
		if (result != nullptr) \
		{ \
			className##* cdo = new className##(); \
			result->SetCDO(cdo); \
		} \
	\
		return result; \
	} \
	void className##::RegisterEngineComponentInstance () \
	{ \
		std::lock_guard<std::mutex> guard(##className##RegisteredComponentsMutex); \
		className##RegisteredComponents.emplace(std::make_pair(std::this_thread::get_id(), this)); \
	} \
	\
	void className##::RemoveEngineComponentInstance () \
	{ \
		CHECK(GetOwningEngine() != nullptr) \
		std::lock_guard<std::mutex> guard(##className##RegisteredComponentsMutex); \
		className##RegisteredComponents.erase(GetOwningEngine()->GetThreadID()); \
	} \
	\
	SD::DString className##::GetComponentName () \
	{ \
		return className##Name; \
	} \
	\
	className##* className##::Find () \
	{ \
		return FindRegisteredComponent(std::this_thread::get_id()); \
	} \
	\
	className##* className##::Find (std::thread::id threadId) \
	{ \
		return FindRegisteredComponent(threadId); \
	} \
	\
	className##* className##::FindRegisteredComponent (std::thread::id threadId) \
	{ \
		{ \
			std::lock_guard<std::mutex> guard(##className##RegisteredComponentsMutex); \
			std::map<std::thread::id, className##*>::iterator result = className##RegisteredComponents.find(threadId); \
			if (result != className##RegisteredComponents.end()) \
			{ \
				return result->second; \
			} \
		} \
		\
		SD::CoreLog.Log(SD::LogCategory::LL_Warning, TXT("A " #className " does not reside in the current thread.")); \
		return nullptr; \
	} \
	\
	SD::DString className##::GetName () const \
	{ \
		return className##Name; \
	}