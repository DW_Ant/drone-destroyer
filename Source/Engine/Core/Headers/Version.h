/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Version.h
  Defines the version number may be used to help identify what version Sand Dune is running in.

  The version number should reflect the version number of Sand Dune's master branch.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class CORE_API Version
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The major version number increments every time there's a new major release in the master branch. */
	static const INT SAND_DUNE_MAJOR_VERSION;

	/* The minor version number increments for every hot fix released within a major version. */
	static const INT SAND_DUNE_MINOR_VERSION;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Generates a formatted string of Sand Dune's version.
	 * Format:  "Major.Minor"
	 */
	static DString GetVersionNumber ();
};
SD_END