/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Core.h
  Contains important file includes and definitions for the Core module.

  The Core module includes essential classes and utilities majority of
  other modules require.  The Core module defines the following:

	- Global definitions (ie:  platforms and environment)
	- Commonly used header includes (ie:  string.h, math.h)
	- Essential Macros (DECLARE_CLASS, IMPLEMENT_CLASS)
	- Data types (INTs, FLOATs, Vectors)
	- Engine, Engine Component, and DClass implementations
	- Primitive classes such as Object, Entity, and Entity Component
=====================================================================
*/

#pragma once

#include "Configuration.h"

#ifndef __cplusplus
#error A C++ compiler is required to compile the Core module.
#endif

#if __cplusplus < 199711L
#error Your compiler must support C++11 to be able to compile the Core module.
#endif

#ifdef _WIN32
//Notify the Engine that we're using a Windows platform (true for both 32 and 64 bit)
#define PLATFORM_WINDOWS
#elif __APPLE__
//Notify the Engine that we're using a Mac platform
#define PLATFORM_MAC
#else
#error Please define your platform here.
#endif

//common includes
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <mutex>
#include <algorithm>
#include <vector>
#include <map>
#include <math.h>
#include <limits>
#include <functional> //C++11 function callbacks
#include <type_traits> //used to check abstract classes

#include <SFML/System.hpp>

//Check Solution Configuration
#ifdef _DEBUG
#define DEBUG_MODE
#endif

//Configuration-specific common includes
#ifdef DEBUG_MODE
#include <cassert>
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef CORE_EXPORT
		#define CORE_API __declspec(dllexport)
	#else
		#define CORE_API __declspec(dllimport)
	#endif
#else
	#define CORE_EXPORT
#endif

//Platform-specific includes
#ifdef PLATFORM_WINDOWS
#include "PlatformWindows.h"
#elif defined(PLATFORM_MAC)
#include "PlatformMac.h"
#endif

#include "CoreMacros.h"