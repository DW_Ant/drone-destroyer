/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Rectangle.h
  A datatype responsible for tracking four borders (left, top, right, bottom).

  Although a rectangle can be defined by a simple height and width, this rectangle
  stores location data as well so coordinate systems may leverage this object as well.

  The Rectangle's coordinate system is agnostic, and will only make sense against other
  rectangles that uses the same coordinate system.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#include "CoreDatatypes.h"

SD_BEGIN
class Vector2;

template <class T>
class Rectangle : public DProperty
{


	/*
	=====================
	  Datatypes
	=====================
	*/

public:
	enum ECoordinateSystem
	{
		CS_XRightYUp,
		CS_XRightYDown
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	T Left;
	T Top;
	T Right;
	T Bottom;

private:
	ECoordinateSystem CoordinateSystem;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Rectangle (ECoordinateSystem inCoordinateSystem = CS_XRightYUp)
	{
		Left = static_cast<T>(0);
		Top = static_cast<T>(0);
		Right = static_cast<T>(0);
		Bottom = static_cast<T>(0);
		CoordinateSystem = inCoordinateSystem;
	}

	Rectangle (const T& inLeft, const T& inTop, const T& inRight, const T& inBottom, ECoordinateSystem inCoordinateSystem = CS_XRightYUp)
	{
		Left = inLeft;
		Top = inTop;
		Right = inRight;
		Bottom = inBottom;
		CoordinateSystem = inCoordinateSystem;
	}

	Rectangle (const Rectangle& copyRectangle)
	{
		Left = copyRectangle.Left;
		Top = copyRectangle.Top;
		Right = copyRectangle.Right;
		Bottom = copyRectangle.Bottom;
		CoordinateSystem = copyRectangle.CoordinateSystem;
	}

		
	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Rectangle& copyRectangle)
	{
		Left = copyRectangle.Left;
		Top = copyRectangle.Top;
		Right = copyRectangle.Right;
		Bottom = copyRectangle.Bottom;
		CoordinateSystem = copyRectangle.CoordinateSystem;
	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual DString ToString () const override
	{
		return (TXT("Left(") + Left.ToString() + TXT(") Top(") + Top.ToString() + TXT(") Right(") + Right.ToString() + TXT(") Bottom(") + Bottom.ToString() + TXT(")"));
	}

	virtual void Serialize (DataBuffer& dataBuffer) const override
	{
		dataBuffer << Left;
		dataBuffer << Top;
		dataBuffer << Right;
		dataBuffer << Bottom;

		INT coordinateSystemNum(static_cast<int>(CoordinateSystem));
		dataBuffer << coordinateSystemNum;
	}

	virtual void Deserialize (const DataBuffer& dataBuffer) override
	{
		dataBuffer >> Left;
		dataBuffer >> Top;
		dataBuffer >> Right;
		dataBuffer >> Bottom;

		INT coordinateSystemNum;
		dataBuffer >> coordinateSystemNum;
		CoordinateSystem = static_cast<ECoordinateSystem>(coordinateSystemNum.Value);
	}

		
	/*
	=====================
	  Methods
	=====================
	*/

public:
	T CalculateArea () const
	{
		return GetWidth() * GetHeight();
	}

	T CalculatePerimeter () const
	{
		return (GetWidth() * 2) + (GetHeight() * 2);
	}

	/**
	 * Returns true if this rectangle is a square.
	 */
	bool IsSquare () const
	{
		return (GetWidth() == GetHeight());
	}

	/**
	 * Returns true if all borders of this rectangle is zero.
	 */
	bool IsEmpty () const
	{
		return (GetWidth() == 0 && GetHeight() == 0);
	}

	/**
	 * Returns true if this Rectangle's coordinate system matches the other Rectangle's coordinate system.
	 */
	bool IsUsingSameCoordinateSystemAs (const Rectangle<T>& otherRectangle) const
	{
		return (CoordinateSystem == otherRectangle.CoordinateSystem);
	}

	/**
	 * Calculates the overlapping rectangle.
	 * Returns an empty rectangle if the two rectangles do not overlap.
	 */
	Rectangle GetOverlappingRectangle (const Rectangle& otherRectangle) const
	{
		CHECK(IsUsingSameCoordinateSystemAs(otherRectangle))

		if (!Overlaps(otherRectangle))
		{
			return Rectangle(CoordinateSystem);
		}

		Rectangle result(CoordinateSystem);

		if (CoordinateSystem == CS_XRightYUp)
		{
			result.Top = Utils::Min(Top, otherRectangle.Top);
			result.Bottom = Utils::Max(Bottom, otherRectangle.Bottom);
		}
		else //XRightYDown
		{
			result.Top = Utils::Max(Top, otherRectangle.Top);
			result.Bottom = Utils::Min(Bottom, otherRectangle.Bottom);
		}

		result.Left = Utils::Max(Left, otherRectangle.Left);
		result.Right = Utils::Min(Right, otherRectangle.Right);

		return result;
	}

	/**
	 * Returns true if this rectangle overlaps with other rectangle.
	 */
	bool Overlaps (const Rectangle& otherRectangle) const
	{
		CHECK(IsUsingSameCoordinateSystemAs(otherRectangle));

		if (CoordinateSystem == CS_XRightYUp)
		{
			if (Top <= otherRectangle.Bottom || Bottom >= otherRectangle.Top)
			{
				//Rectangle is below/above the other rectangle.
				return false;
			}
		}
		else //XRightYDown
		{
			if (Top >= otherRectangle.Bottom || Bottom <= otherRectangle.Top)
			{
				//Rectangle is below/above the other rectangle.
				return false;
			}
		}

		if (Left >= otherRectangle.Right || Right <= otherRectangle.Left)
		{
			//Rectangle is left/right of other rectangle.
			return false;
		}

		return true;
	}

	/**
	 * Returns true if the given point is within this rectangle.
	 */
	bool EncompassesPoint (const Vector2& targetPoint) const
	{
		if (CoordinateSystem == CS_XRightYUp)
		{
			if (targetPoint.Y > static_cast<FLOAT>(Top) || targetPoint.Y < static_cast<FLOAT>(Bottom))
			{
				return false;
			}
		}
		else //XRightYDown
		{
			if (targetPoint.Y < static_cast<FLOAT>(Top) || targetPoint.Y > static_cast<FLOAT>(Bottom))
			{
				return false;
			}
		}

		return (targetPoint.X >= static_cast<FLOAT>(Left) && targetPoint.X <= static_cast<FLOAT>(Right));
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	T GetWidth () const
	{
		return T::Abs((Right - Left));
	}

	T GetHeight () const
	{
		return T::Abs((Bottom - Top));
	}

	ECoordinateSystem GetCoordinateSystem () const
	{
		return CoordinateSystem;
	}


	/*
	=====================
	  Templates
	=====================
	*/

public:
	template <class Type> Rectangle operator* (const Type& scaler)
	{
		return Rectangle(Left * scaler, Top * scaler, Right * scaler, Bottom * scaler);
	}

	template <class Type> void operator*= (const Type& scaler)
	{
		Left *= scaler;
		Top *= scaler;
		Right *= scaler;
		Bottom *= scaler;
	}

	template <class Type> Vector2 operator/ (const Type& scaler)
	{
		if (scaler != 0)
		{
			return Rectangle(Left / scaler, Top / scaler, Right / scaler, Bottom / scaler);
		}

		return this;
	}

	template <class Type> void operator/= (const Type& scaler)
	{
		if (scaler != 0)
		{
			Left /= scaler;
			Top /= scaler;
			Right /= scaler;
			Bottom /= scaler;
		}
	}
};

#pragma region "External Operators"
	template<class T>
	bool operator== (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		return (left.Top == right.Top && left.Right == right.Right && left.Bottom == right.Bottom && left.Left == right.Left && left.GetCoordinateSystem() == right.GetCoordinateSystem());
	}

	template<class T>
	bool operator!= (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		return !(left == right);
	}

	template<class T>
	Rectangle<T> operator+ (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		return Rectangle<T>(left.Left + right.Left, left.Top + right.Top, left.Right + right.Right, left.Bottom + right.Bottom);
	}

	template<class T>
	Rectangle<T>& operator+= (Rectangle<T>& left, const Rectangle<T>& right)
	{
		left.Left += right.Left;
		left.Top += right.Top;
		left.Right += right.Right;
		left.Bottom += right.Bottom;
		return left;
	}

	template<class T>
	Rectangle<T> operator- (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		return Rectangle<T>(left.Left - right.Left, left.Top - right.Top, left.Right - right.Right, left.Bottom - right.Bottom);
	}

	template<class T>
	Rectangle<T>& operator-= (Rectangle<T>& left, const Rectangle<T>& right)
	{
		left.Left -= right.Left;
		left.Top -= right.Top;
		left.Right -= right.Right;
		left.Bottom -= right.Bottom;
		return left;
	}

	template<class T>
	Rectangle<T> operator* (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		return Rectangle<T>(left.Left * right.Left, left.Top * right.Top, left.Right * right.Right, left.Bottom * right.Bottom);
	}

	template<class T>
	Rectangle<T>& operator*= (Rectangle<T>& left, const Rectangle<T>& right)
	{
		left.Left *= right.Left;
		left.Top *= right.Top;
		left.Right *= right.Right;
		left.Bottom *= right.Bottom;
		return left;
	}

	template<class T>
	Rectangle<T> operator/ (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		Rectangle<T> result(left);
		result /= right;
		return result;
	}

	template<class T>
	Rectangle<T>& operator/= (Rectangle<T>& left, const Rectangle<T>& right)
	{
		if (right.Left != 0.f)
		{
			left.Left /= right.Left;
		}

		if (right.Top != 0.f)
		{
			left.Top /= right.Top;
		}

		if (right.Right != 0.f)
		{
			left.Right /= right.Right;
		}

		if (right.Bottom != 0.f)
		{
			left.Bottom /= right.Bottom;
		}

		return left;
	}
#pragma endregion
SD_END