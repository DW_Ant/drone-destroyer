/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DClass.h
  At startup time, the DClass will generate a class tree of all classes
  (with DECLARE_CLASS macro).  The DClasses may possess a reference to the
  parent and children DClasses.

  NOTE:  You can still have multi-inherited classes.  This class is mostly
  used to help iterate through classes, and obtain default objects from each class.
=====================================================================
*/

#pragma once

#include "Core.h"

#include "DString.h"

SD_BEGIN
class Object;
class EngineComponent;

class CORE_API DClass
{


	/*
	=====================
	  Data types
	=====================
	*/

public:
	enum EClassType
	{
		CT_None,
		CT_Object,
		CT_EngineComponent,
		CT_Other
	};

protected:
	struct SOrphanInfo
	{
		DClass* ClassInstance;
		DString ParentClass;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Type of class this DClass refers to, and helps determine which default object pointer in DefaultInstance union is used. */
	enum EClassType ClassType;

protected:
	/* Readable name of class name.  This was renamed from ClassName to DuneClassName to avoid naming conflict with Microsoft's GetClassName macro. */
	DString DuneClassName;
	DClass* ParentClass;

	/* List of all direct subclasses (does not include children subclasses).  This is sorted alphabetically by DuneClassName. */
	std::vector<DClass*> Children;

	/* Remains true until a CDO instance is associated with this object. */
	bool IsAbstractClass;

	union DefaultInstance
	{
		/* Reference to an instantiated object that simply contains a reference to its default properties,
		and is not meant for any modifications at run time.*/
		Object* DefaultObject;
		EngineComponent* DefaultEngineComponent;
	} CDO;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	DClass (const DString& className);

	virtual ~DClass ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Updates the DefaultObject's value if it's not set already.
	 * This DefaultObject will automatically be deleted when this DClass instance is destroyed.
	 */
	virtual void SetCDO (Object* newCDO);
	virtual void SetCDO (EngineComponent* newCDO);

	/**
	 * Returns true if the parameter is equal to or a parent class of the current class.
	 */
	virtual bool IsChildOf (const DClass* targetParentClass) const;

	/**
	 * Returns true if the parameter is equal to or a child of the current class.
	 */
	virtual bool IsParentOf (const DClass* targetChildClass) const;

	inline bool IsA (const DClass* targetChildClass) const
	{
		return IsChildOf(targetChildClass);
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(DString, DuneClassName)

	virtual DString ToString () const;

	const DClass* GetSuperClass () const;
	inline bool IsAbstract () const
	{
		return IsAbstractClass;
	}

	const Object* GetDefaultObject () const;
	const EngineComponent* GetDefaultEngineComponent () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	void AddChild (DClass* child);

	friend class DClassAssembler;
	friend class ClassIterator;
};
SD_END