/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MulticastDelegate.h

  A class that is responsible for managing and broadcasting a collection of SD Functions.

  It is safe to add or remove handlers from the MulticastDelegate even if it's in the middle of
  broadcasting an event.
=====================================================================
*/

#pragma once

#include "SDFunction.h"

SD_BEGIN
template <class R /*returnType*/, class ... P /*params*/>
class MulticastDelegate
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The list of functions to execute whenever it broadcasts. */
	std::vector<SDFunction<R, P...>> Handlers;

	/* Becomes true if this object is in the middle of broadcasting.
	The Handlers are locked until the broadcast is finished. */
	bool Locked;

	/* List of Handlers that are pending registration. These are added when something
	tries to register a handler while it was locked. These will automatically subscribe to the 
	Handlers vector as soon as it unlocks. */
	std::vector<SDFunction<R, P...>> PendingAddHandlers;

	/* List of Handlers that are pending removal. These are added whne something
	tries to unregister a handler whil it was locked. These will automatically unscribe from
	the Handlers vector as soon as it unlocks. */
	std::vector<SDFunction<R, P...>> PendingRemovalHandlers;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	MulticastDelegate () :
		Locked(false)
	{
		//Noop
	}

	~MulticastDelegate ()
	{
		CHECK_INFO(!IsLocked(), TXT("A MulticastDelegate is destroyed while it's broadcasting."))
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Attempts to register the given callback at the end of the handler list.
	 * The function should be unique compared to what's already registered.
	 */
	void RegisterHandler (SDFunction<R, P...> newHandler)
	{
		CHECK(newHandler.IsBounded())
		if (IsLocked())
		{
			PendingAddHandlers.push_back(newHandler);
			return;
		}

#if ENABLE_COMPLEX_CHECKING
		for (size_t i = 0; i < Handlers.size(); ++i)
		{
			CHECK(Handlers.at(i) != newHandler) //Ensure we're not registering the same callback more than once.
		}
#endif
	
		Handlers.push_back(newHandler);
	}

	/**
	 * Returns true if the handler is currently registered.
	 * If checkPending is true, then it'll only return true if it's registered by the time it unlocks.
	 */
	bool IsRegistered (SDFunction<R, P...>  target, bool checkPending) const
	{
		bool currentlyRegistered = false;
		for (size_t i = 0; i < Handlers.size(); ++i)
		{
			if (Handlers.at(i) == target)
			{
				if (!IsLocked())
				{
					return true;
				}

				currentlyRegistered = true;
				break;
			}
		}

		if (!checkPending)
		{
			return currentlyRegistered;
		}

		if (currentlyRegistered)
		{
			for (size_t i = 0; i < PendingRemovalHandlers.size(); ++i)
			{
				if (PendingRemovalHandlers.at(i) == target)
				{
					return false;
				}
			}
		}
		else
		{
			for (size_t i = 0; i < PendingAddHandlers.size(); ++i)
			{
				if (PendingAddHandlers.at(i) == target)
				{
					return true;
				}
			}
		}

		return currentlyRegistered;
	}

	/**
	 * Searches the handler list to try to remove it.
	 */
	void UnregisterHandler (SDFunction<R, P...> target)
	{
		CHECK(target.IsBounded())
		if (IsLocked())
		{
			PendingRemovalHandlers.push_back(target);
			return;
		}

		for (size_t i = 0; i < Handlers.size(); ++i)
		{
			if (Handlers.at(i) == target)
			{
				Handlers.erase(Handlers.begin() + i);
				return;
			}
		}

		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to remove %s from the handler list since it's not found."), target);
	}

	/**
	 * Iterates through each handler to invoke them.
	 */
	void Broadcast (P... params)
	{
		if (IsLocked())
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Recursive Broadcast detected! MulticastDelegate::Broadcast is called while it's in the middle of a broadcast."));
			return;
		}

		Locked = true;
		for (size_t i = 0; i < Handlers.size(); ++i)
		{
			Handlers.at(i).Execute(params...);
		}
		Locked = false;

		for (size_t i = 0; i < PendingAddHandlers.size(); ++i)
		{
			RegisterHandler(PendingAddHandlers.at(i));
		}
		ContainerUtils::Empty(PendingAddHandlers);

		for (size_t i = 0; i < PendingRemovalHandlers.size(); ++i)
		{
			UnregisterHandler(PendingRemovalHandlers.at(i));
		}
		ContainerUtils::Empty(PendingRemovalHandlers);
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const std::vector<SDFunction<R, P...>>& ReadHandlers () const
	{
		return Handlers;
	}

	inline bool IsLocked () const
	{
		return Locked;
	}
};
SD_END