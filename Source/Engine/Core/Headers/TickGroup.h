/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TickGroup.h
  The TickGroup contains the definition about a collection of Tick components.  This is primarily used
  for handling Tick order, and allows an observer to measure how long it takes to process a group.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class TickComponent;

#define TICK_GROUP_SYSTEM "System" //Tick group related to interfacing with OS or Engine related functions
#define TICK_GROUP_PRIORITY_SYSTEM 50000
#define TICK_GROUP_MISC "Misc" //Uncategorized TickGroup
#define TICK_GROUP_PRIORITY_MISC 10000
#ifdef DEBUG_MODE
#define TICK_GROUP_DEBUG "Debug" //TickComponents used for Unit Tests and debugging
#define TICK_GROUP_PRIORITY_DEBUG 1000
#endif

class CORE_API TickGroup
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Name used to identify this group. */
	DString GroupName;

	/* Tick priority value relative to other TickGroups.  Higher priority TickGroups tick sooner before other groups in the Engine's tick cycle. */
	INT TickPriority;

	/* If true, then Engines may tick components within this group. */
	bool bTicking;

	/* List of TickComponents that belongs in this group.  The Tick order of these components should not depend on each other. */
	std::vector<TickComponent*> Ticks;

private:
	/* Becomes true when this group is in the middle of ticking.  Prevents removing TickComponents in the middle of the tick loop. */
	bool IsAccessingTickLoop;

	/* List of Ticks that needs to be unregistered. */
	std::vector<TickComponent*> PendingRemovalTicks;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	TickGroup (const DString& inGroupName, INT inTickPriority);
	virtual ~TickGroup ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adds the TickComponent to the list of Ticks.
	 */
	virtual void RegisterTick (TickComponent* newTick);

	/**
	 * Removes the TickComponent from the list of Ticks.
	 * If the Tick loop is accessed, then the targetTick will be removed when the it's done accessing it.
	 */
	virtual void RemoveTick (TickComponent* targetTick);

	virtual void SetTicking (bool bNewTicking);

	/**
	 * Ticks all registered TickComponents.
	 */
	virtual void TickRegisteredComponents (FLOAT deltaSec);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(DString, GroupName)
	DEFINE_ACCESSOR(INT, TickPriority)
	
	inline const DString& ReadGroupName () const
	{
		return GroupName;
	}

	inline bool IsTicking () const
	{
		return bTicking;
	}

	const std::vector<TickComponent*>& ReadTickComponents () const;
};
SD_END