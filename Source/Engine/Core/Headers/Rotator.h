/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Rotator.h
  A datatype that represents 3 dimensional Euler direction.  As the rotation increases, the rotator will rotate
  clockwise from the perspective of facing down the positive axis.

  The starting rotation (all axis equal to 0) begins facing down the X-axis.

  The rotators assume a left handed coordinate system where X is to the right and Z is up.

  The rotators internally are represented in integers to have a form of 'snapping' by the nearest
  ~0.0055 degrees.

  Additionally, the unsigned ints are used to automatically wrap around to 0 degrees once it passes one full revolution.

  Rotators also support converting unit types from and to degrees and radians.  Be mindful of precision errors
  when converting units.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#include "DProperty.h"
#include "SDFLOAT.h"
#include "Vector3.h"

SD_BEGIN
class TransformMatrix;

class CORE_API Rotator : public DProperty
{


	/*
	=====================
	  Datatypes
	=====================
	*/

public:
	enum ERotationUnit
	{
		RU_Degrees,
		RU_Radians
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Multiplier to use when converting degrees to radians. */
	static const FLOAT DEGREE_TO_RADIAN;

	/* Multiplier to use when converting radians to degrees. */
	static const FLOAT RADIAN_TO_DEGREE;
	
	/* Rotator where all rotation axis are zeroed (points along positive X-axis). */
	static const Rotator ZERO_ROTATOR;

	/* The amount of int units that makes up one entire revolution (360 degrees, 180 degrees, and 90 degrees respectively). */
	static const unsigned int FULL_REVOLUTION;
	static const unsigned int HALF_REVOLUTION;
	static const unsigned int QUARTER_REVOLUTION;

	/* Rotation about the Z-axis.  Positive turns left, negative turns right. Ranges from 0-65535. */
	unsigned int Yaw : 16;

	/* Rotation about the Y-axis.  Positive spins up, negative spins down. turns Ranges from 0-65535. */
	unsigned int Pitch : 16;

	/* Rotation about the X-axis.  Positive spins banks clockwise, Negative banks counterclockwise. Ranges from 0-65535. */
	unsigned int Roll : 16;

private:
	/* Extra buffer to fill in the remaining byte (for alignment purposes). */
	unsigned int Unused : 16;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Rotator ();

	Rotator (const Rotator& copyRotator);

	/* Constructs a rotator through SD rotation units. */
	Rotator (unsigned int inYaw, unsigned int inPitch, unsigned int inRoll);

	/**
	 * Constructs a rotator from the specified components.
	 * @param rotationUnit signals the constructor what units the parameters are using.
	 */
	Rotator (FLOAT inYaw, FLOAT inPitch, FLOAT inRoll, ERotationUnit rotationUnit);

	/* Constructs a rotator from the specified directional vectors.  Vectors are automatically normalized. */
	Rotator (Vector3 directionVector);

	/* Computes the Euler angles from a 4x4 transform matrix. */
	Rotator (const TransformMatrix& transformMatrix);

		
	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Rotator& copyRotator);
	void operator= (Vector3 directionVector);
		

	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual DString ToString () const override;
	virtual void Serialize (DataBuffer& dataBuffer) const override;
	virtual void Deserialize (const DataBuffer& dataBuffer) override;

		
	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Constructs a Rotator from signed integers.  Values are automatically wrapped (not clamped)
	 * to be within 0-65k.  Units are in SD units.
	 */
	static Rotator MakeRotator (INT inYaw, INT inPitch, INT inRoll);

	/**
	 * Sets the rotation from individual components.
	 */
	void SetRotation (FLOAT inYaw, FLOAT inPitch, FLOAT inRoll, ERotationUnit rotationUnit);

	void SetYaw (FLOAT inYaw, ERotationUnit rotationUnit);
	void SetPitch (FLOAT inPitch, ERotationUnit rotationUnit);
	void SetRoll (FLOAT inRoll, ERotationUnit rotationUnit);

	/**
	 * Returns true if the given rotator is perpendicular to this rotator.
	 */
	virtual bool IsPerpendicularTo (const Rotator& otherRotator) const;

	/**
	 * Returns true if the given rotator is parallel to this rotator.
	 */
	virtual bool IsParallelTo (const Rotator& otherRotator) const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns a unit length vector that points in the same direction as this rotator.
	 */
	virtual Vector3 GetDirectionalVector () const;

	void GetDegrees (FLOAT& outYaw, FLOAT& outPitch, FLOAT& outRoll) const;
	void GetRadians (FLOAT& outYaw, FLOAT& outPitch, FLOAT& outRoll) const;

	FLOAT GetYaw (ERotationUnit rotationUnit) const;
	FLOAT GetPitch (ERotationUnit rotationUnit) const;
	FLOAT GetRoll (ERotationUnit rotationUnit) const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Retrieves the multiplier that converts from Rotator to specified unit.
	 */
	FLOAT GetRotatorToUnitConverter (ERotationUnit rotationUnit) const;

	/**
	 * Retrieves the multiplier that converts from specified unit to rotator unit.
	 */
	FLOAT GetUnitToRotatorConverter (ERotationUnit rotationUnit) const;

	/**
	 * Sets Yaw, Pitch, Roll based on the direction of the given normalized vector.
	 * This function will prioritize yaw over pitch where it'll try to rotate along the XY plane before
	 * adjusting the pitch.
	 * This function assumes a left handed coordinate system where each axis are rotated clockwise.
	 * The roll is always 0.
	 */
	void SetComponentsFromVector (const Vector3& normalizedVector);
};

#pragma region "External Operators"
CORE_API bool operator== (const Rotator& left, const Rotator& right);
CORE_API bool operator!= (const Rotator& left, const Rotator& right);
CORE_API Rotator operator+ (const Rotator& left, const Rotator& right);
CORE_API Rotator& operator+= (Rotator& left, const Rotator& right);
CORE_API Rotator operator- (const Rotator& left, const Rotator& right);
CORE_API Rotator& operator-= (Rotator& left, const Rotator& right);
#pragma endregion
SD_END