/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  INT.h
  A class that represents a 64-bit integer (64-bit architectures only).
  For 32-bit architectures, this object represents a 32-bit integer.
  Contains numeric utility functions, and supports typecasting to various standard C++ numbers.

  Warning:  The size of INTs differs based on the platform architecture (32-bit and 64-bit).
  It was decided to have a variable size for this class since 64-bit ints are often used for looping (ie: vector sizes).
  It also makes better use of the 64-bit platform rather than using 32-bit ints.

  If the memory footprint is too large for your application, then it's recommended that you build a 32-bit application instead.
=====================================================================
*/

#pragma once

#include "Configuration.h"
#include "DProperty.h"

//Max possible value of 64/32-bit unsigned int
#define UINT64_INDEX_NONE 0xffffffffffffffff  
#define UINT32_INDEX_NONE 0xffffffff

#ifdef PLATFORM_64BIT
#define UINT_INDEX_NONE UINT64_INDEX_NONE
#define INDEX_NONE UINT64_INDEX_NONE
#else
#define UINT_INDEX_NONE UINT32_INDEX_NONE
#define INDEX_NONE UINT32_INDEX_NONE
#endif

#define INT_INDEX_NONE -1

//Need to create custom MAXINT define since Microsoft defined MAXINT to their INT in basetsd.h
#define SD_MAXINT INT_MAX

//Standard ints to use based on architecture
#ifdef PLATFORM_64BIT
#define INT_TYPE int64
#define UINT_TYPE uint64
#else
#define INT_TYPE int32
#define UINT_TYPE uint32
#endif

SD_BEGIN
class FLOAT;

class CORE_API INT : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	INT_TYPE Value;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	INT ();
#ifdef PLATFORM_32BIT
	INT (const int32 newValue);
#endif
	INT (const INT& copyInt);
	explicit INT (const uint32& newValue);
	explicit INT (const FLOAT& newValue);
	explicit INT (const DString& text);

#ifdef PLATFORM_64BIT
	INT (const int64 newValue);
	explicit INT (const int32 newValue);
	explicit INT (const uint64 newValue);
#endif

		
	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const INT& copyInt);
	void operator= (const int32 otherInt);
	void operator= (const uint32 otherInt);

#ifdef PLATFORM_64BIT
	void operator= (const int64 otherInt);
	void operator= (const uint64 otherInt);
#endif

	INT operator++ (); //++iter
	INT operator++ (int); //iter++

	INT operator- () const;
	INT operator-- ();
	INT operator-- (int);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual DString ToString () const override;
	virtual void Serialize (DataBuffer& dataBuffer) const override;
	virtual void Deserialize (const DataBuffer& dataBuffer) override;

		
	/*
	=====================
	  Methods
	=====================
	*/

public:
	static INT MakeINT (float value);

	/**
	 * Returns a FLOAT from this INT.
	 */
	virtual FLOAT ToFLOAT () const;

	int32 ToInt32 () const;

	inline bool IsEven () const
	{
		//Only check if the first bit is 0.
		return ((Value&1) == 0);
	}

	inline bool IsOdd () const
	{
		return !IsEven();
	}

	/**
	 * Returns the absolute value of this INT
	 */
	static INT Abs (INT value);
	virtual void AbsInline ();

	/**
	 * Converts the value to an int.  If greater than INT_MAX, then the value is set to the max int.
	 */
	static int32 GetInt (uint32 target);
#ifdef PLATFORM_64BIT
	static int64 GetInt (uint64 target);
#endif

	/**
	 * Returns a converted value of this INT to an unsigned int.  Negative values are converted to zero.
	 */
	virtual uint32 ToUnsignedInt32 () const;
#ifdef PLATFORM_64BIT
	virtual uint64 ToUnsignedInt64 () const;
#endif
	/**
	 * Returns either uint32 or uint64 based on platform configuration.
	 */
	virtual UINT_TYPE ToUnsignedInt () const;

	virtual INT NumDigits () const;

	/**
	 * Returns a string representation of this number with leading zeroes if the min digits requirement is not met.
	 */
	virtual DString ToFormattedString (const INT minDigits) const;
};

#pragma region "External Operators"
	CORE_API bool operator== (const INT& left, const INT& right);
	CORE_API bool operator== (const INT& left, const INT_TYPE& right);
	CORE_API bool operator== (const INT_TYPE& left, const INT& right);

	CORE_API bool operator!= (const INT& left, const INT& right);
	CORE_API bool operator!= (const INT& left, const INT_TYPE& right);
	CORE_API bool operator!= (const INT_TYPE& left, const INT& right);

	CORE_API bool operator< (const INT& left, const INT& right);
	CORE_API bool operator< (const INT& left, const INT_TYPE& right);
	CORE_API bool operator< (const INT_TYPE& left, const INT& right);
	CORE_API bool operator<= (const INT& left, const INT& right);
	CORE_API bool operator<= (const INT& left, const INT_TYPE& right);
	CORE_API bool operator<= (const INT_TYPE& left, const INT& right);
	CORE_API bool operator> (const INT& left, const INT& right);
	CORE_API bool operator> (const INT& left, const INT_TYPE& right);
	CORE_API bool operator> (const INT_TYPE& left, const INT& right);
	CORE_API bool operator>= (const INT& left, const INT& right);
	CORE_API bool operator>= (const INT& left, const INT_TYPE& right);
	CORE_API bool operator>= (const INT_TYPE& left, const INT& right);

	CORE_API INT operator>> (const INT& left, const INT& right);
	CORE_API INT operator>> (const INT& left, const INT_TYPE& right);
	CORE_API INT operator>> (const INT_TYPE& left, const INT& right);
	CORE_API INT& operator>>= (INT& left, const INT& right);
	CORE_API INT& operator>>= (INT& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator>>= (INT_TYPE& left, const INT& right);
	CORE_API INT operator<< (const INT& left, const INT& right);
	CORE_API INT operator<< (const INT& left, const INT_TYPE& right);
	CORE_API INT operator<< (const INT_TYPE& left, const INT& right);
	CORE_API INT& operator<<= (INT& left, const INT& right);
	CORE_API INT& operator<<= (INT& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator<<= (INT_TYPE& left, const INT& right);
	CORE_API INT operator& (const INT& left, const INT& right);
	CORE_API INT operator& (const INT& left, const INT_TYPE& right);
	CORE_API INT operator& (const INT_TYPE& left, const INT& right);
	CORE_API INT operator^ (const INT& left, const INT& right);
	CORE_API INT operator^ (const INT& left, const INT_TYPE& right);
	CORE_API INT operator^ (const INT_TYPE& left, const INT& right);
	CORE_API INT operator| (const INT& left, const INT& right);
	CORE_API INT operator| (const INT& left, const INT_TYPE& right);
	CORE_API INT operator| (const INT_TYPE& left, const INT& right);
	CORE_API INT& operator&= (INT& left, const INT& right);
	CORE_API INT& operator&= (INT& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator&= (INT_TYPE& left, const INT& right);
	CORE_API INT& operator^= (INT& left, const INT& right);
	CORE_API INT& operator^= (INT& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator^= (INT_TYPE& left, const INT& right);
	CORE_API INT& operator|= (INT& left, const INT& right);
	CORE_API INT& operator|= (INT& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator|= (INT_TYPE& left, const INT& right);

	CORE_API INT operator+ (const INT& left, const INT& right);
	CORE_API INT operator+ (const INT& left, const INT_TYPE& right);
	CORE_API INT operator+ (const INT_TYPE& left, const INT& right);
	CORE_API INT& operator+= (INT& left, const INT& right);
	CORE_API INT& operator+= (INT& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator+= (INT_TYPE& left, const INT& right);

	CORE_API INT operator- (const INT& left, const INT& right);
	CORE_API INT operator- (const INT& left, const INT_TYPE& right);
	CORE_API INT operator- (const INT_TYPE& left, const INT& right);
	CORE_API INT& operator-= (INT& left, const INT& right);
	CORE_API INT& operator-= (INT& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator-= (INT_TYPE& left, const INT& right);

	CORE_API INT operator* (const INT& left, const INT& right);
	CORE_API INT operator* (const INT& left, const INT_TYPE& right);
	CORE_API INT operator* (const INT_TYPE& left, const INT& right);
	CORE_API INT& operator*= (INT& left, const INT& right);
	CORE_API INT& operator*= (INT& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator*= (INT_TYPE& left, const INT& right);

	CORE_API INT operator/ (const INT& left, const INT& right);
	CORE_API INT operator/ (const INT& left, const INT_TYPE& right);
	CORE_API INT operator/ (const INT_TYPE& left, const INT& right);
	CORE_API INT& operator/= (INT& left, const INT& right);
	CORE_API INT& operator/= (INT& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator/= (INT_TYPE& left, const INT& right);

	CORE_API INT operator% (const INT& left, const INT& right);
	CORE_API INT operator% (const INT& left, const INT_TYPE& right);
	CORE_API INT operator% (const INT_TYPE& left, const INT& right);
	CORE_API INT& operator%= (INT& left, const INT& right);
	CORE_API INT& operator%= (INT& left, const INT_TYPE& right);
	CORE_API INT_TYPE& operator%= (INT_TYPE& left, const INT& right);

#pragma endregion
SD_END