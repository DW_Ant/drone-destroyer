/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  UnitTestLauncher.h
  The Unit Test Launcher iterates and runs all unit tests.
=====================================================================
*/

#pragma once

#include "Object.h"
#include "UnitTester.h"

#ifdef DEBUG_MODE
SD_BEGIN
class CORE_API UnitTestLauncher : public Object
{
	DECLARE_CLASS(UnitTestLauncher)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Iterates through all unit tests where all of its test filters aligns with the specified flags.
	 * Returns false if any test fails.
	 */
	static bool RunAllTests (UnitTester::EUnitTestFlags testFlags);
};
SD_END

#endif