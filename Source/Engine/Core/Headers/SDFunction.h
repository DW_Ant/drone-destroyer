/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SDFunction.h
  The SDFunction's sole purpose is to associate a std::function with
  an identifier to be able to debug and compare delegates.
=====================================================================
*/

#pragma once

#include "DProperty.h"

/**
  Quick macro to define a SDFunction, bind function to a handler, and ensure the function name is consistent with the function wrapper.
  functionOwner is the actual object that owns the event handler.  className is the functionOwner's class (not a string).
  ReturnValue and parameters are types to pass into the template parameters.
  When defining parameter arguments, be sure to use the correct SDFunction to insert placeholders in function binds.  Format is:  SDFUNCTION_#PARAM()
 */
#define SDFUNCTION(functionOwner, className, functionName, returnValue) \
	SD::SDFunction<returnValue>(std::bind(&##className##::##functionName##, functionOwner), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_1PARAM(functionOwner, className, functionName, returnValue, param1) \
	SD::SDFunction<returnValue, param1>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_2PARAM(functionOwner, className, functionName, returnValue, param1, param2) \
	SD::SDFunction<returnValue, param1, param2>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1, std::placeholders::_2), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_3PARAM(functionOwner, className, functionName, returnValue, param1, param2, param3) \
	SD::SDFunction<returnValue, param1, param2, param3>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_4PARAM(functionOwner, className, functionName, returnValue, param1, param2, param3, param4) \
	SD::SDFunction<returnValue, param1, param2, param3, param4>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_5PARAM(functionOwner, className, functionName, returnValue, param1, param2, param3, param4, param5) \
	SD::SDFunction<returnValue, param1, param2, param3, param4, param5>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5), functionOwner, TXT(#className "::" #functionName))

#define SDFUNCTION_6PARAM(functionOwner, className, functionName, returnValue, param1, param2, param3, param4, param5, param6) \
	SD::SDFunction<returnValue, param1, param2, param3, param4, param5, param6>(std::bind(&##className##::##functionName##, functionOwner, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6), functionOwner, TXT(#className "::" #functionName))

SD_BEGIN
//Template arguments:  ReturnType, ParameterTypes
template <class R, class ... P>
class SDFunction : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Object that wraps the function pointer. */
	std::function<R(P...)> Function;

	/* Pointer to the object that implements the callback.  This is strictly used for fingerprinting.
	FriendlyName, alone, is not sufficient since multiple objects could be implement the same callback function. */
	const void* FunctionOwner;

	/* Name of the function to make it easier for identifying and comparing.  Typically the
	format is ObjectName::MemberFunction.  This convention is not strictly enforced but
	is recommended to avoid name conflicts and for consistency. */
	DString FriendlyName;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SDFunction ()
	{
		ClearFunction();
	}

	SDFunction (std::function<R(P...)> inFunction, const void* inFunctionOwner, const DString& inFriendlyName)
	{
		Function = inFunction;
		FunctionOwner = inFunctionOwner;
		FriendlyName = inFriendlyName;
	}

	SDFunction (const SDFunction& otherFunction)
	{
		Function = otherFunction.Function;
		FunctionOwner = otherFunction.FunctionOwner;
		FriendlyName = otherFunction.FriendlyName;
	}

	virtual ~SDFunction ()
	{

	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual DString ToString () const override
	{
		return FriendlyName;
	}

	virtual void Serialize (DataBuffer& dataBuffer) const override
	{
		CHECK_INFO(false, TXT("Serializing SDFunctions to data buffers is not supported."))
	}

	virtual void Deserialize (const DataBuffer& dataBuffer) override
	{
		CHECK_INFO(false, TXT("Deserializing SDFunctions from data buffers is not supported."))
	}


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const SDFunction& otherFunction)
	{
		Function = otherFunction.Function;
		FunctionOwner = otherFunction.FunctionOwner;
		FriendlyName = otherFunction.FriendlyName;
	}

	/**
	 * std::functions can't compare itself to other functions.
	 * Compare function owners and strings instead.
	 */
	bool operator== (const SDFunction& otherFunction) const
	{
		return (FunctionOwner == otherFunction.FunctionOwner && ToString() == otherFunction.ToString());
	}

	bool operator!= (const SDFunction& otherFunction) const
	{
		return !(operator==(otherFunction));
	}

	R operator() (P... params)
	{
		return Function(params...);
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Removes the function binding, and sets all member variables to empty.
	 */
	void ClearFunction ()
	{
		Function = nullptr;
		FunctionOwner = nullptr;
		FriendlyName = DString::EmptyString;
	}

	/**
	 * Returns true if the function is bound to a function.
	 */
	bool IsBounded () const
	{
		return (Function != nullptr);
	}

	/**
	 * Same as the operator(), but as its own method to invoke the delegate.
	 * Useful for cases where you're calling a SDFunction from a vector.
	 */
	R Execute  (P... params)
	{
		return Function(params...);
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DString GetFriendlyName () const
	{
		return FriendlyName;
	}

	DString GetEventHandlerName () const
	{
		INT colonIdx = FriendlyName.Find(TXT("::"), 0, DString::CC_CaseSensitive);
		if (colonIdx == INT_INDEX_NONE || colonIdx <= 0)
		{
			return DString::EmptyString;
		}

		return FriendlyName.SubString(0, colonIdx - 1);
	}

	DString GetFunctionName () const
	{
		INT colonIdx = FriendlyName.Find(TXT("::"), 0, DString::CC_CaseSensitive);
		if (colonIdx == INT_INDEX_NONE)
		{
			return DString::EmptyString;
		}

		colonIdx += 2; //Consider text after the colons
		if (colonIdx < FriendlyName.Length())
		{
			return FriendlyName.SubString(colonIdx);
		}

		return DString::EmptyString;
	}

	const void* GetFunctionOwner () const
	{
		return FunctionOwner;
	}
};
SD_END