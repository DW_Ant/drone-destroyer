/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DClassAssembler.h
  Before Main runs, countless Objects and EngineComponents will instantiate a DClass
  object.  These DClasses will notify the DClassAssembler about their existance, and
  their expected parent class.

  When Main runs and before the Main Engine is initialized, the DClassAssembler will take all
  registered DClass instances and generate a class tree relationship.  Assertions are thrown
  if there are broken connections.
  
  Although DClasses can only have one parent classes, the Objects themselves can still
  have multi-inheritance.  However, they can only derive from at most one parent with an associated
  DClass.

  The class tree is visible in all threads.  Only the main thread creates and cleans up the tree.
=====================================================================
*/

#pragma once

#include "Core.h"

#include "DString.h"

SD_BEGIN
class Object;
class EngineComponent;
class DClass;

class CORE_API DClassAssembler
{


	/*
	=====================
	  Data types
	=====================
	*/

protected:
	struct SDClassInfo
	{
		DString ClassNamespace;
		DClass* ClassInstance;

		DString ParentNamespace;
		DString ParentClassName;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Vector tracking registered DClasses at static initialization time. */
	static std::vector<SDClassInfo> PreloadedDClasses;

	/**
	 * Vector containing the exact same data as PreloadedDClasses.  This is a workaround to how static vectors are initialized.
	 * In Microsoft's C++, a vector clears is size when initialized.  However, it could have data (if DClasses are initialized before this vector).
	 * To preserve the initialized data, there's a copy of PreloadedDClasses.  When the PreloadedDClasses are initialized, it simply copies from its copy.
	 */
	static std::vector<SDClassInfo> PreloadedDClassesCopy;

	/* Becomes true when all DClasses have been initialized, and the assembler constructed the class tree. */
	static bool bLinkedDClasses;

	/* A list of all classes without a parent class sorted alphabetically. */
	static std::vector<DClass*> RootClasses;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a DClass instance.  The DClass is not yet initialized, but it's registered for initialization
	 * when the Engine initializes.
	 * @param classNamespace - Namespace this class resides in (ie:  "SD").
	 * @param className - Technical (cpp) name of the Object instance.
	 * @param parentNamespace - Namespace the parent class resides in.  The parent's DClass's namespace must match.
	 * @param parentClass - Technical (cpp) name of the parent Object that also has a DClass instance.
	 * The parent's DClass's className must match this value to correctly link the two classes.  If empty, then
	 * it's assumed that the DClass instance does not have a parent.
	 * Returns the DClass instance that is not yet initialized.  This instance will be initialized when AssembleDClasses is called.
	 */
	static DClass* LoadClass (const DString& classNamespace, const DString& className, const DString& parentNamespace, const DString& parentClass);

	/**
	 * Takes all registered DClasses, and assembles a class tree.  This also links all DClass' Children and parent pointers.
	 * Returns true if all registered DClasses are linked.
	 */
	static bool AssembleDClasses ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns true if the assembler generated the class tree, and all DClasses' relationships are assembled.
	 */
	static bool IsInitialized ();
	static const std::vector<DClass*> GetRootClasses ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if the registeredDClass is linked with its parent (if any).
	 */
	static bool HasFoundParent (const SDClassInfo& registeredDClass);

	/**
	 * Returns true if the child DClass info's parent info matches the potential parent DClass info.
	 */
	static bool IsChildOf (const SDClassInfo& childInfo, const SDClassInfo& parentInfo);

	/**
	 * Establish the parent/child relationship between the two DClasses.
	 */
	static void LinkDClasses (SDClassInfo& childInfo, SDClassInfo& parentInfo);

	/**
	 * Adds new class instance to the root classes vector.
	 */
	static void AddRootClass (DClass* newRootClass);

private:
	virtual void AbstractClass () = 0;
};
SD_END