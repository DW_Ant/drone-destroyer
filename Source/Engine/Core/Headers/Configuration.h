/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Configuration.h
  This file configures various preprocessor macros specifically for the Core module.
  All definitions are surrounded by ifdef to provide the opportunity for the application
  to define the config variables.  This file simply defines the default values if the
  application does not define them.
=====================================================================
*/

#pragma once

#include "Definitions.h"

//General settings

/* If true, then the DString will validate its parameters.  For example, character indices will clamp between 0
and Length() - 1.  Setting this to 0 will slightly improve performance and will crash for invalid input. */
#ifndef SAFE_STRINGS
	#define SAFE_STRINGS 0
#endif

/* Check macro configuration.
0 = disabled - Improves performance since conditions are compiled out, but crashes may leave messages (if any) that will be more difficult to debug.
1 = Log only - Slightly reduces performance by adding additional conditions.  Should the conditions fail, log warnings are issues.
	The program will attempt to continue.  However most of the checks are validating object pointers before using them.
	Subsequent code will most likely crash if these checks failed.  However there may be some checks that are not fatal if their condition fails.
2 = Crash - Slightly reduces performance by adding additional conditions.  Should the conditions fail, an assertion will trigger immediately. */
#ifndef CHECK_CONFIG
	#ifdef _DEBUG
		#define CHECK_CONFIG 2
	#else
		#define CHECK_CONFIG 1
	#endif
#endif

#ifndef ENABLE_COMPLEX_CHECKING
	#ifdef _DEBUG
		/* If defined, then assertions that reduces application performance are active.  This should be turned off for release builds. */
		#define ENABLE_COMPLEX_CHECKING 1
	#endif
#endif


/* Character encodings (Only one of these are used).  If all are false, then local encoding is used.
Warning, changing the character encoding here may change the used string size.  See DString.h TString typedefs for char sizes.
Multiple string utilities may not have implemented support for UTF-32 or UTF-16.  Compilation errors may occur when changing string encoding. */
#ifndef USE_WIDE_STRINGS
	#define USE_WIDE_STRINGS 0
#endif
#ifndef USE_UTF32
	#define USE_UTF32 0
#endif
#ifndef USE_UTF16
	#define USE_UTF16 0
#endif
#ifndef USE_UTF8
	#define USE_UTF8 1
#endif