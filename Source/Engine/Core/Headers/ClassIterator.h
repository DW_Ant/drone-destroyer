/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ClassIterator.h
  The ClassIterator is an utility class that makes it easy to iterate
  through DClasses.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class DClass;

class CORE_API ClassIterator
{


	/*
	=====================
	  Data Types
	=====================
	*/

protected:
	/* When stepping through the class hierarchy, this struct contains necessary information which child class was iterated. */
	struct SClassIteratorInfo
	{
		const DClass* CurrentClass = nullptr;

		/* Reference to the index of the current selected child class. */
		INT ChildIndex = -1;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Reference to the DClass this iterator is currently pointing to. */
	const DClass* SelectedClass;

protected:
	/* Reference to the initial class (does not iterator through parent classes of initial class). */
	const DClass* InitialClass;

	/* Chain of parent classes between the SelectedClass and the initial class. */
	std::vector<SClassIteratorInfo> ParentClasses;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	/**
	 * Create a class iterator that searches through all Objects.
	 */
	ClassIterator ();

	/**
	 * Create a class iterator that searches from the given initial class and all of its subclasses.
	 */
	ClassIterator (const DClass* newInitialClass);

	virtual ~ClassIterator ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	virtual void operator++ (); //++iter
	virtual void operator++ (int); //iter++


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	const DClass* GetSelectedClass () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeIterator ();

	/**
	 * Selects the next DClass in the hierarchy.  It'll iterate through child classes first.
	 * Then it'll step up in the ParentClasses vector.  Updates Selected Object.
	 */
	virtual void SelectNextClass ();
};
SD_END