/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CopiableObjectInterface.h

  This interface adds functions that determine how an object's properties are copied to others.
  This isn't a duplicator where every property is copied (such as delegates and implementation properties [like timestamps]).
  This is intended for systems such as editors' copy/paste, UI styles, and gameplay copying mechanics.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class CORE_API CopiableObjectInterface
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Copies various properties from the given template.
	 * The template's class should match or be a parent of this object's class.
	 */
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objectTemplate) = 0;
};
SD_END