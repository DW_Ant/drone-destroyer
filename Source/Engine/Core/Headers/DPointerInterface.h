/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DPointerInterface.h

  This interface allows an object to possess a linked list of DPointers.  Each DPointer that references the
  object (that implements this interface) will be within the object's DPointer linked list.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class DPointerBase;

class CORE_API DPointerInterface
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if DPointers are allowed to point at this object.  Otherwise, 
	 * the pointer will be assigned to nullptr instead.
	 */
	virtual bool CanBePointed () const = 0;

	/**
	 * Assigns this DPointer object to be first in the object's DPointer linked list.
	 */
	virtual void SetLeadingDPointer (DPointerBase* newLeadingPointer) const = 0;

	/**
	 * Retrieves the first DPointer in the object's DPointer linked list.
	 */
	virtual DPointerBase* GetLeadingDPointer () const = 0;
};
SD_END