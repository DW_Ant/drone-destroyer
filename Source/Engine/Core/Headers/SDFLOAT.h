/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FLOAT.h
  A class that represents a 32 bit floating point number.  Contains numeric
  utility functions, and supports typecasting to various standard C++ numbers.

  Without the "SD" prefix in the file name, the compiler will generate errors
  such as error C2065: 'FLT_RADIX' : undeclared identifier
  The file name "float.h" is already taken.
=====================================================================
*/

#pragma once

#include "Configuration.h"

#include "DProperty.h"

#ifndef FLOAT_TOLERANCE
/* Define how close floats can be to each other to be considered equal. */
#define FLOAT_TOLERANCE 0.000001f
#endif

#ifndef MAXFLOAT
#define MAXFLOAT std::numeric_limits<float>::max()
#endif

#ifndef MINFLOAT
#define MINFLOAT std::numeric_limits<float>::lowest()
#endif

SD_BEGIN
class INT;

class CORE_API FLOAT : public DProperty
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/* Determines how the trailing digits should be truncated. */
	enum class eTruncateMethod
	{
		TM_RoundDown,
		TM_Round,
		TM_RoundUp
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	float Value;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	FLOAT ();
	FLOAT (const float newValue);
	FLOAT (const FLOAT& copyFloat);
	explicit FLOAT (const INT& newValue);
	explicit FLOAT (const DString& text);

		
	/*
	=====================
	  Operators
	=====================
	*/

public:
	virtual void operator= (const FLOAT& copyFloat);
	virtual void operator= (float otherFloat);

	virtual FLOAT operator++ ();
	virtual FLOAT operator++ (int);
	virtual FLOAT operator- () const;
	virtual FLOAT operator-- ();
	virtual FLOAT operator-- (int);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	DString ToString () const override;
	virtual void Serialize (DataBuffer& dataBuffer) const override;
	virtual void Deserialize (const DataBuffer& dataBuffer) override;

		
	/*
	=====================
	  Methods
	=====================
	*/

public:
	static FLOAT MakeFloat (int value);
	static FLOAT MakeFloat (unsigned int value);
#ifdef PLATFORM_64BIT
	static FLOAT MakeFloat (const int64& value);
	static FLOAT MakeFloat (const uint64& value);
#endif

	/**
	 * Returns true if the value of this float is nearly equal to the other float
	 * within the given tolerance level.
	 */
	virtual bool IsCloseTo (FLOAT other, FLOAT tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns an INT from this FLOAT.  This conversion truncates all decimal values.
	 */
	virtual INT ToINT () const;

	/**
	 * Returns the absolute value of this FLOAT.
	 */
	static FLOAT Abs (const FLOAT value);
	virtual void AbsInline ();

	static FLOAT Round (const FLOAT value);
	virtual void RoundInline ();
	virtual void RoundInline (INT numDecimals);

	static FLOAT RoundUp (const FLOAT value);
	virtual void RoundUpInline ();
	virtual void RoundUpInline (INT numDecimals);

	static FLOAT RoundDown (const FLOAT value);
	virtual void RoundDownInline ();
	virtual void RoundDownInline (INT numDecimals);

	/**
	 * Converts the value to an float.  This function may cause precision errors if double is large enough.
	 */
	virtual float GetFloat (double target);

	/**
	 * Returns a double from this value
	 */
	virtual double ToDouble () const;

	/**
	 * Returns a DString that displays this float based on the specified parameters.
	 *
	 * @param: minNumDigits Determines how many digits should there be. Adds leading zeros to the
	 * front to fulfill requirement (eg: 00003.14). The decimal point and negative sign does not count.
	 *
	 * @param: numDecimals Determines how many decimal places there should be. Adds trailing zeros if there
	 * aren't enough digits (eg: 1.25000). Truncates decimals if there are too many (eg: 1.2).
	 * This parameter is disabled if negative.
	 *
	 * @param: rounding if the function needs to truncate decimal places, this is the rounding method to use.
	 */
	virtual DString ToFormattedString (INT minNumDigits, INT numDecimals, eTruncateMethod rounding) const;
};

#pragma region "External Operators"
	CORE_API bool operator== (const FLOAT& left, const FLOAT& right);
	CORE_API bool operator== (const FLOAT& left, const float& right);
	CORE_API bool operator== (const float& left, const FLOAT& right);

	CORE_API bool operator!= (const FLOAT& left, const FLOAT& right);
	CORE_API bool operator!= (const FLOAT& left, const float& right);
	CORE_API bool operator!= (const float& left, const FLOAT& right);

	CORE_API bool operator< (const FLOAT& left, const FLOAT& right);
	CORE_API bool operator< (const FLOAT& left, const float& right);
	CORE_API bool operator< (const float& left, const FLOAT& right);
	CORE_API bool operator<= (const FLOAT& left, const FLOAT& right);
	CORE_API bool operator<= (const FLOAT& left, const float& right);
	CORE_API bool operator<= (const float& left, const FLOAT& right);
	CORE_API bool operator> (const FLOAT& left, const FLOAT& right);
	CORE_API bool operator> (const FLOAT& left, const float& right);
	CORE_API bool operator> (const float& left, const FLOAT& right);
	CORE_API bool operator>= (const FLOAT& left, const FLOAT& right);
	CORE_API bool operator>= (const FLOAT& left, const float& right);
	CORE_API bool operator>= (const float& left, const FLOAT& right);

	CORE_API FLOAT operator+ (const FLOAT& left, const FLOAT& right);
	CORE_API FLOAT operator+ (const FLOAT& left, const float& right);
	CORE_API FLOAT operator+ (const float& left, const FLOAT& right);
	CORE_API FLOAT& operator+= (FLOAT& left, const FLOAT& right);
	CORE_API FLOAT& operator+= (FLOAT& left, const float& right);
	CORE_API float& operator+= (float& left, const FLOAT& right);

	CORE_API FLOAT operator- (const FLOAT& left, const FLOAT& right);
	CORE_API FLOAT operator- (const FLOAT& left, const float& right);
	CORE_API FLOAT operator- (const float& left, const FLOAT& right);
	CORE_API FLOAT& operator-= (FLOAT& left, const FLOAT& right);
	CORE_API FLOAT& operator-= (FLOAT& left, const float& right);
	CORE_API float& operator-= (float& left, const FLOAT& right);

	CORE_API FLOAT operator* (const FLOAT& left, const FLOAT& right);
	CORE_API FLOAT operator* (const FLOAT& left, const float& right);
	CORE_API FLOAT operator* (const float& left, const FLOAT& right);
	CORE_API FLOAT& operator*= (FLOAT& left, const FLOAT& right);
	CORE_API FLOAT& operator*= (FLOAT& left, const float& right);
	CORE_API float& operator*= (float& left, const FLOAT& right);

	CORE_API FLOAT operator/ (const FLOAT& left, const FLOAT& right);
	CORE_API FLOAT operator/ (const FLOAT& left, const float& right);
	CORE_API FLOAT operator/ (const float& left, const FLOAT& right);
	CORE_API FLOAT& operator/= (FLOAT& left, const FLOAT& right);
	CORE_API FLOAT& operator/= (FLOAT& left, const float& right);
	CORE_API float& operator/= (float& left, const FLOAT& right);

	CORE_API FLOAT operator% (const FLOAT& left, const FLOAT& right);
	CORE_API FLOAT operator% (const FLOAT& left, const float& right);
	CORE_API FLOAT operator% (const float& left, const FLOAT& right);
	CORE_API FLOAT& operator%= (FLOAT& left, const FLOAT& right);
	CORE_API FLOAT& operator%= (FLOAT& left, const float& right);
	CORE_API float& operator%= (float& left, const FLOAT& right);

#if 0
	CORE_API bool operator== (float a, const FLOAT& b);
	CORE_API bool operator!= (float a, const FLOAT& b);
	CORE_API bool operator< (float a, const FLOAT& b);
	CORE_API bool operator<= (float a, const FLOAT& b);
	CORE_API bool operator> (float a, const FLOAT& b);
	CORE_API bool operator>= (float a, const FLOAT& b);

	CORE_API FLOAT operator+ (float a, const FLOAT& b);
	CORE_API FLOAT operator- (float a, const FLOAT& b);
	CORE_API FLOAT operator* (float a, const FLOAT& b);
	CORE_API FLOAT operator/ (float a, const FLOAT& b);
	CORE_API FLOAT operator% (float a, const FLOAT& b);
#endif
#pragma endregion
SD_END