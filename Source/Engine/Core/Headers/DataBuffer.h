/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DataBuffer.h
  A class that with an arbitrary vector of characters.  The data is organized in a continuous memory
  block.  It also provides an interface to read and extract characters sequentually to allow for quick
  data transfer.

  There is no type safety in the data buffer.  Objects using this class should access the data buffer responsibly.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class CORE_API DataBuffer
{


	/*
	=====================
	  Properties
	=====================
	*/

private:
	/* Becomes true if the Data buffer is in little endian format. */
	bool DataBufferIsLittleEndian;

	/* The vector containing the data buffer, itself.  Typically each char within Data could be part of a greater variable.
	For example, a 32-bit int would take up 4 elements in this vector (char is 1 byte).  If an int is pushed to this data buffer,
	then 4 elements in this vector make up that int.  There is no type-safety here.  It's not safe to access the middle of this
	data buffer since it could be accessing a middle of a variable. */
	std::vector<char> Data;

	/* Index of Data the we're reading from.  This should either point to the beginning of a new variable or the end of the buffer. */
	mutable size_t ReadIdx;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	/**
	 * Creates an empty data buffer.
	 */
	DataBuffer ();

	/**
	 * Initializes an empty data buffer of the specified size.  Sets the read/write index to the beginning.
	 */
	DataBuffer (int numBytes);
	DataBuffer (int numBytes, bool inDataBufferIsLittleEndian);
	DataBuffer (const DataBuffer&) = delete;
	virtual ~DataBuffer ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const DataBuffer&) = delete;

	/**
	 * Quick utility operator when interfacing with DProperties.
	 * Reads data from the data buffer, and places the data into variable.
	 */
	template <class T>
	void operator>> (T& outVarToWriteTo) const
	{
		outVarToWriteTo.Deserialize(*this);
	}

	/**
	 * Quick utility operator when interfacing with DProperties.
	 * Appends the data buffer with the contents of the variable.
	 */
	template <class T>
	void operator<< (const T& varToReadFrom)
	{
		varToReadFrom.Serialize(*this);
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this machine uses little endian format.
	 * Otherwise, it'll return false if it's using big endian format.
	 */
	static bool IsSystemLittleEndian ();

	/**
	 * Swaps the byte order of the given character array.  Used when switching endianess.
	 *
	 * Example:  Converts the given byte data (first line) in reverse order (second line)
	 * 00000001 00000100 00010000 10000000
	 * 10000000 00010000 00000100 00000001
	 *
	 * @param outCharBuffer Raw byte data that'll be in reverse order after function execution.
	 * @param numChars Number of bytes in the character buffer that should be swapped.
	 * Specifying too many bytes will result into an access violation.
	 */
	static void SwapByteOrder (char* outCharBuffer, size_t numBytes);

	inline void EmptyBuffer ()
	{
		Data.clear();
		JumpToBeginning();
	}

	/**
	 * Returns the number of bytes in the data buffer.
	 */
	inline size_t GetNumBytes () const
	{
		return Data.size();
	}

	/**
	 * Advances the ReadIdx forward/backwards down the data buffer.
	 * Since there's no type safety in the databuffer, it's the caller's responsibility to advance
	 * the index to a valid position, where a valid position would be either at the end of the buffer, or
	 * at the start of a variable within the buffer.
	 *
	 * @param numBytes Number of bytes to jump ahead.  Specify negative numbers to jump backwards towards 
	 * the beginning of the data buffer.
	 */
	void AdvanceIdx (size_t numBytes) const;

	/**
	 * Jumps the ReadIdx to the beginning of the data buffer.
	 */
	inline void JumpToBeginning () const
	{
		ReadIdx = 0;
	}

	/**
	 * Advances the ReadIdx to the end of the data buffer.
	 */
	inline void JumpToEnd () const
	{
		ReadIdx = Data.size();
	}

	/**
	 * Reads in the next few characters from the data buffer.  Advances the ReadIdx by the number of bytes read.
	 *
	 * @param outReadData The character array that contains the a copy of the bytes pulled from data buffer.
	 * @param numBytes Determines the amount of characters to read from the data buffer.
	 */
	void ReadBytes (char* OUT outReadData, size_t numBytes) const;

	/**
	 * Writes the raw byte data at the end of the data buffer.  The data buffer may allocate new space if it needs to.
	 * 
	 * @param dataToAdd The raw byte data to append to the end of the data buffer.
	 * @param numBytes The number of bytes it should read from the given character buffer.
	 */
	void WriteBytes (const char* dataToAdd, size_t numBytes);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns true if the read idx is pointing at the beginning of the data buffer.
	 */
	inline bool IsReaderAtBeginning () const
	{
		return (ReadIdx == 0);
	}

	/**
	 * Returns true if the read idx is pointing at the end of the data buffer.
	 */
	inline bool IsReaderAtEnd () const
	{
		return (ReadIdx >= Data.size());
	}

	inline bool IsDataBufferLittleEndian () const
	{
		return DataBufferIsLittleEndian;
	}
};
SD_END