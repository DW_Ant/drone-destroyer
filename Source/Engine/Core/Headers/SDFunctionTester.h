/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SDFunctionTester.h
  Object responsible for assisting the DatatypeUnitTester's SDFunction and MulticastDelegate tests.

  In short, this object will bind a couple SDFunctions to its event handlers.
  The test will only become successful if all functions were invoked.
=====================================================================
*/

#pragma once

#include "Object.h"

#ifdef DEBUG_MODE

SD_BEGIN
class CORE_API SDFunctionTester : public Object
{
	DECLARE_CLASS(SDFunctionTester)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Various flags that all must be true to be considered a successful test. */
	bool bTestedVoid;
	bool bTestedParamINT;
	bool bTestedMultiParam;
	bool bTestedReturn;
	bool bTestedReturnMultiParam;
	bool bTestedReturnOutParam;

	/* Increments every time one of its event handlers were called. */
	INT FunctionCounter;

	/* Tests related to the Multicast test */
	MulticastDelegate<void, INT> MulticastTestBroadcaster;
	INT MulticastTestCounter;
	bool MulticastTestHandlerCalled[4];
	bool MulticastAddFunctionCalled;
	bool MulticastRemoveFunctionCalled;

private:
	const DatatypeUnitTester* DatatypeTester;
	UnitTester::EUnitTestFlags TestFlags;

	/* If true then the Add/Remove function handlers can run additional tests. */
	bool AddFunctionCanFail;
	bool RemoveFunctionCanFail;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Method
	=====================
	*/

public:
	/**
	 * Executes all tests. Returns true if all tests passes.
	 */
	virtual bool RunTest (const DatatypeUnitTester* tester, UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if all callbacks were invoked once.
	 */
	virtual bool RunFunctionTest (UnitTester::EUnitTestFlags testFlags);

	virtual bool RunMulticastTest (UnitTester::EUnitTestFlags testFlags);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleVoidFunction ();
	virtual void HandleParamFunction (INT paramValueTest);
	virtual void HandleMultiParamFunction (INT param1, FLOAT param2, DString param3, unsigned int param4);
	virtual FLOAT HandleReturnFunction ();
	virtual BOOL HandleReturnMultiParamFunction (INT param1, Object* param2, std::string param3);
	virtual INT HandleReturnOutParamFunction (INT param1, INT& outParam2);

	virtual void HandleMulticastFunctionA (INT incrementAmount);
	virtual void HandleMulticastFunctionB (INT incrementAmount);
	virtual void HandleMulticastFunctionC (INT incrementAmount);
	virtual void HandleMulticastFunctionD (INT incrementAmount);
	virtual void HandleMulticastAddFunction (INT incrementAmount);
	virtual void HandleMulticastRemoveFunction (INT incrementAmount);
};
SD_END

#endif //debug_mode