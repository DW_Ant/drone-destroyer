/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EngineComponent.h
  The Engine Component contains methods to extend the Engine's functionality.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class DClass;
class Engine;

class CORE_API EngineComponent
{
	DECLARE_ENGINE_COMPONENT(EngineComponent)
		

	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Engine this component is registered to. */
	Engine* OwningEngine;

	/* If true, then this EngineComponent will register itself to the Engine's Tick cycle,
	and receive Pre and Post Tick Updates. */
	bool bTickingComponent;


	/*
	=====================
	  Constructors
	=====================
	*/

protected:
	EngineComponent ();
	virtual ~EngineComponent ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Allows engine component to register parts of the Engine's Object Hash Table to group specific objects
	 * for faster object iteration.  This is called prior to any engine component receiving a PreInitializeComponent call.
	 */
	virtual void RegisterObjectHash ();

	/**
	 * Returns true if all dependencies have been met for this EngineComponent to execute PreInitializeComponent.
	 * @Param:  List of EngineComponents that already executed their PreInitializeComponent method.
	 */
	virtual bool CanRunPreInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const;

	/**
	 * Returns true if all dependencies have been met for this EngineComponent to execute InitializeComponent.
	 * @Param:  List of EngineComponents that already executed their InitializeComponent method.
	 */
	virtual bool CanRunInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const;

	/**
	 * Returns true if all dependencies have been met for this EngineComponent to execute PostInitializeComponent.
	 * @Param:  List of EngineComponents that already executed their PostInitializeComponent method.
	 */
	virtual bool CanRunPostInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const;

	/**
	 * Called prior to majority of essential object instantiation.  Resource Pools and DrawLayers 
	 * are not guaranteed to be instantiated at this point.  Object Hash values are already
	 * registered, and is safe to instantiate objects and entities.
	 */
	virtual void PreInitializeComponent ();

	/**
	 * Called after essential objects are instantiated.  Resource Pools, draw layers are registered,
	 * and is safe to import resources such as textures and fonts.
	 */
	virtual void InitializeComponent ();

	/**
	 * Called after essential objects and resources are imported.
	 */
	virtual void PostInitializeComponent ();

	/**
	 * Allows EngineComponents to modify the log message prior to EngineComponents from processing the message.
	 * @param category Log classification this message belongs to.
	 * @param logLevel Importance level associated with this message.
	 * @param msg The actual log message, itself.
	 */
	virtual void FormatLog (const LogCategory* category, LogCategory::ELogLevel logLevel, OUT DString& msg);

	/**
	 * Notifies EngineComponent of incoming log message to serialize and/or display.
	 * This is invoked after EngineComponents handled their FormatLog methods.
	 * @param category Log classification this message belongs to.
	 * @param logLevel Importance level associated with this message.
	 * @param formattedMsg The actual log message, itself.
	 */
	virtual void ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg);

	/**
	 * Called before the Engine executes its tick cycle.  The render pipeline did not execute yet, and
	 * all TickComponents did not call receive their Tick updates yet.
	 * You must set bTickingComponent to true in the constructor to be able to receive updates.
	 */
	virtual void PreTick (FLOAT deltaSec);

	/**
	 * Called after the Engine cycles through all TickComponents.  It's not guaranteed if the objects are
	 * displayed or not at the time this function is called.
	 * You must set bTickingComponent to true in the constructor to be able to receive updates.
	 */
	virtual void PostTick (FLOAT deltaSec);

	/**
	 * Invoked whenever the engine is about to shutdown.
	 */
	virtual void ShutdownComponent ();

	void SetOwningEngine (Engine* newOwningEngine);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(Engine*, OwningEngine);
	virtual bool GetTickingComponent () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns a list of EngineComponents that must execute their PreInitializeComponent prior
	 * to this component running its PreInitializeComponent.
	 */
	virtual std::vector<const DClass*> GetPreInitializeDependencies () const;

	/**
	 * Returns a list of EngineComponents that must execute their InitializeComponent prior
	 * to this component running its InitializeComponent.
	 */
	virtual std::vector<const DClass*> GetInitializeDependencies () const;

	/**
	 * Returns a list of EngineComponents that must execute their PostInitializeComponent prior
	 * to this component running its PostInitializeComponent.
	 */
	virtual std::vector<const DClass*> GetPostInitializeDependencies () const;

friend class Engine;
friend class DClass;
};
SD_END