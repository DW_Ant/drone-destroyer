/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Vector3.h
  Self-contained 3D vector class with some utilities.
=====================================================================
*/

#pragma once

#include "Configuration.h"

SD_BEGIN
class Vector2;

class CORE_API Vector3 : public DProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	FLOAT X;
	FLOAT Y;
	FLOAT Z;

	/* A 3D vector that has all of its components equal to 0. */
	static const Vector3 ZeroVector;

	static const Vector3 Right;
	static const Vector3 Up;
	static const Vector3 Forward;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Vector3 ();

	Vector3 (const float x, const float y, const float z);

	Vector3 (const FLOAT x, const FLOAT y, const FLOAT z);

	Vector3 (const Vector3& copyVector);

		
	/*
	=====================
	  Operators
	=====================
	*/

public:
	void operator= (const Vector3& copyVector);
	FLOAT operator[] (UINT_TYPE axisIdx);
	const FLOAT operator[] (UINT_TYPE axisIdx) const;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	DString ToString () const override;
	virtual void Serialize (DataBuffer& dataBuffer) const override;
	virtual void Deserialize (const DataBuffer& dataBuffer) override;

		
	/*
	=====================
	  Methods
	=====================
	*/

public:
	static Vector3 SFMLtoSD (const sf::Vector3f sfVector);
	static sf::Vector3f SDtoSFML (const Vector3 sdVector);

	bool IsNearlyEqual (const Vector3& compareTo, FLOAT tolerance = FLOAT_TOLERANCE) const;

	/**
	 * Returns the length of this vector.
	 */
	FLOAT VSize (bool bIncludeZ = true) const;

	/**
	 * Same as VSize except without the sqrt.  This is typically used to quickly compare relative distances
	 * without the performance hit of square rooting them.
	 */
	FLOAT CalcDistSquared (bool bIncludeZ = true) const;

	/**
	 * Adjusts this vector's size so the length of the vector is equal to the specified length.
	 */
	void SetLengthTo (FLOAT targetLength);

	Vector3 static Min (Vector3 input, const Vector3& min);
	void MinInline (const Vector3& min);
	Vector3 static Max (Vector3 input, const Vector3& max);
	void MaxInline (const Vector3& max);

	/**
	 * Clamps each axis to be within the min/max range. Each axis acts independently from others.
	 */
	Vector3 static Clamp (Vector3 input, const Vector3& min, const Vector3& max);
	void ClampInline (const Vector3& min, const Vector3& max);

	FLOAT Dot (const Vector3& otherVector) const;

	/**
	 * Gets the orthogonal vector to this vector and the given vector.
	 */
	Vector3 CrossProduct (const Vector3& otherVector) const;

	void Normalize ();

	/**
	 * Returns true of all axis of this vector are the equal.
	 */
	bool IsUniformed () const;

	/**
	 * Returns true if all axis are 0.
	 */
	bool IsEmpty () const;

	/**
	 * Returns a Vector2 from this vector's components.
	 */
	Vector2 ToVector2 () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Retrieves one of the vector dimensions based on specified axis index.
	 * [0] = X axis
	 * [1] = Y axis
	 * [2] = Z axis
	 */
	FLOAT GetAxis (UINT_TYPE vectorComponent) const;


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Executes a linear interpolation between minBounds and maxBounds based on the given ratio for all axis.
	 * Ratio ranges from [0,1] to return a value between min and max bounds.  0 being equal to min bounds, and 1 being equal to max bounds.
	 * @tparam T The datatype used for alpha/ratio.  Only decimal datatypes (such as floats, doubles, and FLOATs) are supported.
	 */
	template <class T>
	static Vector3 Lerp(T ratio, const Vector3& minBounds, const Vector3& maxBounds)
	{
		Vector3 result;

		result.X = Utils::Lerp(ratio, minBounds.X, maxBounds.X);
		result.Y = Utils::Lerp(ratio, minBounds.Y, maxBounds.Y);
		result.Z = Utils::Lerp(ratio, minBounds.Z, maxBounds.Z);

		return result;
	}
};

#pragma region "External Operators"
	CORE_API bool operator== (const Vector3& left, const Vector3& right);
	CORE_API bool operator!= (const Vector3& left, const Vector3& right);
	CORE_API Vector3 operator+ (const Vector3& left, const Vector3& right);
	CORE_API Vector3& operator+= (Vector3& left, const Vector3& right);
	CORE_API Vector3 operator- (const Vector3& left, const Vector3& right);
	CORE_API Vector3& operator-= (Vector3& left, const Vector3& right);
	CORE_API Vector3 operator* (const Vector3& left, const Vector3& right);
	CORE_API Vector3& operator*= (Vector3& left, const Vector3& right);
	CORE_API Vector3 operator/ (const Vector3& left, const Vector3& right);
	CORE_API Vector3& operator/= (Vector3& left, const Vector3& right);

	template<class T>
	Vector3 operator* (const Vector3& left, const T& right)
	{
		return Vector3(left.X * right, left.Y * right, left.Z * right);
	}

	template<class T>
	Vector3 operator* (const T& left, const Vector3& right)
	{
		return Vector3(left * right.X, left * right.Y, left * right.Z);
	}

	template<class T>
	Vector3 operator*= (Vector3& left, const T& right)
	{
		left.X *= right;
		left.Y *= right;
		left.Z *= right;

		return left;
	}

	template<class T>
	Vector3 operator/ (const Vector3& left, const T& right)
	{
		if (right != 0)
		{
			return Vector3(left.X / right, left.Y / right, left.Z / right);
		}

		return left;
	}

	template<class T>
	Vector3& operator/= (Vector3& left, const T& right)
	{
		if (right != 0)
		{
			left.X /= right;
			left.Y /= right;
			left.Z /= right;
		}

		return left;
	}
#pragma endregion
SD_END