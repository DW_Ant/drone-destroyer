/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ComponentIterator.h
  The ComponentIterator will iterate through the components of the specified entity.

  For recursive iterators (where the iterator will find EntityComponents within EntityComponents), the
  order of components iterates through the sub components before iterating to the next sibling component.

  Entity
	[0] ComponentA
		[1] SubComponentA
			[2] SubSubComponentA
			[3] SubSubComponentB
		[4] SubComponentB
		[5] SubComponentC
			[6] SubSubComponentA
	[7] ComponentB
		[8] SubComponentA
		[9] SubComponentB

  Warning:  Don't change the owning Entity's component list while an iterator is
  cycling for that may cause other components in next cycle to be skipped.
=====================================================================
*/

#pragma once

#include "Core.h"

SD_BEGIN
class Entity;
class EntityComponent;

class CORE_API ComponentIterator
{


	/*
	=====================
	  Datatypes
	=====================
	*/

protected:
	struct SComponentIndex
	{
	public:
		/* The Entity that owns the components this data struct is iterating through. */
		const Entity* TargetEntity;

		/* Component index of TargetEntity's Component List. */
		UINT_TYPE ComponentIdx;

		SComponentIndex (const Entity* inTargetEntity)
		{
			TargetEntity = inTargetEntity;
			ComponentIdx = 0;
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then this iterator will find components within components.  Otherwise, the iterator will only
	jump from one sibling component to the next sibling. */
	bool bRecursive;

protected:
	/* Stack of Entities this object is iterating through.  It'll always iterate the last Entity in the list, and
	when it found that Entity's last component, it'll pop it from the list.  For non recursive iterators, this
	vector size is always 1, and it'll only iterate through sibling components. */
	std::vector<SComponentIndex> ComponentChain;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	ComponentIterator ();
	ComponentIterator (const Entity* inBaseEntity, bool bInRecursive);
	virtual ~ComponentIterator ();
		


	/*
	=====================
	  Operators
	=====================
	*/

public:
	inline operator bool () const
	{
		return (GetSelectedComponent() != nullptr);
	}

	inline bool operator! () const
	{
		return (GetSelectedComponent() == nullptr);
	}

	void operator++ (); //++iter
	void operator++ (int); //iter++


	/*
	=====================
	  Methods
	=====================
	*/

public:
	void SetBaseEntity (const Entity* newBaseEntity);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Returns the root Entity this iterator began with.
	 */
	const Entity* GetBaseEntity () const;
	EntityComponent* GetSelectedComponent () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates the index to point to the next child component.  If there aren't any more children, then
	 * the index will point to the next sibling component (if any).
	 */
	void FindNextComponent ();
};
SD_END