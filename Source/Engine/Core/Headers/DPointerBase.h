/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DPointerBase.h
  Defines the functionality how a DPointer may register and remove itself from the DPointer linked list.

  This class does not contain a pointer to an object.
=====================================================================
*/

#pragma once

#include "DProperty.h"
#include "DPointerInterface.h"

SD_BEGIN
class CORE_API DPointerBase
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Previous DPointer in the Object's DPointer linked list.  The object that this pointer points to will maintain a
	linked list of all DPointers that references that object.  If null, then this pointer is leading the pointer linked list. */
	DPointerBase* PreviousPointer;

	/* Next DPointer in the Object's DPointer linked list.  The object that this pointer points to will maintain a
	linked list of all DPointers that references that object.  If null, then this pointer is at the end of the pointer linked list. */
	DPointerBase* NextPointer;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	DPointerBase ();
	DPointerBase (const DPointerBase& other);
	virtual ~DPointerBase ();


	/*
	=====================
	  Operators
	=====================
	*/

public:
	//Checking if this pointer is pointing to nullptr or not
	virtual operator bool () const = 0;
	virtual bool operator! () const = 0;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(DPointerBase*, PreviousPointer)
	DEFINE_ACCESSOR(DPointerBase*, NextPointer)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets the pointer to point to nullptr.  Warning, calling this function will sever this pointer
	 * from the pointer linked list.
	 */
	virtual void ClearPointer () = 0;

	/**
	 * Returns a raw pointer of this DPointer instance (not the actual object it's pointing at).
	 */
	virtual DPointerBase* GetPointerAddress ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Registers this DPointer to the object's DPointer linked list.
	 */
	void RegisterDPointer (const DPointerInterface* referencedObj);

	/**
	 * Removes this DPointer from the referenced object's DPointer linked list.
	 */
	void RemoveDPointer (const DPointerInterface* oldReferencedObj);
};
SD_END