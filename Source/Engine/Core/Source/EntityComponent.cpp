/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EntityComponent.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, EntityComponent, SD, Entity)

void EntityComponent::InitProps ()
{
	Super::InitProps();

	Owner = nullptr;
	NumInvisibleOwners = 0;
}

void EntityComponent::AddComponentModifier (EntityComponent* newComponent)
{
	Super::AddComponentModifier(newComponent);

	if (Owner != nullptr)
	{
		//Notify the parent components to also adjust their modifiers
		Owner->AddComponentModifier(newComponent);
	}
}

bool EntityComponent::RemoveComponentModifier (EntityComponent* oldComponent)
{
	if (Super::RemoveComponentModifier(oldComponent) && Owner != nullptr)
	{
		Owner->RemoveComponentModifier(oldComponent);
		return true;
	}

	return false;
}

bool EntityComponent::IsVisible () const
{
	return (Super::IsVisible() && NumInvisibleOwners <= 0);
}

unsigned int EntityComponent::CalculateHashID () const
{
	return (Engine::GetEngine(Engine::MAIN_ENGINE_IDX)->GetComponentHashNumber());
}

void EntityComponent::Destroyed ()
{
	DetachSelfFromOwner();

	Super::Destroyed();
}

Entity* EntityComponent::GetRootEntity () const
{
	if (dynamic_cast<EntityComponent*>(Owner.Get()))
	{
		return (dynamic_cast<EntityComponent*>(Owner.Get())->GetRootEntity());
	}

	return Owner.Get();
}

bool EntityComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	return (ownerCandidate != nullptr && ownerCandidate != this);
}

void EntityComponent::DetachSelfFromOwner ()
{
	if (Owner == nullptr)
	{
		return; //already detached
	}

	Owner->RemoveComponent(this);
}

Entity* EntityComponent::GetOwner () const
{
	return Owner.Get();
}

void EntityComponent::AttachTo (Entity* newOwner)
{
	Owner = newOwner;

	//Identify how many EntityComponents are invisible in this new ownership chain.
	EntityComponent* owningComponent = dynamic_cast<EntityComponent*>(Owner.Get());
	if (owningComponent != nullptr)
	{
		NumInvisibleOwners = owningComponent->NumInvisibleOwners;
		if (!owningComponent->bVisible)
		{
			NumInvisibleOwners++; //Handle case where component is attaching to an invisible component
		}

		if (NumInvisibleOwners > 0)
		{
			for (ComponentIterator subComponents(this, true); subComponents.GetSelectedComponent() != nullptr; subComponents++)
			{
				subComponents.GetSelectedComponent()->NumInvisibleOwners += NumInvisibleOwners;
			}
		}
	}
	else if (Owner != nullptr && !Owner->IsVisible())
	{
		NumInvisibleOwners = 1;
	}
}

void EntityComponent::ComponentDetached ()
{
	Owner = nullptr;

	//Update this component and subcomponents' NumInvisibleOwners counter
	if (NumInvisibleOwners > 0)
	{
		INT numToDeduct = NumInvisibleOwners;
		NumInvisibleOwners = 0;
		for (ComponentIterator subComponents(this, true); subComponents.GetSelectedComponent(); subComponents++)
		{
			subComponents.GetSelectedComponent()->NumInvisibleOwners -= numToDeduct;

			//HACK: Quick fix to work around bad reference counted components
			subComponents.GetSelectedComponent()->NumInvisibleOwners = Utils::Max<INT>(subComponents.GetSelectedComponent()->NumInvisibleOwners, 0);
			//CHECK(subComponents.GetSelectedComponent()->NumInvisibleOwners >= 0)
		}
	}
}
SD_END