/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Entity.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, Entity, SD, Object)

void Entity::InitProps ()
{
	Super::InitProps();

	bVisible = true;
	ComponentsModifier = 0;
}
	
unsigned int Entity::CalculateHashID () const
{
	return (Engine::GetEngine(Engine::MAIN_ENGINE_IDX)->GetEntityHashNumber());
}

void Entity::Destroyed ()
{
	//Ensure to mark this object for deletion before deleting components (to slightly increase performance since the entity doesn't need to track its component modifier)
	//This also notifies Components to ignore updating Invisibility ref counters.
	Super::Destroyed();

	//Detach and destroy all components
	for (INT i = Components.size() - 1; i >= 0; i--)
	{
		Components.at(i.Value)->Destroy();
	}

	Components.clear();
	ComponentsModifier = 0;
}

bool Entity::AddComponent (EntityComponent* newComponent, bool bLogOnFail, bool bDeleteOnFail)
{
	if (newComponent == nullptr)
	{
		if (bLogOnFail)
		{
			CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot attach an undefined component to %s"), ToString());
		}

		return false;
	}

	if (newComponent->CanBeAttachedTo(this))
	{
		newComponent->DetachSelfFromOwner();
		newComponent->AttachTo(this);

#ifdef DEBUG_MODE
		for (UINT_TYPE i = 0; i < Components.size(); i++)
		{
			if (Components.at(i) == newComponent)
			{
				Engine::FindEngine()->FatalError(DString::CreateFormattedString(TXT("%s is already registered to %s.  Having duplicate entries in the component list would cause unreliable behavior when removing components.  This check is only executed in debug builds."), newComponent->ToString(), ToString()));
				return false;
			}
		}
#endif
		Components.push_back(newComponent);
		AddComponentModifier(newComponent);
		return true;
	}

	if (bLogOnFail)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Failed to attach %s to %s since the component refused to attach itself to the specified entity."), newComponent->ToString(), ToString());
	}

	if (bDeleteOnFail)
	{
		newComponent->Destroy();
	}

	return false;
}

bool Entity::RemoveComponent (EntityComponent* target)
{
	for (UINT_TYPE i = 0; i < Components.size(); i++)
	{
		if (Components.at(i) == target)
		{
			//Invoke this before anything else since EntityComponent::RemoveComponentModifier references its owner
			RemoveComponentModifier(target);
			Components.at(i)->ComponentDetached();
			Components.erase(Components.begin() + i);

			return true;
		}
	}

	return false;
}

void Entity::AddComponentModifier (EntityComponent* newComponent)
{
	ComponentsModifier |= newComponent->GetObjectHash();
}

bool Entity::RemoveComponentModifier (EntityComponent* oldComponent)
{
	if (GetPendingDelete())
	{
		//Don't bother iterating through components if the Entity is being destroyed.
		return false;
	}

	bool bFoundMatch = false;

	//Iterate through all subcomponents to ensure that this entity does not possess another component of matching hash
	for (UINT_TYPE i = 0; i < Components.size(); i++)
	{
		if (Components.at(i) != oldComponent && Components.at(i)->GetObjectHash() == oldComponent->GetObjectHash())
		{
			bFoundMatch = true;
			break;
		}

		if (Components.at(i)->FindSubComponent(oldComponent->GetObjectHash()) != nullptr)
		{
			bFoundMatch = true;
			break;
		}
	}

	if (!bFoundMatch)
	{
		ComponentsModifier ^= oldComponent->GetObjectHash();
	}

	return !bFoundMatch;
}

EntityComponent* Entity::FindSubComponent (unsigned int targetComponentHash, bool bRecursive) const
{
	for (UINT_TYPE i = 0; i < Components.size(); i++)
	{
		if (Components.at(i) == nullptr)
		{
			continue;
		}

		if ((Components.at(i)->GetObjectHash() & targetComponentHash) > 0)
		{
			return Components.at(i).Get();
		}

		if (bRecursive && (Components.at(i)->GetComponentsModifier() & targetComponentHash) > 0)
		{
			EntityComponent* result = Components.at(i)->FindSubComponent(targetComponentHash);
			if (result != nullptr)
			{
				return result;
			}
		}
	}

	return nullptr;
}

EntityComponent* Entity::FindSubComponent (const DClass* targetClass, bool bRecursive) const
{
	for (UINT_TYPE i = 0; i < Components.size(); i++)
	{
		if (Components.at(i) == nullptr)
		{
			continue;
		}

		if (Components.at(i)->StaticClass()->IsChildOf(targetClass))
		{
			return Components.at(i).Get();
		}

		if (!bRecursive)
		{
			continue;
		}

		EntityComponent* result = Components.at(i)->FindSubComponent(targetClass);
		if (result != nullptr)
		{
			return result;
		}
	}

	return nullptr;
}

void Entity::SetVisibility (bool newVisibility)
{
	if (newVisibility == bVisible)
	{
		return;
	}

	bVisible = newVisibility;
	INT refCountMultiplier = (bVisible) ? -1 : 1;

	//Increment the EntityComponents' visibility reference counter.
	for (ComponentIterator iter(this, true); iter; iter++)
	{
		EntityComponent* selectedComp = iter.GetSelectedComponent();
		selectedComp->NumInvisibleOwners = Utils::Max<INT>(0, selectedComp->NumInvisibleOwners + (1 * refCountMultiplier));
	}
}

bool Entity::IsVisible () const
{
	return bVisible;
}

unsigned int Entity::GetComponentsModifier () const
{
	return ComponentsModifier;
}

#ifdef DEBUG_MODE
void Entity::DebugTick (FLOAT deltaTime)
{
}
#endif
SD_END