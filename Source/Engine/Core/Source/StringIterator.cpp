/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  StringIterator.cpp
=====================================================================
*/

#include "CoreClasses.h"
#include "utf8.h"

SD_BEGIN

#if USE_UTF32 || USE_UTF16 || USE_UTF8
const unsigned int StringIterator::CharacterArraySize = 4;
#else
const unsigned int StringIterator::CharacterArraySize = 1;
#endif

StringIterator::StringIterator (const DString* inString, bool bPointToBeginning)
{
	String = inString;

	//Always point to the beginning if the string is empty since you can't backwards iterate an empty string.
	if (bPointToBeginning || inString->ReadString().size() == 0)
	{
		BaseIterator = inString->ReadString().begin();
		StringCharByteIdx = 0;
	}
	else
	{
		BaseIterator = inString->ReadString().begin() + (inString->ReadString().size() - 1); //jump to the last element

#if USE_UTF32
		CHECK(inString->ReadString().size() >= 4) //UTF-32 characters must take up 4 bytes.
		for (byte i = 1; i < 3; ++i) //Decrement three more times to move to the start of the last character
		{
			--BaseIterator;
		}
#elif USE_UTF16
	#error In StringIterator::StringIterator, please decrement BaseIterator until it points to the start of a valid UTF-16 character.
#elif USE_UTF8
		if (!String->IsEmpty())
		{
			//Need to move the BaseIterator back until it points to the beginning of a valid UTF-8 character.
			//since it's possible that one element of a string is part of a character.
			while (!utf8::is_valid(BaseIterator, inString->ReadString().end()))
			{
				CHECK(!IsAtBeginning()) //This should never break unless the string is completely made of invalid UTF-8 characters.
				--BaseIterator; //Move back an element, and check that new byte data to see if that's a start of a character.
			}
		}
#endif
		StringCharByteIdx = (BaseIterator - inString->ReadString().begin());
	}
	
	CalcSizeOfChar();
	bDirty = true;
}

StringIterator::StringIterator (const StringIterator& other)
{
	BaseIterator = other.BaseIterator;

	for (unsigned int i = 0; i < CharacterArraySize; i++)
	{
		CurrentCharacter[i] = other.CurrentCharacter[i];
	}

	SizeOfCharacter = other.SizeOfCharacter;
	bDirty = other.bDirty;
	String = other.String;
	StringCharByteIdx = other.StringCharByteIdx;
}

StringIterator::~StringIterator ()
{

}

StringIterator& StringIterator::operator++ ()
{
	if (IsAtEnd())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempted to move string iterator beyond the end of the string:  \"%s\""), (*String));
		return *this;
	}

	BaseIterator += SizeOfCharacter;
	StringCharByteIdx += SizeOfCharacter;
	CalcSizeOfChar();
	bDirty = true;

	return *this;
}

StringIterator StringIterator::operator++ (int) //iter++
{
	StringIterator temp = *this;

	++(*this);
	return temp;
}

StringIterator& StringIterator::operator-- ()
{
	//Can't move beyond the beginning
	if (IsAtBeginning())
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempted to move string iterator beyond the beginning of the string:  \"%s\""), (*String));
		return *this;
	}

#if USE_UTF32
	BaseIterator -= 4; //UTF-32 characters are always 4 bytes.
	StringCharByteIdx -= 4;
#elif USE_UTF16
	#error Identify how many bytes the StringIterator will need to decrement for UTF-16 strings.
	StringCharByteIdx = BaseIterator - String->String.begin();
#elif USE_UTF8
	utf8::prior(BaseIterator, String->ReadString().begin());
	StringCharByteIdx = BaseIterator - String->ReadString().begin();
#else
	--BaseIterator;
	StringCharByteIdx--;
#endif

	CalcSizeOfChar();
	bDirty = true;
	return *this;
}

StringIterator StringIterator::operator-- (int) //iter--
{
	StringIterator temp = *this;

	--(*this);
	return temp;
}

StringIterator& StringIterator::operator= (const StringIterator& other)
{
	BaseIterator = other.BaseIterator;

	for (unsigned int i = 0; i < CharacterArraySize; i++)
	{
		CurrentCharacter[i] = other.CurrentCharacter[i];
	}
	
	SizeOfCharacter = other.SizeOfCharacter;
	bDirty = other.bDirty;
	StringCharByteIdx = other.StringCharByteIdx;

	return (*this);
}

bool StringIterator::operator== (const StringIterator& other) const
{
	return (BaseIterator == other.BaseIterator);
}

bool StringIterator::operator!= (const StringIterator& other) const
{
	return (BaseIterator != other.BaseIterator);
}

bool StringIterator::operator== (const std::string::iterator& other) const
{
	return (BaseIterator == other);
}

bool StringIterator::operator!= (const std::string::iterator& other) const
{
	return (BaseIterator != other);
}

bool StringIterator::operator== (const std::string::const_iterator& other) const
{
	return (BaseIterator == other);
}

bool StringIterator::operator!= (const std::string::const_iterator& other) const
{
	return (BaseIterator != other);
}

DString StringIterator::operator* () const
{
	return GetString();
}

DString StringIterator::GetString () const
{
	if (bDirty)
	{
		ReadCharacterData();
	}

#if USE_UTF32
	//UTF-32 characters should be divisible by 32 bits.
	CHECK((SizeOfCharacter%4) == 0)
	#error Please implement StringIterator::GetString() for UTF-32.
#elif USE_UTF16
	//UTF-16 characters should be divisible by 16 bits.
	CHECK((SizeOfCharacter%2) == 0)
	#error Please implement StringIterator::GetString() for UTF-16.
#elif USE_UTF8
	DString result = DString::EmptyString;
	for (unsigned int i = 0; i < SizeOfCharacter; i++)
	{
		result += CurrentCharacter[i];
	}

	return result;
#else
	//Ansi strings are always 1 byte.
	return DString(reinterpret_cast<TStringChar>(CurrentCharacter[0]));
#endif
}

bool StringIterator::IsAtBeginning () const
{
	CHECK(String != nullptr)
	return (BaseIterator == String->ReadString().begin());
}

bool StringIterator::IsAtEnd () const
{
	CHECK(String != nullptr)
	return (BaseIterator == String->ReadString().end());
}

void StringIterator::GetCurrentCharacterData (unsigned char* outByteArray) const
{
	if (bDirty)
	{
		ReadCharacterData();
	}

	for (unsigned int i = 0; i < SizeOfCharacter; i++)
	{
		outByteArray[i] = CurrentCharacter[i];	
	}
}

void StringIterator::ReadCharacterData () const
{
	bDirty = false;
	CHECK(SizeOfCharacter <= CharacterArraySize)
	for (unsigned int i = 0; i < SizeOfCharacter; i++)
	{
		CurrentCharacter[i] = *(BaseIterator + i);
	}
}

void StringIterator::CalcSizeOfChar () const
{
	if (IsAtEnd())
	{
		SizeOfCharacter = 0;
		return;
	}

#if USE_UTF32
	//UTF-32 characters are always 4 bytes long
	SizeOfCharacter = 4;
#elif USE_UTF16
	#error Please implement StringIterator::CalcSizeOfChar for UTF-16.
#elif USE_UTF8
	//Various useful bit flags for UTF-8 encoding
	static const unsigned char firstBitMask = 128; // 1000 0000
	static const unsigned char secondBitMask = 64; // 0100 0000
	static const unsigned char thirdBitMask = 32; // 0010 0000
	static const unsigned char fourthBitMask = 16; // 0001 0000

	unsigned char firstByte = *BaseIterator;
	if ((firstByte & firstBitMask) == 0)
	{
		//Within ASCII range 0xxx xxxx
		SizeOfCharacter = 1;
		return;
	}

	//If the first bit is 1, then the second bit must also be 1.  11xx xxxx
	//Otherwise this means that the iterator is pointing in the middle of a character instead of a start of a code point.
	CHECK_INFO(((firstByte & secondBitMask) > 0), TXT("A string iterator is pointing in the middle of a character instead of at the start of the character."))
	if ((firstByte & thirdBitMask) > 0) //111x xxxx
	{
		if ((firstByte & fourthBitMask) > 0) //1111 xxxx
		{
			SizeOfCharacter = 4;
			return;
		}

		SizeOfCharacter = 3;
		return;
	}

	SizeOfCharacter = 2;
#else
	SizeOfCharacter = 1;
#endif
}
SD_END