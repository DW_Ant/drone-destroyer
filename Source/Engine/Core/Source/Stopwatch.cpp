/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Stopwatch.cpp
=====================================================================
*/

#include "CoreClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
Stopwatch::Stopwatch () :
	Name(TXT("Stopwatch")),
	AutoLog(true),
	StartTime(GetSystemTime()),
	RecordedTime(0.f)
{
	//Noop
}

Stopwatch::Stopwatch (const DString& stopwatchName) :
	Name(stopwatchName),
	AutoLog(true),
	StartTime(GetSystemTime()),
	RecordedTime(0.f)
{
	//Noop
}

Stopwatch::Stopwatch (const DString& stopwatchName, bool inAutoLog) :
	Name(stopwatchName),
	AutoLog(inAutoLog),
	StartTime(GetSystemTime()),
	RecordedTime(0.f)
{
	//Noop
}

Stopwatch::Stopwatch (const Stopwatch& cpyObj) :
	Name(cpyObj.Name),
	AutoLog(cpyObj.AutoLog),
	StartTime(cpyObj.StartTime),
	RecordedTime(cpyObj.RecordedTime)
{
	//Noop
}

Stopwatch::~Stopwatch ()
{
	DestroyStopwatch();
}

void Stopwatch::PauseTime ()
{
	RecordedTime = GetElapsedTime();
	StartTime = -1.f; //pauses timer
}

void Stopwatch::ResumeTime ()
{
	StartTime = GetSystemTime();
}
	
void Stopwatch::ResetTime ()
{
	RecordedTime = 0.f;

	if (StartTime >= 0)
	{
		StartTime = GetSystemTime();
	}
}

FLOAT Stopwatch::GetElapsedTime () const
{
	FLOAT result = RecordedTime;

	if (StartTime >= 0)
	{
		result += GetSystemTime() - StartTime;
	}

	return result;
}

FLOAT Stopwatch::GetSystemTime () const
{
	clock_t curTime = clock();

	return static_cast<float>(curTime);
}

void Stopwatch::DestroyStopwatch ()
{
	if (AutoLog)
	{
		CoreLog.Log(LogCategory::LL_Debug, TXT("%s is destroyed.  The recorded time of this stopwatch was:  %s milliseconds."), Name, GetElapsedTime());
	}
}
SD_END

#endif