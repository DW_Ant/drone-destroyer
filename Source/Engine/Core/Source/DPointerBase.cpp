/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DPointerBase.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
DPointerBase::DPointerBase ()
{
	PreviousPointer = nullptr;
	NextPointer = nullptr;
}

DPointerBase::DPointerBase (const DPointerBase& other)
{
	PreviousPointer = nullptr;
	NextPointer = nullptr;
}

DPointerBase::~DPointerBase ()
{

}

DPointerBase* DPointerBase::GetPointerAddress ()
{
	return this;
}

void DPointerBase::RegisterDPointer (const DPointerInterface* referencedObj)
{
	if (referencedObj == nullptr || !referencedObj->CanBePointed())
	{
		return;
	}

	DPointerBase* oldLeadingPointer = referencedObj->GetLeadingDPointer();
	if (oldLeadingPointer != nullptr && oldLeadingPointer != this)
	{
		//Insert this pointer to be at the front of the linked list
		oldLeadingPointer->PreviousPointer = this;
		NextPointer = oldLeadingPointer;
	}

	referencedObj->SetLeadingDPointer(this);
	PreviousPointer = nullptr;
}

void DPointerBase::RemoveDPointer (const DPointerInterface* oldReferencedObj)
{
	if (PreviousPointer == nullptr)
	{
		if (oldReferencedObj != nullptr)
		{
			//Need to reassign the object's leading pointer
			oldReferencedObj->SetLeadingDPointer(oldReferencedObj->CanBePointed() ? NextPointer : nullptr);
		}
	}
	else
	{
		PreviousPointer->NextPointer = NextPointer;
	}

	if (NextPointer != nullptr)
	{
		NextPointer->PreviousPointer = PreviousPointer;
	}

	//Sever this pointer from any linked list
	PreviousPointer = nullptr;
	NextPointer = nullptr;
}
SD_END