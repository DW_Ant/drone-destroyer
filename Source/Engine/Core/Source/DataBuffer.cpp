/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DataBuffer.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
DataBuffer::DataBuffer ()
{
	DataBufferIsLittleEndian = DataBuffer::IsSystemLittleEndian();
	ReadIdx = 0;
}

DataBuffer::DataBuffer (int numBytes)
{
	DataBufferIsLittleEndian = DataBuffer::IsSystemLittleEndian();
	if (numBytes > 0)
	{
		Data.reserve(numBytes);
	}

	ReadIdx = 0;
}

DataBuffer::DataBuffer (int numBytes, bool inDataBufferIsLittleEndian)
{
	DataBufferIsLittleEndian = inDataBufferIsLittleEndian;
	if (numBytes > 0)
	{
		Data.reserve(numBytes);
	}

	ReadIdx = 0;
}

DataBuffer::~DataBuffer ()
{
	//Noop
}

bool DataBuffer::IsSystemLittleEndian ()
{
	//Nice utility found from here:  http://vijayinterviewquestions.blogspot.com/2007/07/what-little-endian-and-big-endian-how.html
	int num = 1;

	//Look at the first byte of num to see if it contains 1 or not.
	//Little endian would format 1 as 00000001 00000000 00000000 00000000
	//Big endian would format 1 as 00000000 00000000 00000000 00000001
	return (*(char*)&num == 1);
}

void DataBuffer::SwapByteOrder (char* outCharBuffer, size_t numChars)
{
	char* end = (outCharBuffer + numChars);
	std::reverse(outCharBuffer, end);
}

void DataBuffer::AdvanceIdx (size_t numBytes) const
{
	ReadIdx += numBytes;
	CHECK(ReadIdx >= 0 && ReadIdx <= Data.size())
}

void DataBuffer::ReadBytes (char* OUT outReadData, size_t numBytes) const
{
	CHECK((ReadIdx + numBytes) <= Data.size())
	CHECK(numBytes != UINT_INDEX_NONE) //Protection against inf loop (for unsigned int overflow)

	for (size_t i = 0; i < numBytes; ++i)
	{
		outReadData[i] = Data[ReadIdx + i];
	}

	AdvanceIdx(numBytes);
}

void DataBuffer::WriteBytes (const char* dataToAdd, size_t numBytes)
{
	CHECK(numBytes != UINT_INDEX_NONE) //Protection against inf loop (for unsigned int overflow)

	for (size_t i = 0; i < numBytes; ++i)
	{
		Data.push_back(dataToAdd[i]);
	}
}
SD_END