/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DClass.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
DClass::DClass (const DString& className) : CDO{nullptr}
{
	ClassType = CT_None;

	DuneClassName = className;
	ParentClass = nullptr;
	IsAbstractClass = true;
}

DClass::~DClass ()
{
	switch (ClassType)
	{
		case(CT_Object):
			if (CDO.DefaultObject != nullptr)
			{
				delete CDO.DefaultObject;
				CDO.DefaultObject = nullptr;
			}
			break;

		case(CT_EngineComponent):
			if (CDO.DefaultEngineComponent != nullptr)
			{
				delete CDO.DefaultEngineComponent;
				CDO.DefaultEngineComponent = nullptr;
			}
			break;
	}
}

void DClass::SetCDO (Object* newObject)
{
	CHECK_INFO(CDO.DefaultObject == nullptr, "Only 1 CDO can be assigned to a DClass instance.")
	ClassType = CT_Object;
	CDO.DefaultObject = newObject;
	IsAbstractClass = false;
}

void DClass::SetCDO (EngineComponent* newCDO)
{
	CHECK_INFO(CDO.DefaultEngineComponent == nullptr, "Only 1 CDO can be assigned to a DClass instance.")
	ClassType = CT_EngineComponent;
	CDO.DefaultEngineComponent = newCDO;
	IsAbstractClass = false;
}

bool DClass::IsChildOf (const DClass* targetParentClass) const
{
	if (targetParentClass == this)
	{
		return true;
	}
	else if (ParentClass == nullptr)
	{
		return false;
	}

	return (ParentClass->IsChildOf(targetParentClass));
}

bool DClass::IsParentOf (const DClass* targetChildClass) const
{
	//It's faster to do it this way since we don't have to iterate through all children.
	return (targetChildClass->IsChildOf(this));
}

DString DClass::ToString () const
{
	return DuneClassName;
}

const DClass* DClass::GetSuperClass () const
{
	return ParentClass;
}

const Object* DClass::GetDefaultObject () const
{
	if (ClassType == CT_Object)
	{
		return CDO.DefaultObject;
	}

	return nullptr;
}

const EngineComponent* DClass::GetDefaultEngineComponent () const
{
	if (ClassType == CT_EngineComponent)
	{
		return CDO.DefaultEngineComponent;
	}

	return nullptr;
}

void DClass::AddChild (DClass* child)
{
	UINT_TYPE index;

	//Added alphabetically for organizational purposes
	for (index = 0; index < Children.size(); index++)
	{
		if (Children.at(index)->GetDuneClassName().Compare(child->GetDuneClassName(), DString::CC_CaseSensitive) > 0)
		{
			break;
		}
	}

	Children.insert(Children.begin() + index, child);
}
SD_END