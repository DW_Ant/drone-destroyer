/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LogCategory.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
const unsigned int LogCategory::FLAG_STANDARD_OUTPUT =	0x00000001;
const unsigned int LogCategory::FLAG_OS_OUTPUT =		0x00000002;
const unsigned int LogCategory::FLAG_LOG_FILE =			0x00000004;
const unsigned int LogCategory::FLAG_OUTPUT_WINDOW =	0x00000008;
const unsigned int LogCategory::FLAG_CONSOLE_MSG =		0x00000010;
const unsigned int LogCategory::FLAG_ALL =				0xffffffff;

const unsigned int LogCategory::VERBOSITY_ALL =
#ifdef DEBUG_MODE
	LL_Debug |
#endif
	LL_Verbose | LL_Log | LL_Warning | LL_Critical | LL_Fatal;

const unsigned int LogCategory::VERBOSITY_DEFAULT = ~LogCategory::LL_Verbose; //Default to all except for Verbose.

LogCategory CoreLog(TXT("Core"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT);

#ifdef DEBUG_MODE
LogCategory UnitTestLog(TXT("Unit Test"), LogCategory::VERBOSITY_DEFAULT,
	LogCategory::FLAG_LOG_FILE |
	LogCategory::FLAG_OS_OUTPUT |
	LogCategory::FLAG_OUTPUT_WINDOW |
	LogCategory::FLAG_STANDARD_OUTPUT);
#endif

LogCategory::LogCategory (const DString& inLogTitle, unsigned int inVerbosityFlags, unsigned int inUsageFlags)
{
	LogTitle = inLogTitle;
	VerbosityFlags = inVerbosityFlags;
	UsageFlags = inUsageFlags;
}

LogCategory::~LogCategory ()
{
	//Noop
}

DString LogCategory::LogLevelToString (ELogLevel logLevel)
{
	switch (logLevel)
	{
#ifdef DEBUG_MODE
		case (LL_Debug): return TXT("Debug");
#endif
		case (LL_Verbose): return TXT("Verbose");
		case (LL_Log): return TXT("Log");
		case (LL_Warning): return TXT("Warning");
		case (LL_Critical): return TXT("Critical");
		case (LL_Fatal): return TXT("Fatal");
	}

	return DString::EmptyString;
}

bool LogCategory::IsLogLevelRelevant (ELogLevel logLevel) const
{
	return ((logLevel & VerbosityFlags) > 0);
}

void LogCategory::Log (ELogLevel logLevel, const DString& msg) const
{
	if (IsLogLevelRelevant(logLevel))
	{
		Engine* localEngine = Engine::FindEngine();
		if (localEngine != nullptr)
		{
			localEngine->ProcessLog(this, logLevel, msg);
		}
	}
}

void LogCategory::SetVerbosityFlags (unsigned int newVerbosityFlags)
{
	VerbosityFlags = newVerbosityFlags;
}

void LogCategory::SetUsageFlags (unsigned int newUsageFlags)
{
	UsageFlags= newUsageFlags;
}

DString LogCategory::GetLogTitle () const
{
	return LogTitle;
}

const DString& LogCategory::ReadLogTitle () const
{
	return LogTitle;
}

unsigned int LogCategory::GetVerbosityFlags () const
{
	return VerbosityFlags;
}

unsigned int LogCategory::GetUsageFlags () const
{
	return UsageFlags;
}
SD_END