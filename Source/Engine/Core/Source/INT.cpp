/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  INT.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
INT::INT ()
{

}

INT::INT (const int32 newValue)
{
	Value = newValue;
}

INT::INT (const INT& copyInt)
{
	Value = copyInt.Value;
}

INT::INT (const uint32& newValue)
{
	Value = GetInt(newValue);
}

INT::INT (const FLOAT& newValue)
{
	Value = newValue.ToINT().Value;
}

INT::INT (const DString& text)
{
	if (text.IsEmpty())
	{
		Value = 0;
		return;
	}

	try
	{
		Value = stoi(text.ReadString());
	}
	catch(std::out_of_range&)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("There's not enough space to fit %s within an INT.  Defaulting to INT_MAX(%s)."), text, INT(INT_MAX));
		Value = INT_MAX;
	}
	catch(std::invalid_argument&)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot construct an INT from %s.  Defaulting to 0."), text);
		Value = 0;
	}
}

#ifdef PLATFORM_64BIT
INT::INT (const int64 newValue)
{
	Value = newValue;
}

INT::INT (const uint64 newValue)
{
	Value = GetInt(newValue);
}
#endif

void INT::operator= (const INT& copyInt)
{
	Value = copyInt.Value;
}

void INT::operator= (const int32 otherInt)
{
	Value = otherInt;
}

void INT::operator= (const uint32 otherInt)
{
	Value = GetInt(otherInt);
}

#ifdef PLATFORM_64BIT
void INT::operator= (const int64 otherInt)
{
	Value = otherInt;
}

void INT::operator= (const uint64 otherInt)
{
	Value = GetInt(otherInt);
}
#endif

INT INT::operator++ ()
{
	return ++Value;
}

INT INT::operator++ (int)
{
	return Value++;
}

INT INT::operator- () const
{
	return -Value;
}

INT INT::operator-- ()
{
	return --Value;
}

INT INT::operator-- (int)
{
	return Value--;
}

DString INT::ToString () const
{
	return DString::MakeString(Value);
}

void INT::Serialize (DataBuffer& dataBuffer) const
{
	//Convert INT to char array
	const int numBytes = sizeof(Value);
	char charArray[numBytes + 1]; //include null terminator
	memcpy(charArray, &Value, numBytes);

	//Reverse bytes if needed
	if (dataBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(charArray, numBytes);
	}

	dataBuffer.WriteBytes(charArray, numBytes);
}

void INT::Deserialize (const DataBuffer& dataBuffer)
{
	const int numBytes = sizeof(Value);
	char rawData[numBytes];
	dataBuffer.ReadBytes(rawData, numBytes);

	//Reverse bytes if needed
	if (dataBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(rawData, numBytes);
	}

	memcpy(&Value, rawData, numBytes);
}

INT INT::MakeINT (float value)
{
	return INT(FLOAT(value).ToINT());
}

FLOAT INT::ToFLOAT () const
{
	return FLOAT(static_cast<float>(Value));
}

int32 INT::ToInt32 () const
{
#ifdef PLATFORM_64BIT
	return static_cast<int32>(Value);
#else
	return Value;
#endif
}

INT INT::Abs (INT value)
{
	return abs(value.Value);
}

void INT::AbsInline ()
{
	Value = std::abs(Value);
}

int32 INT::GetInt (uint32 target)
{
	if (target > INT_MAX)
	{
		return std::numeric_limits<int>::max();
	}
	else
	{
		return static_cast<int32>(target);
	}
}

#ifdef PLATFORM_64BIT
int64 INT::GetInt (uint64 target)
{
	if (target > INT_MAX)
	{
		return INT_MAX;
	}
	else
	{
		return static_cast<int64>(target);
	}
}
#endif

uint32 INT::ToUnsignedInt32 () const
{
	INT result = Value;
	if (result < 0)
	{
		//Set the value to 0 so that the signed bit will not convert this negative number to a large unsigned int
		result = 0;
	}

	return static_cast<uint32>(result.Value);
}

#ifdef PLATFORM_64BIT
uint64 INT::ToUnsignedInt64 () const
{
	INT result = Value;
	if (result < 0)
	{
		//Set the value to 0 so that the signed bit will not convert this negative number to a large unsigned int
		result = 0;
	}

	return static_cast<uint64>(result.Value);
}
#endif

UINT_TYPE INT::ToUnsignedInt () const
{
#ifdef PLATFORM_64BIT
	return ToUnsignedInt64();
#else
	return ToUnsignedInt32();
#endif
}

INT INT::NumDigits () const
{
	return (Value != 0) ? static_cast<int>(log10((double)abs(Value))) + 1 : 1;
}

DString INT::ToFormattedString (const INT minDigits) const
{
	INT curDigits = NumDigits();
	INT numZeroesNeeded = minDigits - curDigits;
	if (numZeroesNeeded > 0)
	{
		DString prefix = (Value >= 0) ? DString::EmptyString : TXT("-");
		return prefix + DString(TString(numZeroesNeeded.Value, '0')) + Abs(*this).ToString();
	}

	return ToString();
}

#pragma region "External Operators"
bool operator== (const INT& left, const INT& right)
{
	return (left.Value == right.Value);
}

bool operator== (const INT& left, const INT_TYPE& right)
{
	return (left.Value == right);
}

bool operator== (const INT_TYPE& left, const INT& right)
{
	return (left == right.Value);
}

bool operator!= (const INT& left, const INT& right)
{
	return (left.Value != right.Value);
}

bool operator!= (const INT& left, const INT_TYPE& right)
{
	return (left.Value != right);
}

bool operator!= (const INT_TYPE& left, const INT& right)
{
	return (left != right.Value);
}

bool operator< (const INT& left, const INT& right)
{
	return (left.Value < right.Value);
}

bool operator< (const INT& left, const INT_TYPE& right)
{
	return (left.Value < right);
}

bool operator< (const INT_TYPE& left, const INT& right)
{
	return (left < right.Value);
}

bool operator<= (const INT& left, const INT& right)
{
	return (left.Value <= right.Value);
}

bool operator<= (const INT& left, const INT_TYPE& right)
{
	return (left.Value <= right);
}

bool operator<= (const INT_TYPE& left, const INT& right)
{
	return (left <= right.Value);
}

bool operator> (const INT& left, const INT& right)
{
	return (left.Value > right.Value);
}

bool operator> (const INT& left, const INT_TYPE& right)
{
	return (left.Value > right);
}

bool operator> (const INT_TYPE& left, const INT& right)
{
	return (left > right.Value);
}

bool operator>= (const INT& left, const INT& right)
{
	return (left.Value >= right.Value);
}

bool operator>= (const INT& left, const INT_TYPE& right)
{
	return (left.Value >= right);
}

bool operator>= (const INT_TYPE& left, const INT& right)
{
	return (left >= right.Value);
}

INT operator>> (const INT& left, const INT& right)
{
	return INT(left.Value >> right.Value);
}

INT operator>> (const INT& left, const INT_TYPE& right)
{
	return INT(left.Value >> right);
}

INT operator>> (const INT_TYPE& left, const INT& right)
{
	return INT(left >> right.Value);
}

INT& operator>>= (INT& left, const INT& right)
{
	left.Value >>= right.Value;
	return left;
}

INT& operator>>= (INT& left, const INT_TYPE& right)
{
	left.Value >>= right;
	return left;
}

INT_TYPE& operator>>= (INT_TYPE& left, const INT& right)
{
	left >>= right.Value;
	return left;
}

INT operator<< (const INT& left, const INT& right)
{
	return INT(left.Value << right.Value);
}

INT operator<< (const INT& left, const INT_TYPE& right)
{
	return INT(left.Value << right);
}

INT operator<< (const INT_TYPE& left, const INT& right)
{
	return INT(left << right.Value);
}

INT& operator<<= (INT& left, const INT& right)
{
	left.Value <<= right.Value;
	return left;
}

INT& operator<<= (INT& left, const INT_TYPE& right)
{
	left.Value <<= right;
	return left;
}

INT_TYPE& operator<<= (INT_TYPE& left, const INT& right)
{
	left <<= right.Value;
	return left;
}

INT operator& (const INT& left, const INT& right)
{
	return INT(left.Value & right.Value);
}

INT operator& (const INT& left, const INT_TYPE& right)
{
	return INT(left.Value & right);
}

INT operator& (const INT_TYPE& left, const INT& right)
{
	return INT(left & right.Value);
}

INT operator^ (const INT& left, const INT& right)
{
	return INT(left.Value & right.Value);
}

INT operator^ (const INT& left, const INT_TYPE& right)
{
	return INT(left.Value & right);
}

INT operator^ (const INT_TYPE& left, const INT& right)
{
	return INT(left & right.Value);
}

INT operator| (const INT& left, const INT& right)
{
	return INT(left.Value | right.Value);
}

INT operator| (const INT& left, const INT_TYPE& right)
{
	return INT(left.Value | right);
}

INT operator| (const INT_TYPE& left, const INT& right)
{
	return INT(left | right.Value);
}

INT& operator&= (INT& left, const INT& right)
{
	left.Value &= right;
	return left;
}

INT& operator&= (INT& left, const INT_TYPE& right)
{
	left.Value &= right;
	return left;
}

INT_TYPE& operator&= (INT_TYPE& left, const INT& right)
{
	left &= right.Value;
	return left;
}

INT& operator^= (INT& left, const INT& right)
{
	left.Value ^= right.Value;
	return left;
}

INT& operator^= (INT& left, const INT_TYPE& right)
{
	left.Value ^= right;
	return left;
}

INT_TYPE& operator^= (INT_TYPE& left, const INT& right)
{
	left ^= right.Value;
	return left;
}

INT& operator|= (INT& left, const INT& right)
{
	left.Value |= right.Value;
	return left;
}

INT& operator|= (INT& left, const INT_TYPE& right)
{
	left.Value |= right;
	return left;
}

INT_TYPE& operator|= (INT_TYPE& left, const INT& right)
{
	left |= right.Value;
	return left;
}

INT operator+ (const INT& left, const INT& right)
{
	return INT(left.Value + right.Value);
}

INT operator+ (const INT& left, const INT_TYPE& right)
{
	return INT(left.Value + right);
}

INT operator+ (const INT_TYPE& left, const INT& right)
{
	return INT(left + right.Value);
}

INT& operator+= (INT& left, const INT& right)
{
	left.Value += right.Value;
	return left;
}

INT& operator+= (INT& left, const INT_TYPE& right)
{
	left.Value += right;
	return left;
}

INT_TYPE& operator+= (INT_TYPE& left, const INT& right)
{
	left += right.Value;
	return left;
}

INT operator- (const INT& left, const INT& right)
{
	return INT(left.Value - right.Value);
}

INT operator- (const INT& left, const INT_TYPE& right)
{
	return INT(left.Value - right);
}

INT operator- (const INT_TYPE& left, const INT& right)
{
	return INT(left - right.Value);
}

INT& operator-= (INT& left, const INT& right)
{
	left.Value -= right.Value;
	return left;
}

INT& operator-= (INT& left, const INT_TYPE& right)
{
	left.Value -= right;
	return left;
}

INT_TYPE& operator-= (INT_TYPE& left, const INT& right)
{
	left -= right.Value;
	return left;
}

INT operator* (const INT& left, const INT& right)
{
	return INT(left.Value * right.Value);
}

INT operator* (const INT& left, const INT_TYPE& right)
{
	return INT(left.Value * right);
}

INT operator* (const INT_TYPE& left, const INT& right)
{
	return INT(left * right.Value);
}

INT& operator*= (INT& left, const INT& right)
{
	left.Value *= right.Value;
	return left;
}

INT& operator*= (INT& left, const INT_TYPE& right)
{
	left.Value *= right;
	return left;
}

INT_TYPE& operator*= (INT_TYPE& left, const INT& right)
{
	left *= right.Value;
	return left;
}

INT operator/ (const INT& left, const INT& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), left);
		return left;
	}
#endif

	return (right.Value != 0) ? INT(left.Value / right.Value) : left;
}

INT operator/ (const INT& left, const INT_TYPE& right)
{
#ifdef DEBUG_MODE
	if (right == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), left);
		return left;
	}
#endif

	return (right != 0) ? INT(left.Value / right) : left;
}

INT operator/ (const INT_TYPE& left, const INT& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), INT(left));
		return INT(left);
	}
#endif

	return (right.Value != 0) ? INT(left / right.Value) : INT(left);
}

INT& operator/= (INT& left, const INT& right)
{
	if (right.Value != 0)
	{
		left.Value /= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), left);
	}
#endif
		
	return left;
}

INT& operator/= (INT& left, const INT_TYPE& right)
{
	if (right != 0)
	{
		left.Value /= right;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), left);
	}
#endif
		
	return left;
}

INT_TYPE& operator/= (INT_TYPE& left, const INT& right)
{
	if (right.Value != 0)
	{
		left /= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), INT(left));
	}
#endif
		
	return left;
}

INT operator% (const INT& left, const INT& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
		return left;
	}
#endif

	return (right.Value != 0) ? INT(left.Value % right.Value) : left;
}

INT operator% (const INT& left, const INT_TYPE& right)
{
#ifdef DEBUG_MODE
	if (right == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
		return left;
	}
#endif

	return (right != 0) ? INT(left.Value % right) : left;
}

INT operator% (const INT_TYPE& left, const INT& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), INT(left));
		return left;
	}
#endif

	return (right.Value != 0) ? INT(left % right.Value) : INT(left);
}

INT& operator%= (INT& left, const INT& right)
{
	if (right.Value != 0)
	{
		left.Value %= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
	}
#endif
		
	return left;
}

INT& operator%= (INT& left, const INT_TYPE& right)
{
	if (right != 0)
	{
		left.Value %= right;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
	}
#endif
		
	return left;
}

INT_TYPE& operator%= (INT_TYPE& left, const INT& right)
{
	if (right.Value != 0)
	{
		left %= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), INT(left));
	}
#endif
		
	return left;
}

#pragma endregion
SD_END