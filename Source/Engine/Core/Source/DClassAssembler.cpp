/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DClassAssembler.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
std::vector<DClassAssembler::SDClassInfo> DClassAssembler::PreloadedDClasses = PreloadedDClassesCopy;
std::vector<DClassAssembler::SDClassInfo> DClassAssembler::PreloadedDClassesCopy = PreloadedDClasses;
bool DClassAssembler::bLinkedDClasses = false;
std::vector<DClass*> DClassAssembler::RootClasses = RootClasses;

DClass* DClassAssembler::LoadClass (const DString& classNamespace, const DString& className, const DString& parentNamespace, const DString& parentClass)
{
	DClass* classInstance = new DClass(className);

	SDClassInfo newEntry;
	newEntry.ClassNamespace = classNamespace;
	newEntry.ClassInstance = classInstance;
	newEntry.ParentNamespace = parentNamespace;
	newEntry.ParentClassName = parentClass;
	PreloadedDClasses.push_back(newEntry);
	PreloadedDClassesCopy.push_back(newEntry);

	return classInstance;
}

bool DClassAssembler::AssembleDClasses ()
{
	CHECK_INFO(!IsInitialized(), TXT("The DClassAssembler already assembled its registered DClasses."))

	for (UINT_TYPE i = 0; i < PreloadedDClasses.size(); i++)
	{
		if (PreloadedDClasses.at(i).ParentClassName.IsEmpty())
		{
			AddRootClass(PreloadedDClasses.at(i).ClassInstance);
			continue; //root class, no need to find parent.
		}

		//Find parent
		for (UINT_TYPE parentIter = 0; parentIter < PreloadedDClasses.size(); parentIter++)
		{
			if (i == parentIter)
			{
				continue; //DClass cannot be parent to itself.
			}

			if (IsChildOf(PreloadedDClasses.at(i), PreloadedDClasses.at(parentIter)))
			{
				LinkDClasses(PreloadedDClasses.at(i), PreloadedDClasses.at(parentIter));
				break;
			}
		}
	}

	//Validate PreloadedDClasses to ensure that there aren't any broken links.
	for (UINT_TYPE i = 0; i < PreloadedDClasses.size(); i++)
	{
		if (!HasFoundParent(PreloadedDClasses.at(i)))
		{
			Engine::FindEngine()->FatalError(DString::CreateFormattedString(TXT("Failed to assemble class tree for %s.  The parent expected class %s within namespace %s is not found.  Comparisons are case sensitive."),
				PreloadedDClasses.at(i).ClassInstance->GetDuneClassName(), PreloadedDClasses.at(i).ParentClassName, PreloadedDClasses.at(i).ParentNamespace));
			return false;
		}
	}

	//No need to Preserve the PreloadedDClasses vector.  Free up memory.
	ContainerUtils::Empty(PreloadedDClasses);
	ContainerUtils::Empty(PreloadedDClassesCopy);
	bLinkedDClasses = true;

	return true;
}

bool DClassAssembler::IsInitialized ()
{
	return bLinkedDClasses;
}

const std::vector<DClass*> DClassAssembler::GetRootClasses ()
{
	return RootClasses;
}

bool DClassAssembler::HasFoundParent (const SDClassInfo& registeredDClass)
{
	if (registeredDClass.ParentClassName.IsEmpty())
	{
		return true; //Root class
	}

	return (registeredDClass.ClassInstance->GetSuperClass() != nullptr);
}

bool DClassAssembler::IsChildOf (const SDClassInfo& childInfo, const SDClassInfo& parentInfo)
{
	if (childInfo.ParentClassName.IsEmpty())
	{
		return false;
	}

	if (childInfo.ParentClassName.Compare(parentInfo.ClassInstance->GetDuneClassName(), DString::CC_CaseSensitive) != 0)
	{
		return false;
	}

	return (childInfo.ParentNamespace.Compare(parentInfo.ClassNamespace, DString::CC_CaseSensitive) == 0);
}

void DClassAssembler::LinkDClasses (SDClassInfo& childInfo, SDClassInfo& parentInfo)
{
	parentInfo.ClassInstance->AddChild(childInfo.ClassInstance);
	childInfo.ClassInstance->ParentClass = parentInfo.ClassInstance;
}

void DClassAssembler::AddRootClass (DClass* newRootClass)
{
	UINT_TYPE index;

	//Added alphabetically for organizational purposes
	for (index = 0; index < RootClasses.size(); index++)
	{
		if (RootClasses.at(index)->GetDuneClassName().Compare(newRootClass->GetDuneClassName(), DString::CC_CaseSensitive) > 0)
		{
			break;
		}
	}

	RootClasses.insert(RootClasses.begin() + index, newRootClass);
}

SD_END