/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Vector2.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
const Vector2 Vector2::ZeroVector(0.0f, 0.0f);

Vector2::Vector2 ()
{

}

Vector2::Vector2 (const float x, const float y)
{
	X = x;
	Y = y;
}

Vector2::Vector2 (const FLOAT x, const FLOAT y)
{
	X = x;
	Y = y;
}

Vector2::Vector2 (const Vector2& copyVector)
{
	X = copyVector.X;
	Y = copyVector.Y;
}

void Vector2::operator= (const Vector2& copyVector)
{
	X = copyVector.X;
	Y = copyVector.Y;
}

FLOAT Vector2::operator[] (UINT_TYPE axisIdx)
{
	return GetAxis(axisIdx);
}

const FLOAT Vector2::operator[] (UINT_TYPE axisIdx) const
{
	return GetAxis(axisIdx);
}

DString Vector2::ToString () const
{
	return (TXT("(") + X.ToString() + TXT(",") + Y.ToString() + TXT(")"));
}

void Vector2::Serialize (DataBuffer& dataBuffer) const
{
	dataBuffer << X;
	dataBuffer << Y;
}

void Vector2::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> X;
	dataBuffer >> Y;
}

Vector2 Vector2::SFMLtoSD (const sf::Vector2f sfVector)
{
	return Vector2(sfVector.x, sfVector.y);
}

sf::Vector2f Vector2::SDtoSFML (const Vector2 sdVector)
{
	return sf::Vector2f(sdVector.X.Value, sdVector.Y.Value);
}

bool Vector2::IsNearlyEqual (const Vector2& compareTo, FLOAT tolerance) const
{
	return (FLOAT::Abs(X - compareTo.X) <= tolerance && FLOAT::Abs(Y - compareTo.Y) <= tolerance);
}

FLOAT Vector2::VSize () const
{
	return (sqrt(pow(X.Value, 2) + pow(Y.Value, 2)));
}

void Vector2::SetLengthTo (FLOAT targetLength)
{
	Normalize();

	X *= targetLength;
	Y *= targetLength;
}

Vector2 Vector2::Min (Vector2 input, const Vector2& min)
{
	input.MinInline(min);
	return input;
}

void Vector2::MinInline (const Vector2& min)
{
	X = Utils::Min(X, min.X);
	Y = Utils::Min(Y, min.Y);
}

Vector2 Vector2::Max (Vector2 input, const Vector2& max)
{
	input.MaxInline(max);
	return input;
}

void Vector2::MaxInline (const Vector2& max)
{
	X = Utils::Max(X, max.X);
	Y = Utils::Max(Y, max.Y);
}

Vector2 Vector2::Clamp (Vector2 input, const Vector2& min, const Vector2& max)
{
	input.ClampInline(min, max);
	return input;
}

void Vector2::ClampInline (const Vector2& min, const Vector2& max)
{
	X = Utils::Clamp(X, min.X, max.X);
	Y = Utils::Clamp(Y, min.Y, max.Y);
}

FLOAT Vector2::Dot (const Vector2& otherVector) const
{
	return ((X * otherVector.X) + (Y * otherVector.Y)).Value;
}

void Vector2::Normalize ()
{
	FLOAT magnitude = VSize();

	if (magnitude == 0)
	{
#ifdef DEBUG_MODE
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to normalize a zero 2D vector!"));
#endif
		return;
	}

	X /= magnitude;
	Y /= magnitude;
}

bool Vector2::IsUniformed () const
{
	return (X == Y);
}

bool Vector2::IsEmpty () const
{
	return (X == 0.f && Y == 0.f);
}

Vector3 Vector2::ToVector3 () const
{
	return Vector3(X, Y, 0);
}

FLOAT Vector2::GetAxis (UINT_TYPE vectorComponent) const
{
	CHECK(vectorComponent <= 1)

	switch (vectorComponent)
	{
		case(0): return X;
		case(1): return Y;
	}

	return 0.f;
}

#pragma region "External Operators"
bool operator== (const Vector2& left, const Vector2& right)
{
	return (left.X == right.X && left.Y == right.Y);
}

bool operator!= (const Vector2& left, const Vector2& right)
{
	return !(left == right);
}

Vector2 operator+ (const Vector2& left, const Vector2& right)
{
	return Vector2(left.X + right.X, left.Y + right.Y);	
}

Vector2 operator+ (const Vector2& left, const sf::Vector2f& right)
{
	return Vector2(left.X + right.x, left.Y + right.y);
}

sf::Vector2f operator+ (const sf::Vector2f& left, const Vector2& right)
{
	return sf::Vector2f(left.x + right.X.Value, left.y + right.Y.Value);
}

Vector2& operator+= (Vector2& left, const Vector2& right)
{
	left.X += right.X;
	left.Y += right.Y;
	return left;
}

Vector2& operator+= (Vector2& left, const sf::Vector2f& right)
{
	left.X += right.x;
	left.Y += right.y;
	return left;
}

sf::Vector2f& operator+= (sf::Vector2f& left, const Vector2& right)
{
	left.x += right.X.Value;
	left.y += right.Y.Value;
	return left;
}

Vector2 operator- (const Vector2& left, const Vector2& right)
{
	return Vector2(left.X - right.X, left.Y - right.Y);
}

Vector2 operator- (const Vector2& left, const sf::Vector2f& right)
{
	return Vector2(left.X - right.x, left.Y - right.y);
}

sf::Vector2f operator- (const sf::Vector2f& left, const Vector2& right)
{
	return sf::Vector2f(left.x - right.X.Value, left.y - right.Y.Value);
}

Vector2& operator-= (Vector2& left, const Vector2& right)
{
	left.X -= right.X;
	left.Y -= right.Y;
	return left;
}

Vector2& operator-= (Vector2& left, const sf::Vector2f& right)
{
	left.X -= right.x;
	left.Y -= right.y;
	return left;
}

sf::Vector2f& operator-= (sf::Vector2f& left, const Vector2& right)
{
	left.x -= right.X.Value;
	left.y -= right.Y.Value;
	return left;
}

Vector2 operator* (const Vector2& left, const Vector2& right)
{
	return Vector2(left.X * right.X, left.Y * right.Y);
}

Vector2 operator* (const Vector2& left, const sf::Vector2f& right)
{
	return Vector2(left.X * right.x, left.Y * right.y);
}

sf::Vector2f operator* (const sf::Vector2f& left, const Vector2& right)
{
	return sf::Vector2f(left.x * right.X.Value, left.y * right.Y.Value);
}

Vector2& operator*= (Vector2& left, const Vector2& right)
{
	left.X *= right.X;
	left.Y *= right.Y;
	return left;
}

Vector2& operator*= (Vector2& left, const sf::Vector2f& right)
{
	left.X *= right.x;
	left.Y *= right.y;
	return left;
}

sf::Vector2f& operator*= (sf::Vector2f& left, const Vector2& right)
{
	left.x *= right.X.Value;
	left.y *= right.Y.Value;
	return left;
}

Vector2 operator/ (const Vector2& left, const Vector2& right)
{
	Vector2 result(left);
	result /= right;
	return result;
}

Vector2 operator/ (const Vector2& left, const sf::Vector2f& right)
{
	Vector2 result(left);
	result /= right;
	return result;
}

sf::Vector2f operator/ (const sf::Vector2f& left, const Vector2& right)
{
	sf::Vector2f result(left);
	result /= right;
	return left;
}

Vector2& operator/= (Vector2& left, const Vector2& right)
{
	if (right.X != 0.f)
	{
		left.X /= right.X;
	}

	if (right.Y != 0.f)
	{
		left.Y /= right.Y;
	}
		
	return left;
}

Vector2& operator/= (Vector2& left, const sf::Vector2f& right)
{
	if (right.x != 0.f)
	{
		left.X /= right.x;
	}

	if (right.y != 0.f)
	{
		left.Y /= right.y;
	}

	return left;
}

sf::Vector2f& operator/= (sf::Vector2f& left, const Vector2& right)
{
	if (right.X != 0.f)
	{
		left.x /= right.X.Value;
	}

	if (right.Y != 0.f)
	{
		left.y /= right.Y.Value;
	}

	return left;
}
#pragma endregion
SD_END