/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Rotator.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
const FLOAT Rotator::DEGREE_TO_RADIAN = 0.017453293f; // pi/180
const FLOAT Rotator::RADIAN_TO_DEGREE = 57.29577951f; // 180/pi
const Rotator Rotator::ZERO_ROTATOR = Rotator(0.f, 0.f, 0.f, RU_Degrees);
const unsigned int Rotator::FULL_REVOLUTION = 65536;
const unsigned int Rotator::HALF_REVOLUTION = 32768;
const unsigned int Rotator::QUARTER_REVOLUTION = 16384;

Rotator::Rotator ()
{
	//Variables are left uninitialized.
}

Rotator::Rotator (const Rotator& copyRotator) :
	Yaw(copyRotator.Yaw),
	Pitch(copyRotator.Pitch),
	Roll(copyRotator.Roll)
{
	//Noop
}

Rotator::Rotator (unsigned int inYaw, unsigned int inPitch, unsigned int inRoll) :
	Yaw(inYaw),
	Pitch(inPitch),
	Roll(inRoll)
{
	//Noop
}

Rotator::Rotator (FLOAT inYaw, FLOAT inPitch, FLOAT inRoll, ERotationUnit rotationUnit)
{
	SetRotation(inYaw, inPitch, inRoll, rotationUnit);
}

Rotator::Rotator (Vector3 directionVector)
{
	directionVector.Normalize();
	SetComponentsFromVector(directionVector);
}

Rotator::Rotator (const TransformMatrix& transformMatrix)
{
	*this = transformMatrix.GetRotation();
}

void Rotator::operator= (const Rotator& copyRotator)
{
	Yaw = copyRotator.Yaw;
	Pitch = copyRotator.Pitch;
	Roll = copyRotator.Roll;
}

void Rotator::operator= (Vector3 directionVector)
{
	directionVector.Normalize();
	SetComponentsFromVector(directionVector);
}
		
DString Rotator::ToString () const
{
	return DString::CreateFormattedString(TXT("(%s, %s, %s)"), GetYaw(RU_Degrees), GetPitch(RU_Degrees), GetRoll(RU_Degrees));
}

void Rotator::Serialize (DataBuffer& dataBuffer) const
{
	INT serializedYaw(Yaw);
	INT serializedPitch(Pitch);
	INT serializedRoll(Roll);

	dataBuffer << serializedYaw;
	dataBuffer << serializedPitch;
	dataBuffer << serializedRoll;
}

void Rotator::Deserialize (const DataBuffer& dataBuffer)
{
	INT serializedYaw;
	INT serializedPitch;
	INT serializedRoll;

	dataBuffer >> serializedYaw;
	dataBuffer >> serializedPitch;
	dataBuffer >> serializedRoll;

	//Yaw, Pitch, Roll number of bytes are shorter than INTs.  Check for number overflow.
	CHECK(serializedYaw < FULL_REVOLUTION && serializedPitch < FULL_REVOLUTION && serializedRoll < FULL_REVOLUTION)

	Yaw = serializedYaw.Value;
	Pitch = serializedPitch.Value;
	Roll = serializedRoll.Value;
}

Rotator Rotator::MakeRotator (INT inYaw, INT inPitch, INT inRoll)
{
	INT revolution(FULL_REVOLUTION);
	inYaw %= revolution;
	inPitch %= revolution;
	inRoll %= revolution;

	//Make sure the numbers are in the positive range
	if (inYaw < 0)
	{
		inYaw += revolution;
	}

	if (inPitch < 0)
	{
		inPitch += revolution;
	}

	if (inRoll < 0)
	{
		inRoll += revolution;
	}

	return Rotator(inYaw.ToUnsignedInt32(), inPitch.ToUnsignedInt32(), inRoll.ToUnsignedInt32());
}

void Rotator::SetRotation (FLOAT inYaw, FLOAT inPitch, FLOAT inRoll, ERotationUnit rotationUnit)
{
	FLOAT multiplier = GetRotatorToUnitConverter(rotationUnit);

	//Convert from user specified units to Rotator units (0-65k)
	inYaw *= multiplier;
	inPitch *= multiplier;
	inRoll *= multiplier;

	Yaw = static_cast<unsigned int>(INT(inYaw).Value);
	Pitch = static_cast<unsigned int>(INT(inPitch).Value);
	Roll = static_cast<unsigned int>(INT(inRoll).Value);
}

void Rotator::SetYaw (FLOAT inYaw, ERotationUnit rotationUnit)
{
	inYaw *= GetRotatorToUnitConverter(rotationUnit);
	Yaw = static_cast<unsigned int>(INT(inYaw).Value);
}

void Rotator::SetPitch (FLOAT inPitch, ERotationUnit rotationUnit)
{
	inPitch *= GetRotatorToUnitConverter(rotationUnit);
	Pitch = static_cast<unsigned int>(INT(inPitch).Value);
}

void Rotator::SetRoll (FLOAT inRoll, ERotationUnit rotationUnit)
{
	inRoll *= GetRotatorToUnitConverter(rotationUnit);
	Roll = static_cast<unsigned int>(INT(inRoll).Value);
}

bool Rotator::IsPerpendicularTo (const Rotator& otherRotator) const
{
	Vector3 a = GetDirectionalVector();
	Vector3 b = otherRotator.GetDirectionalVector();

	CHECK(a.VSize() == 1.f && b.VSize() == 1.f)
	return (a.Dot(b) == 0.f);
}

bool Rotator::IsParallelTo (const Rotator& otherRotator) const
{
	Vector3 a = GetDirectionalVector();
	Vector3 b = otherRotator.GetDirectionalVector();

	return (a.CrossProduct(b).VSize() == 0.f);
}

Vector3 Rotator::GetDirectionalVector () const
{
	FLOAT radianYaw;
	FLOAT radianPitch;
	FLOAT radianRoll;
	GetRadians(OUT radianYaw, OUT radianPitch, OUT radianRoll);

	Vector3 result;
	result.X = cos(radianYaw.Value) * cos(radianPitch.Value);
	result.Y = -sin(radianYaw.Value) * cos(radianPitch.Value);
	result.Z = sin(radianPitch.Value);

	CHECK(result.VSize() == 1.f)

	return result;
}

void Rotator::GetDegrees (FLOAT& outYaw, FLOAT& outPitch, FLOAT& outRoll) const
{
	FLOAT multiplier = GetUnitToRotatorConverter(RU_Degrees);

	outYaw = FLOAT::MakeFloat(Yaw) * multiplier;
	outPitch = FLOAT::MakeFloat(Pitch) * multiplier;
	outRoll = FLOAT::MakeFloat(Roll) * multiplier;
}

void Rotator::GetRadians (FLOAT& outYaw, FLOAT& outPitch, FLOAT& outRoll) const
{
	FLOAT multiplier = GetUnitToRotatorConverter(RU_Radians);

	outYaw = FLOAT::MakeFloat(Yaw) * multiplier;
	outPitch = FLOAT::MakeFloat(Pitch) * multiplier;
	outRoll = FLOAT::MakeFloat(Roll) * multiplier;
}

FLOAT Rotator::GetYaw (ERotationUnit rotationUnit) const
{
	return FLOAT::MakeFloat(Yaw) * GetUnitToRotatorConverter(rotationUnit);
}

FLOAT Rotator::GetPitch (ERotationUnit rotationUnit) const
{
	return FLOAT::MakeFloat(Pitch) * GetUnitToRotatorConverter(rotationUnit);
}

FLOAT Rotator::GetRoll (ERotationUnit rotationUnit) const
{
	return FLOAT::MakeFloat(Roll) * GetUnitToRotatorConverter(rotationUnit);
}

FLOAT Rotator::GetRotatorToUnitConverter (ERotationUnit rotationUnit) const
{
	switch (rotationUnit)
	{
		case (RU_Degrees) : return (static_cast<float>(FULL_REVOLUTION) / 360.f);
		case (RU_Radians) : return (static_cast<float>(FULL_REVOLUTION) / (PI_FLOAT * 2.f));
	}

	return 1.f;
}

FLOAT Rotator::GetUnitToRotatorConverter (ERotationUnit rotationUnit) const
{
	switch (rotationUnit)
	{
		case (RU_Degrees) : return (360.f / static_cast<float>(FULL_REVOLUTION));
		case (RU_Radians) : return ((PI_FLOAT * 2.f) / static_cast<float>(FULL_REVOLUTION));
	}

	return 1.f;
}

void Rotator::SetComponentsFromVector (const Vector3& normalizedVector)
{
	Roll = 0; //Roll is always zero when pulling from a directional vector.

	if (normalizedVector.IsEmpty())
	{
		Yaw = 0;
		Pitch = 0;
		return;
	}

	CHECK(normalizedVector.VSize() == 1.f)

	//Handle edge cases where the ratio would end up on an asymptote
	if (normalizedVector.X == 0.f)
	{
		//Handle case where the vector is pointing straight up or down.
		if (normalizedVector.Y == 0.f)
		{
			Yaw = 0;
			//Either +90 degrees or -90 degrees. Using left handed cordinate system, it must rotate clockwise about the Y-axis.
			Pitch = (normalizedVector.Z > 0.f) ? QUARTER_REVOLUTION : QUARTER_REVOLUTION * 3;
			return;
		}

		//Yaw is either going to be 90 or -90 depending on which direction the vector faces along the YZ-plane.
		Yaw = (normalizedVector.Y < 0.f) ? QUARTER_REVOLUTION : QUARTER_REVOLUTION * 3;
	}
	else
	{
		float yawRadian = atan2f(-normalizedVector.Y.Value, normalizedVector.X.Value);
		Yaw = INT(FLOAT(yawRadian) * GetRotatorToUnitConverter(RU_Radians)).Value;
	}

	float pitchRadian = asin(normalizedVector.Z.Value);

	Pitch = INT(FLOAT(pitchRadian) * GetRotatorToUnitConverter(RU_Radians)).Value;
}

#pragma region "External Operators"
bool operator== (const Rotator& left, const Rotator& right)
{
	return (left.Yaw == right.Yaw &&
		left.Pitch == right.Pitch &&
		left.Roll == right.Roll);
}

bool operator!= (const Rotator& left, const Rotator& right)
{
	return !(left == right);
}

Rotator operator+ (const Rotator& left, const Rotator& right)
{
	return Rotator(left.Yaw + right.Yaw, left.Pitch + right.Pitch, left.Roll + right.Roll);
}

Rotator& operator+= (Rotator& left, const Rotator& right)
{
	left.Yaw += right.Yaw;
	left.Pitch += right.Pitch;
	left.Roll += right.Roll;

	return left;
}

Rotator operator- (const Rotator& left, const Rotator& right)
{
	return Rotator(left.Yaw - right.Yaw, left.Pitch - right.Pitch, left.Roll - right.Roll);
}

Rotator& operator-= (Rotator& left, const Rotator& right)
{
	left.Yaw -= right.Yaw;
	left.Pitch -= right.Pitch;
	left.Roll -= right.Roll;

	return left;
}
#pragma endregion
SD_END