/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ComponentIterator.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
ComponentIterator::ComponentIterator ()
{
	bRecursive = true;
}

ComponentIterator::ComponentIterator (const Entity* inBaseEntity, bool bInRecursive)
{
	bRecursive = bInRecursive;
	if (inBaseEntity->Components.size() > 0)
	{
		ComponentChain.push_back(SComponentIndex(inBaseEntity));
	}
}

ComponentIterator::~ComponentIterator ()
{
	//Noop
}

void ComponentIterator::operator++ ()
{
	FindNextComponent();
}
void ComponentIterator::operator++ (int)
{
	FindNextComponent();
}

void ComponentIterator::SetBaseEntity (const Entity* newBaseEntity)
{
	ContainerUtils::Empty(ComponentChain);
	if (newBaseEntity->Components.size() > 0)
	{
		ComponentChain.push_back(SComponentIndex(newBaseEntity));
	}
}

const Entity* ComponentIterator::GetBaseEntity () const
{
	if (ComponentChain.size() > 0)
	{
		return ComponentChain.at(0).TargetEntity;
	}

	return nullptr;
}

EntityComponent* ComponentIterator::GetSelectedComponent () const
{
	const SComponentIndex* last = (ComponentChain.size() > 0) ? &ComponentChain.at(ComponentChain.size() - 1) : nullptr;
	if (last != nullptr)
	{
		CHECK(last->ComponentIdx < last->TargetEntity->Components.size())
		return last->TargetEntity->Components.at(last->ComponentIdx).Get();
	}

	return nullptr;
}

void ComponentIterator::FindNextComponent ()
{
	if (ComponentChain.size() <= 0)
	{
		return; //iterator already ended
	}

	SComponentIndex* last = &ComponentChain.at(ComponentChain.size() - 1);
	if (bRecursive && last->TargetEntity->Components.at(last->ComponentIdx)->Components.size() > 0) //Step into sub components
	{
		//The subcomponent has its own sub components.  Add to component chain to iterate through sub sub components.
		ComponentChain.push_back(SComponentIndex(last->TargetEntity->Components.at(last->ComponentIdx).Get()));
		return;
	}

	while (last != nullptr)
	{
		if (++last->ComponentIdx < last->TargetEntity->Components.size()) //Jump to sibling component
		{
			break;
		}

		//The component index is beyond the component list.  Pop the stack and increment the parent index
		ComponentChain.pop_back();

		//Jump to parent Entity and repeat the loop to increment its component index
		last = (ComponentChain.size() > 0) ? &ComponentChain.at(ComponentChain.size() - 1) : nullptr;
	}
}
SD_END