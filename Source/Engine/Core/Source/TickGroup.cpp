/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TickGroup.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
TickGroup::TickGroup (const DString& inGroupName, INT inTickPriority)
{
	GroupName = inGroupName;
	TickPriority = inTickPriority;
	bTicking = true;
	IsAccessingTickLoop = false;
}

TickGroup::~TickGroup ()
{
	//Noop
}

void TickGroup::RegisterTick (TickComponent* newTick)
{
#ifdef DEBUG_MODE
	//Do a quick sanity check, and give a warning if newTick was already added.
	for (TickComponent* registeredTick : Ticks)
	{
		if (registeredTick == newTick)
		{
			//Ensure this isn't in PendingRemovalTicks list.  This may happen if a Tick was removed and added again within a tick loop.
			//In this case there's no need for warning since the duplicated entry will be removed whenever the TickGroup finishes this iteration.
			UINT_TYPE tickIdx = ContainerUtils::FindInVector(PendingRemovalTicks, newTick);
			if (tickIdx == INDEX_NONE)
			{
				CoreLog.Log(LogCategory::ELogLevel::LL_Warning, TXT("A TickComponent should register to a TickGroup at most once.  This warning only appears in debug mode."));
			}

			break;
		}
	}
#endif

	Ticks.push_back(newTick);
}

void TickGroup::RemoveTick (TickComponent* targetTick)
{
	if (IsAccessingTickLoop)
	{
		PendingRemovalTicks.push_back(targetTick);
		return;
	}

	for (UINT_TYPE i = 0; i < Ticks.size(); ++i)
	{
		if (Ticks.at(i) == targetTick)
		{
			Ticks.erase(Ticks.begin() + i);
			return;
		}
	}
}

void TickGroup::SetTicking (bool bNewTicking)
{
	bTicking = bNewTicking;
}

void TickGroup::TickRegisteredComponents (FLOAT deltaSec)
{
	if (IsTicking())
	{
		IsAccessingTickLoop = true;

		for (UINT_TYPE i = 0; i < Ticks.size(); ++i)
		{
			Ticks.at(i)->Tick(deltaSec);
		}

		IsAccessingTickLoop = false;
		for (TickComponent* tick : PendingRemovalTicks)
		{
			RemoveTick(tick);
		}
		ContainerUtils::Empty(PendingRemovalTicks);
	}
}

const std::vector<TickComponent*>& TickGroup::ReadTickComponents () const
{
	return Ticks;
}
SD_END