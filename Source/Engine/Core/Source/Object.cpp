/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Object.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
IMPLEMENT_CLASS_NO_PARENT(SD, Object)

Object::Object ()
{
	FirstProperty = nullptr;

	PointerReferences = nullptr;
#ifdef DEBUG_MODE
	bDestroyedIsCalled = false;
#endif
}

Object::~Object ()
{
	if (FirstProperty != nullptr)
	{
		delete FirstProperty;
		FirstProperty = nullptr;
	}
}

bool Object::CanBePointed () const
{
	//Don't point at objects that are about to be deleted since DPointers are only cleared upon Destroy.
	return !bPendingDelete;
}

void Object::SetLeadingDPointer (DPointerBase* newLeadingPointer) const
{
	PointerReferences = newLeadingPointer;
}

DPointerBase* Object::GetLeadingDPointer () const
{
	return PointerReferences;
}

DString Object::ToString () const
{
	return GetFriendlyName();
}

void Object::InitProps ()
{
#ifdef DEBUG_MODE
	DebugName = TXT("");
#endif
	bPendingDelete = false;
	ObjectHash = -1;

	NextObject = nullptr;
}

void Object::BeginObject ()
{
	ObjectHash = CalculateHashID();
	RegisterObject();
}

void Object::Destroy ()
{
	if (CanBeDestroyed())
	{
		Destroyed();

#ifdef DEBUG_MODE
		if (!bDestroyedIsCalled)
		{
			CoreLog.Log(LogCategory::LL_Fatal, TXT("The %s Destroyed function chain failed to make it back to the root call (Object::Destroyed).  All classes are required to invoke the parent's Destroyed function so that the garbage collector may reclaim memory."), ToString());
		}
#endif
	}
}

bool Object::IsDefaultObject () const
{
	return (GetDefaultObject() == this);
}

DString Object::GetName () const
{
	return StaticClass()->GetDuneClassName();
}

DString Object::GetFriendlyName () const
{
#ifdef DEBUG_MODE
	if (!DebugName.IsEmpty())
	{
		return DebugName;
	}
#endif
	
	if (IsDefaultObject())
	{
		return TXT("Default_") + StaticClass()->ToString();
	}

	return StaticClass()->ToString();
}

const Object* Object::GetDefaultObject () const
{
	return StaticClass()->GetDefaultObject();
}

bool Object::GetPendingDelete () const
{
	return bPendingDelete;
}

unsigned int Object::CalculateHashID () const
{
	return Engine::GetEngine(Engine::MAIN_ENGINE_IDX)->GetObjectHashNumber();
}

void Object::PostEngineInitialize () const
{
	//Do nothing
}

void Object::InitializeObject ()
{
	InitProps();

	if (!IsDefaultObject())
	{
		BeginObject();
	}
}

bool Object::CanBeDestroyed () const
{
	return !bPendingDelete;
}

void Object::Destroyed ()
{
	CHECK_INFO(!bPendingDelete, DString::CreateFormattedString(TXT("Cannot destroy object more than once.  %s is already marked for deletion."), ToString()))
	bPendingDelete = true;

#ifdef DEBUG_MODE
	bDestroyedIsCalled = true;
#endif

	//All pointers that references this object should be set to nullptrs to prevent them from dangling.
	for (DPointerBase* curPointer = PointerReferences; curPointer != nullptr; )
	{
		DPointerBase* nextPointer = curPointer->GetNextPointer();
		curPointer->ClearPointer();
		curPointer = nextPointer;
	}
}

void Object::RegisterObject ()
{
	//Make sure this object resides in a thread that contains an Engine
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	//Add self to Engine's object list
	localEngine->RegisterObject(this);
}
SD_END