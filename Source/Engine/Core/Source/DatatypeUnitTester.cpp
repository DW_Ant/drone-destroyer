/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DatatypeUnitTester.cpp
=====================================================================
*/

#include "CoreClasses.h"
#include "utf8.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, DatatypeUnitTester, SD, UnitTester)

bool DatatypeUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		return (TestDString(testFlags) && TestDataBuffer(testFlags) && TestStringIterator(testFlags) && TestCharacterEncoding(testFlags) && TestBOOL(testFlags) && TestINT(testFlags) && TestFLOAT(testFlags) &&
			TestDPointer(testFlags) && TestRange(testFlags) && TestVector2(testFlags) && TestVector3(testFlags) && TestRotator(testFlags) && TestRectangle(testFlags) && TestSDFunction(testFlags) &&
			TestMatrix(testFlags));
	}
		
	return true;
}

bool DatatypeUnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
{
	//This test should run before any other SD test.
	return true;
}

//It's important to run the DString test first since all of the other tests depends on DStrings to work (for logging).
bool DatatypeUnitTester::TestDString (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("DString"));

	DString blankString;
	DString initString(TXT("Initialized string"));
	DString copiedString(initString);

	//Need to test formatted string early since the logs depend on that functionality to work
	DString formattedString = DString::CreateFormattedString(TXT("Formatted string (%s)"), copiedString);
	DString superFormattedString = DString::CreateFormattedString(TXT("Super Formatted string (%s), (%s), (%s), (%s)"), DString(TXT("CStringA")), DString(TXT("CStringB")), DString(TXT("CStringC")), DString(TXT("CStringD")));

	DString formatParam1 = TXT("Some Value");
	DString formatParam2 = TXT("Insert a string with '%s' macro within");
	DString formatParam3 = TXT("");
	DString formatParam4 = TXT("(Final Value with number 7)");
	DString otherFormatString = DString::CreateFormattedString(TXT("Testing other Format String function with %s where we %s another string.  Empty String=\"%s\". %s.  -=End=-"), formatParam1, formatParam2, formatParam3, formatParam4);
		
	TestLog(testFlags, TXT("Constructor tests.  Note that these logs may appear incorrectly since the FormatString and ToCString was not yet tested."));
	TestLog(testFlags, TXT("    blankString=%s"), blankString);
	TestLog(testFlags, TXT("    initString=%s"), initString);
	TestLog(testFlags, TXT("    copiedString=%s"), copiedString);
	TestLog(testFlags, TXT("    formattedString=%s"), formattedString);
	TestLog(testFlags, TXT("    superFormattedString=%s"), superFormattedString);
	TestLog(testFlags, TXT("    otherFormatString=%s"), otherFormatString);

	SetTestCategory(testFlags, TXT("Comparison"));
	if (initString != TXT("Initialized string"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The initialized string should have been equal to \"Initialized string\".  Instead it's \"%s\""), {initString});
		TestLog(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
		return false;
	}

	if (formattedString != TXT("Formatted string (Initialized string)"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The formattedString should have been equal to \"Formatted string (Initialized string)\".  Instead it's \"%s\""), {formattedString});
		TestLog(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
		return false;
	}

	DString superFormatResult = TXT("Super Formatted string (CStringA), (CStringB), (CStringC), (CStringD)");
	if (superFormattedString != superFormatResult)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The superFormattedString should have been equal to \"%s\".  Instead it's \"%s\""), {superFormatResult, superFormattedString});
		TestLog(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
		return false;
	}

	DString otherFormatStringResult = TXT("Testing other Format String function with Some Value where we Insert a string with '%s' macro within another string.  Empty String=\"\". (Final Value with number 7).  -=End=-");
	if (otherFormatString != otherFormatStringResult)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The otherFormatString should have been equal to \"%s\".  Instead it's \"%s\""), {otherFormatStringResult, otherFormatString});
		TestLog(testFlags, TXT("Note:  The logs may be displayed incorrectly since the FormatString and ToCString was not yet tested."));
		return false;
	}

	//compare CStrings
	if (strcmp(initString.ToCString(), TXT("Initialized string")) != 0)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The CString of initialized string should have been equal to \"Initialized string\".  Instead it's \"%s\""), {initString});
		TestLog(testFlags, TXT("Note:  The logs may be displayed incorrectly since ToCString was not yet tested."));
		return false;
	}

	DString compareString = initString;
	compareString.ToLower();
	if (compareString != TXT("initialized string"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The lower case of initialized string should have been equal to \"initialized string\".  Instead it's \"%s\""), {compareString});
		return false;
	}

	compareString.ToUpper();
	if (compareString != TXT("INITIALIZED STRING"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The upper case of initialized string should have been equal to \"INITIALIZED STRING\".  Instead it's \"%s\""), {compareString});
		return false;
	}

	if (initString.Compare(compareString, DString::CC_IgnoreCase) != 0)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The case insensitive compare of \"%s\" and \"%s\" should have returned true."), {initString, compareString});
		return false;
	}

	if (!blankString.IsEmpty())
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The blank string should be considered empty.  Instead IsEmpty returned false.  Value of blankString is:  \"%s\""), {blankString});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("DString utilities"));
	DString main = TXT("main");
	TestLog(testFlags, TXT("Adding \" appended text\" to \"%s\""), main);
	main += TXT(" appended text");

	if (main != TXT("main appended text"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The resulting appended string should have been \"main appended text\".  Instead it's \"%s\""), {main});
		return false;
	}

	if (main.At(3) != TEXT('n'))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  Index 3 of \"%s\" should have been \'n\'.  Instead it returned:  \"%s\""), {main, DString(main.At(3))});
		return false;
	}

	TestLog(testFlags, TXT("Finding \"appended\" within \"%s\".  The subtext is found at index %s"), main, main.Find(TXT("appended"), 0, DString::CC_IgnoreCase));
	if (main.Find(TXT("appended"), 0, DString::CC_IgnoreCase) != 5)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The index at which \"appended\" appears within \"%s\" should have been 5.  Instead it returned:  \"%s\""), {main, main.Find(TXT("appended"), 0, DString::CC_IgnoreCase).ToString()});
		return false;
	}

	if (main.Find(TXT("APPENDED"), 0, DString::CC_CaseSensitive) >= 0)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The index at which \"APPENDED\" appears within \"%s\" (case sensitive) should have been negative.  Instead it returned:  %s"), {main, main.Find(TXT("APPENDED"), 0, DString::CC_CaseSensitive).ToString()});
		return false;
	}

	if (main.Find(TXT("APPENDED"), 0, DString::CC_IgnoreCase) != 5)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The index at which \"APPENDED\" appears within \"%s\" (case insensitive) should have been 5.  Instead it returned:  %s"), {main, main.Find(TXT("APPENDED"), 0, DString::CC_IgnoreCase).ToString()});
		return false;
	}

	if (main.Find(TXT("appended"), 7, DString::CC_IgnoreCase) >= 0)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  Searching for \"appended\" within \"%s\" starting from index 7 should have returned negative results.  Instead it returned:  %s"), {main, main.Find(TXT("appended"), 7, DString::CC_IgnoreCase).ToString()});
		return false;
	}

	DString closeMatchStr = TXT("NeedNEEDLENeedNeedle In HayStack");
	INT matchIdx = closeMatchStr.Find(TXT("NEEDLE"), 0, DString::CC_CaseSensitive, DString::SD_RightToLeft);
	if (matchIdx != 4)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  Searching for \"Needle\" within \"%s\" (case sensitive from the right) should have returned 4.  Instead it returned %s."), {closeMatchStr, matchIdx.ToString()});
		return false;
	}

	matchIdx = closeMatchStr.Find(TXT("NEEDLE"), 0, DString::CC_IgnoreCase, DString::SD_RightToLeft);
	if (matchIdx != 14)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  Searching for \"Needlen\" within \"%s\" (case insensitive from the right) should have returned 14.  Instead it returned %s."), {closeMatchStr, matchIdx.ToString()});
		return false;
	}

	DString wrappingTestStr = TXT("Some :string: value with colons.");
	BOOL bIsWrapped = wrappingTestStr.IsWrappedByChar(':', 2);
	TestLog(testFlags, TXT("Is char index 2 'm' wrapped by colons in str \"%s\"?  %s"), wrappingTestStr, bIsWrapped);
	if (bIsWrapped)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The character at index 2 'm' should not be wrapped in colons in string:  \"%s\""), {wrappingTestStr});
		return false;
	}

	bIsWrapped = wrappingTestStr.IsWrappedByChar(':', 7);
	TestLog(testFlags, TXT("Is char index 7 't' wrapped by colons in str \"%s\"?  %s"), wrappingTestStr, bIsWrapped);
	if (!bIsWrapped)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The character at index 7 't' should be wrapped in colons in string:  \"%s\""), {wrappingTestStr});
		return false;
	}

	bIsWrapped = wrappingTestStr.IsWrappedByChar(':', 25);
	TestLog(testFlags, TXT("Is char index 25 'c' wrapped by colons in str \"%s\"?  %s"), wrappingTestStr, bIsWrapped);
	if (bIsWrapped)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The character at index 25 'c' should not be wrapped in colons in string:  \"%s\""), {wrappingTestStr});
		return false;
	}

	bIsWrapped = wrappingTestStr.IsWrappedByChar(':', 12);
	TestLog(testFlags, TXT("Is char index 12 ':' wrapped by colons in str \"%s\"?  %s"), wrappingTestStr, bIsWrapped);
	if (bIsWrapped)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The character at index 12 ':' should not be wrapped in colons since the colons, themselves, define the wrapping boundaries."));
		return false;
	}

	main = TXT("Sand Dune project:  Transcendence");
	DString lastWord = main.SubString(20);
	TestLog(testFlags, TXT("Retrieving the last word from \"%s\" returns \"%s\""), main, lastWord);
	if (lastWord != TXT("Transcendence"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The last word of \"%s\" should have been \"Transcendence\".  Instead it returned \"%s\""), {main.ToString(), lastWord.ToString()});
		return false;
	}

	DString secondWord = main.SubString(5, 8);
	TestLog(testFlags, TXT("Retrieving the second word from \"%s\" returns \"%s\""), main, secondWord);
	if (secondWord != TXT("Dune"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The second word of \"%s\" should have been \"Dune\".  Instead it returned \"%s\""), {main, secondWord});
		return false;
	}

	DString duneProject = main.SubStringCount(5, 12);
	TestLog(testFlags, TXT("Retrieving \"Dune Project\" from \"%s\" using SubStringCount returns \"%s\""), main, duneProject);
	if (duneProject != TXT("Dune project"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The 2nd and 3rd word of \"%s\" should have been \"Dune project\".  Instead it returned \"%s\""), {main, duneProject});
		return false;
	}

	main = TXT("Segment1|Segment2");
	INT splitIndex = 8; //at '|' character
	TestLog(testFlags, TXT("Splitting \"%s\" at | character into two other strings."), main);
	DString firstSegment;
	DString secondSegment;
	main.SplitString(splitIndex, firstSegment, secondSegment);
	if (firstSegment != TXT("Segment1") || secondSegment != TXT("Segment2"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  Splitting \"%s\" at the | character (index %s) should have constructed strings:  \"Segment1\" and \"Segment2\".  Instead \"%s\" and \"%s\" was constructed."), {main, splitIndex.ToString(), firstSegment, secondSegment});
		return false;
	}

	//Testing ParseString
	{
		main = TXT("2,5,2,29,918,-192,29,38,61,,44,"); //Double comma will be testing empty strings.
		std::vector<DString> numbers;
		std::vector<DString> correctNumbers = {TXT("2"), TXT("5"), TXT("2"), TXT("29"), TXT("918"), TXT("-192"), TXT("29"), TXT("38"), TXT("61"), TXT(""), TXT("44"), TXT("")};
		TestLog(testFlags, TXT("Parsing \"%s\" into a vector of strings.  There are two tests.  First test does not remove empty entries, and the second test removes empty entries."), main);
		main.ParseString(TEXT(','), numbers, false);
		if (numbers.size() != correctNumbers.size())
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The total number of elements of the number vector constructed from parsing \"%s\" should have been %s.  Instead the vector size is %s"), {main, DString::MakeString(correctNumbers.size()), DString::MakeString(numbers.size())});
			return false;
		}

		//validate each entry
		for (UINT_TYPE i = 0; i < correctNumbers.size(); i++)
		{
			if (correctNumbers.at(i) != numbers.at(i))
			{
				UnitTestError(testFlags, TXT("DString tests failed.  The numbers vector at index %s does not match the expected value:  %s.  Instead it's %s"), {DString::MakeString(i), correctNumbers.at(i), numbers.at(i)});
				return false;
			}
		}

		//Remove the empty string from the expected list.
		ContainerUtils::RemoveItems(OUT correctNumbers, DString::EmptyString);
		main.ParseString(TEXT(','), numbers, true);
		if (numbers.size() != correctNumbers.size())
		{
			UnitTestError(testFlags, TXT("DString tests failed.  The total number of elements of the number vector constructed from parsing \"%s\" (and removing empty entries) should have been %s.  Instead the vector size is %s"), {main, DString::MakeString(correctNumbers.size()), DString::MakeString(numbers.size())});
			return false;
		}

		//validate each entry
		for (UINT_TYPE i = 0; i < correctNumbers.size(); i++)
		{
			if (correctNumbers.at(i) != numbers.at(i))
			{
				UnitTestError(testFlags, TXT("DString tests failed.  The numbers vector at index %s does not match the expected value:  %s.  Instead it's %s"), {DString::MakeString(i), correctNumbers.at(i), numbers.at(i)});
				return false;
			}
		}
	}

	main = TXT("Alpha _ Omega");
	DString edgeString = main.Left(5);
	TestLog(testFlags, TXT("The left part of \"%s\" is \"%s\""), main, edgeString);
	if (edgeString != TXT("Alpha"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The left part of \"%s\" should have returned \"Alpha\".  Instead it returned \"%s\"."), {main, edgeString});
		return false;
	}

	edgeString = main.Right(5);
	TestLog(testFlags, TXT("The right part of \"%s\" is \"%s\""), main, edgeString);
	if (edgeString != TXT("Omega"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The right part of \"%s\" should have returned \"Omega\".  Instead it returned \"%s\"."), {main, edgeString});
		return false;
	}

	TestLog(testFlags, TXT("The length of \"%s\" is %s"), main, main.Length());
	if (main.Length() != 13)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The length of \"%s\" should have returned 13. Instead it returned %s"), {main, main.Length().ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Inserting \" _ Beta\" after alpha in \"%s\""), main);
	main.Insert(5, TXT(" _ Beta"));
	if (main != TXT("Alpha _ Beta _ Omega"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The result of inserting \" _ Beta\" after \"Alpha\" within \"Alpha _ Omega\" should have resulted in \"Alpha _ Beta _ Omega\".  Instead it's:  \"%s\"."), {main});
		return false;
	}

	TestLog(testFlags, TXT("Removing \" _ Omega\" from \"%s\""), main);
	main.Remove(12, 8);
	if (main != TXT("Alpha _ Beta"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The result of removing \" _ Omega\" from \"Alpha _ Beta _ Omega\" should have resulted in \"Alpha _ Beta\".  Instead it's:  \"%s\"."), {main});
		return false;
	}

	TestLog(testFlags, TXT("Replacing spaces with \"[ ]\" in \"%s\""), main);
	main.ReplaceInline(TXT(" "), TXT("[ ]"), DString::CC_CaseSensitive);
	if (main != TXT("Alpha[ ]_[ ]Beta"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The result of replacing \" \" with \"[ ]\" should have resulted in \"Alpha[ ]_[ ]Beta\".  Instead it's \"%s\"."), {main});
		return false;
	}

	TestLog(testFlags, TXT("Replacing all a's with @'s in \"%s\" (case insensitive)."), main);
	main.ReplaceInline(TXT("a"), TXT("aAa"), DString::CC_IgnoreCase);
	if (main != TXT("aAalphaAa[ ]_[ ]BetaAa"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The result of replacing a's (ignore case) with \"aAa\" should have resulted in \"aAalphaAa[ ]_[ ]BetaAa\".  Instead it's \"%s\"."), {main});
		return false;
	}

	TestLog(testFlags, TXT("Replacing all lower-case a's with nothing in \"%s\""), main);
	main.ReplaceInline(TXT("a"), TXT(""), DString::CC_CaseSensitive);
	if (main != TXT("AlphA[ ]_[ ]BetA"))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The result of replacing all lower case a's (case sensitive) with nothing should have resulted in \"AlphA[ ]_[ ]BetA\".  Instead it's \"%s\"."), {main});
		return false;
	}

	DString searchIn = TXT("Needle in haystack.");
	DString replaceResults = DString::Replace(searchIn, TXT("Needle"), TXT("Pitchfork"), DString::CC_CaseSensitive);
	if (replaceResults != TXT("Pitchfork in haystack."))
	{
		UnitTestError(testFlags, TXT("DString tests failed. The result of replacing \"Needle\" with \"Pitchfork\" in \"%s\" should have resulted in \"Pitchfork in Haystack.\".  Instead it's \"%s\"."), {searchIn, replaceResults});
		return false;
	}

	TestLog(testFlags, TXT("Clearing \"%s\""), main);
	main.Clear();
	if (main != TXT(""))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  Clearing a string should have resulted in \"\".  Instead it's:  \"%s\"."), {main});
		return false;
	}

	TestLog(testFlags, TXT("Testing search functions for DString. . ."));
	DString searchString = TXT("Sand Dune");
	DString baseString = TXT("Sand Pit");
	INT mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive);
	TestLog(testFlags, TXT("The first mismatching index when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
	if (mismatchIdx != 5)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s within %s should be 5.  Instead it returned %s."), {searchString, baseString, mismatchIdx.ToString()});
		return false;
	}

	baseString = TXT("SaNd DuNe");
	mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive);
	TestLog(testFlags, TXT("The first mismatching index when searching %s (case sensitive) within %s is %s."), searchString, baseString, mismatchIdx);
	if (mismatchIdx != 2)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s (case sensitive) within %s should be 2.  Instead it returned %s."), {searchString, baseString, mismatchIdx.ToString()});
		return false;
	}

	mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_IgnoreCase);
	TestLog(testFlags, TXT("The first mismatching index (case insensitive) when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
	if (mismatchIdx != INT_INDEX_NONE)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index (case insensitive) when searching %s within %s should be " STRINGIFY(INT_INDEX_NONE) ".  Instead it returned %s."), {searchString, baseString, mismatchIdx.ToString()});
		return false;
	}

	searchString = TXT("Entity");
	baseString = TXT("EntityComponent");
	mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive);
	TestLog(testFlags, TXT("The first mismatching index when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
	if (mismatchIdx != 6)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s within %s should be 6.  Instead it returned %s."), {searchString, baseString, mismatchIdx.ToString()});
		return false;
	}

	searchString = TXT("Something ");
	baseString = TXT("Something Something Dark Side");
	mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive);
	TestLog(testFlags, TXT("The first mismatching index when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
	if (mismatchIdx != 20)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching index when searching %s within %s should be 20.  Instead it returned %s."), {searchString, baseString, mismatchIdx.ToString()});
		return false;
	}

	searchString = TXT(" ");
	baseString = TXT("Trailing Spaces  ");
	mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive, DString::SD_RightToLeft);
	TestLog(testFlags, TXT("The last mismatching index when searching \"%s\" within \"%s\" is %s."), searchString, baseString, mismatchIdx);
	if (mismatchIdx != 14)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The last mismatching index when searching \"%s\" within \"%s\" should be 14.  Instead it returned %s."), {searchString, baseString, mismatchIdx.ToString()});
		return false;
	}

	searchString = TXT("YO");
	baseString = TXT("Yoyo");
	mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_IgnoreCase);
	TestLog(testFlags, TXT("The first mismatching (case insensitive) index when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
	if (mismatchIdx != INT_INDEX_NONE)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The first mismatching (case insensitive) index when searching %s within %s should be negative.  Instead it returned %s."), {searchString, baseString, mismatchIdx.ToString()});
		return false;
	}

	searchString = TXT("na");
	baseString = TXT("banana");
	mismatchIdx = baseString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive, DString::SD_RightToLeft);
	TestLog(testFlags, TXT("The last mismatching index when searching %s within %s is %s."), searchString, baseString, mismatchIdx);
	if (mismatchIdx != 0)
	{
		UnitTestError(testFlags, TXT("DString tests failed.  The last mismatching index when searching %s within %s should be 0.  Instead it returned %s."), {searchString, baseString, mismatchIdx.ToString()});
		return false;
	}

	DString fatString = TXT("            My custom string.        ");
	main = fatString;
	TestLog(testFlags, TXT("Trimming spaces on the ends of \"%s\""), main);
	main.TrimSpaces();
	if (main != TXT("My custom string."))
	{
		UnitTestError(testFlags, TXT("DString tests failed.  Trimming spaces on the end of \"%s\" should have resulted in \"My custom string.\".  Instead it's:  \"%s\"."), {fatString, main});
		return false;
	}

	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("DString"));

	return true;
}

bool DatatypeUnitTester::TestDataBuffer (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Data Buffer"));

	SetTestCategory(testFlags, TXT("Endian Tests"));
	bool bIsLittleEndian = DataBuffer::IsSystemLittleEndian();
	if (bIsLittleEndian)
	{
		TestLog(testFlags, TXT("Byte order is little endian."));
	}
	else
	{
		TestLog(testFlags, TXT("Byte order is big endian."));
	}
	const int numCharsInMsg = 21; //include null terminator
	char testMsg[] = "Original Byte order.";
	char origTestMsg[numCharsInMsg];
	strcpy_s(origTestMsg, numCharsInMsg, testMsg);

	TestLog(testFlags, TXT("The original message before byte swap is:  %s"), DString(testMsg));
	int numChars = 20;
	DataBuffer::SwapByteOrder(testMsg, numChars);
	TestLog(testFlags, TXT("The swapped byte order of original message is:  %s"), DString(testMsg));

	for (int i = 0; i < numChars; ++i)
	{
		if (testMsg[i] != origTestMsg[numChars - i - 1])
		{
			UnitTestError(testFlags, TXT("Failed to swap the byte order of a char array.  There's a byte mismatch with the original message and the reversed message at char index %s of \"%s\".  Reversed message is \"%s\"."), {DString::MakeString(i), DString(origTestMsg), DString(testMsg)});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Write Tests"));
	DataBuffer emptyBuffer;
	DataBuffer reservedBuffer(16);
	if (emptyBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		UnitTestError(testFlags, TXT("An empty buffer's endianess should default to the machine's endianess."));
		return false;
	}

	const int numCharsInBasicData = 20; //include null terminator
	char basicData[] = "Hello Data Buffers!";
	const int numBytesInInt = sizeof(int);
	int someNumber = 392;
	const int numCharsInSuffixData = 16; //include null terminator
	char suffixData[] = "Ending Message.";

	std::vector<int> numbers = {83, 918, 732, 298}; //4 32bit numbers will fill up the 16 byte buffer.

	//Write the data to the buffers
	TestLog(testFlags, TXT("Writing \"%s\" to data buffer."), DString(basicData));
	emptyBuffer.WriteBytes(basicData, numCharsInBasicData);
	
	//Convert int to char array
	TestLog(testFlags, TXT("Writing the number %s to data buffer."), INT(someNumber));
	char someNumberChar[numBytesInInt + 1];
	memcpy(someNumberChar, &someNumber, numBytesInInt);
	//someNumberChar[numBytesInInt] = '\0';
	emptyBuffer.WriteBytes(someNumberChar, numBytesInInt);

	TestLog(testFlags, TXT("Writing \"%s\" to data buffer."), DString(suffixData));
	emptyBuffer.WriteBytes(suffixData, numCharsInSuffixData);

	//Write numbers to the other data buffer
	char numberCharList[(numBytesInInt * 4) + 1];
	//std::copy(numbers.begin(), numbers.end(), numberCharList);
	for (size_t numIdx = 0; numIdx < numbers.size(); ++numIdx)
	{
		memcpy(numberCharList + (numBytesInInt * numIdx), &numbers.at(numIdx), numBytesInInt);
	}

	//Format vector<int> to a string list for test log
	DString numList = DString::EmptyString;
	bool bFirstNum = true;
	for (int num : numbers)
	{
		if (!bFirstNum)
		{
			numList += TXT(", ");
		}

		numList += DString::MakeString(num);
		bFirstNum = false;
	}
	TestLog(testFlags, TXT("Writing the following numbers to the second data buffer:  %s"), numList);
	reservedBuffer.WriteBytes(numberCharList, numBytesInInt * 4);
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Read Tests"));
	//Extract data from emptyBuffer
	char readBasicData[numCharsInBasicData];
	int readSomeNumber = -1;
	char readSuffixData[numCharsInSuffixData];
	emptyBuffer.JumpToBeginning();
	emptyBuffer.ReadBytes(readBasicData, numCharsInBasicData);
	TestLog(testFlags, TXT("Read the following char array from data buffer:  %s"), DString(readBasicData));
	for (int charIdx = 0; charIdx < numCharsInBasicData; ++charIdx)
	{
		if (readBasicData[charIdx] != basicData[charIdx])
		{
			UnitTestError(testFlags, TXT("Failed to extract character array from data buffer.  There's a character mismatch at index %s.  The original data was \"%s\".  It extracted \"%s\" instead."), {DString::MakeString(charIdx), DString(basicData), DString(readBasicData)});
			return false;
		}
	}

	emptyBuffer.ReadBytes(someNumberChar, numBytesInInt);
	memcpy(&readSomeNumber, someNumberChar, numBytesInInt);
	TestLog(testFlags, TXT("Read the number %s from data buffer."), INT(readSomeNumber));
	if (someNumber != readSomeNumber)
	{
		UnitTestError(testFlags, TXT("Failed to extract integer from data buffer.  The original number was %s.  It extracted %s instead."), {DString::MakeString(someNumber), DString::MakeString(readSomeNumber)});
		return false;
	}

	emptyBuffer.ReadBytes(readSuffixData, numCharsInSuffixData);
	TestLog(testFlags, TXT("Read the following char array from data buffer:  %s"), DString(readSuffixData));
	for (int charIdx = 0; charIdx < numCharsInSuffixData; ++charIdx)
	{
		if (suffixData[charIdx] != readSuffixData[charIdx])
		{
			UnitTestError(testFlags, TXT("Failed to extract character array from data buffer.  There's a character mismatch at index %s.  The original data was \"%s\".  It extracted \"%s\" instead."), {DString::MakeString(charIdx), DString(suffixData), DString(readSuffixData)});
			return false;
		}
	}

	emptyBuffer.EmptyBuffer();
	INT numBytesInBuffer = emptyBuffer.GetNumBytes();
	if (numBytesInBuffer != 0)
	{
		UnitTestError(testFlags, TXT("Failed to clear data buffer.  There is still %s bytes in the data buffer after the clear command."), {numBytesInBuffer.ToString()});
		return false;
	}

	std::vector<int> readNumbers;
	reservedBuffer.JumpToBeginning();
	for (UINT_TYPE i = 0; i < numbers.size(); ++i)
	{
		int extractedNum;
		char extractedData[numBytesInInt];
		reservedBuffer.ReadBytes(extractedData, numBytesInInt);
		memcpy(&extractedNum, extractedData, numBytesInInt);
		readNumbers.push_back(extractedNum);
		TestLog(testFlags, TXT("Extracted the number %s from data buffer."), INT(extractedNum));
	}

	CHECK(readNumbers.size() == numbers.size()) //This should never happen due to for loop above
	for (UINT_TYPE i = 0; i < readNumbers.size(); ++i)
	{
		if (readNumbers.at(i) != numbers.at(i))
		{
			UnitTestError(testFlags, TXT("Failed to extract numbers from data buffer.  There's a number mismatch at index %s of number list.  It extracted %s instead of the expected %s."), {DString::MakeString(i), DString::MakeString(readNumbers.at(i)), DString::MakeString(numbers.at(i))});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Streaming"));
	{
		//Run string data buffer test here since the string test runs before the data buffer test, and the string test does not test string streaming.
		DString msg1 = TXT("First string message.");
		DString msg2 = TXT("That other string message.");
		DString msg3 = TXT("The last string message.");
		DataBuffer stringBuffer;

		stringBuffer << msg1;
		stringBuffer << msg2;
		stringBuffer << msg3;

		DString readMsg1;
		DString readMsg2;
		DString readMsg3;
		stringBuffer >> readMsg1;
		stringBuffer >> readMsg2;
		stringBuffer >> readMsg3;

		if (msg1 != readMsg1)
		{
			UnitTestError(testFlags, TXT("Failed to stream DStrings to data buffer.  The first message reads:  \"%s\".  Expected message:  \"%s\""), {readMsg1, msg1});
			return false;
		}

		if (msg2 != readMsg2)
		{
			UnitTestError(testFlags, TXT("Failed to stream DStrings to data buffer.  The second message reads:  \"%s\".  Expected message:  \"%s\""), {readMsg2, msg2});
			return false;
		}

		if (msg3 != readMsg3)
		{
			UnitTestError(testFlags, TXT("Failed to stream DStrings to data buffer.  The first message reads:  \"%s\".  Expected message:  \"%s\""), {readMsg3, msg3});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Data Buffer"));

	return true;
}

bool DatatypeUnitTester::TestStringIterator (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("String Iterator"));

	DString testString = TXT("DStringIterator test string!");
	StringIterator iter(&testString);
	std::vector<DString> expectedChars;
	for (UINT_TYPE i = 0; i < testString.Length(); i++)
	{
		expectedChars.push_back(DString(testString.At(i)));
	}

	SetTestCategory(testFlags, TXT("Incrementing Iterator"));
	TestLog(testFlags, TXT("Using an iterator to increment through this string:  \"%s\""), testString);

	INT idx = 0;
	for (; !iter.IsAtEnd(); iter++)
	{
		DString curCharacter = iter.GetString();
		TestLog(testFlags, TXT("At Idx %s, the iterator points at \"%s\""), INT(idx), curCharacter);
		if (curCharacter != expectedChars.at(idx.ToUnsignedInt()))
		{
			UnitTestError(testFlags, TXT("The iterator did not find the expected character (\"%s\") at idx %s.  Instead the iterator is returning \"%s\""), {expectedChars.at(idx.ToUnsignedInt()), INT(idx).ToString(), curCharacter});
			return false;
		}

		idx++;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Decrementing Iterator"));
	StringIterator backwardsIterator(&testString, false); //Point to last character
	for (idx = expectedChars.size() - 1; idx >= 0; idx--)
	{
		DString curCharacter = backwardsIterator.GetString();
		TestLog(testFlags, TXT("At Idx %s, the iterator points at \"%s\""), INT(idx), curCharacter);
		if (curCharacter != expectedChars.at(idx.ToUnsignedInt()))
		{
			UnitTestError(testFlags, TXT("The iterator did not find the expected character (\"%s\") at idx %s.  Instead the iterator is returning \"%s\""), {expectedChars.at(idx.ToUnsignedInt()), INT(idx).ToString(), curCharacter});
			return false;
		}

		if (!backwardsIterator.IsAtBeginning())
		{
			backwardsIterator--;
		}
	}

	if (!backwardsIterator.IsAtBeginning())
	{
		UnitTestError(testFlags, TXT("Despite reaching the beginning of the expected character array, the iterator does not claim that it's at the beginning of the string."));
		return false;
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("String Iterator"));

	return true;
}

bool DatatypeUnitTester::TestCharacterEncoding (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Character Encoding"));

	bool bTestUnicode = true;
#if USE_UTF32
	TestLog(testFlags, TXT("Character encoding is UTF-32"));
#elif USE_UTF16
	TestLog(testFlags, TXT("Character encoding is UTF-16"));
#elif USE_UTF8
	TestLog(testFlags, TXT("Character encoding is UTF-8"));
#else
	TestLog(testFlags, TXT("Not using character encoding.  DStrings are ANSI."));
	bTestUnicode = false;
#endif

	if (!bTestUnicode)
	{
		TestLog(testFlags, TXT("Skipping character encoding test."));
		ExecuteSuccessSequence(testFlags, TXT("Character Encoding"));
		return true;
	}

	SetTestCategory(testFlags, TXT("Unicode String Iterator"));

	//Note:  When debugging the string values in Visual Studio, you may have to append ",s8" in the watch window's variable names to view the char string in UTF-8.
	//to view UTF-16, enter ',s' at the end of variable name.  And ',s32' for UTF-32 strings.
	DString testString = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 = cosmos"); //Greek word for kosme

	//It's important to test the string iterator since many string utilities (such as FindAt and FindSubText) depends on the string iterator.
	TestLog(testFlags, TXT("Using a string iterator it iterate through ") STRING_CONFIG_NAME TXT(" characters."));
	StringIterator testIter(&testString);
	std::vector<DString> expectedCharacters;
	expectedCharacters.push_back(TXT("\u03ba"));
	expectedCharacters.push_back(TXT("\u1f79"));
	expectedCharacters.push_back(TXT("\u03c3"));
	expectedCharacters.push_back(TXT("\u03bc"));
	expectedCharacters.push_back(TXT("\u03b5"));
	expectedCharacters.push_back(TXT(" "));
	expectedCharacters.push_back(TXT("="));
	expectedCharacters.push_back(TXT(" "));
	expectedCharacters.push_back(TXT("c"));
	expectedCharacters.push_back(TXT("o"));
	expectedCharacters.push_back(TXT("s"));
	expectedCharacters.push_back(TXT("m"));
	expectedCharacters.push_back(TXT("o"));
	expectedCharacters.push_back(TXT("s"));
	for (UINT_TYPE charIdx = 0; charIdx < expectedCharacters.size(); charIdx++)
	{
		if (testIter.IsAtEnd())
		{
			//Log error message without listing testString since it's likely that it will not be able to output that log statement if the string iterator fails.
			UnitTestError(testFlags, TXT("String iterator failed to iterate through ") STRING_CONFIG_NAME TXT(" characters at idx %s since it reached to the end of the string early."), {DString::MakeString(charIdx)});
			return false;
		}

		if (testIter.GetString() != expectedCharacters.at(charIdx))
		{
			//Log error message without listing testString since it's likely that it will not be able to output that log statement if the string iterator fails.
			UnitTestError(testFlags, TXT("String iterator failed to retrieve the expected character at idx %s."), {DString::MakeString(charIdx)});
			return false;
		}
		++testIter;
	}

	//test backwards iterator
	StringIterator reverseTestIter(&testString, false);
	for (INT charIdx = expectedCharacters.size() - 1; charIdx >= 0; charIdx--)
	{
		if (charIdx > 0 && reverseTestIter.IsAtBeginning())
		{
			//Log error message without listing testString since it's likely that it will not be able to output that log statement if the string iterator fails.
			UnitTestError(testFlags, TXT("The reverse string iterator failed to iterate through ") STRING_CONFIG_NAME TXT(" characters at idx %s since it reached to the beginning of the string early."), {charIdx.ToString()});
			return false;
		}
		else if (charIdx == 0 && !reverseTestIter.IsAtBeginning())
		{
			//Log error message without listing testString since it's likely that it will not be able to output that log statement if the string iterator fails.
			UnitTestError(testFlags, TXT("The reverse string iterator test failed since the string iterator is not returning that it's at the beginning of the string despite the character index being equal to %s."), {charIdx.ToString()});
			return false;
		}

		if (reverseTestIter.GetString() != expectedCharacters.at(charIdx.ToUnsignedInt()))
		{
			//Log error message without listing testString since it's likely that it will not be able to output that log statement if the string iterator fails.
			UnitTestError(testFlags, TXT("The reverse string iterator failed to retrieve the expected character at idx %s."), {charIdx.ToString()});
			return false;
		}

		if (charIdx > 0)
		{
			--reverseTestIter;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Encoding Validation"));
	DString validString = testString;
	if (!validString.IsValidEncoding())
	{
		UnitTestError(testFlags, TXT("The valid " STRINGIFY(STRING_CONFIG_NAME) " string is considered not valid."));
		return false;
	}

	std::vector<DString> invalidStrings;
#if USE_UTF32
	#error Please populate the invalidStrings vector for UTF-32.
#elif USE_UTF16
	#error Please populate the invalidStrings vector for UTF-16.
#elif USE_UTF8
	//These strings are copied from:  http://stackoverflow.com/questions/1301402/example-invalid-utf8-string
	invalidStrings.push_back("\xc3\x28");
	invalidStrings.push_back("\xa0\xa1");
	invalidStrings.push_back("\xe2\x28\xa1");
	invalidStrings.push_back("\xe2\x82\x28");
	invalidStrings.push_back("\xf0\x28\x8c\xbc");
	invalidStrings.push_back("\xf0\x90\x28\xbc");
	invalidStrings.push_back("\xf0\x28\x8c\x28");
#endif
	for (UINT_TYPE i = 0; i < invalidStrings.size(); i++)
	{
		if (invalidStrings.at(i).IsValidEncoding())
		{
			UnitTestError(testFlags, TXT("An invalid string at idx %s is considered to be a valid " STRINGIFY(STRING_CONFIG_NAME) " string."), {DString::MakeString(i)});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Encoding Conversions"));
	StringUTF8 utf8 = testString.ToUTF8();
	StringUTF16 utf16 = testString.ToUTF16();
	StringUTF32 utf32 = testString.ToUTF32();
	TestLog(testFlags, TXT("The value of the test string is a Greek word for kosme (cosmos).  \"%s\""), testString);
	if (!testString.IsValidEncoding())
	{
		UnitTestError(testFlags, TXT("Failed to construct a UTF-8 string:  %s"), {testString.ToCString()});
		return false;
	}

	//Length function should return the expected value
	TestLog(testFlags, TXT("The length of \"%s\" is:  %s"), testString, testString.Length());
	if (testString.Length() != 14)
	{
		UnitTestError(testFlags, TXT("The length of \"%s\" is not the expected value of 14.  Instead it's reporting that the length of that string is %s"), {testString, testString.Length().ToString()});
		return false;
	}

	//The converted strings should report the same lengths as the original string
	if (testString.Length() != DString::UTF32Length(utf32))
	{
		UnitTestError(testFlags, TXT("The converted string to UTF-32 does not report the same number of characters as the original string.  The original string's length is %s, and the converted string's length is %s."), {testString.Length().ToString(), DString::UTF32Length(utf32).ToString()});
		return false;
	}

	if (testString.Length() != DString::UTF16Length(utf16))
	{
		UnitTestError(testFlags, TXT("The converted string to UTF-16 does not report the same number of characters as the original string.  The original string's length is %s, and the converted string's length is %s."), {testString.Length().ToString(), DString::UTF16Length(utf16).ToString()});
		return false;
	}

	if (testString.Length() != DString::UTF8Length(utf8))
	{
		UnitTestError(testFlags, TXT("The converted string to UTF-8 does not report the same number of characters as the original string.  The original string's length is %s, and the converted string's length is %s."), {testString.Length().ToString(), DString::UTF16Length(utf16).ToString()});
		return false;
	}

	//When converting from UTF-x back to its original encoding, it should be equal to the same value as original string.
	DString convertedFromUTF32(utf32);
	if (convertedFromUTF32 != testString)
	{
		UnitTestError(testFlags, TXT("Failed to convert original string to UTF-32 encoding, and back to its original character encoding."));
		return false;
	}
	TestLog(testFlags, TXT("Successfully converted string to UTF-32 and back to original."));

	DString convertedFromUTF16(utf16);
	if (convertedFromUTF16 != testString)
	{
		UnitTestError(testFlags, TXT("Failed to convert original string to UTF-16 encoding, and back to its original character encoding."));
		return false;
	}
	TestLog(testFlags, TXT("Successfully converted string to UTF-16 and back to original."));

	DString convertedFromUTF8(utf8);
	if (convertedFromUTF8 != testString)
	{
		UnitTestError(testFlags, TXT("Failed to convert original string to UTF-8 encoding, and back to its original character encoding."));
		return false;
	}
	TestLog(testFlags, TXT("Successfully converted string to UTF-8 and back to original."));

	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("String Utilities"));
	{
		std::function<bool(const DString&)> isValidString([&](const DString& testString)
		{
			bool result = testString.IsValidEncoding();
			if (!result)
			{
				UnitTestError(testFlags, TXT("The string \"%s\" contains invalid characters."), {testString});
			}

			return result;
		});

		DString expectedResult = TXT("\u1f79");
		DString result = testString.FindAt(1);
		if (!isValidString(result))
		{
			return false;
		}

		TestLog(testFlags, TXT("The character of \"%s\" at index 1 is:  \"%s\""), testString, result);
		if (result != expectedResult)
		{
			UnitTestError(testFlags, TXT("Failed to obtain the 2nd character from \"%s\".  It returned \"%s\" instead."), {testString, result});
			return false;
		}

		expectedResult = TXT("c");
		result = testString.FindAt(8);
		if (!isValidString(result))
		{
			return false;
		}

		TestLog(testFlags, TXT("The character of \"%s\" at index 8 is:  \"%s\""), testString, result);
		if (result != expectedResult)
		{
			UnitTestError(testFlags, TXT("Failed to obtain the 9th character from \"%s\".  It returned \"%s\" instead."), {testString, result});
			return false;
		}

		//Test to upper (FindSubText test depends on case insensitive comparison)
		DString toUpperString = testString;
		toUpperString.ToUpper();
		if (!isValidString(toUpperString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Converting \"%s\" to upper case is \"%s\""), testString, toUpperString);
		DString expectedString = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 = COSMOS");
		if (toUpperString != expectedString)
		{
			UnitTestError(testFlags, TXT("Failed to convert \"%s\" to upper case since it resulted in \"%s\"."), {testString, toUpperString});
			return false;
		}

		DString toLowerString = toUpperString;
		toLowerString.ToLower();
		if (!isValidString(toLowerString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Converting \"%s\" to lower case is \"%s\""), toUpperString, toLowerString);
		expectedString = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 = cosmos");
		if (toLowerString != expectedString)
		{
			UnitTestError(testFlags, TXT("Failed to convert \"%s\" to lower case since it resulted in \"%s\"."), {toUpperString, toLowerString});
			return false;
		}

		if (toLowerString.Compare(toUpperString, DString::CC_IgnoreCase) != 0)
		{
			UnitTestError(testFlags, TXT("Failed to compare strings:  \"%s\" and \"%s\".  Comparing those two (case ignored), this function should have returned 0."), {toUpperString, toLowerString});
			return false;
		}

		if (testString.Compare(toUpperString, DString::CC_CaseSensitive) == 0)
		{
			UnitTestError(testFlags, TXT("Failed to compare strings:  \"%s\" and \"%s\".  Comparing those two (case sensitive), this function should have returned non-zero."), {testString, toUpperString});
			return false;
		}

		if (testString == toUpperString)
		{
			UnitTestError(testFlags, TXT("The DString's == operator should be case sensitive.  However, comparing \"%s\" to \"%s\" returned true instead of false."), {testString, toUpperString});
			return false;
		}

		DString searchString = TXT("cosmos");
		INT foundIdx = testString.Find(searchString, 0, DString::CC_CaseSensitive);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case sensitive) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 8)
		{
			UnitTestError(testFlags, TXT("FindSubText failed to find the expected index.  When searching for \"%s\" within \"%s\", FindSubText returned %s instead of 8."), {searchString, testString, foundIdx.ToString()});
			return false;
		}

		foundIdx = testString.Find(searchString, 0, DString::CC_IgnoreCase);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case insensitive) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 8)
		{
			UnitTestError(testFlags, TXT("FindSubText failed to find the expected index.  When searching for \"%s\" within \"%s\", FindSubText returned %s instead of 8."), {searchString, testString, foundIdx.ToString()});
			return false;
		}

		searchString = TXT("\u03bc");
		foundIdx = testString.Find(searchString, 0, DString::CC_CaseSensitive);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case sensitive) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 3)
		{
			UnitTestError(testFlags, TXT("FindSubText failed to find the expected index.  When searching for \"%s\" within \"%s\", FindSubText returned %s instead of 3."), {searchString, testString, foundIdx.ToString()});
			return false;
		}

		foundIdx = testString.Find(searchString, 0, DString::CC_IgnoreCase);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case insensitive) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 3)
		{
			UnitTestError(testFlags, TXT("FindSubText failed to find the expected index.  When searching for \"%s\" within \"%s\", FindSubText returned %s instead of 3."), {searchString, testString, foundIdx.ToString()});
			return false;
		}

		searchString = TXT("\u1f79\u03c3\u03bc\u03b5");
		foundIdx = testString.Find(searchString, 0, DString::CC_CaseSensitive, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case sensitive from the right) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 1)
		{
			UnitTestError(testFlags, TXT("FindSubText failed to find the expected index.  When searching for \"%s\" within \"%s\", FindSubText returned %s instead of 1."), {searchString, testString, foundIdx.ToString()});
			return false;
		}

		foundIdx = testString.Find(searchString, 0, DString::CC_IgnoreCase, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case insensitive from the right) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 1)
		{
			UnitTestError(testFlags, TXT("FindSubText failed to find the expected index.  When searching for \"%s\" within \"%s\", FindSubText returned %s instead of 1."), {searchString, testString, foundIdx.ToString()});
			return false;
		}

		searchString = TXT("o");
		foundIdx = testString.Find(searchString, 0, DString::CC_IgnoreCase, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case insensitive from the right) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 12)
		{
			UnitTestError(testFlags, TXT("FindSubText failed to find the expected index.  When searching for \"%s\" within \"%s\", FindSubText returned %s instead of 12."), {searchString, testString, foundIdx.ToString()});
			return false;
		}

		foundIdx = testString.Find(searchString, 2, DString::CC_IgnoreCase, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("Searching for \"%s\" within \"%s\" (case insensitive from the right starting from position 2) is found at index %s."), searchString, testString, foundIdx);
		if (foundIdx != 9)
		{
			UnitTestError(testFlags, TXT("FindSubText failed to find the expected index.  When searching for \"%s\" within \"%s\", FindSubText returned %s instead of 9."), {searchString, testString, foundIdx.ToString()});
			return false;
		}

		//Testing StartsWith and EndsWith
		{
			DString baseString = TXT("I am the knife in the darkness and you are my light. Trust in me like how I trust you. Doing all of this...and you being at my side when I do work is what makes me happy."); //-Yohkuj
			DString searchPrefix = TXT("I am the knife in the darkness and you are my light.");
			if (!baseString.StartsWith(searchPrefix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's StartsWith test failed. \"%s\" should have started with \"%s\"."), {baseString, searchPrefix});
				return false;
			}

			searchPrefix = TXT("You are the knife");
			if (baseString.StartsWith(searchPrefix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's StartsWith test failed. \"%s\" does not start with \"%s\"."), {baseString, searchPrefix});
				return false;
			}

			DString searchSuffix = TXT("you being at my side when I do work is what makes me happy.");
			if (!baseString.EndsWith(searchSuffix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's EndsWith test failed. \"%s\" should have ended with \"%s\"."), {baseString, searchSuffix});
				return false;
			}

			searchSuffix = TXT("work makes me happy.");
			if (baseString.EndsWith(searchSuffix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's EndsWith test failed. \"%s\" does not end with \"%s\"."), {baseString, searchSuffix});
				return false;
			}

			if (!baseString.StartsWith('I'))
			{
				UnitTestError(testFlags, TXT("DString's StartsWith test failed. \"%s\" should start with 'I'."), {baseString});
				return false;
			}

			if (!baseString.EndsWith('.'))
			{
				UnitTestError(testFlags, TXT("DString's EndsWith test failed. \"%s\" should have ended with '.'."), {baseString});
				return false;
			}

			//Test case where the string is longer than the base string.
			baseString = TXT("Weaver of Hearts");
			searchPrefix = TXT("Weaver of Hearts!");
			if (baseString.StartsWith(searchPrefix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's StartsWith test failed. \"%s\" should not start with \"%s\"."), {baseString, searchPrefix});
				return false;
			}

			searchSuffix = TXT("The Weaver of Hearts");
			if (baseString.EndsWith(searchSuffix, DString::CC_CaseSensitive))
			{
				UnitTestError(testFlags, TXT("DString's EndsWith test failed. \"%s\" does not end with \"%s\"."), {baseString, searchSuffix});
				return false;
			}
		}

		//Testing popback
		{
			const DString originalStr = TXT("Artorias Mistwalker");
			DString popBackStr(originalStr);
			const std::vector<DString> expectedStrs
			{
				TXT("Artorias Mistwalker"),
				TXT("Artorias Mistwalke"),
				TXT("Artorias Mistwalk"),
				TXT("Artorias Mistwal"),
				TXT("Artorias Mistwa"),
				TXT("Artorias Mistw"),
				TXT("Artorias Mist"),
				TXT("Artorias Mis"),
				TXT("Artorias Mi"),
				TXT("Artorias M"),
				TXT("Artorias "),
				TXT("Artorias"),
				TXT("Artoria"),
				TXT("Artori"),
				TXT("Artor"),
				TXT("Arto"),
				TXT("Art"),
				TXT("Ar"),
				TXT("A")
			};

			INT popCounter = 0;
			for (size_t i = 0; i < expectedStrs.size(); ++i)
			{
				if (popBackStr.Compare(expectedStrs.at(i), DString::CC_CaseSensitive) != 0)
				{
					UnitTestError(testFlags, TXT("\"%s\" does not match \"%s\" after popping \"%s\" %s times."), {popBackStr, expectedStrs.at(i), originalStr, popCounter.ToString()});
					return false;
				}

				popBackStr.PopBack();
				++popCounter;
			}

			if (!popBackStr.IsEmpty())
			{
				UnitTestError(testFlags, TXT("Popping \"%s\" %s times did not end with an empty string. There are still \"%s\" remaining."), {originalStr, popCounter.ToString(), popBackStr});
				return false;
			}
		}

		//Test Remove since Format string depends on it.
		DString removeTest = testString;
		removeTest.Remove(8, 3);
		if (!isValidString(removeTest))
		{
			return false;
		}

		expectedResult = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 = mos");
		TestLog(testFlags, TXT("Removing \"cos\" from \"%s\" results in \"%s\"."), testString, removeTest);
		if (removeTest != expectedResult)
		{
			UnitTestError(testFlags, TXT("Removing characters from string test failed.  Removing 3 characters from index 8 from string \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), {testString, expectedResult, removeTest});
			return false;
		}

		removeTest = testString;
		removeTest.Remove(0, 4);
		if (!isValidString(removeTest))
		{
			return false;
		}

		expectedResult = TXT("\u03b5 = cosmos");
		TestLog(testFlags, TXT("Removing \"\u03ba\u1f79\u03c3\u03bc\" from \"%s\" results in \"%s\"."), testString, removeTest);
		if (removeTest != expectedResult)
		{
			UnitTestError(testFlags, TXT("Removing characters from string test failed.  Removing 4 characters from index 0 from string \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), {testString, expectedResult, removeTest});
			return false;
		}

		//Test Insert since Format string depends on it.
		DString baseString = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 cosmos");
		DString insertTest = baseString;
		DString insertingString = TXT("is equal to ");
		insertTest.Insert(6, insertingString);
		if (!isValidString(insertTest))
		{
			return false;
		}

		expectedResult = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 is equal to cosmos");
		TestLog(testFlags, TXT("Inserting \"%s\" at index 6 of string \"%s\" results in \"%s\"."), insertingString, baseString, insertTest);
		if (insertTest != expectedResult)
		{
			UnitTestError(testFlags, TXT("Inserting the string \"%s\" at index 6 of \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), {insertingString, baseString, expectedResult, insertTest});
			return false;
		}

		baseString = TXT("\u03ba = cosmos");
		insertTest = baseString;
		insertingString = TXT("\u1f79\u03c3\u03bc\u03b5");
		insertTest.Insert(1, insertingString);
		if (!isValidString(insertTest))
		{
			return false;
		}

		expectedResult = testString;
		TestLog(testFlags, TXT("Inserting \"%s\" at index 1 of string \"%s\" results in \"%s\"."), insertingString, baseString, insertTest);
		if (insertTest != expectedResult)
		{
			UnitTestError(testFlags, TXT("Inserting the string \"%s\" at index 1 of \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), {insertingString, baseString, expectedResult, insertTest});
			return false;
		}

		//Test Format string since logs depends on it.
		DString ansiReplaceString = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 = %s%s%s%s%s%s");
		DString formattedString = DString::CreateFormattedString(ansiReplaceString, DString(TXT("c")), DString(TXT("o")), DString(TXT("s")), DString(TXT("m")), DString(TXT("o")), DString(TXT("s")));
		if (!isValidString(formattedString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Replacing s macros with the word cosmos in string \"%s\" resulted in \"%s\""), ansiReplaceString, formattedString);
		if (formattedString != testString)
		{
			UnitTestError(testFlags, TXT("Format string test failed since it returned a string that doesn't match the expected string \"%s\".  Instead it returned \"%s\"."), {testString, formattedString});
			return false;
		}

		DString unicodeReplaceString = TXT("%s%s%s%s%s = cosmos");
		formattedString = DString::CreateFormattedString(unicodeReplaceString, DString(TXT("\u03ba")), DString(TXT("\u1f79")), DString(TXT("\u03c3")), DString(TXT("\u03bc")), DString(TXT("\u03b5")));
		if (!isValidString(formattedString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Replacing s macros with the word \u03ba\u1f79\u03c3\u03bc\u03b5 in string \"%s\" resulted in \"%s\""), unicodeReplaceString, formattedString);
		if (formattedString != testString)
		{
			UnitTestError(testFlags, TXT("Format string test failed since it returned a string that doesn't match the expected string \"%s\".  Instead it returned \"%s\"."), {testString, formattedString});
			return false;
		}

		DString subString = testString.SubString(8);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("cosmos");
		TestLog(testFlags, TXT("The Substring of \"%s\" at index 8 to the end is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("Sub string test failed.  The sub string of \"%s\" from index 8 to end should have resulted in \"%s\".  Instead it resulted in \"%s\"."), {testString, expectedResult, subString});
			return false;
		}

		subString = testString.SubString(1, 8);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("\u1f79\u03c3\u03bc\u03b5 = c");
		TestLog(testFlags, TXT("The Substring of \"%s\" from indices 1-8 is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("Sub string test failed.  The sub string of \"%s\" from indices 1-8 should have resulted in \"%s\".  Instead it resulted in \"%s\"."), {testString, expectedResult, subString});
			return false;
		}

		subString = testString.SubStringCount(8, -1);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("cosmos");
		TestLog(testFlags, TXT("The SubstringCount of \"%s\" from index 8 to the end is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("Sub string count test failed.  The sub string of \"%s\" from index 8 to the end should have resulted in \"%s\".  Instead it resulted in \"%s\"."), {testString, expectedResult, subString});
			return false;
		}

		subString = testString.SubStringCount(0, 5);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5");
		TestLog(testFlags, TXT("Using SubStringCount, the first 5 characters of \"%s\" is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("Sub string count test failed.  The first 5 characters of \"%s\" should have been \"%s\".  Instead it's \"%s\"."), {testString, expectedResult, subString});
			return false;
		}

		//Test Left and Right
		subString = testString.Left(5);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5");
		TestLog(testFlags, TXT("Using Left, the first 5 characters of \"%s\" is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("The first 5 characters of \"%s\" should have been \"%s\".  Instead it's \"%s\"."), {testString, expectedResult, subString});
			return false;
		}

		subString = testString.Right(6);
		if (!isValidString(subString))
		{
			return false;
		}

		expectedResult = TXT("cosmos");
		TestLog(testFlags, TXT("Using Right, the last 6 characters of \"%s\" is \"%s\"."), testString, subString);
		if (subString != expectedResult)
		{
			UnitTestError(testFlags, TXT("The last 6 characters of \"%s\" should have been \"%s\".  Instead it's \"%s\"."), {testString, expectedResult, subString});
			return false;
		}

		//Test Split String
		DString segment1;
		DString segment2;
		DString expectedSeg1 = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5 ");
		DString expectedSeg2 = TXT(" cosmos");
		testString.SplitString(6, segment1, segment2);
		if (!isValidString(segment1) || !isValidString(segment2))
		{
			return false;
		}

		TestLog(testFlags, TXT("Splitting the string \"%s\" at index 6 resulted in two the segments:  \"%s\" and \"%s\"."), testString, segment1, segment2);
		if (segment1 != expectedSeg1 || segment2 != expectedSeg2)
		{
			UnitTestError(testFlags, TXT("Split String test failed.  When splitting the string \"%s\" at index 6, the resulting segments should have been \"%s\" and \"%s\".  Instead, the resulting segments are \"%s\" and \"%s\"."), {testString, expectedSeg1, expectedSeg2, segment1, segment2});
			return false;
		}

		//Test parsing string
		std::vector<DString> testStringParts;
		testString.ParseString(' ', testStringParts, true);
		std::vector<DString> expectedParts;
		expectedParts.push_back(TXT("\u03ba\u1f79\u03c3\u03bc\u03b5"));
		expectedParts.push_back(TXT("="));
		expectedParts.push_back(TXT("cosmos"));
		TestLog(testFlags, TXT("Splitting \"%s\" so that each segment between spaces is its own vector resulted in this vector:  %s."), testString, DString::ListToString(testStringParts));
		if (testStringParts.size() != expectedParts.size())
		{
			UnitTestError(testFlags, TXT("Parse String test failed.  Splitting \"%s\" by the space character should have resulted in a vector of size %s.  Instead the resulting vector size is %s."), {testString, DString::MakeString(expectedParts.size()), DString::MakeString(testStringParts.size())});
			return false;
		}

		for (UINT_TYPE i = 0; i < expectedParts.size(); i++)
		{
			if (expectedParts.at(i) != testStringParts.at(i))
			{
				UnitTestError(testFlags, TXT("Parse String test failed.  After splitting \"%s\" by the space character, the expected vector at index %s should have resulted in \"%s\".  Instead the vector at index %s is actually \"%s\"."), {testString, DString::MakeString(i), expectedParts.at(i), DString::MakeString(i), testStringParts.at(i)});
				return false;
			}
		}

		//Test IsWrappedByChar
		BOOL bIsWrapped = testString.IsWrappedByChar('o', 13); //Test the last s after the second o.
		TestLog(testFlags, TXT("The last 's' in \"%s\" surrounded by 'o'?  %s"), testString, bIsWrapped);
		if (bIsWrapped)
		{
			UnitTestError(testFlags, TXT("IsWrappedByChar test failed.  The last s in \"%s\" should not be considered that it's surrounded by o's."), {testString});
			return false;
		}

		bIsWrapped = testString.IsWrappedByChar('o', 10); //Test the s after the first o.
		TestLog(testFlags, TXT("The 's' after the first 'o' in \"%s\" surrounded by 'o'?  %s"), testString, bIsWrapped);
		if (!bIsWrapped)
		{
			UnitTestError(testFlags, TXT("IsWrappedByChar test failed.  The s after the first o in \"%s\" should be considered that it's surrounded by o's."), {testString});
			return false;
		}

		//Test ReplaceInline
		DString replacedString = testString;
		DString greekCosmos = TXT("\u03ba\u1f79\u03c3\u03bc\u03b5");
		DString englishCosmos = TXT("cosmos");
		replacedString.ReplaceInline(greekCosmos, englishCosmos, DString::CC_CaseSensitive);
		expectedResult = TXT("cosmos = cosmos");
		if (!isValidString(replacedString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Replacing \"%s\" in \"%s\" with \"%s\" resulted in \"%s\"."), greekCosmos, testString, englishCosmos, replacedString);
		if (replacedString != expectedResult)
		{
			UnitTestError(testFlags, TXT("String replace test failed.  Replacing \"%s\" in \"%s\" with \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), {greekCosmos, testString, englishCosmos, expectedResult, replacedString});
			return false;
		}

		replacedString = testString;
		replacedString.ReplaceInline(englishCosmos, greekCosmos, DString::CC_CaseSensitive);
		expectedResult = greekCosmos + TXT(" = ") + greekCosmos;
		if (!isValidString(replacedString))
		{
			return false;
		}

		TestLog(testFlags, TXT("Replacing \"%s\" in \"%s\" with \"%s\" resulted in \"%s\"."), englishCosmos, testString, greekCosmos, replacedString);
		if (replacedString != expectedResult)
		{
			UnitTestError(testFlags, TXT("String replace test failed.  Replacing \"%s\" in \"%s\" with \"%s\" should have resulted in \"%s\".  Instead it resulted in \"%s\"."), {englishCosmos, testString, greekCosmos, expectedResult, replacedString});
			return false;
		}

		//Test FindFirstMismatchingIdx
		DString testMismatchIdxString = TXT("|||") + testString + TXT("|||||");
		searchString = TXT("|||");
		INT mismatchIdx = testMismatchIdxString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive, DString::SD_LeftToRight);
		TestLog(testFlags, TXT("Searching for the first mismatching character when searching \"%s\" within \"%s\" resulted in %s."), searchString, testMismatchIdxString, mismatchIdx);
		if (mismatchIdx != 3)
		{
			UnitTestError(testFlags, TXT("Failed to find the first mismatching character.  When searching for the first the first character that does not match \"%s\" within \"%s\", it should have returned 3.  Instead it returned %s."), {searchString, testMismatchIdxString, mismatchIdx.ToString()});
			return false;
		}

		mismatchIdx = testMismatchIdxString.FindFirstMismatchingIdx(searchString, DString::CC_CaseSensitive, DString::SD_RightToLeft);
		TestLog(testFlags, TXT("Searching from the right for the first mismatching character when searching \"%s\" within \"%s\" resulted in %s."), searchString, testMismatchIdxString, mismatchIdx);
		if (mismatchIdx != 16)
		{
			UnitTestError(testFlags, TXT("Failed to find the first mismatching character when searching from the right.  When searching from the right for the first the first character that does not match \"%s\" within \"%s\", it should have returned 16.  Instead it returned %s."), {searchString, testMismatchIdxString, mismatchIdx.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Character Encoding"));

	return true;
}

bool DatatypeUnitTester::TestBOOL (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Bool"));

	TestLog(testFlags, TXT("NOTE:  booleans may be displayed incorrectly since the localization module was not yet tested."));

	BOOL blankBool(false);
	BOOL initBool(true);
	BOOL copiedBool(initBool);
	BOOL stringBool(TXT("True"));

	TestLog(testFlags, TXT("Constructor tests:  BlankBool=%s"), blankBool);
	TestLog(testFlags, TXT("    initializedBool(true)=%s"), initBool);
	TestLog(testFlags, TXT("    copiedBool from initializedBool=%s"), copiedBool);
	TestLog(testFlags, TXT("    stringBool(\"true\")=%s"), stringBool);

	SetTestCategory(testFlags, TXT("Comparison"));
	BOOL trueBool = true;
	BOOL falseBool = false;
	TestLog(testFlags, TXT("True is returning %s"), trueBool);
	if (!trueBool || falseBool)
	{
		UnitTestError(testFlags, TXT("Comparison check failed.  Expected bool conditions (true) and (false) did not return the right conditions.  They returned  %s and %s."), {trueBool.ToString(), falseBool.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Testing bool comparison (true == false)?"), BOOL(trueBool == falseBool));
	if (trueBool == falseBool)
	{
		UnitTestError(testFlags, TXT("Comparison check failed.  True should not equal to false."));
		return false;
	}

	if (trueBool != true)
	{
		UnitTestError(testFlags, TXT("Comparison check failed.  True should be equal to true."));
		return false;
	}

	if (true != trueBool)
	{
		UnitTestError(testFlags, TXT("Comparison check failed.  True should be equal to true."));
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		std::vector<BOOL> bools = {true, false, false, true, true, false, true, false};
		std::vector<BOOL> readBools;
		DataBuffer boolBuffer;

		for (UINT_TYPE i = 0; i < bools.size(); ++i)
		{
			boolBuffer << bools.at(i);
		}

		for (UINT_TYPE i = 0; i < bools.size(); ++i)
		{
			BOOL newBool;
			boolBuffer >> newBool;
			readBools.push_back(newBool);
		}

		if (bools.size() != readBools.size())
		{
			UnitTestError(testFlags, TXT("Bool streaming test failed.  After pushing %s bools to a data buffer.  %s bools were pulled from that data buffer."), {DString::MakeString(bools.size()), DString::MakeString(readBools.size())});
			return false;
		}

		for (UINT_TYPE i = 0; i < bools.size(); ++i)
		{
			if (bools.at(i) != readBools.at(i))
			{
				UnitTestError(testFlags, TXT("Bool streaming test failed.  There's a bool mismatch at index %s.  The bool %s was pushed to that data buffer, but a %s was pulled from there instead."), {DString::MakeString(i), bools.at(i).ToString(), readBools.at(i).ToString()});
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("BOOL"));

	return true;
}

bool DatatypeUnitTester::TestINT (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("INT"));

	//Ensure the INDEX_NONE macro has negative values for signed ints, and for unsigned int, equal to max possible value.
	INT signedNullIndex = INDEX_NONE;
	if (signedNullIndex >= 0)
	{
		UnitTestError(testFlags, TXT("The INDEX_NONE macro should be a negative value for signed integers.  Instead it's equal to:  %s"), {signedNullIndex.ToString()});
		return false;
	}

	size_t unsignedNullIndex = INDEX_NONE;
	if (unsignedNullIndex != SIZE_MAX)
	{
		UnitTestError(testFlags, TXT("The INDEX_NONE macro should be the highest possible value for unsigned integers.  The macro is equal to %s instead of %s"), {DString::MakeString(unsignedNullIndex), DString::MakeString(SIZE_MAX)});
		return false;
	}

	INT blankInt(0);
	INT initializedInt(15);
	INT copiedInt(initializedInt);

	TestLog(testFlags, TXT("Constructor tests:  BlankInt=") + blankInt.ToString());
	TestLog(testFlags, TXT("    initializedInt(15)= ") + initializedInt.ToString());
	TestLog(testFlags, TXT("    copiedInt from initializedInt= ") + copiedInt.ToString());

#ifdef PLATFORM_64BIT
	if (sizeof(blankInt.Value) != 8)
	{
		UnitTestError(testFlags, TXT("INTs are not 64 bit."));
		return false;
	}
#else
	if (sizeof(blankInt.Value) != 4)
	{
		UnitTestError(testFlags, TXT("INTs are not 32 bit."));
		return false;
	}
#endif

	SetTestCategory(testFlags, TXT("Comparison"));
	TestLog(testFlags, TXT("blankInt != 0?  %s"), BOOL(blankInt != 0));
	if (blankInt != 0)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  blankInt is not zero.  Instead it's:  ") + blankInt.ToString());
		return false;
	}

	TestLog(testFlags, TXT("initializedInt(%s) != copiedInt(%s) ? %s"), initializedInt, copiedInt, BOOL(initializedInt != copiedInt));
	if (initializedInt != copiedInt)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedInt != copiedInt.  Values are:  %s and %s"), {initializedInt.ToString(), copiedInt.ToString()});
		return false;
	}

	INT testInt = 5;
	TestLog(testFlags, TXT("5 < 6 ? %s"), BOOL(testInt < 6));
	if (!(testInt < 6))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is not less than 6."), {testInt.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("5 <= 5 ? %s"), BOOL(testInt <= 5));
	if (!(testInt <= 5))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is not less than or equal to 5."), {testInt.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("5 > 3 ? %s"), BOOL(testInt > 3));
	if (!(testInt > 3))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is not greater than 3."), {testInt.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("5 >= 7 ? %s"), BOOL(testInt >= 7));
	if (testInt >= 7)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  testInt(%s) is greater than or equal to 7."), {testInt.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("5 >= 3 && 5 < 7 && 5 == 5 && (5 == 10 || 5 != 9) ? %s"), BOOL(testInt >= 3 && testInt < 7 && testInt == 5 && (testInt == 10 || testInt != 9)));
	if (!(testInt >= 3 && testInt < 7 && testInt == 5 && (testInt == 10 || testInt != 9)))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testInt is:  %s"), {testInt.ToString()});
		return false;
	}

	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	testInt = 10 + 8;
	TestLog(testFlags, TXT("10 + 8 = %s"), testInt);
	if (testInt != 18)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 18.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt = 10 + initializedInt;
	TestLog(testFlags, TXT("10 + 15 = %s"), testInt);
	if (testInt != 25)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 25.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt++;
	TestLog(testFlags, TXT("Increment from 25 leads to:  %s"), testInt);
	if (testInt != 26)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 26.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	--testInt;
	TestLog(testFlags, TXT("Decrement from 26 leads to:  %s"), testInt);
	if (testInt != 25)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 25.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt = initializedInt - 5;
	TestLog(testFlags, TXT("15 - 5 = %s"), testInt);
	if (testInt != 10)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 10.  It's currently:  %s"),  {testInt.ToString()});
		return false;
	}

	testInt += 13;
	TestLog(testFlags, TXT("10 += 13 ---> %s"), testInt);
	if (testInt != 23)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 10.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt = initializedInt * 2;
	TestLog(testFlags, TXT("15 * 2 = %s"), testInt);
	if (testInt != 30)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 30.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt /= 3;
	TestLog(testFlags, TXT("30 /= 3 ---> %s"), testInt);
	if (testInt != 10)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 10.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt += ((20 - initializedInt) * 4) + 3;
	TestLog(testFlags, TXT("10 += ((20 - 15) * 4) + 3 results in:  %s"), testInt);
	if (testInt != 33)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 33.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt = 20;
	testInt %= 3;
	TestLog(testFlags, TXT("20 % 3 = %s"), testInt);
	if (testInt != 2)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 2.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Bitwise Operation"));
	testInt = 14;
	testInt = testInt >> 2;
	TestLog(testFlags, TXT("14 >> 2 = %s"), testInt);
	if (testInt != 3)
	{
		UnitTestError(testFlags, TXT("Bit shifting failed.  Expected value for testInt is 3.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt = 9;
	testInt = testInt << 2;
	TestLog(testFlags, TXT("9 << 2 = %s"), testInt);
	if (testInt != 36)
	{
		UnitTestError(testFlags, TXT("Bit shifting failed.  Expected value for testInt is 36.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt = 13;
	testInt &= 11;
	TestLog(testFlags, TXT("13 & 11 = %s"), testInt);
	if (testInt != 9)
	{
		UnitTestError(testFlags, TXT("Bitwise operation failed.  Expected value for testInt is 9.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt = 13;
	testInt = testInt | 3;
	TestLog(testFlags, TXT("13 | 3 = %s"), testInt);
	if (testInt != 15)
	{
		UnitTestError(testFlags, TXT("Bitwise operation failed.  Expected value for testInt is 15.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}

	testInt = 13;
	testInt ^= 3;
	TestLog(testFlags, TXT("13 ^ 3 = %s"), testInt);
	if (testInt != 14)
	{
		UnitTestError(testFlags, TXT("Bitwise operation failed.  Expected value for testInt is 14.  It's currently:  %s"), {testInt.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Typecasting"));
	TestLog(testFlags, TXT("INT to unsigned int check. . ."));
	testInt = -5;
	unsigned int testUnsignedInt = testInt.ToUnsignedInt32();
	TestLog(testFlags, TXT("INT(-5) converted to unsigned int is:  %s"), DString::MakeString(testUnsignedInt));
	if (testUnsignedInt != 0)
	{
		UnitTestError(testFlags, TXT("Typecasting failed.  Expected resulting unsigned int is:  0"));
		return false;
	}

	testInt = 9001;
	testUnsignedInt = testInt.ToUnsignedInt32();
	TestLog(testFlags, TXT("INT(9001) converted to unsigned int is:  %s"),DString::MakeString(testUnsignedInt));
	if (testUnsignedInt != 9001)
	{
		UnitTestError(testFlags, TXT("Typcasting failed.  Expected resulting unsigned int is:  9001"));
		return false;
	}

	TestLog(testFlags, TXT("Unsigned int to INT check. . ."));
	testUnsignedInt = 3456789012;
	testInt = testUnsignedInt;
	TestLog(testFlags, TXT("UnsignedInt(3456789012) to INT is:  %s.  Expected overflow"), testInt);
	if (testInt != INT_MAX)
	{
		UnitTestError(testFlags, TXT("Typcasting failed.  Expected resulting INT is:  %s"), {INT(INT_MAX).ToString()});
		return false;
	}

	testUnsignedInt = 1337;
	testInt = testUnsignedInt;
	TestLog(testFlags, TXT("UnsignedInt(1337) to INT is:  %s"), testInt);
	if (testInt != 1337)
	{
		UnitTestError(testFlags, TXT("Typecasting failed.  Expected resulting INT is:  1337"));
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("INT Streaming"));
	{
		testInt = 72981;
		INT secondInt = -45278;
		INT readTestInt = 0;
		INT readSecondInt = 0;
		DataBuffer data;
		data << testInt;
		data << secondInt;
		data >> readTestInt;
		data >> readSecondInt;
		if (readTestInt != testInt)
		{
			UnitTestError(testFlags, TXT("INT streaming failed.  After writing %s to data buffer, it recovered %s from it."), {testInt.ToString(), readTestInt.ToString()});
			return false;
		}

		if (readSecondInt != secondInt)
		{
			UnitTestError(testFlags, TXT("INT streaming failed.  After writing a second int (%s) to data buffer, it recovered %s from it."), {secondInt.ToString(), readSecondInt.ToString()});
			return false;
		}

		DataBuffer reversedBuffer(0, !DataBuffer::IsSystemLittleEndian());
		//Pushing ints in other endian format, should also be readable
		readTestInt = 0;
		readSecondInt = 0;
		reversedBuffer << testInt;
		reversedBuffer << secondInt;
		reversedBuffer >> readTestInt;
		reversedBuffer >> readSecondInt;
		if (readTestInt != testInt)
		{
			UnitTestError(testFlags, TXT("INT streaming failed.  After writing %s to a reversed data buffer, it recovered %s from it."), {testInt.ToString(), readTestInt.ToString()});
			return false;
		}

		if (readSecondInt != secondInt)
		{
			UnitTestError(testFlags, TXT("INT streaming failed.  After writing a second int (%s) to a reversed data buffer, it recovered %s from it."), {secondInt.ToString(), readSecondInt.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("INT Utils"));
	{
		testInt = 4;
		TestLog(testFlags, TXT("Is %s even?  %s"), testInt, BOOL(testInt.IsEven()));
		if (!testInt.IsEven() || testInt.IsOdd())
		{
			UnitTestError(testFlags, TXT("INT utils test failed.  %s should be considered an even number."), {testInt.ToString()});
			return false;
		}

		testInt++;
		TestLog(testFlags, TXT("Is %s odd?  %s"), testInt, BOOL(testInt.IsOdd()));
		if (!testInt.IsOdd() || testInt.IsEven())
		{
			UnitTestError(testFlags, TXT("INT utils test failed.  %s should be considered an odd number."), {testInt.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("INT numDigits"));
	{
		std::function<bool(INT, INT)> testNumDigits([&](INT testInt, INT expectedNumDigits)
		{
			INT actualNumDigits = testInt.NumDigits();
			bool passed = (actualNumDigits == expectedNumDigits);
			if (!passed)
			{
				UnitTestError(testFlags, TXT("INT's NumDigits test failed since the number of digits for %s should have been %s. Instead it's %s."), {testInt.ToString(), expectedNumDigits.ToString(), actualNumDigits.ToString()});
			}

			return passed;
		});

		if (!testNumDigits(4, 1))
		{
			return false;
		}

		if (!testNumDigits(40, 2))
		{
			return false;
		}

		if (!testNumDigits(128, 3))
		{
			return false;
		}

		if (!testNumDigits(0, 1))
		{
			return false;
		}

		if (!testNumDigits(-53982, 5))
		{
			return false;
		}

		if (!testNumDigits(-100000, 6))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Formatted String"));
	{
		std::function<bool(INT, INT, const DString&)> testFormattedString([&](INT testInt, INT numDigits, const DString& expectedString)
		{
			DString actualString = testInt.ToFormattedString(numDigits);
			bool passed = (actualString.Compare(expectedString, DString::CC_CaseSensitive) == 0);
			if (!passed)
			{
				UnitTestError(testFlags, TXT("INT's Formatted String test failed since the formatted string of a %s with %s number of digits should have returned \"%s\". Instead it returned \"%s\"."), {testInt.ToString(), numDigits.ToString(), expectedString, actualString});
			}

			return passed;
		});

		if (!testFormattedString(5, 1, TXT("5")))
		{
			return false;
		}

		if (!testFormattedString(5, 3, TXT("005")))
		{
			return false;
		}

		if (!testFormattedString(5, 16, TXT("0000000000000005")))
		{
			return false;
		}

		if (!testFormattedString(100, 2, TXT("100")))
		{
			return false;
		}

		if (!testFormattedString(100, 4, TXT("0100")))
		{
			return false;
		}

		if (!testFormattedString(0, 3, TXT("000")))
		{
			return false;
		}

		if (!testFormattedString(-16, 2, TXT("-16")))
		{
			return false;
		}

		if (!testFormattedString(-16, 4, TXT("-0016")))
		{
			return false;
		}

		if (!testFormattedString(-256, 1, TXT("-256")))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("INT"));
		
	return true;
}

bool DatatypeUnitTester::TestFLOAT (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("FLOAT"));

	FLOAT blankFloat(0.f);
	FLOAT initializedFloat(14.5f);
	FLOAT copiedFloat(initializedFloat);

	TestLog(testFlags, TXT("Constructor tests:  blankFloat = %s"), blankFloat);
	TestLog(testFlags, TXT("    initializedFloat(14.5) = %s"), initializedFloat);
	TestLog(testFlags, TXT("    copiedFloat from initializedInt = %s"), copiedFloat);

	SetTestCategory(testFlags, TXT("Comparison"));
	TestLog(testFlags, TXT("blankFloat != 0?  %s"), BOOL(blankFloat != 0));
	if (blankFloat != 0)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  blankFloat is not zero.  Instead it's:  %s"), {blankFloat.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("initializedFloat(%s) != copiedFloat(%s) ? %s"), initializedFloat, copiedFloat, BOOL(initializedFloat != copiedFloat));
	if (initializedFloat != copiedFloat)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedFloat != copiedInt.  Values are:  %s and %s"), {initializedFloat.ToString(), copiedFloat.ToString()});
		return false;
	}

	FLOAT testFloat = 5.95f;
	TestLog(testFlags, TXT("5.95 < 6 ? %s"), BOOL(testFloat < 6));
	if (!(testFloat < 6))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is not less than 6."), {testFloat.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("5.95 <= 5.95 ? %s"), BOOL(testFloat <= 5.95f));
	if (!(testFloat <= 5.95f))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is not less than or equal to 5.95."), {testFloat.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("5.95 > 3 ? %s"), BOOL(testFloat > 3));
	if (!(testFloat > 3))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is not greater than 3."), {testFloat.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("5.95 >= 7.2 ? %s"), BOOL(testFloat >= 7.2f));
	if (testFloat >= 7.2f)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  testFloat(%s) is greater than or equal to 7.2."), {testFloat.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("5.95 >= 3.14 && 5.95 < 7.8 && 5.95 == 5.95 && (5.95 == 10.05 || 5.95 != 9) ? %s"), BOOL(testFloat >= 3.14f && testFloat < 7.8f && testFloat == FLOAT(5.95f) && (testFloat == 10.05f || testFloat != INT(9).ToFLOAT())));
	if (!(testFloat >= 3.14f && testFloat < 7.8f && testFloat == FLOAT(5.95f) && (testFloat == 10.05f || testFloat != INT(9).ToFLOAT())))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testFloat is:  %s"), {testFloat.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	testFloat = 10.2 + 8.8;
	TestLog(testFlags, TXT("10.2 + 8.8 = %s"), testFloat);
	if (testFloat != 19)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testInt is 19.  It's currently:  %s"), {testFloat.ToString()});
		return false;
	}

	testFloat = 10.f + initializedFloat;
	TestLog(testFlags, TXT("10 + 14.5 = %s"), testFloat);
	if (testFloat != 24.5)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 24.5.  It's currently:  %s"), {testFloat.ToString()});
		return false;
	}

	testFloat++;
	TestLog(testFlags, TXT("Increment from 24.5 leads to:  %s"), testFloat);
	if (testFloat != 25.5)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 25.5.  It's currently:  %s"), {testFloat.ToString()});
		return false;
	}

	--testFloat;
	TestLog(testFlags, TXT("Decrement from 25.5 leads to:  %s"), testFloat);
	if (testFloat != 24.5)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 24.5.  It's currently:  %s"), {testFloat.ToString()});
		return false;
	}

	testFloat = initializedFloat - 1.11115f;
	TestLog(testFlags, TXT("14.5 - 1.11115 = %s"), testFloat);
	if (testFloat != 13.38885f)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 13.38885.  It's currently:  %s"), {testFloat.ToString()});
		return false;
	}

	testFloat += 13.794f;
	TestLog(testFlags, TXT("13.38885 += 13.794  ---> %s"), testFloat);
	if (testFloat != 27.18285f)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 27.18285.  It's currently:  %s"), {testFloat.ToString()});
		return false;
	}

	testFloat = initializedFloat * 2;
	TestLog(testFlags, TXT("14.5 * 2 = %s"), testFloat);
	if (testFloat != 29)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 29.  It's currently:  %s"), {testFloat.ToString()});
		return false;
	}

	testFloat /= 3;
	TestLog(testFlags, TXT("29 /=3  ---> %s"), testFloat);
	if (abs(testFloat.Value - 9.666667) > 0.000001)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is ~9.666667.  It's currently:  %s"), {testFloat.ToString()});
		return false;
	}

	testFloat = 10;
	testFloat += ((20.25f - 14.5f) * 4.f) + 3.1f;
	TestLog(testFlags, TXT("10 += ((20.25 - 14.5) * 4) + 3.1 results in:  %s"), testFloat);
	if (testFloat != 36.1f)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 36.1.  It's currently:  %s"), {testFloat.ToString()});
		return false;
	}

	testFloat = 20.5;
	testFloat %= 3;
	TestLog(testFlags, TXT("20.5 % 3 = %s"), testFloat);
	if (testFloat != 2.5)
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testFloat is 2.5.  It's currently:  %s"), {testFloat.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("IsCloseTo"));
	{
		std::function<bool(FLOAT, FLOAT, FLOAT, bool)> testIsCloseTo([&](FLOAT left, FLOAT right, FLOAT tolerance, bool expectedIsCloseTo)
		{
			bool actual = left.IsCloseTo(right, tolerance);
			if (actual != expectedIsCloseTo)
			{
				if (expectedIsCloseTo)
				{
					UnitTestError(testFlags, TXT("IsCloseTo test failed. %s is expected to be close to %s within %s tolerance."), {left.ToString(), right.ToString(), tolerance.ToString()});
				}
				else
				{
					UnitTestError(testFlags, TXT("IsCloseTo test failed. %s is NOT expected to be close to %s within %s tolerance."), {left.ToString(), right.ToString(), tolerance.ToString()});
				}
			}

			return (actual == expectedIsCloseTo);
		});

		FLOAT a(5.f);
		FLOAT b(5.f);

		if (!testIsCloseTo(a, b, FLOAT_TOLERANCE, true))
		{
			return false;
		}

		b = 5.5f;
		if (!testIsCloseTo(a, b, 0.001f, false))
		{
			return false;
		}

		if (!testIsCloseTo(b, a, 0.001f, false))
		{
			return false;
		}

		if (!testIsCloseTo(a, b, 1.f, true))
		{
			return false;
		}

		a *= -1.f;
		if (!testIsCloseTo(a, b, 1.f, false))
		{
			return false;
		}

		a = -10.f;
		b = -10.1f;
		if (!testIsCloseTo(a, b, 0.11f, true))
		{
			return false;
		}

		if (!testIsCloseTo(a, b, 0.09f, false))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Typcasting"));
	{
		testFloat = -5.84f;
		INT testInt = testFloat.ToINT();
		TestLog(testFlags, TXT("FLOAT(-5.84) converted to INT is:  %s"), testInt);
		if (testInt != -5)
		{
			UnitTestError(testFlags, TXT("Typecasting failed.  Expected resulting INT is:  -5"));
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("FLOAT Streaming"));
	{
		testFloat = 5942.184f;
		FLOAT secondFloat = -548.487f;
		FLOAT readTestFloat = 0.f;
		FLOAT readSecondFloat = 0.f;
		DataBuffer data;
		data << testFloat;
		data << secondFloat;
		data >> readTestFloat;
		data >> readSecondFloat;
		if (readTestFloat != testFloat)
		{
			UnitTestError(testFlags, TXT("FLOAT streaming failed.  After writing %s to data buffer, it recovered %s from it."), {testFloat.ToString(), readTestFloat.ToString()});
			return false;
		}

		if (readSecondFloat != secondFloat)
		{
			UnitTestError(testFlags, TXT("FLOAT streaming failed.  After writing a second float (%s) to data buffer, it recovered %s from it."), {secondFloat.ToString(), readSecondFloat.ToString()});
			return false;
		}

		DataBuffer reversedBuffer(0, !DataBuffer::IsSystemLittleEndian());
		//Pushing ints in other endian format, should also be readable
		readTestFloat = 0.f;
		readSecondFloat = 0.f;
		reversedBuffer << testFloat;
		reversedBuffer << secondFloat;
		reversedBuffer >> readTestFloat;
		reversedBuffer >> readSecondFloat;
		if (readTestFloat != testFloat)
		{
			UnitTestError(testFlags, TXT("FLOAT streaming failed.  After writing %s to a reversed data buffer, it recovered %s from it."), {testFloat.ToString(), readTestFloat.ToString()});
			return false;
		}

		if (readSecondFloat != secondFloat)
		{
			UnitTestError(testFlags, TXT("FLOAT streaming failed.  After writing a second float (%s) to a reversed data buffer, it recovered %s from it."), {secondFloat.ToString(), readSecondFloat.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("FLOAT Utils"));
	FLOAT minBounds = 0.f;
	FLOAT maxBounds = 10.f;
	testFloat = 0.5f;
	TestLog(testFlags, TXT("Lerping %s between %s and %s yields %s"), testFloat, minBounds, maxBounds, Utils::Lerp(testFloat, minBounds, maxBounds));
	if (Utils::Lerp(testFloat, minBounds, maxBounds) != 5.f)
	{
		UnitTestError(testFlags, TXT("Lerping test failed.  Lerping %s between %s and %s should have returned 5.  Instead it returned %s"), {testFloat.ToString(), minBounds.ToString(), maxBounds.ToString(), Utils::Lerp(testFloat, minBounds, maxBounds).ToString()});
		return false;
	}

	minBounds = 10.f;
	maxBounds = 1010.f;
	testFloat = 0.75f;
	TestLog(testFlags, TXT("Lerping %s between %s and %s yields %s"), testFloat, minBounds, maxBounds, Utils::Lerp(testFloat, minBounds, maxBounds));
	if (Utils::Lerp(testFloat, minBounds, maxBounds) != 760.f)
	{
		UnitTestError(testFlags, TXT("Lerping test failed.  Lerping %s between %s and %s should have returned 760.  Instead it returned %s"), {testFloat.ToString(), minBounds.ToString(), maxBounds.ToString(), Utils::Lerp(testFloat, minBounds, maxBounds).ToString()});
		return false;
	}

	testFloat = 10.75f;
	TestLog(testFlags, TXT("Rounding %s results in %s"), testFloat, FLOAT::Round(testFloat));
	if (FLOAT::Round(testFloat) != 11.f)
	{
		UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::Round(testFloat).ToString()});
		return false;
	}

	testFloat = 10.5f;
	TestLog(testFlags, TXT("Rounding %s results in %s"), testFloat, FLOAT::Round(testFloat));
	if (FLOAT::Round(testFloat) != 11.f)
	{
		UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::Round(testFloat).ToString()});
		return false;
	}

	testFloat = 10.25f;
	TestLog(testFlags, TXT("Rounding %s results in %s"), testFloat, FLOAT::Round(testFloat));
	if (FLOAT::Round(testFloat) != 10.f)
	{
		UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::Round(testFloat).ToString()});
		return false;
	}

	testFloat = 10.75f;
	TestLog(testFlags, TXT("Rounding up from %s results in %s"), testFloat, FLOAT::RoundUp(testFloat));
	if (FLOAT::RoundUp(testFloat) != 11.f)
	{
		UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundUp(testFloat).ToString()});
		return false;
	}

	testFloat = 10.5f;
	TestLog(testFlags, TXT("Rounding up from %s results in %s"), testFloat, FLOAT::RoundUp(testFloat));
	if (FLOAT::RoundUp(testFloat) != 11.f)
	{
		UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundUp(testFloat).ToString()});
		return false;
	}

	testFloat = 10.25f;
	TestLog(testFlags, TXT("Rounding up from %s results in %s"), testFloat, FLOAT::RoundUp(testFloat));
	if (FLOAT::RoundUp(testFloat) != 11.f)
	{
		UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 11.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundUp(testFloat).ToString()});
		return false;
	}

	testFloat = 10.75f;
	TestLog(testFlags, TXT("Rounding down from %s results in %s"), testFloat, FLOAT::RoundDown(testFloat));
	if (FLOAT::RoundDown(testFloat) != 10.f)
	{
		UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundDown(testFloat).ToString()});
		return false;
	}

	testFloat = 10.5f;
	TestLog(testFlags, TXT("Rounding down from %s results in %s"), testFloat, FLOAT::RoundDown(testFloat));
	if (FLOAT::RoundDown(testFloat) != 10.f)
	{
		UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundDown(testFloat).ToString()});
		return false;
	}

	testFloat = 10.25f;
	TestLog(testFlags, TXT("Rounding down from %s results in %s"), testFloat, FLOAT::RoundDown(testFloat));
	if (FLOAT::RoundDown(testFloat) != 10.f)
	{
		UnitTestError(testFlags, TXT("Rounding test failed.  Rounding %s should have resulted in 10.f.  Instead it returned %s."), {testFloat.ToString(), FLOAT::RoundDown(testFloat).ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Formatted String"));
	{
		const std::function<bool(FLOAT, INT, INT, FLOAT::eTruncateMethod, const DString&)> testFormatString([&](FLOAT originalFloat, INT minDigits, INT numDecimals, FLOAT::eTruncateMethod rounding, const DString& expectedString)
		{
			const DString formattedStr = originalFloat.ToFormattedString(minDigits, numDecimals, rounding);
			if (formattedStr.Compare(expectedString, DString::CC_CaseSensitive) != 0)
			{
				DString truncateMethodStr;
				switch (rounding)
				{
					case(FLOAT::eTruncateMethod::TM_RoundDown):
						truncateMethodStr = TXT("Round Down");
						break;

					case(FLOAT::eTruncateMethod::TM_Round):
						truncateMethodStr = TXT("Round");
						break;

					case(FLOAT::eTruncateMethod::TM_RoundUp):
						truncateMethodStr = TXT("Round Up");
						break;
				};

				UnitTestError(testFlags, TXT("FLOAT formatted string test failed. The formatted string of a %s (where minDigits=%s, numDecimals=%s and truncateMethod=%s) is expected to be %s. Instead it's %s."), {originalFloat.ToString(), minDigits.ToString(), numDecimals.ToString(), truncateMethodStr, expectedString, formattedStr});
				return false;
			}

			return true;
		});

		FLOAT originalFloat = 1.753f;
		if (!testFormatString(originalFloat, 1, 2, FLOAT::eTruncateMethod::TM_Round, TXT("1.75")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 1, 2, FLOAT::eTruncateMethod::TM_RoundUp, TXT("1.76")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 1, 1, FLOAT::eTruncateMethod::TM_Round, TXT("1.8")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 1, 1, FLOAT::eTruncateMethod::TM_RoundDown, TXT("1.7")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 1, 6, FLOAT::eTruncateMethod::TM_Round, TXT("1.753000")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 6, 6, FLOAT::eTruncateMethod::TM_Round, TXT("1.753000")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 10, 6, FLOAT::eTruncateMethod::TM_Round, TXT("0001.753000")))
		{
			return false;
		}

		originalFloat *= -1.f;
		if (!testFormatString(originalFloat, 10, 6, FLOAT::eTruncateMethod::TM_Round, TXT("-0001.753000")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 5, 2, FLOAT::eTruncateMethod::TM_Round, TXT("-001.75")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 2, 6, FLOAT::eTruncateMethod::TM_RoundUp, TXT("-1.753000")))
		{
			return false;
		}

		if (!testFormatString(originalFloat, 1, 1, FLOAT::eTruncateMethod::TM_RoundDown, TXT("-1.7")))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("FLOAT"));
		
	return true;
}

bool DatatypeUnitTester::TestDPointer (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("DPointer"));

	SetTestCategory(testFlags, TXT("DPointer Comparison"));
	DPointer<DPointerTester> pointer = DPointerTester::CreateObject();
	DPointer<DPointerTester> pointed = DPointerTester::CreateObject();
	pointer->OtherTester = pointed;

	if (pointed->OtherTester != nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to initialize DPointerTester.  The pointer variable should have initialized to nullptr."));
		pointer->Destroy();
		pointed->Destroy();
		return false;
	}

	if (pointer->OtherTester == nullptr || pointer->OtherTester != pointed)
	{
		UnitTestError(testFlags, TXT("Failed to initialize DPointerTester.  The pointer variable should be equal to the pointed object."));
		pointer->Destroy();
		pointed->Destroy();
		return false;
	}

	DPointer<DPointerTester> accessor = pointer;
	if (accessor->OtherTester != pointed)
	{
		UnitTestError(testFlags, TXT("DPointerTest failed.  Could not receive a copy of the pointer."));
		pointer->Destroy();
		pointed->Destroy();
		return false;
	}

	DPointer<PrimitivePointerTester> primTester = new PrimitivePointerTester();
	primTester->OtherTester = pointed;
	PrimitivePointerTester& testerByRef = *primTester;
	if (testerByRef != *primTester.Get()) //Ensure that both are pointing to same memory address
	{
		UnitTestError(testFlags, TXT("DPointerTest failed.  Comparing DPointerTester by reference should have been the equal to the original pointed object."));
		pointer->Destroy();
		pointed->Destroy();
		delete primTester.Get();
		return false;
	}
	delete primTester.Get();

	DPointerTester* rawPointer = pointer.Get();
	if (rawPointer->OtherTester != pointer->OtherTester)
	{
		UnitTestError(testFlags, TXT("DPointerTest failed.  Comparing the DPointerTester by raw pointer should have been equal to the original pointed object."));
		pointer->Destroy();
		pointed->Destroy();
		return false;
	}
	CompleteTestCategory(testFlags);

	//Need to test copying and assigning DPointers around to ensure that their linked lists are maintained.
	SetTestCategory(testFlags, TXT("Pointer Assignment"));
	DPointer<DPointerTester> pointer1 = pointed;
	DPointer<DPointerTester> pointer2 = pointed;
	DPointer<DPointerTester> pointer3 = pointed;

	std::vector<DPointerBase*> expectedPointers;
	expectedPointers.push_back(pointer->OtherTester.GetPointerAddress());
	expectedPointers.push_back(pointer1.GetPointerAddress());
	expectedPointers.push_back(pointer2.GetPointerAddress());
	expectedPointers.push_back(pointer3.GetPointerAddress());

	for (UINT_TYPE i = 0; i < expectedPointers.size(); i++)
	{
		int numMatchingPointers = 0;

		//Each pointer in the object's pointer list should be in list exactly once
		for (DPointerBase* curPointer = pointed->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
		{
			if (curPointer->GetPointerAddress() == expectedPointers.at(i)->GetPointerAddress())
			{
				numMatchingPointers++;
			}
		}

		if (numMatchingPointers != 1)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed.  Each pointer within the object's linked list should appear exactly once.  Instead it was found %s times."), {DString::MakeString(numMatchingPointers)});
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}
	}

	//shuffle pointers around
	pointer1 = nullptr;
	pointer2 = pointer3; //Point to pointed through pointer3
	pointer3 = pointer; //Point from pointed to pointer
	expectedPointers.clear();
	expectedPointers.push_back(pointer->OtherTester.GetPointerAddress());
	expectedPointers.push_back(pointer2.GetPointerAddress());

	std::vector<DPointerBase*> unexpectedPointers;
	unexpectedPointers.push_back(pointer1.GetPointerAddress());
	unexpectedPointers.push_back(pointer3.GetPointerAddress());
	unexpectedPointers.push_back(pointed->OtherTester.GetPointerAddress());

	//Validate Pointed's internal pointer linked list
	for (UINT_TYPE i = 0; i < expectedPointers.size(); i++)
	{
		int numMatchingPointers = 0;
		for (DPointerBase* curPointer = pointed->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
		{
			if (curPointer->GetPointerAddress() == expectedPointers.at(i)->GetPointerAddress())
			{
				numMatchingPointers++;
			}
		}

		if (numMatchingPointers != 1)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed after shuffling pointers around.  Each pointer within the object's linked list should appear exactly once.  Instead it was found %s times."), {DString::MakeString(numMatchingPointers)});
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}
	}

	for (UINT_TYPE i = 0; i < unexpectedPointers.size(); i++)
	{
		int numMatchingPointers = 0;
		for (DPointerBase* curPointer = pointed->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
		{
			if (curPointer == unexpectedPointers.at(i))
			{
				numMatchingPointers++;
			}
		}

		if (numMatchingPointers != 0)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed after shuffling pointers around.  The pointer within the object's linked list should have been removed after the pointer was assigned to some other Object.  It was found %s times."), {DString::MakeString(numMatchingPointers)});
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}
	}

	//Validate Pointer's internal pointer linked list
	expectedPointers.clear();
	expectedPointers.push_back(pointer3.GetPointerAddress());

	unexpectedPointers.clear();
	unexpectedPointers.push_back(pointer->OtherTester.GetPointerAddress());
	unexpectedPointers.push_back(pointer1.GetPointerAddress());
	unexpectedPointers.push_back(pointer2.GetPointerAddress());
	unexpectedPointers.push_back(pointed->OtherTester.GetPointerAddress());

	for (UINT_TYPE i = 0; i < expectedPointers.size(); i++)
	{
		int numMatchingPointers = 0;
		for (DPointerBase* curPointer = pointer->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
		{
			if (curPointer->GetPointerAddress() == expectedPointers.at(i)->GetPointerAddress())
			{
				numMatchingPointers++;
			}
		}

		if (numMatchingPointers != 1)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed after shuffling pointers around.  Each pointer within the object's linked list should appear exactly once.  Instead it was found %s times."), {DString::MakeString(numMatchingPointers)});
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}
	}

	for (UINT_TYPE i = 0; i < unexpectedPointers.size(); i++)
	{
		int numMatchingPointers = 0;
		for (DPointerBase* curPointer = pointer->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
		{
			if (curPointer->GetPointerAddress() == unexpectedPointers.at(i)->GetPointerAddress())
			{
				numMatchingPointers++;
			}
		}

		if (numMatchingPointers != 0)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed after shuffling pointers around.  The pointer within the object's linked list should have been removed after the pointer was assigned to some other Object.  It was found %s times."), {DString::MakeString(numMatchingPointers)});
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}
	}

	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Clearing Pointers"));
	//Test assigning DPointers to null
	DPointer<DPointerTester> clearPointerTester = pointed;
	DPointer<DPointerTester> nullPointerTester = pointed;
	DPointerBase* pointerAddress = &clearPointerTester;

	clearPointerTester.ClearPointer();
	if (clearPointerTester != nullptr)
	{
		UnitTestError(testFlags, TXT("DPointerTest failed after clearing a pointer.  After clearing a pointer, the reference to its pointed object should have been cleared, however the pointer should still exist."));
		pointer->Destroy();
		pointed->Destroy();
		return false;
	}

	//Verify that pointed lost its reference to clearPointerTester
	for (DPointerBase* curPointer = pointed->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
	{
		if (curPointer == pointerAddress)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed.  After clearing the pointer, the reference to that pointer should have been removed from the Object's linked list."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}
	}

	pointerAddress = &nullPointerTester;
	nullPointerTester = nullptr;
	if (nullPointerTester != nullptr)
	{
		UnitTestError(testFlags, TXT("DPointerTest failed.  The pointer is not nullptr after assigning it to nullptr."));
		pointer->Destroy();
		pointed->Destroy();
		return false;
	}

	//Verify that pointed lost its reference to nullPointerTester
	for (DPointerBase* curPointer = pointed->GetLeadingDPointer(); curPointer != nullptr; curPointer = curPointer->GetNextPointer())
	{
		if (curPointer == pointerAddress)
		{
			UnitTestError(testFlags, TXT("DPointerTest failed.  After assigning the pointer to null, the reference to that pointer should have been removed from the Object's linked list."));
			pointer->Destroy();
			pointed->Destroy();
			return false;
		}
	}


	pointer->OtherTester = pointed;
	pointed->OtherTester = pointer; //test circular dependency
	pointer1 = pointed;
	pointer2 = pointer;
	pointer3 = pointer;

	//expected pointers to clear
	expectedPointers.clear();
	expectedPointers.push_back(pointer->OtherTester.GetPointerAddress());
	expectedPointers.push_back(pointer1.GetPointerAddress());

	//pointers expected to still point to something
	unexpectedPointers.clear();
	unexpectedPointers.push_back(pointer2.GetPointerAddress());
	unexpectedPointers.push_back(pointer3.GetPointerAddress());

	pointed->Destroy(); //should set all pointers that are pointing at this object to nullptr

	for (UINT_TYPE i = 0; i < expectedPointers.size(); i++)
	{
		if (*expectedPointers.at(i) != nullptr)
		{
			UnitTestError(testFlags, TXT("DPointer test failed.  After destroying an object, all pointers pointing at that object should have been set to nullptr."));
			pointer->Destroy();
			return false;
		}
	}

	for (UINT_TYPE i = 0; i < unexpectedPointers.size(); i++)
	{
		if (*unexpectedPointers.at(i) == nullptr)
		{
			UnitTestError(testFlags, TXT("DPointer test failed.  After destroying an object, all pointers pointing to a different object should still retain their values."));
			pointer->Destroy();
			return false;
		}
	}

	expectedPointers.clear();
	expectedPointers.push_back(pointer1.GetPointerAddress());
	expectedPointers.push_back(pointer2.GetPointerAddress());
	expectedPointers.push_back(pointer3.GetPointerAddress());

	//clear remaining pointers
	pointer->Destroy();

	for (UINT_TYPE i = 0; i < expectedPointers.size(); i++)
	{
		if (*expectedPointers.at(i) != nullptr)
		{
			UnitTestError(testFlags, TXT("DPointer test failed.  After destroying the second object, the remaining pointers should have been set to nullptr."));
			return false;
		}
	}

	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("DPointer"));
	
	return true;
}

bool DatatypeUnitTester::TestRange (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Range"));

	Range<INT> blankRange;
	Range<INT> initializedRange(0, 100);
	Range<INT> copiedRange(initializedRange);

	TestLog(testFlags, TXT("Constructor tests:  blankRange= %s"), blankRange);
	TestLog(testFlags, TXT("    initializedRange(0,100)= %s"), initializedRange);
	TestLog(testFlags, TXT("    copiedRange from initializedRange= %s"), copiedRange);

	SetTestCategory(testFlags, TXT("Comparison"));
	Range<INT> comparisonRange(25, 100);
	TestLog(testFlags, TXT("initializedRange(%s) == comparisonRange(%s)?  %s"), initializedRange, comparisonRange, BOOL(initializedRange == comparisonRange));
	if (initializedRange == comparisonRange)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedRange %s should not be equal to %s."), {initializedRange.ToString(), comparisonRange.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("initializedRange(%s) == copiedRange(%s) ? %s"), initializedRange, copiedRange, BOOL(initializedRange == copiedRange));
	if (initializedRange != copiedRange)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedRange %s != copiedRange %s."), {initializedRange.ToString(), copiedRange.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Range Streaming"));
	{
		Range<INT> streamIntRange(4, 15);
		Range<FLOAT> streamFloatRange(-48.3f, 81.6f);
		Range<INT> readStreamIntRange;
		Range<FLOAT> readStreamFloatRange;
		DataBuffer rangeBuffer;

		rangeBuffer << streamIntRange;
		rangeBuffer << streamFloatRange;
		rangeBuffer >> readStreamIntRange;
		rangeBuffer >> readStreamFloatRange;

		if (streamIntRange != readStreamIntRange)
		{
			UnitTestError(testFlags, TXT("Streaming int range test failed.  The range %s was pushed to data buffer.  %s was pulled from data buffer."), {streamIntRange.ToString(), readStreamIntRange.ToString()});
			return false;
		}

		if (streamFloatRange != readStreamFloatRange)
		{
			UnitTestError(testFlags, TXT("Streaming float range test failed.  The range %s was pushed to data buffer.  %s was pulled from data buffer."), {streamFloatRange.ToString(), readStreamFloatRange.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utility"));
	Range<INT> testRange(0, 100);
	Range<INT> otherRange(50, 125);
	TestLog(testFlags, TXT("Does %s intersect %s?  %s"), testRange, otherRange, BOOL(testRange.Intersects(otherRange)));
	if (!testRange.Intersects(otherRange))
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  %s should be intersecting %s."), {testRange.ToString(), otherRange.ToString()});
		return false;
	}

	otherRange.Min = 125;
	otherRange.Max = 200;
	TestLog(testFlags, TXT("Does %s intersect %s?  %s"), testRange, otherRange, BOOL(testRange.Intersects(otherRange)));
	if (testRange.Intersects(otherRange))
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be intersecting %s."), {testRange.ToString(), otherRange.ToString()});
		return false;
	}

	otherRange.Min = 50;
	otherRange.Max = 200;
	Range<INT> intersection = testRange.GetIntersectionFrom(otherRange);
	TestLog(testFlags, TXT("The intersection of %s and %s is %s"), testRange, otherRange, intersection);
	if (intersection != Range<INT>(50, 100))
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  The intersection of %s and %s should be 50-100.  Instead it returned %s."), {testRange.ToString(), otherRange.ToString(), intersection.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Is %s completely within %s?  %s"), testRange, otherRange, BOOL(testRange.IsWithin(otherRange)));
	if (testRange.IsWithin(otherRange))
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be completely within %s."), {testRange.ToString(), otherRange.ToString()});
		return false;
	}

	otherRange.Min = -100;
	otherRange.Max = 200;
	TestLog(testFlags, TXT("Is %s completely within %s?  %s"), testRange, otherRange, BOOL(testRange.IsWithin(otherRange)));
	if (!testRange.IsWithin(otherRange))
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  %s should be completely within %s."), {testRange.ToString(), otherRange.ToString()});
		return false;
	}

	otherRange.Min = 100;
	otherRange.Max = 0;
	TestLog(testFlags, TXT("Is %s inverted?  %s"), otherRange, BOOL(otherRange.IsInverted()));
	if (!otherRange.IsInverted())
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  %s should be considered inverted."), {otherRange.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("...Fixing %s inversion..."), otherRange);
	otherRange.FixRange(); //This should fix inverted ranges
	TestLog(testFlags, TXT("Is %s inverted?  %s"), otherRange, BOOL(otherRange.IsInverted()));
	if (otherRange.IsInverted())
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be inverted."), {otherRange.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Does %s contain 25? %s"), testRange, BOOL(testRange.ContainsValue(25)));
	if (!testRange.ContainsValue(25))
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  %s should be containing 25"), {testRange.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Does %s contain 125?  %s"), testRange, BOOL(testRange.ContainsValue(125)));
	if (testRange.ContainsValue(125))
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  %s should not be containing 125"), {testRange.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("The difference of %s is:  %s"), testRange, testRange.Difference());
	if (testRange.Difference() != 100)
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  The expected value of the difference of %s is 100."), {testRange.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("The center of %s is:  %s"), testRange, testRange.Center());
	if (testRange.Center() != 50)
	{
		UnitTestError(testFlags, TXT("Utility functions failed.  The expected value of the center of %s is 50."), {testRange.ToString()});
		return false;
	}
		
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Range"));

	return true;
}

bool DatatypeUnitTester::TestVector2 (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Vector2"));

	Vector2 blankVector = Vector2::ZeroVector;
	Vector2 initializedVector(1, 2);
	Vector2 copiedVector(initializedVector);
	Vector2 typecastedVector(Vector3(10,20,30).ToVector2());

	TestLog(testFlags, TXT("Constructor tests:  BlankVector=%s"), blankVector);
	TestLog(testFlags, TXT("    initializedVector(1,2)= %s"), initializedVector);
	TestLog(testFlags, TXT("    copiedVector from initializedVector= %s"), copiedVector);
	TestLog(testFlags, TXT("    typecastedVector from Vector3(10,20,30)= %s"), typecastedVector);

	SetTestCategory(testFlags, TXT("Comparison"));
	TestLog(testFlags, TXT("Checking initializedVector axis. . ."));
	if (initializedVector[0] != 1.f || initializedVector[1] != 2.f)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  Failed to retrieve individual axis of initializedVector(%s).  It returned %s and %s instead of 1 and 2."), {initializedVector.ToString(), initializedVector[0].ToString(), initializedVector[1].ToString()});
		return false;
	}

	TestLog(testFlags, TXT("initializedVector != copiedVector ? %s"), BOOL(initializedVector != copiedVector));
	if (initializedVector != copiedVector)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedVector != copiedVector.  Values are:  %s and %s"), {initializedVector.ToString(), copiedVector.ToString()});
		return false;
	}

	Vector2 testVector(5, 10);
	TestLog(testFlags, TXT("<5,10> != <1,2> && (<5,10> == <99,99> || <5,10> == <5,10>) ? %s"), BOOL(testVector != copiedVector && (testVector == Vector2(99,99) || testVector == Vector2(5,10))));
	if (!(testVector != copiedVector && (testVector != Vector2(99,99) || testVector == Vector2(5,10))))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testVector is:  %s"), {testVector.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("<5, 10> == <5, 15> ? %s"), BOOL(testVector == Vector2(5, 15)));
	if (testVector == Vector2(5, 15))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have been false.  Instead it returned true."));
		return false;
	}

	TestLog(testFlags, TXT("<5, 10> != <5.025, 10> ? %s"), BOOL(testVector == Vector2(5.025f, 10)));
	if (testVector == Vector2(5.025f, 10))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have been false.  Instead it returned true."));
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	testVector = Vector2(1,2) + Vector2(5,9);
	TestLog(testFlags, TXT("<1,2> + <5,9> = %s"), testVector);
	if (testVector != Vector2(6,11))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <6,11>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector = copiedVector - Vector2(4, 10);
	TestLog(testFlags, TXT("<1,2> - <4,10> = %s"), testVector);
	if (testVector != Vector2(-3,-8))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <-3,-8>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector += Vector2(4,6);
	TestLog(testFlags, TXT("<-3,-8> += <4,6>  ---> %s"), testVector);
	if (testVector != Vector2(1,-2))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1,-2>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector = Vector2(5.f, 8.f) * 2.f;
	TestLog(testFlags, TXT("<5,8> * 2 = %s"), testVector);
	if (testVector != Vector2(10,16))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <10,16>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector /= 8.f;
	TestLog(testFlags, TXT("<10,16> /=8  ---> %s"), testVector);
	if (testVector != Vector2(1.25, 2))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1.25,2>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector *= 10.f;
	TestLog(testFlags, TXT("<1.25,2> *=10  ---> %s"), testVector);
	if (testVector != Vector2(12.5, 20))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <12.5,20>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector += ((Vector2(5.f,5.f) - copiedVector) * 4.f) + Vector2(-10.f,-15.f);
	TestLog(testFlags, TXT("<12.5,20> += ((<5,5> - <1,2>) * 4) + <-10,-15>    results in:  %s"), testVector);
	if (testVector != Vector2(18.5, 17))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <18.5,17>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Vector2 firstVector(2.4f, -48.15f);
		Vector2 secondVector(-51.9f, 104.7f);
		Vector2 readFirstVector;
		Vector2 readSecondVector;
		DataBuffer vectorBuffer;
		
		vectorBuffer << firstVector;
		vectorBuffer << secondVector;
		vectorBuffer >> readFirstVector;
		vectorBuffer >> readSecondVector;

		if (firstVector != readFirstVector)
		{
			UnitTestError(testFlags, TXT("Vector streaming test failed.  The vector %s was pushed to data buffer.  The vector %s was pulled from that data buffer."), {firstVector.ToString(), readFirstVector.ToString()});
			return false;
		}

		if (secondVector != readSecondVector)
		{
			UnitTestError(testFlags, TXT("Vector streaming test failed.  The 2nd vector %s was pushed to data buffer.  The 2nd vector %s was pulled from that data buffer."), {secondVector.ToString(), readSecondVector.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utility"));
	testVector = Vector2(5,5);
	TestLog(testFlags, TXT("Vector Length of <5,5> is:  %s"), testVector.VSize());
	if (abs(testVector.VSize().Value - 7.071067812) > 0.000001)
	{
		UnitTestError(testFlags, TXT("VSize function failed.  Expected length of vector<5,5> is ~7.071067812.  Expected precision is:  10^-6."));
		return false;
	}

	testVector.Normalize();
	TestLog(testFlags, TXT("Normalize<5,5> results in:  %s"), testVector);
	if (testVector.X - 0.70710678f < 0.000001f && testVector.Y - 0.70710678f > 0.000001f)
	{
		UnitTestError(testFlags, TXT("Normalize vector function failed.  Expected resulting vector is:  ~<0.707107, 0.707107> with minimum precision of 10^-6."));
		return false;
	}

	testVector = Vector2(0, 5);
	testVector.SetLengthTo(3.5);
	TestLog(testFlags, TXT("Setting Length of <0,5> to 3.5 results in:  %s"), testVector);
	if (testVector.VSize() != 3.5f || testVector != Vector2(0, 3.5f))
	{
		UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<0,5> to 3.5 should have resulted in <0,3.5>.  The VSize of %s is %s instead."), {testVector.ToString(), testVector.VSize().ToString()});
		return false;
	}

	testVector = Vector2(-191, 238);
	testVector.SetLengthTo(10.f);
	TestLog(testFlags, TXT("Setting length of <-191,238> to 10 results in:  %s"), testVector);
	if (FLOAT::Abs(testVector.VSize() - 10.f) > 0.000001f || (testVector - Vector2(-6.258932f, 7.799088f)).VSize() > 0.000001f)
	{
		UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<-191,238> to 10 should have resulted in ~<-6.258932,7.799088>.  The VSize of %s is %s instead."), {testVector.ToString(), testVector.VSize().ToString()});
		return false;
	}

	{
		TestLog(testFlags, TXT("Testing Vector2 clamp functions (min, max, and clamp)..."));
		std::function<bool(const Vector2&, const Vector2&)> compareVec = [&](const Vector2& actual, const Vector2& expected)
		{
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Clamp test failed since the actual vector (%s) does not match the expected (%s)."), {actual.ToString(), expected.ToString()});
				return false;
			}

			return true;
		};

		Vector2 originalVector(200.f, -100.f);
		Vector2 clampVector(0.f, 0.f);
		Vector2 resultingVector = Vector2::Min(originalVector, clampVector);
		Vector2 expectedVector(0.f, -100.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		clampVector = Vector2(400.f, 150.f);
		resultingVector = Vector2::Min(originalVector, clampVector);
		expectedVector = originalVector;
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		clampVector = Vector2(-200.f, 200.f);
		resultingVector = Vector2::Min(originalVector, clampVector);
		expectedVector = Vector2(-200.f, -100.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		resultingVector = Vector2::Max(originalVector, clampVector);
		expectedVector = Vector2(200.f, 200.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		clampVector = Vector2(250.f, -300.f);
		resultingVector = Vector2::Max(originalVector, clampVector);
		expectedVector = Vector2(250.f, -100.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		Vector2 minVec(0.f, 0.f);
		Vector2 maxVec(100.f, 100.f);
		resultingVector = Vector2::Clamp(originalVector, minVec, maxVec);
		expectedVector = Vector2(100.f, 0.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		minVec = Vector2(-1000.f, 1000.f);
		maxVec = Vector2(-500.f, 5000.f);
		resultingVector = Vector2::Clamp(originalVector, minVec, maxVec);
		expectedVector = Vector2(-500.f, 1000.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		minVec = Vector2(100.f, -300.f);
		maxVec = Vector2(300.f, 100.f);
		resultingVector = Vector2::Clamp(originalVector, minVec, maxVec);
		expectedVector = originalVector;
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}
	}

	testVector = Vector2(5,5);
	Vector2 otherVector = Vector2(-11,-13);
	TestLog(testFlags, TXT("<5,5> dot <-11,-13> = %s"), testVector.Dot(otherVector));
	if (testVector.Dot(otherVector) != -120)
	{
		UnitTestError(testFlags, TXT("Dot product of vectors %s and %s failed.  Expected value is -120."), {testVector.ToString(), otherVector.ToString()});
		return false;
	}

	Vector2 minBounds = Vector2(0, 0);
	Vector2 maxBounds = Vector2(100, 1000);
	FLOAT alpha = 0.5f;
	Vector2 lerpVector = Vector2::Lerp(alpha, minBounds, maxBounds);
	TestLog(testFlags, TXT("Lerping Vector with minBounds=%s and maxBounds=%s with alpha %s resulted in %s."), testVector, minBounds, maxBounds, alpha, lerpVector);
	if (lerpVector != Vector2(50.f, 500.f))
	{
		UnitTestError(testFlags, TXT("Vector2 Lerp test failed.  When lerping %s and %s with alpha %s, the expected vector is (50, 500).  Instead it returned %s."), {minBounds.ToString(), maxBounds.ToString(), alpha.ToString(), lerpVector.ToString()});
		return false;
	}

	alpha = 0.9f;
	lerpVector = Vector2::Lerp(alpha, minBounds, maxBounds);
	if (lerpVector != Vector2(90.f, 900.f))
	{
		UnitTestError(testFlags, TXT("Vector2 Lerp test failed.  When lerping %s and %s with alpha %s, the expected vector is (90, 900).  Instead it returned %s."), {minBounds.ToString(), maxBounds.ToString(), alpha.ToString(), lerpVector.ToString()});
		return false;
	}

	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Vector2"));

	return true;
}

bool DatatypeUnitTester::TestVector3 (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Vector3"));

	Vector3 blankVector = Vector3::ZeroVector;
	Vector3 initializedVector(1, 2, 3);
	Vector3 copiedVector(initializedVector);
	Vector3 typecastedVector(Vector2(10,20).ToVector3());

	TestLog(testFlags, TXT("Constructor tests:  BlankVector=%s"), blankVector);
	TestLog(testFlags, TXT("    initializedVector(1,2,3)= %s"), initializedVector);
	TestLog(testFlags, TXT("    copiedVector from initializedVector= %s"), copiedVector);
	TestLog(testFlags, TXT("    typecastedVector from Vector2(10,20)= %s"), typecastedVector);

	SetTestCategory(testFlags, TXT("Comparison"));
	TestLog(testFlags, TXT("Checking initializedVector axis. . ."));
	if (initializedVector[0] != 1.f || initializedVector[1] != 2.f || initializedVector[2] != 3.f)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  Failed to retrieve individual axis of initializedVector(%s).  It returned %s, %s, and %s instead of 1, 2, 3."), {initializedVector.ToString(), initializedVector[0].ToString(), initializedVector[1].ToString(), initializedVector[2].ToString()});
		return false;
	}

	TestLog(testFlags, TXT("initializedVector != copiedVector ? %s"), BOOL(initializedVector != copiedVector));
	if (initializedVector != copiedVector)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedVector != copiedVector.  Values are:  %s and %s"), {initializedVector.ToString(), copiedVector.ToString()});
		return false;
	}

	Vector3 testVector(5, 10, 15);
	TestLog(testFlags, TXT("<5,10,15> != <1,2,3> && (<5,10,15> == <99,99,15> || <5,10,15> == <5,10,15>) ? %s"), BOOL(testVector != copiedVector && (testVector == Vector3(99,99,15) || testVector == Vector3(5,10,15))));
	if (!(testVector != copiedVector && (testVector != Vector3(99,99,15) || testVector == Vector3(5,10,15))))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  The condition logged above should have returned true.  testVector is:  %s"), {testVector.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	testVector = copiedVector + Vector3(5,9,12);
	TestLog(testFlags, TXT("<1,2,3> + <5,9,12> = %s"), testVector);
	if (testVector != Vector3(6,11,15))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <6,11,15>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector = copiedVector - Vector3(4, 10, 2);
	TestLog(testFlags, TXT("<1,2,3> - <4,10,2> = %s"), testVector);
	if (testVector != Vector3(-3,-8,1))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <-3,-8,1>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector += Vector3(4,6,9);
	TestLog(testFlags, TXT("<-3,-8,1> += <4,6,9>  ---> %s"), testVector);
	if (testVector != Vector3(1,-2,10))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1,-2,10>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector += Vector2(4, -4).ToVector3();
	TestLog(testFlags, TXT("<1,-2,10> += <4,-4>"));
	if (testVector != Vector3(5, -6, 10))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <5, -6, 10>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector = Vector3(5.f, 8.f, 6.f) * 2.f;
	TestLog(testFlags, TXT("<5,8,6> * 2 = %s"), testVector);
	if (testVector != Vector3(10,16,12))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <10,16,12>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector /= 8.f;
	TestLog(testFlags, TXT("<10,16,12> /=8  ---> %s"), testVector);
	if (testVector != Vector3(1.25, 2, 1.5))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <1.25,2,1.5>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector *= 10.f;
	TestLog(testFlags, TXT("<1.25,2,1.5> *=10  ---> %s"), testVector);
	if (testVector != Vector3(12.5, 20, 15))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <12.5,20,15>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}

	testVector += ((Vector2(5.f,5.f).ToVector3() - copiedVector) * 4.f) + Vector3(-10.f,-15.f,20.f);
	TestLog(testFlags, TXT("<12.5,20,15> += ((<5,5,0> - <1,2,3>) * 4) + <-10,-15,20>    results in:  %s"), testVector);
	if (testVector != Vector3(18.5, 17, 23))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testVector is <18.5,17,23>.  It's currently:  %s"), {testVector.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Vector3 firstVector(48.2f, -942.05f, -96.17f);
		Vector3 secondVector(-154.78f, 26.1f, 813.46f);
		Vector3 readFirstVector;
		Vector3 readSecondVector;
		DataBuffer vectorBuffer;

		vectorBuffer << firstVector;
		vectorBuffer << secondVector;
		vectorBuffer >> readFirstVector;
		vectorBuffer >> readSecondVector;
		
		if (firstVector != readFirstVector)
		{
			UnitTestError(testFlags, TXT("Vector3 streaming test failed.  The vector %s was pushed to a data buffer.  The vector %s was pulled from that data buffer."), {firstVector.ToString(), readFirstVector.ToString()});
			return false;
		}

		if (secondVector != readSecondVector)
		{
			UnitTestError(testFlags, TXT("Vector3 streaming test failed.  The 2nd vector %s was pushed to a data buffer.  The 2nd vector %s was pulled from that data buffer."), {secondVector.ToString(), readSecondVector.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utility"));
	testVector = Vector3(5,5,-10);
	TestLog(testFlags, TXT("Vector Length of <5,5,-10> is:  %s"), testVector.VSize());
	if (abs(testVector.VSize().Value - 12.24744871) > 0.000001)
	{
		UnitTestError(testFlags, TXT("VSize function failed.  Expected length of vector<5,5,-10> is ~12.24744871.  Expected precision is:  10^-6."));
		return false;
	}

	testVector.Normalize();
	TestLog(testFlags, TXT("Normalize<5,5,-10> results in:  %s"), testVector);
	if (testVector.X - 0.40824829f < 0.000001f && testVector.Y - 0.40824829f > 0.000001f && testVector.Z + 0.816496581f > 0.000001f)
	{
		UnitTestError(testFlags, TXT("Normalize vector function failed.  Expected resulting vector is:  ~<0.408248, 0.408248, -0.816497> with minimum precision of 10^-6."));
		return false;
	}

	testVector = Vector3(5.f, -4.f, 12.f);
	TestLog(testFlags, TXT("Vector's squared length of <5, -4, 12> is:  %s"), testVector.CalcDistSquared());
	FLOAT expectedDist = 185.f; 
	if (abs(testVector.CalcDistSquared().Value - expectedDist.Value) > 0.000001f)
	{
		UnitTestError(testFlags, TXT("Calc distance squared function failed.  Expected squared distance of vector %s is ~%s.  It computed %s instead."), {testVector.ToString(), expectedDist.ToString(), testVector.CalcDistSquared().ToString()});
		return false;
	}

	testVector = Vector3(0, 0, -15);
	testVector.SetLengthTo(4.25);
	TestLog(testFlags, TXT("Setting Length of <0,0,-15> to 4.25 results in:  %s"), testVector);
	if (testVector.VSize() != 4.25f || testVector != Vector3(0, 0, -4.25f))
	{
		UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<0,0,-15> to 4.25 should have resulted in <0,0,4.25>.  The VSize of %s is %s instead."), {testVector.ToString(), testVector.VSize().ToString()});
		return false;
	}

	testVector = Vector3(-84.3f, 148.2f, 76.7f);
	testVector.SetLengthTo(20.4f);
	TestLog(testFlags, TXT("Setting length of <-84.3, 148.2, 76.7> to 20.4 results in:  %s"), testVector);
	if (FLOAT::Abs(testVector.VSize() - 20.4f) > 0.00001f || (testVector - Vector3(-9.198518f, 16.171061f, 8.369233f)).VSize() > 0.00001f)
	{
		UnitTestError(testFlags, TXT("SetLengthTo function failed.  Setting the length of vector<-84.3, 148.2, 76.7> to 20.4 should have resulted in ~<-9.198518249, 16.17106055, 8.369233093>.  The VSize of %s is %s instead."), {testVector.ToString(), testVector.VSize().ToString()});
		return false;
	}

	{
		TestLog(testFlags, TXT("Testing Vector3 clamp functions (min, max, and clamp)..."));
		std::function<bool(const Vector3&, const Vector3&)> compareVec = [&](const Vector3& actual, const Vector3& expected)
		{
			if (actual != expected)
			{
				UnitTestError(testFlags, TXT("Clamp test failed since the actual vector (%s) does not match the expected (%s)."), {actual.ToString(), expected.ToString()});
				return false;
			}

			return true;
		};

		Vector3 originalVector(0.f, -100.f, 100.f);
		Vector3 clampVector(0.f, 0.f, 0.f);
		Vector3 resultingVector = Vector3::Min(originalVector, clampVector);
		Vector3 expectedVector(0.f, -100.f, 0.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		clampVector = Vector3(-500.f, -750.f, -1000.f);
		resultingVector = Vector3::Min(originalVector, clampVector);
		expectedVector = clampVector;
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		clampVector = Vector3(-200.f, 100.f, 50.f);
		resultingVector = Vector3::Min(originalVector, clampVector);
		expectedVector = Vector3(-200.f, -100.f, 50.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		resultingVector = Vector3::Max(originalVector, clampVector);
		expectedVector = Vector3(0.f, 100.f, 100);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		clampVector = Vector3(-10.f, -50.f, 50.f);
		resultingVector = Vector3::Max(originalVector, clampVector);
		expectedVector = Vector3(0.f, -50.f, 100.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		Vector3 minVec(0.f, 0.f, 0.f);
		Vector3 maxVec(50.f, 50.f, 50.f);
		resultingVector = Vector3::Clamp(originalVector, minVec, maxVec);
		expectedVector = Vector3(0.f, 0.f, 50.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		minVec = Vector3(-1000.f, 1000.f, 50.f);
		maxVec = Vector3(-500.f, 5000.f, 150.f);
		resultingVector = Vector3::Clamp(originalVector, minVec, maxVec);
		expectedVector = Vector3(-500.f, 1000.f, 100.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		minVec = Vector3(100.f, -300.f, 0.f);
		maxVec = Vector3(350.f, -200.f, 250.f);
		resultingVector = Vector3::Clamp(originalVector, minVec, maxVec);
		expectedVector = Vector3(100.f, -200.f, 100.f);
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}

		minVec = Vector3(-10.f, -150.f, 50.f);
		maxVec = Vector3(10, -50.f, 150.f);
		resultingVector = Vector3::Clamp(originalVector, minVec, maxVec);
		expectedVector = originalVector;
		if (!compareVec(resultingVector, expectedVector))
		{
			return false;
		}
	}

	testVector = Vector3(5,5,-10);
	Vector3 otherVector = Vector3(-11,-13,17);
	TestLog(testFlags, TXT("<5,5,-10> dot <-11,-13,17> = %s"), testVector.Dot(otherVector));
	if (testVector.Dot(otherVector) != -290)
	{
		UnitTestError(testFlags, TXT("Dot product of vectors %s and %s failed.  Expected value is -290."), {testVector.ToString(), otherVector.ToString()});
		return false;
	}

	//Example is based on:  Khan Academy.  Great place to brush up on vector math!
	testVector = Vector3(1, -7, 1);
	otherVector = Vector3(5, 2, 4);
	TestLog(testFlags, TXT("<1,-7,1> cross <5,2,4> = %s"), testVector.CrossProduct(otherVector));
	if (testVector.CrossProduct(otherVector) != Vector3(-30, 1, 37))
	{
		UnitTestError(testFlags, TXT("Cross product of vectors %s and %s failed.  Expected value is <-30,1,37>."), {testVector.ToString(), otherVector.ToString()});
		return false;
	}

	Vector3 minBounds = Vector3(0.f, 0.f, -100.f);
	Vector3 maxBounds = Vector3(100.f, 1000.f, 100.f);
	FLOAT alpha = 0.5f;
	Vector3 lerpVector = Vector3::Lerp(alpha, minBounds, maxBounds);
	TestLog(testFlags, TXT("Lerping Vector with minBounds=%s and maxBounds=%s with alpha %s resulted in %s."), testVector, minBounds, maxBounds, alpha, lerpVector);
	if (lerpVector != Vector3(50.f, 500.f, 0.f))
	{
		UnitTestError(testFlags, TXT("Vector3 Lerp test failed.  When lerping %s and %s with alpha %s, the expected vector is (50, 500, 0).  Instead it returned %s."), {minBounds.ToString(), maxBounds.ToString(), alpha.ToString(), lerpVector.ToString()});
		return false;
	}

	alpha = 0.9f;
	lerpVector = Vector3::Lerp(alpha, minBounds, maxBounds);
	if (lerpVector != Vector3(90.f, 900.f, 80.f))
	{
		UnitTestError(testFlags, TXT("Vector3 Lerp test failed.  When lerping %s and %s with alpha %s, the expected vector is (90, 900, 80).  Instead it returned %s."), {minBounds.ToString(), maxBounds.ToString(), alpha.ToString(), lerpVector.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Vector3"));

	return true;
}

bool DatatypeUnitTester::TestRotator (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Rotator"));

	Rotator rawRotator(348, 293, 192);
	Rotator upRotator(180.f, 90.f, 45.f, Rotator::RU_Degrees);
	Rotator copiedRotator(upRotator);
	Rotator backwardRotator(-180.f, 0.f, 0.f, Rotator::RU_Degrees);

	SetTestCategory(testFlags, TXT("Comparison"));
	{
		if (upRotator == backwardRotator)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The up %s and backward %s rotators should not be the same."), {upRotator.ToString(), backwardRotator.ToString()});
			return false;
		}

		if (upRotator != copiedRotator)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotator %s constructed from the up rotator %s should have been considered the same."), {copiedRotator.ToString(), upRotator.ToString()});
			return false;
		}

		//Test wrapping (negative directions should automatically be converted to positive units).
		Rotator normalDegrees(195.f, 210.f, 18.f, Rotator::RU_Degrees);
		Rotator normalRadians(PI_FLOAT * 0.6f, PI_FLOAT * 0.8f, PI_FLOAT * 0.4f, Rotator::RU_Radians);
		Rotator reversedDegrees(-360.f + normalDegrees.GetYaw(Rotator::RU_Degrees), -360.f + normalDegrees.GetPitch(Rotator::RU_Degrees), -360.f + normalDegrees.GetRoll(Rotator::RU_Degrees), Rotator::RU_Degrees);
		Rotator reversedRadians((PI_FLOAT * -2.f) + normalRadians.GetYaw(Rotator::RU_Radians), (PI_FLOAT * -2.f) + normalRadians.GetPitch(Rotator::RU_Radians), (PI_FLOAT * -2.f) + normalRadians.GetRoll(Rotator::RU_Radians), Rotator::RU_Radians);
		if (!IsNearlyEqual(normalDegrees, reversedDegrees) || !IsNearlyEqual(normalRadians, reversedRadians))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Constructing rotators with negative values did not properly wrapped around to positive values.  Degrees:  expected (%s), actual (%s).  Radians:  expected (%s), actual (%s)."), {normalDegrees.ToString(), reversedDegrees.ToString(), normalRadians.ToString(), reversedRadians.ToString()});
			return false;
		}

		Rotator wrappedDegrees(360.f + normalDegrees.GetYaw(Rotator::RU_Degrees), 720.f + normalDegrees.GetPitch(Rotator::RU_Degrees), 1080.f + normalDegrees.GetRoll(Rotator::RU_Degrees), Rotator::RU_Degrees);
		Rotator wrappedRadians((PI_FLOAT * 2.f) + normalRadians.GetYaw(Rotator::RU_Radians), (PI_FLOAT * 4.f) + normalRadians.GetPitch(Rotator::RU_Radians), (PI_FLOAT * 6.f) + normalRadians.GetRoll(Rotator::RU_Radians), Rotator::RU_Radians);
		if (!IsNearlyEqual(normalDegrees, wrappedDegrees) || !IsNearlyEqual(normalRadians, wrappedRadians))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Constructing rotators with values greater than a full revolution should have automatically wrapped back down between 0 to one revolution.  Degrees:  expected (%s), actual (%s).  Radians:  expected (%s), actual (%s)."), {normalDegrees.ToString(), wrappedDegrees.ToString(), normalRadians.ToString(), wrappedRadians.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Unit Conversions"));
	{
		//Construct three identical rotators from different units
		unsigned int quarterAngle = Rotator::FULL_REVOLUTION / 8;
		Rotator sdRotator(quarterAngle, quarterAngle * 2, quarterAngle * 7);

		Rotator degreeRotator(45.f, 90.f, 315.f, Rotator::RU_Degrees);

		FLOAT quarterAngleRadian = PI_FLOAT / 4.f;
		Rotator radianRotator(quarterAngleRadian, quarterAngleRadian * 2.f, quarterAngleRadian * 7.f, Rotator::RU_Radians);

		if (!IsNearlyEqual(sdRotator, degreeRotator) || !IsNearlyEqual(sdRotator, radianRotator))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After creating 3 identical rotators from different units, the 3 rotators were not considered to be the same:  %s (SD Units), %s (degrees), %s (radians)."), {sdRotator.ToString(), degreeRotator.ToString(), radianRotator.ToString()});
			return false;
		}

		//Snap the radianRotator since PI_FLOAT is not precise enough to snap to right angles.
		radianRotator = sdRotator;

		//Pull the components in different units
		{
			//No need to test SD Units since the degree and radian rotators are already saving data in SD units, and the comparison earlier already checked for that.
			
			//Test degrees
			FLOAT yawTarget = 45.f;
			FLOAT pitchTarget = 90.f;
			FLOAT rollTarget = 315.f;
			FLOAT yawDegree;
			FLOAT pitchDegree;
			FLOAT rollDegree;
			degreeRotator.GetDegrees(OUT yawDegree, OUT pitchDegree, OUT rollDegree);

			if (yawDegree != yawTarget || pitchDegree != pitchTarget || rollDegree != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting degrees from a degree Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), {degreeRotator.ToString(), yawDegree.ToString(), pitchDegree.ToString(), rollDegree.ToString(), yawTarget.ToString(), pitchTarget.ToString(), rollTarget.ToString()});
				return false;
			}

			sdRotator.GetDegrees(OUT yawDegree, OUT pitchDegree, OUT rollDegree);
			if (yawDegree != yawTarget || pitchDegree != pitchTarget || rollDegree != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting degrees from a SD Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), {sdRotator.ToString(), yawDegree.ToString(), pitchDegree.ToString(), rollDegree.ToString(), yawTarget.ToString(), pitchTarget.ToString(), rollTarget.ToString()});
				return false;
			}

			radianRotator.GetDegrees(OUT yawDegree, OUT pitchDegree, OUT rollDegree);
			if (yawDegree != yawTarget || pitchDegree != pitchTarget || rollDegree != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting degrees from a radian Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), {radianRotator.ToString(), yawDegree.ToString(), pitchDegree.ToString(), rollDegree.ToString(), yawTarget.ToString(), pitchTarget.ToString(), rollTarget.ToString()});
				return false;
			}

			//Test radians
			yawTarget = quarterAngleRadian;
			pitchTarget = quarterAngleRadian * 2.f;
			rollTarget = quarterAngleRadian * 7.f;
			FLOAT yawRadian;
			FLOAT pitchRadian;
			FLOAT rollRadian;

			radianRotator.GetRadians(OUT yawRadian, OUT pitchRadian, OUT rollRadian);
			if (yawRadian != yawTarget || pitchRadian != pitchTarget || rollRadian != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting radians from a radian Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), {radianRotator.ToString(), yawRadian.ToString(), pitchRadian.ToString(), rollRadian.ToString(), yawTarget.ToString(), pitchTarget.ToString(), rollTarget.ToString()});
				return false;
			}

			sdRotator.GetRadians(OUT yawRadian, OUT pitchRadian, OUT rollRadian);
			if (yawRadian != yawTarget || pitchRadian != pitchTarget || rollRadian != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting radians from a SD Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), {sdRotator.ToString(), yawRadian.ToString(), pitchRadian.ToString(), rollRadian.ToString(), yawTarget.ToString(), pitchTarget.ToString(), rollTarget.ToString()});
				return false;
			}

			degreeRotator.GetRadians(OUT yawRadian, OUT pitchRadian, OUT rollRadian);
			if (yawRadian != yawTarget || pitchRadian != pitchTarget || rollRadian != rollTarget)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  Getting radians from a degree Rotator %s did not return the expected results.  It retrieved %s, %s, %s instead of %s, %s, %s."), {degreeRotator.ToString(), yawRadian.ToString(), pitchRadian.ToString(), rollRadian.ToString(), yawTarget.ToString(), pitchTarget.ToString(), rollTarget.ToString()});
				return false;
			}
		}

		//Constructed Rotator from signed ints
		INT intYaw = 40000; //Test against normal condition
		INT intPitch = 264144; //Test against wrapping
		INT intRoll = -2536; //Test against negative numbers
		Rotator signedIntRotator = Rotator::MakeRotator(intYaw, intPitch, intRoll);
		Rotator expectedIntRotator(40000, 2000, 63000);
		if (signedIntRotator != expectedIntRotator)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Failed to construct a rotator from signed ints (Yaw=%s, Pitch=%s, Roll=%s).  It constructed %s instead of %s."), {intYaw.ToString(), intPitch.ToString(), intRoll.ToString(), signedIntRotator.ToString(), expectedIntRotator.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	{
		//Simple add
		Rotator left(123, 456, 789);
		Rotator right(987, 654, 321);
		Rotator sum = left + right;
		Rotator expected(1110, 1110, 1110);
		if (sum != expected)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Adding the rotators %s and %s resulted in %s instead of %s."), {left.ToString(), right.ToString(), sum.ToString(), expected.ToString()});
			return false;
		}

		//Simple subtraction
		left = Rotator(186, 3084, 8465);
		right = Rotator(4379, 1543, 7361);
		Rotator subtraction = left - right;
		expected = Rotator(61343, 1541, 1104); //Yaw should automatically wrap to positive values.
		if (subtraction != expected)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Subtracting the rotators %s and %s resulted in %s instead of %s."), {left.ToString(), right.ToString(), subtraction.ToString(), expected.ToString()});
			return false;
		}

		unsigned int incrementSize = Rotator::FULL_REVOLUTION / 36; //10 degrees
		unsigned int numIncrements = 96;
		Rotator adjustedRotator(0, 0, 0);

		for (unsigned int i = 0; i < numIncrements; ++i)
		{
			adjustedRotator.Yaw += incrementSize;

			unsigned int expectedRotation = (incrementSize * (i+1)) % Rotator::FULL_REVOLUTION;
			if (adjustedRotator.Yaw != expectedRotation)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  After incrementing a rotator by 10 degrees %s time(s), the resulting rotator's yaw is %s.  Its yaw expected to be %s instead."), {DString::MakeString(i), DString::MakeString(adjustedRotator.Yaw), DString::MakeString(expectedRotation)});
				return false;
			}
		}

		Rotator decrementingRotator(2000, 2200, 63000);
		adjustedRotator = Rotator(0, 0, 0);

		for (unsigned int i = 0; i < numIncrements; ++i)
		{
			adjustedRotator -= decrementingRotator;
			Rotator expectedRotator = Rotator::MakeRotator(-2000 * (i+1), -2200 * (i+1), -63000 * (i+1));
			if (adjustedRotator != expectedRotator)
			{
				UnitTestError(testFlags, TXT("Rotator test failed.  After decrementing a blank rotator by %s by %s many time(s), the resulting rotator is %s instead of %s."), {decrementingRotator.ToString(), DString::MakeString(i), adjustedRotator.ToString(), expectedRotator.ToString()});
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Rotator streamWriteRotator(164, 2983, 64028);
		Rotator streamWriteRotator2(70.f, -45.f, 720.f, Rotator::RU_Degrees);
		Rotator streamReadRotator;
		Rotator streamReadRotator2;
		DataBuffer rotatorBuffer;

		rotatorBuffer << streamWriteRotator;
		rotatorBuffer << streamWriteRotator2;
		rotatorBuffer >> streamReadRotator;
		rotatorBuffer >> streamReadRotator2;

		if (streamReadRotator != streamWriteRotator)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotator %s was pushed to a stream buffer, but %s was read from it instead."), {streamWriteRotator.ToString(), streamReadRotator.ToString()});
			return false;
		}

		if (streamReadRotator2 != streamWriteRotator2)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotator %s was pushed to a stream buffer, but %s was read from it instead."), {streamWriteRotator2.ToString(), streamReadRotator2.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Vector | Rotator conversions"));
	{
		TestLog(testFlags, TXT("Constructing rotators from 3D vectors."));
		Vector3 forwardVector(1.f, 0.f, 0.f);
		Vector3 backwardVector(-1.f, 0.f, 0.f);
		Vector3 upVector(0.f, 0.f, 1.f);
		Vector3 downVector(0.f, 0.f, -1.f);
		Vector3 leftVector(0.f, -500.f, 0.f);
		Vector3 rightVector(0.f, 500.f, 0.f);
		Vector3 forwardRightVector(128.f, 128.f, 0.f);
		Vector3 downBackwardVector(-64.f, 0.f, -64.f);

		Rotator forward(forwardVector);
		Rotator backward(backwardVector);
		Rotator up(upVector);
		Rotator down(downVector);
		Rotator left(leftVector);
		Rotator right(rightVector);
		Rotator forwardRight(forwardRightVector);
		Rotator downBackward(downBackwardVector);

		Rotator expectedForward(0, 0, 0);
		Rotator expectedBackward(180.f, 0.f, 0.f, Rotator::RU_Degrees);
		Rotator expectedUp(0.f, 90.f, 0.f, Rotator::RU_Degrees);
		Rotator expectedDown(0.f, -90.f, 0.f, Rotator::RU_Degrees);
		Rotator expectedLeft(90.f, 0.f, 0.f, Rotator::RU_Degrees);
		Rotator expectedRight(-90.f, 0.f, 0.f, Rotator::RU_Degrees);
		Rotator expectedForwardRight(-45.f, 0.f, 0.f, Rotator::RU_Degrees);
		Rotator expectedDownBackward(180.f, -45.f, 0.f, Rotator::RU_Degrees);

		if (!IsNearlyEqual(forward, expectedForward))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a Rotator from vector %s, it produced %s instead of %s."), {forwardVector.ToString(), forward.ToString(), expectedForward.ToString()});
			return false;
		}

		if (!IsNearlyEqual(backward, expectedBackward))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a Rotator from vector %s, it produced %s instead of %s."), {backwardVector.ToString(), backward.ToString(), expectedBackward.ToString()});
			return false;
		}

		if (!IsNearlyEqual(up, expectedUp))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a Rotator from vector %s, it produced %s instead of %s."), {upVector.ToString(), up.ToString(), expectedUp.ToString()});
			return false;
		}

		if (!IsNearlyEqual(down, expectedDown))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a Rotator from vector %s, it produced %s instead of %s."), {downVector.ToString(), down.ToString(), expectedDown.ToString()});
			return false;
		}

		if (!IsNearlyEqual(left, expectedLeft))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a Rotator from vector %s, it produced %s instead of %s."), {leftVector.ToString(), left.ToString(), expectedLeft.ToString()});
			return false;
		}

		if (!IsNearlyEqual(right, expectedRight))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a Rotator from vector %s, it produced %s instead of %s."), {rightVector.ToString(), right.ToString(), expectedRight.ToString()});
			return false;
		}

		if (!IsNearlyEqual(forwardRight, expectedForwardRight))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a Rotator from vector %s, it produced %s instead of %s."), {forwardRightVector.ToString(), forwardRight.ToString(), expectedForwardRight.ToString()});
			return false;
		}

		if (!IsNearlyEqual(downBackward, expectedDownBackward))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a Rotator from vector %s, it produced %s instead of %s."), {downBackwardVector.ToString(), downBackward.ToString(), expectedDownBackward.ToString()});
			return false;
		}

		//Resnap generated vectors to expected to eliminate precision errors generated from SD <-> Radian conversions.
		forward = expectedForward;
		backward = expectedBackward;
		up = expectedUp;
		down = expectedDown;
		left = expectedLeft;
		right = expectedRight;
		forwardRight = expectedForwardRight;
		downBackward = expectedDownBackward;

		TestLog(testFlags, TXT("Generating 3D Vectors from Rotators."));
		Vector3 generatedForward = forward.GetDirectionalVector();
		Vector3 generatedBackward = backward.GetDirectionalVector();
		Vector3 generatedUp = up.GetDirectionalVector();
		Vector3 generatedDown = down.GetDirectionalVector();
		Vector3 generatedLeft = left.GetDirectionalVector();
		Vector3 generatedRight = right.GetDirectionalVector();
		Vector3 generatedForwardRight = forwardRight.GetDirectionalVector();
		Vector3 generatedDownBackward = downBackward.GetDirectionalVector();

		//Normalize original vectors before their comparisons since previous test was testing if nonnormalized vectors were convertable to directional rotations.
		forwardVector.Normalize();
		backwardVector.Normalize();
		upVector.Normalize();
		downVector.Normalize();
		leftVector.Normalize();
		rightVector.Normalize();
		forwardRightVector.Normalize();
		downBackwardVector.Normalize();

		if (generatedForward != forwardVector)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a rotator %s from vector %s, the rotator was unable to produce the original vector.  Instead it returned %s."), {forward.ToString(), forwardVector.ToString(), generatedForward.ToString()});
			return false;
		}

		if (generatedBackward != backwardVector)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a rotator %s from vector %s, the rotator was unable to produce the original vector.  Instead it returned %s."), {backward.ToString(), backwardVector.ToString(), generatedBackward.ToString()});
			return false;
		}

		if (generatedUp != upVector)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a rotator %s from vector %s, the rotator was unable to produce the original vector.  Instead it returned %s."), {up.ToString(), upVector.ToString(), generatedUp.ToString()});
			return false;
		}

		if (generatedDown != downVector)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a rotator %s from vector %s, the rotator was unable to produce the original vector.  Instead it returned %s."), {down.ToString(), downVector.ToString(), generatedDown.ToString()});
			return false;
		}

		if (generatedLeft != leftVector)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a rotator %s from vector %s, the rotator was unable to produce the original vector.  Instead it returned %s."), {left.ToString(), leftVector.ToString(), generatedLeft.ToString()});
			return false;
		}

		if (generatedRight != rightVector)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a rotator %s from vector %s, the rotator was unable to produce the original vector.  Instead it returned %s."), {right.ToString(), rightVector.ToString(), generatedRight.ToString()});
			return false;
		}

		if (generatedForwardRight != forwardRightVector)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a rotator %s from vector %s, the rotator was unable to produce the original vector.  Instead it returned %s."), {forwardRight.ToString(), forwardRightVector.ToString(), generatedForwardRight.ToString()});
			return false;
		}

		if (generatedDownBackward != downBackwardVector)
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  After constructing a rotator %s from vector %s, the rotator was unable to produce the original vector.  Instead it returned %s."), {downBackward.ToString(), downBackwardVector.ToString(), generatedDownBackward.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Rotator Utils"));
	{
		Rotator up(0.f, 90.f, 0.f, Rotator::RU_Degrees);
		Rotator forward(0.f, 0.f, 0.f, Rotator::RU_Degrees);
		Rotator down(0.f, -90.f, 0.f, Rotator::RU_Degrees);
		Rotator right(-90.f, 0.f, 0.f, Rotator::RU_Degrees);
		Rotator upRight(45.f, 45.f, 0.f, Rotator::RU_Degrees);
		Rotator other(-45.f, 0.f, 0.f, Rotator::RU_Degrees);

		//Test for parallel
		if (!up.IsParallelTo(up))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Rotators should be parallel to themselves.  For some reason %s is not parallel to itself."), {up.ToString()});
			return false;
		}

		if (!up.IsParallelTo(down))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  %s should have been considered parallel to %s."), {up.ToString(), down.ToString()});
			return false;
		}

		if (up.IsParallelTo(forward) || up.IsParallelTo(right) || up.IsParallelTo(other))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The %s rotation should not be parallel to any of the following rotators:  %s, %s, %s."), {up.ToString(), forward.ToString(), right.ToString(), other.ToString()});
			return false;
		}

		if (up.IsPerpendicularTo(up))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  Rotations should never be perpendicular to themselves."));
			return false;
		}

		if (up.IsPerpendicularTo(down))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotation %s should not be considered perpendicular to %s."), {up.ToString(), down.ToString()});
			return false;
		}

		if (!forward.IsPerpendicularTo(right))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotation %s should have been considered perpendicular to %s."), {forward.ToString(), right.ToString()});
			return false;
		}

		if (!forward.IsPerpendicularTo(down))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotation %s should have been considered perpendicular to %s."), {forward.ToString(), down.ToString()});
			return false;
		}

		if (right.IsPerpendicularTo(upRight))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotation %s should not have been considered perpendicular to %s."), {right.ToString(), upRight.ToString()});
			return false;
		}

		if (!upRight.IsPerpendicularTo(other))
		{
			UnitTestError(testFlags, TXT("Rotator test failed.  The rotation %s should have been considered perpendicular to %s."), {upRight.ToString(), other.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Rotator"));

	return true;
}

bool DatatypeUnitTester::TestRectangle (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Rectangle"));

	Rectangle<FLOAT> blankRectangle;
	Rectangle<FLOAT> initializedRectangle(0, 5, 10, 15);
	Rectangle<FLOAT> copiedRectangle(initializedRectangle);

	TestLog(testFlags, TXT("Constructor tests:  BlankRectangle=%s"), blankRectangle);
	TestLog(testFlags, TXT("    initializedRectangle(Left(0), Top(5), Right(10), Bottom(15))= %s"), initializedRectangle);
	TestLog(testFlags, TXT("    copiedRectangle from initializedRectangle= %s"), copiedRectangle);

	SetTestCategory(testFlags, TXT("Comparison"));
	TestLog(testFlags, TXT("blankRectangle != Rect(0,0,0,0)?  %s"), BOOL(blankRectangle != Rectangle<FLOAT>(0,0,0,0)));
	if (blankRectangle != Rectangle<FLOAT>(0,0,0,0))
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  blankRectangle is not zero.  Instead it's:  %s"), {blankRectangle.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("initializedRectangle(%s) != copiedRectangle(%s) ? %s"), initializedRectangle, copiedRectangle, BOOL(initializedRectangle != copiedRectangle));
	if (initializedRectangle != copiedRectangle)
	{
		UnitTestError(testFlags, TXT("Comparison condition failed.  initializedRectangle != copiedRectangle.  Values are:  %s and %s"), {initializedRectangle.ToString(), copiedRectangle.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Arithmetic"));
	Rectangle<FLOAT> baseRectangle(0,0,10,10);
	Rectangle<FLOAT> testRectangle = baseRectangle + Rectangle<FLOAT>(5,5,20,20);
	TestLog(testFlags, TXT("[0,0,10,10] + [5,5,20,20] = %s"), testRectangle);
	if (testRectangle != Rectangle<FLOAT>(5,5,30,30))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRectangle is [5,5,30,30].  It's currently:  %s"), {testRectangle.ToString()});
		return false;
	}

	testRectangle = baseRectangle;
	testRectangle += Rectangle<FLOAT>(0, 4, 35, 50);
	TestLog(testFlags, TXT("[0,0,10,10] += [0,4,35,50]  ---> %s"), testRectangle);
	if (testRectangle != Rectangle<FLOAT>(0,4,45,60))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRectangle is [0,4,45,60].  It's currently:  %s"), {testRectangle.ToString()});
		return false;
	}

	testRectangle = initializedRectangle * 2.f;
	TestLog(testFlags, TXT("[0,5,10,15] * 2 = %s"), testRectangle);
	if (testRectangle != Rectangle<FLOAT>(0,10,20,30))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRectangle is [0,10,20,30].  It's currently:  %s"), {testRectangle.ToString()});
		return false;
	}

	testRectangle /= 4.f;
	TestLog(testFlags, TXT("[0,10,20,30] /= 4  ---> %s"), testRectangle);
	if (testRectangle != Rectangle<FLOAT>(0,2.5f,5,7.5f))
	{
		UnitTestError(testFlags, TXT("Arithmetic failed.  Expected value for testRectangle is [0, 2.5, 5, 7.5].  It's currently:  %s"), {testRectangle.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Rectangle<INT> streamIntRectangle(-45, 32, 24, -21, Rectangle<INT>::CS_XRightYUp);
		Rectangle<FLOAT> streamFloatRectangle(-12.5f, 20.1f, 19.5f, -82.4f, Rectangle<FLOAT>::CS_XRightYDown);
		Rectangle<INT> readStreamIntRectangle;
		Rectangle<FLOAT> readStreamFloatRectangle;
		DataBuffer rectangleBuffer;

		rectangleBuffer << streamIntRectangle;
		rectangleBuffer << streamFloatRectangle;
		rectangleBuffer >> readStreamIntRectangle;
		rectangleBuffer >> readStreamFloatRectangle;

		if (streamIntRectangle != readStreamIntRectangle)
		{
			UnitTestError(testFlags, TXT("Rectangle stream test failed.  The int rectangle %s was pushed to data buffer.  The int rectangle %s was pulled from data buffer."), {streamIntRectangle.ToString(), readStreamIntRectangle.ToString()});
			return false;
		}

		if (streamFloatRectangle != readStreamFloatRectangle)
		{
			UnitTestError(testFlags, TXT("Rectangle stream test failed.  The float rectangle %s was pushed to data buffer.  The float rectangle %s was pulled from data buffer."), {streamFloatRectangle.ToString(), readStreamFloatRectangle.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utility"));
	testRectangle = baseRectangle;
	TestLog(testFlags, TXT("Area of [0,0,10,10] is:  %s"), testRectangle.CalculateArea());
	if (testRectangle.CalculateArea() != 100)
	{
		UnitTestError(testFlags, TXT("Rectangle utilities failed.  Expected value for the area of [0,0,10,10] is 100.  It returned %s instead."), {testRectangle.CalculateArea().ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Perimeter of [0,0,10,10] is:  %s"), testRectangle.CalculatePerimeter());
	if (testRectangle.CalculatePerimeter() != 40)
	{
		UnitTestError(testFlags, TXT("Rectangle utilities failed.  Expected value for the perimeter of [0,0,10,10] is 40.  It returned %s instead."), {testRectangle.CalculatePerimeter().ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Is Rectangle[0,0,10,10] a square?  %s"), BOOL(testRectangle.IsSquare()));
	if (!testRectangle.IsSquare())
	{
		UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should have been considered a square."), {testRectangle.ToString()});
		return false;
	}

	Rectangle<FLOAT> tallRectangle = Rectangle<FLOAT>(0,5,5,15);
	TestLog(testFlags, TXT("Is Rectangle %s a square?  %s"), tallRectangle, BOOL(tallRectangle.IsSquare()));
	if (tallRectangle.IsSquare())
	{
		UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should not have been considered a square."), {tallRectangle.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Is Rectangle[0,0,10,10] blank?  %s"), BOOL(testRectangle.IsEmpty()));
	if (testRectangle.IsEmpty())
	{
		UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should not have been considered an empty rectangle."), {testRectangle.ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Is Rectangle[0,0,0,0] blank?  %s"), BOOL(blankRectangle.IsEmpty()));
	if (!blankRectangle.IsEmpty())
	{
		UnitTestError(testFlags, TXT("Rectangle utilities failed.  %s should have been considered an empty rectangle."), {blankRectangle.ToString()});
		return false;
	}

	testRectangle = Rectangle<FLOAT>(20, 10, 40, -15);
	TestLog(testFlags, TXT("Width of rectangle %s is %s"), testRectangle, testRectangle.GetWidth());
	if (testRectangle.GetWidth() != 20)
	{
		UnitTestError(testFlags, TXT("Rectangle utilities failed.  The width of rectangle %s should have returned 20.  Instead it returned %s"), {testRectangle.ToString(), testRectangle.GetWidth().ToString()});
		return false;
	}

	TestLog(testFlags, TXT("Height of rectangle %s is %s"), testRectangle, testRectangle.GetHeight());
	if (testRectangle.GetHeight() != 25)
	{
		UnitTestError(testFlags, TXT("Rectangle utilities failed.  The height of rectangle %s should have returned 25.  Instead it returned %s"), {testRectangle.ToString(), testRectangle.GetHeight().ToString()});
		return false;
	}

	{
		testRectangle = Rectangle<FLOAT>(20, 10, 40, -15);
		Rectangle<FLOAT> otherRectangle(35,14,70,-100);
		TestLog(testFlags, TXT("Does rectangle %s overlap with rectangle %s ? %s"), testRectangle, otherRectangle, BOOL(testRectangle.Overlaps(otherRectangle)));
		if (!testRectangle.Overlaps(otherRectangle))
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Rectangles %s and %s should be overlapping when the function returned false."), {testRectangle.ToString(), otherRectangle.ToString()});
			return false;
		}

		otherRectangle.Top = testRectangle.Bottom - 5;
		TestLog(testFlags, TXT("Does rectangle %s overlap with rectangle %s ? %s"), testRectangle, otherRectangle, BOOL(testRectangle.Overlaps(otherRectangle)));
		if (testRectangle.Overlaps(otherRectangle))
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Rectangles %s and %s should not be overlapping when the function returned true."), {testRectangle.ToString(), otherRectangle.ToString()});
			return false;
		}

		//Test rectangle within another rectangle
		otherRectangle = testRectangle;
		otherRectangle.Left += 5;
		otherRectangle.Top -= 5;
		otherRectangle.Right -= 5;
		otherRectangle.Bottom += 5;
		TestLog(testFlags, TXT("Does rectangle %s overlap with rectangle %s ? %s"), testRectangle, otherRectangle, BOOL(testRectangle.Overlaps(otherRectangle)));
		if (!testRectangle.Overlaps(otherRectangle))
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Rectangles %s and %s should be overlapping when the function returned false."), {testRectangle.ToString(), otherRectangle.ToString()});
			return false;
		}

		//Test rectangle left of another rectangle
		otherRectangle = testRectangle;
		otherRectangle.Left -= 100;
		otherRectangle.Right -= 100;
		TestLog(testFlags, TXT("Does rectangle %s overlap with rectangle %s ? %s"), testRectangle, otherRectangle, BOOL(testRectangle.Overlaps(otherRectangle)));
		if (testRectangle.Overlaps(otherRectangle))
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Rectangles %s and %s should not be overlapping when the function returned true."), {testRectangle.ToString(), otherRectangle.ToString()});
			return false;
		}

		//Test rectangle above another rectangle
		otherRectangle = testRectangle;
		otherRectangle.Top -= 100;
		otherRectangle.Bottom -= 100;
		TestLog(testFlags, TXT("Does rectangle %s overlap with rectangle %s ? %s"), testRectangle, otherRectangle, BOOL(testRectangle.Overlaps(otherRectangle)));
		if (testRectangle.Overlaps(otherRectangle))
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Rectangles %s and %s should not be overlapping when the function returned true."), {testRectangle.ToString(), otherRectangle.ToString()});
			return false;
		}

		//Test rectangle right of another rectangle
		otherRectangle = testRectangle;
		otherRectangle.Left += 100;
		otherRectangle.Right += 100;
		TestLog(testFlags, TXT("Does rectangle %s overlap with rectangle %s ? %s"), testRectangle, otherRectangle, BOOL(testRectangle.Overlaps(otherRectangle)));
		if (testRectangle.Overlaps(otherRectangle))
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Rectangles %s and %s should not be overlapping when the function returned true."), {testRectangle.ToString(), otherRectangle.ToString()});
			return false;
		}

		//Test rectangle below another rectangle
		otherRectangle = testRectangle;
		otherRectangle.Top += 100;
		otherRectangle.Bottom += 100;
		TestLog(testFlags, TXT("Does rectangle %s overlap with rectangle %s ? %s"), testRectangle, otherRectangle, BOOL(testRectangle.Overlaps(otherRectangle)));
		if (testRectangle.Overlaps(otherRectangle))
		{
			UnitTestError(testFlags, TXT("Rectangle utilities failed.  Rectangles %s and %s should not be overlapping when the function returned true."), {testRectangle.ToString(), otherRectangle.ToString()});
			return false;
		}
	}

	Rectangle<FLOAT> otherRectangle = Rectangle<FLOAT>(5, 12, 30, -35);
	testRectangle = Rectangle<FLOAT>(20, 10, 40, -15);
	Rectangle<FLOAT> overlappingRectangle = testRectangle.GetOverlappingRectangle(otherRectangle);
	TestLog(testFlags, TXT("The resulting overlapping rectangle constructed from rectangles %s and %s is %s"), testRectangle, otherRectangle, overlappingRectangle);
	if (overlappingRectangle != Rectangle<FLOAT>(20, 10, 30, -15))
	{
		UnitTestError(testFlags, TXT("Rectangle utilities failed.  The expected resulting overlapping rectangle was [20,10,30,-15].  The overlapping rectangle is currently:  %s"), {overlappingRectangle.ToString()});
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Testing other coordinate system:  X-Right, Y-Down"));
	{
		//Only test utility functions that depends on coordinate system.
		TestLog(testFlags, TXT("Testing Emcompasses point"));
		Rectangle<INT> coordinateRect(10, 10, 90, 90, Rectangle<INT>::CS_XRightYDown);

		std::function<bool(const Vector2& /*testPoint */, bool /*shouldBeEncompassed*/)> testEncompassPoint = [&](const Vector2& testPoint, bool shouldBeEncompassed)
		{
			if (coordinateRect.EncompassesPoint(testPoint) != shouldBeEncompassed)
			{
				if (shouldBeEncompassed)
				{
					UnitTestError(testFlags, TXT("Rectangle tests failed.  When testing a rectangle with X-right, Y-down coordinate system, the point %s should have been considered inside the rectangle %s."), {testPoint.ToString(), coordinateRect.ToString()});
				}
				else
				{
					UnitTestError(testFlags, TXT("Rectangle tests failed.  When testing a rectangle with X-right, Y-down coordinate system, the point %s should have been considered outside the rectangle %s."), {testPoint.ToString(), coordinateRect.ToString()});
				}

				return false;
			}

			return true;
		};

		if (!testEncompassPoint(Vector2(5, 40), false))
		{
			return false;
		}

		if (!testEncompassPoint(Vector2(40, 5), false))
		{
			return false;
		}

		if (!testEncompassPoint(Vector2(95, 40), false))
		{
			return false;
		}

		if (!testEncompassPoint(Vector2(40, 95), false))
		{
			return false;
		}

		if (!testEncompassPoint(Vector2(40, 40), true))
		{
			return false;
		}

		if (!testEncompassPoint(Vector2(25, 85), true))
		{
			return false;
		}

		if (!testEncompassPoint(Vector2(85, 25), true))
		{
			return false;
		}

		if (!testEncompassPoint(Vector2(85, 75), true))
		{
			return false;
		}

		TestLog(testFlags, TXT("Testing Overlaps"));
		std::function<bool(const Rectangle<INT>& /*otherRect*/, bool /*shouldOverlap*/)> testOverlaps = [&](const Rectangle<INT>& otherRect, bool shouldOverlap)
		{
			if (coordinateRect.Overlaps(otherRect) != shouldOverlap)
			{
				if (shouldOverlap)
				{
					UnitTestError(testFlags, TXT("Rectangle test failed.  A rectangle with X-Right, Y-Down coordinate system with dimensions %s should be overlapping with other rectangle %s."), {coordinateRect.ToString(), otherRect.ToString()});
				}
				else
				{
					UnitTestError(testFlags, TXT("Rectangle test failed.  A rectangle with X-Right, Y-Down coordinate system with dimensions %s should NOT be overlapping with other rectangle %s."), {coordinateRect.ToString(), otherRect.ToString()});
				}

				return false;
			}

			//The Overlaps function should be symmetrical
			if (coordinateRect.Overlaps(otherRect) != otherRect.Overlaps(coordinateRect))
			{
				UnitTestError(testFlags, TXT("Rectangle test failed.  Rectangles %s and %s should produce the same result in Overlaps function regardless which object is passed in parameter.  One of them is return true while the other is returning false."), {coordinateRect.ToString(), otherRect.ToString()});
				return false;
			}

			return true;
		};

		if (!testOverlaps(Rectangle<INT>(-5, 15, -35, 5, Rectangle<INT>::CS_XRightYDown), false))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(-5, -5, 35, 5, Rectangle<INT>::CS_XRightYDown), false))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(95, 16, 115, 32, Rectangle<INT>::CS_XRightYDown), false))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(32, 98, 64, 115, Rectangle<INT>::CS_XRightYDown), false))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(-5, -432, 16, 24, Rectangle<INT>::CS_XRightYDown), true))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(64, -300, 135, 48, Rectangle<INT>::CS_XRightYDown), true))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(64, 48, 135, 256, Rectangle<INT>::CS_XRightYDown), true))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(-48, 48, 55, 256, Rectangle<INT>::CS_XRightYDown), true))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(-100, 32, 200, 64, Rectangle<INT>::CS_XRightYDown), true))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(32, -100, 64, 200, Rectangle<INT>::CS_XRightYDown), true))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(-100, -100, 500, 500, Rectangle<INT>::CS_XRightYDown), true))
		{
			return false;
		}

		if (!testOverlaps(Rectangle<INT>(45, 45, 55, 55, Rectangle<INT>::CS_XRightYDown), true))
		{
			return false;
		}

		TestLog(testFlags, TXT("Testing GetOverlappingRectangle"));
		std::function<bool(const Rectangle<INT>& /*otherRect*/, const Rectangle<INT>& /*expectedResult*/)> testOverlappingRect = [&](const Rectangle<INT>& otherRect, const Rectangle<INT>& expectedResult)
		{
			if (coordinateRect.GetOverlappingRectangle(otherRect) != expectedResult)
			{
				UnitTestError(testFlags, TXT("Rectangle test failed.  Getting the overlapping rectangle within %s and %s using a X-right, Y-down coordinate system should have resulted in %s.  It generated %s instead."), {coordinateRect.ToString(), otherRect.ToString(), expectedResult.ToString(), coordinateRect.GetOverlappingRectangle(otherRect).ToString()});
				return false;
			}

			if (coordinateRect.GetOverlappingRectangle(otherRect) != otherRect.GetOverlappingRectangle(coordinateRect))
			{
				UnitTestError(testFlags, TXT("Rectangle test failed.  The GetOverlappingRectangle function should be a symmetrical function.  RectangleA.GetOverlappingRectangle(RectangleB) generated %s, and RectangleB.GetOverlappingRectangle(RectangleA) generated %s."), {coordinateRect.GetOverlappingRectangle(otherRect).ToString(), otherRect.GetOverlappingRectangle(coordinateRect).ToString()});
				return false;
			}

			return true;
		};

		if (!testOverlappingRect(Rectangle<INT>(-10, -10, 5, 5, Rectangle<INT>::CS_XRightYDown), Rectangle<INT>(Rectangle<INT>::CS_XRightYDown)))
		{
			return false;
		}

		if (!testOverlappingRect(Rectangle<INT>(95, 95, 115, 115, Rectangle<INT>::CS_XRightYDown), Rectangle<INT>(Rectangle<INT>::CS_XRightYDown)))
		{
			return false;
		}

		if (!testOverlappingRect(Rectangle<INT>(-32, -32, 48, 64, Rectangle<INT>::CS_XRightYDown), Rectangle<INT>(10, 10, 48, 64, Rectangle<INT>::CS_XRightYDown)))
		{
			return false;
		}

		if (!testOverlappingRect(Rectangle<INT>(64, -32, 115, 48, Rectangle<INT>::CS_XRightYDown), Rectangle<INT>(64, 10, 90, 48, Rectangle<INT>::CS_XRightYDown)))
		{
			return false;
		}

		if (!testOverlappingRect(Rectangle<INT>(64, 75, 115, 130, Rectangle<INT>::CS_XRightYDown), Rectangle<INT>(64, 75, 90, 90, Rectangle<INT>::CS_XRightYDown)))
		{
			return false;
		}

		if (!testOverlappingRect(Rectangle<INT>(-10, 80, 16, 140, Rectangle<INT>::CS_XRightYDown), Rectangle<INT>(10, 80, 16, 90, Rectangle<INT>::CS_XRightYDown)))
		{
			return false;
		}

		if (!testOverlappingRect(Rectangle<INT>(-40, 20, 255, 40, Rectangle<INT>::CS_XRightYDown), Rectangle<INT>(10, 20, 90, 40, Rectangle<INT>::CS_XRightYDown)))
		{
			return false;
		}

		if (!testOverlappingRect(Rectangle<INT>(20, -100, 50, 255, Rectangle<INT>::CS_XRightYDown), Rectangle<INT>(20, 10, 50, 90, Rectangle<INT>::CS_XRightYDown)))
		{
			return false;
		}

		if (!testOverlappingRect(Rectangle<INT>(-100, -100, 500, 500, Rectangle<INT>::CS_XRightYDown), Rectangle<INT>(10, 10, 90, 90, Rectangle<INT>::CS_XRightYDown)))
		{
			return false;
		}

		if (!testOverlappingRect(Rectangle<INT>(40, 45, 50, 55, Rectangle<INT>::CS_XRightYDown), Rectangle<INT>(40, 45, 50, 55, Rectangle<INT>::CS_XRightYDown)))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Rectangle"));

	return true;
}

bool DatatypeUnitTester::TestSDFunction (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("SDFunction"));

	SDFunctionTester* functionTester = SDFunctionTester::CreateObject();
	if (functionTester == nullptr)
	{
		UnitTestError(testFlags, TXT("Function unit test failed.  Failed to instantiate SDFunctionTester object."));
		return false;
	}

	//This is handled externally since the function test needs to test instanced objects.
	bool passedTests = functionTester->RunTest(this, testFlags);
	functionTester->Destroy();

	if (passedTests)
	{
		ExecuteSuccessSequence(testFlags, TXT("SDFunctionTester"));
	}

	return passedTests;
}

bool DatatypeUnitTester::TestMatrix (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Matrix"));

	Stopwatch matrixTimer(TXT("MatrixTimer"));
	std::function<void()> logTimer = [&matrixTimer, testFlags]()
	{
		TestLog(testFlags, TXT("The matrix unit test was running for %s milliseconds."), matrixTimer.GetElapsedTime());
	};

	Matrix voidMatrix = Matrix::InvalidMatrix;
	Matrix matrix1x5(1, 5);
	Matrix matrix5x1(5, 1);
	Matrix matrix2x2(2, 2);
	Matrix matrix6x6(6, 6);
	Matrix invalidMatrix(0, 4);

	std::vector<FLOAT> matrixData;
	matrixData.push_back(0.f);
	matrixData.push_back(1.f);
	matrixData.push_back(2.f);
	matrixData.push_back(3.f);
	matrix2x2.SetData(matrixData);

	matrixData.push_back(4.f);
	matrix1x5.SetData(matrixData);
	matrix5x1.SetData(matrixData);

	matrixData.push_back(5.f);
	matrixData.push_back(6.f);

	for (FLOAT curValue = 7.f; curValue < 36.f; curValue++)
	{
		matrixData.push_back(curValue);
	}
	matrix6x6.SetData(matrixData);

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("Construct tests.  The following matrices were created:"));
		TestLog(testFlags, TXT("    voidMatrix:  %s"), voidMatrix);
		voidMatrix.LogMatrix();
		TestLog(testFlags, TXT("    1x5 matrix:  %s"), matrix1x5);
		matrix1x5.LogMatrix();
		TestLog(testFlags, TXT("    5x1 matrix:  %s"), matrix5x1);
		matrix5x1.LogMatrix();
		TestLog(testFlags, TXT("    2x2 matrix:  %s"), matrix2x2);
		matrix2x2.LogMatrix();
		TestLog(testFlags, TXT("    6x6 matrix:  %s"), matrix6x6);
		matrix6x6.LogMatrix();
		TestLog(testFlags, TXT("    0x4 matrix:  %s"), invalidMatrix);
		invalidMatrix.LogMatrix();
	}

	SetTestCategory(testFlags, TXT("Matrix Validation"));
	TestLog(testFlags, TXT("Testing matrix validation.  Of the matrices listed above, only the 0x4 and matrix with too much data should be invalid."));
	if (!matrix1x5.IsValid())
	{
		UnitTestError(testFlags, TXT("Matrix tests failed.  The 1x5 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), {matrix1x5.ToString()});
		logTimer();
		return false;
	}

	if (!matrix5x1.IsValid())
	{
		UnitTestError(testFlags, TXT("Matrix tests failed.  The 5x1 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), {matrix5x1.ToString()});
		logTimer();
		return false;
	}

	if (!matrix2x2.IsValid())
	{
		UnitTestError(testFlags, TXT("Matrix tests failed.  The 2x2 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), {matrix2x2.ToString()});
		logTimer();
		return false;
	}

	if (!matrix6x6.IsValid())
	{
		UnitTestError(testFlags, TXT("Matrix tests failed.  The 6x6 matrix should be a valid matrix; instead it returned false for this matrix:  %s"), {matrix6x6.ToString()});
		logTimer();
		return false;
	}

	if (invalidMatrix.IsValid())
	{
		UnitTestError(testFlags, TXT("Matrix tests failed.  The 0x4 matrix should be an invalid matrix; instead it returned true for this matrix:  %s"), {invalidMatrix.ToString()});
		logTimer();
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Comparison"));
	TestLog(testFlags, TXT("Is the 1x5 matrix and 5x1 matrix equal?  %s"), BOOL(matrix1x5 == matrix5x1));
	if (matrix1x5 == matrix5x1)
	{
		UnitTestError(testFlags, TXT("Matrix comparison test failed.  Matrix {%s} should have not been equal to {%s}."), {matrix1x5.ToString(), matrix5x1.ToString()});
		logTimer();
		return false;
	}

	TestLog(testFlags, TXT("Is the 6x6 matrix equal to itself?  %s"), BOOL(matrix6x6 == matrix6x6));
	if (matrix6x6 != matrix6x6)
	{
		UnitTestError(testFlags, TXT("Matrix comparison test failed.  The 6x6 matrix should have been equal to itself."));
		logTimer();
		return false;
	}

	Matrix other2x2(matrix2x2);
	TestLog(testFlags, TXT("Created a copy of {%s}.  Is that copy equal to the 2x2 matrix?  %s"), matrix2x2, BOOL(matrix2x2 == other2x2));
	if (matrix2x2 != other2x2)
	{
		UnitTestError(testFlags, TXT("Matrix comparison test failed.  The 2x2 matrix {%s} should have been equal to its copy."), {matrix2x2.ToString()});
		logTimer();
		return false;
	}

	other2x2.At(1) = 100;
	TestLog(testFlags, TXT("Changed a value to the 2x2 copy.  Is {%s} equal to {%s} ?  %s"), matrix2x2, other2x2, BOOL(matrix2x2 == other2x2));
	if (matrix2x2 == other2x2)
	{
		UnitTestError(testFlags, TXT("Matrix comparison test failed.  The 2x2 matrix {%s} should have not been equal to {%s}."), {matrix2x2.ToString(), other2x2.ToString()});
		logTimer();
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Accessors"));
	Matrix accessor(3, 4);
	matrixData.clear();
	for (unsigned int i = 0; i < 12; i++)
	{
		matrixData.push_back(FLOAT::MakeFloat(i));
	}
	/*
	[0	1	2	3 ]
	[4	5	6	7 ]
	[8	9	10	11]
	*/
	accessor.SetData(matrixData);
	FLOAT dataElement = accessor.GetDataElement(1, 2);
	TestLog(testFlags, TXT("Retrieving data element from 2nd row and 3rd column from matrix {%s} returned %s"), accessor, dataElement);
	if (dataElement != 6.f)
	{
		UnitTestError(testFlags, TXT("Matrix accessor test failed.  Retrieving the data element at the 2nd row, 3rd column from matrix {%s} should have returned 6.  Instead it found %s"), {accessor.ToString(), dataElement.ToString()});
		logTimer();
		return false;
	}

	ContainerUtils::Empty(matrixData);
	matrixData = accessor.GetColumn(3);
	std::vector<FLOAT> accessorAnsKey;
	accessorAnsKey.push_back(3.f);
	accessorAnsKey.push_back(7.f);
	accessorAnsKey.push_back(11.f);
	TestLog(testFlags, TXT("Retrieving the last column from matrix {%s} returned %s"), accessor, DString::ListToString(matrixData));
	if (matrixData != accessorAnsKey)
	{
		UnitTestError(testFlags, TXT("Matrix accessor test failed.  Retrieving the last column from matrix {%s} should have resulted in %s.  Instead it found %s"), {accessor.ToString(), DString::ListToString(accessorAnsKey).ToString(), DString::ListToString(matrixData).ToString()});
		logTimer();
		return false;
	}

	matrixData.clear();
	matrixData = accessor.GetRow(1);
	accessorAnsKey.clear();
	accessorAnsKey.push_back(4.f);
	accessorAnsKey.push_back(5.f);
	accessorAnsKey.push_back(6.f);
	accessorAnsKey.push_back(7.f);
	TestLog(testFlags, TXT("Retrieving the second row from matrix {%s} returned %s"), accessor, DString::ListToString(matrixData));
	if (matrixData != accessorAnsKey)
	{
		UnitTestError(testFlags, TXT("Matrix accessor test failed.  Retrieving the second row from matrix {%s} should have resulted in %s.  Instead it found %s"), {accessor.ToString(), DString::ListToString(accessorAnsKey).ToString(), DString::ListToString(matrixData).ToString()});
		logTimer();
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Operations"));
	Matrix a(2, 4);
	Matrix b(2, 4);
	matrixData.clear();
	matrixData.push_back(5.f);
	matrixData.push_back(12.f);
	matrixData.push_back(0.52f);
	matrixData.push_back(-15.f);
	matrixData.push_back(-4.5f);
	matrixData.push_back(9.2f);
	matrixData.push_back(18.1f);
	matrixData.push_back(-92.68f);
	a.SetData(matrixData);

	std::vector<FLOAT> results = matrixData;

	matrixData.clear();
	matrixData.push_back(3.f);
	matrixData.push_back(16.f);
	matrixData.push_back(0.61f);
	matrixData.push_back(-12.f);
	matrixData.push_back(-8.2f);
	matrixData.push_back(5.5f);
	matrixData.push_back(29.9f);
	matrixData.push_back(92.68f);
	b.SetData(matrixData);

	for (UINT_TYPE i = 0; i < matrixData.size(); i++)
	{
		results.at(i) += matrixData.at(i);
	}

	Matrix resultMatrix(2, 4);
	Matrix answerKey(2, 4);
	answerKey.SetData(results);
	resultMatrix = a + b;
	TestLog(testFlags, TXT("The sum of %s and %s equals %s"), a, b, resultMatrix);
	if (resultMatrix != answerKey)
	{
		UnitTestError(testFlags, TXT("Matrix operator test failed.  The sum of %s and %s should have resulted in %s.  Instead it's %s"), {a.ToString(), b.ToString(), answerKey.ToString(), resultMatrix.ToString()});
		logTimer();
		return false;
	}

	for (UINT_TYPE i = 0; i < a.GetNumElements(); i++)
	{
		answerKey.At(i) = a.At(i) - b.At(i);
	}
	resultMatrix = a - b;
	TestLog(testFlags, TXT("The difference of %s and %s equals %s"), a, b, resultMatrix);
	if (resultMatrix != answerKey)
	{
		UnitTestError(testFlags, TXT("Matrix operator test failed.  The difference of %s and %s should have resulted in %s.  Instead it's %s"), {a.ToString(), b.ToString(), answerKey.ToString(), resultMatrix.ToString()});
		logTimer();
		return false;
	}

	FLOAT scaler = 4.5f;
	for (UINT_TYPE i = 0; i < answerKey.GetNumElements(); i++)
	{
		answerKey.At(i) *= 4.5f;
	}
	Matrix oldResult(resultMatrix);
	resultMatrix *= scaler;
	TestLog(testFlags, TXT("Scaling matrix %s by %s equals %s"), oldResult, scaler, resultMatrix);
	if (resultMatrix != answerKey)
	{
		UnitTestError(testFlags, TXT("Matrix operator test failed.  Scaling matrix %s by %s should have resulted in %s.  Instead it's %s"), {oldResult.ToString(), scaler.ToString(), answerKey.ToString(), resultMatrix.ToString()});
		logTimer();
		return false;
	}

	//Make sure the ForEachElement method works before using it
	{
		Matrix forEachTest(5, 2,
		{
			1, 1,
			2, 3,
			5, 8,
			13, 21,
			34, 55
		});

		FLOAT expectedSum = 143.f;
		FLOAT counter = 0;
		forEachTest.ForEachElement([&](UINT_TYPE i)
		{
			counter += forEachTest.At(i);
		});

		if (counter != expectedSum)
		{
			UnitTestError(testFlags, TXT("the Matrix ForEachElement test failed.  Taking the sum of all elements from the matrix listed below should have resulted in %s.  Instead it returned %s."), {expectedSum.ToString(), counter.ToString()});
			forEachTest.LogMatrix();
			logTimer();
			return false;
		}
	}

	Matrix aMultiply(2, 2);
	Matrix bMultiply(2, 2);
	Matrix multiplyAnsKey(2, 2);
	Matrix multiplyResult(2, 2);
	matrixData.clear();
	matrixData.push_back(4.f);
	matrixData.push_back(9.f);
	matrixData.push_back(2.5f);
	matrixData.push_back(-2.f);
	aMultiply.SetData(matrixData);

	matrixData.clear();
	matrixData.push_back(6.f);
	matrixData.push_back(7.f);
	matrixData.push_back(4.25f);
	matrixData.push_back(-4.f);
	bMultiply.SetData(matrixData);

	matrixData.clear();
	matrixData.push_back(62.25f);
	matrixData.push_back(-8.f);
	matrixData.push_back(6.5f);
	matrixData.push_back(25.5f);
	multiplyAnsKey.SetData(matrixData);

	multiplyResult = aMultiply * bMultiply;
	//Round off accumulated float precision errors
	aMultiply.ForEachElement([&](UINT_TYPE i){aMultiply.At(i).RoundInline(3);});
	bMultiply.ForEachElement([&](UINT_TYPE i){bMultiply.At(i).RoundInline(3);});
	multiplyAnsKey.ForEachElement([&](UINT_TYPE i){multiplyAnsKey.At(i).RoundInline(3);});
	multiplyResult.ForEachElement([&](UINT_TYPE i){multiplyResult.At(i).RoundInline(3);});
	TestLog(testFlags, TXT("Multiplying %s with %s is equal to %s"), aMultiply, bMultiply, multiplyResult);
	if (multiplyResult != multiplyAnsKey)
	{
		UnitTestError(testFlags, TXT("Matrix operator test failed.  Multiplying %s with %s should have resulted in %s.  Instead it's equal to %s."), {aMultiply.ToString(), bMultiply.ToString(), multiplyAnsKey.ToString(), multiplyResult.ToString()});
		logTimer();
		return false;
	}

	Matrix aRectMultiply(3, 2,
	{
		2.f, 4.f,
		6.5f, 8.f,
		-10.f, -18.23f
	});

	Matrix bRectMultiply(2, 3,
	{
		18.2f, -3.f, -12.7f,
		2.f, 100.f, 12.5f
	});

	Matrix rectMultAnsKey(3, 3,
	{
		44.4f, 394.f, 24.6f,
		134.3f, 780.5f, 17.45f,
		-218.46f, -1793.f, -100.875f
	});

	Matrix rectMultResult(3, 3);
	rectMultResult = aRectMultiply * bRectMultiply;

	//Round off accumulated float precision errors
	aRectMultiply.ForEachElement([&](UINT_TYPE i){aRectMultiply.At(i).RoundInline(3);});
	bRectMultiply.ForEachElement([&](UINT_TYPE i){bRectMultiply.At(i).RoundInline(3);});
	rectMultAnsKey.ForEachElement([&](UINT_TYPE i){rectMultAnsKey.At(i).RoundInline(3);});
	rectMultResult.ForEachElement([&](UINT_TYPE i){rectMultResult.At(i).RoundInline(3);});
	TestLog(testFlags, TXT("Multiplying %s with %s is equal to %s"), aRectMultiply, bRectMultiply, rectMultResult);

	//comparing operators
	for (UINT_TYPE i = 0; i < rectMultResult.GetNumElements(); i++)
	{
		if (rectMultResult.At(i) != rectMultAnsKey.At(i))
		{
			UnitTestError(testFlags, TXT("Elements:  %s and %s do not match."), {rectMultResult.At(i).ToString(), rectMultAnsKey.At(i).ToString()});
			logTimer();
			return false;
		}
	}

	if (rectMultResult != rectMultAnsKey)
	{
		UnitTestError(testFlags, TXT("Matrix operator test failed.  Multiplying %s with %s should have resulted in %s.  Instead it's equal to %s."), {aRectMultiply.ToString(), bRectMultiply.ToString(), rectMultAnsKey.ToString(), rectMultResult.ToString()});
		logTimer();
		return false;
	}

	TestLog(testFlags, TXT("Note: The Matrix test will later test division after it verified the inverse functions are working correctly."));
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Streaming"));
	{
		//Create a 6x4 matrix
		Matrix streamMatrix(6, 4,
		{
			26.4f,		-2.5f,		82.18f,		981.7f,
			10.9f,		-946.7f,	-715.3f,	55.05f,
			915.6f,		7418.6f,	-210.64f,	7182.f,
			3648.13f,	836.4f,		-8745.48f,	1874.34f,
			184.1f,		-48.15f,	6.5f,		9461.f,
			5432.01f,	-4842.48f,	-8496.1f,	184.123f
		});

		//Create a 1x3 matrix
		Matrix smallStreamMatrix(1, 3, {496.15f, 84715.16f, -4845.57f});
		DataBuffer matrixStream;

		matrixStream << streamMatrix;
		matrixStream << smallStreamMatrix;

		Matrix readStreamMatrix;
		Matrix readSmallStreamMatrix;
		matrixStream >> readStreamMatrix;
		matrixStream >> readSmallStreamMatrix;

		if (readStreamMatrix != streamMatrix)
		{
			UnitTestError(testFlags, TXT("Failed to stream matrices.  The following matrix was written to buffer:  %s.  The following matrix was read from buffer:  %s."), {streamMatrix.ToString(), readStreamMatrix.ToString()});
			logTimer();
			return false;
		}

		if (readSmallStreamMatrix != smallStreamMatrix)
		{
			UnitTestError(testFlags, TXT("Failed to stream matrices.  The following matrix was written to buffer:  %s.  The following matrix was read from buffer:  %s."), {smallStreamMatrix.ToString(), readSmallStreamMatrix.ToString()});
			logTimer();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Utilities"));
	TestLog(testFlags, TXT("Is a 2x2 matrix a square?  %s"), BOOL(matrix2x2.IsSquare()));
	if (!matrix2x2.IsSquare())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  A 2x2 matrix should be considered a square matrix but IsSquare returned false."));
		logTimer();
		return false;
	}

	TestLog(testFlags, TXT("Is 1x5 matrix a square?  %s"), BOOL(matrix1x5.IsSquare()));
	if (matrix1x5.IsSquare())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  A 1x5 matrix should not have been considered a square matrix, but IsSquare returned true."));
		logTimer();
		return false;
	}

	//Test upper/lower triangular, unitriangular, and unit matrices.
	/*
	[5	5	5]
	[0	5	5]
	[0	0	5]
	*/
	Matrix upperTriangle(3, 3,
	{
		5, 5, 5,
		0, 5, 5,
		0, 0, 5
	});

	/*
	[5	0	0]
	[5	5	0]
	[5	5	5]
	*/
	Matrix lowerTriangle(3, 3,
	{
		5, 0, 0,
		5, 5, 0,
		5, 5, 5
	});

	/*
	[1	0	0]
	[5	1	0]
	[5	5	1]
	*/
	Matrix unitriangularMatrix(3, 3,
	{
		1, 0, 0,
		5, 1, 0,
		5, 5, 1
	});

	/*
	[1	0	0]
	[0	1	0]
	[0	0	1]
	*/
	Matrix unitMatrix(3, 3,
	{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1
	});

	TestLog(testFlags, TXT("Constructed the following matrices:  upperTriangle, lowerTriangle, unitriangularMatrix, unitMatrix respectively. . ."));
	if (ShouldHaveDebugLogs(testFlags))
	{
		lowerTriangle.LogMatrix();
		TestLog(testFlags, TXT("-------------------"));
		upperTriangle.LogMatrix();
		TestLog(testFlags, TXT("-------------------"));
		unitriangularMatrix.LogMatrix();
		TestLog(testFlags, TXT("-------------------"));
		unitMatrix.LogMatrix();
	}

	//Test lower triangular matrix function
	TestLog(testFlags, TXT("lowerTriangle actually a lower triangular matrix?  %s.  upperTriangle actually a lower triangular matrix?  %s.  UnitriangularMatrix actually a lower triangle matrix?  %s.  Unit matrix actually a lower triangle matrix?  %s."), BOOL(lowerTriangle.IsLowerTriangularMatrix()), BOOL(upperTriangle.IsLowerTriangularMatrix()), BOOL(unitriangularMatrix.IsLowerTriangularMatrix()), BOOL(unitMatrix.IsLowerTriangularMatrix()));
	if (!lowerTriangle.IsLowerTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered a lower triangular matrix.  Instead it returned false."), {lowerTriangle.ToString()});
		logTimer();
		return false;
	}

	if (upperTriangle.IsLowerTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered a lower triangular matrix.  Instead it returned true."), {upperTriangle.ToString()});
		logTimer();
		return false;
	}

	//Skipping lower triangle check for unitriangular matrix since it can be either lower or upper triangular

	if (!unitMatrix.IsLowerTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered a lower triangular matrix.  Instead it returned false."), {unitMatrix.ToString()});
		logTimer();
		return false;
	}

	//Test upper triangular matrix function
	TestLog(testFlags, TXT("lowerTriangle actually an upper triangular matrix?  %s.  upperTriangle actually an upper triangular matrix?  %s.  UnitriangularMatrix actually a lower triangle matrix?  %s.  Unit matrix actually a lower triangle matrix?  %s."), BOOL(lowerTriangle.IsUpperTriangularMatrix()), BOOL(upperTriangle.IsUpperTriangularMatrix()), BOOL(unitriangularMatrix.IsUpperTriangularMatrix()), BOOL(unitMatrix.IsUpperTriangularMatrix()));
	if (lowerTriangle.IsUpperTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an upper triangular matrix.  Instead it returned true."), {lowerTriangle.ToString()});
		logTimer();
		return false;
	}

	if (!upperTriangle.IsUpperTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered an upper triangular matrix.  Instead it returned false."), {upperTriangle.ToString()});
		logTimer();
		return false;
	}

	//Skipping upper triangle check for unitriangular matrix since it can be either lower or upper triangular

	if (!unitMatrix.IsUpperTriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered an upper triangular matrix.  Instead it returned false."), {unitMatrix.ToString()});
		logTimer();
		return false;
	}

	//Test unitriangular matrix function
	TestLog(testFlags, TXT("lowerTriangle actually an unitriangular matrix?  %s.  upperTriangle actually an unitriangular matrix?  %s.  UnitriangularMatrix actually an unitriangular matrix?  %s.  Unit matrix actually an unitriangular matrix?  %s."), BOOL(lowerTriangle.IsUnitriangularMatrix()), BOOL(upperTriangle.IsUnitriangularMatrix()), BOOL(unitriangularMatrix.IsUnitriangularMatrix()), BOOL(unitMatrix.IsUnitriangularMatrix()));
	if (lowerTriangle.IsUnitriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an unitriangular matrix.  Instead it returned true."), {lowerTriangle.ToString()});
		logTimer();
		return false;
	}

	if (upperTriangle.IsUnitriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an unitriangular matrix.  Instead it returned true."), {upperTriangle.ToString()});
		logTimer();
		return false;
	}

	if (!unitriangularMatrix.IsUnitriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered an unitriangular matrix.  Instead it returned false."), {unitriangularMatrix.ToString()});
		logTimer();
		return false;
	}

	if (!unitMatrix.IsUnitriangularMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered an unitriangular matrix.  Instead it returned false."), {unitMatrix.ToString()});
		logTimer();
		return false;
	}

	//Test unit matrix function
	TestLog(testFlags, TXT("lowerTriangle actually an unit matrix?  %s.  upperTriangle actually an unit matrix?  %s.  UnitriangularMatrix actually an unit matrix?  %s.  Unit matrix actually an unit matrix?  %s."), BOOL(lowerTriangle.IsUnitMatrix()), BOOL(upperTriangle.IsUnitMatrix()), BOOL(unitriangularMatrix.IsUnitMatrix()), BOOL(unitMatrix.IsUnitMatrix()));
	if (lowerTriangle.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an unit matrix.  Instead it returned true."), {lowerTriangle.ToString()});
		logTimer();
		return false;
	}

	if (upperTriangle.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an unit matrix.  Instead it returned true."), {upperTriangle.ToString()});
		logTimer();
		return false;
	}

	if (unitriangularMatrix.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should not have been considered an unit matrix.  Instead it returned true."), {unitriangularMatrix.ToString()});
		logTimer();
		return false;
	}

	if (!unitMatrix.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix {%s} should have been considered an unit matrix.  Instead it returned false."), {unitMatrix.ToString()});
		logTimer();
		return false;
	}

	Matrix zeroMatrix = matrix6x6.GetZeroMatrix();
	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The zero matrix of the 6x6 matrix listed below is listed below."));
		matrix6x6.LogMatrix();
		TestLog(testFlags, TXT("--- Zero Matrix listed below ---"));
		zeroMatrix.LogMatrix();
	}

	for (UINT_TYPE i = 0; i < matrix6x6.GetNumElements(); i++)
	{
		if (zeroMatrix.At(i) != 0.f)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  All elements of a zero matrix should be zero.  Instead element %s is not zero in matrix:  {%s}"), {INT(i).ToString(), zeroMatrix.ToString()});
			logTimer();
			return false;
		}
	}

	Matrix identityMatrix(6, 6);
	Matrix ansKey6x6(6, 6);

	/*
	[1	0	0	0	0	0]
	[0	1	0	0	0	0]
	[0	0	1	0	0	0]
	[0	0	0	1	0	0]
	[0	0	0	0	1	0]
	[0	0	0	0	0	1]
	*/
	ansKey6x6.SetData( 
	{
		1, 0, 0, 0, 0, 0,
		0, 1, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0,
		0, 0, 0, 1, 0, 0,
		0, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 0, 1
	});

	matrix6x6.GetIdentityMatrix(identityMatrix);
	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The identity matrix of the 6x6 matrix listed below is listed below."));
		matrix6x6.LogMatrix();
		TestLog(testFlags, TXT("--- Identity Matrix listed below ---"));
		identityMatrix.LogMatrix();
	}

	if (identityMatrix != ansKey6x6)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The identity matrix of the 6x6 matrix should have been {%s}.  Instead it returned {%s}"), {ansKey6x6.ToString(), identityMatrix.ToString()});
		logTimer();
		return false;
	}

	//Transpose matrix test
	{
		Matrix testMatrix(2, 2,
		{
			1, 2,
			3, 4
		});

		Matrix expectedTransposeMatrix(2, 2,
		{
			1, 3,
			2, 4
		});

		Matrix transposeMatrix = testMatrix.GetTransposeMatrix();
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The original matrix is:"));
			testMatrix.LogMatrix();
			TestLog(testFlags, TXT("--- Its transpose matrix is ---"));
			transposeMatrix.LogMatrix();
		}

		if (transposeMatrix != expectedTransposeMatrix)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The transpose matrix does not match the expected {%s}."), {expectedTransposeMatrix.ToString()});
			logTimer();
			return false;
		}

		testMatrix = Matrix(2, 4,
		{
			0, 1, 2, 3,
			4, 5, 6, 7
		});

		expectedTransposeMatrix = Matrix(4, 2,
		{
			0, 4,
			1, 5,
			2, 6,
			3, 7
		});

		transposeMatrix = testMatrix.GetTransposeMatrix();
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The original matrix is:"));
			testMatrix.LogMatrix();
			TestLog(testFlags, TXT("--- Its transpose matrix is ---"));
			transposeMatrix.LogMatrix();
		}

		if (transposeMatrix != expectedTransposeMatrix)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The transpose matrix does not match the expected {%s}."), {expectedTransposeMatrix.ToString()});
			logTimer();
			return false;
		}

		testMatrix = Matrix(4, 4,
		{
			0, 1, 2, 3,
			4, 5, 6, 7,
			8, 9, 10, 11,
			12, 13, 14, 15
		});

		expectedTransposeMatrix = Matrix(4, 4,
		{
			0, 4, 8, 12,
			1, 5, 9, 13,
			2, 6, 10, 14,
			3, 7, 11, 15
		});

		transposeMatrix = testMatrix.GetTransposeMatrix();
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The original matrix is:"));
			testMatrix.LogMatrix();
			TestLog(testFlags, TXT("--- Its transpose matrix is ---"));
			transposeMatrix.LogMatrix();
		}

		if (transposeMatrix != expectedTransposeMatrix)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The transpose matrix does not match the expected {%s}."), {expectedTransposeMatrix.ToString()});
			logTimer();
			return false;
		}
	}

	//Orthogonal matrix text
	{
		const FLOAT orthoTolerance = 0.00001f;
		Matrix orthoTest(2, 2,
		{
			1, 0,
			0, 1
		});

		if (!orthoTest.IsOrthogonalMatrix(orthoTolerance))
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The matrix {%s} should have been considered an orthogonal matrix, but it is not."), {orthoTest.ToString()});
			logTimer();
			return false;
		}

		orthoTest.SetData(
		{
			1, 0,
			0, -1
		});

		if (!orthoTest.IsOrthogonalMatrix(orthoTolerance))
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The matrix {%s} should have been considered an orthogonal matrix, but it is not."), {orthoTest.ToString()});
			logTimer();
			return false;
		}

		const FLOAT oneOverSqrtTwo = 0.707106781f;
		orthoTest.SetData(
		{
			oneOverSqrtTwo, oneOverSqrtTwo,
			oneOverSqrtTwo, -oneOverSqrtTwo
		});

		if (!orthoTest.IsOrthogonalMatrix(orthoTolerance))
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The matrix {%s} should have been considered an orthogonal matrix, but it is not."), {orthoTest.ToString()});
			logTimer();
			return false;
		}

		orthoTest.SetData(
		{
			0.f, 0.5f,
			2.f, 0.f
		});

		if (orthoTest.IsOrthogonalMatrix(orthoTolerance))
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The matrix {%s} should have NOT been considered an orthogonal matrix, but it is instead."), {orthoTest.ToString()});
			logTimer();
			return false;
		}

		orthoTest.SetData(
		{
			2.f, 0.f,
			0.f, 0.5f
		});

		if (orthoTest.IsOrthogonalMatrix(orthoTolerance))
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed. The matrix {%s} should have NOT been considered an orthogonal matrix, but it is instead."), {orthoTest.ToString()});
			logTimer();
			return false;
		}
	}

	Matrix coMatrix = matrix6x6.GetComatrix();
	ansKey6x6.SetData(
	{
		0, -1, 2, -3, 4, -5,
		-6, 7, -8, 9, -10, 11,
		12, -13, 14, -15, 16, -17,
		-18, 19, -20, 21, -22, 23,
		24, -25, 26, -27, 28, -29,
		-30, 31, -32, 33, -34, 35
	});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The comatrix of the 6x6 matrix listed below is listed below."));
		matrix6x6.LogMatrix();
		TestLog(testFlags, TXT("--- Comatrix listed below ---"));
		coMatrix.LogMatrix();
	}

	if (coMatrix != ansKey6x6)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The comatrix of the 6x6 matrix should have been {%s}.  Instead it returned {%s}"), {ansKey6x6.ToString(), coMatrix.ToString()});
		logTimer();
		return false;
	}

	//SubMatrix test section
	{
		/*
		[0	1	2	3	5 ]
		[6	7	8	9	11]
		[12	13	14	15	17]
		[24	25	26	27	29]
		[30	31	32	33	35]
		*/
		Matrix subMatrix = matrix6x6.GetSubMatrix(3, 4);
		Matrix subMatrixAns(5, 5);

		subMatrixAns.SetData(
		{
			0, 1, 2, 3, 5,
			6, 7, 8, 9, 11,
			12, 13, 14, 15, 17,
			24, 25, 26, 27, 29,
			30, 31, 32, 33, 35
		});

		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The sub matrix of the 6x6 matrix (listed below) that excludes row idx 3 column idx 4 is listed below."));
			matrix6x6.LogMatrix();
			TestLog(testFlags, TXT("--- Submatrix listed below ---"));
			subMatrix.LogMatrix();
		}

		if (subMatrix != subMatrixAns)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix is not equal to the expected matrix.  See logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix that excludes row index 3 and column index 4 should have been the matrix below."));
				subMatrixAns.LogMatrix();
				TestLog(testFlags, TXT("Instead, the sub matrix is. . ."));
				subMatrix.LogMatrix();
			}

			logTimer();
			return false;
		}

		/*
		0  1  2  3  4  5
		6  7  8  9  10 11
		12 13 14 15 16 17
		18 19 20 21 22 23
		24 25 26 27 28 29
		30 31 32 33 34 35
		*/

		/*
		[9	10	11]
		[15	16	17]
		[21	22	23]
		*/
		subMatrix = matrix6x6.GetSubMatrix(1, 3, 3, 5);
		subMatrixAns = Matrix(3, 3,
		{
			9, 10, 11,
			15, 16, 17,
			21, 22, 23
		});

		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The sub matrix of the 6x6 matrix (listed below) that only includes row indices 1-3 and column indices 3-5 is listed below."));
			matrix6x6.LogMatrix();
			TestLog(testFlags, TXT("--- Submatrix listed below ---"));
			subMatrix.LogMatrix();
		}

		if (subMatrix != subMatrixAns)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix is not equal to the expected matrix.  See logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix with row indices 1-3 and column indices 3-5 should have been the matrix below."));
				subMatrixAns.LogMatrix();
				TestLog(testFlags, TXT("Instead, the sub matrix is. . ."));
				subMatrix.LogMatrix();
			}

			logTimer();
			return false;
		}

		/*
		[1	2	3	4	5 ]
		[7	8	9	10	11]
		[13	14	15	16	17]
		*/
		subMatrix = matrix6x6.GetSubMatrix(0, 2, 1, 5);
		subMatrixAns = Matrix(3, 5,
		{
			1, 2, 3, 4, 5,
			7, 8, 9, 10, 11,
			13, 14, 15, 16, 17
		});

		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("The sub matrix of the 6x6 matrix (listed below) that only includes row indices 0-2 and column indices 1-5 is listed below."));
			matrix6x6.LogMatrix();
			TestLog(testFlags, TXT("--- Submatrix listed below ---"));
			subMatrix.LogMatrix();
		}

		if (subMatrix != subMatrixAns)
		{
			UnitTestError(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix is not equal to the expected matrix.  See logs for details."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Matrix utility test failed.  The submatrix of the 6x6 matrix with row indices 0-2 and column indices 1-5 should have been the matrix below."));
				subMatrixAns.LogMatrix();
				TestLog(testFlags, TXT("Instead, the sub matrix is. . ."));
				subMatrix.LogMatrix();
			}

			logTimer();
			return false;
		}
	}

	FLOAT deter2x2 = -2.f;
	TestLog(testFlags, TXT("The determinant of {%s} is %s"), matrix2x2, matrix2x2.CalculateDeterminant());
	if (matrix2x2.CalculateDeterminant() != deter2x2)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The determinant for a matrix {%s} should have been %s.  Instead it returned %s"), {matrix2x2.ToString(), deter2x2.ToString(), matrix2x2.CalculateDeterminant().ToString()});
		logTimer();
		return false;
	}

	//Example given is based from KhanAcademy.org:  https://www.khanacademy.org/math/precalculus/precalc-matrices/determinants-and-inverses-of-large-matrices/v/finding-the-determinant-of-a-3x3-matrix-method-1
	Matrix deterMatrix3x3(3, 3, 
	{
		4, -1, 1,
		4, 5, 3,
		-2, 0, 0
	});
	FLOAT deter3x3Ans = 16.f;
	FLOAT deter3x3 = deterMatrix3x3.CalculateDeterminant();

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The determinant of the matrix listed below is:  %s"), deter3x3);
		deterMatrix3x3.LogMatrix();
	}

	if (deter3x3 != deter3x3Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The determinant for a matrix {%s} should have been %s.  Instead it returned %s"), {deterMatrix3x3.ToString(), deter3x3Ans.ToString(), deter3x3.ToString()});
		logTimer();
		return false;
	}

	//Answer to the given matrix was generated from:  http://www.wolframalpha.com/input/?i=determinant
	/*
	[15		-7		1.4		-2.8]
	[-8.2	12		-14		4.6 ]
	[9.1	17.3	5		-22 ]
	[-12	-3.25	-10		7   ]
	*/
	Matrix deterMatrix4x4(4, 4,
	{
		15.f, -7.f, 1.4f, -2.8f,
		-8.2f, 12.f, -14.f, 4.6f,
		9.1f, 17.3f, 5.f, -22.f,
		-12.f, -3.25f, -10.f, 7.f
	});
	FLOAT deter4x4Ans = -42104.237f;
	FLOAT deter4x4 = deterMatrix4x4.CalculateDeterminant();
	deter4x4.RoundInline(3); //Clear any accumulated precision errors

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The determinant of the matrix listed below is:  %s"), deter4x4);
		deterMatrix4x4.LogMatrix();
	}

	if (deter4x4 != deter4x4Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The determinant for a matrix {%s} should have been %s.  Instead it returned %s"), {deterMatrix4x4.ToString(), deter4x4Ans.ToString(), deter4x4.ToString()});
		logTimer();
		return false;
	}

	//Example given is based from KhanAcademy.org:  https://www.khanacademy.org/math/precalculus/precalc-matrices/determinants-and-inverses-of-large-matrices/v/inverting-3x3-part-1-calculating-matrix-of-minors-and-cofactor-matrix
	/*
	[-1	-2	2]
	[2	1	1]
	[3	4	5]
	*/
	Matrix majorMatrix(3, 3,
	{
		-1, -2, 2,
		2, 1, 1,
		3, 4, 5
	});

	/*
	[1		7		5]
	[-18	-11		2]
	[-4		-5		3]
	*/
	Matrix minorMatrixAns(3, 3,
	{
		1, 7, 5,
		-18, -11, 2,
		-4, -5, 3
	});

	Matrix minorMatrix(3, 3);
	majorMatrix.GetMatrixOfMinors(minorMatrix);
	minorMatrix.ForEachElement([&](UINT_TYPE i){minorMatrix.At(i).RoundInline(3);});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The matrix of minors for the matrix listed below is listed below."));
		TestLog(testFlags, TXT("---- Original Matrix ----"));
		majorMatrix.LogMatrix();
		TestLog(testFlags, TXT("---- Matrix of minors ----"));
		minorMatrix.LogMatrix();
	}

	if (minorMatrix != minorMatrixAns)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The matrix of minors for {%s} should have been {%s}.  Instead it returned {%s}"), {majorMatrix.ToString(), minorMatrixAns.ToString(), minorMatrix.ToString()});
		logTimer();
		return false;
	}

	/*
	[2	-6]
	[8	-4]
	*/
	Matrix adjugate2x2(2, 2,
	{
		2, -6,
		8, -4
	});

	/*
	[-4	6]
	[-8	2]
	*/
	Matrix adjugate2x2Ans(2, 2,
	{
		-4, 6,
		-8, 2
	});

	Matrix adjugate2x2Result(2, 2);
	adjugate2x2.CalculateAdjugate(adjugate2x2Result);

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("---- The adjugate of this matrix ----"));
		adjugate2x2.LogMatrix();
		TestLog(testFlags, TXT("---- is ----"));
		adjugate2x2Result.LogMatrix();
	}

	if (adjugate2x2Result != adjugate2x2Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The adjugate of matrix {%s} should have been {%s}.  Instead it returned {%s}"), {adjugate2x2.ToString(), adjugate2x2Ans.ToString(), adjugate2x2Result.ToString()});
		logTimer();
		return false;
	}

	/*
	[3	1	2 ]
	[9	-7	-6]
	[4	8	-5]
	*/
	Matrix adjugate3x3(3, 3,
	{
		3, 1, 2,
		9, -7, -6,
		4, 8, -5
	});

	/*
	Answer key generated through:  http://www.wolframalpha.com/input/?i=adjugate
	[83		21		8  ]
	[21		-23		36 ]
	[100	-20		-30]
	*/
	Matrix adjugate3x3Ans(3, 3,
	{
		83.f, 21.f, 8.f,
		21.f, -23.f, 36.f,
		100.f, -20.f, -30.f
	});

	Matrix adjugate3x3Result(3, 3);
	adjugate3x3.CalculateAdjugate(adjugate3x3Result);

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("---- The adjugate of this matrix ----"));
		adjugate3x3.LogMatrix();
		TestLog(testFlags, TXT("---- is ----"));
		adjugate3x3Result.LogMatrix();
	}
					
	if (adjugate3x3Result != adjugate3x3Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The adjugate of the matrix is not equal to the expected matrix.  See logs for details."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  The adjugate of original matrix listed below. . ."));
			adjugate3x3.LogMatrix();
			TestLog(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
			adjugate3x3Ans.LogMatrix();
			TestLog(testFlags, TXT(". . . Instead it generated this matrix. . ."));
			adjugate3x3Result.LogMatrix();
		}

		logTimer();
		return false;
	}

	/*
	[12		5		16		7.1 ]
	[-10	-3		4.5		-3  ]
	[11		6.2		8.5		-9.5]
	[4		9		-6		10.1]
	*/
	Matrix adjugate4x4(4, 4,
	{
		12.f, 5.f, 16.f, 7.1f,
		-10.f, -3.f, 4.5f, -3.f,
		11.f, 6.2f, 8.5f, -9.5f,
		4.f, 9.f, -6.f, 10.1f
	});

	/*
	Answer key generated through:  http://www.wolframalpha.com/input/?i=adjugate
	[-411.99	3032.94		30.3		1218.99 ]
	[659.45		-2749.4		-2051.6		-3209.95]
	[-1256.5	-1558.76	-148.4		280.7   ]
	[-1170.9	322.8		1728		-432.3  ]
	*/
	Matrix adjugate4x4Ans(4, 4,
	{
		-411.99f, 3032.94f, 30.3f, 1218.99f,
		659.45f, -2749.4f, -2051.6f, -3209.95f,
		-1256.5f, -1558.76f, -148.4f, 280.7f,
		-1170.9f, 322.8f, 1728.f, -432.3f
	});

	Matrix adjugate4x4Result(4, 4);
	adjugate4x4.CalculateAdjugate(adjugate4x4Result);

	//Remove accumulated precision errors
	adjugate4x4Result.ForEachElement([&](UINT_TYPE i){adjugate4x4Result.At(i).RoundInline(3);});
	adjugate4x4Ans.ForEachElement([&](UINT_TYPE i){adjugate4x4Ans.At(i).RoundInline(3);});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("---- The adjugate of this matrix ----"));
		adjugate4x4.LogMatrix();
		TestLog(testFlags, TXT("---- is ----"));
		adjugate4x4Result.LogMatrix();
	}

	if (adjugate4x4Result != adjugate4x4Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The adjugate of the matrix is not equal to the expected matrix.  See the logs for details."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  The adjugate of original matrix listed below. . ."));
			adjugate4x4.LogMatrix();
			TestLog(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
			adjugate4x4Ans.LogMatrix();
			TestLog(testFlags, TXT(". . . Instead it generated this matrix. . ."));
			adjugate4x4Result.LogMatrix();
		}

		logTimer();
		return false;
	}
	/*
	[2	8]
	[4	7]
	*/
	Matrix preInverse2x2(2, 2,
	{
		2, 8,
		4, 7
	});

	/*
	[-7/18	4/9]	=>	[-0.388889	0.444444 ]
	[2/9	-1/9]	=>	[0.222222	-0.111111]
	*/
	Matrix inverse2x2Ans(2, 2,
	{
		-0.388889f, 0.444444f,
		0.222222f, -0.111111f
	});

	Matrix inverse2x2(2, 2);
	preInverse2x2.CalculateInverse(inverse2x2);

	Matrix inverseIdentity2x2(2, 2);
	inverseIdentity2x2 = preInverse2x2 * inverse2x2;

	//Truncate some decimal places due to comparison ahead
	inverse2x2.ForEachElement([&](UINT_TYPE i){inverse2x2.At(i).RoundInline(3);});
	inverse2x2Ans.ForEachElement([&](UINT_TYPE i){inverse2x2Ans.At(i).RoundInline(3);});
	inverseIdentity2x2.ForEachElement([&](UINT_TYPE i){inverseIdentity2x2.At(i).RoundInline(3);});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The inverse of the matrix below. . ."));
		preInverse2x2.LogMatrix();
		TestLog(testFlags, TXT(". . . is approximately (rounded to 3 decimal places) . . ."));
		inverse2x2.LogMatrix();
	}

	if (inverse2x2 != inverse2x2Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The inverse of the matrix is not equal to the expected matrix.  See the logs for details."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  The inverse of original matrix listed below. . ."));
			preInverse2x2.LogMatrix();
			TestLog(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
			inverse2x2Ans.LogMatrix();
			TestLog(testFlags, TXT(". . . Instead it generated this matrix. . ."));
			inverse2x2.LogMatrix();
		}

		logTimer();
		return false;
	}

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("Multiplying the inverse with the original matrix resulted in the matrix listed below. . ."));
		inverseIdentity2x2.LogMatrix();
	}

	if (!inverseIdentity2x2.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix.  The matrix listed below is not classified as an unit matrix."));
			inverseIdentity2x2.LogMatrix();
		}

		logTimer();
		return false;
	}

	/*
	[20	23	20]
	[16	28	0]
	[26	24	15]
	*/
	Matrix preInverse3x3(3, 3,
	{
		20, 23, 20,
		16, 28, 0,
		26, 24, 15
	});

	/*
	Answer key generated through:  http://www.wolframalpha.com/input/?i=inverse+matrix
	1/4000	[-420	-135	560]	=>	[-0.105	-0.03375	0.14  ]
			[240	220		-320]	=>	[0.06	0.055		-0.08 ]
			[344	-118	-192]	=>	[0.086	-0.0295		-0.048]
	*/
	Matrix inverse3x3Ans(3, 3,
	{
		-0.105f, -0.03375f, 0.14f,
		0.06f, 0.055f, -0.08f,
		0.086f, -0.0295f, -0.048f
	});

	Matrix inverse3x3(3, 3);
	preInverse3x3.CalculateInverse(inverse3x3);

	Matrix inverseIdentity3x3(3, 3);
	inverseIdentity3x3 = preInverse3x3 * inverse3x3;

	//Truncate some decimal places due to comparison ahead
	inverse3x3.ForEachElement([&](UINT_TYPE i){inverse3x3.At(i).RoundInline(3);});
	inverse3x3Ans.ForEachElement([&](UINT_TYPE i){inverse3x3Ans.At(i).RoundInline(3);});
	inverseIdentity3x3.ForEachElement([&](UINT_TYPE i){inverseIdentity3x3.At(i).RoundInline(3);});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The inverse of the matrix below. . ."));
		preInverse3x3.LogMatrix();
		TestLog(testFlags, TXT(". . . is approximately (rounded to 3 decimal places) . . ."));
		inverse3x3.LogMatrix();
	}

	if (inverse3x3 != inverse3x3Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The inverse of the matrix is not equal to the expected matrix.  See the logs for details."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  The inverse of original matrix listed below. . ."));
			preInverse3x3.LogMatrix();
			TestLog(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
			inverse3x3Ans.LogMatrix();
			TestLog(testFlags, TXT(". . . Instead it generated this matrix. . ."));
			inverse3x3.LogMatrix();
		}

		logTimer();
		return false;
	}

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("Multiplying the inverse with the original matrix resulted in the matrix listed below. . ."));
		inverseIdentity3x3.LogMatrix();
	}

	if (!inverseIdentity3x3.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix.  The matrix listed below is not classified as an unit matrix."));
			inverseIdentity3x3.LogMatrix();
		}

		logTimer();
		return false;
	}

	/*
	[1.08	0.15	-0.85	-2.07 ]
	[-7.1	1.5		0.68	-11.8 ]
	[0.62	-1.30	5.4		0.75  ]
	[-8.75	-3.15	-4.5	3.7   ]
	*/
	Matrix preInverse4x4(4, 4,
	{
		1.08f, 0.15f, -0.85f, -2.07f,
		-7.1f, 1.5f, 0.68f, -11.8f,
		0.62f, -1.3f, 5.4f, 0.75f,
		-8.75f, -3.15f, -4.5f, 3.7f
	});

	/*
	Answer key generated through:  http://www.wolframalpha.com/input/?i=inverse+matrix
	[0.299873	-0.0590573	0.032073	-0.0270793]
	[-0.896168	0.0762782	-0.312901	-0.194678 ]
	[-0.20763	0.0303889	0.113471	-0.0422456]
	[-0.306317	-0.0377637	-0.0525347	-0.0108882]
	*/
	Matrix inverse4x4Ans(4, 4,
	{
		0.299873f, -0.0590573f, 0.032073f, -0.0270793f,
		-0.896168f, 0.0762782f, -0.312901f, -0.194678f,
		-0.20763f, 0.0303889f, 0.113471f, -0.0422456f,
		-0.306317f, -0.0377637f, -0.0525347f, -0.0108882f
	});

	Matrix inverse4x4(4, 4);
	preInverse4x4.CalculateInverse(inverse4x4);

	Matrix inverseIdentity4x4(4, 4);
	inverseIdentity4x4 = preInverse4x4 * inverse4x4;

	//Truncate some decimal places due to comparison ahead
	inverse4x4.ForEachElement([&](UINT_TYPE i){inverse4x4.At(i).RoundInline(6);});
	inverse4x4Ans.ForEachElement([&](UINT_TYPE i){inverse4x4Ans.At(i).RoundInline(6);});
	inverseIdentity4x4.ForEachElement([&](UINT_TYPE i){inverseIdentity4x4.At(i).RoundInline(6);});

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("The inverse of the matrix below. . ."));
		preInverse4x4.LogMatrix();
		TestLog(testFlags, TXT(". . . is approximately (rounded to 6 decimal places) . . ."));
		inverse4x4.LogMatrix();
	}

	if (inverse4x4 != inverse4x4Ans)
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  The inverse of the matrix is not equal to the expected matrix.  See the logs for details."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  The inverse of original matrix listed below. . ."));
			preInverse4x4.LogMatrix();
			TestLog(testFlags, TXT(". . . should have been equal to the marix listed below. . ."));
			inverse4x4Ans.LogMatrix();
			TestLog(testFlags, TXT(". . . Instead it generated this matrix. . ."));
			inverse4x4.LogMatrix();
		}

		logTimer();
		return false;
	}

	if (ShouldHaveDebugLogs(testFlags))
	{
		TestLog(testFlags, TXT("Multiplying the inverse with the original matrix resulted in the matrix listed below. . ."));
		inverseIdentity4x4.LogMatrix();
	}

	if (!inverseIdentity4x4.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix."));
		if (ShouldHaveDebugLogs(testFlags))
		{
			TestLog(testFlags, TXT("Matrix utility test failed.  Multiplying a matrix with its inverse should have resulted in an identity matrix.  The matrix listed below is not classified as an unit matrix."));
			inverseIdentity4x4.LogMatrix();
		}

		logTimer();
		return false;
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Matrix Division"));
	{
		Matrix originalMatrix(4, 4,
		{
			4.f, 40.f, 400.f, 1.f,
			-8.f, -16.f, -32.f, 4.f,
			12.f, -12.f, 24.f, -24.f,
			80.f, 60.f, 40.f, 20.f
		});

		Matrix scaledMatrix = originalMatrix / 4.f;
		Matrix expectedMatrix = Matrix(4, 4,
		{
			1.f, 10.f, 100.f, 0.25f,
			-2.f, -4.f, -8.f, 1.f,
			3.f, -3.f, 6.f, -6.f,
			20.f, 15.f, 10.f, 5.f
		});

		if (scaledMatrix != expectedMatrix)
		{
			UnitTestError(testFlags, TXT("Matrix division test failed. Expected matrix does not match the calculated scaled matrix."));

			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Original matrix..."));
				originalMatrix.LogMatrix();

				TestLog(testFlags, TXT("Calculated matrix after dividing original by 4..."));
				scaledMatrix.LogMatrix();

				TestLog(testFlags, TXT("Expected scaled matrix..."));
				expectedMatrix.LogMatrix();
			}

			logTimer();
			return false;
		}

		Matrix multiplier = Matrix(4, 4,
		{
			3.f, 4.f, 3.f, 10.f,
			-15.f, 8.f, 6.f, 0.f,
			-2.f, 3.f, 3.f, 7.f,
			-1.f, 1.f, 5.f, 1.f
		});

		scaledMatrix = originalMatrix * multiplier;
		scaledMatrix /= multiplier;

		if (!scaledMatrix.IsNearlyEqual(originalMatrix, 0.0001f))
		{
			UnitTestError(testFlags, TXT("Matrix division test failed. After multiplying a matrix by a matrix, dividing the resulting matrix by the inverse multiplier matrix should have resulted in the original matrix."));

			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Original matrix..."));
				originalMatrix.LogMatrix();

				TestLog(testFlags, TXT("Resulting matrix..."));
				scaledMatrix.LogMatrix();
			}

			logTimer();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Transform Matrix"));
	{
		TestLog(testFlags, TXT("Testing transform matrices (Scale)."));
		{
			std::function<bool(const TransformMatrix&, const Vector3&)> testScalarMatrix([&](const TransformMatrix& givenMatrix, const Vector3& expectedScalar)
			{
				Vector3 scalarVector = givenMatrix.GetScale();
				if (scalarVector != expectedScalar)
				{
					UnitTestError(testFlags, TXT("TransformMatrix::GetScale test failed. The resulting scalar vector is %s instead of %s."), {scalarVector.ToString(), expectedScalar.ToString()});
					if (ShouldHaveDebugLogs(testFlags))
					{
						TestLog(testFlags, TXT("Given transform matrix..."));
						givenMatrix.LogMatrix();
					}
					return false;
				}

				return true;
			});

			TransformMatrix testMatrix = TransformMatrix::IDENTITY_TRANSFORM;
			if (!testScalarMatrix(testMatrix, Vector3(1.f, 1.f, 1.f)))
			{
				return false;
			}

			Vector3 expectedScale(2.f, 4.f, 8.f);
			testMatrix.Scale(expectedScale);
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			//Setting translation should not affect scale.
			testMatrix.Translate(Vector3(1234.f, 4321.f, -50.f));
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			//Applying another scale to the same matrix
			expectedScale *= 3.f;
			testMatrix.Scale(Vector3(3.f, 3.f, 3.f));
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			//Test inversions
			expectedScale = Vector3(-0.5f, 2.f, 9.f);
			testMatrix = TransformMatrix::IDENTITY_TRANSFORM;
			testMatrix.Scale(expectedScale);
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			expectedScale = Vector3(-1.5f, -2.5f, 7.f);
			testMatrix = TransformMatrix::IDENTITY_TRANSFORM;
			testMatrix.Scale(expectedScale);
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			expectedScale = Vector3(4.5f, -4.25f, -2.f);
			testMatrix = TransformMatrix::IDENTITY_TRANSFORM;
			testMatrix.Scale(expectedScale);
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}

			expectedScale = Vector3(-4.5f, -5.f, -2.f);
			testMatrix = TransformMatrix::IDENTITY_TRANSFORM;
			testMatrix.Scale(expectedScale);
			if (!testScalarMatrix(testMatrix, expectedScale))
			{
				return false;
			}
		}

		TestLog(testFlags, TXT("Testing transform matrices (Rotation)."));
		{
			enum rotationAxis
			{
				RA_XAxis,
				RA_YAxis,
				RA_ZAxis
			};

			std::function<bool(FLOAT, rotationAxis, const Vector3&, const Vector3&)> testRotationMatrix([&](FLOAT radians, rotationAxis axis, const Vector3& startingPoint, const Vector3& expectedRotatedPoint)
			{
				TransformMatrix originalPoint;
				originalPoint.SetTranslation(startingPoint);

				DString axisString;
				TransformMatrix rotation;

				switch (axis)
				{
					case(RA_XAxis):
						rotation = TransformMatrix::GetRotationMatrixAboutXAxis(radians);
						axisString = TXT("X");
						break;

					case(RA_YAxis):
						rotation = TransformMatrix::GetRotationMatrixAboutYAxis(radians);
						axisString = TXT("Y");
						break;

					case(RA_ZAxis):
						rotation = TransformMatrix::GetRotationMatrixAboutZAxis(radians);
						axisString = TXT("Z");
						break;
				}
			
				TransformMatrix rotatedMatrix = rotation * originalPoint;
				TransformMatrix expectedRotatedMatrix;
				expectedRotatedMatrix.SetTranslation(expectedRotatedPoint);

				Vector3 rotatedTranslation = rotatedMatrix.GetTranslation();
				Vector3 expectedTranslation = expectedRotatedMatrix.GetTranslation();
				if (!rotatedTranslation.IsNearlyEqual(expectedTranslation, 0.0001f))
				{
					UnitTestError(testFlags, TXT("Rotation matrix test failed. After rotating %s radians about the %s-axis, the expected transform does not match the calculated transform. actual translation %s != expected translation %s"), {radians.ToString(), axisString, rotatedTranslation.ToString(), expectedTranslation.ToString()});

					if (ShouldHaveDebugLogs(testFlags))
					{
						TestLog(testFlags, TXT("Original transform..."));
						originalPoint.LogMatrix();

						TestLog(testFlags, TXT("Rotation matrix..."));
						rotation.LogMatrix();

						TestLog(testFlags, TXT("Calculated rotated matrix..."));
						rotatedMatrix.LogMatrix();

						TestLog(testFlags, TXT("Expected rotated matrix..."));
						expectedRotatedMatrix.LogMatrix();
					}

					logTimer();
					return false;
				}

				return true;
			});
		
			FLOAT halfPi = PI_FLOAT * 0.5f;

			//Ensure 0 rotation does not affect translation
			{
				const Vector3 noRotationVector(5.f, 5.f, 5.f);
				if (!testRotationMatrix(0.f, RA_XAxis, noRotationVector, noRotationVector))
				{
					return false;
				}

				if (!testRotationMatrix(0.f, RA_YAxis, noRotationVector, noRotationVector))
				{
					return false;
				}

				if (!testRotationMatrix(0.f, RA_ZAxis, noRotationVector, noRotationVector))
				{
					return false;
				}
			}

			//Test rotating by 90 degrees (two tests per rotate by axis to test the signess of sin(90))
			{
				if (!testRotationMatrix(halfPi, RA_XAxis, Vector3(0.f, 1.f, 0.f), Vector3(0.f, 0.f, -1.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi, RA_XAxis, Vector3(0.f, 0.f, 1.f), Vector3(0.f, 1.f, 0.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi, RA_YAxis, Vector3(1.f, 0.f, 0.f), Vector3(0.f, 0.f, 1.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi, RA_YAxis, Vector3(0.f, 0.f, 1.f), Vector3(-1.f, 0.f, 0.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi, RA_ZAxis, Vector3(0.f, 1.f, 0.f), Vector3(1.f, 0.f, 0.f)))
				{
					return false;
				}

				if (!testRotationMatrix(-halfPi, RA_ZAxis, Vector3(1.f, 0.f, 0.f), Vector3(0.f, 1.f, 0.f)))
				{
					return false;
				}
			}

			//Test 180 rotations (cos(180)=>1 and sin(180)=>0). One test per axis since each component in the translation vector are not zero.
			{
				if (!testRotationMatrix(halfPi * 2.f, RA_XAxis, Vector3(0.5f, -0.5f, 1.f), Vector3(0.5f, 0.5f, -1.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi * 2.f, RA_YAxis, Vector3(0.5f, -0.5f, 1.f), Vector3(-0.5f, -0.5f, -1.f)))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi * 2.f, RA_ZAxis, Vector3(0.5f, -0.5f, 1.f), Vector3(-0.5f, 0.5f, 1.f)))
				{
					return false;
				}
			}

			//Test full revolution to ensure it doesn't affect the resulting translation
			{
				const Vector3 testTranslation(0.25f, -33.f, 14.f);
				if (!testRotationMatrix(halfPi * 4.f, RA_XAxis, testTranslation, testTranslation))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi * 4.f, RA_YAxis, testTranslation, testTranslation))
				{
					return false;
				}

				if (!testRotationMatrix(halfPi * -4.f, RA_ZAxis, testTranslation, testTranslation))
				{
					return false;
				}
			}

			if (!testRotationMatrix(halfPi, RA_ZAxis, Vector3(-15.f, 0.1f, 64.f), Vector3(0.1f, 15.f, 64.f)))
			{
				return false;
			}
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Extracting Euler Angles from Transform Matrix"));
	{
		std::function<bool(const Rotator&)> testEulerExtraction([&](const Rotator& testRotation)
		{
			TransformMatrix rotatedMatrix;
			rotatedMatrix.Rotate(testRotation);
			Rotator extractedRotation = rotatedMatrix.GetRotation();

			//Compare by directional vector since there are multiple different transforms that could lead to the same direction.
			//For example: Pitch 135 is also equal to Yaw 180, and pitch 45.
			Vector3 testRotationDir = testRotation.GetDirectionalVector();
			Vector3 extractedRotationDir = extractedRotation.GetDirectionalVector();

			if (!testRotationDir.IsNearlyEqual(extractedRotationDir, 0.001f))
			{
				UnitTestError(testFlags, TXT("Extracting Euler Angles from Transform Matrix test failed. After rotating an identity transform matrix by %s, the test is expected to extract %s from the resulting matrix. It pulled %s instead. Compared against directional vectors: (expected) %s != (actual) %s"), {testRotation.ToString(), testRotation.ToString(), extractedRotation.ToString(), testRotationDir.ToString(), extractedRotationDir.ToString()});
				if (ShouldHaveDebugLogs(testFlags))
				{
					TestLog(testFlags, TXT("Rotated transform matrix..."));
					rotatedMatrix.LogMatrix();
				}

				logTimer();
				return false;
			}

			return true;
		});

		if (!testEulerExtraction(Rotator::ZERO_ROTATOR))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(45, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(80, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(90, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(100, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(180, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(200, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(270, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(300, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 45, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, -45, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 135, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 0, 80, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 0, 150, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 45, 280, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(45, 45, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(85, -15, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, 45, 35, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(0, -20, 160, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(40, 45, 50, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(185, 60, -20, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(90, 0, 0, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}

		if (!testEulerExtraction(Rotator(90, 180, 270, Rotator::ERotationUnit::RU_Degrees)))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	logTimer();
	ExecuteSuccessSequence(testFlags, TXT("Matrix"));

	return true;
}

bool DatatypeUnitTester::IsNearlyEqual(const Rotator& a, const Rotator& b)
{
	INT distance = INT(a.Yaw) - INT(b.Yaw);
	distance.AbsInline();
	if (distance > 1 && distance < Rotator::FULL_REVOLUTION - 1)
	{
		return false;
	}

	distance = INT(a.Pitch) - INT(b.Pitch);
	distance.AbsInline();
	if (distance > 1 && distance < Rotator::FULL_REVOLUTION - 1)
	{
		return false;
	}

	distance = INT(a.Roll) - INT(b.Roll);
	distance.AbsInline();
	if (distance > 1 && distance < Rotator::FULL_REVOLUTION - 1)
	{
		return false;
	}

	return true;
}
SD_END

#endif