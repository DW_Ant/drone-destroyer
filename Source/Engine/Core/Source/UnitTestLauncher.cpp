/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  UnitTestLauncher.cpp
=====================================================================
*/
 
#include "CoreClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, UnitTestLauncher, SD, Object)

bool UnitTestLauncher::RunAllTests (UnitTester::EUnitTestFlags testFlags)
{
	//Generate a list of unit tests that needs to run
	std::vector<const UnitTester*> incompleteTests;
	for (ClassIterator iter(UnitTester::SStaticClass()); iter.SelectedClass; iter++)
	{
		const UnitTester* unitTester = dynamic_cast<const UnitTester*>(iter.SelectedClass->GetDefaultObject());
		if (unitTester != nullptr)
		{
			incompleteTests.push_back(unitTester);
		}
	}

	std::vector<const UnitTester*> completedTests;
	while(incompleteTests.size() > 0)
	{
		INT oldIncompleteSize = incompleteTests.size();

		UINT_TYPE i = 0;
		while (i < incompleteTests.size())
		{
			if (!incompleteTests.at(i)->MetRequirements(completedTests))
			{
				//Not all requirements are met.  Try next test.
				i++;
				continue;
			}

			if (incompleteTests.at(i)->IsTestEnabled(testFlags)) //If disabled, assume this test returned true
			{
				if (!incompleteTests.at(i)->RunTests(testFlags))
				{
					return false;
				}
			}

			completedTests.push_back(incompleteTests.at(i));
			incompleteTests.erase(incompleteTests.begin() + i);
		}

		if (oldIncompleteSize == incompleteTests.size())
		{
			UnitTester::TestLog(testFlags, TXT("Unable to complete all unit tests since there's a circular dependency.  The following unit tests depend on each other:"));
			for (INT i = 0; i < incompleteTests.size(); i++)
			{
				UnitTester::TestLog(testFlags, TXT("    [%s]:  %s"), i, incompleteTests.at(i.ToUnsignedInt())->GetName());
			}

			return false;
		}
	}

	UnitTester::TestLog(testFlags, TXT("========================"));
	UnitTester::TestLog(testFlags, TXT("All unit tests PASSED!"));
	UnitTester::TestLog(testFlags, TXT("========================"));
	UnitTester::TestLog(testFlags, TXT(""));

	return true;
}
SD_END

#endif