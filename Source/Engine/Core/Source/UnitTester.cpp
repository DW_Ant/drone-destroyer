/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  UnitTester.cpp
=====================================================================
*/

#include "CoreClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(SD, UnitTester, SD, Object)

void UnitTester::TestLog (EUnitTestFlags testFlags, const DString& msg)
{
	if ((testFlags & SD::UnitTester::UTF_ErrorsOnly) == 0 && (testFlags & SD::UnitTester::UTF_Verbose) > 0)
	{
		UnitTestLog.Log(LogCategory::LL_Debug, msg);
	}
}

bool UnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
{
	bool bCompletedDatatype = false;
	bool bCompletedEngineInteg = false;

	for (UINT_TYPE i = 0; i < completedTests.size(); i++)
	{
		if (dynamic_cast<const DatatypeUnitTester*>(completedTests.at(i)) != nullptr)
		{
			bCompletedDatatype = true;
			if (bCompletedEngineInteg)
			{
				return true;
			}
		}
		else if (dynamic_cast<const EngineIntegrityUnitTester*>(completedTests.at(i)) != nullptr)
		{
			bCompletedEngineInteg = true;
			if (bCompletedDatatype)
			{
				return true;
			}
		}
	}

	return false;
}

bool UnitTester::IsTestEnabled (EUnitTestFlags testFlags) const
{
	return true;
}

bool UnitTester::ShouldHaveDebugLogs (EUnitTestFlags testFlags) const
{
	return ((testFlags & SD::UnitTester::UTF_ErrorsOnly) == 0 && (testFlags & SD::UnitTester::UTF_Verbose) > 0);
}

void UnitTester::UnitTestError (EUnitTestFlags testFlags, const DString& errorMsg, std::initializer_list<const char*> args) const
{
	ExecuteFailSequence(testFlags);
	UnitTestError_Static(testFlags, errorMsg, args);
}

void UnitTester::UnitTestError (EUnitTestFlags testFlags, const DString& errorMsg, const std::vector<DString>& args) const
{
	ExecuteFailSequence(testFlags);
	UnitTestError_Static(testFlags, errorMsg, args);
}

void UnitTester::UnitTestError (EUnitTestFlags testFlags, const DString& errorMsg) const
{
	ExecuteFailSequence(testFlags);
	UnitTestError_Static(testFlags, errorMsg);
}

void UnitTester::UnitTestError_Static (EUnitTestFlags testFlags, const DString& errorMsg, std::initializer_list<const char*> args)
{
	std::vector<DString> strParams;
	for (auto elem : args)
	{
		strParams.push_back(DString(elem));
	}

	UnitTestError_Static(testFlags, errorMsg, strParams);
}

void UnitTester::UnitTestError_Static (EUnitTestFlags testFlags, const DString& errorMsg, const std::vector<DString>& args)
{
	DString fullMsg = DString::FormatString(errorMsg, args);
	if ((testFlags & UTF_Muted) == 0)
	{
		UnitTestLog.Log(LogCategory::LL_Warning, fullMsg);
	}

	if ((testFlags & UTF_CrashOnError) > 0)
	{
		Engine::FindEngine()->FatalError(fullMsg);
	}
}

void UnitTester::UnitTestError_Static (EUnitTestFlags testFlags, const DString& errorMsg)
{
	std::vector<DString> strParams;
	UnitTestError_Static(testFlags, errorMsg, strParams);
}

void UnitTester::SetTestCategory (EUnitTestFlags testFlags, const DString& newTestCategory) const
{
	if (newTestCategory.IsEmpty() && !TestCategory.IsEmpty())
	{
		TestLog(testFlags, TXT("Finished %s tests."), newTestCategory);
	}

	TestCategory = newTestCategory;
	if (!TestCategory.IsEmpty())
	{
		TestLog(testFlags, TXT("Begin %s testing. . ."), TestCategory);
	}
}

void UnitTester::CompleteTestCategory (EUnitTestFlags testFlags) const
{
	if (!TestCategory.IsEmpty())
	{
		TestLog(testFlags, TXT("%s tests passed!"), TestCategory);
	}

	TestCategory = TXT("");
}

void UnitTester::BeginTestSequence (EUnitTestFlags testFlags, const DString& testName) const
{
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT("-== Begin %s Test ==-"), testName);
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT(""));
}

void UnitTester::ExecuteSuccessSequence (EUnitTestFlags testFlags, const DString& testName) const
{
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT("-== %s Test PASSSED! ==-"), testName);
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT(""));
	TestCategory = TXT("");
}

void UnitTester::ExecuteFailSequence (EUnitTestFlags testFlags) const
{
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT("-== ERROR:  %s Test FAILED! ==-"), GetName());
	TestLog(testFlags, TXT("========================"));
	TestLog(testFlags, TXT(""));
	TestCategory = TXT("");
}
SD_END

#endif