/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TickTester.cpp
=====================================================================
*/

#include "CoreClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, TickTester, SD, Entity)

void TickTester::InitProps ()
{
	Super::InitProps();

	OwningTester = nullptr;
	MaxTickCounter = 10;

	bMiscTickFirst = true;
	MiscTickCounter1 = 0;
	MiscTickCounter2 = 0;
	DebugTickCounter1 = 0;
	DebugTickCounter2 = 0;
}

void TickTester::Destroyed ()
{
	UnitTester::TestLog(TestFlags, TXT("TickTester test terminated."));

	Super::Destroyed();
}

void TickTester::BeginTest ()
{
	//OwningTester must be assigned first
	CHECK(OwningTester != nullptr)

	UnitTester::TestLog(TestFlags, TXT("TickTester test initiated."));

	TickComponent* miscTick1 = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (!AddComponent(miscTick1))
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since it was unable to attach the first Misc Tick Component."));
		Destroy();
		return;
	}
	miscTick1->SetTickHandler(SDFUNCTION_1PARAM(this, TickTester, HandleMiscTick1, void, FLOAT));

	//Instantiate and attach debug tick before second misc tick to test instantiation order does not change the tick order.
	TickComponent* debugTick1 = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (!AddComponent(debugTick1))
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since it was unable to attach the first Debug Tick Component."));
		Destroy();
		return;
	}
	debugTick1->SetTickHandler(SDFUNCTION_1PARAM(this, TickTester, HandleDebugTick1, void, FLOAT));

	TickComponent* miscTick2 = TickComponent::CreateObject(TICK_GROUP_MISC);
	if (!AddComponent(miscTick2))
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since it was unable to attach the second Misc Tick Component."));
		Destroy();
		return;
	}
	miscTick2->SetTickHandler(SDFUNCTION_1PARAM(this, TickTester, HandleMiscTick2, void, FLOAT));

	TickComponent* debugTick2 = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (!AddComponent(debugTick2))
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since it was unable to attach the second Debug Tick Component."));
		Destroy();
		return;
	}
	debugTick2->SetTickHandler(SDFUNCTION_1PARAM(this, TickTester, HandleDebugTick2, void, FLOAT));

	//Create a TickComponent that should never tick (test IsTicking())
	TickComponent* disabledTick = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (!AddComponent(disabledTick))
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since it was unable to attach the disabled Tick Component."));
		Destroy();
		return;
	}
	disabledTick->SetTicking(false);
	disabledTick->SetTickHandler(SDFUNCTION_1PARAM(this, TickTester, HandleDisabledTick, void, FLOAT));
}

void TickTester::ValidateTickTest ()
{
	CHECK(OwningTester != nullptr)

	//Validate TickOrder
	if (bMiscTickFirst)
	{
		if (MiscTickCounter1 < DebugTickCounter1 || MiscTickCounter1 < DebugTickCounter2 ||
			MiscTickCounter2 < DebugTickCounter1 || MiscTickCounter2 < DebugTickCounter2)
		{
			OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since a Debug Tick Component ticked before a Misc Tick Component."));
			Destroy();
			return;
		}
	}
	else
	{
		if (DebugTickCounter1 < MiscTickCounter1 || DebugTickCounter1 < MiscTickCounter2 ||
			DebugTickCounter2 < MiscTickCounter1 || DebugTickCounter2 < MiscTickCounter2)
		{
			OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since a Misc Tick Component ticked before a Debug Tick Component."));
			Destroy();
			return;
		}
	}

	//Are any TickComponents skipping frames?
	if (INT::Abs(MiscTickCounter1 - MiscTickCounter2) > 1 || INT::Abs(MiscTickCounter2 - DebugTickCounter1) > 1 ||
		INT::Abs(DebugTickCounter1 - DebugTickCounter2) > 1)
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since certain tick components are ticking more frequently than other tick components.  MiscTickCounters:  %s and %s.  DebugTickCounters:  %s and %s."),
			{MiscTickCounter1.ToString().ToCString(), MiscTickCounter2.ToString().ToCString(), DebugTickCounter1.ToString().ToCString(), DebugTickCounter2.ToString().ToCString()});
		Destroy();
		return;
	}

	//Check if all TickComponents ticked enough times
	if (MiscTickCounter1 >= MaxTickCounter && MiscTickCounter2 >= MaxTickCounter &&
		DebugTickCounter1 >= MaxTickCounter && DebugTickCounter2 >= MaxTickCounter)
	{
		UnitTester::TestLog(TestFlags, TXT("TickTester completed.  All TickComponents ticked in correct order."));
		Destroy();
		return;
	}
}

void TickTester::HandleMiscTick1 (FLOAT deltaSec)
{
	MiscTickCounter1++;
	ValidateTickTest();
}

void TickTester::HandleMiscTick2 (FLOAT deltaSec)
{
	MiscTickCounter2++;
	ValidateTickTest();
}

void TickTester::HandleDebugTick1 (FLOAT deltaSec)
{
	DebugTickCounter1++;
	ValidateTickTest();
}

void TickTester::HandleDebugTick2 (FLOAT deltaSec)
{
	DebugTickCounter2++;
	ValidateTickTest();
}

void TickTester::HandleDisabledTick (FLOAT deltaSec)
{
	if (OwningTester != nullptr)
	{
		OwningTester->UnitTestError(TestFlags, TXT("TickTester failed since a disabled TickComponent is still ticking."));
	}

	Destroy();
}
SD_END

#endif