/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EngineIntegrityUnitTester.cpp
=====================================================================
*/

#include "CoreClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, EngineIntegrityUnitTester, SD, UnitTester)

bool EngineIntegrityUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_FeatureTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		EngineIntegrityTester* tester = EngineIntegrityTester::CreateObject();
		if (tester == nullptr)
		{
			UnitTestError(testFlags, TXT("Failed to launch Engine Integrity Tests.  Could not instantiate test object."));
			return false;
		}

		bool bResult = (tester->TestClassIterator(this, testFlags) && tester->TestObjectInstantiation(this, testFlags) &&
				tester->TestObjectIterator(this, testFlags) && tester->TestPropertyIterator(this, testFlags) &&
				tester->TestCleanUp(this, testFlags) && tester->TestComponents(this, testFlags) &&
				tester->TestEntityVisibility(this, testFlags));

		tester->Destroy();
		if (!bResult)
		{
			return false;
		}
	}

	//Async test
	if ((testFlags & UTF_FeatureTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Asynchronous) > 0)
	{
		if (!LaunchTickTester(testFlags))
		{
			return false;
		}
	}

	return true;
}

bool EngineIntegrityUnitTester::MetRequirements (const std::vector<const UnitTester*>& completedTests) const
{
	for (UINT_TYPE i = 0; i < completedTests.size(); i++)
	{
		if (dynamic_cast<const DatatypeUnitTester*>(completedTests.at(i)) != nullptr)
		{
			return true;
		}
	}

	return false;
}

bool EngineIntegrityUnitTester::LaunchTickTester (EUnitTestFlags testFlags) const
{
	TickTester* tickTester = TickTester::CreateObject();
	if (tickTester == nullptr)
	{
		UnitTestError(testFlags, TXT("Failed to launch Tick Tester.  Could not instantiate test object."));
		return false;
	}

	tickTester->TestFlags = testFlags;
	tickTester->OwningTester = this;
	tickTester->BeginTest();
	return true;
}
SD_END

#endif