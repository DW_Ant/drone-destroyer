/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Vector3.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
const Vector3 Vector3::ZeroVector(0.0f, 0.0f, 0.0f);
const Vector3 Vector3::Right(1.f, 0.f, 0.f);
const Vector3 Vector3::Up(0.f, 1.f, 0.f);
const Vector3 Vector3::Forward(0.f, 0.f, 1.f);

Vector3::Vector3 ()
{

}

Vector3::Vector3 (const float x, const float y, const float z)
{
	X = x;
	Y = y;
	Z = z;
}

Vector3::Vector3 (const FLOAT x, const FLOAT y, const FLOAT z)
{
	X = x;
	Y = y;
	Z = z;
}

Vector3::Vector3 (const Vector3& copyVector)
{
	X = copyVector.X;
	Y = copyVector.Y;
	Z = copyVector.Z;
}

void Vector3::operator= (const Vector3& copyVector)
{
	X = copyVector.X;
	Y = copyVector.Y;
	Z = copyVector.Z;
}

FLOAT Vector3::operator[] (UINT_TYPE axisIdx)
{
	return GetAxis(axisIdx);
}

const FLOAT Vector3::operator[] (UINT_TYPE axisIdx) const
{
	return GetAxis(axisIdx);
}

DString Vector3::ToString () const
{
	return (TXT("(") + X.ToString() + TXT(",") + Y.ToString() + TXT(",") + Z.ToString() + TXT(")"));
}

void Vector3::Serialize (DataBuffer& dataBuffer) const
{
	dataBuffer << X;
	dataBuffer << Y;
	dataBuffer << Z;
}

void Vector3::Deserialize (const DataBuffer& dataBuffer)
{
	dataBuffer >> X;
	dataBuffer >> Y;
	dataBuffer >> Z;
}

Vector3 Vector3::SFMLtoSD (const sf::Vector3f sfVector)
{
	return Vector3(sfVector.x, sfVector.y, sfVector.z);
}

sf::Vector3f Vector3::SDtoSFML (const Vector3 sdVector)
{
	return sf::Vector3f(sdVector.X.Value, sdVector.Y.Value, sdVector.Z.Value);
}

bool Vector3::IsNearlyEqual (const Vector3& compareTo, FLOAT tolerance) const
{
	return (FLOAT::Abs(X - compareTo.X) <= tolerance &&
		FLOAT::Abs(Y - compareTo.Y) <= tolerance &&
		FLOAT::Abs(Z - compareTo.Z) <= tolerance);
}

FLOAT Vector3::VSize (bool bIncludeZ) const
{
	if (bIncludeZ)
	{
		return (sqrt(pow(X.Value, 2) + pow(Y.Value, 2) + pow(Z.Value, 2)));
	}
	else
	{
		return (sqrt(pow(X.Value, 2) + pow(Y.Value, 2)));
	}
}

FLOAT Vector3::CalcDistSquared (bool bIncludeZ) const
{
	if (bIncludeZ)
	{
		return pow(X.Value, 2) + pow(Y.Value, 2) + pow(Z.Value, 2);
	}
	else
	{
		return pow(X.Value, 2) + pow(Y.Value, 2);
	}
}

void Vector3::SetLengthTo (FLOAT targetLength)
{
	Normalize();

	X *= targetLength;
	Y *= targetLength;
	Z *= targetLength;
}

Vector3 Vector3::Min (Vector3 input, const Vector3& min)
{
	input.MinInline(min);
	return input;
}

void Vector3::MinInline (const Vector3& min)
{
	X = Utils::Min(X, min.X);
	Y = Utils::Min(Y, min.Y);
	Z = Utils::Min(Z, min.Z);
}

Vector3 Vector3::Max (Vector3 input, const Vector3& max)
{
	input.MaxInline(max);
	return input;
}

void Vector3::MaxInline (const Vector3& max)
{
	X = Utils::Max(X, max.X);
	Y = Utils::Max(Y, max.Y);
	Z = Utils::Max(Z, max.Z);
}

Vector3 Vector3::Clamp (Vector3 input, const Vector3& min, const Vector3& max)
{
	input.ClampInline(min, max);
	return input;
}

void Vector3::ClampInline (const Vector3& min, const Vector3& max)
{
	X = Utils::Clamp(X, min.X, max.X);
	Y = Utils::Clamp(Y, min.Y, max.Y);
	Z = Utils::Clamp(Z, min.Z, max.Z);
}

FLOAT Vector3::Dot (const Vector3& otherVector) const
{
	return ((X * otherVector.X) + (Y * otherVector.Y) + (Z * otherVector.Z)).Value;
}

Vector3 Vector3::CrossProduct (const Vector3& otherVector) const
{
	Vector3 result;

	result.X = (Y * otherVector.Z) - (Z * otherVector.Y);
	result.Y = (Z * otherVector.X) - (X * otherVector.Z);
	result.Z = (X * otherVector.Y) - (Y * otherVector.X);

	return result;
}

void Vector3::Normalize ()
{
	FLOAT magnitude = VSize();

	if (magnitude == 0)
	{
#ifdef DEBUG_MODE
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to normalize a zero 3D vector!"));
#endif
		return;
	}

	X /= magnitude;
	Y /= magnitude;
	Z /= magnitude;
}

bool Vector3::IsUniformed () const
{
	return (X == Y && X == Z);
}

bool Vector3::IsEmpty () const
{
	return (X == 0.f && Y == 0.f && Z == 0.f);
}

Vector2 Vector3::ToVector2 () const
{
	return Vector2(X, Y);
}

FLOAT Vector3::GetAxis (UINT_TYPE vectorComponent) const
{
	CHECK(vectorComponent <= 2)

	switch (vectorComponent)
	{
		case(0): return X;
		case(1): return Y;
		case(2): return Z;
	}

	return 0.f;
}

#pragma region "External Operators"
bool operator== (const Vector3& left, const Vector3& right)
{
	return (left.X == right.X && left.Y == right.Y && left.Z == right.Z);
}

bool operator!= (const Vector3& left, const Vector3& right)
{
	return !(left == right);
}

Vector3 operator+ (const Vector3& left, const Vector3& right)
{
	return Vector3(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
}

Vector3& operator+= (Vector3& left, const Vector3& right)
{
	left.X += right.X;
	left.Y += right.Y;
	left.Z += right.Z;
	return left;
}

Vector3 operator- (const Vector3& left, const Vector3& right)
{
	return Vector3(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
}

Vector3& operator-= (Vector3& left, const Vector3& right)
{
	left.X -= right.X;
	left.Y -= right.Y;
	left.Z -= right.Z;
	return left;
}

Vector3 operator* (const Vector3& left, const Vector3& right)
{
	return Vector3(left.X * right.X, left.Y * right.Y, left.Z * right.Z);
}

Vector3& operator*= (Vector3& left, const Vector3& right)
{
	left.X *= right.X;
	left.Y *= right.Y;
	left.Z *= right.Z;
	return left;
}

Vector3 operator/ (const Vector3& left, const Vector3& right)
{
	Vector3 result(left);
	result /= right;
	return result;
}

Vector3& operator/= (Vector3& left, const Vector3& right)
{
	if (right.X != 0.f)
	{
		left.X /= right.X;
	}

	if (right.Y != 0.f)
	{
		left.Y /= right.Y;
	}

	if (right.Z != 0.f)
	{
		left.Z /= right.Z;
	}

	return left;
}
#pragma endregion
SD_END