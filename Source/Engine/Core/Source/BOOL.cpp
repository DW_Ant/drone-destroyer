/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BOOL.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
INT BOOL::LanguageID = INT_INDEX_NONE;
DString BOOL::ToTrueText = TXT("True");
DString BOOL::ToFalseText = TXT("False");
std::mutex BOOL::LocalizationMutex;

BOOL::BOOL ()
{

}

BOOL::BOOL (const bool newValue)
{
	Value = newValue;
}

BOOL::BOOL (const BOOL& copyBool)
{
	Value = copyBool.Value;
}

BOOL::BOOL (const INT& intValue)
{
	Value = (intValue == 0) ? false : true;
}

BOOL::BOOL (const FLOAT& floatValue)
{
	Value = (floatValue == 0) ? false : true;
}

BOOL::BOOL (const DString& stringValue)
{
	Value = stringValue.ToBool();
}

void BOOL::operator= (const BOOL& copyBool)
{
	Value = copyBool.Value;
}

void BOOL::operator= (const bool otherBool)
{
	Value = otherBool;
}

BOOL::operator bool () const
{
	return Value;
}

bool BOOL::operator ! () const
{
	return !Value;
}

DString BOOL::ToString () const
{
	return (Value) ? TXT("True") : TXT("False");
}

void BOOL::Serialize (DataBuffer& dataBuffer) const
{
	char byteToPush = (Value) ? '1' : '0';
	dataBuffer.WriteBytes(&byteToPush, 1);
}

void BOOL::Deserialize (const DataBuffer& dataBuffer)
{
	char pulledByte;
	dataBuffer.ReadBytes(&pulledByte, 1);

	Value = (pulledByte != '0');
}

FLOAT BOOL::ToFLOAT () const
{
	return (Value) ? FLOAT(1.f) : FLOAT(0.f);
}

INT BOOL::ToINT () const
{
	return (Value) ? 1 : 0;
}

#pragma region "External Operators"
bool operator== (const BOOL& left, const BOOL& right)
{
	return (left.Value == right.Value);
}

bool operator== (const BOOL& left, const bool& right)
{
	return (left.Value == right);
}

bool operator== (const bool& left, const BOOL& right)
{
	return (left == right.Value);
}

bool operator!= (const BOOL& left, const BOOL& right)
{
	return (left.Value != right.Value);
}

bool operator!= (const BOOL& left, const bool& right)
{
	return (left.Value != right);
}

bool operator!= (const bool& left, const BOOL& right)
{
	return (left != right.Value);
}
#pragma endregion
SD_END