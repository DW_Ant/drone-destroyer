/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TickComponent.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, TickComponent, SD, EntityComponent)

TickComponent::TickComponent () : Super()
{
	PendingTickGroupName = TICK_GROUP_MISC;
}

TickComponent::TickComponent (const DString& inTickGroup) : Super()
{
	PendingTickGroupName = inTickGroup;
}

void TickComponent::InitProps ()
{
	Super::InitProps();

	TickInterval = -1.f; //Update every frame
	bTicking = true;
	OwningTickGroup = nullptr;
	OnTick.ClearFunction();

	AccumulatedDeltaTime = 0.f;
}

void TickComponent::BeginObject ()
{
	Super::BeginObject();

	Engine* curEngine = Engine::FindEngine();
	CHECK(curEngine != nullptr) //TickComponents must register to an Engine

	OwningTickGroup = curEngine->FindTickGroup(PendingTickGroupName);
	if (OwningTickGroup == nullptr)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("The TickGroup (%s) is not registered to the local Engine.  The TickComponent cannot Tick."), PendingTickGroupName);
		return;
	}

	OwningTickGroup->RegisterTick(this);
}

unsigned int TickComponent::CalculateHashID () const
{
	return Engine::GetEngine(Engine::MAIN_ENGINE_IDX)->GetTickComponentHashNumber();
}

void TickComponent::Destroyed ()
{
	if (OwningTickGroup != nullptr)
	{
		OwningTickGroup->RemoveTick(this);
	}

	Super::Destroyed();
}

TickComponent* TickComponent::CreateObject (const DString& inTickGroup)
{
	TickComponent* newObject = new TickComponent(inTickGroup);
	if (newObject == nullptr)
	{
		CoreLog.Log(LogCategory::LL_Fatal, TXT("Failed to allocate memory to instantiate a TickComponent!"));
		return nullptr;
	}
	
	newObject->InitializeObject();
	
	return newObject;
}

void TickComponent::Tick (FLOAT deltaSec)
{
	AccumulatedDeltaTime += deltaSec;
	if (AccumulatedDeltaTime >= TickInterval)
	{
		if (OnTick.IsBounded())
		{
			OnTick(AccumulatedDeltaTime);
		}

		ResetAccumulatedTickTime();
	}
}

void TickComponent::ResetAccumulatedTickTime ()
{
	AccumulatedDeltaTime = 0.f;
}

void TickComponent::SetTickInterval (FLOAT newTickInterval)
{
	TickInterval = newTickInterval;
}

void TickComponent::SetTickHandler (SDFunction<void, FLOAT> newHandler)
{
	OnTick = newHandler;
}

void TickComponent::SetTicking (bool bNewTicking)
{
	if (bNewTicking == bTicking)
	{
		return;
	}

	if (OwningTickGroup == nullptr)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot activate Tick component since the specified Tick component is not associated with a TickGroup."));
		bTicking = false;
		return;
	}

	bTicking = bNewTicking;
	if (bTicking)
	{
		OwningTickGroup->RegisterTick(this);
	}
	else
	{
		OwningTickGroup->RemoveTick(this);
	}
}

bool TickComponent::IsTicking () const
{
	return bTicking;
}
SD_END