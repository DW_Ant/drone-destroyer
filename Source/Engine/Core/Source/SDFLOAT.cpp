/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FLOAT.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
FLOAT::FLOAT ()
{

}

FLOAT::FLOAT (const float newValue)
{
	Value = newValue;
}

FLOAT::FLOAT (const FLOAT& copyFloat)
{
	Value = copyFloat.Value;
}

FLOAT::FLOAT (const INT& newValue)
{
	Value = newValue.ToFLOAT().Value;
}

FLOAT::FLOAT (const DString& text)
{
	if (text.IsEmpty())
	{
		Value = 0.f;
		return;
	}

	try
	{
		Value = stof(text.ReadString());
	}
	catch(std::invalid_argument&)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Cannot construct a FLOAT from %s.  Defaulting to 0."), text);
		Value = 0.f;
	}
	catch(std::out_of_range&)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("There's not enough space to fit %s within a float.  Defaulting to FLT_MAX_10_EXP(%s)."), text, FLOAT(FLT_MAX_10_EXP));
		Value = FLT_MAX_10_EXP;
	}
}

void FLOAT::operator= (const FLOAT& copyFloat)
{
	Value = copyFloat.Value;
}

void FLOAT::operator= (const float otherFloat)
{
	Value = otherFloat;
}

FLOAT FLOAT::operator++ ()
{
	return ++Value;
}

FLOAT FLOAT::operator++ (int)
{
	return Value++;
}

FLOAT FLOAT::operator- () const
{
	return -Value;
}

FLOAT FLOAT::operator-- ()
{
	return --Value;
}

FLOAT FLOAT::operator-- (int)
{
	return Value--;
}

DString FLOAT::ToString () const
{
	return DString::MakeString(Value);
}

void FLOAT::Serialize (DataBuffer& dataBuffer) const
{
	//Convert FLOAT to char array
	const int numBytes = sizeof(Value);
	char charArray[numBytes + 1]; //include null terminator
	memcpy(charArray, &Value, numBytes);

	//Reverse bytes if needed
	if (dataBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(charArray, numBytes);
	}

	dataBuffer.WriteBytes(charArray, numBytes);
}

void FLOAT::Deserialize (const DataBuffer& dataBuffer)
{
	const int numBytes = sizeof(Value);
	char rawData[numBytes];
	dataBuffer.ReadBytes(rawData, numBytes);

	//Reverse bytes if needed
	if (dataBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(rawData, numBytes);
	}

	memcpy(&Value, rawData, numBytes);
}

FLOAT FLOAT::MakeFloat (int value)
{
	return INT(value).ToFLOAT();
}

FLOAT FLOAT::MakeFloat (unsigned int value)
{
	return INT(value).ToFLOAT();
}

#ifdef PLATFORM_64BIT
FLOAT FLOAT::MakeFloat (const int64& value)
{
	return INT(value).ToFLOAT();
}

FLOAT FLOAT::MakeFloat (const uint64& value)
{
	return INT(value).ToFLOAT();
}
#endif

bool FLOAT::IsCloseTo (FLOAT other, FLOAT tolerance) const
{
	return (Abs(*this - other) <= tolerance);
}

INT FLOAT::ToINT () const
{
	return INT(static_cast<int>(trunc(Value)));
}

FLOAT FLOAT::Abs (const FLOAT value)
{
	return FLOAT(abs(value.Value));
}

void FLOAT::AbsInline ()
{
	Value = std::abs(Value);
}

FLOAT FLOAT::Round (const FLOAT value)
{
	return FLOAT(round(value.Value));
}

void FLOAT::RoundInline ()
{
	Value = round(Value);
}

void FLOAT::RoundInline (INT numDecimals)
{
	float multiplier = pow(10.f, numDecimals.ToFLOAT().Value);
	Value = round(Value * multiplier)/multiplier;
}

FLOAT FLOAT::RoundUp (const FLOAT value)
{
	return FLOAT(ceil(value.Value));
}

void FLOAT::RoundUpInline ()
{
	Value = ceil(Value);
}

void FLOAT::RoundUpInline (INT numDecimals)
{
	float multiplier = pow(10.f, numDecimals.ToFLOAT().Value);
	Value = ceil(Value * multiplier)/multiplier;
}

FLOAT FLOAT::RoundDown (const FLOAT value)
{
	return FLOAT(floor(value.Value));
}

void FLOAT::RoundDownInline ()
{
	Value = floor(Value);
}

void FLOAT::RoundDownInline (INT numDecimals)
{
	float multiplier = pow(10.f, numDecimals.ToFLOAT().Value);
	Value = floor(Value * multiplier)/multiplier;
}

float FLOAT::GetFloat (double target)
{
	Value = (float)target;
	return Value;
}

double FLOAT::ToDouble () const
{
	return static_cast<double>(Value);
}

DString FLOAT::ToFormattedString (INT minNumDigits, INT numDecimals, eTruncateMethod rounding) const
{
	if (numDecimals <= 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("FLOAT::ToFormattedString must display at least one decimal point. If there's a need to truncate decimals, use ToINT or one of the Rounding functions."));
		return ToINT().ToString();
	}

	FLOAT roundedFloat(Abs(*this));
	if (numDecimals >= 0)
	{
		switch (rounding)
		{
			case(eTruncateMethod::TM_RoundDown):
				roundedFloat.RoundDownInline(numDecimals);
				break;

			case(eTruncateMethod::TM_Round):
				roundedFloat.RoundInline(numDecimals);
				break;

			case(eTruncateMethod::TM_RoundUp):
				roundedFloat.RoundUpInline(numDecimals);
				break;
		}
	}

	DString result = roundedFloat.ToString();
	INT decimalIdx = result.Find(TXT("."), 1 /* one instead of zero because floats never lead with decimal point*/, DString::CC_CaseSensitive);
	CHECK(decimalIdx != INT_INDEX_NONE) //Floats should always print a decimal

	if ((result.Length() - decimalIdx) > numDecimals)
	{
		//Drop out unwanted decimal digits.
		DString::SubString(OUT result, 0, decimalIdx + numDecimals);
	}

	const INT numIgnoreChars = 1; //Ignore one of the characters (the decimal).
	INT numLeadingZeros = minNumDigits - (result.Length() - numIgnoreChars);
	const DString prefix = (Value >= 0.f) ? DString::EmptyString : TXT("-"); //Add negative sign back
	if (numLeadingZeros > 0)
	{
		return prefix + DString(TString(numLeadingZeros.Value, '0')) + result;
	}

	return prefix + result;
}

#pragma region "External Operators"
bool operator== (const FLOAT& left, const FLOAT& right)
{
	return (abs(left.Value - right.Value) < FLOAT_TOLERANCE);
}

bool operator== (const FLOAT& left, const float& right)
{
	return (abs(left.Value - right) < FLOAT_TOLERANCE);
}

bool operator== (const float& left, const FLOAT& right)
{
	return (abs(left - right.Value) < FLOAT_TOLERANCE);
}

bool operator!= (const FLOAT& left, const FLOAT& right)
{
	return !(left.Value == right.Value);
}

bool operator!= (const FLOAT& left, const float& right)
{
	return !(left.Value == right);
}

bool operator!= (const float& left, const FLOAT& right)
{
	return !(left == right.Value);
}

bool operator< (const FLOAT& left, const FLOAT& right)
{
	return (left.Value < right.Value);
}

bool operator< (const FLOAT& left, const float& right)
{
	return (left.Value < right);
}

bool operator< (const float& left, const FLOAT& right)
{
	return (left < right.Value);
}

bool operator<= (const FLOAT& left, const FLOAT& right)
{
	return (left.Value <= right.Value);
}

bool operator<= (const FLOAT& left, const float& right)
{
	return (left.Value <= right);
}

bool operator<= (const float& left, const FLOAT& right)
{
	return (left <= right.Value);
}

bool operator> (const FLOAT& left, const FLOAT& right)
{
	return (left.Value > right.Value);
}

bool operator> (const FLOAT& left, const float& right)
{
	return (left.Value > right);
}

bool operator> (const float& left, const FLOAT& right)
{
	return (left > right.Value);
}

bool operator>= (const FLOAT& left, const FLOAT& right)
{
	return (left.Value >= right.Value);
}

bool operator>= (const FLOAT& left, const float& right)
{
	return (left.Value >= right);
}

bool operator>= (const float& left, const FLOAT& right)
{
	return (left >= right.Value);
}

FLOAT operator+ (const FLOAT& left, const FLOAT& right)
{
	return FLOAT(left.Value + right.Value);
}

FLOAT operator+ (const FLOAT& left, const float& right)
{
	return FLOAT(left.Value + right);
}

FLOAT operator+ (const float& left, const FLOAT& right)
{
	return FLOAT(left + right.Value);
}

FLOAT& operator+= (FLOAT& left, const FLOAT& right)
{
	left.Value += right.Value;
	return left;
}

FLOAT& operator+= (FLOAT& left, const float& right)
{
	left.Value += right;
	return left;
}

float& operator+= (float& left, const FLOAT& right)
{
	left += right.Value;
	return left;
}

FLOAT operator- (const FLOAT& left, const FLOAT& right)
{
	return FLOAT(left.Value - right.Value);
}

FLOAT operator- (const FLOAT& left, const float& right)
{
	return FLOAT(left.Value - right);
}

FLOAT operator- (const float& left, const FLOAT& right)
{
	return FLOAT(left - right.Value);
}

FLOAT& operator-= (FLOAT& left, const FLOAT& right)
{
	left.Value -= right.Value;
	return left;
}

FLOAT& operator-= (FLOAT& left, const float& right)
{
	left.Value -= right;
	return left;
}

float& operator-= (float& left, const FLOAT& right)
{
	left -= right.Value;
	return left;
}

FLOAT operator* (const FLOAT& left, const FLOAT& right)
{
	return FLOAT(left.Value * right.Value);
}

FLOAT operator* (const FLOAT& left, const float& right)
{
	return FLOAT(left.Value * right);
}

FLOAT operator* (const float& left, const FLOAT& right)
{
	return FLOAT(left * right.Value);
}

FLOAT& operator*= (FLOAT& left, const FLOAT& right)
{
	left.Value *= right.Value;
	return left;
}

FLOAT& operator*= (FLOAT& left, const float& right)
{
	left.Value *= right;
	return left;
}

float& operator*= (float& left, const FLOAT& right)
{
	left *= right.Value;
	return left;
}

FLOAT operator/ (const FLOAT& left, const FLOAT& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), left);
		return left;
	}
#endif

	return (right.Value != 0.f) ? FLOAT(left.Value / right.Value) : left;
}

FLOAT operator/ (const FLOAT& left, const float& right)
{
#ifdef DEBUG_MODE
	if (right == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), left);
		return left;
	}
#endif

	return (right != 0.f) ? FLOAT(left.Value / right) : left;
}

FLOAT operator/ (const float& left, const FLOAT& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), FLOAT(left));
		return FLOAT(left);
	}
#endif

	return (right.Value != 0.f) ? FLOAT(left / right.Value) : FLOAT(left);
}

FLOAT& operator/= (FLOAT& left, const FLOAT& right)
{
	if (right.Value != 0.f)
	{
		left.Value /= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), left);
	}
#endif
		
	return left;
}

FLOAT& operator/= (FLOAT& left, const float& right)
{
	if (right != 0.f)
	{
		left.Value /= right;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), left);
	}
#endif
		
	return left;
}

float& operator/= (float& left, const FLOAT& right)
{
	if (right.Value != 0.f)
	{
		left /= right.Value;
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), FLOAT(left));
	}
#endif
		
	return left;
}

FLOAT operator% (const FLOAT& left, const FLOAT& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
		return left;
	}
#endif

	return (right.Value != 0.f) ? FLOAT(fmod(left.Value, right.Value)) : left;
}

FLOAT operator% (const FLOAT& left, const float& right)
{
#ifdef DEBUG_MODE
	if (right == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
		return left;
	}
#endif

	return (right != 0.f) ? FLOAT(fmod(left.Value, right)) : left;
}

FLOAT operator% (const float& left, const FLOAT& right)
{
#ifdef DEBUG_MODE
	if (right.Value == 0.f)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), FLOAT(left));
		return left;
	}
#endif

	return (right.Value != 0.f) ? FLOAT(fmod(left, right.Value)) : FLOAT(left);
}

FLOAT& operator%= (FLOAT& left, const FLOAT& right)
{
	if (right.Value != 0.f)
	{
		left.Value = fmod(left.Value, right.Value);
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
	}
#endif
		
	return left;
}

FLOAT& operator%= (FLOAT& left, const float& right)
{
	if (right != 0.f)
	{
		left.Value = fmod(left.Value, right);
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), left);
	}
#endif
		
	return left;
}

float& operator%= (float& left, const FLOAT& right)
{
	if (right.Value != 0.f)
	{
		left = fmod(left, right.Value);
	}
#ifdef DEBUG_MODE
	else
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to modulo %s by zero!"), FLOAT(left));
	}
#endif
		
	return left;
}

#if 0
bool operator== (float a, const FLOAT& b)
{
	return (a == b.Value);
}

bool operator!= (float a, const FLOAT& b)
{
	return (a != b.Value);
}

bool operator< (float a, const FLOAT& b)
{
	return (a < b.Value);
}

bool operator<= (float a, const FLOAT& b)
{
	return (a <= b.Value);
}
bool operator> (float a, const FLOAT& b)
{
	return (a > b.Value);
}

bool operator>= (float a, const FLOAT& b)
{
	return (a >= b.Value);
}

FLOAT operator+ (float a, const FLOAT& b)
{
	return FLOAT(a + b.Value);
}

FLOAT operator- (float a, const FLOAT& b)
{
	return FLOAT(a - b.Value);
}

FLOAT operator* (float a, const FLOAT& b)
{
	return FLOAT(a * b.Value);
}

FLOAT operator/ (float a, const FLOAT& b)
{
#ifdef DEBUG_MODE
	if (b == 0)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Attempting to divide %s by zero!"), FLOAT(a));
		return FLOAT(a);
	}
#endif
	return (b != 0) ? FLOAT(a/b.Value) : FLOAT(a);
}

FLOAT operator% (float a, const FLOAT& b)
{
	return FLOAT(fmod(a, b.Value));
}
#endif
#pragma endregion
SD_END