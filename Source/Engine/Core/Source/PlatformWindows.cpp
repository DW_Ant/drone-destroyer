/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlatformWindows.cpp
=====================================================================
*/


#include "CoreClasses.h"

#ifdef PLATFORM_WINDOWS

SD_BEGIN
void InitializePlatform ()
{
	
}

void PlatformOpenWindow (const DString& windowMsg, const DString& windowTitle)
{
#if USE_WIDE_STRINGS
	MessageBox(NULL, StrToLPCWSTR(windowMsg), StrToLPCWSTR(windowTitle), MB_ICONERROR); 
#else
	MessageBox(NULL, StrToLPCSTR(windowMsg), StrToLPCSTR(windowTitle), MB_ICONERROR);
#endif
}

void OS_Sleep (INT milliseconds)
{
	CHECK(milliseconds > 0)
	Sleep(static_cast<DWORD>(milliseconds.Value));
}

bool OS_CopyToClipboard (const DString& copyContent)
{
	//This code is based on MSDN documentation about copying text to OS's clipboard
	if (!OpenClipboard(0))
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to copy %s to clipboard since application was unable to open clipboard.  Error code:  %s"), copyContent, DString::MakeString(GetLastError()));
		return false;
	}

	EmptyClipboard();

	if (copyContent.IsEmpty())
	{
		//Copying nothing to the clipboard will do nothing more than clearing the clipboard
		CloseClipboard();
		return true;
	}

	TCHAR* cStr = new TCHAR[copyContent.Length().Value + 1];
#if USE_WIDE_STRINGS
	wcscpy_s(cStr, copyContent.Length().Value + 1, copyContent.ToCString());
#else
	strcpy_s(cStr, copyContent.Length().Value + 1, copyContent.ToCString());
#endif
	size_t strSize = copyContent.ReadString().capacity() + sizeof(TCHAR); //include null terminator

	//Allocate global memory for the object
	HGLOBAL globalCopy = GlobalAlloc(GMEM_MOVEABLE, strSize);
	if (!globalCopy)
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to copy %s to clipboard since application was unable to allocate global memory."), copyContent);
		delete[] cStr;
		CloseClipboard();
		return false;
	}

	//Lock the handle and copy the text to the buffer
	LPVOID lock = GlobalLock(globalCopy);
	memcpy(lock, cStr, strSize);
	GlobalUnlock(globalCopy);

	SetClipboardData(CF_TEXT, globalCopy);
	CloseClipboard();
	delete[] cStr;

	return true;
}

DString OS_PasteFromClipboard ()
{
	//This code is based on MSDN's documentation about retrieving text from OS's clipboard
	if (!IsClipboardFormatAvailable(CF_TEXT))
	{
		//Currently the clipboard does not contain plain text
		return TXT("");
	}

	if (!OpenClipboard(0))
	{
		CoreLog.Log(LogCategory::LL_Warning, TXT("Unable to get content from clipboard since application was unable to open clipboard.  Error code:  %s"), DString::MakeString(GetLastError()));
		return TXT("");
	}

	HGLOBAL globalBuffer = GetClipboardData(CF_TEXT);
	if (!globalBuffer)
	{
		CloseClipboard();
		return TXT("");
	}

	TCHAR* text = static_cast<TCHAR*>(GlobalLock(globalBuffer));
	if (!text)
	{
		CloseClipboard();
		return TXT("");
	}

	DString result(text);

	GlobalUnlock(globalBuffer);
	CloseClipboard();

	return result;
}

void OS_BreakExecution ()
{
	if (IsDebuggerPresent())
	{
		DebugBreak();
	}
}
SD_END

#endif