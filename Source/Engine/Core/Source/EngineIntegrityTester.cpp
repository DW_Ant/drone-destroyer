/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EngineIntegrityTester.cpp
=====================================================================
*/

#include "CoreClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, EngineIntegrityTester, SD, Object)

void EngineIntegrityTester::Destroyed ()
{
	//At least try to remove left over objects if one of the tests failed
	for (UINT_TYPE i = 0; i < InstantiatedObjects.size(); i++)
	{
		InstantiatedObjects.at(i)->Destroy();
	}

	Super::Destroyed();
}

bool EngineIntegrityTester::TestClassIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Class Iterator"));

	//Populate classes that should be found
	std::vector<const DClass*> requiredObjectClasses;
	std::vector<const DClass*> requiredEngineClasses;
	requiredObjectClasses.push_back(Object::SStaticClass());
	requiredObjectClasses.push_back(Entity::SStaticClass());
	requiredObjectClasses.push_back(EntityComponent::SStaticClass());
	requiredObjectClasses.push_back(DatatypeUnitTester::SStaticClass());
	requiredObjectClasses.push_back(EngineIntegrityUnitTester::SStaticClass());
	requiredObjectClasses.push_back(UnitTester::SStaticClass());
	requiredObjectClasses.push_back(Utils::SStaticClass());
	requiredEngineClasses.push_back(EngineComponent::SStaticClass());

	UnitTester::TestLog(testFlags, TXT("Launching object class iterator."));
	INT classCounter = 0;
	for (ClassIterator iter; iter.SelectedClass; iter++)
	{
		classCounter++;
		UnitTester::TestLog(testFlags, TXT("Class Iterator found class:  ") + iter.SelectedClass->GetDuneClassName());
		if (!iter.GetSelectedClass()->IsAbstract() && iter.SelectedClass->ClassType != DClass::CT_Object)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed.  The object class iterator found a class that's not an object named %s."), {iter.SelectedClass->GetDuneClassName()});
			return false;
		}

		if (iter.GetSelectedClass()->IsAbstract() && iter.GetSelectedClass()->GetDefaultObject() != nullptr)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed.  The class %s claims that it's abstract, but it has a ClassDefaultObject associated with it."), {iter.GetSelectedClass()->GetDuneClassName()});
			return false;
		}

		if (!iter.GetSelectedClass()->IsAbstract() && iter.GetSelectedClass()->GetDefaultObject() == nullptr)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed.  The class %s is not abstract, but it doesn't have a default object associated with it."), {iter.GetSelectedClass()->GetDuneClassName()});
			return false;
		}

		const Object* curObject = static_cast<const Object*>(iter.SelectedClass->GetDefaultObject());
		if (curObject != nullptr)
		{
			UnitTester::TestLog(testFlags, TXT("    Default object is:  ") + curObject->GetName());
		}

		for (UINT_TYPE i = 0; i < requiredObjectClasses.size(); i++)
		{
			if (requiredObjectClasses.at(i) == iter.SelectedClass)
			{
				requiredObjectClasses.erase(requiredObjectClasses.begin() + i);
				break;
			}
		}
	}

	UnitTester::TestLog(testFlags, TXT("Total number of found classes:  %s."), classCounter);
	if (classCounter <= 0)
	{
		tester->UnitTestError(testFlags, TXT("Class iterator test failed!  Class iterator was unable to find any registered classes."));
		return false;
	}

	if (requiredObjectClasses.size() > 0)
	{
		tester->UnitTestError(testFlags, TXT("Class iterator test failed!  The iterator failed to find some of the expected object classes.  See the log for a list of classes that were not found:"));
		for (UINT_TYPE i = 0; i < requiredObjectClasses.size(); i++)
		{
			UnitTester::TestLog(testFlags, TXT("    [Unit Test failure - Missing class] %s"), requiredObjectClasses.at(i)->GetDuneClassName());
		}

		return false;
	}

	UnitTester::TestLog(testFlags, TXT("Launching engine component class iterator."));
	INT numEngineClasses = 0;
	for (ClassIterator iter(EngineComponent::SStaticClass()); iter.SelectedClass; iter++)
	{
		UnitTester::TestLog(testFlags, TXT("Engine class Iterator found class:  ") + iter.SelectedClass->GetDuneClassName());
		numEngineClasses++;
		if (iter.SelectedClass->ClassType != DClass::CT_EngineComponent)
		{
			tester->UnitTestError(testFlags, TXT("Class iterator test failed.  The engine component class iterator found a class that's not an engine component named %s."), {iter.SelectedClass->GetDuneClassName()});
			return false;
		}

		const EngineComponent* cdo = iter.SelectedClass->GetDefaultEngineComponent();
		if (cdo != nullptr)
		{
			UnitTester::TestLog(testFlags, TXT("    Default engine component is:  ") + cdo->GetName());
		}

		for (UINT_TYPE i = 0; i < requiredEngineClasses.size(); i++)
		{
			if (requiredEngineClasses.at(i) == iter.SelectedClass)
			{
				requiredEngineClasses.erase(requiredEngineClasses.begin() + i);
				break;
			}
		}
	}

	if (requiredEngineClasses.size() > 0)
	{
		tester->UnitTestError(testFlags, TXT("Class iterator test failed!  The iterator failed to find some of the expected engine component classes.  See the logs for a list of classes that were not found."));
		for (UINT_TYPE i = 0; i < requiredEngineClasses.size(); i++)
		{
			UnitTester::TestLog(testFlags, TXT("    [Unit test failure - missing class] %s"), requiredEngineClasses.at(i)->GetDuneClassName());
		}

		return false;
	}

	tester->ExecuteSuccessSequence(testFlags, TXT("Class Iterator"));
	return true;
}

bool EngineIntegrityTester::TestObjectInstantiation (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Object Instantiation"));

	//Count how many objects use to exist
	TotalObjectsBeforeTest = 0;
	for (ObjectIterator iter; iter.SelectedObject; iter++)
	{
		TotalObjectsBeforeTest++;
	}

	if (tester->ShouldHaveDebugLogs(testFlags))
	{
		Engine::FindEngine()->LogObjectHashTable();
		UnitTester::TestLog(testFlags, TXT("There are a total of %s objects before this unit test created an object."), TotalObjectsBeforeTest);
	}

	for (ClassIterator iter; iter.SelectedClass; iter++)
	{
		const Object* defaultObject = (iter.SelectedClass->GetDefaultObject());
		if (defaultObject != nullptr)
		{
			Object* newObject = defaultObject->CreateObjectOfMatchingClass();
			if (newObject != nullptr)
			{
				UnitTester::TestLog(testFlags, TXT("Created a new object:  %s"), newObject->ToString());
				InstantiatedObjects.push_back(newObject);
			}
		}
	}

	UnitTester::TestLog(testFlags, TXT("Total number of instantiated objects:  %s."), INT(InstantiatedObjects.size()));
	if (InstantiatedObjects.size() <= 0)
	{
		tester->UnitTestError(testFlags, TXT("Object instantiation test failed!  Unit tester was unable to create an object."));
		return false;
	}

	if (tester->ShouldHaveDebugLogs(testFlags))
	{
		UnitTester::TestLog(testFlags, TXT("Displaying Engine's hash table. . ."));
		Engine::FindEngine()->LogObjectHashTable();
	}

	tester->ExecuteSuccessSequence(testFlags, TXT("Object Instantiation"));
	return true;
}

bool EngineIntegrityTester::TestObjectIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Object Iterator"));

	INT numFoundObjects = 0;
	for (ObjectIterator iter; iter.SelectedObject; iter++)
	{
		UnitTester::TestLog(testFlags, TXT("Object iterator found %s"), iter.SelectedObject->ToString());
		numFoundObjects++;
	}

	UnitTester::TestLog(testFlags, TXT("Total number of found objects:  %s"), numFoundObjects);
	if (numFoundObjects.ToUnsignedInt() < InstantiatedObjects.size())
	{
		tester->UnitTestError(testFlags, TXT("Object iterator test failed!  The iterator is expected to find at least %s objects.  It only found %s."), {INT(InstantiatedObjects.size()).ToString(), numFoundObjects.ToString()});
		return false;
	}

	tester->ExecuteSuccessSequence(testFlags, TXT("Object Iterator"));
	return true;
}

bool EngineIntegrityTester::TestPropertyIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Property Iterator"));

	//Initialize variables to test if iterator can retrieve values.
	PropIterINT = 105;
	PropIterFLOAT = 208.f;
	PropIterString = TXT("Random value to string.");
	PropIterVector = Vector3(10.f, 30.f, 91.f);
	UnitTester::TestLog(testFlags, TXT("Initialized member variables to:  (INT) %s, (FLOAT) %s, (DString) \"%s\", (Vector3) %s"), PropIterINT, PropIterFLOAT, PropIterString, PropIterVector);

	INT originalPropINT = PropIterINT;
	FLOAT originalPropFLOAT = PropIterFLOAT;
	DString originalPropString = PropIterString;
	Vector3 originalPropVector = PropIterVector;

	bool bFoundINTExt = false;
	bool bFoundFLOATExt = false;
	bool bFoundStringExt = false;
	bool bFoundVectorExt = false;

	//Add property extensions to enable property iterator to find these variables
	DPropertyExtension* intExtension = new DPropertyExtension();
	intExtension->BindPropertyToObject(this);
	intExtension->SetOwningProperty(&PropIterINT);

	DPropertyExtension* floatExtension = new DPropertyExtension();
	floatExtension->BindPropertyToObject(this);
	floatExtension->SetOwningProperty(&PropIterFLOAT);

	DPropertyExtension* stringExtension = new DPropertyExtension();
	stringExtension->BindPropertyToObject(this);
	stringExtension->SetOwningProperty(&PropIterString);

	DPropertyExtension* vectorExtension = new DPropertyExtension();
	vectorExtension->BindPropertyToObject(this);
	vectorExtension->SetOwningProperty(&PropIterVector);
	UnitTester::TestLog(testFlags, TXT("Attached property extensions to those four member variables.  Launching property iterator to see if it can find all four variables."));

	for (PropertyIterator<DPropertyExtension> iter(this); iter.GetSelectedProperty() != nullptr; iter++)
	{
		DPropertyExtension* curProp = iter.GetSelectedProperty();
		if (curProp->GetOwningProperty() == &PropIterINT)
		{
			if (bFoundINTExt)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found PropIterINT variable more than once."));
				return false;
			}

			INT* curINT = dynamic_cast<INT*>(curProp->GetOwningProperty());
			if (curINT == nullptr)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterINT, but it failed to dynamically cast the property as an INT."));
				return false;
			}

			if (curINT->Value != originalPropINT.Value)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterINT, but the value is different from the original INT value.  It should have been equal to %s.  Instead it's currently %s."), {originalPropINT.ToString(), PropIterINT.ToString()});
				return false;
			}

			UnitTester::TestLog(testFlags, TXT("Property iterator found INT variable:  %s"), (*curINT));
			bFoundINTExt = true;
		}
		else if (curProp->GetOwningProperty() == &PropIterFLOAT)
		{
			if (bFoundFLOATExt)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found PropIterFLOAT variable more than once."));
				return false;
			}

			FLOAT* curFLOAT = dynamic_cast<FLOAT*>(curProp->GetOwningProperty());
			if (curFLOAT == nullptr)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterFLOAT, but it failed to dynamically cast the property as a FLOAT."));
				return false;
			}

			if (curFLOAT->Value != originalPropFLOAT.Value)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterFLOAT, but the value is different from the original FLOAT value.  It should have been equal to %s.  Instead it's currently %s."), {originalPropFLOAT.ToString(), PropIterFLOAT.ToString()});
				return false;
			}

			UnitTester::TestLog(testFlags, TXT("Property iterator found FLOAT variable:  %s"), (*curFLOAT));
			bFoundFLOATExt = true;
		}
		else if (curProp->GetOwningProperty() == &PropIterString)
		{
			if (bFoundStringExt)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found PropIterString variable more than once."));
				return false;
			}

			DString* curString = dynamic_cast<DString*>(curProp->GetOwningProperty());
			if (curString == nullptr)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterString, but it failed to dynamically cast the property as a String."));
				return false;
			}

			if (*curString != originalPropString)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterString, but the value is different from the original String value.  It should have been equal to \"%s\".  Instead it's currently \"%s\"."), {originalPropString.ToString(), PropIterString.ToString()});
				return false;
			}

			UnitTester::TestLog(testFlags, TXT("Property iterator found DString variable:  \"%s\""), (*curString));
			bFoundStringExt = true;
		}
		else if (curProp->GetOwningProperty() == &PropIterVector)
		{
			if (bFoundVectorExt)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found PropIterVector variable more than once."));
				return false;
			}

			Vector3* curVector = dynamic_cast<Vector3*>(curProp->GetOwningProperty());
			if (curVector == nullptr)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterVector, but it failed to dynamically cast the property as a Vector3."));
				return false;
			}

			if (*curVector != originalPropVector)
			{
				tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator found a property of matching memory address of PropIterVector, but the values are different from the original Vector3 values.  It should have been equal to %s.  Instead it's currently %s."), {originalPropVector.ToString(), PropIterVector.ToString()});
				return false;
			}

			UnitTester::TestLog(testFlags, TXT("Property iterator found Vector3 variable:  %s"), (*curVector));
			bFoundVectorExt = true;
		}
	}

	UnitTester::TestLog(testFlags, TXT("Property iterator finished iterating through properties."));
	if (!bFoundINTExt)
	{
		tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator failed to find the INT extension."));
		return false;
	}

	if (!bFoundFLOATExt)
	{
		tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator failed to find the FLOAT extension."));
		return false;
	}

	if (!bFoundStringExt)
	{
		tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator failed to find the DString extension."));
		return false;
	}

	if (!bFoundVectorExt)
	{
		tester->UnitTestError(testFlags, TXT("Property iterator test failed.  The iterator failed to find the Vector3 extension."));
		return false;
	}

	tester->ExecuteSuccessSequence(testFlags, TXT("Property Iterator"));
	return true;
}

bool EngineIntegrityTester::TestCleanUp (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Object Cleanup"));

	for (UINT_TYPE i = 0; i < InstantiatedObjects.size(); i++)
	{
		UnitTester::TestLog(testFlags, TXT("Destroying object:  %s"), InstantiatedObjects.at(i)->ToString());
		InstantiatedObjects.at(i)->Destroy();
	}

	//Force garbage collector for immediate updates to the hash table
	Engine::FindEngine()->CollectGarbage();

	if (tester->ShouldHaveDebugLogs(testFlags))
	{
		UnitTester::TestLog(testFlags, TXT("Displaying Engine's hash table. . ."));
		Engine::FindEngine()->LogObjectHashTable();
	}
	InstantiatedObjects.clear();

	//Count all objects to see if there were any left remaining
	INT totalNumObjects = 0;
	for (ObjectIterator iter; iter.SelectedObject; iter++)
	{
		totalNumObjects++;
	}
	UnitTester::TestLog(testFlags, TXT("There are now a total of %s objects after unit test clean up."), totalNumObjects);

	if (TotalObjectsBeforeTest != totalNumObjects)
	{
		tester->UnitTestError(testFlags, TXT("The total number of objects before test (%s) differs from total number of objects after test (%s)!"), {TotalObjectsBeforeTest.ToString(), totalNumObjects.ToString()});
		UnitTester::TestLog(testFlags, TXT("The total number of objects before test (%s) differs from total number of objects after test (%s)!"), TotalObjectsBeforeTest, totalNumObjects);
		UnitTester::TestLog(testFlags, TXT("Created objects that automatically spawn other objects should also clean up after themselves."));
		return false;
	}

	tester->ExecuteSuccessSequence(testFlags, TXT("Object Cleanup"));
	return true;
}

bool EngineIntegrityTester::TestComponents (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Component"));

	UnitTester::TestLog(testFlags, TXT("Creating an entity, and attaching a Tick Component and attaching a Life Span component to the tick component."));
	Entity* testEntity = Entity::CreateObject();
	TickComponent* testTick = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	LifeSpanComponent* testLifeSpan = LifeSpanComponent::CreateObject();
	if (!testEntity->AddComponent(testTick) || !testTick->AddComponent(testLifeSpan))
	{
		tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to attach either Tick Component to entity or Life Span Component to Tick Component."));
		return false;
	}

	UnitTester::TestLog(testFlags, TXT("Attempting to retrieve components from Entity"));
	testTick = dynamic_cast<TickComponent*>(testEntity->FindSubComponent(Engine::FindEngine()->GetTickComponentHashNumber()));
	if (testTick == nullptr)
	{
		tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to find Tick Component attached to %s."), {testEntity->ToString()});
		return false;
	}

	testLifeSpan = dynamic_cast<LifeSpanComponent*>(testEntity->FindSubComponent(LifeSpanComponent::SStaticClass(), true));
	if (testLifeSpan == nullptr)
	{
		tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to find Life Span Component from %s."), {testEntity->ToString()});
		return false;
	}

	/*
	Entity
		Tick0
			LifeSpan0
				tick [inherited from LifeSpan]
				tick3
		tick1
		tick2
	*/

	UnitTester::TestLog(testFlags, TXT("Attaching two more Tick Components to owning entity, and another Tick component to the life span component."));
	TickComponent* tick1 = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	TickComponent* tick2 = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	TickComponent* tick3 = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (!testEntity->AddComponent(tick1) || !testEntity->AddComponent(tick2) || !testLifeSpan->AddComponent(tick3))
	{
		tester->UnitTestError(testFlags, TXT("Components test failed.  Unable to attach multiple tick components to the test entity."));
		return false;
	}
	std::vector<EntityComponent*> componentsToFind;
	componentsToFind.push_back(testTick);
	componentsToFind.push_back(testLifeSpan);
	componentsToFind.push_back(tick1);
	componentsToFind.push_back(tick2);
	componentsToFind.push_back(tick3);

	for (ComponentIterator iter(testEntity, true); iter.GetSelectedComponent() != nullptr; iter++)
	{
		for (UINT_TYPE i = 0; i < componentsToFind.size(); i++)
		{
			if (componentsToFind.at(i) == iter.GetSelectedComponent())
			{
				componentsToFind.erase(componentsToFind.begin() + i);
				break;
			}
		}
	}

	if (componentsToFind.size() > 0)
	{
		tester->UnitTestError(testFlags, TXT("Components test failed.  The component iterator was not able to find all components attached to %s."), {testEntity->ToString()});
		UnitTester::TestLog(testFlags, TXT("The following components were not found. . ."));
		for (UINT_TYPE i = 0; i < componentsToFind.size(); i++)
		{
			UnitTester::TestLog(testFlags, TXT("    [%s] = %s"), INT(i), componentsToFind.at(i)->ToString());
		}
		return false;
	}

	UnitTester::TestLog(testFlags, TXT("Detaching components from their owners."));
	testEntity->RemoveComponent(testTick);
	testEntity->RemoveComponent(tick1);
	testEntity->RemoveComponent(tick3);
	testEntity->RemoveComponent(tick2); //out of order is intended

	UnitTester::TestLog(testFlags, TXT("Component test passed!  Was able to find all Tick and Life Span components from %s."), testEntity->ToString());
	testEntity->Destroy();
	testTick->Destroy();
	tick1->Destroy();
	tick2->Destroy();
	tick3->Destroy();

	tester->ExecuteSuccessSequence(testFlags, TXT("Component"));
	return true;
}

bool EngineIntegrityTester::TestEntityVisibility (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags)
{
	tester->BeginTestSequence(testFlags, TXT("Entity Visibility"));
	std::vector<Entity*> entitiesToRemove;
	std::function<void()> cleanUpEntities = [&]
	{
		for (UINT_TYPE i = 0; i < entitiesToRemove.size(); ++i)
		{
			entitiesToRemove.at(i)->Destroy();
		}

		ContainerUtils::Empty(entitiesToRemove);
	};

	Entity* baseEntityA = Entity::CreateObject(); //EntityA has a shallow but wide component list
	Entity* baseEntityB = Entity::CreateObject(); //EntityB has a thin but deep component list.

	entitiesToRemove.push_back(baseEntityA);
	entitiesToRemove.push_back(baseEntityB);

	EntityComponent* compA = EntityComponent::CreateObject();
	EntityComponent* compB = EntityComponent::CreateObject();
	EntityComponent* compD = EntityComponent::CreateObject();
	EntityComponent* compE = EntityComponent::CreateObject();
	EntityComponent* compF = EntityComponent::CreateObject();
	EntityComponent* testedComponent = EntityComponent::CreateObject(); //This component will move around while testing visibility flags

	//No need to add components to entitiesToRemove vector since EntityComponents are automatically destroyed when their owners are destroyed.

	bool bFailedToAttach = false;
	bFailedToAttach |= !baseEntityA->AddComponent(compA);
	bFailedToAttach |= !baseEntityA->AddComponent(compB);
	bFailedToAttach |= !baseEntityB->AddComponent(compD);
	bFailedToAttach |= !compD->AddComponent(compE);
	bFailedToAttach |= !compE->AddComponent(compF);
	bFailedToAttach |= !compF->AddComponent(testedComponent);

	if (bFailedToAttach)
	{
		tester->UnitTestError(testFlags, TXT("Failed to initialize visibility test.  One of the EntityComponents couldn't attach to an Entity."));
		cleanUpEntities();
		return false;
	}

	if (!testedComponent->IsVisible())
	{
		tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  Entities should appear visible by default."));
		cleanUpEntities();
		return false;
	}

	testedComponent->SetVisibility(false);
	if (testedComponent->IsVisible())
	{
		tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  After setting an EntityComponent to be invisible, it should be invisible."));
		cleanUpEntities();
		return false;
	}

	testedComponent->SetVisibility(true);
	if (!testedComponent->IsVisible())
	{
		tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  After restoring an EntityComponent's visibility flag, it should be visible."));
		cleanUpEntities();
		return false;
	}

	baseEntityB->SetVisibility(false);
	if (testedComponent->IsVisible())
	{
		tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  After setting its root owner to invisible, the EntityComponent should also be invisibie."));
		cleanUpEntities();
		return false;
	}

	compD->SetVisibility(false);
	compE->SetVisibility(false);
	compF->SetVisibility(false);
	baseEntityB->SetVisibility(true);
	std::function<bool(bool /*bExpectedVisibility*/)> testEntity = [&](bool bExpectedVisibility)
	{
		if (testedComponent->IsVisible() != bExpectedVisibility)
		{
			if (bExpectedVisibility)
			{
				tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  All of its owning components and the entity component is visible.  testedComponent->IsVisible is still returning false."));
			}
			else
			{
				tester->UnitTestError(testFlags, TXT("Entity visibility test failed.  If any of the Owning Components are invisible, the child EntityComponent must also be invisible."));
			}

			cleanUpEntities();
			return false;
		}
		return true;
	};

	if (!testEntity(false))
	{
		return false;
	}

	compD->SetVisibility(true);
	if (!testEntity(false))
	{
		return false;
	}

	compE->SetVisibility(true);
	if (!testEntity(false))
	{
		return false;
	}

	compF->SetVisibility(true);
	if (!testEntity(true))
	{
		return false;
	}

	//Set middle component visibility to false then remove that component
	UnitTester::TestLog(testFlags, TXT("Testing entity component visibility after breaking the component chain in the middle."));
	compE->SetVisibility(false);
	if (!testEntity(false))
	{
		return false;
	}

	compF->DetachSelfFromOwner();
	compD->AddComponent(compF);
	if (!testEntity(true))
	{
		return false;
	}

	//Set other Entity visibility to false, then move testComponent to that component chain to see if it inherits visibility settings.
	UnitTester::TestLog(testFlags, TXT("Testing entity component visibility after moving the component to a different owner."));
	baseEntityA->SetVisibility(false);
	compA->SetVisibility(false);
	testedComponent->DetachSelfFromOwner();
	compA->AddComponent(testedComponent);
	if (!testEntity(false))
	{
		return false;
	}

	testedComponent->DetachSelfFromOwner();
	baseEntityA->AddComponent(testedComponent);
	if (!testEntity(false))
	{
		return false;
	}

	baseEntityA->SetVisibility(true);
	if (!testEntity(true))
	{
		return false;
	}

	//Changing sibling component visibility shouldn't affect each other's visibility
	UnitTester::TestLog(testFlags, TXT("Testing entity component visibility after adjusting sibling component's visibility."));
	compB->SetVisibility(false);
	if (!testEntity(true))
	{
		return false;
	}

	//Test the case where a component attaches to an old Entity that had visibility reenabled
	baseEntityB->SetVisibility(true);
	compD->SetVisibility(true);
	testedComponent->DetachSelfFromOwner();
	compD->AddComponent(testedComponent);
	if (!testEntity(true))
	{
		return false;
	}

	cleanUpEntities();
	tester->ExecuteSuccessSequence(testFlags, TXT("Entity Visibility"));
	return true;
}
SD_END

#endif