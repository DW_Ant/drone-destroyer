/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EngineComponent.cpp
=====================================================================
*/

#include "CoreClasses.h"

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT_PARENT(SD, EngineComponent,,)

EngineComponent::EngineComponent ()
{
	OwningEngine = nullptr;
	bTickingComponent = false;
}

EngineComponent::~EngineComponent ()
{
	
}

void EngineComponent::RegisterObjectHash ()
{
	
}

bool EngineComponent::CanRunPreInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const
{
	std::vector<const DClass*> dependencies = GetPreInitializeDependencies();
	for (auto curDependency : dependencies)
	{
		bool bFoundDependency = false;
		for (auto curExecutedComp : executedEngineComponents)
		{
			if (curDependency == curExecutedComp)
			{
				bFoundDependency = true;
				break;
			}
		}

		if (!bFoundDependency)
		{
			return false;
		}
	}

	return true;
}

bool EngineComponent::CanRunInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const
{
	std::vector<const DClass*> dependencies = GetInitializeDependencies();
	for (auto curDependency : dependencies)
	{
		bool bFoundDependency = false;
		for (auto curExecutedComp : executedEngineComponents)
		{
			if (curDependency == curExecutedComp)
			{
				bFoundDependency = true;
				break;
			}
		}

		if (!bFoundDependency)
		{
			return false;
		}
	}

	return true;
}

bool EngineComponent::CanRunPostInitializeComponent (const std::vector<const DClass*> executedEngineComponents) const
{
	std::vector<const DClass*> dependencies = GetPostInitializeDependencies();
	for (auto curDependency : dependencies)
	{
		bool bFoundDependency = false;
		for (auto curExecutedComp : executedEngineComponents)
		{
			if (curDependency == curExecutedComp)
			{
				bFoundDependency = true;
				break;
			}
		}

		if (!bFoundDependency)
		{
			return false;
		}
	}

	return true;
}

void EngineComponent::PreInitializeComponent ()
{
	RegisterEngineComponentInstance();
}

void EngineComponent::InitializeComponent ()
{
	
}

void EngineComponent::PostInitializeComponent ()
{
	
}

void EngineComponent::FormatLog (const LogCategory* category, LogCategory::ELogLevel logLevel, OUT DString& msg)
{

}

void EngineComponent::ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg)
{

}

void EngineComponent::PreTick (FLOAT deltaSec)
{
	
}

void EngineComponent::PostTick (FLOAT deltaSec)
{
	
}

void EngineComponent::ShutdownComponent ()
{
	RemoveEngineComponentInstance();
}

void EngineComponent::SetOwningEngine (Engine* newOwningEngine)
{
	OwningEngine = newOwningEngine;
}

bool EngineComponent::GetTickingComponent () const
{
	return bTickingComponent;
}

std::vector<const DClass*> EngineComponent::GetPreInitializeDependencies () const
{
	return std::vector<const DClass*>();
}

std::vector<const DClass*> EngineComponent::GetInitializeDependencies () const
{
	return std::vector<const DClass*>();
}

std::vector<const DClass*> EngineComponent::GetPostInitializeDependencies () const
{
	return std::vector<const DClass*>();
}
SD_END