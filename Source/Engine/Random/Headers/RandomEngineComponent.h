/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RandomEngineComponent.h
  Simple engine component responsible for managing the random seed.
=====================================================================
*/

#pragma once

#include "Random.h"

SD_BEGIN
class RANDOM_API RandomEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(RandomEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Latest seed used on the number generator. */
	unsigned int Seed;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void SetSeed (unsigned int newSeed);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(unsigned int, Seed);
};
SD_END