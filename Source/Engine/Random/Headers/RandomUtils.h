/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RandomUtils.h
  Contains various/misc functions that yields different results based on fRand()
=====================================================================
*/

#pragma once

#include "Random.h"

SD_BEGIN
class RANDOM_API RandomUtils : public BaseUtils
{
	DECLARE_CLASS(RandomUtils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a random number ranging from [0,1].
	 */
	static FLOAT fRand ();

	/**
	 * Returns a random value ranging from [min, max]
	 */
	static FLOAT RandRange (FLOAT min, FLOAT max);

	/**
	 * Returns a random value from [0, max)
	 */
	static INT Rand (INT max);

	/**
	 * Returns a random 2D point within the circle of the given radius.
	 * Positive X is right of center; negative Y is below center.
	 */
	static Vector2 RandPointWithinCircle (FLOAT maxRadius);
	static Vector2 RandPointWithinCircle (FLOAT minRadius, FLOAT maxRadius);


	/*
	=====================
	  Templates
	=====================
	*/

	template <class T>
	static void ShuffleVector (std::vector<T>& outDataList)
	{
		RandomEngineComponent* randEngine = RandomEngineComponent::Find();
		CHECK(randEngine != nullptr)

		std::mt19937 twisterEngine;
		twisterEngine.seed(randEngine->GetSeed());
		std::shuffle(std::begin(outDataList), std::end(outDataList), twisterEngine);
	}
};
SD_END