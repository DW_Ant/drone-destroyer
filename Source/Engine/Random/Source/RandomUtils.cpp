/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RandomUtils.cpp
=====================================================================
*/

#include "RandomClasses.h"

SD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(SD, RandomUtils, SD, BaseUtils)

FLOAT RandomUtils::fRand ()
{
	int randResult = std::rand();

	return FLOAT(static_cast<float>(randResult) / RAND_MAX);
}

FLOAT RandomUtils::RandRange (FLOAT min, FLOAT max)
{
	return (fRand() * (max - min)) + min;
}

INT RandomUtils::Rand (INT max)
{
	//Reason for subtract 1.0e-6, covers the case where fRand returns 1.  Return value should always be less than max.
	return INT(max.ToFLOAT() * (fRand() - 1.0e-6f));
}

Vector2 RandomUtils::RandPointWithinCircle (FLOAT maxRadius)
{
	FLOAT selectedX = RandRange(-1.f, 1.f);

	FLOAT maxY = std::cos((selectedX * PI_FLOAT * 0.5f).Value);
	FLOAT selectedY = RandRange(-maxY, maxY);

	Vector2 result(selectedX, selectedY);
	result *= maxRadius;

#ifdef DEBUG_MODE
	CHECK(result.VSize() <= maxRadius);
#endif

	return result;
}

Vector2 RandomUtils::RandPointWithinCircle (FLOAT minRadius, FLOAT maxRadius)
{
	//Safety check
	if (minRadius > maxRadius)
	{
		//Display debugging message.  Swapping parameters wont cause harm to the application other than a minor performance hit.
		RandomLog.Log(LogCategory::LL_Warning, TXT("Bad parameters were given to RandPointWithinCircles(%s, %s).  The minRadius is greater than maxRadius.  Reversing parameters."), minRadius, maxRadius);

		FLOAT oldMinRadius = minRadius;
		minRadius = maxRadius;
		maxRadius = oldMinRadius;
	}

	Vector2 randPoint = RandPointWithinCircle(maxRadius - minRadius);
	if (randPoint.VSize() == 0)
	{
		//Random point ended up being in center of circle.  Pick a random direction to fulfill minRadius.
		FLOAT selectedTheta = RandRange(-PI_FLOAT, PI_FLOAT);
		FLOAT selectedX = cos(selectedTheta.Value) * minRadius;
		FLOAT selectedY = sin(selectedTheta.Value) * minRadius;
		return Vector2(selectedX, selectedY);
	}

	//Increase selected point by minRadius.  Solve using similar right triangles.
	FLOAT curRadius = sqrt(pow(randPoint.X.Value, 2) + pow(randPoint.Y.Value, 2));
	FLOAT deltaX = (randPoint.X * minRadius)/curRadius;
	FLOAT deltaY = (randPoint.Y * minRadius)/curRadius;

	return Vector2(randPoint.X + deltaX, randPoint.Y + deltaY);
}
SD_END