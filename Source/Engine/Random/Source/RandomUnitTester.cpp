/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RandomUnitTester.cpp
=====================================================================
*/

#include "RandomClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, RandomUnitTester, SD, UnitTester)

bool RandomUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		return (TestUtilityFunctions(testFlags) && TestRandomEngine(testFlags));
	}

	return true;
}

bool RandomUnitTester::TestUtilityFunctions (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Random Utilities"));

	TestLog(testFlags, TXT("Testing fRand.  fRand should be returning a random number from 0-1.  Running 20 iterations. . ."));
	for (UINT_TYPE i = 0; i < 20; i++)
	{
		FLOAT randResult = RandomUtils::fRand();
		TestLog(testFlags, TXT("    fRand returned %s"), randResult);

		if (randResult < 0 || randResult > 1)
		{
			UnitTestError(testFlags, TXT("fRand test failed.  The expected result for fRand should range between 0-1.  Instead it returned %s"), {randResult.ToString()});
			return false;
		}
	}
	TestLog(testFlags, TXT("fRand test passed!"));

	TestLog(testFlags, TXT("Testing RandRange for range from 50 - 1000.  Running 20 iterations. . ."));
	for (UINT_TYPE i = 0; i < 20; i++)
	{
		FLOAT randResult = RandomUtils::RandRange(50, 1000);
		TestLog(testFlags, TXT("    RandRange from 50 - 1000 returned:  %s"), randResult);

		if (randResult < 50 || randResult > 1000)
		{
			UnitTestError(testFlags, TXT("RandRange test failed.  The expected result for RandRange(50,1000) should be between 50-1000.  Instead it returned %s"), {randResult.ToString()});
			return false;
		}
	}
	TestLog(testFlags, TXT("RandRange test passed!"));

	TestLog(testFlags, TXT("Testing Rand for range between 0-9.  Running 20 iterations. . ."));
	for (UINT_TYPE i = 0; i < 20; i++)
	{
		INT randResult = RandomUtils::Rand(10);
		TestLog(testFlags, TXT("    Rand from 0-9 returned:  %s"), randResult);
		if (randResult < 0 || randResult > 9)
		{
			UnitTestError(testFlags, TXT("Rand test failed.  The expected result for Rand(10) should be between 0-9.  Instead it returned %s"), {randResult.ToString()});
			return false;
		}
	}
	TestLog(testFlags, TXT("Rand test passed!"));

	TestLog(testFlags, TXT("Testing shuffle vector of size 100.  Vector was created with sorted elements."));
	std::vector<INT> shuffledVector;
	for (INT i = 0; i < 100; i++)
	{
		shuffledVector.push_back(i);
	}
	RandomUtils::ShuffleVector(shuffledVector);
	for (UINT_TYPE i = 0; i < shuffledVector.size(); i++)
	{
		TestLog(testFlags, TXT("    shuffledVector[%s] = %s"), INT(i), shuffledVector.at(i));
	}

	TestLog(testFlags, TXT("Testing random points within a circle with max radius 50.  Running 20 iterations. . ."));
	for (UINT_TYPE i = 0; i < 20; i++)
	{
		Vector2 randPoint = RandomUtils::RandPointWithinCircle(50);
		TestLog(testFlags, TXT("    RandPoint within circle returned:  %s"), randPoint);

		if (randPoint.VSize() > 50)
		{
			UnitTestError(testFlags, TXT("RandPointWithinCircle test failed.  The distance from the given point and circle's center should be no greater than the circle's max radius (50).  Instead the distance from center and the given point is %s for point %s"), {randPoint.VSize().ToString(), randPoint.ToString()});
			return false;
		}
	}

	TestLog(testFlags, TXT("Testing random points within a hollow circle (min radius 100, max radius 1000).  Running 20 iterations. . ."));
	for (UINT_TYPE i = 0; i < 20; i++)
	{
		Vector2 randPoint = RandomUtils::RandPointWithinCircle(100, 1000);
		TestLog(testFlags, TXT("    RandPoint within hollow circle returned:  %s"), randPoint);

		if (randPoint.VSize() < 100 || randPoint.VSize() > 1000)
		{
			UnitTestError(testFlags, TXT("RandPointWithinCircle test failed.  The distance from the given point and the circle's center should be between 100-1000.  Instead the distance from center and the given point is %s for point %s"), {randPoint.VSize().ToString(), randPoint.ToString()});
			return false;
		}
	}
	TestLog(testFlags, TXT("RandPointWithinCircle test passed!"));

	ExecuteSuccessSequence(testFlags, TXT("Random Utilities"));
	return true;
}

bool RandomUnitTester::TestRandomEngine (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Random Engine"));

	RandomEngineComponent* randEngine = RandomEngineComponent::Find();
	CHECK(randEngine != nullptr)

	unsigned int oldSeed = randEngine->GetSeed();
	unsigned int testSeed = 1;
	randEngine->SetSeed(testSeed);
	
	std::vector<FLOAT> randNumbers;
	for (UINT_TYPE i = 0; i < 1000; i++)
	{
		randNumbers.push_back(RandomUtils::fRand());
	}

	TestLog(testFlags, TXT("Resetting seed to %s to see if it will reproduce the same 1k numbers."), INT(oldSeed));
	randEngine->SetSeed(testSeed);

	for (UINT_TYPE i = 0; i < 1000; i++)
	{
		FLOAT randFloat = RandomUtils::fRand();
		if (randFloat != randNumbers.at(i))
		{
			UnitTestError(testFlags, TXT("Random Engine unit test failed.  After resetting the seed back to %s, it should have produced the same set of numbers.  Instead fRand returned %s instead of the expected %s (at index %s)."), {DString::MakeString(oldSeed), randFloat.ToString(), randNumbers.at(i).ToString(), DString::MakeString(i)});
			randEngine->SetSeed(oldSeed);
			return false;
		}
	}

	TestLog(testFlags, TXT("After resetting the seed in RandomEngineComponent, the random number generator was able to reproduce the same set of numbers."));

	randEngine->SetSeed(testSeed);
	TestLog(testFlags, TXT("Shuffling a vector of size 100 with rand engine's seed set to %s."), INT(testSeed));
	std::vector<INT> shuffledVector;
	std::vector<INT> seededVector;
	for (INT i = 0; i < 100; ++i)
	{
		shuffledVector.push_back(i);
		seededVector.push_back(i);
	}
	RandomUtils::ShuffleVector(shuffledVector);

	TestLog(testFlags, TXT("Resetting seed to %s to see if it will shuffle the vector the exact same way as before."), INT(testSeed));
	RandomUtils::ShuffleVector(seededVector);
	for (UINT_TYPE i = 0; i < shuffledVector.size(); ++i)
	{
		if (shuffledVector.at(i) != seededVector.at(i))
		{
			UnitTestError(testFlags, TXT("Random Engine unit test failed.  After resetting the seed back to %s, it should have shuffled a vector the same way as before.  The original vector at index %s is %s.  The seeded vector at index %s is %s."), {DString::MakeString(testSeed), DString::MakeString(i), shuffledVector.at(i).ToString(), DString::MakeString(i), seededVector.at(i).ToString()});
			randEngine->SetSeed(oldSeed);
			return false;
		}
	}
	TestLog(testFlags, TXT("The shuffled vector is identical to the previous vector when it was shuffled with the same seed."));

	randEngine->SetSeed(oldSeed);
	ExecuteSuccessSequence(testFlags, TXT("Random Engine"));
	return true;
}
SD_END

#endif