/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CommandLineParser.h
  Contains several utilities meant to retrieve command line arguments.
  The parser expects the command line to either have key value pairs divided by an equals sign:  Key=Value
  Or a key prefixed with a hyphen (these are also known as switches):  -Flag
=====================================================================
*/

#pragma once

#include "Command.h"

SD_BEGIN

class COMMAND_API CommandLineParser : public Object
{
	DECLARE_CLASS(CommandLineParser)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delimiter character that'll divide multi-value key values.  For example if the delimiter was commas:  Key=Value1,Value2,Value3 */
	static const TCHAR MultiValueDelimiter;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the switch exists within the Engine's command line arguments.
	 */
	static bool ContainsSwitch (const Engine* localEngine, const DString& switchName);

	/**
	 * Returns true if the key exists in command line args and it is also assigned.
	 */
	static bool ContainsKeyValue (const Engine* localEngine, const DString& keyName);

	/**
	 * Searches through the command line args to find the matching key, then it returns the value for that key (the text after the '=' char).
	 * Returns empty text if the key is not found.
	 */
	static DString GetKeyValue (const Engine* localEngine, const DString& keyName);


	/*
	=====================
	  Templates
	=====================
	*/

	template <class T>
	static T GetKeyValueAsType (const Engine* localEngine, const DString& keyName)
	{
		const DString keyValueText = GetKeyValue(localEngine, keyName);
		if (keyValueText.IsEmpty())
		{
			return T();
		}

		return T(keyValueText);
	}
};

SD_END