/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CommandUnitTester.cpp
=====================================================================
*/

#include "CommandClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN

IMPLEMENT_CLASS(SD, CommandUnitTester, SD, UnitTester)

bool CommandUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		return (TestCommandLineParser(testFlags) && TestCommandList(testFlags) && TestExecCommand(testFlags));
	}

	return true;
}

bool CommandUnitTester::TestCommandLineParser (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Command Line Parser"));

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	DString originalCmdLine = localEngine->GetCommandLineArgs();
	DString tempCmdLine = TXT("-Switch1 -Switch2 MyINT=15 MyFLOAT=24.7 MyString=NoSpaces MySpaceString=\"String with spaces\" TestingQuotes=\"This -Switch should not be found.  FakeKey=However this -HiddenSwitch should be found.\" -HiddenSwitch");
	localEngine->SetCommandLineArgs(tempCmdLine);

	TestLog(testFlags, TXT("The Engine command line args reads:  \"%s\".  The command line args will restore to that value after this test."), originalCmdLine);
	TestLog(testFlags, TXT("Temporarily setting the engine command line args to:  %s"), tempCmdLine);
	DString switch1String = TXT("Switch1");
	BOOL bSwitch1 = CommandLineParser::ContainsSwitch(localEngine, switch1String);
	TestLog(testFlags, TXT("Does cmd line contain \"%s\"?  %s"), switch1String, bSwitch1);
	if (!bSwitch1)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find \"%s\" from \"%s\""), {switch1String, tempCmdLine});
		return false;
	}

	DString switch2String = TXT("Switch2");
	BOOL bSwitch2 = CommandLineParser::ContainsSwitch(localEngine, switch2String);
	TestLog(testFlags, TXT("Does cmd line contain \"%s\"?  %s"), switch2String, bSwitch2);
	if (!bSwitch2)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find \"%s\" from \"%s\""), {switch2String, tempCmdLine});
		return false;
	}

	DString switchString = TXT("Switch");
	BOOL bSwitch = CommandLineParser::ContainsSwitch(localEngine, switchString);
	TestLog(testFlags, TXT("Does cmd line contain \"%s\"?  %s"), switchString, bSwitch);
	if (bSwitch)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  It found \"%s\" from \"%s\""), {switchString, tempCmdLine});
		return false;
	}

	DString hiddenSwitchString = TXT("HiddenSwitch");
	BOOL bHiddenSwitch = CommandLineParser::ContainsSwitch(localEngine, hiddenSwitchString);
	TestLog(testFlags, TXT("Does cmd line contain \"%s\"?  %s"), hiddenSwitchString, bHiddenSwitch);
	if (!bHiddenSwitch)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find \"%s\" from \"%s\""), {hiddenSwitchString, tempCmdLine});
		return false;
	}

	DString intKey = TXT("MyINT");
	DString intValueStr = CommandLineParser::GetKeyValue(localEngine, intKey);
	INT intValue(intValueStr);
	TestLog(testFlags, TXT("The raw INT value from cmd line \"%s\" is:  \"%s\".  Converting that to an INT returns %s"), tempCmdLine, intValueStr, intValue);
	if (intValueStr != TXT("15") || intValue != 15)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find the expected value \"15\" for \"%s\" within \"%s\".  Instead it returned \"%s\"."), {intKey, tempCmdLine, intValueStr});
		return false;
	}

	DString floatKey = TXT("MyFLOAT");
	DString floatValueStr = CommandLineParser::GetKeyValue(localEngine, floatKey);
	FLOAT floatValue = CommandLineParser::GetKeyValueAsType<FLOAT>(localEngine, floatKey);
	TestLog(testFlags, TXT("The raw FLOAT value from cmd line \"%s\" is:  \"%s\".  Converting that to a FLOAT returns %s"), tempCmdLine, floatValueStr, floatValue);
	if (floatValueStr != TXT("24.7") || floatValue != 24.7f)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find the expected value \"24.7\" for \"%s\" within \"%s\".  Instead it returned \"%s\"."), {floatKey, tempCmdLine, floatValueStr});
		return false;
	}

	DString stringKey = TXT("MyString");
	DString stringValue = CommandLineParser::GetKeyValue(localEngine, stringKey);
	DString expectedStringValue = TXT("NoSpaces");
	TestLog(testFlags, TXT("The value of \"%s\" from cmd line \"%s\" is:  \"%s\"."), stringKey, tempCmdLine, stringValue);
	if (stringValue != expectedStringValue)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  The value of \"%s\" from \"%s\" does not match the expected value \"%s\".  Instead it returned \"%s\"."), {stringKey, tempCmdLine, expectedStringValue, stringValue});
		return false;
	}

	DString spaceStringKey = TXT("MySpaceString");
	DString spaceStringValue = CommandLineParser::GetKeyValue(localEngine, spaceStringKey);
	DString expectedSpaceStringValue = TXT("String with spaces");
	TestLog(testFlags, TXT("The value of \"%s\" from cmd line \"%s\" is:  \"%s\"."), spaceStringKey, tempCmdLine, spaceStringValue);
	if (spaceStringValue != expectedSpaceStringValue)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  The value of \"%s\" from \"%s\" does not match the expected value \"%s\".  Instead it returned \"%s\"."), {spaceStringKey, tempCmdLine, expectedSpaceStringValue, spaceStringValue});
		return false;
	}

	DString quoteKey = TXT("TestingQuotes");
	BOOL bHasQuoteKey = CommandLineParser::ContainsKeyValue(localEngine, quoteKey);
	TestLog(testFlags, TXT("Does cmd line contain value for key \"%s\"?  %s"), quoteKey, bHasQuoteKey);
	if (!bHasQuoteKey)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  Could not find \"%s\" from \"%s\""), {quoteKey, tempCmdLine});
		return false;
	}

	DString nonExistsKey = TXT("FakeKey");
	BOOL bHasFakeKey = CommandLineParser::ContainsKeyValue(localEngine, nonExistsKey);
	TestLog(testFlags, TXT("Does cmd line contain value for key \"%s\"?  %s"), nonExistsKey, bHasFakeKey);
	if (bHasFakeKey)
	{
		localEngine->SetCommandLineArgs(originalCmdLine);
		UnitTestError(testFlags, TXT("Command Line Parser test failed.  It found \"%s\" from \"%s\""), {nonExistsKey, tempCmdLine});
		return false;
	}

	TestLog(testFlags, TXT("Restoring engine command line args to:  \"%s\""), originalCmdLine);
	localEngine->SetCommandLineArgs(originalCmdLine);
	ExecuteSuccessSequence(testFlags, TXT("Command Line Parser"));
	return true;
}

bool CommandUnitTester::TestCommandList (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Command List"));
	SetTestCategory(testFlags, TXT("Parameter Extraction"));

	DString testCmd = TXT("MyCommand Param1 Param2 \"Param3 with Spaces\"");
	DString functionName;
	DString params;
	DString expectedFunctionName = TXT("MyCommand");
	DString expectedParams = TXT("Param1 Param2 \"Param3 with Spaces\"");
	CommandList::ExtractFunctionName(testCmd, functionName, params);
	TestLog(testFlags, TXT("Extracting function name from parameters from command \"%s\".  Function=\"%s\" Params=\"%s\""), testCmd, functionName, params);
	if (functionName != expectedFunctionName || params != expectedParams)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Extracting function name from its parameters from command \"%s\" should have returned function=\"%s\" and params=\"%s\".  Instead it returned function=\"%s\" and params=\"%s\""), {testCmd, expectedFunctionName, expectedParams, functionName, params});
		return false;
	}

	testCmd = TXT("MySimpleCommand");
	expectedFunctionName = TXT("MySimpleCommand");
	expectedParams = TXT("");
	CommandList::ExtractFunctionName(testCmd, functionName, params);
	TestLog(testFlags, TXT("Extracting function name from parameters from command \"%s\".  Function=\"%s\" Params=\"%s\""), testCmd, functionName, params);
	if (functionName != expectedFunctionName || params != expectedParams)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Extracting function name from its parameters from command \"%s\" should have returned function=\"%s\" and params=\"%s\".  Instead it returned function=\"%s\" and params=\"%s\""), {testCmd, expectedFunctionName, expectedParams, functionName, params});
		return false;
	}

	CompleteTestCategory(testFlags);
	SetTestCategory(testFlags, TXT("Find Registered Commands"));

	CommandEngineComponent* localCmdEngine = CommandEngineComponent::Find();
	CHECK(localCmdEngine != nullptr && localCmdEngine->GetCmdList() != nullptr);
	CommandList* cmdList = localCmdEngine->GetCmdList();
	DString targetExecFunction = TXT("CmdTester_NoParamFunction");
	const ExecCommand* execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_MultiParamFunction");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_2Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_3Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_4Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_5Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_6Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_7Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_8Param");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_OnlineOnly");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	targetExecFunction = TXT("CmdTester_CheatOfflineCommand");
	execFunction = cmdList->FindExecCommand(targetExecFunction);
	TestLog(testFlags, TXT("Searching for command:  %s"), targetExecFunction);
	if (execFunction == nullptr)
	{
		UnitTestError(testFlags, TXT("Command List test failed.  Failed to find exec command:  %s"), {targetExecFunction});
		return false;
	}

	CompleteTestCategory(testFlags);
	ExecuteSuccessSequence(testFlags, TXT("Command List"));
	return true;
}

bool CommandUnitTester::TestExecCommand (EUnitTestFlags testFlags) const
{
	//Redirect the test to an object that implements various exec commands
	ExecCommandTester* tester = ExecCommandTester::CreateObject();
	CHECK(tester != nullptr)

	bool bResult = tester->RunTest(this, testFlags);
	tester->Destroy();

	return bResult;
}
SD_END

#endif