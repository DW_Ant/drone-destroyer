/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CommandLineParser.cpp
=====================================================================
*/

#include "CommandClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, CommandLineParser, SD, Object)

const TCHAR CommandLineParser::MultiValueDelimiter = ',';

bool CommandLineParser::ContainsSwitch (const Engine* localEngine, const DString& switchName)
{
	//Append the engineParams with a space char to help with parsing (ie:  Ensure the last switch has a space after it).
	const DString engineParams = localEngine->GetCommandLineArgs() + TXT(" ");
	INT curIdx = 0;

	do
	{
		//The formula "-Switch " (space at end) is expected to handle case where there are switch names within other switch names "-Switch" and "-Switch1"
		curIdx = engineParams.Find(TXT("-") + switchName + TXT(" "), curIdx, DString::CC_IgnoreCase, DString::SD_LeftToRight);
		if (curIdx == INT_INDEX_NONE)
		{
			return false; //not found
		}

		if (!engineParams.IsWrappedByChar('\"', curIdx))
		{
			//This switch is not within quotes
			return true;
		}

		curIdx += (TXT("-") + switchName + TXT(" ")).Length();
	}
	while (curIdx < engineParams.Length());

	return false;
}

bool CommandLineParser::ContainsKeyValue (const Engine* localEngine, const DString& keyName)
{
	return !GetKeyValue(localEngine, keyName).IsEmpty();
}

DString CommandLineParser::GetKeyValue (const Engine* localEngine, const DString& keyName)
{
	const DString engineParams = TXT(" ") + localEngine->GetCommandLineArgs();
	INT curIdx = 0;

	do
	{
		curIdx = engineParams.Find(TXT(" ") + keyName + TXT("="), curIdx, DString::CC_IgnoreCase, DString::SD_LeftToRight);
		if (curIdx == INT_INDEX_NONE)
		{
			return DString::EmptyString; //not found
		}

		if (!engineParams.IsWrappedByChar('\"', curIdx))
		{
			break;
		}

		curIdx += (TXT(" ") + keyName + TXT("=")).Length();
	}
	while (curIdx < engineParams.Length());

	if (curIdx >= engineParams.Length())
	{
		return TXT(""); //Did not find the key that's not outside of quotes
	}

	//Return the contents after the '=' sign
	curIdx += (TXT(" ") + keyName + TXT("=")).Length();
	if (curIdx >= engineParams.Length())
	{
		return DString::EmptyString;
	}

	//If the next character is a double quote, then return the text between the double quotes
	if (engineParams.At(curIdx) == '\"')
	{
		//skip over first quote
		curIdx++;
		if (curIdx >= engineParams.Length())
		{
			CommandLog.Log(LogCategory::LL_Warning, TXT("Invalid command line arguments:  \"%s\".  The value of key \"%s\" suddenly ends right after opening with double quotes."), engineParams, keyName);
			return DString::EmptyString;
		}

		INT endQuoteIdx = engineParams.Find(TXT("\""), curIdx, DString::CC_IgnoreCase, DString::SD_LeftToRight);
		if (endQuoteIdx == INT_INDEX_NONE)
		{
			CommandLog.Log(LogCategory::LL_Warning, TXT("Invalid command line arguments:  \"%s\".  Could not find closing quotes for key \"%s\"."), engineParams, keyName);
			return DString::EmptyString;
		}

		return engineParams.SubString(curIdx, endQuoteIdx - 1);
	}

	//Return the text until the next space character
	INT spaceIdx = engineParams.Find(TXT(" "), curIdx, DString::CC_IgnoreCase, DString::SD_LeftToRight);
	if (spaceIdx == INT_INDEX_NONE)
	{
		spaceIdx = -1; //Set to negative so it'll return the rest of the string (in SubString function).
	}

	return engineParams.SubString(curIdx, spaceIdx - 1); //Minus 1 to spaceIdx to exclude the space character from result.
}
SD_END