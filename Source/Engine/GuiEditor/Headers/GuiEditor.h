/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiEditor.h
  Contains important file includes and definitions for the GuiEditor module.

  The GuiEditor module houses utilities and entities that'll make it easier to
  edit/configure existing UIs.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#pragma once

#include "Core\Headers\CoreClasses.h"
#include "Gui\Headers\GuiClasses.h"
#include "BaseEditor\Headers\BaseEditorClasses.h"
#include "Input\Headers\InputClasses.h"

#error The GuiEditor does not compile since it depends on the Base Editor module.

#if !(MODULE_CORE)
#error The GuiEditor module requires the Core module.
#endif

#if !(MODULE_GUI)
#error The GuiEditor module requires the Gui module.
#endif

#if !(MODULE_BASE_EDITOR)
#error The GuiEditor module requires the Base Editor module.
#endif

#if !(MODULE_INPUT)
#error The GuiEditor module requires the Input module.
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef GUI_EDITOR_EXPORT
		#define GUI_EDITOR_API __declspec(dllexport)
	#else
		#define GUI_EDITOR_API __declspec(dllimport)
	#endif
#else
	#define GUI_EDITOR_API
#endif

#define GUI_EDITOR_TITLE "Gui Editor"

SD_BEGIN
extern LogCategory GUI_EDITOR_API GuiEditorLog;
SD_END