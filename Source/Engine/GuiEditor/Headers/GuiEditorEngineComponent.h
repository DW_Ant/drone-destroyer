/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiEditorEngineComponent.h
  Engine component that enables Gui editing capabilities, and manages the window
  handle that renders the editor.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#pragma once

#include "GuiEditor.h"

SD_BEGIN
class GuiEditorContainer;

class GUI_EDITOR_API GuiEditorEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(GuiEditorEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Entity that'll kickoff the editor window. */
	DPointer<GuiEditorContainer> EditorLauncher;

	/* Window handle this container launched that'll be rendering the UI editor. */
	DPointer<Window> EditorWindow;

	/* Render Target that'll be drawing objects to the EditorWindow. */
	DPointer<RenderTarget> EditorRenderTarget;

	/* Input broadcaster that'll be polling input events from the EditorWindow. */
	DPointer<InputBroadcaster> EditorBroadcaster;

	/* Determines the refresh rate interval for the GuiEditor RenderTarget and Window handle (in seconds). */
	FLOAT EditorFrameRate;

private:
	/* Time needed before the window handle draws all pending objects. */
	FLOAT TimeRemaining;


	/*
	=====================
	  Constructors
	=====================
	*/

protected:
	GuiEditorEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PostInitializeComponent () override;
	virtual void PreTick (FLOAT deltaSec) override;
	virtual void PostTick (FLOAT deltaSec) override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes a new window handle for the editor if it's not yet instantiated.
	 */
	virtual void InitializeEditorWindow ();

	/**
	 * Sends a window message for shutdown before actually destroying the window handle.
	 */
	virtual void DestroyEditorWindow ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual Window* GetEditorWindow () const;
	virtual InputBroadcaster* GetEditorBroadcaster () const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleEditorLauncherDestroyed ();
	virtual void HandleWindowEvent (const sf::Event& newEvent);
};
SD_END