/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiEditorEngineComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GuiEditorClasses.h"

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(SD, GuiEditorEngineComponent)

GuiEditorEngineComponent::GuiEditorEngineComponent () : Super()
{
	bTickingComponent = true;

	EditorLauncher = nullptr;
	EditorWindow = nullptr;
	EditorRenderTarget = nullptr;
	EditorBroadcaster = nullptr;
	EditorFrameRate = 0.034f; //~30 frames per second

	TimeRemaining = EditorFrameRate;
}

void GuiEditorEngineComponent::PostInitializeComponent ()
{
	Super::PostInitializeComponent();

	//Create an entity may launch the editor sequence.
	EditorLauncher = GuiEditorContainer::CreateObject();

	//Setup OnDestroy callbacks in case something else destroyed that entity before this engine component destroyed it.
	EditorLauncher->OnDestroy = SDFUNCTION(this, GuiEditorEngineComponent, HandleEditorLauncherDestroyed, void);
}

void GuiEditorEngineComponent::PreTick (FLOAT deltaSec)
{
	Super::PreTick(deltaSec);

	if (EditorWindow != nullptr)
	{
		EditorWindow->PollWindowEvents();
	}

	if (EditorWindow != nullptr) //Double check is necessary since it's possible for PollWindowEvents to delete window
	{
		EditorWindow->Resource->clear(sf::Color::Black);
	}
}

void GuiEditorEngineComponent::PostTick (FLOAT deltaSec)
{
	Super::PostTick(deltaSec);

	if (EditorWindow != nullptr)
	{
		TimeRemaining  -= deltaSec;
		if (TimeRemaining <= 0)
		{
			EditorWindow->Resource->display();
			TimeRemaining = EditorFrameRate;
		}
	}
}

void GuiEditorEngineComponent::ShutdownComponent ()
{
	DestroyEditorWindow();

	if (EditorBroadcaster != nullptr)
	{
		EditorBroadcaster->Destroy();
		EditorBroadcaster = nullptr;
	}

	if (EditorLauncher != nullptr)
	{
		EditorLauncher->OnDestroy.ClearFunction();
		EditorLauncher->Destroy();
		EditorLauncher = nullptr;
	}

	Super::ShutdownComponent();
}

void GuiEditorEngineComponent::InitializeEditorWindow ()
{
	if (EditorWindow != nullptr)
	{
		return; //already initialized
	}

	EditorWindow = Window::CreateObject();
	EditorWindow->Resource = new sf::RenderWindow(sf::VideoMode(800, 600), GUI_EDITOR_TITLE);
	EditorWindow->Resource->setMouseCursorVisible(false);
	EditorWindow->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, GuiEditorEngineComponent, HandleWindowEvent, void, const sf::Event&));

	//Instantiate render target that'll draw to this window
	EditorRenderTarget = RenderTarget::CreateObject();
	EditorRenderTarget->SetFrameRate(EditorFrameRate);
	EditorRenderTarget->SetRenderWindow(EditorWindow.Get());

	TimeRemaining = EditorFrameRate;
}

void GuiEditorEngineComponent::DestroyEditorWindow ()
{
	if (EditorBroadcaster != nullptr)
	{
		EditorBroadcaster->Destroy();
		EditorBroadcaster = nullptr;
	}

	if (EditorWindow != nullptr)
	{
		EditorWindow->Destroy();
		EditorWindow = nullptr;
	}

	if (EditorRenderTarget != nullptr)
	{
		EditorRenderTarget->Destroy();
		EditorRenderTarget = nullptr;
	}
}

Window* GuiEditorEngineComponent::GetEditorWindow () const
{
	return EditorWindow.Get();
}

InputBroadcaster* GuiEditorEngineComponent::GetEditorBroadcaster () const
{
	return EditorBroadcaster.Get();
}

void GuiEditorEngineComponent::HandleEditorLauncherDestroyed ()
{
	EditorLauncher = nullptr;
}

void GuiEditorEngineComponent::HandleWindowEvent (const sf::Event& newEvent)
{
	if (newEvent.type == sf::Event::Closed)
	{
		DestroyEditorWindow();
	}
}
SD_END