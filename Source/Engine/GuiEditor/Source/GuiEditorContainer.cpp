/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiEditorContainer.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GuiEditorClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, GuiEditorContainer, SD, Entity)

void GuiEditorContainer::InitProps ()
{
	Super::InitProps();

	Editor = nullptr;
}

void GuiEditorContainer::BeginObject ()
{
	Super::BeginObject();

	//Create an input component listening for events on the main window to listen for the signal to launch editor.
	InputComponent* input = InputComponent::CreateObject();
	if (AddComponent(input))
	{
		input->SetInputPriority(500);
		input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, GuiEditorContainer, HandleKeyInput, bool, const sf::Event&);
	}
}

void GuiEditorContainer::Destroyed ()
{
	if (OnDestroy.IsBounded())
	{
		OnDestroy();
	}

	Super::Destroyed();
}

void GuiEditorContainer::ActivateEditor ()
{
	GuiEditorEngineComponent* localEditorEngine = GuiEditorEngineComponent::Find();
	CHECK(localEditorEngine != nullptr)

	localEditorEngine->InitializeEditorWindow();
	//TODO:  the Gui, itself
}

//Must press Ctrl + Alt + G to activate editor
bool GuiEditorContainer::HandleKeyInput (const sf::Event& newKeyEvent)
{
	InputBroadcaster* mainBroadcaster = InputEngineComponent::Find()->GetMainBroadcaster();
	if (mainBroadcaster == nullptr || !mainBroadcaster->GetCtrlHeld() || !mainBroadcaster->GetAltHeld())
	{
		return false;
	}

	if (newKeyEvent.type == sf::Event::KeyReleased && newKeyEvent.key.code == sf::Keyboard::G) //G for Gui editor
	{
		ActivateEditor();
		return true;
	}

	return false;
}
SD_END