/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BaseEditor.h
  Contains important file includes and definitions for the Base Editor module.

  The Base Editor module defines essential utilities all editors (such as map, Gui, and particle
  editors) will be using.  This editor will define the following:
  
	-Meta data for properties (such as clamping, tooltips, and rendering).
	-Maintain object variables so that properties may be iterated.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#pragma once

#include "Core\Headers\CoreClasses.h"
#include "Graphics\Headers\GraphicsClasses.h"
#include "Input\Headers\InputClasses.h"
#include "Gui\Headers\GuiClasses.h"

#error The Base Editor is not compatible with the Graphics Engine changes, and is scheduled for a major refactor.

#if !(MODULE_CORE)
#error The Base Editor module requires the Core module.
#endif

#if !(MODULE_GRAPHICS)
#error The Base Editor module requires the Graphics module.
#endif

#if !(MODULE_INPUT)
#error The Base Editor module requires the Input module.
#endif

#if !(MODULE_GUI)
#error The Base Editor module requires the Gui module.
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef BASE_EDITOR_EXPORT
		#define BASE_EDITOR_API __declspec(dllexport)
	#else
		#define BASE_EDITOR_API __declspec(dllimport)
	#endif
#else
	#define BASE_EDITOR_API
#endif

SD_BEGIN
extern LogCategory BASE_EDITOR_API BaseEditorLog;
SD_END