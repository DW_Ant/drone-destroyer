/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableVector3.h
  Defines Vector3-specific functionality to assist developers when making changes to Vector3s.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#pragma once

#include "EditableVector2.h"

SD_BEGIN
class BASE_EDITOR_API EditableVector3 : public EditableVector2
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Various callbacks for each axis. */
	std::function<void(DString&)> OnZPreChange;
	std::function<void(DString&)> OnZPostChange;

protected:
	/* The data type owning this editable property object. */
	Vector3* Vect3OwningProperty;

	/* Entities instantiated to render individual axis. */
	DPointer<PropertyLine> PropertyLineZ;

	/* Editable variables for the x,y axis. */
	DPointer<EditableFLOAT> EditableZ;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	EditableVector3 ();
	EditableVector3 (const EditableVector3& copyProperty);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void SetOwningProperty (DProperty* newOwningProperty) override;
	virtual void SetPropertyEditor (PropertyLine* newPropertyEditor) override;
	virtual void SetReadOnly (bool bNewReadOnly) override;
	virtual INT GetMaxHeight () const override;

protected:
	virtual void InitializeAxis () override;
	virtual void ExpandProperty () override;
	virtual void CollapseProperty () override;
	virtual void RemoveAxisPropertyLines () override;
	virtual DProperty* GetXAxis () const override;
	virtual DProperty* GetYAxis () const override;
	virtual DProperty* GetOwningProperty () const override;
	virtual void HandleAxisBeginTextEdit (PropertyLine* focusedLine) override;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleZPreChange (DString& outPropertyValue);
	virtual void HandleZPostChange (DString& outPropertyValue);
	virtual void HandleZLineDestroyed ();
};
SD_END