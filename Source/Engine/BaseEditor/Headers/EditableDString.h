/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableDString.h
  Defines DString-specific functionality to assist developers when making changes to DStrings.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#pragma once

#include "EditableProperty.h"

SD_BEGIN
class BASE_EDITOR_API EditableDString : public EditableProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, then this string's character limit is specified by the MaxCharacters value. */
	bool bApplyCharLimit;

	/* Determines the max number of characters allowed for this string (excluding null terminator).  Only applicable if bApplyCharLimit is true. */
	INT MaxCharacters;

protected:
	/* The data type owning this editable property object. */
	DString* OwningProperty;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	EditableDString ();
	EditableDString (const EditableDString& copyProperty);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void SetOwningProperty (DProperty* newOwningProperty) override;
	virtual void EditVariable (DString& outNewValue) override;
	virtual DString GetStringValue () const override;
	virtual DString ExportVariable () const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ApplyCharLimit ();
};
SD_END