/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableBOOL.h
  Defines BOOL-specific functionality to assist developers when making changes to BOOLs.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#pragma once

#include "EditableProperty.h"

SD_BEGIN
class BASE_EDITOR_API EditableBOOL : public EditableProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Texture icon that'll render the checkbox. */
	DPointer<Texture> CheckboxTexture;

protected:
	/* The data type owning this editable property object. */
	BOOL* OwningProperty;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	EditableBOOL ();
	EditableBOOL (const EditableBOOL& copyProperty);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void SetOwningProperty (DProperty* newOwningProperty) override;
	virtual void EditVariable (DString& outNewValue) override;
	virtual DString GetStringValue () const override;
	virtual DString ExportVariable () const override;
	virtual bool OverrideMouseClickEvent (const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType eventType, INT mouseRelevance) override;
	virtual void HandleRender (PropertyLineRenderComponent* propertyRenderer, const sf::Transformable& renderTransform, const Window* targetWindow, INT& outRenderFlags, FLOAT& outPropNameOffset) override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Queries the TexturePool to retrieve the checkbox texture reference.
	 */
	virtual Texture* FindCheckboxTexture () const;
};
SD_END