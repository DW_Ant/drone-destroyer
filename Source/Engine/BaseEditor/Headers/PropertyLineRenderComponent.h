/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PropertyLineRenderComponent.h
  A component responsible for rendering the contents of an editable property.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#pragma once

#include "BaseEditor.h"

SD_BEGIN
class BASE_EDITOR_API PropertyLineRenderComponent : public RenderComponent
{
	DECLARE_CLASS(PropertyLineRenderComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Various render flags that may override which aspects of the render process should be visible. */
	static const INT RENDER_PROPERTY_DIVIDER;
	static const INT RENDER_PROPERTY_NAME;
	static const INT RENDER_PROPERTY_VALUE;
	static const INT RENDER_TEXT_CURSOR;
	static const INT RENDER_READ_ONLY_FOREGROUND;
	static const INT RENDER_ALL;

	/* Color at which the background is drawn. */
	sf::Color BackgroundColor;

	/* Read only foreground to overlay over the property value section. */
	sf::Color ForegroundColor;

	/* Color to render the text. */
	sf::Color TextColor;

	/* If true, then this render component will render the foreground that represents that this property is read only. */
	bool bPropertyReadOnly;

	INT TextSize;

	/* How many pixels does the divider between the PropertyName and PropertyValue take. */
	FLOAT DividerWidth;

	/* Callback to invoke to add or override the Value portion of this render component. */
	SDFunction<void, PropertyLineRenderComponent*, const sf::Transformable&, const Window*, INT&, FLOAT&> OnRender;

protected:
	/* Position the property name/value divider relative to the transform interface. */
	FLOAT PropertyNameWidth;

	/* If true then the user is currently typing into the property text. */
	bool bUserEnteringValue;

	DString PropertyName;
	DString PropertyValue;

private:
	/* Cached versions of PropertyName/Value so that the Renderer doesn't have to compute clamped values on every frame. */
	DString ClampedPropertyName;
	DString ClampedPropertyValue;

	/* Recorded width value the TransformationInterface reported on previous Render cycle. */
	FLOAT PrevWidth;

	/* Recorded property name x position offset generated from the OnRender callback.  This is used to detect changes in x position offset, and if there's a need to recalculate word wrapping. */
	FLOAT PrevPosXOffset;

	DPointer<FontPool> LocalFontPool;
	Engine* LocalEngine;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void CalculateClampedPropertyValue ();
	virtual void CalculateClampedPropertyName ();

	virtual void SetPropertyNameWidth (FLOAT newPropertyNameWidth);
	virtual void SetPropertyName (const DString& newPropertyName);
	virtual void SetPropertyValue (const DString& newPropertyValue);
	virtual void SetUserEnteringValue (bool bIsEnteringValue);

	/**
	 * Appends text to the current property value.
	 */
	virtual void AddPropertyValue (const DString& addedText);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual FLOAT GetPropertyNameWidth () const;
	virtual DString GetPropertyName () const;
	virtual DString GetPropertyValue () const;
	virtual bool GetUserEnteringValue () const;
};
SD_END