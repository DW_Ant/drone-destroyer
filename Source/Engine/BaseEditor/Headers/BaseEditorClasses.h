/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BaseEditorClasses.h
  Contains all header includes for the Base Editor module.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#pragma once

#define MODULE_BASEEDITOR 1

#include "BaseEditor.h"
#include "BaseEditorEngineComponent.h"
#include "EditableProperty.h"
#include "EditableINT.h"
#include "EditableFLOAT.h"
#include "EditableDString.h"
#include "EditableBOOL.h"
#include "EditableVector2.h"
#include "EditableVector3.h"
#include "PropertyLine.h"
#include "PropertyLineRenderComponent.h"
#include "CollapsibleProperty.h"

#ifdef DEBUG_MODE
#include "BaseEditorUnitTester.h"
#include "BaseEditorTester.h"
#endif