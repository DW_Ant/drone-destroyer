/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CollapsibleProperty.h
  Adds a button to the editable property that listens for click events.  When clicked, it'll execute
  Collapse or Expand property functions.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#pragma once

#include "EditableProperty.h"

SD_BEGIN
class BASE_EDITOR_API CollapsibleProperty
{


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Becomes true if this property is expanded. */
	bool bExpanded;

	/* Collapsible button attributes (size and position) on previous render call. */
	Rectangle<FLOAT> CollapsibleButtonAttributes;



	/*
	=====================
	  Constructors
	=====================
	*/

public:
	CollapsibleProperty ();
	CollapsibleProperty (const CollapsibleProperty& copyProperty);


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Renders a collapsible button near the property name section.  Returns the button's x size.
	 */
	virtual FLOAT RenderCollapsibleButton (PropertyLineRenderComponent* propertyRenderer, const sf::Transformable& renderTransform, const Window* targetWindow, INT& outRenderFlags);

	/**
	 * Checks the mouse click event to see if it clicked on the collapsible button.
	 * Returns true if the event is over the button.
	 */
	virtual bool MouseEventOverCollapsibleButton (const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType eventType, INT mouseRelevance);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if the given mouse coordinates are over the collapsible button.
	 */
	virtual bool CoordinatesOverCollapsibleButton (FLOAT x, FLOAT y);

	/**
	 * Invoked whenever the collapsed button was clicked.
	 */
	virtual void ExpandProperty ();

	/**
	 * Invoked whenever the expanded button was clicked.
	 */
	virtual void CollapseProperty ();
};
SD_END