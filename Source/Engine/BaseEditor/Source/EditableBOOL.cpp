/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableBOOL.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

using namespace std;

SD_BEGIN
EditableBOOL::EditableBOOL () : EditableProperty()
{
	CheckboxTexture = nullptr;
	OwningProperty = nullptr;
}

EditableBOOL::EditableBOOL (const EditableBOOL& copyProperty) : EditableProperty(copyProperty)
{
	CheckboxTexture = copyProperty.CheckboxTexture;
}

void EditableBOOL::SetOwningProperty (DProperty* newOwningProperty)
{
	EditableProperty::SetOwningProperty(newOwningProperty);

	OwningProperty = dynamic_cast<BOOL*>(newOwningProperty);
	if (OwningProperty == nullptr)
	{
		Engine::FindEngine()->FatalError(TXT("EditableBOOLs may only describe BOOL datatypes."));
	}
}

void EditableBOOL::EditVariable (DString& outNewValue)
{
	if (OnPrePropertyChange != nullptr)
	{
		OnPrePropertyChange(outNewValue);
	}

	OwningProperty->Value = outNewValue.ToBool();
	outNewValue = OwningProperty->ToString();

	if (OnPostPropertyChange != nullptr)
	{
		OnPostPropertyChange(outNewValue);
	}
}

DString EditableBOOL::GetStringValue () const
{
	return OwningProperty->ToString();
}

DString EditableBOOL::ExportVariable () const
{
	return PropertyName + TXT("=") + OwningProperty->ToString();
}

bool EditableBOOL::OverrideMouseClickEvent (const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType eventType, INT mouseRelevance)
{
	if (mouseRelevance == 0 || GetReadOnly()) //Not over current property
	{
		return false;
	}

	if (mouseRelevance < 2 || eventType != sf::Event::MouseButtonReleased)
	{
		return true; //still consume mouse event
	}

	*OwningProperty = !(*OwningProperty);
	EditVariable(OwningProperty->ToString()); //Invoke OnChange callbacks

	return true;
}

void EditableBOOL::HandleRender (PropertyLineRenderComponent* propertyRenderer, const sf::Transformable& renderTransform, const Window* targetWindow, INT& outRenderFlags, FLOAT& outPropNameOffset)
{
	if (CheckboxTexture == nullptr)
	{
		CheckboxTexture = FindCheckboxTexture();
		if (CheckboxTexture == nullptr)
		{
			return;
		}
	}

	outRenderFlags = PropertyLineRenderComponent::RENDER_PROPERTY_DIVIDER | PropertyLineRenderComponent::RENDER_PROPERTY_NAME | PropertyLineRenderComponent::RENDER_READ_ONLY_FOREGROUND; //Prevent the renderer from drawing property value text, but leave ReadOnly color overlay alone.

	sf::Vector2u textureSize = CheckboxTexture->GetTextureResource()->getSize();
	Vector2 sdTextureSize = CheckboxTexture->GetDimensions();

	//Expected to draw checkbox as a 1x2 texture where the bottom half is the checked checkbox.
	sf::IntRect sfRect;
	sfRect.width = textureSize.x;
	sfRect.height = textureSize.y/2; //Only render one of the checkboxes
	sfRect.left = 0;

	if (OwningProperty->Value)
	{
		sfRect.top = sfRect.height; //Render bottom half for checked checkbox
	}
	else
	{
		sfRect.top = 0; //Render top half
	}

	sf::Sprite sprite(*CheckboxTexture->GetTextureResource());
	sprite.setTextureRect(sfRect);

	Vector2 spriteScale = Vector2(renderTransform.getScale().y, renderTransform.getScale().y);
	spriteScale /= (sdTextureSize * Vector2(1.f,0.5f)); //(1,0.5) is to take account of subdivision where only a part of the texture is drawn.
	sprite.setScale(Vector2::SDtoSFML(spriteScale));
	sprite.setPosition(sf::Vector2f(renderTransform.getPosition().x + propertyRenderer->GetPropertyNameWidth().Value + propertyRenderer->DividerWidth.Value + 4.f, renderTransform.getPosition().y));
	targetWindow->Resource->draw(sprite);
}

Texture* EditableBOOL::FindCheckboxTexture () const
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	if (localTexturePool != nullptr)
	{
		return localTexturePool->FindTexture(TXT("Editor.Checkbox"));
	}

	BaseEditorLog.Log(LogCategory::LL_Warning, TXT("Failed to find EditableBOOL's Checkbox Texture since the current thread does not have a TexturePool."));
	return nullptr;
}
SD_END