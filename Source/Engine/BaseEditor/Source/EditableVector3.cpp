/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableVector3.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

using namespace std;

SD_BEGIN
EditableVector3::EditableVector3 () : EditableVector2()
{
	Vect3OwningProperty = nullptr;

	PropertyLineZ = nullptr;

	EditableZ = nullptr;
}

EditableVector3::EditableVector3 (const EditableVector3& copyProperty) : EditableVector2(copyProperty)
{
	//Copied vectors will maintain their own instances of editable axis.
	PropertyLineZ = nullptr;

	EditableZ = nullptr;
}

void EditableVector3::SetOwningProperty (DProperty* newOwningProperty)
{
	Vect3OwningProperty = dynamic_cast<Vector3*>(newOwningProperty);
	if (Vect3OwningProperty == nullptr)
	{
		Engine::FindEngine()->FatalError(TXT("EditableVector3s may only describe Vector3 datatypes."));
		return;
	}

	InitializeAxis();
}

void EditableVector3::SetPropertyEditor (PropertyLine* newPropertyEditor)
{
	EditableVector2::SetPropertyEditor(newPropertyEditor);

	if (newPropertyEditor == nullptr)
	{
		return;
	}

	PropertyLineZ = newPropertyEditor->CreateObjectOfMatchingClass();
	if (PropertyLineZ != nullptr)
	{
		PropertyLineZ->GetTransform()->SetAbsCoordinates(Vector2(0.f, newPropertyEditor->GetTransform()->GetCurrentSizeY() * 3.f));
		PropertyLineZ->GetTransform()->SetRelativeTo(newPropertyEditor->GetTransform());
		PropertyLineZ->SetActive(bExpanded);
		PropertyLineZ->OnDestroy = SDFUNCTION(this, EditableVector3, HandleYLineDestroyed, void);
		PropertyLineZ->OnBeginTextEdit = SDFUNCTION_1PARAM(this, EditableVector3, HandleAxisBeginTextEdit, void, PropertyLine*);

		if (EditableZ != nullptr)
		{
			PropertyLineZ->BindPropertyVariable(EditableZ.Get());
		}
	}
}

void EditableVector3::SetReadOnly (bool bNewReadOnly)
{
	EditableVector2::SetReadOnly(bNewReadOnly);

	if (EditableZ != nullptr)
	{
		EditableZ->SetReadOnly(bNewReadOnly);
	}
}

INT EditableVector3::GetMaxHeight () const
{
	return 4; //This + X + Y + Z
}

void EditableVector3::ExpandProperty ()
{
	EditableVector2::ExpandProperty();

	if (PropertyLineZ != nullptr)
	{
		PropertyLineZ->SetActive(true);
	}
}

void EditableVector3::CollapseProperty ()
{
	EditableVector2::CollapseProperty();

	if (PropertyLineZ != nullptr)
	{
		PropertyLineZ->SetActive(false);
	}
}

void EditableVector3::InitializeAxis ()
{
	EditableVector2::InitializeAxis();

	if (EditableZ != nullptr)
	{
		delete EditableZ.Get();
	}

	EditableZ = new EditableFLOAT();
	EditableZ->SetReadOnly(GetReadOnly());
	EditableZ->PropertyName = TXT("Z");
	EditableZ->SetOwningProperty(&Vect3OwningProperty->Z);
	EditableZ->OnPrePropertyChange = SDFUNCTION_1PARAM(this, EditableVector3, HandleZPreChange, void, DString&);
	EditableZ->OnPostPropertyChange = SDFUNCTION_1PARAM(this, EditableVector3, HandleZPostChange, void, DString&);
	if (PropertyLineZ != nullptr)
	{
		PropertyLineZ->BindPropertyVariable(EditableZ.Get());
	}
}

void EditableVector3::RemoveAxisPropertyLines ()
{
	EditableVector2::RemoveAxisPropertyLines();

	if (PropertyLineZ != nullptr)
	{
		PropertyLineZ->Destroy();
		PropertyLineZ = nullptr;
	}
}

DProperty* EditableVector3::GetXAxis () const
{
	return &Vect3OwningProperty->X;
}

DProperty* EditableVector3::GetYAxis () const
{
	return &Vect3OwningProperty->Y;
}

DProperty* EditableVector3::GetOwningProperty () const
{
	return Vect3OwningProperty;
}

void EditableVector3::HandleAxisBeginTextEdit (PropertyLine* focusedLine)
{
	if (focusedLine == PropertyLineX)
	{
		PropertyLineY->ApplyEdits();
		PropertyLineZ->ApplyEdits();
	}
	else if (focusedLine == PropertyLineY)
	{
		PropertyLineX->ApplyEdits();
		PropertyLineZ->ApplyEdits();
	}
	else
	{
		PropertyLineX->ApplyEdits();
		PropertyLineY->ApplyEdits();
	}

	//Notify parent property line that this property just gained edit focus
	if (PropertyEditor != nullptr)
	{
		PropertyEditor->SetTextFocus(true);
	}
}

void EditableVector3::HandleZPreChange (DString& outPropertyValue)
{
	if (OnZPreChange != nullptr)
	{
		OnZPreChange(outPropertyValue);
	}

	if (OnPrePropertyChange != nullptr)
	{
		OnPrePropertyChange(GetOwningProperty()->ToString());
	}
}

void EditableVector3::HandleZPostChange (DString& outPropertyValue)
{
	if (OnZPostChange != nullptr)
	{
		OnZPostChange(outPropertyValue);
	}

	DString vectorString = GetOwningProperty()->ToString();
	if (OnPostPropertyChange != nullptr)
	{
		OnPostPropertyChange(vectorString);
	}

	//Update main text field (making a change in an axis should cause the main property field to update, too).
	if (PropertyEditor != nullptr)
	{
		PropertyEditor->SetPropertyTextValue(vectorString);	
	}
}

void EditableVector3::HandleZLineDestroyed ()
{
	PropertyLineZ = nullptr;
}
SD_END