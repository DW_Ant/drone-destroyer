/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BaseEditorEngineComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

using namespace std;

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(SD, BaseEditorEngineComponent)

BaseEditorEngineComponent::BaseEditorEngineComponent () : Super()
{
	CollapsibleButtonTexture = nullptr;
}

void BaseEditorEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	localTexturePool->CreateAndImportTexture(TXT("Editor") + DString(DIR_SEPARATOR) + TXT("CursorResizeHorizontal.png"), TXT("Editor.CursorResizeHorizontal"));
	localTexturePool->CreateAndImportTexture(TXT("Editor") + DString(DIR_SEPARATOR) + TXT("Checkbox.png"), TXT("Editor.Checkbox"));
	CollapsibleButtonTexture = localTexturePool->CreateAndImportTexture(TXT("Editor") + DString(DIR_SEPARATOR) + TXT("CollapsibleButton.png"), TXT("Editor.CollapsibleButton"));
}
SD_END