/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MousePointer.cpp
=====================================================================
*/

#include "InputClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, MousePointer, SD, Entity)

void MousePointer::InitProps ()
{
	Super::InitProps();

	SetEnableFractionScaling(false);

	bContinuouslyEvalClampPos = true;
	bContinuouslyEvalMouseIcon = true;

	ShowMouseValue = 0;
	bLocked = false;
	MouseIcon = nullptr;
	MouseInput = nullptr;
	DefaultIcon = nullptr;
	OwningWindowHandle = nullptr;
}

void MousePointer::BeginObject ()
{
	Super::BeginObject();

	MouseInput = InputComponent::CreateObject();
	if (!AddComponent(MouseInput.Get()))
	{
		Engine::FindEngine()->FatalError(TXT("Unable to attach input component to Mouse Pointer object."));
		return;
	}
	MouseInput->MouseMoveDelegate = SDFUNCTION_3PARAM(this, MousePointer, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
	//This should be the first interface to receive mouse input (so that mouse positions are updated first).
	InputEngineComponent::Find()->GetMainBroadcaster()->SetInputPriority(MouseInput.Get(), 1000000);

	MouseIcon = SpriteComponent::CreateObject();
	if (!AddComponent(MouseIcon.Get()))
	{
		Engine::FindEngine()->FatalError(TXT("Failed to attached SpriteComponent to a mouse pointer object."));
		return;
	}

	SetPivot(MouseIcon->GetTextureSize() * 0.5f); //Center sprite over center
	SetSize(GetDefaultMouseSize());
	SetDepth(MAXFLOAT); //Ensure this mouse is drawn over anything in the Canvas layer
}

void MousePointer::Destroyed ()
{
	SetOwningWindowHandle(nullptr);

	Super::Destroyed();
}

Vector2 MousePointer::GetAbsMousePosition ()
{
	return OS_GetMousePosition();
}

Vector2 MousePointer::GetRelativeMousePosition (Window* windowHandle)
{
	if (windowHandle == nullptr)
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Attempted to retrieve relative mouse position without passing in a window handle.  Absolute position is returned instead."));
		return GetAbsMousePosition();
	}

	INT windowPosX;
	INT windowPosY;
	windowHandle->GetWindowPosition(OUT windowPosX, OUT windowPosY, false);

	Vector2 absMousePos = OS_GetMousePosition();
	return Vector2(absMousePos.X - windowPosX.ToFLOAT(), absMousePos.Y - windowPosY.ToFLOAT());
}

void MousePointer::SetAbsMousePosition (const Vector2& newMousePos)
{
	OS_SetMousePosition(newMousePos);
}

void MousePointer::SetRelativeMousePosition (Window* windowHandle, const Vector2& newMousePos)
{
	if (windowHandle == nullptr)
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Attempted to set mouse position relative to a window without passing in the window, itself.  Coordinates will be set in desktop space instead."));
		SetAbsMousePosition(newMousePos);
		return;
	}

	INT windowPosX;
	INT windowPosY;
	windowHandle->GetWindowPosition(OUT windowPosX, OUT windowPosY, false);

	OS_SetMousePosition(Vector2(newMousePos.X + windowPosX.ToFLOAT(), newMousePos.Y + windowPosY.ToFLOAT()));
}

void MousePointer::ConditionallyEvaluateMouseIconOverride (SDFunction<Texture*, MousePointer*, const sf::Event::MouseMoveEvent&> targetCallback)
{
	if (IsIconOverrideCurrentlyViewed(targetCallback))
	{
		EvaluateMouseIconOverrides();
	}
}

void MousePointer::PushPositionLimit (Rectangle<INT> newRegionLimit, SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> limitCallback)
{
	if (!limitCallback.IsBounded())
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Cannot set a mouse limit region to (%s) without associating a callback for this region.  The role of the callback is notifiy the MousePointer when the region is no longer relevant."), newRegionLimit);
		return;
	}

	SMouseLimit newLimitEntry;
	newLimitEntry.LimitRegion = newRegionLimit;
	newLimitEntry.LimitCallback = limitCallback;
	ClampPositionsStack.push_back(newLimitEntry);

	Rectangle<INT> newRegion = newLimitEntry.LimitRegion;
	//Set the curLimit relative to the OS's borders.
	AddWindowOffsetTo(newRegion);

	OS_ClampMousePointer(newRegion);
}

bool MousePointer::RemovePositionLimit (SDFunction<bool, MousePointer*, const sf::Event::MouseMoveEvent&> targetCallback)
{
	for (UINT_TYPE i = 0; i < ClampPositionsStack.size(); i++) //This is the reason why ClampPositionsStack is a vector instead of a stack
	{
		if (targetCallback == ClampPositionsStack.at(i).LimitCallback)
		{
			ClampPositionsStack.erase(ClampPositionsStack.begin() + i);
			if (ClampPositionsStack.size() == 0)
			{
				//Last clamp was removed.  Remove clamped borders.
				OS_ClampMousePointer(Rectangle<INT>());
			}
			else if (i >= ClampPositionsStack.size())
			{
				//Last element was removed, reapply new limits
				EvaluateClampedMousePointer();
			}

			return true;
		}
	}

	return false;
}

void MousePointer::PushMouseIconOverride (const Texture* newIcon, SDFunction<Texture*, MousePointer*, const sf::Event::MouseMoveEvent&> callbackFunction)
{
	if (!callbackFunction.IsBounded())
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Cannot set a mouse pointer icon (%s) without associating a callback for this override.  The role of the callback is notifiy the MousePointer when it can stop using this new icon."), newIcon->GetTextureName());
		return;
	}

	MouseIconOverrideStack.push_back(callbackFunction);
	MouseIcon->SetSpriteTexture(newIcon);
	UpdateSpriteOffset();
}

bool MousePointer::RemoveIconOverride (SDFunction<Texture*, MousePointer*, const sf::Event::MouseMoveEvent&> targetCallback)
{
	for (UINT_TYPE i = 0; i < MouseIconOverrideStack.size(); i++) //This is the reason why MouseIconOverrideStack is a vector instead of a stack
	{
		if (targetCallback == MouseIconOverrideStack.at(i))
		{
			MouseIconOverrideStack.erase(MouseIconOverrideStack.begin() + i);
			if (i >= MouseIconOverrideStack.size())
			{
				//Last element was removed, reapply new limits
				EvaluateMouseIconOverrides();
			}

			return true;
		}
	}

	return false;
}

bool MousePointer::IsIconOverrideCurrentlyViewed (SDFunction<Texture*, MousePointer*, const sf::Event::MouseMoveEvent&> targetCallback) const
{
	if (MouseIconOverrideStack.size() <= 0)
	{
		return false;
	}

	return (MouseIconOverrideStack.back() == targetCallback);
}

bool MousePointer::IsIconOverrideRegistered (SDFunction<Texture*, MousePointer*, const sf::Event::MouseMoveEvent&> targetCallback) const
{
	return (ContainerUtils::FindInVector(MouseIconOverrideStack, targetCallback) != UINT_INDEX_NONE);
}

void MousePointer::SetDefaultIcon (Texture* newIcon)
{
	DefaultIcon = newIcon;

	if (MouseIcon != nullptr && MouseIconOverrideStack.size() <= 0)
	{
		MouseIcon->SetSpriteTexture(DefaultIcon.Get());
		UpdateSpriteOffset();
	}
}

void MousePointer::SetMousePosition (INT newX, INT newY)
{
	if (bLocked)
	{
		return;
	}

	SetPosition(Vector2(newX.ToFLOAT(), newY.ToFLOAT()));

	if (bContinuouslyEvalClampPos)
	{
		EvaluateClampedMousePointer();
	}

	if (bContinuouslyEvalMouseIcon)
	{
		EvaluateMouseIconOverrides();
	}
}

void MousePointer::EvaluateMouseIconOverrides ()
{
	if (ContainerUtils::IsEmpty(MouseIconOverrideStack))
	{
		return;
	}

	sf::Event::MouseMoveEvent newMoveEvent;
	newMoveEvent.x = ReadCachedAbsPosition().X.ToINT().ToInt32();
	newMoveEvent.y = ReadCachedAbsPosition().Y.ToINT().ToInt32();

	while (!ContainerUtils::IsEmpty(MouseIconOverrideStack))
	{
		SDFunction<Texture*, MousePointer*, const sf::Event::MouseMoveEvent&> curOverride = MouseIconOverrideStack.at(MouseIconOverrideStack.size() - 1);
		if (!curOverride.IsBounded())
		{
			MouseIconOverrideStack.pop_back();
			continue;
		}

		Texture* overrideTexture = overrideTexture = curOverride(this, newMoveEvent);
		if (overrideTexture == nullptr)
		{
			MouseIconOverrideStack.pop_back();
			continue;
		}

		//Only update mouse sprite if the texture is different
		if (overrideTexture->GetTextureResource() != MouseIcon->GetSprite()->getTexture())
		{
			MouseIcon->SetSpriteTexture(overrideTexture);
			UpdateSpriteOffset();
		}

		return;
	}

	//At this point we're assuming the stack is empty
	CHECK(ContainerUtils::IsEmpty(MouseIconOverrideStack));

	//Restore default texture
	MouseIcon->SetSpriteTexture(DefaultIcon.Get());
	UpdateSpriteOffset();
}

void MousePointer::EvaluateClampedMousePointer ()
{
	if (ClampPositionsStack.size() > 0)
	{
		sf::Event::MouseMoveEvent newMoveEvent;
		Vector2 absCoordinates = ReadCachedAbsPosition();
		newMoveEvent.x = absCoordinates.X.ToINT().ToInt32();
		newMoveEvent.y = absCoordinates.Y.ToINT().ToInt32();

		SMouseLimit curLimit = ClampPositionsStack.at(ClampPositionsStack.size() - 1);

		if (!curLimit.LimitCallback.IsBounded() || !curLimit.LimitCallback(this, newMoveEvent))
		{
			//Remove the back element and try the next one
			ClampPositionsStack.erase(ClampPositionsStack.begin() + ClampPositionsStack.size() - 1);
			EvaluateClampedMousePointer(); //Recursively check the top of the stack until a callback returns true (or when there's nothing)

			if (ClampPositionsStack.size() <= 0)
			{
				//Remove clamped borders
				OS_ClampMousePointer(Rectangle<INT>());
			}
			else
			{
				Rectangle<INT> curLimitRect = curLimit.LimitRegion;
				//Set the curLimit relative to the OS's borders.
				AddWindowOffsetTo(curLimitRect);

				OS_ClampMousePointer(curLimitRect);
			}
		}
	}
}

void MousePointer::SetMouseVisibility (bool bVisible)
{
	ShowMouseValue = (bVisible) ? Utils::Max<INT>(ShowMouseValue - 1, 0) : ShowMouseValue + 1;

	if (MouseIcon != nullptr)
	{
		MouseIcon->SetVisibility(GetMouseVisibility());
	}
}

void MousePointer::SetLocked (bool bNewLocked)
{
	bLocked = bNewLocked;
}

void MousePointer::SetOwningWindowHandle (Window* newOwningWindowHandle)
{
	//Restore window's cursor visible state
	if (OwningWindowHandle != nullptr && OwningWindowHandle->GetResource() != nullptr)
	{
		//Restore OS Mouse Pointer if there are no other instantiated MousePointer classes
		OwningWindowHandle->GetResource()->setMouseCursorVisible(true);

		for (ObjectIterator iter(GetObjectHash()); iter.SelectedObject; iter++)
		{
			MousePointer* mouse = dynamic_cast<MousePointer*>(iter.SelectedObject.Get());
			if (mouse != nullptr && iter.SelectedObject.Get() != this)
			{
				//Although it's not likely, there's another mouse pointer instance that is also drawing a cursor
				OwningWindowHandle->GetResource()->setMouseCursorVisible(false);
				break;
			}
		}
	}

	OwningWindowHandle = newOwningWindowHandle;
	if (OwningWindowHandle != nullptr && OwningWindowHandle->GetResource() != nullptr)
	{
		OwningWindowHandle->GetResource()->setMouseCursorVisible(false);
	}
}

MousePointer* MousePointer::GetMousePointer ()
{
	InputEngineComponent* inputEngine = InputEngineComponent::Find();
	if (inputEngine != nullptr)
	{
		return inputEngine->GetMouse();
	}

	return nullptr;
}

void MousePointer::GetMousePosition (INT& outX, INT& outY) const
{
	const Vector2 absCoordinates = ReadCachedAbsPosition();
	outX = absCoordinates.X.ToINT();
	outY = absCoordinates.Y.ToINT();
}

void MousePointer::GetMousePosition (FLOAT& outX, FLOAT& outY) const
{
	const Vector2 absCoordinates = ReadCachedAbsPosition();
	outX = absCoordinates.X;
	outY = absCoordinates.Y;
}

Vector2 MousePointer::GetMousePosition () const
{
	return ReadCachedAbsPosition();
}

const Vector2& MousePointer::ReadMousePosition () const
{
	return ReadCachedAbsPosition();
}

bool MousePointer::GetMouseVisibility () const
{
	return (ShowMouseValue == 0);
}

bool MousePointer::GetLocked () const
{
	return bLocked;
}

INT MousePointer::GetClampSize () const
{
	return INT(ClampPositionsStack.size());
}

Texture* MousePointer::GetDefaultIcon () const
{
	return DefaultIcon.Get();
}

SpriteComponent* MousePointer::GetMouseIcon () const
{
	return MouseIcon.Get();
}

InputComponent* MousePointer::GetMouseInput () const
{
	return MouseInput.Get();
}

void MousePointer::AddWindowOffsetTo (Rectangle<INT>& outRectangle) const
{
	if (OwningWindowHandle == nullptr)
	{
		return;
	}

	INT windowPosX;
	INT windowPosY;
	OwningWindowHandle->GetWindowPosition(windowPosX, windowPosY);

	outRectangle.Left += windowPosX;
	outRectangle.Top += windowPosY;
	outRectangle.Right += windowPosX;
	outRectangle.Bottom += windowPosY;
}

void MousePointer::UpdateSpriteOffset ()
{
	SetPivot((MouseIcon->GetTextureSize() * 0.5f));
}

Vector2 MousePointer::GetDefaultMouseSize () const
{
	return Vector2(30.f, 50.f);
}

void MousePointer::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	SetMousePosition(sfmlEvent.x, sfmlEvent.y);
}
SD_END