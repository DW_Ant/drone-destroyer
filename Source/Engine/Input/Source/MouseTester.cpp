/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MouseTester.cpp
=====================================================================
*/

#include "InputClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, MouseTester, SD, Entity)

DPointer<Texture> MouseTester::Holding1Tx = nullptr;
DPointer<Texture> MouseTester::Holding2Tx = nullptr;
DPointer<Texture> MouseTester::Holding3Tx = nullptr;
DPointer<Texture> MouseTester::Holding4Tx = nullptr;

void MouseTester::InitProps ()
{
	Super::InitProps();

	bActive = true;
	bSettingLimitRegion = false;
	Input = nullptr;
	RegionRenderer = nullptr;

	bHolding1 = false;
	bHolding2 = false;
	bHolding3 = false;
	bHolding4 = false;
	bClampingMouse = false;
}

void MouseTester::BeginObject ()
{
	Super::BeginObject();

	Input = InputComponent::CreateObject();
	if (AddComponent(Input.Get()))
	{
		Input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, MouseTester, HandleCaptureInput, bool, const sf::Event&);
		Input->MouseMoveDelegate = SDFUNCTION_3PARAM(this, MouseTester, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
		Input->MouseClickDelegate = SDFUNCTION_3PARAM(this, MouseTester, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
	}

	RegionRenderer = MouseTesterRender::CreateObject();
	if (AddComponent(RegionRenderer.Get()))
	{
		if (!IsDefaultObject())
		{
			GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
			CHECK(graphicsEngine != nullptr)
			PlanarDrawLayer* canvasLayer = graphicsEngine->GetCanvasDrawLayer();
			CHECK(canvasLayer != nullptr)
			canvasLayer->RegisterSingleComponent(RegionRenderer.Get());
			SetDepth(25.f);
		}
	}
}

void MouseTester::PostEngineInitialize () const
{
	Super::PostEngineInitialize();

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	Holding1Tx = localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Tests"), TXT("CursorPointer1.png")), TXT("Interaface/Tests/CursorPointer1"));
	Holding2Tx = localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Tests"), TXT("CursorPointer2.png")), TXT("Interaface/Tests/CursorPointer2"));
	Holding3Tx = localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Tests"), TXT("CursorPointer3.png")), TXT("Interaface/Tests/CursorPointer3"));
	Holding4Tx = localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Tests"), TXT("CursorPointer4.png")), TXT("Interaface/Tests/CursorPointer4"));
}

void MouseTester::Destroyed ()
{
	MousePointer* pointer = InputEngineComponent::Find()->GetMouse();
	if (pointer != nullptr)
	{
		pointer->RemoveIconOverride(SDFUNCTION_2PARAM(this, MouseTester, HandleMouseIcon1, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
		pointer->RemoveIconOverride(SDFUNCTION_2PARAM(this, MouseTester, HandleMouseIcon2, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
		pointer->RemoveIconOverride(SDFUNCTION_2PARAM(this, MouseTester, HandleMouseIcon3, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
		pointer->RemoveIconOverride(SDFUNCTION_2PARAM(this, MouseTester, HandleMouseIcon4, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
		pointer->RemovePositionLimit(SDFUNCTION_2PARAM(this, MouseTester, HandlePushLimit, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
	}

	Super::Destroyed();
}

#define HOLD_NUM_SWITCH(num) \
if (bHolding##num && keyEvent.type == sf::Event::KeyReleased) \
{ \
	bHolding##num = false; \
	mouse->EvaluateMouseIconOverrides(); \
} \
else if (!bHolding##num && keyEvent.type == sf::Event::KeyPressed) \
{ \
	bHolding##num = true; \
	mouse->PushMouseIconOverride(Holding##num##Tx.Get(), SDFUNCTION_2PARAM(this, MouseTester, HandleMouseIcon##num##, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&)); \
} \
else {}

bool MouseTester::HandleCaptureInput (const sf::Event& keyEvent)
{
	if (!bActive)
	{
		return false;
	}

	MousePointer* mouse = InputEngineComponent::Find()->GetMouse();
	CHECK(mouse != nullptr)

	switch (keyEvent.key.code)
	{
		case(sf::Keyboard::Num1):
			HOLD_NUM_SWITCH(1);
			return true;

		case(sf::Keyboard::Num2):
			HOLD_NUM_SWITCH(2);
			return true;

		case(sf::Keyboard::Num3):
			HOLD_NUM_SWITCH(3);
			return true;

		case(sf::Keyboard::Num4):
			HOLD_NUM_SWITCH(4);
			return true;

		case(sf::Keyboard::Escape):
			if (keyEvent.type == sf::Event::KeyReleased)
			{
				UnitTester::TestLog(TestFlags, TXT("Escape released.  Ending Mouse Tester."));
				Destroy();
			}
			return true;
	}

	return false;
}
#undef HOLD_NUM_SWITCH

void MouseTester::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	if (bActive && bSettingLimitRegion && RegionRenderer != nullptr)
	{
		RegionRenderer->FlexiblePoint = Vector2(static_cast<float>(sfmlEvent.x), static_cast<float>(sfmlEvent.y));
	}
}

bool MouseTester::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (!bActive)
	{
		return false;
	}

	if (sfmlEvent.button == sf::Mouse::Button::Left)
	{
		bSettingLimitRegion = (eventType == sf::Event::MouseButtonPressed);
		if (RegionRenderer != nullptr && bSettingLimitRegion)
		{
			RegionRenderer->PivotPoint = Vector2(static_cast<float>(sfmlEvent.x), static_cast<float>(sfmlEvent.y));
			RegionRenderer->FlexiblePoint = RegionRenderer->PivotPoint;
		}

		if (eventType == sf::Event::MouseButtonReleased) //Apply limit
		{
			Rectangle<INT> limitRegion;
			limitRegion.Left = Utils::Min(RegionRenderer->PivotPoint.X, RegionRenderer->FlexiblePoint.X).ToINT();
			limitRegion.Top = Utils::Min(RegionRenderer->PivotPoint.Y, RegionRenderer->FlexiblePoint.Y).ToINT();
			limitRegion.Right = Utils::Max(RegionRenderer->PivotPoint.X, RegionRenderer->FlexiblePoint.X).ToINT();
			limitRegion.Bottom = Utils::Max(RegionRenderer->PivotPoint.Y, RegionRenderer->FlexiblePoint.Y).ToINT();

			InputEngineComponent::Find()->GetMouse()->PushPositionLimit(limitRegion, SDFUNCTION_2PARAM(this, MouseTester, HandlePushLimit, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
			bClampingMouse = true;
		}

		return true;
	}

	if (sfmlEvent.button == sf::Mouse::Button::Right)
	{
		if (eventType == sf::Event::MouseButtonReleased)
		{
			bClampingMouse = false;
			InputEngineComponent::Find()->GetMouse()->EvaluateClampedMousePointer();
			if (RegionRenderer != nullptr)
			{
				//Stop rendering rectangle
				RegionRenderer->PivotPoint = Vector2(0,0);
				RegionRenderer->FlexiblePoint = Vector2(0,0);
			}
		}

		return true;
	}

	return false;
}

Texture* MouseTester::HandleMouseIcon1 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	if (bHolding1)
	{
		return Holding1Tx.Get();
	}

	return nullptr;
}

Texture* MouseTester::HandleMouseIcon2 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	if (bHolding2)
	{
		return Holding2Tx.Get();
	}

	return nullptr;
}

Texture* MouseTester::HandleMouseIcon3 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	if (bHolding3)
	{
		return Holding3Tx.Get();
	}

	return nullptr;
}

Texture* MouseTester::HandleMouseIcon4 (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	if (bHolding4)
	{
		return Holding4Tx.Get();
	}

	return nullptr;
}

bool MouseTester::HandlePushLimit (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfEvent)
{
	return bClampingMouse;
}
SD_END

#endif