/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputEngineComponent.cpp
=====================================================================
*/

#include "InputClasses.h"

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(SD, InputEngineComponent)

InputEngineComponent::InputEngineComponent () : Super()
{
	MainBroadcaster = nullptr;
	Mouse = nullptr;
}

void InputEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	MainBroadcaster = InputBroadcaster::CreateObject();
	CHECK(MainBroadcaster != nullptr)

	Mouse = MousePointer::CreateObject();
	CHECK(Mouse != nullptr)

	GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
	CHECK(graphicsEngine != nullptr)
	PlanarDrawLayer* canvasLayer = graphicsEngine->GetCanvasDrawLayer();
	CHECK(canvasLayer != nullptr)
	canvasLayer->RegisterSingleComponent(Mouse->GetMouseIcon());
}

void InputEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)
	ImportMouseIcons(localTexturePool);

	const DString defaultIconName = TXT("Interface.MouseIcons.CursorPointer");
	Texture* defaultIcon = localTexturePool->FindTexture(defaultIconName);
	if (defaultIcon == nullptr)
	{
		Engine::FindEngine()->FatalError(TXT("Unable to import essential texture for InputEngineComponent: ") + defaultIconName);
		return;
	}

	Mouse->SetDefaultIcon(defaultIcon);
	GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
	if (localGraphicsEngine != nullptr)
	{
		Mouse->SetOwningWindowHandle(localGraphicsEngine->GetPrimaryWindow());
		MainBroadcaster->SetInputSource(localGraphicsEngine->GetPrimaryWindow());
	}
	else
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Local engine does not have a GraphicsEngineComponent.  Local InputEngineComponent will not be able to register mouse pointer to a window handle."));
	}

	MainBroadcaster->SetAssociatedMouse(Mouse.Get());
}

void InputEngineComponent::ShutdownComponent ()
{
	Super::ShutdownComponent();

	if (MainBroadcaster != nullptr)
	{
		MainBroadcaster->Destroy();
		MainBroadcaster = nullptr;
	}

	if (Mouse != nullptr)
	{
		Mouse->Destroy();
		Mouse = nullptr;
	}

	//Remove clamped mouse position limits
	OS_ClampMousePointer(Rectangle<INT>());
}

std::vector<const DClass*> InputEngineComponent::GetPreInitializeDependencies () const
{
	std::vector<const DClass*> dependencies = Super::GetPreInitializeDependencies();

	//GraphicsEngine must initialize first since the mouse pointer needs to register to canvas draw layer.
	dependencies.push_back(GraphicsEngineComponent::SStaticClass());

	return dependencies;
}

InputBroadcaster* InputEngineComponent::FindBroadcasterForWindow (Window* window) const
{
	for (InputBroadcaster* curBroadcaster : InputBroadcasters)
	{
		if (curBroadcaster->GetInputSource() == window)
		{
			return curBroadcaster;
		}
	}

	return nullptr;
}

void InputEngineComponent::RegisterInputBroadcaster (InputBroadcaster* newBroadcaster)
{
	InputBroadcasters.push_back(newBroadcaster);
}

void InputEngineComponent::UnregisterInputBroadcaster (InputBroadcaster* oldBroadcaster)
{
	for (UINT_TYPE i = 0; i < InputBroadcasters.size(); i++)
	{
		if (InputBroadcasters.at(i) == oldBroadcaster)
		{
			InputBroadcasters.erase(InputBroadcasters.begin() + i);
			return;
		}
	}
}

InputBroadcaster* InputEngineComponent::GetMainBroadcaster () const
{
	return MainBroadcaster.Get();
}

MousePointer* InputEngineComponent::GetMouse () const
{
	return Mouse.Get();
}

void InputEngineComponent::ImportMouseIcons (TexturePool* localTexturePool)
{
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("MouseIcons"), TXT("CursorPointer.png")), TXT("Interface.MouseIcons.CursorPointer"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("MouseIcons"), TXT("CursorPrecision.png")), TXT("Interface.MouseIcons.CursorPrecision"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("MouseIcons"), TXT("CursorResizeBottomLeft.png")), TXT("Interface.MouseIcons.CursorResizeBottomLeft"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("MouseIcons"), TXT("CursorResizeHorizontal.png")), TXT("Interface.MouseIcons.CursorResizeHorizontal"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("MouseIcons"), TXT("CursorResizeTopLeft.png")), TXT("Interface.MouseIcons.CursorResizeTopLeft"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("MouseIcons"), TXT("CursorResizeVertical.png")), TXT("Interface.MouseIcons.CursorResizeVertical"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("MouseIcons"), TXT("CursorScrollbarPan.png")), TXT("Interface.MouseIcons.CursorScrollbarPan"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("MouseIcons"), TXT("CursorText.png")), TXT("Interface.MouseIcons.CursorText"));
}
SD_END