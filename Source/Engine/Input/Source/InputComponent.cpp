/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputComponent.cpp
=====================================================================
*/

#include "InputClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, InputComponent, SD, EntityComponent)

void InputComponent::InitProps ()
{
	Super::InitProps();

	InputMessenger = nullptr;
	PendingInputMessenger = nullptr;
	bInputEnabled = true;
	InputPriority = 0;
}

void InputComponent::BeginObject ()
{
	Super::BeginObject();

	InputEngineComponent* localInputEngine = InputEngineComponent::Find();
	if (localInputEngine != nullptr && localInputEngine->GetMainBroadcaster() != nullptr)
	{
		//By default, this component will register to the main render window since most of the input components will be handled there
		localInputEngine->GetMainBroadcaster()->AddInputComponent(this);
	}
}

void InputComponent::Destroyed ()
{
	if (InputMessenger != nullptr)
	{
		InputMessenger->RemoveInputComponent(this);
		InputMessenger = nullptr;
	}

	Super::Destroyed();
}

bool InputComponent::CanReceiveInput () const
{
	return bInputEnabled;
}

bool InputComponent::CaptureInput (const sf::Event& keyEvent)
{
	if (CaptureInputDelegate.IsBounded())
	{
		return CaptureInputDelegate(keyEvent);
	}

	return false;
}

bool InputComponent::CaptureText (const sf::Event& keyEvent)
{
	if (CaptureTextDelegate.IsBounded())
	{
		return CaptureTextDelegate(keyEvent);
	}

	return false;
}

void InputComponent::MouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	if (MouseMoveDelegate.IsBounded())
	{
		MouseMoveDelegate(mouse, sfmlEvent, deltaMove);
	}
}

bool InputComponent::MouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (MouseClickDelegate.IsBounded())
	{
		return MouseClickDelegate(mouse, sfmlEvent, eventType);
	}

	return false;
}

bool InputComponent::MouseWheelScroll (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	if (MouseWheelScrollDelegate.IsBounded())
	{
		return MouseWheelScrollDelegate(mouse, sfmlEvent);
	}

	return false;
}

void InputComponent::SetInputEnabled (bool bNewInputEnabled)
{
	bInputEnabled = bNewInputEnabled;
}

void InputComponent::SetInputPriority (INT newPriority)
{
	InputBroadcaster* broadcaster = (PendingInputMessenger != nullptr) ? PendingInputMessenger.Get() : InputMessenger.Get();
	broadcaster->SetInputPriority(this, newPriority);
}

InputBroadcaster* InputComponent::GetInputMessenger () const
{
	return InputMessenger.Get();
}

INT InputComponent::GetInputPriority () const
{
	return InputPriority;
}
SD_END