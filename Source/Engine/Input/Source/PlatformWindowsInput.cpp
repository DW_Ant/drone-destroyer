/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlatformWindowsInput.cpp
=====================================================================
*/


#include "InputClasses.h"

#ifdef PLATFORM_WINDOWS

SD_BEGIN
void OS_SetMousePosition (const Vector2& mousePosition)
{
	if (!SetCursorPos(mousePosition.X.ToINT().ToInt32(), mousePosition.Y.ToINT().ToInt32()))
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Unable to set the cursor position to %s.  Error code:  %s"), mousePosition, DString::MakeString(GetLastError()));
	}
}

Vector2 OS_GetMousePosition ()
{
	POINT mousePos;
	if (!GetCursorPos(OUT &mousePos))
	{
		InputLog.Log(LogCategory::LL_Warning, TXT("Unable to get the cursor position.  Error code:  %s"), DString::MakeString(GetLastError()));
		return Vector2::ZeroVector;
	}

	return Vector2(FLOAT::MakeFloat(mousePos.x), FLOAT::MakeFloat(mousePos.y));
}

void OS_ClampMousePointer (const Rectangle<INT>& region)
{
	if (region.IsEmpty())
	{
		ClipCursor(NULL);
		return;
	}

	RECT borders;
	borders.left = static_cast<LONG>(region.Left.Value);
	borders.top = static_cast<LONG>(region.Top.Value);
	borders.right = static_cast<LONG>(region.Right.Value);
	borders.bottom = static_cast<LONG>(region.Bottom.Value);

	ClipCursor(&borders);
}
SD_END

#endif