/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MouseTesterRender.cpp
=====================================================================
*/

#include "InputClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, MouseTesterRender, SD, RenderComponent)

void MouseTesterRender::InitProps ()
{
	Super::InitProps();

	BorderThickness = 4.f;
}

void MouseTesterRender::Render (RenderTarget* renderTarget, const Camera* camera)
{
	CHECK(renderTarget != nullptr)
	if (PivotPoint.IsEmpty() && FlexiblePoint.IsEmpty())
	{
		return;
	}

	Vector2 topLeft;
	topLeft.X = Utils::Min(PivotPoint.X, FlexiblePoint.X);
	topLeft.Y = Utils::Min(PivotPoint.Y, FlexiblePoint.Y);

	Vector2 bottomRight;
	bottomRight.X = Utils::Max(PivotPoint.X, FlexiblePoint.X);
	bottomRight.Y = Utils::Max(PivotPoint.Y, FlexiblePoint.Y);

	sf::RectangleShape outline;
	outline.setSize(Vector2::SDtoSFML(Vector2(bottomRight.X - topLeft.X, bottomRight.Y - topLeft.Y)));
	outline.setPosition(Vector2::SDtoSFML(topLeft));
	outline.setOutlineThickness(BorderThickness);
	outline.setOutlineColor(sf::Color::Red);
	outline.setFillColor(sf::Color::Transparent);
	renderTarget->Draw(outline);
}
SD_END

#endif