/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlatformMacInput.h
  Includes any Windows-specific libraries and definitions for the Input module.
=====================================================================
*/

#pragma once

#include "Input.h"

#ifdef PLATFORM_MAC
SD_BEGIN


/*
=====================
  Methods
=====================
*/

/**
 * Sets the mouse coordinates relative to the top left corner of the desktop.
 */
void INPUT_API OS_SetMousePosition (const Vector2& mousePosition);

/**
 * Retrieves the OS mouse position relative to the top left corner of the desktop.
 */
Vector2 INPUT_API OS_GetMousePosition ();

/**
 * Clamps the operating system's mouse pointer to be within a rectangle.
 * If everything is 0, then no clamps are applied.
 * The Region assumes X-right and Y-down coordinate system.
 */
void INPUT_API OS_ClampMousePointer (const Rectangle<INT>& region);
SD_END

#endif //PLATFORM_MAC