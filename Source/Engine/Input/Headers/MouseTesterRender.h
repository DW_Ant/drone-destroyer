/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MouseTesterRender.h
  Specialized render component used to draw a rectangle representing
  the limit region the MouseTester establishes.
=====================================================================
*/

#pragma once

#include "Input.h"

#ifdef DEBUG_MODE
SD_BEGIN
class INPUT_API MouseTesterRender : public RenderComponent
{
	DECLARE_CLASS(MouseTesterRender)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Rectangle corner coordinates relative to the upper left corner of window.  This corner is where the tester pressed down on. */
	Vector2 PivotPoint;

	/* Rectangle corner coordinates relative to the upper left corner of window.  This corner is the opposite corner of the pivot. */
	Vector2 FlexiblePoint;

	float BorderThickness;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
};
SD_END

#endif