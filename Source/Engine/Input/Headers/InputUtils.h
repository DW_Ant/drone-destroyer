/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputUtils.h
  Contains miscellaneous static utility functions for the Input module.
=====================================================================
*/

#pragma once

#include "Input.h"

SD_BEGIN
class MousePointer;
class InputBroadcaster;

class INPUT_API InputUtils : public BaseUtils
{
	DECLARE_CLASS(InputUtils)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Convient function that sets up a InputBroadcaster and a MousePointer to the specified window handle.
	 *
	 * @param window The window handle that needs a InputBroadcaster and a MousePointer associated with it.
	 * @param input Registers this input to the specified window.
	 * @param mouse Mouse object that'll render itself to the specified window, and link it with the input broadcaster.
	 */
	static void SetupInputForWindow (Window* window, InputBroadcaster* input, MousePointer* mouse);
};
SD_END