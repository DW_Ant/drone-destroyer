/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Input.h
  Contains important file includes and definitions for the Input module.

  The input module contains classes such as InputComponents and InputBroadcaster.
  The main broadcaster and default behavior for InputComponents will react to callbacks
  from the main application window; they could be listening to events from other windows.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"

#if !(MODULE_CORE)
#error The Input module requires the Core module.  Please enable INCLUDE_CORE in Configuration.h.
#endif

#if !(MODULE_GRAPHICS)
#error The Input module requires the Graphics module.
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef INPUT_EXPORT
		#define INPUT_API __declspec(dllexport)
	#else
		#define INPUT_API __declspec(dllimport)
	#endif
#else
	#define INPUT_API
#endif

//Platform-specific includes
#ifdef PLATFORM_WINDOWS
#include "PlatformWindowsInput.h"
#elif defined(PLATFORM_MAC)
#include "PlatformMacInput.h"
#endif

SD_BEGIN
extern LogCategory INPUT_API InputLog;
SD_END