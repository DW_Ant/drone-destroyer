/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Font.cpp
=====================================================================
*/

#include "SfmlGraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, Font, SD, Object)

const INT Font::NUM_SPACES_PER_TAB = 4;
const Directory Font::FONT_DIRECTORY(Directory::BASE_DIRECTORY / TXT("Fonts"));

void Font::InitProps ()
{
	Super::InitProps();

	FontName = TXT("UnknownFont");
	FontResource = nullptr;
}

DString Font::GetFriendlyName () const
{
	if (!FontName.IsEmpty())
	{
		return TXT("Font:  ") + FontName;
	}

	return Super::GetFriendlyName();
}

void Font::Destroyed ()
{
	Release();

	Super::Destroyed();
}

void Font::Release ()
{
	if (FontResource != nullptr)
	{
		delete FontResource;
	}

	FontResource = nullptr;
}

void Font::SetResource (sf::Font* newResource)
{
	FontResource = newResource;
}

FLOAT Font::GetCharacterWidth (const DString& character, INT charSize) const
{
	if (FontResource == nullptr)
	{
		SfmlGraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to calculate character width for \"%s\" since a font resource is not yet assigned to %s"), DString(character), ToString());
		return 0.f;
	}

	CHECK(character.Length() == 1)
	sf::Glyph glyph = FontResource->getGlyph(character.ToUTF32().at(0), charSize.ToUnsignedInt32(), false);

	if (character == '\t')
	{
		return FLOAT(glyph.advance * FLOAT(NUM_SPACES_PER_TAB));
	}
	else if (character == '\n')
	{
		return 0.f;
	}

	return FLOAT(glyph.advance);
}

FLOAT Font::CalculateStringWidth (const DString& line, INT charSize) const
{
	return CalculateStringWidth(line, charSize, 0);
}

FLOAT Font::CalculateStringWidth (const DString& line, INT charSize, INT startingIdx, INT numChars) const
{
	if (numChars < 0)
	{
		numChars = line.Length() - startingIdx;
	}

	CHECK(numChars + startingIdx <= line.Length())
	FLOAT result = 0.f;
	StringIterator iter(&line);
	for (INT i = 0; i < startingIdx; i++)
	{
		++iter;
	}

	for (INT i = 0; i < numChars; i++)
	{
		result += GetCharacterWidth(*iter, charSize);
		++iter;
	}

	return result;
}

INT Font::FindCharNearWidthLimit (const DString& line, INT charSize, FLOAT maxWidth) const
{
	FLOAT curWidth = 0.f;

	INT charIdx = 0;
	for (StringIterator iter(&line); !iter.IsAtEnd(); ++iter, charIdx++)
	{
		FLOAT newWidth = GetCharacterWidth(*iter, charSize);
		if (curWidth + newWidth >= maxWidth)
		{
			return (charIdx - 1); //return previous character that fits within bounds.
		}

		curWidth += newWidth;
	}

	return line.Length() - 1; //The whole string fits, return the last character.
}

DString Font::GetFontName () const
{
	return FontName;
}

const sf::Font* Font::GetFontResource () const
{
	return FontResource;
}
SD_END