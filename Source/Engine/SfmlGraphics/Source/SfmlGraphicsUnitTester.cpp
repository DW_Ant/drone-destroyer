/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SfmlGraphicsUnitTester.cpp
=====================================================================
*/

#include "SfmlGraphicsClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, SfmlGraphicsUnitTester, SD, UnitTester)

bool SfmlGraphicsUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		return TestColor(testFlags) && TestSfVector(testFlags);
	}

	return true;
}

bool SfmlGraphicsUnitTester::TestColor (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Color"));
	SetTestCategory(testFlags, TXT("Constructors"));
	Color red(255, 0, 0, 255);
	Color blue(0, 0, 255, 255);
	Color blueCopy(blue);
	uint32 greenBits = (255 << 16);
	greenBits += 255; //Add alpha
	Color green(greenBits);
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Comparison"));
	{
		if (red == blue)
		{
			UnitTestError(testFlags, TXT("The color red (%s) should not be equal to blue (%s)."), {red.ToString(), blue.ToString()});
			return false;
		}

		if (blue != blueCopy)
		{
			UnitTestError(testFlags, TXT("The blue color (%s) should be equal to its copy constructed blue (%s)."), {blue.ToString(), blueCopy.ToString()});
			return false;
		}

		if (green.Source.r != 0 || green.Source.g != 255 || green.Source.b != 0 || green.Source.a != 255)
		{
			UnitTestError(testFlags, TXT("The green color constructed from this integer (%s) should have the RGBA channels at 0,255,0,255.  Instead it's %s"), {DString::MakeString(greenBits), green.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Streaming"));
	{
		Color white(255, 255, 255, 255);
		Color greyish(120, 131, 127, 200);
		Color black(0, 0, 0, 215);
		Color readWhite;
		Color readGreyish;
		Color readBlack;
		DataBuffer colorBuffer;

		colorBuffer << white;
		colorBuffer << greyish;
		colorBuffer << black;
		colorBuffer >> readWhite;
		colorBuffer >> readGreyish;
		colorBuffer >> readBlack;

		if (white != readWhite)
		{
			UnitTestError(testFlags, TXT("Color streaming test failed.  After pushing white (%s) to data buffer, it read %s from that data buffer."), {white.ToString(), readWhite.ToString()});
			return false;
		}

		if (greyish != readGreyish)
		{
			UnitTestError(testFlags, TXT("Color streaming test failed.  After pushing greyish (%s) to data buffer, it read %s from that data buffer."), {greyish.ToString(), readGreyish.ToString()});
			return false;
		}

		if (black != readBlack)
		{
			UnitTestError(testFlags, TXT("Color streaming test failed.  After pushing black (%s) to data buffer, it read %s from that data buffer."), {black.ToString(), readBlack.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Utilities"));
	{
		Color white = sf::Color::White;
		Color black = sf::Color::Black;
		Color grey(127, 127, 127, 255);
		UnitTester::TestLog(testFlags, TXT("Testing color lerping."));
		Color lerpedColor = Color::Lerp(0.f, black, white);
		if (lerpedColor != black)
		{
			UnitTestError(testFlags, TXT("Color lerping test failed.  Lerping 0 between %s and %s should have resulted in %s.  Instead it returned %s."), {black.ToString(), white.ToString(), black.ToString(), lerpedColor.ToString()});
			return false;
		}

		lerpedColor = Color::Lerp(1.f, black, white);
		if (lerpedColor != white)
		{
			UnitTestError(testFlags, TXT("Color lerping test failed.  Lerping 1 between %s and %s should have resulted in %s.  Instead it returned %s."), {black.ToString(), white.ToString(), white.ToString(), lerpedColor.ToString()});
			return false;
		}

		lerpedColor = Color::Lerp(0.5f, black, white);
		if (lerpedColor != grey)
		{
			UnitTestError(testFlags, TXT("Color lerping test failed.  Lerping 0.5 between %s and %s should have resulted in %s.  Instead it returned %s."), {black.ToString(), white.ToString(), grey.ToString(), lerpedColor.ToString()});
			return false;
		}

		Color expectedColor(191, 191, 191, 255);
		lerpedColor = Color::Lerp(0.75f, black, white);
		if (lerpedColor != expectedColor)
		{
			UnitTestError(testFlags, TXT("Color lerping test failed.  Lerping 0.75 between %s and %s should have resulted in %s.  Instead it returned %s."), {black.ToString(), white.ToString(), expectedColor.ToString(), lerpedColor.ToString()});
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	ExecuteSuccessSequence(testFlags, TXT("Color"));

	return true;
}

bool SfmlGraphicsUnitTester::TestSfVector (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("SFML Vector"));

	const std::function<DString(const sf::Vector2f&)> vecToString([](const sf::Vector2f& v)
	{
		return (TXT("(") + FLOAT(v.x).ToString() + TXT(", ") + FLOAT(v.y).ToString() + TXT(")"));
	});

	SetTestCategory(testFlags, TXT("Math"));
	{
		sf::Vector2f vecA(2.f, 4.f);
		sf::Vector2f vecB(-6.f, 8.f);

		sf::Vector2f result = vecA * vecB;
		sf::Vector2f expected(-12.f, 32.f);
		if (result != expected)
		{
			UnitTestError(testFlags, TXT("SFML Vector unit test failed. Multiplying %s with %s should have resulted in %s. Instead it's %s."), {vecToString(vecA), vecToString(vecB), vecToString(expected), vecToString(result)});
			return false;
		}

		result *= vecB;
		expected = sf::Vector2f(72.f, 256.f);
		if (result != expected)
		{
			UnitTestError(testFlags, TXT("SFML Vector unit test failed. Vector multiplication should have resulted in %s instead of %s."), {vecToString(expected), vecToString(result)});
			return false;
		}

		result /= vecB;
		expected = sf::Vector2f(-12.f, 32.f);
		if (result != expected)
		{
			UnitTestError(testFlags, TXT("SFML Vector unit test failed. Vector division should have resulted in %s instead of %s."), {vecToString(expected), vecToString(result)});
			return false;
		}

		result = vecB / vecA;
		expected = sf::Vector2f(-3.f, 2.f);
		if (result != expected)
		{
			UnitTestError(testFlags, TXT("SFML Vector unit test failed. Dividing %s with %s should have resulted in %s. Instead it's %s."), {vecToString(vecB), vecToString(vecA), vecToString(expected), vecToString(result)});
			return false;
		}
	}
	CompleteTestCategory(testFlags);
	
	ExecuteSuccessSequence(testFlags, TXT("SFML Vector"));
	return true;
}

SD_END
#endif