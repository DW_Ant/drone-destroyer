/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Color.cpp
=====================================================================
*/

#include "SfmlGraphicsClasses.h"

SD_BEGIN
const Color Color::WHITE(255, 255, 255, 255);
const Color Color::BLACK(0, 0, 0, 255);
const Color Color::RED(255, 0, 0, 255);
const Color Color::YELLOW(255, 255, 0, 255);
const Color Color::GREEN(0, 255, 0, 255);
const Color Color::CYAN(0, 255, 255, 255);
const Color Color::BLUE(0, 0, 255, 255);
const Color Color::MAGENTA(255, 0, 255, 255);
const Color Color::INVISIBLE(0, 0, 0, 0);

Color::Color ()
{
	Source = sf::Color();
}

Color::Color (const Color& otherColor)
{
	Source = otherColor.Source;
}

Color::Color (const sf::Color& otherColor)
{
	Source = otherColor;
}

Color::Color (uint8 red, uint8 green, uint8 blue, uint8 alpha)
{
	sf::Uint8 sfRed = red;
	sf::Uint8 sfGreen = green;
	sf::Uint8 sfBlue = blue;
	sf::Uint8 sfAlpha = alpha;

	Source = sf::Color(sfRed, sfGreen, sfBlue, sfAlpha);
}

Color::Color (uint32 colors)
{
	Source = sf::Color(colors);
}

DString Color::ToString () const
{
	return DString::CreateFormattedString(TXT("[%s,%s,%s,%s]"), INT(Source.r), INT(Source.g), INT(Source.b), INT(Source.a));
}

void Color::Serialize (DataBuffer& dataBuffer) const
{
	//Push an unsigned int32 from data buffer instead of an INT to avoid signed/unsigned mismatch with conversion.
	const int numBytes = sizeof(uint32);
	char charArray[numBytes + 1]; //include null terminator
	uint32 sourceNum = Source.toInteger();
	memcpy(charArray, &sourceNum, numBytes);

	//Reverse bytes if needed
	if (dataBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(charArray, numBytes);
	}

	dataBuffer.WriteBytes(charArray, numBytes);
}

void Color::Deserialize (const DataBuffer& dataBuffer)
{
	//Pull an unsigned int32 from data buffer instead of an INT to avoid signed/unsigned mismatch with conversion.
	const int numBytes = sizeof(uint32);
	char rawData[numBytes];
	dataBuffer.ReadBytes(rawData, numBytes);

	//Reverse bytes if needed
	if (dataBuffer.IsDataBufferLittleEndian() != DataBuffer::IsSystemLittleEndian())
	{
		DataBuffer::SwapByteOrder(rawData, numBytes);
	}

	uint32 allChannels;
	memcpy(&allChannels, rawData, numBytes);

	Source = sf::Color(allChannels);
}

Color Color::Lerp (FLOAT ratio, Color min, Color max)
{
	INT r = ((ratio * static_cast<float>((max.Source.r - min.Source.r))) + min.Source.r).ToINT();
	INT g = ((ratio * static_cast<float>((max.Source.g - min.Source.g))) + min.Source.g).ToINT();
	INT b = ((ratio * static_cast<float>((max.Source.b - min.Source.b))) + min.Source.b).ToINT();
	INT a = ((ratio * static_cast<float>((max.Source.a - min.Source.a))) + min.Source.a).ToINT();
	
	r = Utils::Clamp<INT>(r, 0, 255);
	g = Utils::Clamp<INT>(g, 0, 255);
	b = Utils::Clamp<INT>(b, 0, 255);
	a = Utils::Clamp<INT>(a, 0, 255);

	return Color(static_cast<sf::Uint8>(r.Value), static_cast<sf::Uint8>(g.Value), static_cast<sf::Uint8>(b.Value), static_cast<sf::Uint8>(a.Value));
}

bool operator== (const Color &left, const Color &right)
{
	return left.Source == right.Source;
}
 
bool operator!= (const Color &left, const Color &right)
{
	return left.Source != right.Source;
}

Color operator+ (const Color &left, const Color &right)
{
	return Color(left.Source + right.Source);
}

Color operator- (const Color &left, const Color &right)
{
	return Color(left.Source - right.Source);
}

Color operator* (const Color &left, const Color &right)
{
	return Color(left.Source * right.Source);
}
 
Color& operator+= (Color &left, const Color &right)
{
	left.Source += right.Source;
	return left;
}

Color& operator-= (Color &left, const Color &right)
{
	left.Source -= right.Source;
	return left;
}
 
Color& operator*= (Color &left, const Color &right)
{
	left.Source *= right.Source;
	return left;
}
SD_END