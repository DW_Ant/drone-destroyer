/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SfmlOutputStream.cpp
=====================================================================
*/

#include "SfmlOutputStream.h"

SD_BEGIN
bool SfmlOutputStream::OutputActive = false;

SfmlOutputStream::SfmlOutputStream ()
{
	CHECK_INFO(!OutputActive, TXT("SFML's output is already redirected by another instance of SfmlOutputStream. Ensure the previous instance is destroyed before creating a new one."))

	OriginalSfmlBuffer = sf::err().rdbuf();
	sf::err().rdbuf(SfmlOutput.rdbuf());
	OutputActive = true;
}

SfmlOutputStream::~SfmlOutputStream ()
{
	sf::err().rdbuf(OriginalSfmlBuffer); //restore sfml output stream
	OutputActive = false;
}

DString SfmlOutputStream::ReadOutput () const
{
	return DString(SfmlOutput.str());
}
SD_END