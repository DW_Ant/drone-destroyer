/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Texture.cpp
=====================================================================
*/

#include "SfmlGraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, Texture, SD, Object)

const Directory Texture::TEXTURE_DIRECTORY(Directory::BASE_DIRECTORY / TXT("Images"));

void Texture::InitProps ()
{
	Super::InitProps();

	Width = 0;
	Height = 0;
	TextureName = TXT("UnknownTexture");
	TextureResource = nullptr;
}

DString Texture::GetFriendlyName () const
{
	if (!TextureName.IsEmpty())
	{
		return TXT("Texture:  ") + TextureName;
	}

	return Super::GetFriendlyName();
}

void Texture::Destroyed ()
{
	Release();

	Super::Destroyed();
}

void Texture::Release ()
{
	if (TextureResource != nullptr)
	{
		delete TextureResource;
		TextureResource = nullptr;

		Width = 0;
		Height = 0;
		TextureName = TXT("Unknown Texture");
	}
}

bool Texture::SetTexelData (const std::vector<sf::Uint8>& newTexelData)
{
	if (newTexelData.size() != Width * Height * 4)
	{
		SfmlGraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to set texel data since the length of the texel data does not match the size of the texture (%sx%s).  Length of texel data is %s. Expected size is %s"), Width, Height, INT(newTexelData.size()), INT(newTexelData.size() * 4));
		return false;
	}

	sf::Uint8* textureData = new (std::nothrow) sf::Uint8[Width.Value * Height.Value * 4];
	if (textureData == nullptr)
	{
		SfmlGraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to allocate memory to update pixel data for %s."), TextureName);
		return false;
	}

	// Copy pixelData to textureData
	for (size_t i = 0; i < newTexelData.size(); i++)
	{
		textureData[i] = newTexelData.at(i);
	}

	TextureResource->update(textureData);

	delete[] textureData;
	return true;
}

void Texture::SetSmooth (bool bSmooth)
{
	TextureResource->setSmooth(bSmooth);
}

void Texture::SetRepeat (bool bRepeating)
{
	TextureResource->setRepeated(bRepeating);
}

void Texture::SetResource (sf::Texture* newResource)
{
	if (TextureResource != nullptr)
	{
		Release();
	}

	TextureResource = newResource;
	if (TextureResource != nullptr)
	{
		sf::Vector2u dimensions = TextureResource->getSize();
		Width = dimensions.x;
		Height = dimensions.y;
	}
}

sf::Texture* Texture::EditTextureResource ()
{
	return TextureResource;
}

void Texture::GetDimensions (INT &outWidth, INT &outHeight) const
{
	outWidth = Width;
	outHeight = Height;
}

void Texture::GetDimensions (Vector2& outDimensions) const
{
	outDimensions.X = Width.ToFLOAT();
	outDimensions.Y = Height.ToFLOAT();
}

Vector2 Texture::GetDimensions () const
{
	return Vector2(Width.ToFLOAT(), Height.ToFLOAT());
}

INT Texture::GetWidth () const
{
	return Width;
}

INT Texture::GetHeight () const
{
	return Height;
}

DString Texture::GetTextureName () const
{
	return TextureName;
}

const sf::Texture* Texture::GetTextureResource () const
{
	return TextureResource;
}

bool Texture::GetImage (sf::Image& outImage) const
{
	if (TextureResource == nullptr)
	{
		return false;
	}

	outImage = TextureResource->copyToImage();
	return true;
}
SD_END