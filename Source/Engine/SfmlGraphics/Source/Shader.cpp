/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Shader.cpp
=====================================================================
*/

#include "SfmlGraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, Shader, SD, Object)

const Directory Shader::SHADER_DIRECTORY(Directory::BASE_DIRECTORY / TXT("Shaders"));

void Shader::InitProps ()
{
	Super::InitProps();

	SFShader = nullptr;
	ShaderName = DString::EmptyString;
}

DString Shader::GetFriendlyName () const
{
	if (!ShaderName.IsEmpty())
	{
		return TXT("Shader:  ") + ShaderName;
	}

	return Super::GetFriendlyName();
}

void Shader::Destroyed ()
{
	if (SFShader != nullptr)
	{
		delete SFShader;
		SFShader = nullptr;
	}

	Super::Destroyed();
}

Shader* Shader::CreateShader (DString fileName, sf::Shader::Type shaderType)
{
	fileName = SHADER_DIRECTORY.ReadDirectoryPath() + fileName;

	sf::Shader* newShader = new sf::Shader();
	{
		SfmlOutputStream sfOutput;
		if (!newShader->loadFromFile(fileName.ToCString(), shaderType))
		{
			SfmlGraphicsLog.Log(LogCategory::LL_Warning, TXT("There was an error in loading shader %s:  %s"), fileName, sfOutput.ReadOutput());
			delete newShader;
			return nullptr;
		}
	}

	Shader* result = Shader::CreateObject();
	CHECK(result != nullptr)

	result->SetSFShader(newShader);
	result->ShaderName = fileName;
	return result;
}

Shader* Shader::CreateShader (DString vertexFileName, DString fragmentFileName)
{
	vertexFileName = SHADER_DIRECTORY.ReadDirectoryPath() + vertexFileName;
	fragmentFileName = SHADER_DIRECTORY.ReadDirectoryPath() + fragmentFileName;

	sf::Shader* newShader = new sf::Shader();
	{
		SfmlOutputStream sfmlOutput;
		if (!newShader->loadFromFile(vertexFileName.ToCString(), fragmentFileName.ToCString()))
		{
			SfmlGraphicsLog.Log(LogCategory::LL_Warning, TXT("Error in loading shader(s) %s-%s:  %s"), vertexFileName, fragmentFileName, sfmlOutput.ReadOutput());
			delete newShader;
			return false;
		}
	}

	Shader* result = Shader::CreateObject();
	CHECK(result != nullptr)

	result->SetSFShader(newShader);
	return result;
}

void Shader::SetSFShader (sf::Shader* newSFShader)
{
	SFShader = newSFShader;
}
SD_END