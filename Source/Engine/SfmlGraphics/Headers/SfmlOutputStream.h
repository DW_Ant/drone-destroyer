/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SfmlOutputStream.h
  A scoped object that'll redirect SFML's output stream to a local stream that then can
  be used to read from.

  It'll then restore SFML's stream after destruction. Only one instance of SfmlOutputStream can
  exist at the same time.
=====================================================================
*/

#pragma once

#include "SFMLGraphics.h"

SD_BEGIN
class SD_SFML_GRAPHICS_API SfmlOutputStream
{

	/*
	=====================
	  Properties
	=====================
	*/

protected:
	std::ostringstream SfmlOutput;
	std::streambuf* OriginalSfmlBuffer;

private:
	static bool OutputActive;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SfmlOutputStream ();
	virtual ~SfmlOutputStream ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual DString ReadOutput () const;
};

SD_END