/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Shader.h
  A wrapper to the SFML's shader object where this will contain a handle to a shader resource
  that will be implementing either a vertex or fragment shader.

  NOTE:  Shaders do not use ResourcePools like textures and fonts since recycled shaders may have influences
  on other shared shaders.  For example, a shader with a texture uniform may be set based on the sprite component it's associated with, but
  associating a specific texture to a shader should not affect other shaders that's running the same shader script.
=====================================================================
*/

#pragma once

#include "SfmlGraphics.h"

#define FRAGMENT_SHADER_EXTENSION TXT("frag")
#define VERTEX_SHADER_EXTENSION TXT("vert")

//Sand-Dune specific shader uniform names
#define SHADER_BASE_TEXTURE TXT("SDBaseTexture")
#define SHADER_GLOBAL_TIME TXT("SDGlobalTime")
#define SHADER_DELTA_TIME TXT("SDDeltaTime")

SD_BEGIN
class SD_SFML_GRAPHICS_API Shader : public Object
{
	DECLARE_CLASS(Shader)
	

	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const Directory SHADER_DIRECTORY;

	sf::Shader* SFShader;

protected:
	/* Name of the file handle that's implementing the shader. */
	DString ShaderName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual DString GetFriendlyName () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Opens the specified file, and attempts to create and initialize a shader with that file.
	 * The path to the file is relative to the folder matching the SHADER_LOCATION macro value.
	 * Returns a pointer of the created shader.  Returns null if it's unsuccessful or the file is not found.
	 */
	static Shader* CreateShader (DString fileName, sf::Shader::Type shaderType);
	static Shader* CreateShader (DString vertexFileName, DString fragmentFileName);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void SetSFShader (sf::Shader* newSFShader);
};
SD_END