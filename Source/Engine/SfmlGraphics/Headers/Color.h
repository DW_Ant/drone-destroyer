/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Color.h
  An object representing a color for the SFML's color object.  This
  object is treated as a datatype.
=====================================================================
*/

#pragma once

#include "SfmlGraphics.h"

SD_BEGIN
class SD_SFML_GRAPHICS_API Color : public DProperty
{
	

	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Common colors */
	static const Color WHITE;
	static const Color BLACK;
	static const Color RED;
	static const Color YELLOW;
	static const Color GREEN;
	static const Color CYAN;
	static const Color BLUE;
	static const Color MAGENTA;
	static const Color INVISIBLE;

	/* The color object actually implementing this datatype. */
	sf::Color Source;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	Color ();
	Color (const Color& otherColor);
	Color (const sf::Color& otherColor);
	Color (uint8 red, uint8 green, uint8 blue, uint8 alpha = 255);

	/**
	 * Constructor that creates a color from a single 32-bit int.
	 * The INT's bits are divided into 4 uint8s where each segment represents a color component.
	 * The leading bits define the Red color, followed by the Green, Blue, and Alpha channels.
	 */
	Color (uint32 colors);


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual DString ToString () const override;
	virtual void Serialize (DataBuffer& dataBuffer) const override;
	virtual void Deserialize (const DataBuffer& dataBuffer) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Linear interpolation between min and max based on the given ratio.
	 * Ratio ranges from [0,1] to return a value between min and max.  0 being equal to min, and 1 being equal to max.
	 */
	static Color Lerp (FLOAT ratio, Color min, Color max);
};

#pragma region "Operators"
//Same as sf::Color operators.  Please refer to SFML documentation to see what these operators do
//http://www.sfml-dev.org/documentation/2.3.2/classsf_1_1Color.php
SD_SFML_GRAPHICS_API bool operator== (const Color &left, const Color &right);
SD_SFML_GRAPHICS_API bool operator!= (const Color &left, const Color &right);
SD_SFML_GRAPHICS_API Color operator+ (const Color &left, const Color &right);
SD_SFML_GRAPHICS_API Color operator- (const Color &left, const Color &right);
SD_SFML_GRAPHICS_API Color operator* (const Color &left, const Color &right);
SD_SFML_GRAPHICS_API Color& operator+= (Color &left, const Color &right);
SD_SFML_GRAPHICS_API Color& operator-= (Color &left, const Color &right);
SD_SFML_GRAPHICS_API Color& operator*= (Color &left, const Color &right);

#pragma endregion
SD_END