/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LibGraphicsUtils.h
  An object containing utilities related to the graphics library.
  Utility functions include translating SD data types to SFML data types.
=====================================================================
*/

#pragma once

#include "SFMLGraphics.h"

SD_BEGIN
class SD_SFML_GRAPHICS_API LibGraphicsUtils : public BaseUtils
{
	DECLARE_CLASS(LibGraphicsUtils)
};


#pragma region "External Operators"
template <typename T>
sf::Vector2<T> operator* (const sf::Vector2<T>& left, const sf::Vector2<T>& right)
{
	return sf::Vector2<T>(left.x * right.x, left.y * right.y);
}

template <typename T>
sf::Vector2<T>& operator*= (sf::Vector2<T>& left, const sf::Vector2<T>& right)
{
	left.x *= right.x;
	left.y *= right.y;
	return left;
}

template <typename T>
sf::Vector2<T> operator/ (const sf::Vector2<T>& left, const sf::Vector2<T>& right)
{
	CHECK(right.x != 0.f && right.y != 0.f)
	if (right.x == 0.f || right.y == 0.f)
	{
		SfmlGraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to divide by zero! Dividing sf::Vector (%s,%s) by (%s,%s)"), FLOAT(left.x), FLOAT(left.y), FLOAT(right.x), FLOAT(right.y));
		return left;
	}

	return sf::Vector2<T>(left.x / right.x, left.y / right.y);
}

template <typename T>
sf::Vector2<T>& operator/= (sf::Vector2<T>& left, const sf::Vector2<T>& right)
{
	CHECK(right.x != 0.f && right.y != 0.f)
	if (right.x == 0.f || right.y == 0.f)
	{
		SfmlGraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to divide by zero! Dividing sf::Vector (%s,%s) by (%s,%s)"), FLOAT(left.x), FLOAT(left.y), FLOAT(right.x), FLOAT(right.y));
		return left;
	}

	left.x /= right.x;
	left.y /= right.y;
	return left;
}
#pragma endregion
SD_END