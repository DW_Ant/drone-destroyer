/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SfmlGraphics.h
  Contains important file includes and definitions for the SFML Graphics module.

  The SFMLGraphics module is a wrapper to the SFML Graphics module.
  This contains wrappers for various graphics-related objects such as
  textures and fonts, and conversion utilities (such as SDVector to SFVector).
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"

#if !(MODULE_CORE)
#error The SFML Graphics module requires the Core module.
#endif

#if !(MODULE_FILE)
#error The SFML Graphics module requires the File module.
#endif

#include <SFML/Graphics.hpp>

#ifdef PLATFORM_WINDOWS
	#ifdef SFML_GRAPHICS_EXPORT
		/* Added SD_ prefix to SFML_GRAPHICS_API definition to avoid name clash with SFML's graphics api macro. */
		#define SD_SFML_GRAPHICS_API __declspec(dllexport)
	#else
		#define SD_SFML_GRAPHICS_API __declspec(dllimport)
	#endif
#else
	#define SD_SFML_GRAPHICS_API
#endif

SD_BEGIN
extern SD_SFML_GRAPHICS_API LogCategory SfmlGraphicsLog;
SD_END