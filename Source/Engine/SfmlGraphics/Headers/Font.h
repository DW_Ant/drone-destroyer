/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Font.h
  An object responsible for storing a reference to a font resource.
=====================================================================
*/

#pragma once

#include "SfmlGraphics.h"

SD_BEGIN
class SD_SFML_GRAPHICS_API Font : public Object
{
	DECLARE_CLASS(Font)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Reads the number of spaces that are equal to one tab (in terms of character width).  This variable actually doesn't
	configure the tab width, it only records the width of SFML's tabs. */
	static const INT NUM_SPACES_PER_TAB;

	/* Location where fonts are stored. */
	static const Directory FONT_DIRECTORY;

	DString FontName;

protected:
	sf::Font* FontResource;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual DString GetFriendlyName () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void Release ();
	virtual void SetResource (sf::Font* newResource);

	/**
	 * Computes the total width in pixels of the specified character.
	 * @Param character:  The string that contains the character.  Reasoning for a string param instead of char is because some
	 * character encodings (such as UTF-8 or UTF-16) may require more than one 'character' to represent a single character.
	 */
	virtual FLOAT GetCharacterWidth (const DString& character, INT charSize) const;

	/**
	 * Computes the total width in pixels of the whole string considering the text size.
	 */
	virtual FLOAT CalculateStringWidth (const DString& line, INT charSize) const;

	/**
	 * Computes the total width (in pixels) of the characters (starting from startingIdx) within given string.
	 * If numChars is not negative, then it'll tally the total width of numChars starting from startingIdx.
	 * Otherwise, it'll count all following characters of startingIdx to the end of the string.
	 */
	virtual FLOAT CalculateStringWidth (const DString& line, INT charSize, INT startingIdx, INT numChars = -1) const;

	/**
	 * Returns the character index (of the specified string) that is closest to the maxWidth limit (never returns characters beyond the limit).
	 * Returns -1 if the first character is beyond the width limit.
	 */
	virtual INT FindCharNearWidthLimit (const DString& line, INT charSize, FLOAT maxWidth) const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual DString GetFontName () const;

	/**
	 * Returns the SFML's font resource.
	 */
	virtual const sf::Font* GetFontResource () const;
};
SD_END