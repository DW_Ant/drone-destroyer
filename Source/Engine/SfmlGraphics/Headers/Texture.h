/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Texture.h
  An object responsible for storing a reference to a texture resource.
=====================================================================
*/

#pragma once

#include "SFMLGraphics.h"

SD_BEGIN
class SD_SFML_GRAPHICS_API Texture : public Object
{
	DECLARE_CLASS(Texture)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const Directory TEXTURE_DIRECTORY;

protected:
	sf::Texture* TextureResource;

	INT Width;
	INT Height;
	DString TextureName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual DString GetFriendlyName () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void Release ();

	/**
	 * Overrides pixel data of the texture.
	 * @param newTexelData The vector of each texel data. The vector size must exactly match the size
	 * of this texture x 4. Each element in the vector corresponds to a single color channel in a texel.
	 *
	 * Texel[0]
	 *		newTexelData[0]=Red
	 *		newTexelData[1]=Green
	 *		newTexelData[2]=Blue
	 *		newTexelData[3]=Alpha
	 * Texel[1]
	 *		newTexelData[4]=Red
	 *		newTexelData[5]=Green
	 *		newTexelData[6]=Blue
	 *		newTexelData[7]=Alpha
	 * Texel[2]
	 * ...
	 *
	 * Each channel is a 8-bit value (from 0-255, where 0 corresponds to black. 255 corresponds to white).
	 * Returns true on success.
	 */
	virtual bool SetTexelData (const std::vector<sf::Uint8>& newTexelData);

	virtual void SetSmooth (bool bSmooth);

	virtual void SetRepeat (bool bRepeating);

	/**
	 * Destroys the previous resource, and sets the texture resource to the given params.
	 */
	virtual void SetResource (sf::Texture* newResource);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetTextureName (const DString& newTextureName)
	{
		TextureName = newTextureName;
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual sf::Texture* EditTextureResource ();

	virtual void GetDimensions (INT& outWidth, INT& outHeight) const;
	virtual void GetDimensions (Vector2& outDimensions) const;
	virtual Vector2 GetDimensions () const;
	virtual INT GetWidth () const;
	virtual INT GetHeight () const;
	virtual DString GetTextureName () const;

	/**
	 * Returns the SFML's texture resource.
	 */
	virtual const sf::Texture* GetTextureResource () const;

	/**
	 * Returns a copy of the raw image data that make up this texture. This is generally
	 * useful for manipulating textures dynamically.
	 *
	 * This is a slow operation since it downloads the texture data from the graphics
	 * card and copies them to a new image.
	 *
	 * Returns true if there is a valid image.
	 */
	virtual bool GetImage (sf::Image& outImage) const;
};
SD_END