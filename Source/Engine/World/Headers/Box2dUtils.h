/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Box2dUtils.h
  Contains utilities functions to help interface with Box 2D library.
=====================================================================
*/

#pragma once

#include "World.h"

SD_BEGIN
class WORLD_API Box2dUtils : public BaseUtils
{
	DECLARE_CLASS(Box2dUtils)
	

	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Converts the given distance unit from Sand Dune units to Box 2D units.
	 */
	static float ToBox2dDist (FLOAT sdDist);
	static FLOAT ToSdDist (float box2dDist);

	/**
	 * Converts a 2D vector from Sand Dune's coordinate system to Box 2D coordinate system.
	 * And vice versa.
	 */
	static b2Vec2 ToBox2dCoordinates (const Vector2& sdVect);
	static b2Vec2 ToBox2dCoordinates (const Vector3& sdVect);
	static Vector2 ToSdCoordinates (const b2Vec2& b2Vect);
	static Vector3 To3dSdCoordinates (const b2Vec2& b2Vect);

	/**
	 * Converts Sand Dune's Rotators to Box 2D rotation units.
	 */
	static float ToBox2dRotation (Rotator sdRotation);
	static Rotator ToSdRotation (float box2dRotation);

	/**
	 * Creates a box 2d rectangular shape on the stack using the given rectangular dimensions.
	 * The parameter are in SandDune units, and this function converts them to box 2d coordinates.
	 *
	 * @param dimensions The rectangle that ONLY describes the width and height. This function does not create position data for this shape.
	 */
	static b2PolygonShape GetBox2dRectangle (const Rectangle<FLOAT>& dimensions);
};
SD_END