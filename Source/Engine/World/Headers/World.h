/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  World.h
  The World module defines the objects needed to construct a universe. It'll define
  the coordinate system for the scene, the environment (such as maps and physics),
  and Entities that'll roam around the scene.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\SfmlGraphics\Headers\SfmlGraphicsClasses.h"

#include <Box2D\Box2D.h>


#if !(MODULE_CORE)
#error The World module requires the Core module.
#endif

#if !(MODULE_GRAPHICS)
#error The World module requires the Graphics module.
#endif

#if !(MODULE_SFML_GRAPHICS)
#error The World module requires the SFML Graphics module.
#endif


#ifdef PLATFORM_WINDOWS
	#ifdef WORLD_EXPORT
		#define WORLD_API __declspec(dllexport)
	#else
		#define WORLD_API __declspec(dllimport)
	#endif
#else
	#define WORLD_API
#endif

#define TICK_GROUP_PHYSICS "Physics"
#define TICK_GROUP_PRIORITY_PHYSICS 400 //Update physics before rendering.

SD_BEGIN
extern LogCategory WORLD_API WorldLog;
SD_END