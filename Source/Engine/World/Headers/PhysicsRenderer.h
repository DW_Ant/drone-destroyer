/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PhysicsRenderer.h

  A debugging render component that is used to visualize a PhysicsComponent.

  The shape is dictated by the owner's shape. This component assumes its owner only
  has at most 1 shape.

  This class is only available in debug mode.
=====================================================================
*/

#pragma once

#include "World.h"

SD_BEGIN

#ifdef DEBUG_MODE
class PhysicsComponent;

class WORLD_API PhysicsRenderer : public RenderComponent
{
	DECLARE_CLASS(PhysicsRenderer)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	/**
	 * A series of properties used to determine how this component should view in scene.
	 */
	enum eShapeType
	{
		ST_Unknown,
		ST_Polygon,
		ST_Circle
	};


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct PolygonDrawData
	{
		/* Series of vertices that are listed in counter clockwise order. SFML doesn't support rendering concave polygons.
		The vertices are in SD units and uses SD coordinate system. */
		std::vector<sf::Vector2f> Vertices;
	};

	struct CircleDrawData
	{
		FLOAT Radius;
	};

	struct SDrawData
	{
		eShapeType ShapeType;
	
		PolygonDrawData PolygonData;
		CircleDrawData CircleData;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Color FillColor;
	Color OutlineColor;

protected:
	std::vector<SDrawData> DrawData;
	

	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Searches through the Owner's component list and returns its PhysicsComponent if found.
	 */
	virtual PhysicsComponent* FindPhysicsComponent ();

	/**
	 * Populates the draw data based on the condition of the given PhysicsComponent.
	 */
	virtual void GetDrawShapesDataFrom (PhysicsComponent* physComp);
};
#endif
SD_END