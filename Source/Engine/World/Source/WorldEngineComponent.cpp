/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  WorldEngineComponent.cpp
=====================================================================
*/

#include "WorldClasses.h"

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(SD, WorldEngineComponent)

WorldEngineComponent::WorldEngineComponent () : EngineComponent(),
	DefaultGravity(Vector3::ZeroVector),
	WorldPhysics(b2Vec2(0.f, 0.f)),
	PhysicsTicker(nullptr),
	NumPhysIterationsPerFrame(5)
{
	//Noop
}

WorldEngineComponent::~WorldEngineComponent ()
{
	//Noop
}

void WorldEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	WorldPhysics.SetContactListener(this);
	WorldPhysics.SetContactFilter(this);

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_PHYSICS, TICK_GROUP_PRIORITY_PHYSICS);
	
#ifdef DEBUG_MODE
	localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, WorldEngineComponent, HandlePreGarbageCollection, void));
#endif
}

void WorldEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	PhysicsTicker = Entity::CreateObject();
	CHECK(PhysicsTicker != nullptr)

	TickComponent* comp = TickComponent::CreateObject(TICK_GROUP_PHYSICS);
	if (PhysicsTicker->AddComponent(comp))
	{
		comp->SetTickHandler(SDFUNCTION_1PARAM(this, WorldEngineComponent, HandleTickPhysics, void, FLOAT));
	}
}

void WorldEngineComponent::ShutdownComponent ()
{
	if (PhysicsTicker != nullptr)
	{
		PhysicsTicker->Destroy();
		PhysicsTicker = nullptr;
	}

#ifdef DEBUG_MODE
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, WorldEngineComponent, HandlePreGarbageCollection, void));
#endif

	Super::ShutdownComponent();
}

void WorldEngineComponent::BeginContact (b2Contact* contact)
{
	CHECK(contact != nullptr)
	PhysicsComponent* physA = nullptr;
	PhysicsComponent* physB = nullptr;
	if (!GetPhysicsComponentsFromContact(contact, OUT physA, OUT physB))
	{
		return;
	}

	physA->HandleBeginContact(physB, contact);
	physB->HandleBeginContact(physA, contact);
}

void WorldEngineComponent::EndContact (b2Contact* contact)
{
	CHECK(contact != nullptr)
	PhysicsComponent* physA = nullptr;
	PhysicsComponent* physB = nullptr;
	if (!GetPhysicsComponentsFromContact(contact, OUT physA, OUT physB))
	{
		return;
	}

	physA->HandleEndContact(physB, contact);
	physB->HandleEndContact(physA, contact);
}

void WorldEngineComponent::PreSolve (b2Contact* contact, const b2Manifold* oldManifold)
{
	CHECK(contact != nullptr)
	PhysicsComponent* physA = nullptr;
	PhysicsComponent* physB = nullptr;
	if (!GetPhysicsComponentsFromContact(contact, OUT physA, OUT physB))
	{
		return;
	}

	contact->SetEnabled((physA->CanBlockComponent(physB, contact, oldManifold) && physB->CanBlockComponent(physA, contact, oldManifold)));
}

void WorldEngineComponent::PostSolve (b2Contact* contact, const b2ContactImpulse* impulse)
{
	CHECK(contact != nullptr)
	PhysicsComponent* physA = nullptr;
	PhysicsComponent* physB = nullptr;
	if (!GetPhysicsComponentsFromContact(contact, OUT physA, OUT physB))
	{
		return;
	}

	physA->HandleCollision(physB, contact, impulse);
	physB->HandleCollision(physA, contact, impulse);
}

bool WorldEngineComponent::ShouldCollide (b2Fixture* fixtureA, b2Fixture* fixtureB)
{
	//First apply default behavior from Box2d. If box 2d does not permit collision, then return early.
	if (!b2ContactFilter::ShouldCollide(fixtureA, fixtureB))
	{
		return false;
	}

	CHECK(fixtureA != nullptr && fixtureB != nullptr)

	//Get PhysicsComponents from fixtures.
	b2Body* bodyA = fixtureA->GetBody();
	b2Body* bodyB = fixtureB->GetBody();
	if (bodyA == nullptr || bodyB == nullptr)
	{
		return false;
	}

	PhysicsComponent* physA = reinterpret_cast<PhysicsComponent*>(bodyA->GetUserData());
	PhysicsComponent* physB = reinterpret_cast<PhysicsComponent*>(bodyB->GetUserData());

	return (physA->CanBlockComponent(physB) && physB->CanBlockComponent(physA));
}

void WorldEngineComponent::DestroyPhysicsBody (b2Body* body)
{
#if ENABLE_COMPLEX_CHECKING
	CHECK(ContainerUtils::FindInVector(PendingBodyDestruction, body) == UINT_INDEX_NONE)
#endif

	PendingBodyDestruction.push_back(body);
}

void WorldEngineComponent::SetPhysicsUpdateRate (FLOAT newUpdateRate, INT numStepsPerUpdate)
{
	NumPhysIterationsPerFrame = numStepsPerUpdate;

	TickComponent* physTickComp = dynamic_cast<TickComponent*>(PhysicsTicker->FindSubComponent(TickComponent::SStaticClass(), false));
	if (physTickComp == nullptr)
	{
		WorldLog.Log(LogCategory::LL_Warning, TXT("Unable to update the physics rate to %s since the WorldEngineComponent is unable to find the Tick Component responsible for updating the Physics engine!"), newUpdateRate);
		return;
	}

	physTickComp->SetTickInterval(newUpdateRate);
}

void WorldEngineComponent::SetDefaultGravity (const Vector3 newDefaultGravity)
{
	DefaultGravity = newDefaultGravity;
}

bool WorldEngineComponent::GetPhysicsComponentsFromContact (b2Contact* contactData, PhysicsComponent*& outPhysCompA, PhysicsComponent*& outPhysCompB)
{
	b2Fixture* fixtureA = contactData->GetFixtureA();
	b2Fixture* fixtureB = contactData->GetFixtureB();
	if (fixtureA == nullptr || fixtureB == nullptr)
	{
		return false;
	}

	const b2Body* bodyA = fixtureA->GetBody();
	const b2Body* bodyB = fixtureB->GetBody();
	if (bodyA == nullptr || bodyB == nullptr)
	{
		return false;
	}

	if (bodyA->GetUserData() == nullptr || bodyB->GetUserData() == nullptr)
	{
		return false;
	}

	outPhysCompA = reinterpret_cast<PhysicsComponent*>(bodyA->GetUserData());
	outPhysCompB = reinterpret_cast<PhysicsComponent*>(bodyB->GetUserData());
	return true;
}

void WorldEngineComponent::HandleTickPhysics (FLOAT deltaSec)
{
	//World Physics should not be locked at this time. The simulation should start and finish within a tick cycle.
	CHECK(!WorldPhysics.IsLocked())

	//Remove all pending physics bodies before simulating physics.
	while (!ContainerUtils::IsEmpty(PendingBodyDestruction))
	{
		b2Body* lastBody = ContainerUtils::GetLast(PendingBodyDestruction);

		//Need to clear the user data from the PhysicsBody since it may return a corrupted pointer due to ContactList callback.
		lastBody->SetUserData(nullptr);

		//WorldPhysics::DestroyBody will free the memory associated with the physics body.
		WorldPhysics.DestroyBody(lastBody);
		PendingBodyDestruction.pop_back();
	}

	//Notify all callbacks that a physics cycle is about to start.
	OnPrePhysics.Broadcast(deltaSec);

	WorldPhysics.Step(deltaSec.Value, NumPhysIterationsPerFrame.ToInt32(), NumPhysIterationsPerFrame.ToInt32());

	//TODO:  Clear forces?

	OnPostPhysics.Broadcast(deltaSec);
}

void WorldEngineComponent::HandlePreGarbageCollection ()
{
#ifdef DEBUG_MODE
	//Ensure that a garbage collection event is not happening in a middle of the Physics cycle.
	//Otherwise there's a risk that PhysicsComponents could be removed, causing dangling pointers in Box2D
	CHECK_INFO(!WorldPhysics.IsLocked(), "Garbage Collection should not run in the middle of a Physics timestep!")
#endif
}
SD_END