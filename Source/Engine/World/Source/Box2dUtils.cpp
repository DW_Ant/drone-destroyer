/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Box2dUtils.cpp
=====================================================================
*/

#include "WorldClasses.h"

SD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(SD, Box2dUtils, SD, BaseUtils)

//Sand Dune uses centimeters. Box2D uses meters.
float Box2dUtils::ToBox2dDist (FLOAT sdDist)
{
	return sdDist.Value * 0.01f;
}

FLOAT Box2dUtils::ToSdDist (float box2dDist)
{
	return box2dDist * 100.f;
}

//Sand Dune uses a left handed coordinate system. X right, Y backwards, Z up.
//Box 2D coordinate system is X right, Y forward.
b2Vec2 Box2dUtils::ToBox2dCoordinates (const Vector2& sdVect)
{
	return b2Vec2(ToBox2dDist(sdVect.X), -ToBox2dDist(sdVect.Y));
}

b2Vec2 Box2dUtils::ToBox2dCoordinates (const Vector3& sdVect)
{
	return b2Vec2(ToBox2dDist(sdVect.X), -ToBox2dDist(sdVect.Y));
}

Vector2 Box2dUtils::ToSdCoordinates (const b2Vec2& b2Vect)
{
	return Vector2(ToSdDist(b2Vect.x), -ToSdDist(b2Vect.y));
}

Vector3 Box2dUtils::To3dSdCoordinates (const b2Vec2& b2Vect)
{
	return Vector3(ToSdDist(b2Vect.x), -ToSdDist(b2Vect.y), 0.f);
}

//Sand Dune uses an internal rotation unit for self normalization. Units rotate clockwise.
//Box 2D uses a floating point value in radians that rotates the object counterclockwise
float Box2dUtils::ToBox2dRotation (Rotator sdRotation)
{
	//Only yaw values are considered being a 2D coordinate system.
	FLOAT yaw = sdRotation.GetYaw(Rotator::RU_Radians);

	//Invert Yaw to rotate counterclockwise instead of clockwise.
	//yaw *= -1.f;

	return yaw.Value;
}

Rotator Box2dUtils::ToSdRotation (float box2dRotation)
{
	//Invert rotation to rotate clockwise instead of counterclockwise.
	//box2dRotation *= -1.f;

	return Rotator(box2dRotation, 0.f, 0.f, Rotator::RU_Radians);
}

b2PolygonShape Box2dUtils::GetBox2dRectangle (const Rectangle<FLOAT>& dimensions)
{
	b2PolygonShape results;

	//SetAsBox takes in half height and half width
	results.SetAsBox(ToBox2dDist(dimensions.GetWidth() * 0.5f), ToBox2dDist(dimensions.GetHeight() * 0.5f));

	return results;
}

SD_END