/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PhysicsRenderer.cpp
=====================================================================
*/

#include "WorldClasses.h"

SD_BEGIN
#ifdef DEBUG_MODE
IMPLEMENT_CLASS(SD, PhysicsRenderer, SD, RenderComponent)

void PhysicsRenderer::InitProps ()
{
	Super::InitProps();

	FillColor = Color(0, 0, 0, 0); //invisible
	OutlineColor = sf::Color::White;
}

void PhysicsRenderer::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const PhysicsRenderer* copyObj = dynamic_cast<const PhysicsRenderer*>(objTemplate);
	if (copyObj != nullptr)
	{
		FillColor = copyObj->FillColor;
		OutlineColor = copyObj->OutlineColor;

		for (size_t i = 0; i < copyObj->DrawData.size(); ++i)
		{
			SDrawData newDrawData;
			newDrawData.ShapeType = copyObj->DrawData.at(i).ShapeType;
			switch(newDrawData.ShapeType)
			{
				case(ST_Polygon) :
				{
					ContainerUtils::Empty(newDrawData.PolygonData.Vertices);
					newDrawData.PolygonData.Vertices.resize(copyObj->DrawData.at(i).PolygonData.Vertices.size());
					for (UINT_TYPE vertexIdx = 0; vertexIdx < copyObj->DrawData.at(i).PolygonData.Vertices.size(); ++vertexIdx)
					{
						newDrawData.PolygonData.Vertices.at(vertexIdx) = copyObj->DrawData.at(i).PolygonData.Vertices.at(vertexIdx);
					}
					break;
				}
				case(ST_Circle) :
					newDrawData.CircleData.Radius = copyObj->DrawData.at(i).CircleData.Radius;
					break;
			}

			DrawData.push_back(newDrawData);
		}
	}
}

void PhysicsRenderer::Render (RenderTarget* renderTarget, const Camera* camera)
{
	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	if (ContainerUtils::IsEmpty(DrawData))
	{
		PhysicsComponent* physComp = FindPhysicsComponent();
		CHECK(physComp != nullptr)

		GetDrawShapesDataFrom(physComp);
	}

	for (const SDrawData& drawData : DrawData)
	{
		switch (drawData.ShapeType)
		{
			case(ST_Unknown) :
				WorldLog.Log(LogCategory::LL_Warning, TXT("Attempted to draw physics renderer without a shape type defined."));
				break;

			case(ST_Polygon) :
			{
				sf::ConvexShape renderedShape(drawData.PolygonData.Vertices.size());

				renderedShape.setPosition(projectionData.Position);
				renderedShape.setScale(projectionData.Scale);
				renderedShape.setRotation(projectionData.Rotation);
				renderedShape.setOrigin(projectionData.Pivot);

				for (UINT_TYPE i = 0; i < drawData.PolygonData.Vertices.size(); ++i)
				{
					renderedShape.setPoint(i, drawData.PolygonData.Vertices.at(i));
				}

				renderedShape.setOutlineColor(OutlineColor.Source);
				renderedShape.setFillColor(FillColor.Source);
				renderTarget->Draw(renderedShape);
				break;
			}

			case(ST_Circle) :
			{
				sf::CircleShape renderedShape;

				renderedShape.setPosition(projectionData.Position);
				renderedShape.setScale(projectionData.Scale);
				renderedShape.setRotation(projectionData.Rotation);
				renderedShape.setOrigin(projectionData.Pivot + (sf::Vector2f(1.f, 1.f) * drawData.CircleData.Radius.Value)); //box2d circles are relative to the center.
				renderedShape.setRadius(drawData.CircleData.Radius.Value);

				renderedShape.setOutlineColor(OutlineColor.Source);
				renderedShape.setFillColor(FillColor.Source);
				renderTarget->Draw(renderedShape);
				break;
			}
		}
	}
}

void PhysicsRenderer::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	PhysicsComponent* physComp = FindPhysicsComponent();
	CHECK(physComp != nullptr)

	GetDrawShapesDataFrom(physComp);
}

void PhysicsRenderer::ComponentDetached ()
{
	ContainerUtils::Empty(DrawData);

	Super::ComponentDetached();
}

PhysicsComponent* PhysicsRenderer::FindPhysicsComponent ()
{
	CHECK(GetOwner() != nullptr);

	return dynamic_cast<PhysicsComponent*>(GetOwner()->FindSubComponent(PhysicsComponent::SStaticClass(), false));
}

void PhysicsRenderer::GetDrawShapesDataFrom (PhysicsComponent* physComp)
{
	CHECK(physComp != nullptr);

	b2Body* physBody = physComp->GetPhysicsBody();
	if (physBody == nullptr)
	{
		//PhyscsComponent has not initialized its physics data. Try again later.
		ContainerUtils::Empty(DrawData);
		return;
	}

	for (b2Fixture* fixture = physBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		SDrawData newDrawData;
		if (b2CircleShape* circleShape = dynamic_cast<b2CircleShape*>(fixture->GetShape()))
		{
			newDrawData.ShapeType = ST_Circle;
			newDrawData.CircleData.Radius = Box2dUtils::ToSdDist(circleShape->m_radius);
		}
		else if (b2PolygonShape* polyShape = dynamic_cast<b2PolygonShape*>(fixture->GetShape()))
		{
			newDrawData.ShapeType = ST_Polygon;

			ContainerUtils::Empty(newDrawData.PolygonData.Vertices);
			newDrawData.PolygonData.Vertices.resize(polyShape->m_count);
			for (UINT_TYPE i = 0; i < newDrawData.PolygonData.Vertices.size(); ++i)
			{
				newDrawData.PolygonData.Vertices.at(i) = Vector2::SDtoSFML(Box2dUtils::ToSdCoordinates(polyShape->m_vertices[i]));
			}
		}
		else
		{
			WorldLog.Log(LogCategory::LL_Warning, TXT("PhysicsRenderer is unable to render %s since its shape is not implemented."), physComp->ToString());
		}

		DrawData.push_back(newDrawData);
	}
}

#endif
SD_END