/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  WorldUnitTester.cpp
=====================================================================
*/

#include "WorldClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, WorldUnitTester, SD, UnitTester)

bool WorldUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool success = true;

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		success = TestConversions(testFlags);
	}

	return success;
}

bool WorldUnitTester::TestConversions (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Box 2D Conversions"));

	//This test essentially validates that the Box2D <-> Sand Dune conversion functions are symmetrical.
	FLOAT originalDist = 15.2f;
	Vector2 originalPos(-20.4f, 55.6f);
	Rotator originalRot(125.f, 0.f, 0.f, Rotator::RU_Degrees);

	float boxDist = Box2dUtils::ToBox2dDist(originalDist);
	b2Vec2 boxPos = Box2dUtils::ToBox2dCoordinates(originalPos);
	float boxRot = Box2dUtils::ToBox2dRotation(originalRot);

	//Convert it back to see if they yield the same results as the original.
	FLOAT convertedDist = Box2dUtils::ToSdDist(boxDist);
	Vector2 convertedPos = Box2dUtils::ToSdCoordinates(boxPos);
	Rotator convertedRot = Box2dUtils::ToSdRotation(boxRot);

	//Compare the values
	if (originalDist != convertedDist)
	{
		UnitTestError(testFlags, TXT("Box 2D Conversion test failed. The original distance value (%s) does not equal to the converted distance value (%s)."), {originalDist.ToString(), convertedDist.ToString()});
		return false;
	}

	if (originalPos != convertedPos)
	{
		UnitTestError(testFlags, TXT("Box 2D Conversion test failed. The original coordinates (%s) do not equal to the converted coordinates (%s)."), {originalPos.ToString(), convertedPos.ToString()});
		return false;
	}

	INT deltaYaw = INT(originalRot.Yaw) - INT(convertedRot.Yaw);
	deltaYaw.AbsInline();
	if (deltaYaw > 1) //1 to handle some degree of tolerance for floating point precision conversion errors.
	{
		UnitTestError(testFlags, TXT("Box 2D Conversion test failed. The original rotation (%s) does not equal to the converted rotation (%s)."), {originalRot.ToString(), convertedRot.ToString()});
		return false;
	}

	ExecuteSuccessSequence(testFlags, TXT("Box 2D Conversions"));
	return true;
}
SD_END

#endif