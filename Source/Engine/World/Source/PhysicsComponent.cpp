/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PhysicsComponent.cpp
=====================================================================
*/

#include "WorldClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, PhysicsComponent, SD, EntityComponent)

b2BodyDef PhysicsComponent::BodyInit;
const unsigned int PhysicsComponent::COLLISION_FLAG_ENTITY =			0x00000001;
const unsigned int PhysicsComponent::COLLISION_FLAG_WORLD_GEOMETRY =	0x00000002;

void PhysicsComponent::InitProps ()
{
	Super::InitProps();

	OverwritePosition = false;
	OverwriteVelocity = false;
	OverwriteRotation = false;

	PhysicsTick = nullptr;
	PhysicsBody = nullptr;

	Acceleration = Vector3::ZeroVector;
	Velocity = Vector3::ZeroVector;

	PrevPosition = Vector3::ZeroVector;
	PrevVelocity = Velocity;
	PrevRotation = Rotator::ZERO_ROTATOR;
}

void PhysicsComponent::BeginObject ()
{
	Super::BeginObject();

	WorldEngineComponent* worldEngine = WorldEngineComponent::Find();
	CHECK(worldEngine != nullptr)

	worldEngine->OnPrePhysics.RegisterHandler(SDFUNCTION_1PARAM(this, PhysicsComponent, HandlePrePhysics, void, FLOAT));
	worldEngine->OnPostPhysics.RegisterHandler(SDFUNCTION_1PARAM(this, PhysicsComponent, HandlePostPhysics, void, FLOAT));
}

void PhysicsComponent::Destroyed ()
{
	WorldEngineComponent* worldEngine = WorldEngineComponent::Find();
	if (worldEngine != nullptr) //If the engine component is not yet destroyed
	{
		worldEngine->OnPrePhysics.UnregisterHandler(SDFUNCTION_1PARAM(this, PhysicsComponent, HandlePrePhysics, void, FLOAT));
		worldEngine->OnPostPhysics.UnregisterHandler(SDFUNCTION_1PARAM(this, PhysicsComponent, HandlePostPhysics, void, FLOAT));

		if (PhysicsBody != nullptr)
		{
			worldEngine->DestroyPhysicsBody(PhysicsBody);
		}
	}
	
	Super::Destroyed();
}

bool PhysicsComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (dynamic_cast<SceneTransform*>(ownerCandidate) != nullptr)
	{
		return Super::CanBeAttachedTo(ownerCandidate);
	}

	WorldLog.Log(LogCategory::LL_Warning, TXT("PhysicsComponents can only attach to Scene Transformable Entities."));
	return false;
}

void PhysicsComponent::PostEngineInitialize () const
{
	Super::PostEngineInitialize();

	//Initialize of BodyInit's member variables that will not change.
	BodyInit.enabled = true;
	BodyInit.awake = true;
	BodyInit.allowSleep = true;

	BodyInit.angularVelocity = 0.f;
	BodyInit.gravityScale = 1.f;
}

void PhysicsComponent::InitializePhysics (const SPhysicsInitData& initData)
{
	if (PhysicsBody != nullptr)
	{
		WorldLog.Log(LogCategory::LL_Warning, TXT("Cannot reinitialize physics after the PhysicsComponent already has an associated physics body."));
		return;
	}

	WorldEngineComponent* worldEngine = WorldEngineComponent::Find();
	CHECK(worldEngine != nullptr)

	SceneTransform* ownerTransform = dynamic_cast<SceneTransform*>(GetOwner());
	if (ownerTransform == nullptr)
	{
		WorldLog.Log(LogCategory::LL_Warning, TXT("Cannot initialize PhysicsComponent since it is not yet attached to a Scene Transform."));
		return;
	}

	b2World& world = worldEngine->EditWorldPhysics();

	switch (initData.PhysicsType)
	{
		case(PT_Static):
			BodyInit.type = b2_staticBody;
			break;

		case (PT_Kinematic):
			BodyInit.type = b2_kinematicBody;
			break;

		case (PT_Dynamic):
			BodyInit.type = b2_dynamicBody;
			break;
	}

	PrevPosition = ownerTransform->GetTranslation();
	PrevVelocity = Velocity;
	PrevRotation = ownerTransform->GetRotation();
	
	BodyInit.position = Box2dUtils::ToBox2dCoordinates(ownerTransform->ReadTranslation());
	BodyInit.linearVelocity = Box2dUtils::ToBox2dCoordinates(Velocity);
	BodyInit.angle = Box2dUtils::ToBox2dRotation(ownerTransform->GetRotation());

	BodyInit.bullet = initData.EnableContinuousCollision;
	BodyInit.fixedRotation = initData.IsFixedRotation;

	BodyInit.angularDamping = initData.AngularDamping.Value;
	BodyInit.linearDamping = initData.LinearDamping.Value;
	BodyInit.userData = this;
	
	PhysicsBody = world.CreateBody(&BodyInit);
	CHECK(PhysicsBody != nullptr)
}

void PhysicsComponent::SetCollisionCategories (uint16 collisionCategory)
{
	if (PhysicsBody == nullptr)
	{
		WorldLog.Log(LogCategory::LL_Warning, TXT("Cannot set collision data to a PhysicsComponent before initializing its Physics."));
		return;
	}

	for (b2Fixture* fixture = PhysicsBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		b2Filter collisionFilter = fixture->GetFilterData();
		collisionFilter.categoryBits = collisionCategory;
		fixture->SetFilterData(collisionFilter);
	}
}

void PhysicsComponent::SetCollisionMasks (uint16 collisionMasks)
{
	if (PhysicsBody == nullptr)
	{
		WorldLog.Log(LogCategory::LL_Warning, TXT("Cannot set collision data to a PhysicsComponent before initializing its Physics."));
		return;
	}

	for (b2Fixture* fixture = PhysicsBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		b2Filter collisionFilter = fixture->GetFilterData();
		collisionFilter.maskBits = collisionMasks;
		fixture->SetFilterData(collisionFilter);
	}
}

void PhysicsComponent::SetCollisionGroup (int16 collisionGroup)
{
	if (PhysicsBody == nullptr)
	{
		WorldLog.Log(LogCategory::LL_Warning, TXT("Cannot set collision data to a PhysicsComponent before initializing its Physics."));
		return;
	}

	for (b2Fixture* fixture = PhysicsBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		b2Filter collisionFilter = fixture->GetFilterData();
		collisionFilter.groupIndex = collisionGroup;
		fixture->SetFilterData(collisionFilter);
	}
}

void PhysicsComponent::SetCollisionData (uint16 collisionCategories, uint16 collisionMasks, int16 collisionGroup)
{
	if (PhysicsBody == nullptr)
	{
		WorldLog.Log(LogCategory::LL_Warning, TXT("Cannot set collision data to a PhysicsComponent before initializing its Physics."));
		return;
	}

	for (b2Fixture* fixture = PhysicsBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		b2Filter collisionFilter = fixture->GetFilterData();

		collisionFilter.categoryBits = collisionCategories;
		collisionFilter.maskBits = collisionMasks;
		collisionFilter.groupIndex = collisionGroup;

		fixture->SetFilterData(collisionFilter);
	}
}

void PhysicsComponent::SetAcceleration (const Vector3& newAcceleration)
{
	Acceleration = newAcceleration;
}

void PhysicsComponent::SetVelocity (const Vector3& newVelocity)
{
	Velocity = newVelocity;
}

bool PhysicsComponent::CanBlockComponent (PhysicsComponent* other, b2Contact* contact, const b2Manifold* oldManifold) const
{
	return true;
}

bool PhysicsComponent::CanBlockComponent (PhysicsComponent* other) const
{
	return true;
}

void PhysicsComponent::HandlePrePhysics (FLOAT deltaSec)
{
	if (PhysicsBody == nullptr)
	{
		WorldLog.Log(LogCategory::LL_Warning, TXT("A PhysicsComponent is still registered to the WorldEngineComponent's Physics callback without having a PhysicsBody."));
		return;
	}

	SceneTransform* ownerTransform = dynamic_cast<SceneTransform*>(GetOwner());
	if (ownerTransform == nullptr)
	{
		WorldLog.Log(LogCategory::LL_Warning, TXT("A PhysicsComponent was detached from its owner SceneTransform. It cannot simulate physics without a transformation."));
		return;
	}

	//If the game overwrote this component's velocity or position, notify the physics engine before it runs its cycle.
	if (PrevPosition != ownerTransform->GetTranslation() || PrevRotation != ownerTransform->GetRotation())
	{
		b2Vec2 box2Pos = Box2dUtils::ToBox2dCoordinates(ownerTransform->GetTranslation());
		float box2Rot = Box2dUtils::ToBox2dRotation(ownerTransform->GetRotation());
		PhysicsBody->SetTransform(box2Pos, box2Rot);
	}

	//Change velocity when a change is detected or when there's continuous acceleration.
	if (PrevVelocity != Velocity || !Acceleration.IsEmpty())
	{
		b2Vec2 newVelocity = Box2dUtils::ToBox2dCoordinates(Velocity + (Acceleration * deltaSec));
		PhysicsBody->SetLinearVelocity(newVelocity);
	}
}

void PhysicsComponent::HandlePostPhysics (FLOAT deltaSec)
{
	if (PhysicsBody != nullptr)
	{
		PrevPosition = Box2dUtils::To3dSdCoordinates(PhysicsBody->GetPosition());
		PrevVelocity = Box2dUtils::To3dSdCoordinates(PhysicsBody->GetLinearVelocity());
		PrevRotation = Box2dUtils::ToSdRotation(PhysicsBody->GetAngle());

		if (!OverwriteVelocity)
		{
			Velocity.X = PrevVelocity.X;
			Velocity.Y = PrevVelocity.Y;
		}

		if (SceneTransform* owner = dynamic_cast<SceneTransform*>(GetOwner()))
		{
			if (!OverwritePosition)
			{
				owner->EditTranslation().X = PrevPosition.X;
				owner->EditTranslation().Y = PrevPosition.Y;
			}

			if (!OverwriteRotation)
			{
				owner->SetRotation(PrevRotation);
			}
		}

		OverwritePosition = false;
		OverwriteVelocity = false;
		OverwriteRotation = false;
	}
}

void PhysicsComponent::HandleBeginContact (PhysicsComponent* otherComp, b2Contact* contactData)
{
	if (OnBeginContact.IsBounded())
	{
		OnBeginContact(otherComp, contactData);
	}
}

void PhysicsComponent::HandleEndContact (PhysicsComponent* otherComp, b2Contact* contactData)
{
	if (OnEndContact.IsBounded())
	{
		OnEndContact(otherComp, contactData);
	}
}

void PhysicsComponent::HandleCollision (PhysicsComponent* otherComp, b2Contact* contactData, const b2ContactImpulse* impulse)
{
	if (OnCollision.IsBounded())
	{
		OnCollision(otherComp, contactData, impulse);
	}
}
SD_END