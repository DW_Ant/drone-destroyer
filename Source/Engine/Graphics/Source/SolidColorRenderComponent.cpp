/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolidColorRenderComponent.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, SolidColorRenderComponent, SD, RenderComponent)

void SolidColorRenderComponent::InitProps ()
{
	Super::InitProps();

	Shape = ST_Rectangle;
	SolidColor = sf::Color(0, 0, 0, 255);
	BaseSize = 1.f;
	RectHeight = 1.f;
	CenterOrigin = false;
}

void SolidColorRenderComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const SolidColorRenderComponent* colorTemplate = dynamic_cast<const SolidColorRenderComponent*>(objTemplate);
	if (colorTemplate != nullptr)
	{
		Shape = colorTemplate->Shape;
		SolidColor = colorTemplate->SolidColor;
		BaseSize = colorTemplate->BaseSize;
		RectHeight = colorTemplate->RectHeight;
		CenterOrigin = colorTemplate->CenterOrigin;
	}
}

void SolidColorRenderComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	Transformation::SScreenProjectionData projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	switch (Shape)
	{
		case (ST_Rectangle):
		{
			//Make sure the pivot scales relative to the base size
			projectionData.Pivot.x *= BaseSize;
			projectionData.Pivot.y *= RectHeight;

			sf::RectangleShape renderedShape;
			renderedShape.setPosition(projectionData.Position);
			renderedShape.setScale(projectionData.GetFinalSize());
			renderedShape.setRotation(projectionData.Rotation);
			renderedShape.setOrigin(projectionData.Pivot);
			renderedShape.setSize(sf::Vector2f(BaseSize.Value, RectHeight.Value));

			if (CenterOrigin)
			{
				renderedShape.setOrigin(SD::Vector2::SDtoSFML(SD::Vector2(BaseSize, RectHeight) * 0.5f));
			}

			renderedShape.setFillColor(SolidColor.Source);
			renderTarget->Draw(renderedShape);	

			break;
		}
		case (ST_Circle):
		{
			//Make sure the pivot scales relative to the base size. We use half the BaseSize since it's the total width (diameter), but SFML expects radii.
			projectionData.Pivot.x *= BaseSize * 0.5f;
			projectionData.Pivot.y *= BaseSize * 0.5f;

			sf::CircleShape renderedShape;
			renderedShape.setPosition(projectionData.Position);
			renderedShape.setScale(projectionData.GetFinalSize());
			renderedShape.setRotation(projectionData.Rotation);
			renderedShape.setOrigin(projectionData.Pivot);
			renderedShape.setRadius(BaseSize.Value * 0.5f);

			if (CenterOrigin)
			{
				renderedShape.setOrigin(SD::Vector2::SDtoSFML(SD::Vector2(BaseSize, BaseSize) * 0.5f));
			}

			renderedShape.setFillColor(SolidColor.Source);
			renderTarget->Draw(renderedShape);

			break;
		}
	}
}
SD_END