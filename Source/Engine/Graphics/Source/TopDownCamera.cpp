/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TopDownCamera.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, TopDownCamera, SD, SceneCamera)

void TopDownCamera::InitProps ()
{
	Super::InitProps();

	Zoom = 0.01f; //1 meter in the scene consumes about 1 cm of screen space.
}

void TopDownCamera::ProjectToScreenCoordinates (const Vector2& viewportSize, const SceneTransform& transform, Transformation::SScreenProjectionData& outProjectionData) const
{
	Vector3 relativeDist = ReadAbsTranslation() - transform.ReadAbsTranslation();

	FLOAT scalarMultiplier = GetScalarProjection();
	outProjectionData.Position.x = ((viewportSize.X * 0.5f) - (relativeDist.X * scalarMultiplier) + (AbsPivotPoint.X * scalarMultiplier)).Value;
	outProjectionData.Position.y = ((viewportSize.Y * 0.5f) - (relativeDist.Y * scalarMultiplier) + (AbsPivotPoint.Y * scalarMultiplier)).Value;

	outProjectionData.BaseSize = Vector2::SDtoSFML(transform.ReadAbsScale().ToVector2());
	outProjectionData.Scale = sf::Vector2f(scalarMultiplier.Value, scalarMultiplier.Value);

	outProjectionData.Rotation = -transform.ReadAbsRotation().GetYaw(Rotator::RU_Degrees).Value;
	outProjectionData.Pivot = Vector2::SDtoSFML(transform.ReadAbsPivotPoint().ToVector2());
}

void TopDownCamera::CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const
{
	Vector2 deltaMousePos = pixelPos - (viewportSize * 0.5f);
	deltaMousePos /= GetScalarProjection();
	outLocation.X = AbsTranslation.X + deltaMousePos.X; 
	outLocation.Y = AbsTranslation.Y + deltaMousePos.Y;
	outDirection = Rotator(0, Rotator::QUARTER_REVOLUTION, 0); //Look straight down
}

FLOAT TopDownCamera::GetDistSquaredToPoint (const Vector3& worldLocation) const
{
	//This camera is aligned to Z-axis with its projection perpendicular to XY plane. The distance is simply the Z distances between point and camera.
	return std::powf((worldLocation.Z - AbsTranslation.Z).Value, 2.f);
}

FLOAT TopDownCamera::GetScalarProjection () const
{
	FLOAT dpi = GraphicsEngineComponent::GetDpiScale().ToFLOAT();
	const FLOAT inchesPerCentimeter = 0.3937008f;

	return dpi * inchesPerCentimeter * Zoom;
}
SD_END