/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SpriteTester.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, SpriteTester, SD, Entity)

void SpriteTester::InitProps ()
{
	Super::InitProps();

	EnableFractionScaling = false;

	SwellMultiplier = 1.f;

	LivingTime = 0.f;
	TestingStage = -1;
	Tick = nullptr;
	Sprite = nullptr;
}

void SpriteTester::BeginObject ()
{
	Super::BeginObject();

	Tick = TickComponent::CreateObject(TICK_GROUP_DEBUG);
	if (!AddComponent(Tick.Get()))
	{
		Tick->Destroy();
		Tick = nullptr;
		UnitTester::TestLog(TestFlags, TXT("Sprite Test failed.  Unable to attach a Tick Component to SpriteTester."));
		Destroy();
		return;
	}
	Tick->SetTickHandler(SDFUNCTION_1PARAM(this, SpriteTester, HandleTick, void, FLOAT));

	SetPosition(Vector2(128.f, 128.f));
	SetSize(Vector2(256.f, 256.f));
	SetDepth(5.f);

	Sprite = SpriteComponent::CreateObject();
	if (!AddComponent(Sprite.Get()))
	{
		UnitTester::TestLog(TestFlags, TXT("Sprite Test failed.  Unable to attach a Sprite Component to SpriteTester."));
		Destroy();
		return;
	}

	if (PlanarDrawLayer* canvas = GraphicsEngineComponent::Find()->GetCanvasDrawLayer())
	{
		canvas->RegisterSingleComponent(Sprite.Get());
	}
}

FLOAT SpriteTester::CalculateSpriteScale () const
{
	//Formula:  (sin((x*speed))/4) + 0.25     sin: x is time.  Divide 4 keeps the wave height less than 1.  +0.5 so the valley of wave is positive (min value 0.25, max value 0.75).
	FLOAT sizeMultiplier = (sinf((LivingTime * SwellMultiplier).Value)/4.f) + 0.25f;

	const FLOAT baseSize = 256.f;
	return baseSize * sizeMultiplier;
}

bool SpriteTester::TickTestStage (FLOAT deltaSec)
{
	FLOAT stageIncrementInterval = SwellMultiplier / (2 * PI_FLOAT); //Multiplier for the frequency in how often the stage increments.  This should increment every time the wave is at a valley.
	FLOAT oldTime = LivingTime;
	LivingTime += deltaSec;
	bool bNewStage = ((LivingTime * stageIncrementInterval).ToINT()) != ((oldTime * stageIncrementInterval).ToINT());
		
	return bNewStage;
}

void SpriteTester::HandleTick (FLOAT deltaSec)
{
	if (TestingStage < 4)
	{
		FLOAT spriteScale = CalculateSpriteScale();
		SetSize(Vector2(spriteScale, spriteScale));
	}

	if (!TickTestStage(deltaSec))
	{
		return;
	}

	TestingStage++;
	switch(TestingStage.Value)
	{
		case(0):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Displaying scaled texture, no sub divisions."));
			Sprite->SetSpriteTexture(TexturePool::FindTexturePool()->FindTexture(TXT("DebuggingTexture")));
			Sprite->SetSubDivision(1, 1, 0, 0);
			break;

		case(1):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Displaying scaled texture, sub divisions (1x2).  Displaying lower half."));
			Sprite->SetSubDivision(1, 2, 0, 1);
			break;

		case(2):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Displaying scaled texture, sub divisions (2x2).  Displaying upper right corner"));
			Sprite->SetSubDivision(2, 2, 1, 0);
			break;

		case(3):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Displaying scaled texture, sub divisions (4x4)"));
			Sprite->SetSubDivision(4, 4, 0, 0);
			break;

		case(4):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Drawing top left half of sprite.  Setting scale multipliers to cover 1/4th the window."));
			Sprite->SetDrawCoordinatesMultipliers(0.5f, 0.5f, 0.f, 0.f);
			SetSize(Vector2(0.25f, 0.25f)); //Cover only a quarter of the window.
			break;

		case(5):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Drawing center of half of sprite."));
			Sprite->SetDrawCoordinatesMultipliers(0.5f, 0.5f, 0.5f, 0.5f);
			break;

		case(6):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Drawing 4x of sprite (should be 2x2 tiles)."));
			Sprite->SetDrawCoordinatesMultipliers(2.f, 2.f, 0.f, 0.f);
			break;

		case(7):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester: Drawing 4x of sprite (should be 2x2 tiles).  Setting scale to 1/8."));
			Sprite->SetDrawCoordinatesMultipliers(2.f, 2.f, 0.f, 0.f);
			SetSize(Vector2(0.125f, 0.125f)); //8th of the window
			break;

		case(8):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Drawing 12.5% of sprite.  Setting scale multipliers to cover 1/4th the window."));
			Sprite->SetDrawCoordinatesMultipliers(0.125f, 0.125f, 0.f, 0.f);
			SetSize(Vector2(0.25f, 0.25f));
			break;

		case(9):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Drawing top left 64 pixels of sprite."));
			Sprite->SetDrawCoordinates(0, 0, 64, 64);
			break;

		case(10):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Drawing 256 pixels of sprite starting from coordinate 128."));
			Sprite->SetDrawCoordinates(128, 128, 256, 256);
			break;

		case(11):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Drawing 1024 pixels of sprite.  Setting scale to cover half the window."));
			Sprite->SetDrawCoordinates(0, 0, 1024, 1024);
			SetSize(Vector2(0.5f, 0.5f));
			break;

		case(12):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Drawing 1024 pixels of sprite.  Starting coordinates at 32 pixels.  Setting scale multipliers to 1/4th the window."));
			Sprite->SetDrawCoordinates(32, 32, 1024, 1024);
			SetSize(Vector2(0.25f, 0.25f));
			break;

		case(13):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Clearing texture coordinates.  Drawing sprite plainly."));
			Sprite->SetDrawCoordinatesMultipliers(1.f, 1.f, 0.f, 0.f);
			break;

		case(14):
			UnitTester::TestLog(TestFlags, TXT("SpriteTester:  Finished Sprite Component test.  Destroying self."));
			Destroy();
			break;
	}
}
SD_END

#endif