/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FontPool.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, FontPool, SD, ResourcePool)
IMPLEMENT_RESOURCE_POOL(Font)

void FontPool::InitProps ()
{
	Super::InitProps();

	DefaultFont = nullptr;
}

void FontPool::BeginObject ()
{
	Super::BeginObject();

	RegisterResourcePool();

	DefaultFont = FindFont(TXT("DefaultFont"));
	if (DefaultFont == nullptr)
	{
		//Use FindFontPool instead of self to refer to the registered font pool instance for proper clean up.
		DefaultFont = CreateAndImportFont(TXT("QuattrocentoSans-Regular.ttf"), TXT("DefaultFont"));
	}
}

void FontPool::ReleaseResources ()
{
	for (UINT_TYPE i = 0; i < ImportedResources.size(); i++)
	{
		ImportedResources.at(i).Font->Destroy();
	}

	DefaultFont = nullptr;
	ContainerUtils::Empty(ImportedResources);
}

void FontPool::Destroyed ()
{
	RemoveResourcePool();

	Super::Destroyed();
}

Font* FontPool::CreateAndImportFont (DString fileName, DString fontName)
{
	if (!IsValidName(fontName))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to create font since the specified name (%s) is either invalid or taken."), fontName);
		return nullptr;
	}

	//Import texture
	fileName = Font::FONT_DIRECTORY.ReadDirectoryPath() + fileName;
	sf::Font* newFont = new sf::Font();
	if (!newFont->loadFromFile(fileName.ToCString()))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to create font.  Could not find %s"), fileName);
		delete newFont;
		return nullptr;
	}

	Font* result = Font::CreateObject();
	if (result == nullptr)
	{
		delete newFont;
		return nullptr;
	}

	result->SetResource(newFont);
	result->FontName = fontName;
	if (!ImportFont(result, fontName))
	{
		//Failed to import.  destroy font
		result->Release();
		result->Destroy();
		return nullptr;
	}

	return result;
}

Font* FontPool::GetDefaultFont () const
{
	return DefaultFont.Get();
}
SD_END