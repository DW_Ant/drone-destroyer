/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DrawLayer.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(SD, DrawLayer, SD, Object)

void DrawLayer::InitProps ()
{
	Super::InitProps();

	DrawPriority = -1;
}

void DrawLayer::SetDrawPriority (INT newDrawPriority)
{
	const DrawLayer* defaultObject = dynamic_cast<const DrawLayer*>(GetDefaultObject());

	//Check if the draw priority was assigned already or not.
	if (defaultObject != nullptr && GetDrawPriority() != defaultObject->GetDrawPriority())
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot reassign a DrawLayer's priority value after it has been established."));
		return;
	}

	DrawPriority = newDrawPriority;
}
SD_END