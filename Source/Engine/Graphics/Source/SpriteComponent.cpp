/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SpriteComponent.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, SpriteComponent, SD, RenderComponent)

void SpriteComponent::InitProps ()
{
	Super::InitProps();

	CenterOrigin = false;

	Sprite = nullptr;

	SpriteShader = nullptr;
	RenderState = sf::RenderStates::Default;

	BaseSize = Vector2(1.f, 1.f);
	SubDivideScaleX = 1.f;
	SubDivideScaleY = 1.f;

	InvisibleTexture = true; //Sprite texture not yet assigned
	RenderTextureOwner = nullptr;
}

void SpriteComponent::BeginObject ()
{
	Super::BeginObject();

	Sprite = new sf::Sprite();
}

void SpriteComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const SpriteComponent* spriteTemplate = dynamic_cast<const SpriteComponent*>(objTemplate);
	if (spriteTemplate != nullptr)
	{
		SetSpriteTexture(spriteTemplate->Sprite->getTexture());
		SetSpriteShader(spriteTemplate->GetSpriteShader());

		BaseSize = spriteTemplate->BaseSize;
		SubDivideScaleX = spriteTemplate->SubDivideScaleX;
		SubDivideScaleY = spriteTemplate->SubDivideScaleY;

		InvisibleTexture = spriteTemplate->InvisibleTexture;
	}
}

void SpriteComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	if (Sprite == nullptr || InvisibleTexture)
	{
		return;
	}

	CHECK(GetOwnerTransform() != nullptr)
	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);
	Sprite->setPosition(projectionData.Position);
	Sprite->setScale(projectionData.GetFinalSize() * BaseSize);
	Sprite->setRotation(projectionData.Rotation);
	Sprite->setOrigin(projectionData.Pivot);

	if (CenterOrigin)
	{
		Sprite->setOrigin(Vector2::SDtoSFML(GetTextureSize() * 0.5f));
	}

	//Apply sprite scale multipliers
	sf::Vector2f spriteSize(SubDivideScaleX.Value, SubDivideScaleY.Value);
	spriteSize /= GetTextureSize();
	spriteSize *= Sprite->getScale();
	Sprite->setScale(spriteSize);

	renderTarget->Draw(*Sprite, RenderState);

	//Special case for RenderTextures.  This is used to notify the RenderTexture where it's being drawn in relation to the window.
	if (RenderTextureOwner != nullptr)
	{
		RenderTextureOwner->SetLatestDrawCoordinates(renderTarget->GetWindowCoordinates() + Vector2::SFMLtoSD(projectionData.Position));
	}
}

void SpriteComponent::Destroyed ()
{
	if (SpriteShader != nullptr)
	{
		SpriteShader->Destroy();
	}

	if (Sprite != nullptr)
	{
		delete Sprite;
		Sprite = nullptr;
	}

	Super::Destroyed();
}

bool SpriteComponent::SetSubDivision (INT numHorizontalSegments, INT numVerticalSegments, INT horizontalIndex, INT verticalIndex)
{
	if (Sprite->getTexture() == nullptr)
	{
		return false;
	}

	if (numHorizontalSegments <= 0 || numVerticalSegments <= 0 || horizontalIndex < 0 || verticalIndex < 0)
	{
		return false;
	}

	if (horizontalIndex >= numHorizontalSegments || verticalIndex >= numVerticalSegments)
	{
		return false;
	}

	sf::Vector2u totalSize = Sprite->getTexture()->getSize();

	sf::IntRect sfRect;
	sfRect.width = totalSize.x/numHorizontalSegments.ToUnsignedInt32();
	sfRect.height = totalSize.y/numVerticalSegments.ToUnsignedInt32();
	sfRect.left = horizontalIndex.ToUnsignedInt32() * sfRect.width;
	sfRect.top = verticalIndex.ToUnsignedInt32() * sfRect.height;

	Sprite->setTextureRect(sfRect);

	SubDivideScaleX = numHorizontalSegments.ToFLOAT();
	SubDivideScaleY = numVerticalSegments.ToFLOAT();

	return true;
}

void SpriteComponent::SetDrawCoordinatesMultipliers (FLOAT xScaleMultiplier, FLOAT yScaleMultiplier, FLOAT xPosMultiplier, FLOAT yPosMultiplier)
{
	if (Sprite == nullptr || Sprite->getTexture() == nullptr)
	{
		return;
	}

	sf::Vector2f totalSize = sf::Vector2f(Sprite->getTexture()->getSize());
	sf::FloatRect sfRect;
	sfRect.width = totalSize.x * xScaleMultiplier.Value;
	sfRect.height = totalSize.y * yScaleMultiplier.Value;
	sfRect.left = totalSize.x * xPosMultiplier.Value;
	sfRect.top = totalSize.y * yPosMultiplier.Value;

	Sprite->setTextureRect(sf::IntRect(sfRect));

	SubDivideScaleX = 1/xScaleMultiplier;
	SubDivideScaleY = 1/yScaleMultiplier;
}

void SpriteComponent::SetDrawCoordinates (INT u, INT v, INT ul, INT vl)
{
	sf::IntRect sfRect;
	sfRect.width = ul.ToInt32();
	sfRect.height = vl.ToInt32();
	sfRect.left = u.ToInt32();
	sfRect.top = v.ToInt32();

	Sprite->setTextureRect(sfRect);

	if (Sprite != nullptr && Sprite->getTexture() != nullptr)
	{
		sf::Vector2u totalSize = Sprite->getTexture()->getSize();
		SubDivideScaleX = INT(totalSize.x).ToFLOAT() / ul.ToFLOAT();
		SubDivideScaleY = INT(totalSize.y).ToFLOAT() / vl.ToFLOAT();
	}
}

void SpriteComponent::SetSpriteTexture (const sf::Texture* newTexture)
{
	CHECK(Sprite != nullptr)

	if (newTexture == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Texture resource is null when attempting to set it on a SpriteComponent.  If nullptr is intended, use ClearSpriteTexture instead."));
		return;
	}

	if (SpriteShader != nullptr && SpriteShader->SFShader != nullptr)
	{
		SpriteShader->SFShader->setUniform(SHADER_BASE_TEXTURE, newTexture);
	}

	Sprite->setTexture(*newTexture);
	InvisibleTexture = false;
	RenderTextureOwner = nullptr;

	//Need to update draw coordinates since setting the texture initially would update the the sprite's draw rectangle, but subsequent texture updates will not.
	//For example, if a sprite initializes to use a 256x256 texture, the sprite's textureRect value is 256,256,0,0 (zero being positions).  Later if that same
	//	sprite changed to a 128x128 texture, the sprite's textureRect is still 256,256,0,0.
	Vector2 newTextureSize = GetTextureSize();
	sf::IntRect curRect = Sprite->getTextureRect();
	SetDrawCoordinates(curRect.left, curRect.top, (newTextureSize.X/SubDivideScaleX).ToINT(), (newTextureSize.Y/SubDivideScaleY).ToINT());

	OnTextureChangedCallbacks.Broadcast();
}

void SpriteComponent::SetSpriteTexture (const Texture* newTexture)
{
	const sf::Texture* newSfTexture = nullptr;
	if (newTexture != nullptr)
	{
		newSfTexture = newTexture->GetTextureResource();
	}

	SetSpriteTexture(newSfTexture);
}

void SpriteComponent::SetSpriteTexture (RenderTexture* renderTextureOwner)
{
	if (renderTextureOwner->GetResource() == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Failed to assign sprite texture since the RenderTexture does not have a resource associated with it."));
		return;
	}

	SetSpriteTexture(&renderTextureOwner->GetResource()->getTexture());
	RenderTextureOwner = renderTextureOwner;
}

void SpriteComponent::ClearSpriteTexture ()
{
	InvisibleTexture = true;
}

void SpriteComponent::SetSpriteShader (Shader* newSpriteShader)
{
	CHECK(Sprite != nullptr)

	if (SpriteShader != nullptr)
	{
		SpriteShader->Destroy();
	}

	SpriteShader = newSpriteShader;
	if (SpriteShader == nullptr)
	{
		RenderState = sf::RenderStates::Default;
		return;
	}

	if (SpriteShader->SFShader == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Setting sprite shader (%s) to %s before binding a SF Shader instance upon the shader, itself, will cause the sprite component to render incorrectly."), SpriteShader->GetFriendlyName(), ToString());
		return;
	}

	SpriteShader->SFShader->setUniform(SHADER_BASE_TEXTURE, *Sprite->getTexture());
	RenderState = sf::RenderStates(SpriteShader->SFShader);
}

const sf::Sprite* SpriteComponent::GetSprite () const
{
	return Sprite;
}

Shader* SpriteComponent::GetSpriteShader () const
{
	return SpriteShader.Get();
}

void SpriteComponent::GetTextureSize (INT& outWidth, INT& outHeight) const
{
	CHECK(Sprite != nullptr)

	outWidth = 0;
	outHeight = 0;

	if (Sprite->getTexture() != nullptr)
	{
		sf::Vector2u textureSize = Sprite->getTexture()->getSize();
		outWidth = textureSize.x;
		outHeight = textureSize.y;
	}
}

Vector2 SpriteComponent::GetTextureSize () const
{
	INT width;
	INT height;
	GetTextureSize(OUT width, OUT height);

	return Vector2(width.ToFLOAT(), height.ToFLOAT());
}

void SpriteComponent::GetDrawCoordinates (INT& outU, INT& outV, INT& outUl, INT& outVl) const
{
	if (Sprite != nullptr)
	{
		const sf::IntRect& region = Sprite->getTextureRect();
		outU = region.left;
		outV = region.top;
		outUl = region.width;
		outVl = region.height;
	}
}
SD_END