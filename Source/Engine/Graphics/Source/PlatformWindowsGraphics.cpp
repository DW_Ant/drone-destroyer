/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlatformWindowsGraphics.cpp
=====================================================================
*/


#include "GraphicsClasses.h"

#ifdef PLATFORM_WINDOWS

SD_BEGIN
INT OS_GetSystemDpi (const Window* windowHandle)
{
	if (windowHandle == nullptr)
	{
		return -1;
	}

	UINT winDpi = GetDpiForWindow(windowHandle->GetResource()->getSystemHandle());

	return INT(winDpi);
}

Rectangle<INT> OS_GetClientWindowPos (const Window* windowHandle)
{
	RECT clientWindow;
	if (!GetClientRect(windowHandle->GetResource()->getSystemHandle(), &clientWindow))
	{
		return Rectangle<INT>();
	}

	RECT window;
	if (!GetWindowRect(windowHandle->GetResource()->getSystemHandle(), &window))
	{
		return Rectangle<INT>();
	}

	INT borderThickness = ((window.right - window.left) - clientWindow.right) / 2;
	INT titlebarThickness = ((window.bottom - window.top) - clientWindow.bottom - borderThickness);

	//Convert windowsRect to SD Rectangle
	Rectangle<INT> result(Rectangle<INT>::CS_XRightYDown);
	result.Left = window.left + borderThickness;
	result.Top = window.top + titlebarThickness;
	result.Right = window.right - borderThickness;
	result.Bottom = window.bottom - borderThickness;

	return result;
}
SD_END

#endif