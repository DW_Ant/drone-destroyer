/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlanarCamera.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, PlanarCamera, SD, Camera)

void PlanarCamera::InitProps ()
{
	Super::InitProps();

	Zoom = 1.f;
	ViewExtents = Vector2::ZeroVector;
}

void PlanarCamera::CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const
{
	Vector2 planeLocation = GetCachedAbsPosition();

	//pixelPos is relative to the top left corner of viewport, but the viewport's center draws from the camera.  Offset pixelPos by half the viewport size.
	planeLocation += (pixelPos - (viewportSize * 0.5f));

	outLocation = planeLocation.ToVector3();
	
	//Planar cameras don't have height and direction.
	outDirection = Rotator::ZERO_ROTATOR;
}

void PlanarCamera::SetZoom (FLOAT newZoom)
{
	if (Zoom != newZoom)
	{
		if (newZoom <= 0)
		{
			const FLOAT fallbackZoom = 0.01f;
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Attempted to set a PlanarCamera's zoom to %s. Clamping zoom to %s"), newZoom, fallbackZoom);
			newZoom = fallbackZoom;
		}

		Zoom = newZoom;
		OnZoomChanged.Broadcast(Zoom);
	}
}

void PlanarCamera::SetViewExtents (const Vector2& newViewExtents)
{
	ViewExtents = newViewExtents;
}
SD_END