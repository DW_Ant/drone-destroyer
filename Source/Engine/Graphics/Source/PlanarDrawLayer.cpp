/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlanarDrawLayer.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, PlanarDrawLayer, SD, DrawLayer)

const INT PlanarDrawLayer::CANVAS_LAYER_PRIORITY = 1000;

void PlanarDrawLayer::BeginObject ()
{
	Super::BeginObject();

	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, PlanarDrawLayer, HandleGarbageCollection, void));
	}
}

void PlanarDrawLayer::RenderDrawLayer (RenderTarget* renderTarget, Camera* cam)
{
	PlanarCamera* planarCam = dynamic_cast<PlanarCamera*>(cam);
	if (planarCam != nullptr)
	{
		//Notify the camera to calculate its absolute coordinates.
		planarCam->ComputeAbsTransform();
	}
	
	RenderComponents(renderTarget, planarCam);
}

void PlanarDrawLayer::Destroyed ()
{
	Engine* localEngine = Engine::FindEngine();
	if (localEngine != nullptr)
	{
		localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, PlanarDrawLayer, HandleGarbageCollection, void));
	}

	Super::Destroyed();
}

void PlanarDrawLayer::RegisterSingleComponent (RenderComponent* newRenderComp)
{
#if ENABLE_COMPLEX_CHECKING
	//Make sure the component is registered at most once
	CHECK(ContainerUtils::FindInVector(ComponentsToDraw, newRenderComp) == UINT_INDEX_NONE)
#endif

	if (dynamic_cast<const PlanarTransform*>(newRenderComp->GetOwner()) == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot register the component %s to the PlanarDrawLayer since the component's owner does not does not have a PlanarTransformation."), newRenderComp->ToString());
		return;
	}

	ComponentsToDraw.push_back(newRenderComp);
}

void PlanarDrawLayer::RenderComponents (RenderTarget* renderTarget, PlanarCamera* cam)
{
	//Early out if there aren't any components to render (since this function is slow)
	if (ContainerUtils::IsEmpty(ComponentsToDraw))
	{
		return;
	}

	//One reason why ComponentsToDraw vector should be minimal due to slow performance from excessive casting owner as transform.
	PurgeExpiredComponents();
	std::sort(ComponentsToDraw.begin(), ComponentsToDraw.end(), [](RenderComponent* a, RenderComponent* b)
	{
		if (const PlanarTransform* aTransform = dynamic_cast<const PlanarTransform*>(a->GetOwner()))
		{
			if (const PlanarTransform* bTransform = dynamic_cast<const PlanarTransform*>(b->GetOwner()))
			{
				return (aTransform->GetDepth() < bTransform->GetDepth());
			}
		}

		CHECK_INFO(false, TXT("Somehow one of the render components are no longer a component for a Planar Transform despite validating the list."));
		return true;
	});

	for (UINT_TYPE i = 0; i < ComponentsToDraw.size(); ++i)
	{
		if (PlanarTransform* transform = dynamic_cast<PlanarTransform*>(ComponentsToDraw.at(i)->GetOwner()))
		{
			transform->ComputeAbsTransform();
			transform->MarkProjectionDataDirty();

			if (ComponentsToDraw.at(i)->IsRelevant(renderTarget, cam))
			{
				ComponentsToDraw.at(i)->Render(renderTarget, cam);
			}
		}
		else
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("A RenderComponent (%s) is registered to a PlanarDrawLayer (%s), but its owner does not have a PlanarTransformation."), ComponentsToDraw.at(i)->ToString(), ToString());
		}
	}
}

void PlanarDrawLayer::PurgeExpiredComponents ()
{
	//Remove empty/destroyed components from list
	UINT_TYPE i = 0;
	while (i < ComponentsToDraw.size())
	{
		if (!VALID_OBJECT(ComponentsToDraw.at(i)))
		{
			ComponentsToDraw.erase(ComponentsToDraw.begin() + i);
			continue;
		}

		//Make sure the components are still a component of a planar transform object (this could happen if they changed owners after registration).
		if (dynamic_cast<const PlanarTransform*>(ComponentsToDraw.at(i)->GetOwner()) == nullptr)
		{
			ComponentsToDraw.erase(ComponentsToDraw.begin() + i);
			continue;
		}

		++i;
	}
}

void PlanarDrawLayer::HandleGarbageCollection ()
{
	PurgeExpiredComponents();
}
SD_END