/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderTextureTester.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
#ifdef DEBUG_MODE

IMPLEMENT_CLASS(SD, RenderTextureTester, SD, Entity)

void RenderTextureTester::InitProps ()
{
	Super::InitProps();

	SplitScreenWindow = nullptr;
	WindowDrawLayer = nullptr;
	FrameSize = Vector2(384.f, 256.f);
	RenderTextureOwner = nullptr;
	TopRenderTexture = nullptr;
	BottomRenderTexture = nullptr;
	TopCamera = nullptr;
	BottomCamera = nullptr;
}

void RenderTextureTester::BeginObject ()
{
	Super::BeginObject();

	const INT width = FrameSize.X.ToINT();
	const INT height = FrameSize.Y.ToINT();
	const FLOAT frameRate = 0.033333f; //~30 frames per second

	SplitScreenWindow = Window::CreateObject();
	SplitScreenWindow->SetResource(new sf::RenderWindow(sf::VideoMode(width.ToUnsignedInt32(), height.ToUnsignedInt32() * 2), "Render Texture Test"));
	SplitScreenWindow->GetTick()->SetTickInterval(frameRate);
	SplitScreenWindow->GetPollingTickComponent()->SetTickInterval(frameRate);
	SplitScreenWindow->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, RenderTextureTester, HandleWindowEvent, void, const sf::Event&));

	//Setup draw layer for split screen window
	WindowDrawLayer = PlanarDrawLayer::CreateObject();
	SplitScreenWindow->RegisterDrawLayer(WindowDrawLayer.Get());

	//Setup camera
	TopCamera = PlanarCamera::CreateObject();
	TopCamera->SetViewExtents(FrameSize);
	BottomCamera = PlanarCamera::CreateObject();
	BottomCamera->SetViewExtents(FrameSize);
	BottomCamera->SetZoom(0.5f);

	GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
	CHECK(graphicsEngine != nullptr)
	Window* mainWindow = graphicsEngine->GetPrimaryWindow();
	CHECK(mainWindow != nullptr)

	//Setup RenderTextures
	TopRenderTexture = RenderTexture::CreateObject();
	TopRenderTexture->CreateResource(width, height, false);
	TopRenderTexture->GetTick()->SetTickInterval(frameRate);
	TopRenderTexture->SetDrawLayersFrom(mainWindow);
	TopRenderTexture->SetDestroyDrawLayersOnDestruction(false); //Don't destroy draw layers since the mainWindow still uses them
	TopRenderTexture->AssignCamera(PlanarDrawLayer::CANVAS_LAYER_PRIORITY, TopCamera.Get());

	BottomRenderTexture = RenderTexture::CreateObject();
	BottomRenderTexture->CreateResource(width, height, false);
	BottomRenderTexture->GetTick()->SetTickInterval(frameRate);
	BottomRenderTexture->SetDrawLayersFrom(mainWindow);
	BottomRenderTexture->SetDestroyDrawLayersOnDestruction(false); //Don't destroy draw layers since the mainWindow still uses them
	BottomRenderTexture->AssignCamera(PlanarDrawLayer::CANVAS_LAYER_PRIORITY, BottomCamera.Get());

	//Setup Sprite transforms
	RenderTextureOwner = Entity::CreateObject();
	PlanarTransformComponent* topTransform = PlanarTransformComponent::CreateObject();
	if (RenderTextureOwner->AddComponent(topTransform))
	{
		topTransform->SetSize(Vector2(1.f, 0.5f));
		topTransform->SetPosition(Vector2::ZeroVector);
	}
	else
	{
		//These aren't DPointers, must be explicitly set to null if AddComponent failed.
		topTransform = nullptr;
	}
	
	PlanarTransformComponent* bottomTransform = PlanarTransformComponent::CreateObject();
	if (RenderTextureOwner->AddComponent(bottomTransform))
	{
		bottomTransform->SetSize(Vector2(1.f, 0.5f));
		bottomTransform->SetPosition(Vector2(0.f, 0.5f));
	}
	else
	{
		//These aren't DPointers, must be explicitly set to null if AddComponent failed.
		bottomTransform = nullptr;
	}

	CHECK(topTransform != nullptr && bottomTransform != nullptr)

	//Setup Sprite components that'll be rendering a RenderTexture on the external window.
	SpriteComponent* topSprite = SpriteComponent::CreateObject();
	if (topTransform->AddComponent(topSprite))
	{
		WindowDrawLayer->RegisterSingleComponent(topSprite);
		topSprite->SetSpriteTexture(&TopRenderTexture->GetResource()->getTexture());
	}

	SpriteComponent* bottomSprite = SpriteComponent::CreateObject();
	if (bottomTransform->AddComponent(bottomSprite))
	{
		WindowDrawLayer->RegisterSingleComponent(bottomSprite);
		bottomSprite->SetSpriteTexture(&BottomRenderTexture->GetResource()->getTexture());
	}

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)
	Texture* whiteFrame = localTexturePool->FindTexture(TXT("WhiteFrame"), true);
	CHECK(whiteFrame != nullptr)

	//Setup frames rendered in main window
	//Top frame
	PlanarDrawLayer* canvasLayer = graphicsEngine->GetCanvasDrawLayer();
	CHECK(TopCamera != nullptr && canvasLayer != nullptr)
	{
		TopCamera->SetPosition(FrameSize * 0.5f); //Place the view in top left corner by placing the camera at center of frame
		TopCamera->SetSize(FrameSize);

		PlanarTransformComponent* cameraSpriteTransform = PlanarTransformComponent::CreateObject();
		if (TopCamera->AddComponent(cameraSpriteTransform))
		{
			//The border sprite represents the view boundaries the camera can see from.
			cameraSpriteTransform->SetPosition(FrameSize * -0.5f);

			SpriteComponent* borderSprite = SpriteComponent::CreateObject();
			if (cameraSpriteTransform->AddComponent(borderSprite))
			{
				borderSprite->SetSpriteTexture(whiteFrame);
				canvasLayer->RegisterSingleComponent(borderSprite);

				Shader* colorShift = Shader::CreateShader(TXT("ColorShift.frag"), sf::Shader::Type::Fragment);
				if (colorShift != nullptr)
				{
					colorShift->SFShader->setUniform("ColorMultiplier", sf::Glsl::Vec4(1.f, 0.f, 0.f, 1.f));
					borderSprite->SetSpriteShader(colorShift);
				}
			}
		}
	}

	//Bottom frame
	CHECK(BottomCamera != nullptr)
	{
		BottomCamera->SetPosition(FrameSize * Vector2(1.5f, 1.5f));
		BottomCamera->SetSize(FrameSize);

		PlanarTransformComponent* cameraSpriteTransform = PlanarTransformComponent::CreateObject();
		if (BottomCamera->AddComponent(cameraSpriteTransform))
		{
			cameraSpriteTransform->SetPosition(FrameSize * -0.5f);

			SpriteComponent* borderSprite = SpriteComponent::CreateObject();
			if (cameraSpriteTransform->AddComponent(borderSprite))
			{
				borderSprite->SetSpriteTexture(whiteFrame);
				canvasLayer->RegisterSingleComponent(borderSprite);

				Shader* colorShift = Shader::CreateShader(TXT("ColorShift.frag"), sf::Shader::Type::Fragment);
				if (colorShift != nullptr)
				{
					colorShift->SFShader->setUniform("ColorMultiplier", sf::Glsl::Vec4(0.f, 0.f, 1.f, 1.f));
					borderSprite->SetSpriteShader(colorShift);
				}
			}
		}
	}
}

void RenderTextureTester::Destroyed ()
{
	if (SplitScreenWindow != nullptr)
	{
		//Don't invoke RenderTextureTester::Destroy when the window is destroyed
		SplitScreenWindow->UnregisterPollingDelegate(SDFUNCTION_1PARAM(this, RenderTextureTester, HandleWindowEvent, void, const sf::Event&));
		SplitScreenWindow->Destroy();
	}

	//No need to destroy DrawLayer since the Window is configured to destroy DrawLayers on Destruction.

	if (TopRenderTexture != nullptr)
	{
		TopRenderTexture->Destroy();
	}

	if (BottomRenderTexture != nullptr)
	{
		BottomRenderTexture->Destroy();
	}

	if (RenderTextureOwner != nullptr)
	{
		RenderTextureOwner->Destroy();
	}

	//No need to destroy cameras since the RenderTextures destroyed them.

	//No need to destroy frames since they will be automatically destroyed since they are sub components of this Entity.

	Super::Destroyed();
}

void RenderTextureTester::HandleWindowEvent (const sf::Event& evnt)
{
	if (evnt.type == sf::Event::Closed)
	{
		//The window is already marked for destruction.  Clear nullptr before RenderTextureTester::Destroyed calls destroy on a destroyed object.
		SplitScreenWindow = nullptr;
		Destroy();
	}
}

#endif
SD_END