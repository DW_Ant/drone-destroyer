/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TexturePool.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, TexturePool, SD, ResourcePool)
IMPLEMENT_RESOURCE_POOL(Texture)

void TexturePool::BeginObject ()
{
	Super::BeginObject();

	RegisterResourcePool();
}

void TexturePool::ReleaseResources ()
{
	for (UINT_TYPE i = 0; i < ImportedResources.size(); i++)
	{
		ImportedResources.at(i).Texture->Destroy();
	}

	ContainerUtils::Empty(ImportedResources);
}

void TexturePool::Destroyed ()
{
	RemoveResourcePool();

	Super::Destroyed();
}

Texture* TexturePool::CreateAndImportTexture (const PrimitiveFileAttributes& file, const DString& textureName)
{
	if (!IsValidName(textureName))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to create texture since the specified name (%s) is either invalid or taken."), textureName);
		return nullptr;
	}

	sf::Texture* newTexture = new sf::Texture();
	{
		SfmlOutputStream sfmlOutput;

		//Import texture
		if (!newTexture->loadFromFile(file.GetName(true, true).ToCString()))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to create texture.  Could not load %s.  %s"), file.GetName(true, true), sfmlOutput.ReadOutput());
			delete newTexture;
			return nullptr;
		}
	}

	Texture* result = Texture::CreateObject();
	if (result == nullptr)
	{
		delete newTexture;
		return nullptr;
	}

	result->SetResource(newTexture);

#ifdef REQPOWEROF2
	INT width;
	INT height;
	result->GetDimensions(OUT width, OUT height);

	//Typically sprites are automatically padded so that they are in powers of 2.
	if (!Utils::IsPowerOf2(width) || !Utils::IsPowerOf2(height))
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to import texture %s.  The dimensions of that image is not in powers of two (%sx%s)"), file.GetName(true, true), width, height);
		result->Destroy();
		return nullptr;
	}
#endif

	SetTextureAttributes(result, textureName);

	if (!ImportTexture(result, textureName))
	{
		//Failed to import.  destroy texture
		result->Destroy();
		return nullptr;
	}

	return result;
}

void TexturePool::SetTextureAttributes (Texture* target, const DString& textureName)
{
	target->SetTextureName(textureName);
	target->SetRepeat(true); //By default:  textures should be repeating since smeared textures are typically not desired.
}
SD_END