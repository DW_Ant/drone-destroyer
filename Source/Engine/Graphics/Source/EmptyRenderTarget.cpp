/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EmptyRenderTarget.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, EmptyRenderTarget, SD, RenderTarget)

void EmptyRenderTarget::InitProps ()
{
	Super::InitProps();

	Size = Vector2::ZeroVector;
}

void EmptyRenderTarget::Reset ()
{
	//Noop
}

void EmptyRenderTarget::Draw (const sf::Drawable& drawable, const sf::RenderStates& renderState)
{
	//Noop
}

void EmptyRenderTarget::Display ()
{
	//Noop
}

void EmptyRenderTarget::GetSize (INT& outWidth, INT& outHeight) const
{
	outWidth = Size.X.ToINT();
	outHeight = Size.Y.ToINT();
}

Vector2 EmptyRenderTarget::GetSize () const
{
	return Size;
}

void EmptyRenderTarget::SetSize (const Vector2& newSize)
{
	Size = newSize;
}
SD_END

#endif