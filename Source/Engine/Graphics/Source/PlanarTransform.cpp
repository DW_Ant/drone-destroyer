/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlanarTransform.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
PlanarTransform::PlanarTransform () : Transformation(),
	UnitType(eUnitType::UT_Pixels),
	Position(Vector2::ZeroVector),
	Depth(0.f),
	Size(1.f, 1.f),
	Pivot(Vector2::ZeroVector),
	BoundingBoxTranslation(0.f, 0.f),
	EnableFractionScaling(true),
	RelativeTo(nullptr),
	AnchorLeftDist(-1.f),
	AnchorTopDist(-1.f),
	AnchorRightDist(-1.f),
	AnchorBottomDist(-1.f),
	AnchorsChanged(false),
	DependsOnRenderTargetSize(false),
	RootRenderTarget(nullptr),
	ExternalCamera(nullptr)
{

}

PlanarTransform::PlanarTransform (const Vector2& inPosition, const Vector2& inSize, const Vector2& inPivot, FLOAT inDepth, const PlanarTransform* inRelativeTo) : Transformation(),
	UnitType(eUnitType::UT_Pixels),
	Position(inPosition),
	Depth(inDepth),
	Size(inSize),
	Pivot(inPivot),
	BoundingBoxTranslation(0.f, 0.f),
	EnableFractionScaling(true),
	AnchorLeftDist(-1.f),
	AnchorTopDist(-1.f),
	AnchorRightDist(-1.f),
	AnchorBottomDist(-1.f),
	AnchorsChanged(false),
	DependsOnRenderTargetSize(false),
	RootRenderTarget(nullptr),
	ExternalCamera(nullptr)
{
	SetRelativeTo(inRelativeTo);
}

PlanarTransform::PlanarTransform (const PlanarTransform& copyObj) : Transformation(copyObj),
	UnitType(copyObj.UnitType),
	Position(copyObj.Position),
	Depth(copyObj.Depth),
	Size(copyObj.Size),
	Pivot(copyObj.Pivot),
	BoundingBoxTranslation(copyObj.BoundingBoxTranslation),
	EnableFractionScaling(copyObj.EnableFractionScaling),
	AnchorLeftDist(copyObj.AnchorLeftDist),
	AnchorTopDist(copyObj.AnchorTopDist),
	AnchorRightDist(copyObj.AnchorRightDist),
	AnchorBottomDist(copyObj.AnchorBottomDist),
	AnchorsChanged(copyObj.AnchorsChanged),
	DependsOnRenderTargetSize(copyObj.DependsOnRenderTargetSize),
	RootRenderTarget(nullptr),
	ExternalCamera(nullptr)
{
	SetRelativeTo(copyObj.RelativeTo);
}

PlanarTransform::~PlanarTransform ()
{

}

bool PlanarTransform::IsWithinView (const RenderTarget* renderTarget, const Camera* camera) const
{
	const SScreenProjectionData& projectionData = GetProjectionData(renderTarget, camera);

	sf::Vector2f finalSize = projectionData.GetFinalSize();
	Vector2 renderSize = renderTarget->GetSize();
	return (projectionData.Position.x + finalSize.x >= 0.f && projectionData.Position.y + finalSize.y >= 0.f && projectionData.Position.x <= renderSize.X && projectionData.Position.y <= renderSize.Y);
}

void PlanarTransform::CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const
{
	CHECK(target != nullptr)
	if (RelativeTo == nullptr)
	{
		RootRenderTarget = target;
		ExternalCamera = cam;
	}

	Vector2 relPos(ReadCachedAbsPosition());
	outProjectionData.BaseSize = Vector2::SDtoSFML(ReadCachedAbsSize());
	outProjectionData.Scale = sf::Vector2f(1.f, 1.f);

	const PlanarCamera* planarCam = dynamic_cast<const PlanarCamera*>(cam);
	if (planarCam != nullptr)
	{
		const Vector2 halfExtents = planarCam->ReadViewExtents() * 0.5f;

		relPos -= planarCam->ReadCachedAbsPosition();
		relPos += halfExtents;

		if (planarCam->GetZoom() != 1.f)
		{
			const Vector2 distFromCenter = (ReadCachedAbsPosition() - planarCam->ReadCachedAbsPosition());
			relPos += (distFromCenter * planarCam->GetZoom()) - distFromCenter;

			outProjectionData.Scale *= planarCam->GetZoom().Value;
		}
	}
	else if (cam != nullptr) //PlanarCamera cast failed, but a camera is specified.
	{
		//Give warning if there is a camera but it's not using the correct coordinate space
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("The camera associated with a PlanarTransform is not a PlanarCamera.  The transform will assume that the coordinates are in screen space."));
	}

	if (UnitType == eUnitType::UT_Centimeters)
	{
		//Convert from cm to pixels
		const FLOAT cmsPerInch = 2.54f;
		relPos *= (GraphicsEngineComponent::GetDpiScale().ToFLOAT() * cmsPerInch);
		outProjectionData.Scale *= (GraphicsEngineComponent::GetDpiScale().ToFLOAT() * cmsPerInch).Value;
	}

	//Convert to SFML and assign its results to the out parameters.
	outProjectionData.Position = Vector2::SDtoSFML(relPos);
	outProjectionData.Rotation = 0.f;
	outProjectionData.Pivot = Vector2::SDtoSFML(ReadPivot());
}

void PlanarTransform::ComputeAbsTransform ()
{
	std::vector<const PlanarTransform*> transformChain;
	for (const PlanarTransform* curTransform = this; curTransform != nullptr; curTransform = curTransform->RelativeTo)
	{
		transformChain.push_back(curTransform);
	}

	//If any axis of the rootTransform is less than or equal to 1, then assume it's scaled relative to viewport size
	//We ignore subcomponents since we can compute their abs size if the root Entity does not depend on viewport size.
	//We don't consider position since it doesn't quite make sense to specify absolute position if it's scaled relative to the viewport.
	const PlanarTransform* rootTransform = ContainerUtils::GetLast(transformChain);
	if (rootTransform == this)
	{
		DependsOnRenderTargetSize = ComputeDependsOnRenderTargetSize();
	}
	else
	{
		DependsOnRenderTargetSize = rootTransform->DependsOnRenderTargetSize;
	}

	Vector2 accumulatedPosition = Vector2::ZeroVector;

	//Fallback to 1,1 in case the root render target is not yet assigned. This may be true for Entities such as the PlanarCamera that has size set to 1.
	Vector2 compoundedSize = (DependsOnRenderTargetSize && rootTransform->RootRenderTarget != nullptr) ? rootTransform->RootRenderTarget->GetSize() : Vector2(1.f, 1.f);
	while (transformChain.size() > 0)
	{
		const PlanarTransform* curRelative = transformChain.at(transformChain.size() - 1);

		accumulatedPosition.X += (FLOAT::Abs(curRelative->Position.X) <= 1 && EnableFractionScaling) ? compoundedSize.X * curRelative->Position.X : curRelative->Position.X;
		accumulatedPosition.Y += (FLOAT::Abs(curRelative->Position.Y) <= 1 && EnableFractionScaling) ? compoundedSize.Y * curRelative->Position.Y : curRelative->Position.Y;

		compoundedSize.X = (FLOAT::Abs(curRelative->Size.X) <= 1 && EnableFractionScaling) ? compoundedSize.X * curRelative->Size.X : curRelative->Size.X;
		compoundedSize.Y = (FLOAT::Abs(curRelative->Size.Y) <= 1 && EnableFractionScaling) ? compoundedSize.Y * curRelative->Size.Y : curRelative->Size.Y;

		transformChain.pop_back();
	}

	Vector2 parentSize(1.f, 1.f);
	if (rootTransform == this)
	{
		if (DependsOnRenderTargetSize && RootRenderTarget != nullptr) //Only update parent size when it matters (save from unecessary call to HandleAbsTransformChange).
		{
			parentSize = RootRenderTarget->GetSize();
		}
	}
	else if (RelativeTo != nullptr)
	{
		parentSize = RelativeTo->CachedSize;
	}

	if (CachedPosition != accumulatedPosition || CachedSize != compoundedSize || parentSize != ParentAbsSize)
	{
		CachedPosition = accumulatedPosition;
		CachedSize = compoundedSize;
		ParentAbsSize = parentSize;
		HandleAbsTransformChange();
	}

	//Force update if they haven't already. This could happen if anchors have changed, but size & position didn't change.
	if (AnchorsChanged)
	{
		ApplyAnchors();
	}
}

bool PlanarTransform::IsWithinBounds (const Vector2& point) const
{
	return (point.X >= CachedPosition.X + BoundingBoxTranslation.X - Pivot.X && point.Y >= CachedPosition.Y + BoundingBoxTranslation.Y - Pivot.Y &&
		point.X <= ((CachedPosition.X + CachedSize.X + BoundingBoxTranslation.X) - Pivot.X) && point.Y <= ((CachedPosition.Y + CachedSize.Y + BoundingBoxTranslation.Y) - Pivot.Y));
}

Rectangle<FLOAT> PlanarTransform::GetBoundingBox () const
{
	return Rectangle<FLOAT>(CachedPosition.X + BoundingBoxTranslation.X - Pivot.X, CachedPosition.Y + BoundingBoxTranslation.Y - Pivot.Y,
		CachedPosition.X + CachedSize.X + BoundingBoxTranslation.X - Pivot.X, CachedPosition.Y + CachedSize.Y + BoundingBoxTranslation.Y - Pivot.Y, Rectangle<FLOAT>::CS_XRightYDown);
}

void PlanarTransform::ApplyAnchors ()
{
	AnchorsChanged = false;
	Vector2 parentPos = (RelativeTo != nullptr) ? RelativeTo->GetCachedAbsPosition() : Vector2::ZeroVector;

	//Calculate horizontal position
	if (AnchorLeftDist >= 0.f)
	{
		Position.X = AnchorLeftDist;
		CachedPosition.X = parentPos.X + Position.X;
	}

	if (AnchorRightDist >= 0.f)
	{
		if (AnchorLeftDist >= 0.f)
		{
			//Need to adjust size instead of position since the left border is also locked in place.
			Size.X = ParentAbsSize.X - AnchorLeftDist - AnchorRightDist;
			CachedSize.X = Size.X;
		}
		else
		{
			Position.X = ParentAbsSize.X - CachedSize.X - AnchorRightDist;
			CachedPosition.X = parentPos.X + Position.X;
		}
	}

	//Calculate vertical position
	if (AnchorTopDist >= 0.f)
	{
		Position.Y = AnchorTopDist;
		CachedPosition.Y = parentPos.Y + Position.Y;
	}

	if (AnchorBottomDist >= 0.f)
	{
		if (AnchorTopDist >= 0.f)
		{
			//Need to adjust size instead of position since the top border is also locked in place.
			Size.Y = ParentAbsSize.Y - AnchorTopDist - AnchorBottomDist;
			CachedSize.Y = Size.Y;
		}
		else
		{
			Position.Y = ParentAbsSize.Y - CachedSize.Y - AnchorBottomDist;
			CachedPosition.Y = parentPos.Y + Position.Y;
		}
	}
}

void PlanarTransform::CopyPlanarTransform (const PlanarTransform* copyFrom)
{
	SetPosition(copyFrom->Position);
	SetDepth(copyFrom->Depth);
	SetSize(copyFrom->Size);
	SetPivot(copyFrom->Pivot);
	SetEnableFractionScaling(copyFrom->EnableFractionScaling);
	SetAnchorLeft(copyFrom->AnchorLeftDist);
	SetAnchorTop(copyFrom->AnchorTopDist);
	SetAnchorRight(copyFrom->AnchorRightDist);
	SetAnchorBottom(copyFrom->AnchorBottomDist);
}

void PlanarTransform::SetPosition (const Vector2& newPosition)
{
	Position = newPosition;
}

void PlanarTransform::SetPosition (FLOAT x, FLOAT y)
{
	Position.X = x;
	Position.Y = y;
}

void PlanarTransform::SetDepth (FLOAT newDepth)
{
	Depth = newDepth;
}

void PlanarTransform::SetSize (const Vector2& newSize)
{
	Size = newSize;
}

void PlanarTransform::SetSize (FLOAT width, FLOAT height)
{
	Size.X = width;
	Size.Y = height;
}

void PlanarTransform::SetPivot (const Vector2& newPivot)
{
	Pivot = newPivot;
}

void PlanarTransform::SetPivot (FLOAT x, FLOAT y)
{
	Pivot.X = x;
	Pivot.Y = y;
}

void PlanarTransform::SetBoundingBoxTranslation (const Vector2& newBoundingBoxTranslation)
{
	BoundingBoxTranslation = newBoundingBoxTranslation;
}

void PlanarTransform::SetEnableFractionScaling (bool newEnableFractionScaling)
{
	EnableFractionScaling = newEnableFractionScaling;
}

void PlanarTransform::SetRelativeTo (const PlanarTransform* newRelativeTo)
{
#if ENABLE_COMPLEX_CHECKING
	//Ensure this isn't setting a circular relativeTo
	const PlanarTransform* relativeTo = newRelativeTo;
	while (relativeTo != nullptr)
	{
		CHECK_INFO(relativeTo != this, "Circular relative transformation detected.  Cannot set a PlanarTransform object relative to itself.")
		relativeTo = relativeTo->GetRelativeTo();
	}
#endif

	RelativeTo = newRelativeTo;

	//This pointer should only be set on Root Transforms. If SetRelativeTo(nullptr) is called, the root transform will be set on next screen projection.
	RootRenderTarget = nullptr;
	ExternalCamera = nullptr;
}

void PlanarTransform::SetAnchorLeft (FLOAT leftDist)
{
	AnchorsChanged |= (leftDist != AnchorLeftDist);
	AnchorLeftDist = leftDist;
}

void PlanarTransform::SetAnchorTop (FLOAT topDist)
{
	AnchorsChanged |= (topDist != AnchorTopDist);
	AnchorTopDist = topDist;
}

void PlanarTransform::SetAnchorRight (FLOAT rightDist)
{
	AnchorsChanged |= (rightDist != AnchorRightDist);
	AnchorRightDist = rightDist;
}

void PlanarTransform::SetAnchorBottom (FLOAT bottomDist)
{
	AnchorsChanged |= (bottomDist != AnchorBottomDist);
	AnchorBottomDist = bottomDist;
}

const RenderTarget* PlanarTransform::FindRootRenderTarget () const
{
	for (const PlanarTransform* transform = this; transform != nullptr; transform = transform->GetRelativeTo())
	{
		if (transform->RootRenderTarget != nullptr)
		{
			return transform->RootRenderTarget.Get();
		}
	}

	return nullptr;
}

const Camera* PlanarTransform::FindExternalCamera () const
{
	for (const PlanarTransform* transform = this; transform != nullptr; transform = transform->GetRelativeTo())
	{
		if (transform->ExternalCamera != nullptr)
		{
			return transform->ExternalCamera.Get();
		}
	}

	return nullptr;
}

bool PlanarTransform::ComputeDependsOnRenderTargetSize () const
{
	//This function should only be called on root transforms for performance reasons.
	CHECK(RelativeTo == nullptr)

	//Check size and position
	bool result = (EnableFractionScaling &&
			(FLOAT::Abs(Position.X) <= 1.f || FLOAT::Abs(Position.Y) <= 1.f ||
			FLOAT::Abs(Size.X) <= 1.f || FLOAT::Abs(Size.Y) <= 1.f));

	if (!result)
	{
		//Check anchors
		result = (AnchorTopDist >= 0.f || AnchorRightDist >= 0.f || AnchorBottomDist >= 0.f || AnchorLeftDist >= 0);
	}

	return result;
}

void PlanarTransform::HandleAbsTransformChange ()
{
	ApplyAnchors();
}
SD_END