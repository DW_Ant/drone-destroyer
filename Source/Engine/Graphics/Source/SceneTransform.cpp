/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SceneTransform.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
SceneTransform::SceneTransform () : Transformation(),
	AutoComputeAbsTransform(true),
	Translation(Vector3::ZeroVector),
	Scale(1.f, 1.f, 1.f),
	GlobalScale(1.f),
	Rotation(),
	PivotPoint(Vector3::ZeroVector),
	AbsTranslation(Vector3::ZeroVector),
	AbsScale(1.f, 1.f, 1.f),
	AbsRotation(),
	AbsPivotPoint(Vector3::ZeroVector),
	RelativeTo(nullptr)
{
	
}

SceneTransform::SceneTransform (const Vector3& inTranslation, const Vector3& inScale, FLOAT inGlobalScale, Rotator inRotation, const Vector3& inPivotPoint, const SceneTransform* inRelativeTo) : Transformation(),
	AutoComputeAbsTransform(true),
	Translation(inTranslation),
	Scale(inScale),
	GlobalScale(inGlobalScale),
	Rotation(inRotation),
	PivotPoint(inPivotPoint)
{
	SetRelativeTo(inRelativeTo);
	CalculateAbsoluteTransform();
}

SceneTransform::SceneTransform (const SceneTransform& copyData) : Transformation(copyData),
	AutoComputeAbsTransform(copyData.AutoComputeAbsTransform),
	Translation(copyData.Translation),
	Scale(copyData.Scale),
	GlobalScale(copyData.GlobalScale),
	Rotation(copyData.Rotation),
	PivotPoint(copyData.PivotPoint),
	AbsTranslation(copyData.AbsTranslation),
	AbsScale(copyData.AbsScale),
	AbsRotation(copyData.AbsRotation),
	AbsPivotPoint(copyData.AbsPivotPoint),
	RelativeTo(copyData.RelativeTo)
{

}

SceneTransform::~SceneTransform ()
{
	//Noop
}

bool SceneTransform::IsWithinView (const RenderTarget* renderTarget, const Camera* camera)  const
{
	//TODO:  Implement SceneTransform::IsWithinView
	return true;
}

void SceneTransform::CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const
{
	const SceneCamera* sceneCam = dynamic_cast<const SceneCamera*>(cam);
	if (sceneCam == nullptr)
	{
		GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to compute SceneTransform's Screen Space position since it doesn't know its relative position to a camera using a different coordinate system."));

		//Default to identity since we don't know which axis converges where in screen space.
		outProjectionData.Position = sf::Vector2f(0.f, 0.f);
		outProjectionData.BaseSize = sf::Vector2f(1.f, 1.f);
		outProjectionData.Scale = sf::Vector2f(1.f, 1.f);
		outProjectionData.Rotation = 0.f;
		outProjectionData.Pivot = sf::Vector2f(0.f, 0.f);
		return;
	}

	//The cameras will compute the relative coordinates due to various projections.
	sceneCam->ProjectToScreenCoordinates(target->GetSize(), *this, OUT outProjectionData);
}

void SceneTransform::ResetTransform (bool bResetPivot)
{
	Translation = Vector3::ZeroVector;
	Scale = Vector3(1.f, 1.f, 1.f);
	GlobalScale = 1.f;
	Rotation = Rotator::ZERO_ROTATOR;

	if (bResetPivot)
	{
		PivotPoint = Vector3::ZeroVector;
	}
}

TransformMatrix SceneTransform::ToTransformMatrix () const
{
	TransformMatrix transformMatrix; //Generate identity matrix
	ApplyTransformTo(OUT transformMatrix);

	return transformMatrix;
}

void SceneTransform::ApplyTransformTo (TransformMatrix& outMatrix) const
{
	outMatrix.Translate(Translation);
	outMatrix.Scale(Scale * GlobalScale);
	outMatrix.Rotate(Rotation);
}

void SceneTransform::CalculateAbsoluteTransform () const
{
	std::vector<const SceneTransform*> transformChain;
	transformChain.push_back(this);

	const SceneTransform* curParent = GetRelativeTo();
	while (curParent != nullptr)
	{
		transformChain.push_back(curParent);
		curParent = curParent->GetRelativeTo();
	}

	AbsTransform = TransformMatrix::IDENTITY_TRANSFORM; // transformChain.at(transformChain.size() - 1)->ToTransformMatrix();
	AbsPivotPoint = PivotPoint;
	while (!ContainerUtils::IsEmpty(transformChain))
	{
		AbsTransform *= ContainerUtils::GetLast(transformChain)->ToTransformMatrix();
		AbsPivotPoint += ContainerUtils::GetLast(transformChain)->PivotPoint;
		transformChain.pop_back();
	}

	CHECK(AbsTransform.IsValid())
	AbsTransform.CalcTransformAttributes(OUT AbsTranslation, OUT AbsScale, OUT AbsRotation);
}

void SceneTransform::SetTranslation (const Vector3& newTranslation)
{
	Translation = newTranslation;
}

void SceneTransform::SetScale (const Vector3& newScale)
{
	Scale = newScale;
}

void SceneTransform::SetGlobalScale (FLOAT newGlobalScale)
{
	GlobalScale = newGlobalScale;
}

void SceneTransform::SetRotation (const Rotator& newRotation)
{
	Rotation = newRotation;
}

void SceneTransform::SetPivotPoint (const Vector3& newPivotPoint)
{
	PivotPoint = newPivotPoint;
}

void SceneTransform::SetRelativeTo (const SceneTransform* newRelativeTo)
{
#if ENABLE_COMPLEX_CHECKING
	//Ensure this isn't setting a circular relativeTo
	const SceneTransform* relativeTo = newRelativeTo;
	while (relativeTo != nullptr)
	{
		CHECK_INFO(relativeTo != this, "Circular relative transformation detected.  Cannot set a Transformation object relative to itself.")

		relativeTo = relativeTo->GetRelativeTo();
	}
#endif

	RelativeTo = newRelativeTo;
}
SD_END