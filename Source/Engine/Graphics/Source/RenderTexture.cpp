/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderTexture.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, RenderTexture, SD, RenderTarget)

void RenderTexture::InitProps ()
{
	Super::InitProps();

	ResetColor = Color(0, 0, 0, 255);
	Resource = nullptr;
	LatestDrawCoordinates = Vector2::ZeroVector;

	CreatedResource = false;
}

void RenderTexture::BeginObject ()
{
	Super::BeginObject();

	Resource = new sf::RenderTexture();
}

void RenderTexture::Reset ()
{
	CHECK(Resource != nullptr)
	Resource->clear(ResetColor.Source);
}

void RenderTexture::Draw (const sf::Drawable& drawable, const sf::RenderStates& renderState)
{
	CHECK(Resource != nullptr)
	Resource->draw(drawable, renderState);
}

void RenderTexture::Display ()
{
	CHECK(Resource != nullptr)
	Resource->display();
}

Vector2 RenderTexture::GetWindowCoordinates () const
{
	return LatestDrawCoordinates;
}

void RenderTexture::GetSize (INT& outWidth, INT& outHeight) const
{
	CHECK(Resource != nullptr)
	sf::Vector2u textureSize = Resource->getTexture().getSize();
	outWidth = textureSize.x;
	outHeight = textureSize.y;
}

Vector2 RenderTexture::GetSize () const
{
	INT width;
	INT height;
	GetSize(OUT width, OUT height);

	return Vector2(width.ToFLOAT(), height.ToFLOAT());
}

void RenderTexture::Destroyed ()
{
	if (Resource != nullptr)
	{
		delete Resource;
		Resource = nullptr;
	}

	Super::Destroyed();
}

bool RenderTexture::CreateResource (INT width, INT height, bool hasDepthBuffer)
{
	CHECK_INFO(!CreatedResource, "Cannot create another resource on a RenderTexture that already has created one.")

	CreatedResource = Resource->create(width.ToUnsignedInt32(), height.ToUnsignedInt32(), hasDepthBuffer);
	return CreatedResource;
}

void RenderTexture::SetLatestDrawCoordinates (const Vector2& newLatestDrawCoordinates)
{
	LatestDrawCoordinates = newLatestDrawCoordinates;
}
SD_END