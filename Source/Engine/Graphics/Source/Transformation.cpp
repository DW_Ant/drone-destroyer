/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Transformation.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
Transformation::Transformation () :
	ProjectionDataDirty(true)
{

}

Transformation::Transformation (const Transformation& cpy) :
	ProjectionDataDirty(true)
{

}

Transformation::~Transformation ()
{

}

void Transformation::MarkProjectionDataDirty () const
{
	ProjectionDataDirty = true;
}

const Transformation::SScreenProjectionData& Transformation::GetProjectionData (const RenderTarget* renderTarget, const Camera* camera) const
{
	if (!ProjectionDataDirty)
	{
		return LatestProjectionData;
	}

	CalculateScreenSpaceTransform(renderTarget, camera, OUT LatestProjectionData);
	ProjectionDataDirty = false;

	return LatestProjectionData;
}
SD_END