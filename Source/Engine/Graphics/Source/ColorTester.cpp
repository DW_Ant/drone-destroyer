/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ColorTester.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, ColorTester, SD, Entity)

void ColorTester::InitProps ()
{
	Super::InitProps();

	ColorComponent = nullptr;
}

void ColorTester::BeginObject ()
{
	Super::BeginObject();

	SetSize(Vector2(256.f, 256.f));
	SetDepth(10.f);
}

bool ColorTester::BeginTest (UnitTester::EUnitTestFlags testFlags)
{
	ColorComponent = SolidColorRenderComponent::CreateObject();
	if (AddComponent(ColorComponent.Get()))
	{
		ColorComponent->SolidColor = sf::Color::Cyan;
	}
	else
	{
		UnitTester::TestLog(testFlags, TXT("Solid Color test failed.  Unable to attach SolidColorRenderComponent to the ColorTester."));
		Destroy();
		return false;
	}

	LifeSpanComponent* lifeSpan = LifeSpanComponent::CreateObject();
	if (!AddComponent(lifeSpan))
	{
		UnitTester::TestLog(testFlags, TXT("Solid Color test failed.  Unable to attach LifeSpanComponent to the ColorTester."));
		Destroy();
		return false;
	}

	PlanarDrawLayer* canvasLayer = GraphicsEngineComponent::Find()->GetCanvasDrawLayer();
	if (canvasLayer == nullptr)
	{
		UnitTester::TestLog(testFlags, TXT("Solid Color test failed.  There isn't a CanvasDrawLayer to register the component for rendering."));
		Destroy();
		return false;
		
	}

	canvasLayer->RegisterSingleComponent(ColorComponent.Get());
	lifeSpan->LifeSpan = 3.f;
	UnitTester::TestLog(testFlags, TXT("Solid Color test launched.  A cyan component created, and it will automatically perish in 3 seconds."));	
	return true;
}
SD_END

#endif