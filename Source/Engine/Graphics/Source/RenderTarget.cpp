/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderTarget.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(SD, RenderTarget, SD, Entity)

void RenderTarget::InitProps ()
{
	Super::InitProps();

	ParentRenderTarget = nullptr;
	Tick = nullptr;

	DestroyCamerasOnDestruction = true;
	DestroyDrawLayersOnDestruction = true;
}

void RenderTarget::BeginObject ()
{
	Super::BeginObject();

	Tick = TickComponent::CreateObject(TICK_GROUP_RENDER);
	if (AddComponent(Tick.Get()))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, RenderTarget, HandleTick, void, FLOAT));
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->RegisterPreGarbageCollectEvent(SDFUNCTION(this, RenderTarget, HandleGarbageCollection, void));
}

void RenderTarget::Destroyed ()
{
	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->RemovePreGarbageCollectEvent(SDFUNCTION(this, RenderTarget, HandleGarbageCollection, void));

	if (DestroyCamerasOnDestruction)
	{
		for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
		{
			if (DrawLayers.at(i).Cam != nullptr)
			{
				DrawLayers.at(i).Cam->Destroy();
				DrawLayers.at(i).Cam = nullptr;
			}
		}
	}

	if (DestroyDrawLayersOnDestruction)
	{
		for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
		{
			if (DrawLayers.at(i).Layer != nullptr)
			{
				DrawLayers.at(i).Layer->Destroy();
				DrawLayers.at(i).Layer = nullptr;
			}
		}
	}

	Super::Destroyed();
}

void RenderTarget::GenerateScene ()
{
	RemovePurgedDrawLayers();
	Reset();

	if (ParentRenderTarget != nullptr)
	{
		for (UINT_TYPE i = 0; i < ParentRenderTarget->DrawLayers.size(); ++i)
		{
			ParentRenderTarget->DrawLayers.at(i).Layer->RenderDrawLayer(this, ParentRenderTarget->DrawLayers.at(i).Cam);
		}
	}

	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		DrawLayers.at(i).Layer->RenderDrawLayer(this, DrawLayers.at(i).Cam);
	}

	Display();
}

void RenderTarget::RegisterDrawLayer (DrawLayer* drawLayer, Camera* associatedCamera)
{
	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		if (DrawLayers.at(i).Layer == drawLayer)
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Cannot register %s to %s more than once."), drawLayer->ToString(), ToString());
			return;
		}
	}

	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		if (DrawLayers.at(i).Layer->GetDrawPriority() > drawLayer->GetDrawPriority())
		{
			DrawLayers.insert(DrawLayers.begin() + i, SDrawLayerCamera(drawLayer, associatedCamera));
			return;
		}
	}

	//This draw layer has highest priority since there aren't any layers with greater priority values.  Add to end of list to make it render over others.
	DrawLayers.push_back(SDrawLayerCamera(drawLayer, associatedCamera));
}

void RenderTarget::RemoveDrawLayer (DrawLayer* target)
{
	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		if (DrawLayers.at(i).Layer == target)
		{
			DrawLayers.erase(DrawLayers.begin() + i);
			break;
		}
	}
}

bool RenderTarget::AssignCamera (DrawLayer* targetDrawLayer, Camera* newAssociatedCamera)
{
	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		if (DrawLayers.at(i).Layer == targetDrawLayer)
		{
			DrawLayers.at(i).Cam = newAssociatedCamera;
			return true;
		}
	}

	return false;
}

bool RenderTarget::AssignCamera (INT drawLayerPriority, Camera* newAssociatedCamera)
{
	for (UINT_TYPE i = 0; i < DrawLayers.size(); ++i)
	{
		if (DrawLayers.at(i).Layer->GetDrawPriority() == drawLayerPriority)
		{
			DrawLayers.at(i).Cam = newAssociatedCamera;
			return true;
		}
	}

	return false;
}

void RenderTarget::SetDrawLayersFrom (RenderTarget* targetToCopyFrom)
{
	CHECK(targetToCopyFrom != this) //Doesn't make sense to copy from self
	DrawLayers = targetToCopyFrom->DrawLayers;
}

Vector2 RenderTarget::GetWindowCoordinates () const
{
	return Vector2::ZeroVector;
}

void RenderTarget::SetParentRenderTarget (RenderTarget* newParentRenderTarget)
{
	ParentRenderTarget = newParentRenderTarget;
}

void RenderTarget::SetDestroyCamerasOnDestruction (bool newDestroyCamerasOnDestruction)
{
	DestroyCamerasOnDestruction = newDestroyCamerasOnDestruction;
}

void RenderTarget::SetDestroyDrawLayersOnDestruction (bool newDestroyDrawLayersOnDestruction)
{
	DestroyDrawLayersOnDestruction = newDestroyDrawLayersOnDestruction;
}

void RenderTarget::RemovePurgedDrawLayers ()
{
	UINT_TYPE i = 0;
	while (i < DrawLayers.size())
	{
		if (!VALID_OBJECT(DrawLayers.at(i).Layer))
		{
			DrawLayers.erase(DrawLayers.begin() + i);
			continue; //Don't increment i since the DrawLayer vector changed size.
		}
		else if (!VALID_OBJECT(DrawLayers.at(i).Cam))
		{
			//Camera was destroyed while a DrawLayer is viewing from that camera.
			//Simply set the cam to null so that the rendered objects are now in screen space.
			DrawLayers.at(i).Cam = nullptr;
		}

		++i;
	}
}

void RenderTarget::HandleTick (FLOAT deltaSec)
{
	GenerateScene();
}

void RenderTarget::HandleGarbageCollection ()
{
	RemovePurgedDrawLayers();
}
SD_END