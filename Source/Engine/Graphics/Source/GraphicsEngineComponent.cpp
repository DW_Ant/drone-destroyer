/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsEngineComponent.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(SD, GraphicsEngineComponent)

INT GraphicsEngineComponent::DEFAULT_DPI_SCALE = 96; //Window's default DPI scaling factor.
INT GraphicsEngineComponent::DpiScale = 96;

GraphicsEngineComponent::GraphicsEngineComponent () : Super()
{
	bTickingComponent = true;

	PrimaryWindow = nullptr;
	CanvasDrawLayer = nullptr;
	RenderComponentHashNumber = 0;
}

void GraphicsEngineComponent::RegisterObjectHash ()
{
	Super::RegisterObjectHash();

	RenderComponentHashNumber = Engine::FindEngine()->RegisterObjectHash(TXT("Render Component"));
}

void GraphicsEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

#ifdef PLATFORM_WINDOWS
	//Windows should be notified as soon as possible before anything is constructed that depends on DPI.
	static bool notifiedWindowsDpiAwareness = false;
	if (!notifiedWindowsDpiAwareness)
	{
		notifiedWindowsDpiAwareness = true;
		if (!SetProcessDpiAwarenessContext(DPI_AWARENESS_CONTEXT_SYSTEM_AWARE))
		{
			GraphicsLog.Log(LogCategory::LL_Warning, TXT("Unable to notify Windows of this application's DPI awareness. This may introduce scaling issues. Error code: %s"), DString::MakeString(GetLastError()));
			//Fallthrough - try to start the system anyways.
		}
	}
#endif

#ifdef DEBUG_MODE
	GraphicsLog.Log(LogCategory::LL_Log, TXT("Initializing Graphics Engine. . ."));
#endif

#ifdef DEBUG_MODE
	CHECK(sf::Shader::isAvailable())
#else
	if (!sf::Shader::isAvailable())
	{
		Engine::FindEngine()->FatalError(TXT("Your system does not support shaders."));
	}
#endif

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_RENDER, TICK_GROUP_PRIORITY_RENDER);

	//Create the various resource pools
	if (TexturePool::FindTexturePool() == nullptr)
	{
		TexturePool::CreateObject();
	}

	if (FontPool::FindFontPool() == nullptr)
	{
		FontPool::CreateObject();
	}

	//Setup the primary render target and its camera
	PrimaryWindow = Window::CreateObject();
	PrimaryWindow->SetResource(new sf::RenderWindow(sf::VideoMode(800, 600), PROJECT_NAME));
	PrimaryWindow->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, GraphicsEngineComponent, HandleMainWindowEvent, void, const sf::Event&));
	PrimaryWindow->SetDestroyCamerasOnDestruction(false);
	PrimaryWindow->SetDestroyDrawLayersOnDestruction(false);
	DpiScale = OS_GetSystemDpi(PrimaryWindow.Get());
	if (DpiScale <= 0)
	{
		DpiScale = DEFAULT_DPI_SCALE;
	}

	//Create base DrawLayers
	CanvasDrawLayer = PlanarDrawLayer::CreateObject();
	CanvasDrawLayer->SetDrawPriority(PlanarDrawLayer::CANVAS_LAYER_PRIORITY);
	PrimaryWindow->RegisterDrawLayer(CanvasDrawLayer.Get());
}

void GraphicsEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	ImportEngineTextures();
}

void GraphicsEngineComponent::ShutdownComponent ()
{
	if (PrimaryWindow != nullptr)
	{
		PrimaryWindow->Destroy();
	}

	if (CanvasDrawLayer != nullptr)
	{
		CanvasDrawLayer->Destroy();
	}

	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	if (localTexturePool != nullptr)
	{
		localTexturePool->Destroy();
	}

	FontPool* localFontPool = FontPool::FindFontPool();
	if (localFontPool != nullptr)
	{
		localFontPool->Destroy();
	}

	Super::ShutdownComponent();
}

INT GraphicsEngineComponent::GetDpiScale ()
{
	return DpiScale;
}

void GraphicsEngineComponent::ImportEngineTextures ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Engine") / TXT("Grids"), TXT("64-Grid.jpg")), TXT("Engine.Grids.64-Grid"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Engine") / TXT("Shapes"), TXT("DottedCircle.png")), TXT("Engine.Shapes.DottedCircle"));

	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY, TXT("DebuggingTexture.jpg")), TXT("DebuggingTexture"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY, TXT("WhiteFrame.png")), TXT("WhiteFrame"));
}

void GraphicsEngineComponent::HandleMainWindowEvent (const sf::Event& newEvent)
{
	if (newEvent.type == sf::Event::Closed)
	{
		Engine::FindEngine()->Shutdown();
	}
}
SD_END