/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderComponent.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

SD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(SD, RenderComponent, SD, EntityComponent)

void RenderComponent::InitProps ()
{
	Super::InitProps();

	OwnerTransform = nullptr;
}

void RenderComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	CHECK(objTemplate != this) //It's pointless for an object to copy properties from self.
}

bool RenderComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	//Owners must derive from a Transformation object.
	return (Super::CanBeAttachedTo(ownerCandidate) && dynamic_cast<Transformation*>(ownerCandidate) != nullptr);
}

unsigned int RenderComponent::CalculateHashID () const
{
	return (GraphicsEngineComponent::Find()->GetRenderComponentHashNumber());
}

void RenderComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwnerTransform = dynamic_cast<const Transformation*>(newOwner);
	CHECK(OwnerTransform != nullptr)
}

void RenderComponent::ComponentDetached ()
{
	OwnerTransform = nullptr;

	Super::ComponentDetached();
}

bool RenderComponent::IsRelevant (const RenderTarget* renderTarget, const Camera* camera) const
{
	if (!IsVisible())
	{
		return false;
	}

	Transformation* ownerTransform = dynamic_cast<Transformation*>(Owner.Get());
	CHECK(ownerTransform != nullptr)
	return ownerTransform->IsWithinView(renderTarget, camera);
}

const Transformation* RenderComponent::GetOwnerTransform () const
{
	return OwnerTransform;
}
SD_END