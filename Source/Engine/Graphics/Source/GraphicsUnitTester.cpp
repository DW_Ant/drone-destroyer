/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsUnitTester.cpp
=====================================================================
*/

#include "GraphicsClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, GraphicsUnitTester, SD, UnitTester)

bool GraphicsUnitTester::RunTests (EUnitTestFlags testFlags) const
{
	bool bResult = true;

	if ((testFlags & UTF_SmokeTest) > 0 && (testFlags & UTF_Automatic) > 0 && (testFlags & UTF_CanDetectErrors) > 0 && (testFlags & UTF_Synchronous) > 0)
	{
		bResult = TestPlanarTransforms(testFlags) && TestSceneTransforms(testFlags);
	}

	if (bResult && (testFlags & UTF_FeatureTest) > 0 && (testFlags & UTF_Manual) > 0 && (testFlags & UTF_Asynchronous) > 0 && (testFlags & UTF_NeverFails) > 0)
	{
		bResult = TestSolidColor(testFlags) && TestSprites(testFlags) && TestRenderTexture(testFlags);
	}
		
	return bResult;
}

bool GraphicsUnitTester::TestPlanarTransforms (UnitTester::EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Planar Transforms"));

	std::vector<Entity*> entitiesToDestroy;
	std::function<void()> clearEntities = [&entitiesToDestroy]
	{
		for (UINT_TYPE i = 0; i < entitiesToDestroy.size(); ++i)
		{
			entitiesToDestroy.at(i)->Destroy();
		}
		ContainerUtils::Empty(entitiesToDestroy);
	};

	TestLog(testFlags, TXT("Setting up a few PlanarTransformed Entities to be relative to each other."));

	//Need to setup a render target since PlanarTransform's root size may depend on the RenderTarget it's drawn to.
	EmptyRenderTarget* blankRender = EmptyRenderTarget::CreateObject();
	Vector2 rootSize(100.f, 100.f);
	blankRender->SetSize(rootSize);

	PlanarTransformComponent* rootEntity = PlanarTransformComponent::CreateObject();
	PlanarTransformComponent* childEntity = PlanarTransformComponent::CreateObject();
	PlanarTransformComponent* leafEntity = PlanarTransformComponent::CreateObject();
	childEntity->SetRelativeTo(rootEntity);
	leafEntity->SetRelativeTo(childEntity);

	{
		//The RootEntity's size depends on a viewport size. Call CalculateScreenSpaceTransform to pair up the root with a RenderTarget.
		//Normally this will automatically be called when it actually renders the Entity to a viewport. But for an isolated test, we force it here.
		Transformation::SScreenProjectionData unusedProjectionData;
		rootEntity->CalculateScreenSpaceTransform(blankRender, nullptr, OUT unusedProjectionData);
	}

	entitiesToDestroy.push_back(blankRender);
	entitiesToDestroy.push_back(rootEntity);
	entitiesToDestroy.push_back(childEntity);
	entitiesToDestroy.push_back(leafEntity);

	std::function<void()> computeAbsCoordinates = [&]
	{
		rootEntity->ComputeAbsTransform();
		childEntity->ComputeAbsTransform();
		leafEntity->ComputeAbsTransform();
	};

	SetTestCategory(testFlags, TXT("Unit Coordinates"));
	{
		TestLog(testFlags, TXT("Testing Unit Coordinates where leafEntity is relative to childEntity.  ChildEntity is relative to rootEntity."));
		rootEntity->SetPosition(Vector2(16.f, 16.f));
		rootEntity->SetSize(Vector2(64.f, 64.f));

		childEntity->SetPosition(Vector2(16.f, 32.f));
		childEntity->SetSize(Vector2(36.f, 48.f));

		leafEntity->SetPosition(Vector2(8.f, 12.f));
		leafEntity->SetSize(Vector2(16.f, 24.f));

		computeAbsCoordinates();

		//Test Position
		Vector2 expected(16.f, 16.f);
		if (rootEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute coordinates for the root Entity at position %s should have been %s.  Instead it's %s."), {rootEntity->ReadPosition().ToString(), expected.ToString(), rootEntity->ReadCachedAbsPosition().ToString()});
			clearEntities();
			return false;
		}

		expected += Vector2(16.f, 32.f);
		if (childEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute coordinates for the child Entity at position %s should have been %s.  Instead it's %s."), {childEntity->ReadPosition().ToString(), expected.ToString(), childEntity->ReadCachedAbsPosition().ToString()});
			clearEntities();
			return false;
		}

		expected += Vector2(8.f, 12.f);
		if (leafEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute coordinates for the leaf Entity at position %s should have been %s.  Instead it's %s."), {leafEntity->ReadPosition().ToString(), expected.ToString(), leafEntity->ReadCachedAbsPosition().ToString()});
			clearEntities();
			return false;
		}

		//Test Size
		expected = Vector2(64.f, 64.f);
		if (rootEntity->ReadCachedAbsSize() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute size for the root Entity with size %s should have been %s.  Instead it's %s."), {rootEntity->ReadSize().ToString(), expected.ToString(), rootEntity->ReadCachedAbsSize().ToString()});
			clearEntities();
			return false;
		}

		expected = Vector2(36.f, 48.f);
		if (childEntity->ReadCachedAbsSize() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute size for the child Entity with size %s should have been %s.  Instead it's %s."), {childEntity->ReadSize().ToString(), expected.ToString(), childEntity->ReadCachedAbsSize().ToString()});
			clearEntities();
			return false;
		}

		expected = Vector2(16.f, 24.f);
		if (leafEntity->ReadCachedAbsSize() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute size for the leaf Entity with size %s should have been %s.  Instead it's %s."), {leafEntity->ReadSize().ToString(), expected.ToString(), leafEntity->ReadCachedAbsSize().ToString()});
			clearEntities();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Bounding Box"));
	{
		/*
		This category depends on the three Entities having these attributes set:
		rootEntity->SetPosition(Vector2(16.f, 16.f));
		rootEntity->SetSize(Vector2(64.f, 64.f));

		childEntity->SetPosition(Vector2(16.f, 32.f));
		childEntity->SetSize(Vector2(36.f, 48.f));

		leafEntity->SetPosition(Vector2(8.f, 12.f));
		leafEntity->SetSize(Vector2(16.f, 24.f));
		*/
		Rectangle<FLOAT> actual = rootEntity->GetBoundingBox();
		Rectangle<FLOAT> expected(16.f, 16.f, 80.f, 80.f, Rectangle<FLOAT>::CS_XRightYDown);
		if (actual != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The bounding box of the Root Entity with position %s and size %s should have been %s.  Instead it's %s."), {rootEntity->ReadCachedAbsPosition().ToString(), rootEntity->ReadCachedAbsSize().ToString(), expected.ToString(), actual.ToString()});
			clearEntities();
			return false;
		}

		actual = childEntity->GetBoundingBox();
		expected = Rectangle<FLOAT>(32.f, 48.f, 68.f, 96.f, Rectangle<FLOAT>::CS_XRightYDown);
		if (actual != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The bounding box of the Child Entity with position %s and size %s should have been %s.  Instead it's %s."), {childEntity->ReadCachedAbsPosition().ToString(), childEntity->ReadCachedAbsSize().ToString(), expected.ToString(), actual.ToString()});
			clearEntities();
			return false;
		}

		actual = leafEntity->GetBoundingBox();
		expected = Rectangle<FLOAT>(40.f, 60.f, 56.f, 84.f, Rectangle<FLOAT>::CS_XRightYDown);
		if (actual != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The bounding box of the Leaf Entity with position %s and size %s should have been %s.  Instead it's %s."), {leafEntity->ReadCachedAbsPosition().ToString(), leafEntity->ReadCachedAbsSize().ToString(), expected.ToString(), actual.ToString()});
			clearEntities();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("IsWithinBounds"));
	{
		rootEntity->SetPosition(Vector2(10.f, 20.f));
		rootEntity->SetSize(Vector2(80.f, 60.f));

		childEntity->SetPosition(Vector2(20.f, 5.f));
		childEntity->SetSize(Vector2(40.f, 50.f));
		computeAbsCoordinates();

		std::function<bool(PlanarTransformComponent* /*tester*/, const Vector2& /* testPoint */, bool /* isPointInBounds */)> testWithinBounds = [&](PlanarTransformComponent* tester, const Vector2& testPoint, bool isPointInBounds)
		{
			if (tester->IsWithinBounds(testPoint) != isPointInBounds)
			{
				if (isPointInBounds)
				{
					UnitTestError(testFlags, TXT("Planar Transform test failed.  The IsWithinBounds test should have returned true when testing point %s on an Entity at abs position %s and abs size %s.  IsWithinBounds returned false instead."), {testPoint.ToString(), tester->ReadCachedAbsPosition().ToString(), tester->ReadCachedAbsSize().ToString()});
				}
				else
				{
					UnitTestError(testFlags, TXT("Planar Transform test failed.  The IsWithinBounds test should have returned false when testing point %s on an Entity at abs position %s and abs size %s.  IsWithinBounds returned true instead."), {testPoint.ToString(), tester->ReadCachedAbsPosition().ToString(), tester->ReadCachedAbsSize().ToString()});
				}

				clearEntities();
				return false;
			}

			return true;
		};

		//Test RootEntity
		if (!testWithinBounds(rootEntity, Vector2(2.f, 4.f), false))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(30.f, 5.f), false))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(95.f, 32.f), false))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(32.f, 95.f), false))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(4.f, 32.f), false))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(24.f, 24.f), true))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(82.f, 24.f), true))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(82.f, 75.f), true))
		{
			return false;
		}

		if (!testWithinBounds(rootEntity, Vector2(24.f, 75.f), true))
		{
			return false;
		}

		//Test LeafEntity
		//AbsPosition(30, 25)
		//AbsSize(40, 50)
		//Valid bounds rectangle in abs coordinates (Left: 30, Top: 25, Right: 70, Bottom: 75)
		if (!testWithinBounds(childEntity, Vector2(15.f, 24.f), false))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(16.f, 48.f), false))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(48.f, 24.f), false))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(84.f, 50.f), false))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(48.f, 80.f), false))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(32.f, 32.f), true))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(64.f, 32.f), true))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(64.f, 64.f), true))
		{
			return false;
		}

		if (!testWithinBounds(childEntity, Vector2(32.f, 64.f), true))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Relative Coordinates"));
	{
		TestLog(testFlags, TXT("Testing Relative Coordinates where leafEntity is relative to childEntity.  ChildEntity is relative to rootEntity."));
		rootEntity->SetPosition(Vector2(0.f, 0.f));
		rootEntity->SetSize(Vector2(1.f, 1.f));

		childEntity->SetPosition(Vector2(0.1f, 0.2f));
		childEntity->SetSize(Vector2(0.8f, 0.6f));

		leafEntity->SetPosition(Vector2(0.5f, 0.5f));
		leafEntity->SetSize(Vector2(0.5f, 0.5f));
		computeAbsCoordinates();

		//Test Position
		Vector2 expected(0.f, 0.f);
		if (rootEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute position of the root entity with relative Position set to %s should have been %s.  It's %s instead."), {rootEntity->ReadPosition().ToString(), expected.ToString(), rootEntity->ReadCachedAbsPosition().ToString()});
			clearEntities();
			return false;
		}

		expected = Vector2(10.f, 20.f);
		if (childEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute position of the child entity with relative Position set to %s should have been %s.  It's %s instead."), {childEntity->ReadPosition().ToString(), expected.ToString(), childEntity->ReadCachedAbsPosition().ToString()});
			clearEntities();
			return false;
		}

		expected = Vector2(50.f, 50.f);
		if (leafEntity->ReadCachedAbsPosition() != expected)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute position of the leaf entity with relative Position set to %s should have been %s.  It's %s instead."), {leafEntity->ReadPosition().ToString(), expected.ToString(), leafEntity->ReadCachedAbsPosition().ToString()});
			clearEntities();
			return false;
		}

		//Test Size
		expected = Vector2(100.f, 100.f);

		//tolerance added due to accumulated float precision errors with CachedAbsSize
		float vectorLengthTolerance = 0.00001f;
		if ((rootEntity->ReadCachedAbsSize() - expected).VSize() > vectorLengthTolerance)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute size of the root entity with relative Size set to %s should have been %s.  It's %s instead."), {rootEntity->ReadSize().ToString(), expected.ToString(), rootEntity->ReadCachedAbsSize().ToString()});
			clearEntities();
			return false;
		}

		expected = Vector2(80.f, 60.f);
		if ((childEntity->ReadCachedAbsSize() - expected).VSize() > vectorLengthTolerance)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute size of the child entity with relative Size set to %s should have been %s.  It's %s instead."), {childEntity->ReadSize().ToString(), expected.ToString(), childEntity->ReadCachedAbsSize().ToString()});
			clearEntities();
			return false;
		}

		expected = Vector2(40.f, 30.f);
		if ((leafEntity->ReadCachedAbsSize() - expected).VSize() > vectorLengthTolerance)
		{
			UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute size of the leaf entity with relative Size set to %s should have been %s.  It's %s instead."), {leafEntity->ReadSize().ToString(), expected.ToString(), leafEntity->ReadCachedAbsSize().ToString()});
			clearEntities();
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	SetTestCategory(testFlags, TXT("Anchoring"));
	{
		std::function<void()> resetAttributes = [&]
		{
			rootEntity->SetAnchorLeft(-1.f);
			rootEntity->SetAnchorTop(-1.f);
			rootEntity->SetAnchorRight(-1.f);
			rootEntity->SetAnchorBottom(-1.f);

			childEntity->SetAnchorLeft(-1.f);
			childEntity->SetAnchorTop(-1.f);
			childEntity->SetAnchorRight(-1.f);
			childEntity->SetAnchorBottom(-1.f);

			leafEntity->SetAnchorLeft(-1.f);
			leafEntity->SetAnchorTop(-1.f);
			leafEntity->SetAnchorRight(-1.f);
			leafEntity->SetAnchorBottom(-1.f);

			rootEntity->SetSize(Vector2(90.f, 85.f));
			rootEntity->SetPosition(Vector2(5.f, 10.f));

			childEntity->SetSize(Vector2(60.f, 64.f));
			childEntity->SetPosition(Vector2(5.f, 10.f));

			leafEntity->SetSize(Vector2(40.f, 48.f));
			leafEntity->SetPosition(Vector2(5.f, 10.f));
		};

		TestLog(testFlags, TXT("Testing Left Anchor"));
		resetAttributes();
		rootEntity->SetAnchorLeft(8.f);
		childEntity->SetAnchorLeft(32.f);
		leafEntity->SetAnchorLeft(16.f);
		computeAbsCoordinates();

		std::function<bool(PlanarTransformComponent* /*entity*/, const Vector2& /*expectedPos*/)> testAnchoring = [&](PlanarTransformComponent* entity, const Vector2& expectedPos)
		{
			if (entity->ReadCachedAbsPosition() != expectedPos)
			{
				UnitTestError(testFlags, TXT("Planar Transform test failed.  The absolute position of an anchored Entity should have been %s.  Instead it's %s."), {expectedPos.ToString(), entity->ReadCachedAbsPosition().ToString()});
				clearEntities();
				return false;
			}

			return true;
		};

		Vector2 expected(8.f, 10.f);
		if (!testAnchoring(rootEntity, expected))
		{
			return false;
		}

		expected = Vector2(40.f, 20.f);
		if (!testAnchoring(childEntity, expected))
		{
			return false;
		}

		expected = Vector2(56.f, 30.f);
		if (!testAnchoring(leafEntity, expected))
		{
			return false;
		}

		TestLog(testFlags, TXT("Testing Top Anchor"));
		resetAttributes();
		rootEntity->SetAnchorTop(12.f);
		childEntity->SetAnchorTop(16.f);
		leafEntity->SetAnchorTop(8.f);
		computeAbsCoordinates();

		expected = Vector2(5.f, 12.f);
		if (!testAnchoring(rootEntity, expected))
		{
			return false;
		}

		expected = Vector2(10.f, 28.f);
		if (!testAnchoring(childEntity, expected))
		{
			return false;
		}

		expected = Vector2(15.f, 36.f);
		if (!testAnchoring(leafEntity, expected))
		{
			return false;
		}

		TestLog(testFlags, TXT("Testing Right Anchor"));
		resetAttributes();
		rootEntity->SetAnchorRight(12.f);
		childEntity->SetAnchorRight(16.f);
		leafEntity->SetAnchorRight(8.f);
		computeAbsCoordinates();

		expected = Vector2(-2.f, 10.f);
		if (!testAnchoring(rootEntity, expected))
		{
			return false;
		}

		expected = Vector2(12.f, 20.f);
		if (!testAnchoring(childEntity, expected))
		{
			return false;
		}

		expected = Vector2(24.f, 30.f);
		if (!testAnchoring(leafEntity, expected))
		{
			return false;
		}

		TestLog(testFlags, TXT("Testing Bottom Anchor"));
		resetAttributes();
		rootEntity->SetAnchorBottom(12.f);
		childEntity->SetAnchorBottom(16.f);
		leafEntity->SetAnchorBottom(8.f);
		computeAbsCoordinates();

		expected = Vector2(5.f, 3.f);
		if (!testAnchoring(rootEntity, expected))
		{
			return false;
		}

		expected = Vector2(10.f, 8.f);
		if (!testAnchoring(childEntity, expected))
		{
			return false;
		}

		expected = Vector2(15.f, 16.f);
		if (!testAnchoring(leafEntity, expected))
		{
			return false;
		}

		TestLog(testFlags, TXT("Testing Multi Anchor"));
		resetAttributes();
		rootEntity->SetAnchorLeft(2.f);
		rootEntity->SetAnchorTop(4.f);
		rootEntity->SetAnchorRight(6.f);
		rootEntity->SetAnchorBottom(8.f);

		childEntity->SetAnchorLeft(3.f);
		childEntity->SetAnchorTop(7.f);
		childEntity->SetAnchorRight(13.f);
		childEntity->SetAnchorBottom(17.f);

		leafEntity->SetAnchorLeft(12.f);
		leafEntity->SetAnchorTop(15.f);
		leafEntity->SetAnchorRight(18.f);
		leafEntity->SetAnchorBottom(21.f);

		computeAbsCoordinates();

		std::function<bool(PlanarTransformComponent* /*entity*/, const Vector2& /*expectedPos*/, const Vector2& /*expectedSize*/)> testMultiAnchor = [&](PlanarTransformComponent* entity, const Vector2& expectedPos, const Vector2& expectedSize)
		{
			if (entity->ReadCachedAbsPosition() != expectedPos)
			{
				UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute position of a multi-anchored Entity should have been %s.  It's actually %s instead."), {expectedPos.ToString(), entity->ReadCachedAbsPosition().ToString()});
				clearEntities();
				return false;
			}

			if (entity->ReadCachedAbsSize() != expectedSize)
			{
				UnitTestError(testFlags, TXT("Planar Transform test failed.  The expected absolute size of a multi-anchored Entity should have been %s.  It's actually %s instead."), {expectedSize.ToString(), entity->ReadCachedAbsSize().ToString()});
				clearEntities();
				return false;
			}

			return true;
		};

		Vector2 expectedPos(2, 4);
		Vector2 expectedSize(92.f, 88.f);
		if (!testMultiAnchor(rootEntity, expectedPos, expectedSize))
		{
			return false;
		}

		expectedPos = Vector2(5.f, 11.f);
		expectedSize = Vector2(76.f, 64.f);
		if (!testMultiAnchor(childEntity, expectedPos, expectedSize))
		{
			return false;
		}

		expectedPos = Vector2(17.f, 26.f);
		expectedSize = Vector2(46.f, 28.f);
		if (!testMultiAnchor(leafEntity, expectedPos, expectedSize))
		{
			return false;
		}
	}
	CompleteTestCategory(testFlags);

	clearEntities();
	ExecuteSuccessSequence(testFlags, TXT("Planar Transforms"));
	return true;
}

bool GraphicsUnitTester::TestSceneTransforms (UnitTester::EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Scene Transforms"));

	TestLog(testFlags, TXT("Spawning 3 Entities that implements a SceneTransform.  1 Entity is the root, and the other 2 Entities are relative to the root Entity."));
	std::vector<SceneEntity*> entitiesToDestroy;
	std::function<void()> clearEntities = [&entitiesToDestroy]
	{
		for (UINT_TYPE i = 0; i < entitiesToDestroy.size(); ++i)
		{
			entitiesToDestroy.at(i)->Destroy();
		}
		ContainerUtils::Empty(entitiesToDestroy);
	};

	SceneEntity* rootEntity = SceneEntity::CreateObject();
	rootEntity->DebugName = TXT("Root Entity");
	entitiesToDestroy.push_back(rootEntity);

	//All Entities should initialize with transform matrices equal to their identity matrix.
	TransformMatrix transformMatrix = rootEntity->ToTransformMatrix();
	if (!transformMatrix.IsUnitMatrix())
	{
		UnitTestError(testFlags, TXT("Entity transform test failed.  The transform matrix should initialize to the identity matrix.  The Entity instead initialized with this matrix data %s"), {transformMatrix.ToString()});
		clearEntities();
		return false;
	}

	SceneEntity* subEntity1 = SceneEntity::CreateObject();
	SceneEntity* subEntity2 = SceneEntity::CreateObject();
	subEntity1->DebugName = TXT("Sub Entity 1");
	subEntity2->DebugName = TXT("Sub Entity 2");
	subEntity1->SetRelativeTo(rootEntity);
	subEntity2->SetRelativeTo(rootEntity);
	entitiesToDestroy.push_back(subEntity1);
	entitiesToDestroy.push_back(subEntity2);

	SetTestCategory(testFlags, TXT("Translation Tests"));
	{
		//At this point everything should still be at the origin.  Verify that.
		Vector3 expectedAbsPosition(0.f, 0.f, 0.f);
		rootEntity->CalculateAbsoluteTransform();
		subEntity1->CalculateAbsoluteTransform();
		subEntity2->CalculateAbsoluteTransform();
		Vector3 actualAbsPosition = rootEntity->GetAbsTranslation();
		if (actualAbsPosition != expectedAbsPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After spawning an entity that's not relative to anything, it's absolute translation wasn't %s.  Instead it's %s."), {expectedAbsPosition.ToString(), actualAbsPosition.ToString()});
			clearEntities();
			return false;
		}

		actualAbsPosition = subEntity1->GetAbsTranslation();
		bool bPassed = (actualAbsPosition == expectedAbsPosition);
		actualAbsPosition = subEntity2->GetAbsTranslation();
		bPassed &= (actualAbsPosition == expectedAbsPosition);
		if (!bPassed)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After spawning an entity that's relative to %s, it's translation wasn't %s."), {rootEntity->ToString(), expectedAbsPosition.ToString()});
			clearEntities();
			return false;
		}

		TestLog(testFlags, TXT("Verified that all entities have their default position in absolute space are at the origin."));

		Vector3 deltaMove(5.f, 50.f, 0.f);
		rootEntity->EditTranslation() += deltaMove;
		rootEntity->CalculateAbsoluteTransform();
		subEntity1->CalculateAbsoluteTransform();
		actualAbsPosition = rootEntity->GetAbsTranslation();
		expectedAbsPosition = deltaMove;
		if (actualAbsPosition != expectedAbsPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After moving the root entity by %s the root entity absolute translation should be %s.  Instead it's %s."), {deltaMove.ToString(), expectedAbsPosition.ToString(), actualAbsPosition.ToString()});
			clearEntities();
			return false;
		}

		actualAbsPosition = subEntity1->GetAbsTranslation();
		if (actualAbsPosition != expectedAbsPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After moving the root entity by %s and with the sub entity's transform having no translation modifications, the sub entity's absolute position should be %s.  Instead it's %s."), {deltaMove.ToString(), expectedAbsPosition.ToString(), actualAbsPosition.ToString()});
			clearEntities();
			return false;
		}

		deltaMove = Vector3(-10.f, 100.f, 30.f);
		subEntity1->EditTranslation() += deltaMove;
		expectedAbsPosition += deltaMove;
		subEntity1->CalculateAbsoluteTransform();
		actualAbsPosition = subEntity1->GetAbsTranslation();
		if (actualAbsPosition != expectedAbsPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After moving the sub entity by %s, it's absolute position should have been %s.  Intead it's %s."), {deltaMove.ToString(), expectedAbsPosition.ToString(), actualAbsPosition.ToString()});
			clearEntities();
			return false;
		}

		//Spawn a chain of Entities relative to each other, and test their coordinates
		{
			SceneEntity* superSubEntityA = SceneEntity::CreateObject();
			SceneEntity* superSubEntityB = SceneEntity::CreateObject();
			SceneEntity* superSubEntityC = SceneEntity::CreateObject();
			SceneEntity* superSubEntityD = SceneEntity::CreateObject();

			std::vector<SceneEntity*> superSubEntities;
			superSubEntities.push_back(superSubEntityA);
			superSubEntities.push_back(superSubEntityB);
			superSubEntities.push_back(superSubEntityC);
			superSubEntities.push_back(superSubEntityD);
			std::function<void()> clearSubEntities = [&]
			{
				for (UINT_TYPE i = 0; i < superSubEntities.size(); ++i)
				{
					superSubEntities.at(i)->Destroy();
				}

				ContainerUtils::Empty(superSubEntities);
			};

			superSubEntityA->SetRelativeTo(subEntity2);
			superSubEntityB->SetRelativeTo(superSubEntityA);
			superSubEntityC->SetRelativeTo(superSubEntityB);
			superSubEntityD->SetRelativeTo(superSubEntityC);

			std::vector<Vector3> expectedAbsCoordinates;
			std::vector<Vector3> deltaMoves;

			expectedAbsCoordinates.push_back(rootEntity->GetAbsTranslation()); //subEntity2

			Vector3 deltaMove(182.5f, 28.1f, -45.6f);
			deltaMoves.push_back(deltaMove);
			superSubEntityA->EditTranslation() += deltaMove;
			expectedAbsCoordinates.push_back(expectedAbsCoordinates.at(expectedAbsCoordinates.size() - 1) + deltaMove); //superSubEntityA

			deltaMove = Vector3(24.f, -57.f, 115.5f);
			deltaMoves.push_back(deltaMove);
			superSubEntityB->EditTranslation() += deltaMove;
			expectedAbsCoordinates.push_back(expectedAbsCoordinates.at(expectedAbsCoordinates.size() - 1) + deltaMove); //superSubEntityB

			//Add three movement actions for the C
			{
				Vector3 totalMove = Vector3::ZeroVector;
				deltaMove = Vector3(15.f, -64.5f, 119.15f);
				totalMove += deltaMove;
				superSubEntityC->EditTranslation() += deltaMove;
				deltaMove = Vector3(-32.7f, 82.5f, -120.5f);
				totalMove += deltaMove;
				superSubEntityC->EditTranslation() += deltaMove;
				deltaMove = Vector3(71.f, -48.2f, 44.44f);
				totalMove += deltaMove;
				superSubEntityC->EditTranslation() += deltaMove;
				expectedAbsCoordinates.push_back(expectedAbsCoordinates.at(expectedAbsCoordinates.size() - 1) + totalMove); //superSubEntityC
			}

			//D did not move, so it's expected to have the same coordinates as C
			superSubEntityD->SetTranslation(Vector3::ZeroVector);
			expectedAbsCoordinates.push_back(expectedAbsCoordinates.at(expectedAbsCoordinates.size() - 1)); //superSubEntityD

			std::vector<Vector3> actualAbsCoordinates;
			subEntity2->CalculateAbsoluteTransform();
			actualAbsCoordinates.push_back(subEntity2->GetAbsTranslation());
			for (UINT_TYPE i = 0; i < superSubEntities.size(); ++i)
			{
				superSubEntities.at(i)->CalculateAbsoluteTransform();
				actualAbsCoordinates.push_back(superSubEntities.at(i)->GetAbsTranslation());
			}
			CHECK(actualAbsCoordinates.size() == expectedAbsCoordinates.size())

			//Verify the sub entity coordinates
			for (UINT_TYPE i = 0; i < actualAbsCoordinates.size(); ++i)
			{
				actualAbsCoordinates.at(i).X.RoundInline(4);
				actualAbsCoordinates.at(i).Y.RoundInline(4);
				actualAbsCoordinates.at(i).Z.RoundInline(4);

				expectedAbsCoordinates.at(i).X.RoundInline(4);
				expectedAbsCoordinates.at(i).Y.RoundInline(4);
				expectedAbsCoordinates.at(i).Z.RoundInline(4);

				if (actualAbsCoordinates.at(i) != expectedAbsCoordinates.at(i))
				{
					UnitTestError(testFlags, TXT("Entity transform test failed.  There's a translation mismatch from expected %s compared to the actual %s at index %s."), {expectedAbsCoordinates.at(i).ToString(), actualAbsCoordinates.at(i).ToString(), DString::MakeString(i)});
					clearSubEntities();
					clearEntities();
					return false;
				}
			}

			TestLog(testFlags, TXT("Entity transform test instantiated 4 Entities relative to each other, and verified that their translation adjustments align with expected coordinates."));

			deltaMove = Vector3(100.f, 200.f, -150.f);
			rootEntity->EditTranslation() += deltaMove;
			for (UINT_TYPE i = 0; i < expectedAbsCoordinates.size(); ++i)
			{
				expectedAbsCoordinates.at(i) += deltaMove;
			}

			subEntity2->CalculateAbsoluteTransform();
			actualAbsCoordinates.at(0) = subEntity2->GetAbsTranslation();
			for (UINT_TYPE i = 0; i < superSubEntities.size(); ++i)
			{
				superSubEntities.at(i)->CalculateAbsoluteTransform();
				actualAbsCoordinates.at(i + 1) = superSubEntities.at(i)->GetAbsTranslation();
			}

			//Verify sub entity coordinates
			for (UINT_TYPE i = 0; i < expectedAbsCoordinates.size(); ++i)
			{
				actualAbsCoordinates.at(i).X.RoundInline(4);
				actualAbsCoordinates.at(i).Y.RoundInline(4);
				actualAbsCoordinates.at(i).Z.RoundInline(4);

				expectedAbsCoordinates.at(i).X.RoundInline(4);
				expectedAbsCoordinates.at(i).Y.RoundInline(4);
				expectedAbsCoordinates.at(i).Z.RoundInline(4);

				if (actualAbsCoordinates.at(i) != expectedAbsCoordinates.at(i))
				{
					UnitTestError(testFlags, TXT("Entity transform test failed.  After moving the root entity, there's a translation mismatch from expected %s compared to the actual coordinates %s at index %s."), {expectedAbsCoordinates.at(i).ToString(), actualAbsCoordinates.at(i).ToString(), DString::MakeString(i)});
					clearSubEntities();
					clearEntities();
					return false;
				}
			}

			TestLog(testFlags, TXT("After moving the root entity, the sub entities did update their absolute coordinates as well."));
			clearSubEntities();
		}
	}
	CompleteTestCategory(testFlags);

	for (SceneEntity* subEntity : entitiesToDestroy)
	{
		subEntity->ResetTransform();
	}

	SetTestCategory(testFlags, TXT("Scaling Tests"));
	{
		Vector3 rootScaling(2.f, 1.f, 4.f);
		rootEntity->SetScale(rootScaling);
		Vector3 actualScaling = rootEntity->GetScale();

		if (rootScaling != actualScaling)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After scaling the root entity by %s, the transform scaling attributes reads %s instead of %s."), {rootScaling.ToString(), actualScaling.ToString(), rootScaling.ToString()});
			clearEntities();
			return false;
		}
		TestLog(testFlags, TXT("Correctly retrieved scaling information from root Entity."));

		//Sub Entities (currently at identity matrix) should have the same scaling info.
		//Getting the scale value by accessing certain elements doesn't make sense unless the matrix doesn't have rotation.  Ensure there isn't a rotation.
		//CHECK(subEntity1->Rotation) //TODO:  Enable when Quats are added
		subEntity1->CalculateAbsoluteTransform();
		TransformMatrix absMatrix = subEntity1->GetAbsTransform();
		actualScaling = absMatrix.GetScale();
		if (rootScaling != actualScaling)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After scaling the root entity by %s, the sub entity's transform matrix, which should be the identity matrix (%s), computed its absolute scaling to be %s instead of %s."),
				{rootScaling.ToString(), subEntity1->ToTransformMatrix().ToString(), actualScaling.ToString(), rootScaling.ToString()});
			clearEntities();
			return false;
		}

		Vector3 subEntityScale(8.f, -3.f, 0.5f);
		subEntity1->SetScale(subEntityScale);
		Vector3 expectedScaling = subEntityScale * rootScaling;
		subEntity1->CalculateAbsoluteTransform();
		absMatrix = subEntity1->GetAbsTransform(); 
		actualScaling = absMatrix.GetScale();
		
		for (UINT_TYPE i = 0; i < 3; ++i)
		{
			expectedScaling[i].RoundInline(4);
			actualScaling[i].RoundInline(4);
		}

		if (expectedScaling != actualScaling)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  After scaling the sub entity by %s, the sub entity's absolute scale computed to be %s instead of %s."), {subEntityScale.ToString(), actualScaling.ToString(), expectedScaling.ToString()});
			clearEntities();
			return false;
		}
		TestLog(testFlags, TXT("Correctly retrieved sub entity scaling information."));

		//Apply translations to both sub entities and see if their abs positions are affected by RelativeTo's scaling.
		SceneEntity* subEntity3 = SceneEntity::CreateObject();
		entitiesToDestroy.push_back(subEntity3);

		Vector3 subEntityTranslation(500.f, -100.f, 0.5f);
		subEntity1->SetTranslation(subEntityTranslation);
		subEntity2->SetTranslation(subEntityTranslation);
		subEntity2->SetRelativeTo(subEntity1);
		subEntity3->SetTranslation(subEntityTranslation);
		subEntity3->SetRelativeTo(subEntity2);

		subEntity1->CalculateAbsoluteTransform();
		Vector3 actualPosition = subEntity1->GetAbsTranslation();
		Vector3 expectedPosition = (rootScaling * subEntityTranslation);

		std::function<void(INT /*numDecimals*/)> roundTheComparisonVectors = [&](INT numDecimals)
		{
			for (UINT_TYPE i = 0; i < 3; ++i)
			{
				actualPosition[i].RoundInline(numDecimals);
				expectedPosition[i].RoundInline(numDecimals);
			}
		};
		roundTheComparisonVectors(3);

		if (actualPosition != expectedPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  With the root entity scaled to %s, and the sub entity's translation set to %s, the expected absolute translation of the sub entity should have been %s.  Instead it computed %s."),
				{rootScaling.ToString(), subEntityTranslation.ToString(), expectedPosition.ToString(), actualPosition.ToString()});
			clearEntities();
			return false;
		}
		TestLog(testFlags, TXT("Correctly retrieved sub entity absolute coordinates even with the root entity scaled to %s."), rootScaling);

		Vector3 rootShift(200.f, 400.f, -350.f);
		rootEntity->SetTranslation(rootShift);
		subEntity1->CalculateAbsoluteTransform();
		actualPosition = subEntity1->GetAbsTranslation();
		expectedPosition += rootShift;
		roundTheComparisonVectors(3);

		if (actualPosition != expectedPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  With the root entity scaled to %s and shifted to %s, and the sub entity's translation set to %s, the expected absolute translation of the sub entity should have been %s.  Instead it computed %s."),
				{rootScaling.ToString(), rootShift.ToString(), subEntityTranslation.ToString(), expectedPosition.ToString(), actualPosition.ToString()});
			clearEntities();
			return false;
		}

		subEntity2->CalculateAbsoluteTransform();
		actualPosition = subEntity2->GetAbsTranslation();
		expectedPosition = (rootShift) + (rootScaling * subEntityTranslation) + (rootScaling * subEntity1->ReadScale() * subEntityTranslation);
		roundTheComparisonVectors(3);

		if (actualPosition != expectedPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  With the root entity scaled to %s and shifted to %s, and the immediate parent entity scaled to %s and translated to %s, the expected absolute translation of the sub sub entity (with relative translation of %s) should have been %s.  Instead it computed %s."),
				{rootScaling.ToString(), rootShift.ToString(), subEntity1->ReadScale().ToString(), subEntityTranslation.ToString(), subEntityTranslation.ToString(), expectedPosition.ToString(), actualPosition.ToString()});
			clearEntities();
			return false;
		}

		subEntity3->CalculateAbsoluteTransform();
		actualPosition = subEntity3->GetAbsTranslation();
		expectedPosition += (rootScaling * subEntity1->ReadScale() * subEntity2->ReadScale() * subEntityTranslation);
		roundTheComparisonVectors(3);

		if (actualPosition != expectedPosition)
		{
			UnitTestError(testFlags, TXT("Entity transform test failed.  The sub entity of a sub entity with no scale modifiers should have a simple translation shift relative to the parent sub entity.  Its absolute translation is %s instead of %s."), {expectedPosition.ToString(), actualPosition.ToString()});
			clearEntities();
			return false;
		}
		TestLog(testFlags, TXT("Correctly retrieved sub entity absolute coordinates after shifting the scaled root entity."));
		
		subEntity2->SetRelativeTo(rootEntity);
		subEntity3->Destroy();
		entitiesToDestroy.erase(entitiesToDestroy.begin() + entitiesToDestroy.size() - 1);
	}
	CompleteTestCategory(testFlags);

	for (SceneEntity* subEntity : entitiesToDestroy)
	{
		subEntity->ResetTransform();
	}

	std::function<bool(const Vector3&, const Vector3&, const Rotator&, const Vector3&, const Vector3&, const Rotator&, const Vector3&, const Vector3&, const Rotator&)> runTransformTest
		([&](const Vector3& rootTranslation, const Vector3& rootScale, const Rotator& rootRotation, const Vector3& subEntityTranslation, const Vector3& subEntityScale, const Rotator& subEntityRotation, const Vector3& expectedSubAbsPosition, const Vector3& expectedSubScale, const Rotator& expectedSubRotation)
		{
			const float tolerance = 0.01f;

			rootEntity->ResetTransform(true);
			subEntity1->ResetTransform(true);

			TestLog(testFlags, TXT("Translating Root Entity by %s. Scaling Root Entity by %s. Rotating Root Entity by %s."), rootTranslation, rootScale, rootRotation);
			TestLog(testFlags, TXT("Translating Sub  Entity by %s. Scaling Sub  Entity by %s. Rotating Sub  Entity by %s."), subEntityTranslation, subEntityScale, subEntityRotation);

			rootEntity->EditTranslation() = rootTranslation;
			rootEntity->EditScale() = rootScale;
			rootEntity->EditRotation() = rootRotation;
			subEntity1->EditTranslation() = subEntityTranslation;
			subEntity1->EditScale() = subEntityScale;
			subEntity1->EditRotation() = subEntityRotation;

			rootEntity->CalculateAbsoluteTransform();
			subEntity1->CalculateAbsoluteTransform();

			if (!subEntity1->ReadAbsTranslation().IsNearlyEqual(expectedSubAbsPosition, tolerance))
			{
				UnitTestError(testFlags, TXT("Scene Transform test failed. After transforming the root entity by: Translation %s, Scale %s, Rotation %s. And transforming the attached entity by: Translation %s, Scale %s, Rotation %s. The expected absolute position of the sub entity should have been %s. Instead it's %s."), {rootTranslation.ToString(), rootScale.ToString(), rootRotation.ToString(), subEntityTranslation.ToString(), subEntityScale.ToString(), subEntityRotation.ToString(), expectedSubAbsPosition.ToString(), subEntity1->ReadAbsTranslation().ToString()});
				clearEntities();
				return false;
			}

			if (!subEntity1->ReadAbsScale().IsNearlyEqual(expectedSubScale, tolerance))
			{
				UnitTestError(testFlags, TXT("Scene Transform test failed. After transforming the root entity by: Translation %s, Scale %s, Rotation %s. And transforming the attached entity by: Translation %s, Scale %s, Rotation %s. The expected absolute scale of the sub entity should have been %s. Instead it's %s."), {rootTranslation.ToString(), rootScale.ToString(), rootRotation.ToString(), subEntityTranslation.ToString(), subEntityScale.ToString(), subEntityRotation.ToString(), expectedSubScale.ToString(), subEntity1->ReadAbsScale().ToString()});
				clearEntities();
				return false;
			}

			//Convert to directional vectors since there are multiple ways for rotations to point to the same direction.
			Vector3 subEntityDirection = subEntity1->ReadAbsRotation().GetDirectionalVector();
			Vector3 expectedDirection = expectedSubRotation.GetDirectionalVector();
			if (!subEntityDirection.IsNearlyEqual(expectedDirection, tolerance))
			{
				UnitTestError(testFlags, TXT("Scene Transform test failed. After transforming the root entity by: Translation %s, Scale %s, Rotation %s. And transforming the attached entity by: Translation %s, Scale %s, Rotation %s. The expected absolute rotation of the sub entity should have been %s. Instead it's %s. The directional vectors of those rotators are not equal: %s != %s"), {rootTranslation.ToString(), rootScale.ToString(), rootRotation.ToString(), subEntityTranslation.ToString(), subEntityScale.ToString(), subEntityRotation.ToString(), expectedSubRotation.ToString(), subEntity1->ReadAbsRotation().ToString(), subEntityDirection.ToString(), expectedDirection.ToString()});
				clearEntities();
				return false;
			}

			return true;
		});

	std::function<bool(const TransformMatrix&, const TransformMatrix&)> verifyTransformMatrix([&](const TransformMatrix& givenMatrix, const TransformMatrix& expectedMatrix)
	{
		if (!givenMatrix.IsNearlyEqual(expectedMatrix, 0.01f))
		{
			UnitTestError(testFlags, TXT("Scene Transform test failed. The given matrix does not match the expected matrix."));
			if (ShouldHaveDebugLogs(testFlags))
			{
				TestLog(testFlags, TXT("Given Matrix"));
				givenMatrix.LogMatrix();

				TestLog(testFlags, TXT("-========-"));
				TestLog(testFlags, TXT("Expected Matrix"));
				expectedMatrix.LogMatrix();
			}

			clearEntities();
			return false;
		}

		return true;
	});

	SetTestCategory(testFlags, TXT("Rotation Tests"));
	{
		//Constants that'll be the same for all tests for this section
		const Vector3 rootEntityTranslation(Vector3::ZeroVector);
		const Vector3 rootEntityScale(1.f, 1.f, 1.f);
		const Vector3 subEntityTranslation(1.f, 0.f, 0.f);
		const Vector3 subEntityScale(1.f, 1.f, 1.f);
		const Vector3 expectedScale(1.f, 1.f, 1.f);
		const unsigned int fortyFiveDegreeSpin = static_cast<unsigned int>(static_cast<float>(Rotator::QUARTER_REVOLUTION) * 0.5f);

		if (!runTransformTest(rootEntityTranslation, rootEntityScale, Rotator::ZERO_ROTATOR, subEntityTranslation, subEntityScale, Rotator::ZERO_ROTATOR, Vector3(1.f, 0.f, 0.f), expectedScale, Rotator::ZERO_ROTATOR))
		{
			return false;
		}

		//Spin 45 degrees along yaw each iteration.
		Rotator spin = Rotator::ZERO_ROTATOR;
		for (int i = 0; i < 8; ++i)
		{
			Vector3 expectedAbsPosition = spin.GetDirectionalVector() * subEntityTranslation.VSize();

			//Sub Entity should be orbiting around root
			if (!runTransformTest(rootEntityTranslation, rootEntityScale, spin, subEntityTranslation, subEntityScale, Rotator::ZERO_ROTATOR, expectedAbsPosition, subEntityScale, spin))
			{
				return false;
			}

			spin.Yaw += fortyFiveDegreeSpin;
		}

		std::vector<Rotator> expectedAbsRotation
		{
			/* [0] */ Rotator(0, 0, 0),
			/* [1] */ Rotator(180.f, 45.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [2] */ Rotator(-90.f, 0.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [3] */ Rotator(0.f, 45.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [4] */ Rotator(0.f, 0.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [5] */ Rotator(0.f, -45.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [6] */ Rotator(90.f, 0.f, 0.f, Rotator::ERotationUnit::RU_Degrees),
			/* [7] */ Rotator(-180.f, -45.f, 0.f, Rotator::ERotationUnit::RU_Degrees)
		};

		//Spin 45 degrees along pitch each iteration. Meanwhile, the sub entity will also be spinning 45 degrees yaw.
		spin = Rotator::ZERO_ROTATOR;
		Rotator subSpin = Rotator::ZERO_ROTATOR;
		for (size_t i = 0; i < expectedAbsRotation.size(); ++i)
		{
			Vector3 expectedAbsPosition = spin.GetDirectionalVector() * subEntityTranslation.VSize();

			//Sub Entity should be orbiting around root. While the sub entity should also have their own spin.
			if (!runTransformTest(rootEntityTranslation, rootEntityScale, spin, subEntityTranslation, subEntityScale, subSpin, expectedAbsPosition, subEntityScale, expectedAbsRotation.at(i)))
			{
				return false;
			}

			spin.Pitch += fortyFiveDegreeSpin;
			subSpin.Yaw += fortyFiveDegreeSpin;
			subSpin.Pitch += Rotator::QUARTER_REVOLUTION;
			subSpin.Roll += Rotator::HALF_REVOLUTION; //woah dizzy Entity
		}
	}
	CompleteTestCategory(testFlags);

	//This category will be verifying the sub entity's position, scale, and rotation based on its local transform and the root's transform.
	SetTestCategory(testFlags, TXT("Relative Transform Tests"));
	{
		Vector3 rootTranslation(1.f, 2.f, 3.f);
		Vector3 rootScale(1.f, 1.f, 1.f);
		Rotator rootRotation(Rotator::ZERO_ROTATOR);
		Vector3 subTranslation(10.f, 20.f, 30.f);
		Vector3 subScale(1.f, 1.f, 1.f);
		Rotator subRotation(Rotator::ZERO_ROTATOR);
		Vector3 expectedTranslation(11.f, 22.f, 33.f);
		Vector3 expectedScale(1.f, 1.f, 1.f);
		Rotator expectedRotation(Rotator::ZERO_ROTATOR);
		if (!runTransformTest(rootTranslation, rootScale, rootRotation, subTranslation, subScale, subRotation, expectedTranslation, expectedScale, expectedRotation))
		{
			return false;
		}

		rootRotation = Rotator(45.f, 0.f, 180.f, Rotator::RU_Degrees);
		expectedRotation = Rotator(45.f, 0.f, 180.f, Rotator::RU_Degrees);
		expectedTranslation = Vector3(-6.0711f, -19.2132f, -27.0000f);
		if (!runTransformTest(rootTranslation, rootScale, rootRotation, subTranslation, subScale, subRotation, expectedTranslation, expectedScale, expectedRotation))
		{
			return false;
		}

		subRotation = Rotator(-45.f, 90.f, 0.f, Rotator::RU_Degrees);
		expectedRotation = Rotator(0.f, -90.f, 0.f, Rotator::RU_Degrees);
		if (!runTransformTest(rootTranslation, rootScale, rootRotation, subTranslation, subScale, subRotation, expectedTranslation, expectedScale, expectedRotation))
		{
			return false;
		}

		subScale.Y = 2.f;
		expectedScale.Y = 2.f;
		if (!runTransformTest(rootTranslation, rootScale, rootRotation, subTranslation, subScale, subRotation, expectedTranslation, expectedScale, expectedRotation))
		{
			return false;
		}

		rootScale.X = 2.f;
		rootScale.Y = 3.f;
		rootEntity->EditScale() = rootScale; //Assign this early for the ToTransformMatrix tests.

		//Verify the transform matrices are valid before verifying its sub components
		TransformMatrix expectedRootTransform(Vector3(2.f, 3.f, 1.f), Matrix(4, 4,
			{
				1.414f,		-1.414f,	0.f,	1.f,
				-2.121f,	-2.121f,	0.f,	2.f,
				0.f,		0.f,		-1.f,	3.f,
				0.f,		0.f,		0.f,	1.f
			}));

		if (!verifyTransformMatrix(rootEntity->ToTransformMatrix(), expectedRootTransform))
		{
			return false;
		}

		TransformMatrix expectedSubTransform(Vector3(1.f, 2.f, 1.f), Matrix(4, 4,
			{
				0.f,	-0.707f,	-0.707f,	10.f,
				0.f,	1.414f,		-1.414f,	20.f,
				1.f,	0.f,		0.f,		30.f,
				0.f,	0.f,		0.f,		1.f
			}));

		if (!verifyTransformMatrix(subEntity1->ToTransformMatrix(), expectedSubTransform))
		{
			return false;
		}

		rootEntity->CalculateAbsoluteTransform();
		subEntity1->CalculateAbsoluteTransform();
		TransformMatrix expectedAbsTransform(Vector3(2.f, 6.f, 1.f), Matrix(4, 4,
			{
				0.f,	-3.f,		1.f,		-13.14f,
				0.f,	-1.5f,		4.499f,		-61.63f,
				-1.f,	0.f,		0.f,		-27.f,
				0.f,	0.f,		0.f,		1.f
			}));

		if (!verifyTransformMatrix(subEntity1->GetAbsTransform(), expectedAbsTransform))
		{
			return false;
		}

		expectedScale.X = 2.f;
		expectedScale.Y = 6.f;
		expectedTranslation = Vector3(-13.14f, -61.63f, -27.f);
		//TODO: Reenable this test. For some reason, scaling the root entity changes the orientation of the Sub Entity.
		/*
		if (!runTransformTest(rootTranslation, rootScale, rootRotation, subTranslation, subScale, subRotation, expectedTranslation, expectedScale, expectedRotation))
		{
			return false;
		}
		*/
	}
	CompleteTestCategory(testFlags);

	clearEntities();
	ExecuteSuccessSequence(testFlags, TXT("Scene Transforms"));
	return true;
}

bool GraphicsUnitTester::TestSolidColor (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Solid Color Component"));

	ColorTester* colorTester = ColorTester::CreateObject();
	return (colorTester->BeginTest(testFlags)); //The ColorTester will automatically expire when test finishes.
}

bool GraphicsUnitTester::TestSprites (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Sprite"));
	SpriteTester* tester = SpriteTester::CreateObject();
	tester->TestFlags = testFlags;
	TestLog(testFlags, TXT("SpriteTester created.  Entity will automatically perish after animation."));

	return true;
}

bool GraphicsUnitTester::TestRenderTexture (EUnitTestFlags testFlags) const
{
	BeginTestSequence(testFlags, TXT("Render Texture"));
	RenderTextureTester* tester = RenderTextureTester::CreateObject();
	TestLog(testFlags, TXT("RenderTextureTester created.  Close the Render Texture window to terminate the test."));

	return true;
}

SD_END
#endif