/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SceneDrawLayer.h
  A DrawLayer used to render any SceneTransform Entity.
=====================================================================
*/

#pragma once

#include "DrawLayer.h"

SD_BEGIN
class RenderComponent;
class SceneCamera;

class GRAPHICS_API SceneDrawLayer : public DrawLayer
{
	DECLARE_CLASS(SceneDrawLayer)


	/*
	=====================
	  Data types
	=====================
	*/

protected:
	/* Simple data struct used to cache information about a registered component. */
	struct SRegisteredComponent
	{
		RenderComponent* Component;

		/* This component's relative distance to the most recent camera. This is cached in order to avoid
		calling the DistSquared function every time this Entity changes position during the sort function. */
		FLOAT DistSquared;

		SRegisteredComponent (RenderComponent* inComponent) :
			Component(inComponent),
			DistSquared(-1.f)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of RenderComponents to render.  These RenderComponents must be a component for a SceneTransform object. */
	std::vector<SRegisteredComponent> ComponentsToDraw;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void BeginObject () override;
	virtual void RenderDrawLayer (RenderTarget* renderTarget, Camera* cam) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers a single render component to the render list. The component will be rejected if it's
	 * not a render component for an Entity with a SceneTransform.
	 * Returns true on success.
	 */
	virtual bool RegisterSingleComponent (RenderComponent* newRenderComp);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Renders all registered components to the specified renderTarget.
	 */
	virtual void RenderComponents (RenderTarget* renderTarget, SceneCamera* cam);

	/**
	 * Iterates through the component list and sorts them based on their position relative to the given camera.
	 * This actually edits the component list to reduce the amount of shuffling next time this is called.
	 */
	virtual void SortRenderComponents (SceneCamera* cam);

	/**
	 * Iterates through the component list and removes anything that are pending destruction.
	 * NOTE: This must be called before the actual pointers are purged since the Components are not DPointers.
	 */
	virtual void PurgeExpiredComponents ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleGarbageCollection ();
};
SD_END