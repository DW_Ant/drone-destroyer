/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlanarCamera.h
  A specific camera that simply determines the draw offset on a 2D plane.  This camera is
  stuck on a plane, and it can only move in two directions.
=====================================================================
*/

#pragma once

#include "Camera.h"
#include "PlanarTransform.h"

SD_BEGIN
class GRAPHICS_API PlanarCamera : public Camera, public PlanarTransform
{
	DECLARE_CLASS(PlanarCamera)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Delegate that's broadcasted whenever the Zoom property changes. */
	MulticastDelegate<void, FLOAT> OnZoomChanged;

protected:
	/* Determines the scalar multiplier for all objects within view.  2.0 implies that objects are rendered twice as large. */
	FLOAT Zoom;

	/* Draw extents of this camera.  Note this doesn't always correspond 1-to-1 to the size of the viewport that's using this
	camera for drawing.  This is primarily used to help position the camera if a system has control over viewport size, and
	the camera.  For example:  ScrollbarComponents align the camera so that it's top left extents meets the top left corner of sprite.
	ViewExtents are the width and height rectangular region.  Left edge is determined by camera's position minus half of extents.X.
	If no extents are specified, then drawn rendered components are placed relative to this camera's center position.
	Zoom does not affect the ViewExtents. */
	Vector2 ViewExtents;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetZoom (FLOAT newZoom);
	virtual void SetViewExtents (const Vector2& newViewExtents);

	/**
	 * Returns the view extents with the zoom applied.
	 */
	inline Vector2 GetZoomedExtents () const
	{
		return ViewExtents / Zoom;
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(FLOAT, Zoom)
	DEFINE_ACCESSOR(Vector2, ViewExtents)

	inline const Vector2& ReadViewExtents () const
	{
		return ViewExtents;
	}
};
SD_END