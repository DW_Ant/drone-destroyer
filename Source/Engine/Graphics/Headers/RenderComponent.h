/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderComponent.h
  A component class responsible for drawing content to a RenderTarget.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class Transformation;
class RenderTarget;
class Camera;

class GRAPHICS_API RenderComponent : public EntityComponent, public CopiableObjectInterface
{
	DECLARE_CLASS(RenderComponent)


	/*
	=====================
	  Properties
	=====================
	*/

private:
	/* Transform object that influences where this Entity should draw in the RenderTarget. */
	const Transformation* OwnerTransform;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual unsigned int CalculateHashID () const override;
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Implementation that draws something to the given renderTarget from the camera perspective.
	 */
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) = 0;

	/**
	 * Returns true if the entity should even be added to the RenderTarget's draw order array.
	 */
	virtual bool IsRelevant (const RenderTarget* renderTarget, const Camera* camera) const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual const Transformation* GetOwnerTransform () const;
};
SD_END