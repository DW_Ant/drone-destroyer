/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SolidColorRenderComponent.h
  A render component that's responsible for drawing solid colors.
=====================================================================
*/

#pragma once

#include "RenderComponent.h"

SD_BEGIN
class GRAPHICS_API SolidColorRenderComponent : public RenderComponent
{
	DECLARE_CLASS(SolidColorRenderComponent)


	/*
	=====================
	  Datatypes
	=====================
	*/

public:
	enum ShapeType
	{
		ST_Rectangle,
		ST_Circle
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	ShapeType Shape;
	Color SolidColor;

	/* Used to determine the base size (total width) of this Entity (in absolute units).
	This behaves differently rather than simply setting the transform's scale.
	This unit will not affect any transforms relative to this component. Contrast to transform's scale,
	Entities relative to a transform with scale other than one also has their translation affected based on its parent's scale.*/
	FLOAT BaseSize;

	/* Determines the rectangle height (only applicable for if Shape is a rectangle). */
	FLOAT RectHeight;

	/* HACK: Do not submit to engine. Simple workaround to ensure the shapes are rendered relative to their center rather than top left corner. */
	bool CenterOrigin;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
};
SD_END