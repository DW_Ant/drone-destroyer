/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsClasses.h
  Contains all header includes for the Graphics module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the GraphicsClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#define MODULE_GRAPHICS 1

#include "Camera.h"
#include "ColorTester.h"
#include "DrawLayer.h"
#include "EmptyRenderTarget.h"
#include "FontPool.h"
#include "Graphics.h"
#include "GraphicsEngineComponent.h"
#include "GraphicsUnitTester.h"
#include "InstancedSpriteComponent.h"
#include "PlanarCamera.h"
#include "PlanarDrawLayer.h"
#include "PlanarTransform.h"
#include "PlanarTransformComponent.h"
#include "PlatformMacGraphics.h"
#include "PlatformWindowsGraphics.h"
#include "RenderComponent.h"
#include "RenderTarget.h"
#include "RenderTexture.h"
#include "RenderTextureTester.h"
#include "ResolutionComponent.h"
#include "ResourcePool.h"
#include "ResourcePoolMacros.h"
#include "SceneCamera.h"
#include "SceneDrawLayer.h"
#include "SceneEntity.h"
#include "SceneTransform.h"
#include "SceneTransformComponent.h"
#include "SolidColorRenderComponent.h"
#include "SpriteComponent.h"
#include "SpriteTester.h"
#include "TexturePool.h"
#include "TopDownCamera.h"
#include "Transformation.h"
#include "Window.h"

