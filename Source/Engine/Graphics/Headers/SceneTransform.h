/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SceneTransform.h
  An object containing a series of properties that defines the object's local scaling, position,
  rotation, pivot point, and which coordinate system it's relative to.

  Although this is a 2D application, a 3D transformation is still used primarily for perspective
  projections where distant objects are rendered smaller than the close objects.  Also
  this would make it easier to transition to 3D if the need for it comes.

  The coordinate system is a left handed coordinate system where X is forward, R is right, and Z is up.
=====================================================================
*/

#pragma once

#include "Transformation.h"

SD_BEGIN

class GRAPHICS_API SceneTransform : public Transformation
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* HACK! Quick flag that enables auto calculating abs transform every frame. A better solution would be to mark certain transforms static or not. */
	bool AutoComputeAbsTransform;

protected:
	Vector3 Translation;
	Vector3 Scale;

	/* Scales the scalar vector in all three axis uniformly. */
	FLOAT GlobalScale;

	/* Rotates this object about its pivot point. Being a left handed coordinate system, all axis rotates clockwise. */
	Rotator Rotation;

	/* Point from this transform scales from and rotates about. This is essentially the translation offset from this Object's center. */
	Vector3 PivotPoint;

	/* Translation, Scale, and Rotation in world coordinates. This should be recomputed every time this transform is rendered, and
	it may become stale quickly. Use ComputeAbsoluteTransform to refresh these variables. */
	mutable TransformMatrix AbsTransform;
	mutable Vector3 AbsTranslation;
	mutable Vector3 AbsScale;
	mutable Rotator AbsRotation;
	mutable Vector3 AbsPivotPoint;

private:
	/* The SceneTransform this object is relative to.  If this is null, then this transform matrix is in absolute coordinates
	where it's relative to the universe's transform (arbitrary origin and no scaling/rotation data). */
	const SceneTransform* RelativeTo;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SceneTransform ();
	SceneTransform (const Vector3& inTranslation, const Vector3& inScale, FLOAT inGlobalScale, Rotator inRotation, const Vector3& inPivotPoint, const SceneTransform* inRelativeTo);
	SceneTransform (const SceneTransform& copyData);
	virtual ~SceneTransform ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool IsWithinView (const RenderTarget* renderTarget, const Camera* camera)  const override;
	virtual void CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Resets this transform where it's positioned at origin, and
	 * it contains no rotation, and all scaling axis is 1.
	 * @param bResetPivot if true, then the PivotPoint vector will restore back to 0, 0, 0.
	 */
	void ResetTransform (bool bResetPivot = true);

	/**
	 * Generates a 4x4 matrix of this Transformation.
	 */
	TransformMatrix ToTransformMatrix () const;

	/**
	 * Apply this scene's transform to the given matrix in the order of Translation, Scaling, then Rotation.
	 */
	void ApplyTransformTo (TransformMatrix& outMatrix) const;

	/**
	 * Recomputes the absolute transform variables by climbing up the relative chain.
	 * This cannot be a virtual function since the constructor invokes this method.
	 */
	void CalculateAbsoluteTransform () const;


	/*
	=====================
	  Mutators
	=====================
	*/

	void SetTranslation (const Vector3& newTranslation);
	void SetScale (const Vector3& newScale);
	void SetGlobalScale (FLOAT newGlobalScale);
	void SetRotation (const Rotator& newRotation);
	void SetPivotPoint (const Vector3& newPivotPoint);
	void SetRelativeTo (const SceneTransform* newRelativeTo);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Vector3& EditTranslation ()
	{
		return Translation;
	}

	inline Vector3& EditScale ()
	{
		return Scale;
	}

	inline Rotator& EditRotation ()
	{
		return Rotation;
	}

	inline Vector3& EditPivotPoint ()
	{
		return PivotPoint;
	}

	inline const Vector3& ReadTranslation () const
	{
		return Translation;
	}

	inline const Vector3& ReadScale () const
	{
		return Scale;
	}

	inline const Rotator& ReadRotation () const
	{
		return Rotation;
	}

	inline const Vector3& ReadPivotPoint () const
	{
		return PivotPoint;
	}

	inline const TransformMatrix& ReadAbsTransform () const
	{
		return AbsTransform;
	}

	inline const Vector3& ReadAbsTranslation () const
	{
		return AbsTranslation;
	}

	inline const Vector3& ReadAbsScale () const
	{
		return AbsScale;
	}

	inline const Rotator& ReadAbsRotation () const
	{
		return AbsRotation;
	}

	inline const Vector3& ReadAbsPivotPoint () const
	{
		return AbsPivotPoint;
	}

	DEFINE_ACCESSOR(Vector3, Translation)
	DEFINE_ACCESSOR(Vector3, Scale)
	DEFINE_ACCESSOR(FLOAT, GlobalScale)
	DEFINE_ACCESSOR(Rotator, Rotation)
	DEFINE_ACCESSOR(Vector3, PivotPoint)
	DEFINE_ACCESSOR(TransformMatrix, AbsTransform)
	DEFINE_ACCESSOR(Vector3, AbsTranslation)
	DEFINE_ACCESSOR(Vector3, AbsScale)
	DEFINE_ACCESSOR(Rotator, AbsRotation)
	DEFINE_ACCESSOR(Vector3, AbsPivotPoint)

	inline const SceneTransform* GetRelativeTo () const
	{
		return RelativeTo;
	}
};
SD_END