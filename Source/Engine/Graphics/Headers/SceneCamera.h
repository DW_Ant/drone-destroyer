/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SceneCamera.h
  A Camera that's able to project an Entity with 3D coordinates into 2D screenspace.

  This camera also implements the SceneTransform interface so that relative positions
  to drawn Entities can be computed since this camera shares the same coordinate system.
=====================================================================
*/

#pragma once

#include "Camera.h"
#include "SceneTransform.h"

SD_BEGIN
class GRAPHICS_API SceneCamera : public Camera, public SceneTransform
{
	DECLARE_CLASS(SceneCamera)


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * The opposite of CalculatePixelProjection, this function projects a 3D transform into 2D screen coordinates.
	 * 
	 * @param viewportSize The number of pixels available to draw in the viewport. Used to determine the view frustum limits.
	 * @param transform The transform the camera is using to view. It'll use this transform relative to this camera's transform to compute the screen projection.
	 * @param outProjectionData The resulting transformation of objects projected to the screen from this camera's perspective.
	 */
	virtual void ProjectToScreenCoordinates (const Vector2& viewportSize, const SceneTransform& transform, Transformation::SScreenProjectionData& outProjectionData) const = 0;

	/**
	 * Determines the distance the given location is relative to the closest screen point.
	 * For perspective cameras, this is simply the distance from the camera to the point.
	 * For orthographic cameras, this is the distance from the given location to the closest point on the near plane.
	 */
	virtual FLOAT GetDistSquaredToPoint (const Vector3& worldLocation) const = 0;

	inline FLOAT GetDistToPoint (const Vector3& worldLocation) const
	{
		return std::sqrtf(GetDistSquaredToPoint(worldLocation).Value);
	}
};
SD_END