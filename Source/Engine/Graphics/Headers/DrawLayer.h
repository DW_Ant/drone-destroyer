/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DrawLayer.h
  A DrawLayer defines a collection of Entities that should render in a certain order relative
  to each other.

  A DrawLayer is registered to RenderTargets, and their DrawPriority determines which
  DrawLayer should render over others.
  A common example is the UI Draw Layer render over the Scene Draw Layer.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class RenderTarget;
class Camera;

class GRAPHICS_API DrawLayer : public Object
{
	DECLARE_CLASS(DrawLayer)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Draw priority of this DrawLayer where higher values will cause this DrawLayer to render over
	lesser DrawLayers. */
	INT DrawPriority;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Kicks off the render process for this DrawLayer.
	 * This function should execute all registered render component's Render function in a defined order.
	 */
	virtual void RenderDrawLayer (RenderTarget* renderTarget, Camera* cam) = 0;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * Changes the DrawLayer's priority.  This can only be changed once, and it should be set
	 * before it's registered to any RenderTarget.
	 */
	virtual void SetDrawPriority (INT newDrawPriority);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(INT, DrawPriority)
};
SD_END