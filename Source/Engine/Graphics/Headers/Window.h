/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Window.h
  An object representing a Window.  Contains various delegates other
  objects could register to for event handling.
=====================================================================
*/

#pragma once

#include "Graphics.h"
#include "RenderTarget.h"

SD_BEGIN
class GRAPHICS_API Window : public RenderTarget
{
	DECLARE_CLASS(Window)
	

	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Color to use when this object needs to reset. */
	Color DefaultColor;

	/* If true, then this object will delete the WindowHandle object when this object is deleted.
	If false, then the owning object is expected to clean up the resources.  Otherwise it'll cause a memory leak. */
	bool bClearsHandleOnDelete;

	/* If true, then this Window event will invoke the close event on destruction. */
	bool InvokeCloseOnDestruction;

protected:
	/* Mutex used to protect which window resource is active or not. This is primarily to notify OpenGL
	which context it should use. Only one window can be active within a process. */
	static std::mutex WindowResourceMutex;

	/* Actual handle that's broadcasting the events, and interfaces with the OS. */
	sf::RenderWindow* Resource;

	/* Tick component that allows this object to poll window events. */
	DPointer<TickComponent> PollingTickComponent;

	/* List of event handlers that may be interested to any of this window handle's events. */
	MulticastDelegate<void, const sf::Event&> PollingDelegates;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void Reset () override;
	virtual void GenerateScene () override;
	virtual void Draw (const sf::Drawable& drawable, const sf::RenderStates& renderState = sf::RenderStates::Default) override;
	virtual void Display () override;
	virtual void GetSize (INT& outWidth, INT& outHeight) const override;
	virtual Vector2 GetSize () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers the owning objects event handler to receive notifications from window events.
	 * The external object is responsible for calling UnregisterPollingDelegate to avoid dangling pointers.
	 */
	virtual void RegisterPollingDelegate (SDFunction<void, const sf::Event&> newCallback);
	virtual void UnregisterPollingDelegate (SDFunction<void, const sf::Event&> targetCallback);

	/**
	 * Notifies this object to check all pending window events to broadcast delegates.
	 * The Window does not automatically poll its own events since this object could be external from main loop.
	 */
	virtual void PollWindowEvents ();

	/**
	 * Retrieves the OS's coordinates of the current window.  If bIncludeTitlebar is false, then it'll retrieve
	 * the coordinates within the view.
	 */
	virtual void GetWindowPosition (INT& outPosX, INT& outPosY, bool bIncludeTitlebar = false) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSize (const Vector2& newSize);
	virtual void SetResource (sf::RenderWindow* newResource);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual sf::RenderWindow* GetResource () const;
	virtual void GetWindowSize (INT& outWidth, INT& outHeight) const;
	virtual Vector2 GetWindowSize () const;
	virtual TickComponent* GetPollingTickComponent () const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandlePollingTick (FLOAT deltaSec);
};
SD_END