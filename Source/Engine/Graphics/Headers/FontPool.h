/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FontPool.h
  FontPool is responsible for maintaining all imported Fonts for
  recycling.
=====================================================================
*/

#pragma once

#include "ResourcePool.h"
#include "ResourcePoolMacros.h"

#ifndef DEFAULT_FONT_NAME
#define DEFAULT_FONT_NAME "QuattrocentoSans-Regular.ttf"
#endif

SD_BEGIN
class GRAPHICS_API FontPool : public ResourcePool
{
	DECLARE_CLASS(FontPool)
	DECLARE_RESOURCE_POOL(Font)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<Font> DefaultFont;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ReleaseResources () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a Font object, imports file to resource, and registers the object to the FontPool.
	 */
	virtual Font* CreateAndImportFont (DString fileName, DString fontName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual Font* GetDefaultFont () const;
};
SD_END