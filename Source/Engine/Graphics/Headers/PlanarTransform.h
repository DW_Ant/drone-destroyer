/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlanarTransform.h
  A Transform that defines a 2D Entities' position relative to other 2D Entities.

  This transform assumes the coordinate space relative to some arbitrary origin (in UnitType).
  When projected to the screen without a camera, the origin automatically snaps to the top
  left corner of the RenderTarget.

  Entities can determine their position and size relative to the other Entities.
  For example, the size and positional values between 0-1 is a fraction of their Owner's
  positional and size attributes.

  Unit values (in UnitType) for coordinates and size can be specified when
  values greater than 1 is given.


  +-----------+
  | ANCHORING |
  +-----------+
  Planar Transforms have the ability to anchor themselves to their parent transform (the
  transform this transform is relative to).  The dev can specify borders (top, right, bottom,
  and left) or any combination.  When anchored, this transform's relative position and size
  will automatically be adjusted whenever the parent transform changes size.  The transform's
  border distance to the parent transform's border will always be the same until SetAnchor
  is reapplied.

  When anchored, this transform's Position and Size may automatically be converted from
  relative coordinates (when 0-1 is specified) to coordinates in pixels (greater than 1).
  This is because the relative coordinates are irrelevant as soon as the parent transform
  changes size.  These values are computed on the every render cycle since this may be dependent
  on the size of the RenderTarget.

  Anchoring may provide inconsistent behaviors to transforms that are registered to multiple
  different-sized RenderTargets and if their root transforms are in relative size.  It's
  recommended to only apply anchoring to planar transforms for Entities that are only rendered
  in one RenderTarget (such as UI).

  When realigning transforms to fit the anchor constraints, the system may to overwrite the
  Position values only.  If setting position isn't sufficient enough (when both opposite borders
  are anchored), then it'll overwrite the transform's size, too.

  Anchoring only works for axis-aligned transforms (no rotations).

  Due to accessing the parent size, the Anchoring system relies on the parent transforms to
  compute their absolute size before this transform anchors to parent.  Otherwise, the anchoring
  may take a few frames to apply.
=====================================================================
*/

#pragma once

#include "Transformation.h"
#include "Camera.h"
#include "RenderTarget.h"

SD_BEGIN
class GRAPHICS_API PlanarTransform : public Transformation
{


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum class eUnitType
	{
		//One unit corresponds to a single pixel. Using this unit will cause the transform to scale differently on devices with different pixel densities (typically appears smaller on mobile devices)
		UT_Pixels,

		//One unit corresponds to one centimeter on the screen. This should be universal size regardless of the system's pixel density.
		UT_Centimeters
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines what is the value of a single unit (in position and scale). Transforms scaled relative to their owners
	will inherit the owner's UnitType. */
	eUnitType UnitType;

protected:
	/* Specifies the location the PivotPoint should be located in UnitType.  Values between 0-1
	are interpretted as fractions relative to the Owner or RenderTarget where 0 is left/top aligned,
	0.5 is centered, and 1 is right/bottom aligned.  Values greater than 1 are in absolute units
	(in UnitType) relative to the Owners (if any).
	If anchored then the Position values are automatically updated and converted to units in UnitType.
	This is because the parent transforms (relativeTo) could change size at any time, and
	this transform will need to realign itself to abide to the anchor's restrictions.
	When realigning, the relative location (when less than 1 is specified) is no longer relevant. */
	Vector2 Position;

	/* Depth value that determines this object's 'distance' from all cameras.  Objects with lesser depth values
	are rendered in front of other objects with greater depth (distance) values. */
	FLOAT Depth;

	/* Scalar data where relative to the Owner/RenderTarget where 0.1 is 10% the size of Owner, and 1.0
	is the Owner's full size.  Anything greater than 1 is absolute size in cm regardless of Owner scalar values.
	If anchored then the Size values may be automatically updated and converted to units in pixels.
	This is because the parent transforms (relativeTo) could change size at any time, and this transform
	will need to realign itself to abide to the anchor's restrictions.  When realigning, the relative size
	(when less than 1 is specified) is no longer relevant. */
	Vector2 Size;

	/* Origin offset from the top left corner in absolute cm.  Entities are scaled relative to this value,
	and is rotated about this point. */
	Vector2 Pivot;

	/* Translates the bounding box.  This is a workaround to an issue that's introduced when applying a position, pivot,
	scale, and rotation to an Entity that may also have influence on its scale (such as SpriteComponent's adjusting size to
	fill the sprite in specified bounding box).  Unfortunately, setting the Pivot to rotate the object around a particular point,
	and scaling it relative to that point may conflict with the resulting position.

	Example case in Solitaire Sample Game:
	Looking at the X-axis only of the left border of a card.
	CardSize=90
	Pivot=90  <-- Left border is at -90.
	Position=8 + Pivot <-- Left border is at 8
	Add small rotation.
	The sprite component cuts the sprite in half due to texture size (45) <-- This causes the pivot point to be at the center of card,
	which allows the card spin from its center.  However, the bounding box is offset by 45.
	Setting the bounding box to 45 counters that translation allowing functions like WithinBounds to be aligned with visuals.*/
	Vector2 BoundingBoxTranslation;

	/* If true, then this object may scale position and size if the coordinates specified are between 0 and 1. */
	bool EnableFractionScaling;

private:
	/* Transform this object is relative to.  If none is specified, then this transform is relative to the RenderTarget. */
	const PlanarTransform* RelativeTo;

	/* If anchored, these values store the distances between this transform's border and the parent transform's border.
	This transform is not anchored if it's negative. */
	FLOAT AnchorLeftDist;
	FLOAT AnchorTopDist;
	FLOAT AnchorRightDist;
	FLOAT AnchorBottomDist;

	/* Becomes true if the transform needs to change because the anchors have changed. */
	bool AnchorsChanged;

	/* Computed at each Render call, this is the latest absolute position and size (in UnitType).
	It's the accumulated transform data with relative owners considered.
	By default, these values are updated every frame, but should the frame delay cause issues, call the
	ComputeAbsTransform method to refresh these values.

	If this object depends on the size of the RenderTarget, then these values are still in UnitType,
	but they are computed based on the latest RenderTarget they're drawn to. If there isn't a RenderTarget,
	then they assume the RenderTarget size uses normalized coordinates (between -1 to 1). */
	mutable Vector2 CachedPosition;
	mutable Vector2 CachedSize;

	/* Becomes true if this Entity's CachedPosition or CachedSize depends on the size of the viewport.
	This flag propagates to all PlanarTransforms relative to this object.
	This updates everytime the absolute transform is computed. */
	mutable bool DependsOnRenderTargetSize;

	/* Size (in cms) of the parent bounds.  This is either the CachedSize of RelativeTo or
	the size of the RenderTarget.  This is primarily used for anchor calculations. */
	mutable Vector2 ParentAbsSize;

	/* The RenderTarget this Entity is most rendered in.
	This is used for Planar objects that have their scale and position dependent on the render target's size.
	Should ComputeAbsTransform is called, it refers to the most recently drawn RenderTarget.

	Entities that are rendered in multiple RenderTargets should absolutely NOT have position and scalar values
	relative to an Entity, who's rendered in multiple RenderTargets. That just doesn't make sense.
	This property is updated every time this transform is computing its screen coordinates.
	This pointer is only valid on root transforms. */
	mutable DPointer<const RenderTarget> RootRenderTarget;

	/* Camera used for rendering this object on the RootRenderTarget. Only applicable on the root transform. */
	mutable DPointer<const Camera> ExternalCamera;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	PlanarTransform ();
	PlanarTransform (const Vector2& inPosition, const Vector2& inSize, const Vector2& inPivot = Vector2::ZeroVector, FLOAT inDepth = 0.f, const PlanarTransform* inRelativeTo = nullptr);
	PlanarTransform (const PlanarTransform& copyObj);
	virtual ~PlanarTransform ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual bool IsWithinView (const RenderTarget* renderTarget, const Camera* camera) const override;
	virtual void CalculateScreenSpaceTransform (const RenderTarget* target, const Camera* cam, SScreenProjectionData& outProjectionData) const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Steps through this Transform's Relative chain to recompute its absolute position and size.
	 */
	virtual void ComputeAbsTransform ();

	/**
	 * Returns true if the given point is within this Entity's bounds.
	 * @param point Position relative to the top left corner of the RenderTarget.
	 */
	virtual bool IsWithinBounds (const Vector2& point) const;

	/**
	 * Returns the rectangle dimensions that encapsulates this transform's absolute size and position.
	 */
	virtual Rectangle<FLOAT> GetBoundingBox () const;

	/**
	 * Recomputes Position and Size to consider anchor restrictions.  If anchored, the two vectors
	 * will automatically be converted from scalar to units in pixels.  This function assumes
	 * the absolute transform was just computed.  This function may also update the CachedAbs Vectors
	 * if the anchors influenced this transform's dimensions.
	 */
	virtual void ApplyAnchors ();

	/**
	 * Copies the planar transform properties from the given transform.
	 */
	virtual void CopyPlanarTransform (const PlanarTransform* copyFrom);


	/*
	=====================
	  Mutators
	=====================
	*/

	void SetPosition (const Vector2& newPosition);
	void SetPosition (FLOAT x, FLOAT y);
	void SetDepth (FLOAT newDepth);
	void SetSize (const Vector2& newSize);
	void SetSize (FLOAT width, FLOAT height);
	void SetPivot (const Vector2& newPivot);
	void SetPivot (FLOAT x, FLOAT y);
	void SetBoundingBoxTranslation (const Vector2& newBoundingBoxTranslation);
	void SetEnableFractionScaling (bool newEnableFractionScaling);
	void SetRelativeTo (const PlanarTransform* newRelativeTo);

	/**
	 * Specifies the distance this transform's border(s) should be from its parent transform's borders.
	 * Anchoring should NOT be used for transforms that expects to be positioned/scaled relative to their parent transforms.
	 * Anchoring should NOT be used for transforms that are used in multiple different-sized RenderTargets.
	 * Anchoring should NOT be used for transforms with rotations.
	 * See class description for details on these restrictions.
	 *
	 * @param borderDist The number of pixels this border should be from the parent transform's border.
	 * Set to negative values to disable anchoring.
	 */
	void SetAnchorLeft (FLOAT leftDist);
	void SetAnchorTop (FLOAT topDist);
	void SetAnchorRight (FLOAT rightDist);
	void SetAnchorBottom (FLOAT bottomDist);

	/**
	 * Obtains the RenderTarget from the root transform.
	 */
	const RenderTarget* FindRootRenderTarget () const;
	const Camera* FindExternalCamera () const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(Vector2, Position)
	DEFINE_ACCESSOR(FLOAT, Depth)
	DEFINE_ACCESSOR(Vector2, Size)
	DEFINE_ACCESSOR(Vector2, Pivot)
	DEFINE_ACCESSOR(Vector2, BoundingBoxTranslation)
	DEFINE_ACCESSOR(bool, EnableFractionScaling)
	DEFINE_ACCESSOR(const PlanarTransform*, RelativeTo)

	inline Vector2 GetCachedAbsPosition () const
	{
		return CachedPosition;
	}

	inline Vector2 GetCachedAbsSize () const
	{
		return CachedSize;
	}

	inline const Vector2& ReadPosition () const
	{
		return Position;
	}

	inline const Vector2& ReadSize () const
	{
		return Size;
	}

	inline const Vector2& ReadPivot () const
	{
		return Pivot;
	}

	inline const Vector2& ReadBoundingBoxTranslation () const
	{
		return BoundingBoxTranslation;
	}

	inline const Vector2& ReadCachedAbsPosition () const
	{
		return CachedPosition;
	}

	inline const Vector2& ReadCachedAbsSize () const
	{
		return CachedSize;
	}

	DEFINE_ACCESSOR(FLOAT, AnchorLeftDist)
	DEFINE_ACCESSOR(FLOAT, AnchorTopDist)
	DEFINE_ACCESSOR(FLOAT, AnchorRightDist)
	DEFINE_ACCESSOR(FLOAT, AnchorBottomDist)

	inline bool IsAnchoredLeft () const
	{
		return (AnchorLeftDist >= 0.f);
	}

	inline bool IsAnchoredTop () const
	{
		return (AnchorTopDist >= 0.f);
	}

	inline bool IsAnchoredRight () const
	{
		return (AnchorRightDist >= 0.f);
	}

	inline bool IsAnchoredBottom () const
	{
		return (AnchorBottomDist >= 0.f);
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if this transform depends on the size of the RenderTarget this is rendered to.
	 * Typically is true for Objects that scale or align themselves to the view (such as GuiComponents).
	 * Typically returns false for Objects that are the same size and position regardless of RenderTarget size (such as minimap icons).
	 */
	virtual bool ComputeDependsOnRenderTargetSize () const;

	/**
	 * Invoked whenever this transform's absolute size or position has changed since last frame.
	 */
	virtual void HandleAbsTransformChange ();
};
SD_END