/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ResourcePoolMacros.h
  Defines various macros to make it easier to implement various ResourcePools.
  Using Macros instead of templated classes primarily to utilize Object macros
  such as DECLARE_CLASS() and IMPLEMENT_CLASS().  This will also make it easier
  to implement any additional functions to a particular resource pool should
  there ever be a need to handle unique cases.
=====================================================================
*/

#pragma once

#include "Graphics.h"

/**
  Declares various functions and variables for a ResourcePool.  The ResourcePool must be named [ResourceType]Pool.
 */
#define DECLARE_RESOURCE_POOL(ResourceType) \
\
/* Data Types */ \
\
protected: \
	struct S##ResourceType##Mapping \
	{ \
		SD::DString ResourceType##Name; \
		ResourceType##* ResourceType##; \
	}; \
\
\
/* Properties */ \
\
protected: \
	/* Resource instances.  Just like an Engine Component, each resource instance is placed in its own thread. */ \
	static std::vector<SD::DPointer<##ResourceType##Pool>> ResourceType##PoolInstances; \
\
	/* Mutex used for accessing or modifying the ResourcePoolInstances vector. */ \
	static std::mutex ResourceType##PoolInstancesMutex; \
\
	std::vector<S##ResourceType##Mapping> ImportedResources; \
\
\
/* Methods */ \
\
public: \
	/** \
	  Adds new resource to the ImportedResources vector. \
	  Returns false if a texture with matching name is already imported. \
	*/ \
	virtual bool Import##ResourceType (##ResourceType##* new##ResourceType##, const SD::DString& ##ResourceType##Name); \
	\
	/** \
	  Returns true if the given name is valid and not clashing with another imported resource. \
	*/ \
	virtual bool IsValidName (const SD::DString& ResourceType##Name) const; \
\
	/** \
	 * Searches through the resource list, and returns the resource that is mapped to the name that matches the param name. \
	 * @param logOnMissing If true, then it'll generate a log warning if the specified name is not mapped to a resource. \
	 */ \
	virtual ResourceType##* Find##ResourceType (const SD::DString& ResourceType##Name, bool logOnMissing = false); \
\
	/** \
	  Removes a particular texture from the ImportedTextures vector. \
	  Returns true if found and removed. \
	*/ \
	virtual bool Remove##ResourceType (ResourceType##* target); \
\
	/** \
	  Clears any empty or invalid entries in the resource pool. \
	  This should be called after removing many resources. \
	 */ \
	virtual void FlushInvalidEntries (); \
\
	/** \
	  Lists all imported resources to the logs. \
	 */ \
	virtual void LogImportedResources () const; \
\
\
/* Accessors */ \
public: \
	static ResourceType##Pool* Find##ResourceType##Pool (); \
	virtual std::vector<##ResourceType##*> GetImportedResources () const; \
/* Implementation */ \
protected: \
	/** \
	 * Registers this resource pool instance to the ResourcePoolInstance vectors. \
	 * This function will refuse if an instance with the same ThreadID is already registered, \
	 * or if this resource pool is already registered. \
	*/ \
	bool RegisterResourcePool (); \
\
	/** \
	 * Removes this resource pool instance from the ResourcePoolInstance vectors. \
	 */ \
	void RemoveResourcePool (); \
/* Restore class access-specifier defaults. */ \
private:

/**
  Implements the functions and variables declared from DECLARE_RESOURCE_POOL
 */
#define IMPLEMENT_RESOURCE_POOL(ResourceType) \
std::vector<SD::DPointer<##ResourceType##Pool>> ResourceType##Pool::ResourceType##PoolInstances; \
std::mutex ResourceType##Pool::ResourceType##PoolInstancesMutex; \
\
bool ResourceType##Pool::Import##ResourceType (##ResourceType##* new##ResourceType##, const SD::DString& ResourceType##Name) \
{ \
	if (Find##ResourceType##(##ResourceType##Name, false) != nullptr) \
	{ \
		SD::GraphicsLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to import " #ResourceType " named %s since another " #ResourceType " is already imported with that name."), ResourceType##Name); \
		return false; \
	} \
\
	if (##ResourceType##Name.IsEmpty()) \
	{ \
		SD::GraphicsLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to import " #ResourceType " without specifying a name.")); \
		return false; \
	} \
\
	S##ResourceType##Mapping newResource; \
	newResource.##ResourceType = new##ResourceType##; \
	newResource.##ResourceType##Name = ResourceType##Name; \
\
	ImportedResources.push_back(newResource); \
\
	return true; \
} \
\
bool ResourceType##Pool::IsValidName (const SD::DString& ResourceType##Name) const \
{ \
	if (ResourceType##Name.IsEmpty()) \
	{ \
		return false; \
	} \
\
	/* Ensure the name is not already taken */ \
	for (UINT_TYPE i = 0; i < ImportedResources.size(); i++) \
	{ \
		if (ImportedResources.at(i).##ResourceType##Name.Compare(##ResourceType##Name, SD::DString::CC_CaseSensitive) == 0) \
		{ \
			return false; \
		} \
	} \
\
	return true; \
} \
\
ResourceType##* ResourceType##Pool::Find##ResourceType (const SD::DString& ResourceType##Name, bool logOnMissing) \
{ \
	for (UINT_TYPE i = 0; i < ImportedResources.size(); i++) \
	{ \
		if (ImportedResources.at(i).##ResourceType##Name == ResourceType##Name) \
		{ \
			if (ImportedResources.at(i).##ResourceType == nullptr) \
			{ \
				ImportedResources.erase(ImportedResources.begin() + i); \
				if (logOnMissing) \
				{ \
					SD::GraphicsLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to find imported " #ResourceType ".  Although there was a " #ResourceType " named %s, it's no longer valid since that was removed."), ResourceType##Name); \
				} \
				\
				return nullptr; /* Resource is about to be deleted.  The name is no longer mapped to anything */ \
			}  \
\
			return ImportedResources.at(i).##ResourceType##; \
		} \
	} \
\
	if (logOnMissing) \
	{ \
		SD::GraphicsLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to find imported " #ResourceType " with the name %s."), ResourceType##Name); \
	} \
	\
	return nullptr; \
} \
\
bool ResourceType##Pool::Remove##ResourceType (##ResourceType##* target) \
{ \
	for (UINT_TYPE i = 0; i < ImportedResources.size(); i++) \
	{ \
		if (ImportedResources.at(i).##ResourceType == target) \
		{ \
			if (ImportedResources.at(i).##ResourceType == nullptr) \
			{ \
				ImportedResources.erase(ImportedResources.begin() + i); \
				return true; /* Already destroyed */ \
			} \
\
			ImportedResources.at(i).##ResourceType##->Destroy(); \
			ImportedResources.erase(ImportedResources.begin() + i); \
			return true; \
		} \
	} \
\
	return false; \
} \
\
void ResourceType##Pool::FlushInvalidEntries () \
{ \
	UINT_TYPE i = 0; \
	while (i < ImportedResources.size()) \
	{ \
		if (ImportedResources.at(i).##ResourceType == nullptr) \
		{ \
			ImportedResources.erase(ImportedResources.begin() + i); \
			continue; \
		} \
		\
		i++; \
	} \
} \
\
void ResourceType##Pool::LogImportedResources () const \
{ \
	SD::GraphicsLog.Log(SD::LogCategory::LL_Log, TXT("-== Begin " #ResourceType " listing ==-")); \
	for (UINT_TYPE i = 0; i < ImportedResources.size(); i++) \
	{ \
		if (ImportedResources.at(i).##ResourceType != nullptr) \
		{ \
			SD::GraphicsLog.Log(SD::LogCategory::LL_Log, TXT("    [%s] Imported " #ResourceType ":  %s"), SD::INT(i), ImportedResources.at(i).ResourceType##Name); \
		} \
		else \
		{ \
			SD::GraphicsLog.Log(SD::LogCategory::LL_Log, TXT("    [%s] <Deleted texture>"), SD::INT(i)); \
		} \
	} \
\
	SD::GraphicsLog.Log(SD::LogCategory::LL_Log, TXT("-== End " #ResourceType " listing ==-")); \
} \
\
ResourceType##Pool* ResourceType##Pool::Find##ResourceType##Pool () \
{ \
	std::lock_guard<std::mutex> guard(##ResourceType##PoolInstancesMutex); \
	\
	std::thread::id threadID = std::this_thread::get_id(); \
	for (UINT_TYPE i = 0; i < ResourceType##PoolInstances.size(); ++i) \
	{	\
		if (##ResourceType##PoolInstances.at(i)->ThreadID == threadID) \
		{ \
			return ResourceType##PoolInstances.at(i).Get(); \
		} \
	} \
	return nullptr; \
} \
\
std::vector<##ResourceType##*> ResourceType##Pool::GetImportedResources () const \
{ \
	std::vector<##ResourceType##*> results; \
	for (UINT_TYPE i = 0; i < ImportedResources.size(); i++) \
	{ \
		results.push_back(ImportedResources.at(i).##ResourceType##); \
	} \
\
	return results; \
} \
\
bool ResourceType##Pool::RegisterResourcePool () \
{ \
	std::lock_guard<std::mutex> guard(##ResourceType##PoolInstancesMutex); \
	for (UINT_TYPE i = 0; i < ResourceType##PoolInstances.size(); ++i) \
	{ \
		/* Ensure this resource pool isn't already registered. */ \
		if (##ResourceType##PoolInstances.at(i).Get() == this) \
		{ \
			return false; \
		} \
		\
		/* Ensure no other resource pools (in the same thread) were already registered. */ \
		if (##ResourceType##PoolInstances.at(i)->ThreadID == ThreadID) \
		{ \
			return false; \
		} \
	} \
	\
	ResourceType##PoolInstances.push_back(this); \
	return true; \
} \
\
void ResourceType##Pool::RemoveResourcePool () \
{ \
	std::lock_guard<std::mutex> guard(##ResourceType##PoolInstancesMutex); \
	for (UINT_TYPE i = 0; i < ResourceType##PoolInstances.size(); ++i) \
	{ \
		if (##ResourceType##PoolInstances.at(i).Get() == this) \
		{ \
			ResourceType##PoolInstances.erase(##ResourceType##PoolInstances.begin() + i); \
			return; \
		} \
	} \
}