/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SpriteComponent.h
  A render component that's responsible for drawing a single sprite.
=====================================================================
*/

#pragma once

#include "RenderComponent.h"

SD_BEGIN
class GRAPHICS_API SpriteComponent : public RenderComponent
{
	DECLARE_CLASS(SpriteComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* List of callbacks to invoke whenever the texture is updated. */
	MulticastDelegate<void> OnTextureChangedCallbacks;

	/* HACK: Do not submit to engine. Simple workaround to ensure sprites are rendered relative to their center rather than top left corner. */
	bool CenterOrigin;

protected:
	/* The instanced SFML's object that'll handle drawing the actual sprite. */
	sf::Sprite* Sprite;

	/* Special affect that's applied over the sprite. */
	DPointer<Shader> SpriteShader;

	/* Render state that'll is used how the sprite is drawn to the render target. */
	sf::RenderStates RenderState;

	/* Base size of the SpriteComponent. Used to scale the sprite without scaling any components attached to it. */
	Vector2 BaseSize;

	/* Scale multipliers based on displaying subdivision of a sprite. */
	FLOAT SubDivideScaleX;
	FLOAT SubDivideScaleY;

	/* Becomes true if this component should not render the Sprite object.  Setting the sprite's texture to nullptr
	leads to undefined behavior.  The component will check this flag before attempting to render it. */
	bool InvisibleTexture;

	/* Only valid when rendering a texture that came from a RenderTexture.  This is used to notify the RenderTexture
	where its viewport is drawn on the window. */
	DPointer<RenderTexture> RenderTextureOwner;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets the sfml's sprite sub rectangle based on the split and display values.
	 * numHorizontal/VerticalSegments determine the number of equal parts.  (2 = splitting in half, 4 = splitting in quarters)
	 * horizontal/verticalIndex determines which segment to display.  Reads from left->right and top->bottom.
	 * 	If horizontalIndex is 4, then it'll display the fourth segment from the left.
	 * 	If verticalIndex is 0, then it'll display the top segment.
	 * Function returns false if the specified parameters are not valid and no changes were applied to the sprite.
	 * Note:  Calling this will be overriding SetDrawCoordinates if it returns true.
	 */
	virtual bool SetSubDivision (INT numHorizontalSegments, INT numVerticalSegments, INT horizontalIndex, INT verticalIndex);

	/**
	 * Scales the draw coordinates using multipliers relative to the sprite size.
	 * @Param ScaleMultipliers:  Determines how much of the texture is drawn.  Setting 0.5 will draw half the texture.
	 * @Param posMultipliers:  Determines draw position offset.  Setting to 0.5 will move the center of the texture to top left corner.
	 * Note:  Calling this will be overriding SetSubDivision.
	 */
	virtual void SetDrawCoordinatesMultipliers (FLOAT xScaleMultiplier, FLOAT yScaleMultiplier, FLOAT xPosMultiplier = 0.f, FLOAT yPosMultiplier = 0.f);

	/**
	 * Same as SetDrawCoordinates(FLOATs), but this uses absolute coordinates instead of multipliers.
	 * U = x position, V = y position, UL = x Length, VL = y Length.
	 * Note:  Calling this will be overriding SetSubDivision.
	 */
	virtual void SetDrawCoordinates (INT u, INT v, INT ul, INT vl);


	/*
	=====================
	  Mutators
	=====================
	*/

public:

	/**
	 * Updates the texture to render for this sprite.  Note:  You should update the DrawCoordinates or SubDivision values after changing the texture.
	 */
	virtual void SetSpriteTexture (const sf::Texture* newTexture);
	virtual void SetSpriteTexture (const Texture* newTexture);
	virtual void SetSpriteTexture (RenderTexture* renderTextureOwner);
	virtual void ClearSpriteTexture ();

	/**
	 * Replaces the sprite's shader with the specified shader.  This will also update the new shader's base texture resource
	 * to reflect the sprite's current texture.
	 */
	virtual void SetSpriteShader (Shader* newSpriteShader);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual const sf::Sprite* GetSprite () const;
	virtual Shader* GetSpriteShader () const;

	/**
	 * Returns the base size of the texture (without transform scalars).
	 */
	virtual void GetTextureSize (INT& outWidth, INT& outHeight) const;
	virtual Vector2 GetTextureSize () const;

	inline const Vector2& ReadBaseSize () const
	{
		return BaseSize;
	}

	inline Vector2& EditBaseSize ()
	{
		return BaseSize;
	}

	inline RenderTexture* GetRenderTextureOwner () const
	{
		return RenderTextureOwner.Get();
	}

	/**
	 * Returns the draw coordinates of the texture this sprite is rendering.
	 * See SetDrawCoordinates and SetSubDivision.
	 * outU = xPosition of rectangle from the left border of texture.
	 * outV = yPosition of rectangle from the top border of texture.
	 * outUL = The width of the rectangle.
	 * outUV = The height of the rectangle.
	 */
	virtual void GetDrawCoordinates (INT& outU, INT& outV, INT& outUl, INT& outVl) const;
};
SD_END