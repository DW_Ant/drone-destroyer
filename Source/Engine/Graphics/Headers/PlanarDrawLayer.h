/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlanarDrawLayer.h
  A DrawLayer that is designed to render a 2D scene to the RenderTarget.  This DrawLayer
  corresponds to PlanarTransform objects.

  It's recommended to use specialized subclasses instead of this one for performance and 
  maintainability reasons.  Freely registered render components may become difficult to manage
  draw order for large projects.
=====================================================================
*/

#pragma once

#include "DrawLayer.h"

SD_BEGIN
class RenderComponent;
class PlanarCamera;

class GRAPHICS_API PlanarDrawLayer : public DrawLayer
{
	DECLARE_CLASS(PlanarDrawLayer)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Draw Layer priority relative to other DrawLayers.  The Canvas Draw Layer uses a simple free RenderComponent registration, where
	RenderComponents that is a component for a planar transformable object can register themselves to this layer.
	It's recommended to keep component registration to a minimum for this layer due to slow performance and maintainability reasons.
	Large projects may be difficult to maintain render depth (draw order). */
	static const INT CANVAS_LAYER_PRIORITY;

protected:
	/* List of RenderComponents to render.  These RenderComponents must be a component for a PlanarTransformable object.
	This list is automatically sorted by that transform's depth value. */
	std::vector<RenderComponent*> ComponentsToDraw;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void BeginObject () override;
	virtual void RenderDrawLayer (RenderTarget* renderTarget, Camera* cam) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void RegisterSingleComponent (RenderComponent* newRenderComp);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void RenderComponents (RenderTarget* renderTarget, PlanarCamera* cam);
	virtual void PurgeExpiredComponents ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleGarbageCollection ();
};
SD_END