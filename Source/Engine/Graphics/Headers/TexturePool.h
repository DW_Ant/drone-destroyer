/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TexturePool.h
  TexturePool is responsible for maintaining all imported Textures for
  recycling.
=====================================================================
*/

#pragma once

#include "ResourcePool.h"
#include "ResourcePoolMacros.h"

SD_BEGIN
class GRAPHICS_API TexturePool : public ResourcePool
{
	DECLARE_CLASS(TexturePool)
	DECLARE_RESOURCE_POOL(Texture)


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void BeginObject () override;

protected:
	virtual void ReleaseResources () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a Texture object, imports file to resource, and registers the object to the TexturePool.
	 */
	virtual Texture* CreateAndImportTexture (const PrimitiveFileAttributes& file, const DString& textureName);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	void SetTextureAttributes (Texture* target, const DString& textureName);
};
SD_END