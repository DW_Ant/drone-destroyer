/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderTextureTester.h
  A test class that creates a rendered texture resource, and displays that texture within a frame.
=====================================================================
*/

#pragma once

#include "Graphics.h"

#ifdef DEBUG_MODE

SD_BEGIN
class RenderTexture;
class SolidColorRenderComponent;

class GRAPHICS_API RenderTextureTester : public Entity
{
	DECLARE_CLASS(RenderTextureTester)
	

	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Window that'll be rendering the split screen RenderTextures. */
	Window* SplitScreenWindow;

	/* Draw layer for rendering contents in the split screen window. */
	DPointer<PlanarDrawLayer> WindowDrawLayer;

	/* Determines the size of the frame.  This also happens to determine the size of the render textures. */
	Vector2 FrameSize;

	/* Entity that'll be owning the RenderTexture objects. */
	Entity* RenderTextureOwner;

	/* The texture that'll be displaying the contents within the frame. */
	DPointer<RenderTexture> TopRenderTexture;
	DPointer<RenderTexture> BottomRenderTexture;

	/* Cameras that determines the location of the borders. */
	DPointer<PlanarCamera> TopCamera;
	DPointer<PlanarCamera> BottomCamera;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleWindowEvent (const sf::Event& evnt);
};
SD_END
#endif