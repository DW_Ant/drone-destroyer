/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Graphics.h
  Contains important file includes and definitions for the Graphics module.

  The Graphics module is responsible for handling the graphics pipeline,
  and define the resources needed for rendering such as ResourcePools and
  RenderComponents.

  The Graphics module also defines the fundamental concept of the Scene where
  Entities may implement a transform interface to help define where they are
  in relation to other Entities.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\File\Headers\FileClasses.h"
#include "Engine\Random\Headers\RandomClasses.h"
#include "Engine\SfmlGraphics\Headers\SfmlGraphicsClasses.h"

#if !(MODULE_CORE)
#error The Graphics module requires the Core module.
#endif

#if !(MODULE_FILE)
#error The Graphics module requires the File module.
#endif

#if !(MODULE_RANDOM)
#error The Graphics module requires the Random module.
#endif

//Include library-specific includes
#if !(MODULE_SFML_GRAPHICS)
#error The Graphics module requires the SFML Graphics module.
#endif

#define TICK_GROUP_RENDER "Render" //TickGroup that'll be iterating through render components
#define TICK_GROUP_PRIORITY_RENDER 500

#ifdef PLATFORM_WINDOWS
	#ifdef GRAPHICS_EXPORT
		#define GRAPHICS_API __declspec(dllexport)
	#else
		#define GRAPHICS_API __declspec(dllimport)
	#endif
#else
	#define GRAPHICS_API
#endif

//Platform-specific includes
#ifdef PLATFORM_WINDOWS
#include "PlatformWindowsGraphics.h"
#elif defined(PLATFORM_MAC)
#include "PlatformMacGraphics.h"
#endif

SD_BEGIN
extern LogCategory GRAPHICS_API GraphicsLog;
SD_END