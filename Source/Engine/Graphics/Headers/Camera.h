/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Camera.h
  The Camera contains defines the angle to take perspective from when identifying
  where objects should draw themselves to render targets.  This Entity also contains
  lense settings that may influence how objects appear.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class GRAPHICS_API Camera : public Entity
{
	DECLARE_CLASS(Camera)


	/*
	=====================
	  Datatypes
	=====================
	*/

public:
	enum EViewMode
	{
		VM_Normal //Normal view:  textured components, supports shaders, lighting, etc...
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The "lense" of the camera that may influence how objects appear.*/
	EViewMode ViewMode;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Calculates the starting location and a direction from this camera.
	 * This function converts a screen coordinate to scene dimensions.
	 * This direction and location essentially informs where that pixelPos is found in the camera coordinate space.
	 *
	 * @param viewportSize The total width and height of the viewport in screen space (num pixels).
	 * @param pixelPos The point of interest on the viewport.
	 * @param outDirection The rotation the ray is facing.
	 * @param outLocation The location in scene space that determines the starting location of the ray.
	 */
	virtual void CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const;
};
SD_END