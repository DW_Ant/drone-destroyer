/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsEngineComponent.h
  EngineComponent responsible for launching the application's window, and
  running the render pipeline.  This component is also responsible for
  housing the ResourcePools.

  This is essentially the GraphicsCore.
=====================================================================
*/

#pragma once

#include "Graphics.h"
#include "ResourcePool.h"
#include "RenderTarget.h"

SD_BEGIN
class DrawLayer;
class PlanarDrawLayer;

class GRAPHICS_API GraphicsEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(GraphicsEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Default DPI setting to fallback to if the system does not have it yet defined (in dots per inch). */
	static INT DEFAULT_DPI_SCALE;

	/* Latest DPI setting obtained from the primary window handle (in dots per inch). */
	static INT DpiScale;

	/* The window handle that'll typically render the main scene. */
	DPointer<Window> PrimaryWindow;

	/* Primitive DrawLayer used to render 'loose' Entities such as MousePointer. */
	DPointer<PlanarDrawLayer> CanvasDrawLayer;

	unsigned int RenderComponentHashNumber;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	GraphicsEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void RegisterObjectHash () override;
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Methods
	=====================
	*/

	/**
	 * Obtains the current system's DPI settings (in dots per inch).
	 */
	static INT GetDpiScale ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Window* GetPrimaryWindow () const
	{
		return PrimaryWindow.Get();
	}

	inline PlanarDrawLayer* GetCanvasDrawLayer () const
	{
		return CanvasDrawLayer.Get();
	}

	inline unsigned int GetRenderComponentHashNumber () const
	{
		return RenderComponentHashNumber;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Imports various essential textures.
	 */
	virtual void ImportEngineTextures ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleMainWindowEvent (const sf::Event& newEvent);
};
SD_END