/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderTexture.h
  A wrapper to the SFML's RenderTexture.  This is a 2D texture that permits RenderComponents
  to draw themselves to this texture.

  Drawing to a texture is useful for generating images relative to a frame.  This is useful for
  split screen and drawing a scene within a sub component such as a scrollbar.

  Essentially this class is a wrapper to SFML's RenderTexture.  The purpose behind this wrapper is
  to "register" this class in Sand Dune's framework to allow RenderComponents to draw contents to this
  class as a SD::RenderTarget and to generate meta data of this class (such as CDOs and class trees).
=====================================================================
*/

#pragma once

#include "Graphics.h"
#include "RenderTarget.h"

SD_BEGIN
class GRAPHICS_API RenderTexture : public RenderTarget
{
	DECLARE_CLASS(RenderTexture)
	

	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The color to use when clearing the texture resource. */
	Color ResetColor;

protected:
	/* The actual render texture implementation.  Note:  a pointer is used here since the class default object is instantiated at startup time,
	and that would cause a freeze.  Potentially because of accessing a mutex GlResource constructor.
	See:  https://github.com/SFML/SFML/issues/741
	A work around is to make this variable a pointer, and only assign the Resource variable on BeginObject instead of constructor since
	CDO's do not invoke BeginObject. */
	sf::RenderTexture* Resource;

	/* The window coordinates where this texture is last drawn relative to the top left corner of the Window. */
	Vector2 LatestDrawCoordinates;

private:
	bool CreatedResource;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void Reset () override;
	virtual void Draw (const sf::Drawable& drawable, const sf::RenderStates& renderState = sf::RenderStates::Default) override;
	virtual void Display () override;
	virtual Vector2 GetWindowCoordinates () const override;
	virtual void GetSize (INT& outWidth, INT& outHeight) const override;
	virtual Vector2 GetSize () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates a SFML Render Texture with the specified dimensions.
	 * Returns true on success.
	 * See sf::RenderTexture::create
	 */
	bool CreateResource (INT width, INT height, bool hasDepthBuffer);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	void SetLatestDrawCoordinates (const Vector2& newLatestDrawCoordinates);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline const sf::RenderTexture* GetResource () const
	{
		return Resource;
	}

	inline sf::RenderTexture* EditResource ()
	{
		return Resource;
	}
};
SD_END