/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TopDownCamera.h
  An Orthographic camera that's able to view SceneTransformable Entities.
  Its view frustum is perpendicular to the X-Y plane.
=====================================================================
*/

#pragma once

#include "Graphics.h"

SD_BEGIN
class GRAPHICS_API TopDownCamera : public SceneCamera
{
	DECLARE_CLASS(TopDownCamera)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the size of the view frustum. This multiplier essentially acts like the camera's zoom level.
	The greater the value, the more 'zoomed in' it appears. A value of 1 renders everything with 1-to-1 scaling.
	A value of 2 makes everything render twice as large.
	When left at 1, the camera will render match 1-to-1 (1 cm on the screen corresponds to 1 cm in scene space). */
	FLOAT Zoom;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void ProjectToScreenCoordinates (const Vector2& viewportSize, const SceneTransform& transform, Transformation::SScreenProjectionData& outProjectionData) const override;
	virtual void CalculatePixelProjection (const Vector2& viewportSize, const Vector2& pixelPos, Rotator& outDirection, Vector3& outLocation) const override;
	virtual FLOAT GetDistSquaredToPoint (const Vector3& worldLocation) const override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Computes the multiplier that converts from "pixel units" to "scene units"
	 */
	virtual FLOAT GetScalarProjection () const;
};
SD_END