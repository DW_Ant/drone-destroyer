/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CheckboxComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, CheckboxComponent, SD, GuiComponent)

void CheckboxComponent::InitProps ()
{
	Super::InitProps();

	CaptionComponent = nullptr;
	CheckboxSprite = nullptr;

	bEnabled = true;
	bPressedDown = false;
	bHovered = false;
	bChecked = false;
	bCheckboxRightSide = true;
}

void CheckboxComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const CheckboxComponent* checkboxTemplate = dynamic_cast<const CheckboxComponent*>(objTemplate);
	if (checkboxTemplate != nullptr)
	{
		SetCheckboxRightSide(checkboxTemplate->GetCheckboxRightSide());

		bool bCreatedObj;
		CaptionComponent = ReplaceTargetWithObjOfMatchingClass(CaptionComponent.Get(), checkboxTemplate->CaptionComponent.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(CaptionComponent.Get());
		}

		if (CaptionComponent != nullptr)
		{
			CaptionComponent->CopyPropertiesFrom(checkboxTemplate->CaptionComponent.Get());
		}

		CheckboxSprite = ReplaceTargetWithObjOfMatchingClass(CheckboxSprite.Get(), checkboxTemplate->CheckboxSprite.Get(), OUT bCreatedObj);
		if (bCreatedObj && AddComponent(CheckboxSprite.Get()) && CheckboxSprite->RenderComponent != nullptr)
		{
			CheckboxSprite->RenderComponent->OnTextureChangedCallbacks.RegisterHandler(SDFUNCTION(this, CheckboxComponent, HandleSpriteTextureChanged, void));
		}

		if (CheckboxSprite != nullptr)
		{
			CheckboxSprite->CopyPropertiesFrom(checkboxTemplate->CheckboxSprite.Get());
		}
	}
}

bool CheckboxComponent::CanBeFocused () const
{
	return (FocusInterface::CanBeFocused() && bEnabled);
}

bool CheckboxComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	if (keyEvent.key.code == sf::Keyboard::Return || keyEvent.key.code == sf::Keyboard::Space)
	{
		if (keyEvent.type == sf::Event::KeyPressed)
		{
			bPressedDown = true;
			RefreshCheckboxSprite();
			return true;
		}
		else if (keyEvent.type == sf::Event::KeyReleased)
		{
			bPressedDown = false;
			bChecked = !bChecked;
			RefreshCheckboxSprite();
			return true;
		}
	}

	return true;
}

bool CheckboxComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	return false;
}

void CheckboxComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	InitializeCaptionComponent();
	InitializeCheckboxSprite();
	RefreshCheckboxSprite();
	RefreshComponentPositions();
}

void CheckboxComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	bool bCurrentHovered = IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));

	if (bCurrentHovered != bHovered)
	{
		bHovered = bCurrentHovered;
		RefreshCheckboxSprite();
	}
}

void CheckboxComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	//If user released mouse button outside of checkbox borders, then clear pressed down state.
	if (bPressedDown && eventType == sf::Event::MouseButtonReleased && sfmlEvent.button == sf::Mouse::Left &&
		!IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))))
	{
		bPressedDown = false;
		RefreshCheckboxSprite();
	}
}

bool CheckboxComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (IsVisible() && sfmlEvent.button == sf::Mouse::Left &&
		IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))))
	{
		bPressedDown = (eventType == sf::Event::MouseButtonPressed);
		if (eventType == sf::Event::MouseButtonReleased)
		{
			bChecked = !bChecked;
			if (OnChecked.IsBounded())
			{
				OnChecked(this);
			}
		}

		RefreshCheckboxSprite();
		return true;
	}

	return Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType);
}

bool CheckboxComponent::AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const
{
	return (bEnabled && Super::AcceptsMouseEvents(mousePosX, mousePosY));
}

void CheckboxComponent::HandleAbsTransformChange ()
{
	Super::HandleAbsTransformChange();

	if (CheckboxSprite == nullptr || CheckboxSprite->RenderComponent == nullptr)
	{
		return;
	}

	Vector2 textureSize = CheckboxSprite->RenderComponent->GetTextureSize();

	//Reduce texture size since the sprite is subdivided
	textureSize.X *= 0.5f;
	textureSize.Y *= 0.25f;

	FLOAT textureSizeRatio = textureSize.X / textureSize.Y;

	//The sprite height is equal to the height of this component.  Compute the width so that the texture's aspect ratio is preserved.
	FLOAT spriteWidth = ReadCachedAbsSize().Y * textureSizeRatio;
	CheckboxSprite->SetSize(Vector2(spriteWidth, ReadCachedAbsSize().Y));

	RefreshComponentPositions();
}

void CheckboxComponent::SetEnabled (bool bNewEnabled)
{
	bEnabled = bNewEnabled;
	RefreshCheckboxSprite();
}

void CheckboxComponent::SetChecked (bool bNewChecked)
{
	if (bNewChecked != bChecked)
	{
		bChecked = bNewChecked;
		RefreshCheckboxSprite();
	}
}

void CheckboxComponent::SetCheckboxRightSide (bool bNewCheckboxRightSide)
{
	if (bCheckboxRightSide == bNewCheckboxRightSide)
	{
		return;
	}

	bCheckboxRightSide = bNewCheckboxRightSide;
	if (CheckboxSprite != nullptr && CaptionComponent != nullptr)
	{
		RefreshComponentPositions();
		CaptionComponent->SetHorizontalAlignment((bCheckboxRightSide) ? LabelComponent::eHorizontalAlignment::HA_Left : LabelComponent::eHorizontalAlignment::HA_Right);
	}
}

bool CheckboxComponent::GetEnabled () const
{
	return bEnabled;
}

bool CheckboxComponent::GetHovered () const
{
	return bHovered;
}

bool CheckboxComponent::GetChecked () const
{
	return bChecked;
}

bool CheckboxComponent::GetCheckboxRightSide () const
{
	return bCheckboxRightSide;
}

void CheckboxComponent::InitializeCaptionComponent ()
{
	CaptionComponent = LabelComponent::CreateObject();
	if (AddComponent(CaptionComponent.Get()))
	{
		CaptionComponent->SetAutoRefresh(false);
		CaptionComponent->SetWrapText(true);
		CaptionComponent->SetClampText(true);
		CaptionComponent->SetHorizontalAlignment((bCheckboxRightSide) ? LabelComponent::eHorizontalAlignment::HA_Left : LabelComponent::eHorizontalAlignment::HA_Right);
		CaptionComponent->SetVerticalAlignment(LabelComponent::eVerticalAlignment::VA_Center);
		CaptionComponent->SetAutoRefresh(true);
	}
}

void CheckboxComponent::InitializeCheckboxSprite ()
{
	CheckboxSprite = FrameComponent::CreateObject();
	if (AddComponent(CheckboxSprite.Get()))
	{
		CheckboxSprite->SetLockedFrame(true);
		CheckboxSprite->SetBorderThickness(0.f);
		CheckboxSprite->RenderComponent->TopBorder = nullptr;
		CheckboxSprite->RenderComponent->RightBorder = nullptr;
		CheckboxSprite->RenderComponent->BottomBorder = nullptr;
		CheckboxSprite->RenderComponent->LeftBorder = nullptr;
		CheckboxSprite->RenderComponent->TopRightCorner = nullptr;
		CheckboxSprite->RenderComponent->BottomRightCorner = nullptr;
		CheckboxSprite->RenderComponent->BottomLeftCorner = nullptr;
		CheckboxSprite->RenderComponent->TopLeftCorner = nullptr;
		CheckboxSprite->RenderComponent->SetSpriteTexture(GuiTheme::GetGuiTheme()->CheckboxTexture.Get());
	}
}

void CheckboxComponent::RefreshComponentPositions ()
{
	CHECK(CaptionComponent != nullptr && CheckboxSprite != nullptr)

	//Setting the left and right anchors on the caption component to scale the component to fit the rest of the Checkbox based on the amount of space the sprite takes.
	//Only set one of the anchors on the sprite component to snap it on one of the sides.

	const FLOAT checkboxWidth = CheckboxSprite->ReadSize().X;
	if (bCheckboxRightSide)
	{
		CaptionComponent->SetAnchorLeft(0.f);
		CaptionComponent->SetAnchorRight(checkboxWidth);
		CheckboxSprite->SetAnchorLeft(-1.f);
		CheckboxSprite->SetAnchorRight(0.f);
	}
	else
	{
		CaptionComponent->SetAnchorLeft(checkboxWidth);
		CaptionComponent->SetAnchorRight(0.f);
		CheckboxSprite->SetAnchorLeft(0.f);
		CheckboxSprite->SetAnchorRight(-1.f);
	}
}

void CheckboxComponent::RefreshCheckboxSprite ()
{
	CHECK(CheckboxSprite != nullptr && CheckboxSprite->RenderComponent != nullptr)

	INT column = INT(bChecked);
	INT row = 0;

	if (!bEnabled)
	{
		row = 3;
	}
	else if (bPressedDown)
	{
		row = 2;
	}
	else if (bHovered)
	{
		row = 1;
	}

	CheckboxSprite->RenderComponent->SetSubDivision(2, 4, column, row);
}

void CheckboxComponent::HandleSpriteTextureChanged ()
{
	RefreshCheckboxSprite();
}
SD_END