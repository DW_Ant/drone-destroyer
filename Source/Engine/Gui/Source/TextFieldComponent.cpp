/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFieldComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, TextFieldComponent, SD, LabelComponent)

void TextFieldComponent::InitProps ()
{
	Super::InitProps();

	RenderComponentClass = TextFieldRenderComponent::SStaticClass();

	MaxNumCharacters = -1;
	DragScrollRate = 0.2f;

	SetRenderComponent(nullptr);
	bEditable = true;
	bSelectable = true;
	CursorPosition = -1;
	ActiveLineIndex = -1;
	HighlightEndPosition = -1;
	IconOverrideMouse = nullptr;

	bIgnoreNextFocusedTextEvent = false;
	bDraggingOverText = false;
	LastDragScrollTime = 0.f;
	OverridingMouseIcon = nullptr;
	bIsCtrlCmd = false;
}

void TextFieldComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const TextFieldComponent* textFieldTemplate = dynamic_cast<const TextFieldComponent*>(objTemplate);
	if (textFieldTemplate != nullptr)
	{
		MaxNumCharacters = textFieldTemplate->MaxNumCharacters;
		DragScrollRate = textFieldTemplate->DragScrollRate;
		SetEditable(textFieldTemplate->GetEditable());
		SetSelectable(textFieldTemplate->GetSelectable());
	}
}

bool TextFieldComponent::CanBeFocused () const
{
	if (!bSelectable && !bEditable)
	{
		return false;
	}

	return IsVisible();
}

bool TextFieldComponent::CanTabOut () const
{
	if (CursorPosition >= 0)
	{
		return false; //The user is currently editing text.
	}

	return FocusInterface::CanTabOut();
}

void TextFieldComponent::GainFocus ()
{
	FocusInterface::GainFocus();

	SetCursorPosition(0);
	bIgnoreNextFocusedTextEvent = true; //The user is in the middle of a tab press.  We'll need to ignore the next key event so that tab is not applied to the text content.
}

void TextFieldComponent::LoseFocus ()
{
	FocusInterface::LoseFocus();

	SetCursorPosition(-1);
	SetHighlightEndPosition(-1);
}

bool TextFieldComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	if (OnEdit.IsBounded() && OnEdit(keyEvent))
	{
		return true;
	}

	if (keyEvent.key.code == sf::Keyboard::Escape && keyEvent.type == sf::Event::KeyReleased && CursorPosition >= 0)
	{
		SetCursorPosition(-1);
		SetHighlightEndPosition(-1);
		return true;
	}
	else if (keyEvent.key.code == sf::Keyboard::Return && keyEvent.type == sf::Event::KeyReleased && CursorPosition < 0)
	{
		SetCursorPosition(0);
		return true;
	}

	return ExecuteTextFieldInput(keyEvent);
}

bool TextFieldComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	if (bIgnoreNextFocusedTextEvent)
	{
		bIgnoreNextFocusedTextEvent = false;
		return true; //Cause the default text event to be ignored, too.
	}

	return ExecuteTextFieldText(keyEvent);
}

void TextFieldComponent::SetRenderComponent (TextRenderComponent* newRenderComponent)
{
	Super::SetRenderComponent(newRenderComponent);

	if (RenderComponent != nullptr)
	{
		RenderComponent->RegisterOnShaderChangedCallback(SDFUNCTION(this, TextFieldComponent, HandleShaderChanged, void));
		UpdateHighlightShaderParameters();
	}
}

void TextFieldComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	std::pair<DString, sf::Shader::Type> themeShaderInfo = GuiTheme::GetGuiTheme()->HighlightingShader;
	GetRenderComponent()->SetTextShader(Shader::CreateShader(themeShaderInfo.first, themeShaderInfo.second));
}

bool TextFieldComponent::ExecuteConsumableInput (const sf::Event& evnt)
{
	Super::ExecuteConsumableInput(evnt);

	if (OnEdit.IsBounded() && OnEdit(evnt))
	{
		return true;
	}

	return ExecuteTextFieldInput(evnt);

}

bool TextFieldComponent::ExecuteConsumableText (const sf::Event& evnt)
{
	return ExecuteTextFieldText(evnt);
}

void TextFieldComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	UpdateMousePointerIcon(mouse, sfmlEvent);

	if (bDraggingOverText)
	{
		FLOAT elapsedTime = Engine::FindEngine()->GetElapsedTime();
		if (elapsedTime - LastDragScrollTime >= DragScrollRate && !IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))))
		{
			LastDragScrollTime = elapsedTime;
			const Vector2& textPosition = ReadCachedAbsPosition();

#if 0	//TODO:  Change scrollbar camera position
			if (GetTextScrollbar() != nullptr)
			{
				if (sfmlEvent.y < textPosition.Y.Value)
				{
					//Scroll up
					GetTextScrollbar()->SetScrollPosition(GetTextScrollbar()->GetScrollPosition() - 1);
				}
				else if (sfmlEvent.y > textPosition.Y.Value + ReadCachedAbsSize().Y.Value)
				{
					//Scroll down
					GetTextScrollbar()->SetScrollPosition(GetTextScrollbar()->GetScrollPosition() + 1);
				}
			}
#endif
		}

		INT charIndex = FindCursorPos(sfmlEvent.x, sfmlEvent.y);
		if (charIndex >= 0)
		{
			SetHighlightEndPosition(charIndex);
		}
	}
}

void TextFieldComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (sfmlEvent.button != sf::Mouse::Left)
	{
		return;
	}

	bool bWithinRange = IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));
	if (!bWithinRange && !bDraggingOverText)
	{
		//deselect
		SetCursorPosition(-1);
		SetHighlightEndPosition(-1);
	}
}

bool TextFieldComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (!bSelectable || !IsVisible())
	{
		return false;
	}

	if (sfmlEvent.button != sf::Mouse::Left)
	{
		//The text field only listens for left mouse button events, but it still captures other mouse events if the click was over component.
		return IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));
	}

	if (eventType == sf::Event::MouseButtonReleased)
	{
		bool bOldDraggingOverText = bDraggingOverText;
		bDraggingOverText = false;

		return bOldDraggingOverText;
	}
	else if (IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))))
	{
		INT charIndex = FindCursorPos(INT(sfmlEvent.x), INT(sfmlEvent.y));

		if (charIndex >= 0)
		{
			SetCursorPosition(charIndex);
		}

		SetHighlightEndPosition(-1);
		bDraggingOverText = true;

		return true;
	}

	return false;
}

#if 0 //TODO: Update shaders whenever the camera changes position
void TextFieldComponent::HandleScrollPositionChanged (INT newScrollIndex)
{
	Super::HandleScrollPositionChanged(newScrollIndex);

	TextFieldRenderComponent* textRenderComp = dynamic_cast<TextFieldRenderComponent*>(GetRenderComponent());
	if (textRenderComp != nullptr)
	{
		//Recalculate the cursor draw position since the characters changed positions.
		textRenderComp->bForceRecalcCursorCoordinates = true;
	}

	if (HighlightEndPosition >= 0)
	{
		UpdateHighlightShaderParameters();
	}
}
#endif

void TextFieldComponent::Destroyed ()
{
	if (IconOverrideMouse != nullptr)
	{
		IconOverrideMouse->RemoveIconOverride(SDFUNCTION_2PARAM(this, TextFieldComponent, HandleMouseIconOverride, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
		OverridingMouseIcon = nullptr;
		IconOverrideMouse = nullptr;
	}

	Super::Destroyed();
}

bool TextFieldComponent::IsCursorVisible () const
{
	if (CursorPosition < 0)
	{
		return false;
	}

#if 0 //TODO:  Reference scrollbar?
	if (TextScrollbar != nullptr)
	{
		return (TextScrollbar->IsIndexVisible(ActiveLineIndex));
	}
#endif

	return true;
}

void TextFieldComponent::InsertTextAtCursor (DString insertedText)
{
	if (CursorPosition < 0)
	{
		return;
	}

	DString fullText = GetContent();
	if (MaxNumCharacters > 0)
	{
		INT availableChars = MaxNumCharacters - fullText.Length();
		if (availableChars <= 0)
		{
			return;
		}
		
		//Clamp inserted text
		if (insertedText.Length() > availableChars)
		{
			insertedText = insertedText.Left(availableChars);
		}
	}

	INT textIdx = GetTextPosition();
	fullText.Insert(textIdx, insertedText);

	SetText(fullText);

	//Need to detect if the ActiveLineIndex changed (due to word wrapping)
	INT oldActiveLineIndex = ActiveLineIndex;
	SetCursorPosition(CursorPosition + insertedText.Length());

	if (oldActiveLineIndex != ActiveLineIndex)
	{
		//The cursor moved to a different line (advancing to the next line requires the cursor to skip over the extra cursor position at end of line).
		INT deltaLine = ActiveLineIndex - oldActiveLineIndex;
		SetCursorPosition(CursorPosition + deltaLine);
	}
}

void TextFieldComponent::SetEditable (bool bNewEditable)
{
	if (bEditable == bNewEditable)
	{
		return; //Nothing needs changing
	}

	bEditable = bNewEditable;
	if (!bEditable)
	{
		//deselect
		SetHighlightEndPosition(-1);
		SetCursorPosition(-1);
		if (!bSelectable)
		{
			//Notify mouse pointer that this is not overriding mouse pointer
			OverridingMouseIcon = nullptr;
		}
	}
}

void TextFieldComponent::SetSelectable (bool bNewSelectable)
{
	if (bSelectable == bNewSelectable)
	{
		return;
	}

	bSelectable = bNewSelectable;
	if (!bSelectable)
	{
		//deselect
		SetHighlightEndPosition(-1);
		SetCursorPosition(-1);
		if (!bEditable)
		{
			//Notify mouse pointer that this is not overriding mouse pointer
			OverridingMouseIcon = nullptr;
		}
	}
}

void TextFieldComponent::SetCursorPosition (INT newPosition)
{
	if (newPosition == CursorPosition)
	{
		return;
	}

	CursorPosition = Utils::Min(newPosition, CountNumCursorPositions(true) - 1);
	CalculateActiveLineIndex();

	if (CursorPosition >= 0 && !IsCursorVisible())
	{
#if 0 //TODO:  change camera position
		if (TextScrollbar != nullptr)
		{
			//Update the scroll position so that the cursor becomes visible
			bool bCursorAbove = (ActiveLineIndex < GetScrollPosition());
			INT newScrollPosition = (bCursorAbove) ? ActiveLineIndex - TextScrollbar->ScrollJumpInterval : ActiveLineIndex + TextScrollbar->GetNumVisibleScrollPositions() + TextScrollbar->ScrollJumpInterval;
			TextScrollbar->SetScrollPosition(newScrollPosition);
		}
#endif
	}

	UpdateHighlightShaderParameters();
}

void TextFieldComponent::SetHighlightEndPosition (INT newPosition)
{
	if (HighlightEndPosition == newPosition)
	{
		return;
	}

	HighlightEndPosition = Utils::Min(newPosition, CountNumCursorPositions(true) - 1);
	UpdateHighlightShaderParameters();
}

bool TextFieldComponent::GetEditable () const
{
	return bEditable;
}

bool TextFieldComponent::GetSelectable () const
{
	return bSelectable;
}

INT TextFieldComponent::GetTextPosition () const
{
	//There are extra cursor positions at the end of each line, need to offset delete index based on the line the cursor is found.
	return CursorPosition - ActiveLineIndex;
}

INT TextFieldComponent::GetTextPosition (INT cursorIdx, INT lineIdx) const
{
	//There are extra cursor positions at the end of each line, need to offset delete index based on the line the cursor is found.
	return (cursorIdx - lineIdx);
}

DString TextFieldComponent::GetSelectedText () const
{
	if (HighlightEndPosition < 0 || HighlightEndPosition == CursorPosition || CursorPosition < 0)
	{
		return DString::EmptyString; //Nothing is selected
	}

	//Need to run separate calculation for HighlightEndPosition since there isn't an ActiveLineIndex variable for HighlightEndPosition (don't want to recalculate ActiveLineIdx on mouse move for HighlightEndPos).
	INT highlightEndPosLineIdx = GetCursorLineData(HighlightEndPosition);
	CHECK(highlightEndPosLineIdx != INT_INDEX_NONE)

	INT textPosition = GetTextPosition();
	INT highlightEndTextPos = GetTextPosition(HighlightEndPosition, highlightEndPosLineIdx);
	if (INT::Abs(textPosition - highlightEndTextPos) == 1) //Only one character is selected
	{
		return GetContent().SubStringCount(Utils::Min(textPosition, highlightEndTextPos), 1);
	}
	else if (highlightEndTextPos < textPosition) //The trailing highlight should not include the following character (unlike the start of highlight)
	{
		return GetContent().SubString(highlightEndTextPos, textPosition - 1);
	}
	else
	{
		return GetContent().SubString(textPosition, highlightEndTextPos - 1);
	}
}

void TextFieldComponent::UpdateMousePointerIcon (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent)
{
	if (mouse == nullptr)
	{
		return;
	}

	bool bHoveringOverText = ((bSelectable || bEditable) && IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))));
	if (bHoveringOverText && IsVisible())
	{
		OverridingMouseIcon = GuiTheme::GetGuiTheme()->TextPointer;
		mouse->PushMouseIconOverride(OverridingMouseIcon.Get(), SDFUNCTION_2PARAM(this, TextFieldComponent, HandleMouseIconOverride, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
		IconOverrideMouse = mouse;
	}
}

INT TextFieldComponent::FindCursorPos (INT absX, INT absY)
{
	CHECK(GetRenderComponent() != nullptr)
	if (ContainerUtils::IsEmpty(Content))
	{
		return -1;
	}

	const Vector2& finalCoordinates = ReadCachedAbsPosition();
	INT skippedPos = 0;
	const std::vector<TextRenderComponent::STextData>& textData = GetRenderComponent()->ReadTexts();
	INT lineIndex = 0; 
	lineIndex = -1;
	for (UINT_TYPE i = 0; i < textData.size(); i++)
	{
		//Special case for last line:  causes the last line to be selected if the user clicks on the blank space under last line.
		if (i == textData.size() - 1)
		{
			if (absY.ToFLOAT() - finalCoordinates.Y > ReadCachedAbsSize().Y)
			{
				return -1; //out of bounds
			}

			lineIndex = i;
			break;
		}
		else if ((textData.at(i+1).DrawOffset.y + finalCoordinates.Y.Value) >= absY.ToFLOAT().Value)
		{
			lineIndex = i;
			break;
		}

		//Add one since there are String.Length+1 cursor positions for every line.  For example:  for string "abcd", there are 5 cursor positions ranging from 0-4.
		skippedPos += Content.at(i).Length() + 1;
	}

	if (lineIndex < 0)
	{
		return -1; //out of bounds
	}

	FLOAT widthLimit = absX.ToFLOAT() - finalCoordinates.X - textData.at(lineIndex.ToUnsignedInt()).DrawOffset.x;
	INT curCharIndex = GetFont()->FindCharNearWidthLimit(Content.at(lineIndex.ToUnsignedInt()), GetCharacterSize(), widthLimit);

	//Move to cursor position that is right before the clicked character.  For example for string "abcd", if user clicked on character 'b', the charIdx returns 0, we want to place cursor after 'a'
	curCharIndex++;
		
	return (skippedPos + curCharIndex);
}

INT TextFieldComponent::CountNumCursorPositions (bool bFullText) const
{
	INT totalCursorPositions = 0;
	const INT numExtraPositionsPerLine = 1;
	for (UINT_TYPE i = 0; i < Content.size(); i++)
	{
		totalCursorPositions += Content.at(i).Length() + numExtraPositionsPerLine;
	}

	if (!bFullText)
	{
		return totalCursorPositions;
	}

	return totalCursorPositions;
}

Vector2 TextFieldComponent::CalcCharPosition (INT cursorIdx) const
{
	if (GetRenderComponent() == nullptr)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to calculate character position for %s since it does not possess a valid render component."), ToString());
		return Vector2(-1.f, -1.f);
	}

	if (cursorIdx < 0)
	{
		return Vector2(-1.f, -1.f);
	}

	INT localCursorIdx;
	INT lineIdx;
	Vector2 result;
	
	lineIdx = GetCursorLineData(cursorIdx, localCursorIdx);
	if (!ContainerUtils::IsValidIndex(GetRenderComponent()->ReadTexts(), lineIdx.ToUnsignedInt()))
	{
		return Vector2(-1.f, -1.f);
	}

	result = Vector2::SFMLtoSD(GetRenderComponent()->ReadTexts().at(lineIdx.ToUnsignedInt()).DrawOffset);

	DString partialLine = GetLine(lineIdx).Left(localCursorIdx);
	result.X += GetFont()->CalculateStringWidth(partialLine, GetCharacterSize());
	result += ReadCachedAbsPosition();

	return result;
}

INT TextFieldComponent::GetCursorLineData (INT cursorIdx, INT& outLocalCursorIdx) const
{
	if (cursorIdx < 0)
	{
		outLocalCursorIdx = INT_INDEX_NONE;
		return INT_INDEX_NONE;
	}

	INT beginningLineCursorIdx = 0;
	UINT_TYPE lineIdx = UINT_INDEX_NONE;

	//Find the line the cursorIdx is positioned.
	for (UINT_TYPE i = 0; i < Content.size(); i++)
	{
		if (Content.at(i).Length() + beginningLineCursorIdx >= cursorIdx)
		{
			lineIdx = i;
			break;
		}

		beginningLineCursorIdx += Content.at(i).Length() + 1;
	}

	if (lineIdx == UINT_INDEX_NONE)
	{
		outLocalCursorIdx = INT_INDEX_NONE;
		return INT_INDEX_NONE;
	}

	outLocalCursorIdx = cursorIdx - beginningLineCursorIdx;
	return lineIdx;
}

INT TextFieldComponent::GetCursorLineData (INT cursorIdx) const
{
	INT unusedLocalIdx = 0;
	return GetCursorLineData(cursorIdx, unusedLocalIdx);
}

void TextFieldComponent::CalculateActiveLineIndex ()
{
	INT localCursorPosition;
	ActiveLineIndex = GetCursorLineData(CursorPosition, localCursorPosition);
}

void TextFieldComponent::DeleteSelectedText ()
{
	if (HighlightEndPosition < 0 || CursorPosition < 0 || HighlightEndPosition == CursorPosition)
	{
		return;
	}

	DString fullText = GetContent();
	INT cursorTextPos = GetTextPosition();
	INT highlightEndLineIdx = GetCursorLineData(HighlightEndPosition);
	INT highlightEndTextIdx = GetTextPosition(HighlightEndPosition, highlightEndLineIdx);

	INT startIdx = (highlightEndTextIdx < cursorTextPos) ? highlightEndTextIdx : cursorTextPos;
	INT numCharsToDelete = (highlightEndTextIdx < cursorTextPos) ? (cursorTextPos - highlightEndTextIdx) : (highlightEndTextIdx - cursorTextPos);
	if (startIdx + numCharsToDelete > fullText.Length())
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to delete selected text for %s since the start highlight position (%s) and the number of characters to delete (%s) goes beyond the text length (%s)."), ToString(), startIdx, numCharsToDelete, fullText.Length());
		return;
	}

	fullText.Remove(startIdx, numCharsToDelete);
	SetText(fullText);
}

void TextFieldComponent::UpdateHighlightPositionPriorToMove (bool bShouldHighlight)
{
	if (bShouldHighlight && HighlightEndPosition < 0)
	{
		SetHighlightEndPosition(CursorPosition);
	}
	else if (!bShouldHighlight)
	{
		SetHighlightEndPosition(-1);
	}
}

void TextFieldComponent::UpdateHighlightShaderParameters ()
{
	Shader* highlighter = (GetRenderComponent() != nullptr) ? GetRenderComponent()->GetTextShader() : nullptr;
	if (highlighter == nullptr)
	{
		return;
	}

	bool bIsHighlighting = (HighlightEndPosition >= 0 && HighlightEndPosition != CursorPosition);
	highlighter->SFShader->setUniform("bIsHighlighting", bIsHighlighting);

	if (bIsHighlighting)
	{
		Vector2 startPos;
		Vector2 endPos;
		if (HighlightEndPosition < CursorPosition)
		{
			startPos = CalcCharPosition(HighlightEndPosition);
			endPos = CalcCharPosition(CursorPosition);
		}
		else
		{
			startPos = CalcCharPosition(CursorPosition);
			endPos = CalcCharPosition(HighlightEndPosition);
		}

		SDtoGLSLvector(startPos);
		SDtoGLSLvector(endPos);

		highlighter->SFShader->setUniform("FirstHighlightPos", Vector2::SDtoSFML(startPos));
		highlighter->SFShader->setUniform("LastHighlightPos", Vector2::SDtoSFML(endPos));
		highlighter->SFShader->setUniform("LineHeight", (FLOAT(CharacterSize) + LineSpacing).Value);
	}
}

void TextFieldComponent::SDtoGLSLvector (Vector2& outGLSLvector) const
{
	FLOAT compSizeY = ReadCachedAbsSize().Y;
	outGLSLvector.Y = compSizeY - outGLSLvector.Y;
}

void TextFieldComponent::PauseBlinker ()
{
	TextFieldRenderComponent* textRender = dynamic_cast<TextFieldRenderComponent*>(GetRenderComponent());
	if (textRender != nullptr)
	{
		textRender->PauseBlinker(textRender->MoveDetectionDuration);
	}
}

void TextFieldComponent::JumpCursorToPrevWord ()
{
	if (CursorPosition < 0)
	{
		return;
	}

	INT localCursorPosition;
	INT lineIdx = GetCursorLineData(CursorPosition, localCursorPosition);
	CHECK(lineIdx != INT_INDEX_NONE)
	DString curLine = GetLine(lineIdx);

	StringIterator iter(&curLine);
	for (INT i = 0; i < localCursorPosition; i++)
	{
		++iter;
	}

	bool bFoundSpace = false;
	INT newLocalPosition = localCursorPosition - 1;
	if (!iter.IsAtBeginning())
	{
		--iter; //ignore current character
		while (true)
		{
			if (!bFoundSpace)
			{
				//Keep moving until we found a space character
				bFoundSpace = (*iter == ' ');
			}
			else if (*iter != ' ') //found a non-space character before the space character previously found.
			{
				//Found a character before a space position. (move cursor after that character)
				newLocalPosition++;
				break; 
			}

			if (iter.IsAtBeginning())
			{
				newLocalPosition = 0;
				break;
			}

			--iter;
			newLocalPosition--;
		}
	}

	INT deltaCursorPos = localCursorPosition - newLocalPosition;
	SetCursorPosition(CursorPosition - deltaCursorPos);
}

void TextFieldComponent::JumpCursorToNextWord ()
{
	if (CursorPosition < 0)
	{
		return;
	}

	INT localCursorPosition;
	INT lineIdx = GetCursorLineData(CursorPosition, localCursorPosition);
	CHECK(lineIdx != INT_INDEX_NONE)
	DString curLine = GetLine(lineIdx);

	StringIterator iter(&curLine);
	for (INT i = 0; i < localCursorPosition; i++)
	{
		++iter;
	}

	bool bFoundSpace = false;
	INT newLocalPosition = localCursorPosition;
	for ( ; newLocalPosition < curLine.Length(); newLocalPosition++)
	{
		if (!bFoundSpace)
		{
			//Keep moving until we found a space character
			bFoundSpace = (*iter == ' ');
		}
		else if (*iter != ' ') //found a non space character after the first space character.
		{
			break; //Found a character after a space position.
		}

		++iter;
	}

	//Set deltaCursorPos to at least one to allow it to transfer between lines
	INT deltaCursorPos = Utils::Max<INT>(1, newLocalPosition - localCursorPosition);
	SetCursorPosition(CursorPosition + deltaCursorPos);
}

void TextFieldComponent::JumpCursorToBeginningLine ()
{
	INT skippedChars = 0;

	for (UINT_TYPE i = 0; i < Content.size(); i++)
	{
		if (i >= ActiveLineIndex)
		{
			break;
		}

		skippedChars += Content.at(i).Length() + 1; //include end line character
	}

	SetCursorPosition(skippedChars);
}

void TextFieldComponent::JumpCursorToEndLine ()
{
	INT skippedChars = 0;

	for (UINT_TYPE i = 0; i < Content.size(); i++)
	{
		skippedChars += Content.at(i).Length() + 1; //include end line character
		if (i >= ActiveLineIndex)
		{
			break;
		}
	}
	skippedChars--; //Move cursor at the end of current line instead of moving it to beginning of next line.

	SetCursorPosition(skippedChars);
}

void TextFieldComponent::JumpCursorToLine (INT deltaLines)
{
	INT localCursorPosition;
	INT oldLineIdx = GetCursorLineData(CursorPosition, localCursorPosition);
	if (oldLineIdx < 0)
	{
		return; //cursor not placed
	}

	FLOAT cursorPosX = GetFont()->CalculateStringWidth(Content.at(oldLineIdx.ToUnsignedInt()), GetCharacterSize(), 0, localCursorPosition);

	INT newLineIdx = Utils::Clamp<INT>(oldLineIdx + deltaLines, 0, Content.size() - 1);
	const DString& oldLine = Content.at(oldLineIdx.ToUnsignedInt());
	const DString& newLine = Content.at(newLineIdx.ToUnsignedInt());

	INT newLineCursorIdx = GetFont()->FindCharNearWidthLimit(newLine, GetCharacterSize(), cursorPosX);
	newLineCursorIdx++; //Include the character at cursorPosX

	//Identify how many cursor positions we'll need to move between current position to newLineCursorIdx
	INT newCursorPos = 0;
	if (deltaLines > 0)
	{
		newCursorPos = CursorPosition;
		newCursorPos += (oldLine.Length() + 1) - localCursorPosition; //Calc how many positions need to skip to exit cur line.
		newCursorPos += newLineCursorIdx; //Move from beginning of new line to specific spot in new line.

		//If skipping over multiple lines, we need to count how many additional cursor positions we'll need to skip over.
		for (INT i = oldLineIdx + 1; i < newLineIdx; i++)
		{
			newCursorPos += Content.at(i.ToUnsignedInt()).Length() + 1;
		}
	}
	else
	{
		newCursorPos = (CursorPosition - localCursorPosition) - 1;
		newCursorPos -= (newLine.Length() - newLineCursorIdx);

		//If skipping over multiple lines, we need to count how many additional cursor positions we'll need to skip over.
		for (INT i = oldLineIdx - 1; i > newLineIdx; i--)
		{
			newCursorPos -= (Content.at(i.ToUnsignedInt()).Length() + 1);
		}

		newCursorPos = Utils::Max<INT>(newCursorPos, 0); //Jump cursor to beginning of first line if user is moving up from first line.
	}

	SetCursorPosition(newCursorPos);
}

void TextFieldComponent::JumpCursorToBeginning ()
{
	SetCursorPosition(0);
}

void TextFieldComponent::JumpCursorToEnd ()
{
	//This function will automatically clamp to the max cursor position.
	SetCursorPosition(SD_MAXINT);
}

bool TextFieldComponent::ExecuteTextFieldInput (const sf::Event& keyEvent)
{
	if (CursorPosition < 0)
	{
		return false;
	}

	if (!bSelectable && !bEditable)
	{
		return false;
	}

	bool bIsReleaseEvent = (keyEvent.type == sf::Event::KeyReleased);
	if (!bIsReleaseEvent)
	{
		bIsCtrlCmd = (InputBroadcaster::GetCtrlHeld());
	}

	if (InputBroadcaster::GetCtrlHeld() || bIsCtrlCmd)
	{
		if (bIsReleaseEvent && keyEvent.key.code != sf::Keyboard::LControl && keyEvent.key.code != sf::Keyboard::RControl)
		{
			bIsCtrlCmd = false;
		}

		switch (keyEvent.key.code)
		{
			case(sf::Keyboard::A):
				if (bIsReleaseEvent)
				{
					//Select all
					JumpCursorToEnd();
					SetHighlightEndPosition(0);
				}
				return true;

			case(sf::Keyboard::C):
				if (bIsReleaseEvent)
				{
					OS_CopyToClipboard(GetSelectedText());
				}
				return true;

			case(sf::Keyboard::V):
				if (bEditable && bIsReleaseEvent)
				{
					DString pastedText = OS_PasteFromClipboard();
					if (!pastedText.IsEmpty())
					{
						DeleteSelectedText();
						if (CursorPosition > HighlightEndPosition && HighlightEndPosition >= 0)
						{
							SetCursorPosition(HighlightEndPosition);
						}

						SetHighlightEndPosition(-1);
						InsertTextAtCursor(pastedText);
					}
				}

				return true;

			case(sf::Keyboard::X):
				if (bIsReleaseEvent)
				{
					OS_CopyToClipboard(GetSelectedText());
					if (bEditable)
					{
						DeleteSelectedText();
						if (CursorPosition > HighlightEndPosition && HighlightEndPosition >= 0)
						{
							SetCursorPosition(HighlightEndPosition);
						}
						SetHighlightEndPosition(-1);
					}
				}
				return true;

			case(sf::Keyboard::End):
				if (!bIsReleaseEvent)
				{
					//Select text from current position to the end
					UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
					JumpCursorToEnd();
				}
				return true;

			case(sf::Keyboard::Home):
				if (!bIsReleaseEvent)
				{
					UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
					JumpCursorToBeginning();
				}
				return true;

			case(sf::Keyboard::Left):
				if (!bIsReleaseEvent)
				{
					UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
					JumpCursorToPrevWord();
				}
				return true;

			case(sf::Keyboard::Right):
				if (!bIsReleaseEvent)
				{
					UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
					JumpCursorToNextWord();
				}
				return true;

			case(sf::Keyboard::Up):
#if 0 //TODO:  Update camera position
				if (!bIsReleaseEvent && TextScrollbar != nullptr)
				{
					TextScrollbar->SetScrollPosition(TextScrollbar->GetScrollPosition() - 1);
				}
#endif

				return true;

			case(sf::Keyboard::Down):
#if 0 //TODO:  Update camera position
				if (!bIsReleaseEvent && TextScrollbar != nullptr && !bSingleLine)
				{
					TextScrollbar->SetScrollPosition(TextScrollbar->GetScrollPosition() + 1);
				}
#endif

				return true;

			case (sf::Keyboard::Delete):
				if (bIsReleaseEvent || !bEditable)
				{
					return true;
				}

				if (HighlightEndPosition >= 0)
				{
					DeleteSelectedText();
					if (CursorPosition > HighlightEndPosition)
					{
						SetCursorPosition(HighlightEndPosition);
					}
					SetHighlightEndPosition(-1);
				}
				else
				{
					//Delete rest of line from cursor position
					UpdateHighlightPositionPriorToMove(true);
					JumpCursorToEndLine();
					DeleteSelectedText();
					SetCursorPosition(HighlightEndPosition);
					SetHighlightEndPosition(-1);
					InsertTextAtCursor(TXT("\n"));
				}
				PauseBlinker();

				return true;
		}
	}

	switch (keyEvent.key.code)
	{
		case(sf::Keyboard::Escape):
			if (!bIsReleaseEvent)
			{
				SetCursorPosition(-1);
				SetHighlightEndPosition(-1);
			}
			return true;

		case(sf::Keyboard::Home):
			if (!bIsReleaseEvent)
			{
				UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
				JumpCursorToBeginningLine();
			}
			return true;

		case(sf::Keyboard::End):
			if (!bIsReleaseEvent)
			{
				UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
				JumpCursorToEndLine();
			}
			return true;

		case(sf::Keyboard::PageUp):
			if (!bIsReleaseEvent)
			{
#if 0 //TODO:  update camera position
				INT numLines = SD_MAXINT; //Select first line
				if (TextScrollbar != nullptr)
				{
					numLines = TextScrollbar->GetNumVisibleScrollPositions();
				}
#endif

				UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
				JumpCursorToLine(-5); //TODO:  replace with numLines
				//JumpCursorToLine(-numLines);
			}
			return true;

		case(sf::Keyboard::PageDown):
			if (!bIsReleaseEvent)
			{
				INT numLines = SD_MAXINT; //Select last line
#if 0 //TODO:  Update camera position
				if (TextScrollbar != nullptr)
				{
					numLines = TextScrollbar->GetNumVisibleScrollPositions();
				}

				UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
				JumpCursorToLine(numLines);
#endif
			}
			return true;

		case(sf::Keyboard::Left):
			if (!bIsReleaseEvent)
			{
				UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
				SetCursorPosition(Utils::Max<INT>(CursorPosition - 1, 0));
			}
			return true;

		case(sf::Keyboard::Right):
			if (!bIsReleaseEvent)
			{
				UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
				SetCursorPosition(CursorPosition + 1);
			}
			return true;

		case(sf::Keyboard::Up):
			if (!bIsReleaseEvent)
			{
				UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
				JumpCursorToLine(-1);
			}
			return true;

		case(sf::Keyboard::Down):
			if (!bIsReleaseEvent)
			{
				UpdateHighlightPositionPriorToMove(InputBroadcaster::GetShiftHeld());
				JumpCursorToLine(1);
			}
			return true;

		case(sf::Keyboard::Delete):
			if (bIsReleaseEvent || !bEditable)
			{
				return true;
			}

			if (HighlightEndPosition >= 0)
			{
				DeleteSelectedText();
				if (CursorPosition > HighlightEndPosition)
				{
					SetCursorPosition(HighlightEndPosition);
				}
				SetHighlightEndPosition(-1);
			}
			else
			{
				//delete next character
				DString fullText = GetContent();
				INT deleteIdx = GetTextPosition();
				if (fullText.Length() > 0 && deleteIdx < fullText.Length())
				{
					fullText.Remove(deleteIdx);
					SetText(fullText);
				}
			}

			//Forcibly make cursor render solid since the cursor position did not change here.
			PauseBlinker();

			return true;
				
	}

	//Capture input anyway since this text field is focused
	return true;
}

bool TextFieldComponent::ExecuteTextFieldText (const sf::Event& keyEvent)
{
	if (CursorPosition < 0 || InputBroadcaster::GetCtrlHeld() || !bEditable)
	{
		return false;
	}

	bool bWasHighlighted = (HighlightEndPosition >= 0);
	if (bWasHighlighted)
	{
		DeleteSelectedText();
		if (CursorPosition > HighlightEndPosition)
		{
			SetCursorPosition(HighlightEndPosition);
		}

		SetHighlightEndPosition(-1);
	}

	DString msg = sf::String(keyEvent.text.unicode);

	switch (keyEvent.text.unicode)
	{
		case(8): //backspace
			if (bWasHighlighted || CursorPosition <= 0)
			{
				return true;
			}

			//delete previous character
			{
				INT deleteIdx = GetTextPosition();
				deleteIdx--; //delete character before the cursor.
				if (deleteIdx >= 0)
				{
					DString fullText = GetContent();
					if (fullText.Length() <= 0)
					{
						return true;
					}

					fullText.Remove(deleteIdx);
					SetText(fullText);

					SetCursorPosition(CursorPosition - 1);
				}
				return true;
			}
				
		case(9): //tab
			msg = TXT("\t");
			break;

		case(13): //return
#if 0 //TODO:  single line text fields
			if (bSingleLine)
			{
				//Can't create new line characters for single line text fields.
				return true;
			}
#endif

			if (OnReturn.IsBounded())
			{
				OnReturn(this);
				SetCursorPosition(-1);
				return true;
			}

			msg = TXT("\n");
			break;

		case(27): //escape
			SetCursorPosition(-1);
			//TODO: Restore previous text
			return true;
	}

	if (OnAllowTextInput.IsBounded() && !OnAllowTextInput(msg))
	{
		return false;
	}

	InsertTextAtCursor(msg);
	return true;
}

Texture* TextFieldComponent::HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent)
{
	if (OverridingMouseIcon == nullptr)
	{
		return nullptr;
	}

	bool isHovering = ((bEditable || bSelectable) && IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))));
	if (!isHovering)
	{
		OverridingMouseIcon = nullptr;
	}

	return OverridingMouseIcon.Get();
}

void TextFieldComponent::HandleShaderChanged ()
{
	UpdateHighlightShaderParameters();
}
SD_END