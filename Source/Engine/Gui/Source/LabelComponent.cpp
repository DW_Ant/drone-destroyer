/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LabelComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, LabelComponent, SD, GuiComponent)

void LabelComponent::InitProps ()
{
	Super::InitProps();

	if (GuiTheme::GetGuiTheme() != nullptr)
	{
		SetFont(GuiTheme::GetGuiTheme()->GuiFont.Get());
	}

	SetCharacterSize(16);
	bAutoRefresh = true;
	bWrapText = true;
	bClampText = true;
	AutoSizeHorizontal = false;
	AutoSizeVertical = false;
	LineSpacing = 4;
	FirstLineOffset = 0.f;
	ClampedText = DString::EmptyString;
	RenderComponent = nullptr;
	RenderComponentClass = TextRenderComponent::SStaticClass();
	VerticalAlignment = VA_Top;
	HorizontalAlignment = HA_Left;
	MaxNumLines = 1;
}

void LabelComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const LabelComponent* labelTemplate = dynamic_cast<const LabelComponent*>(objTemplate);
	if (labelTemplate != nullptr)
	{
		//Handle RenderComponent first since various LabelComponent properties (such as setting font) affects the render component
		bool bCreatedObj;
		RenderComponent = ReplaceTargetWithObjOfMatchingClass(RenderComponent.Get(), labelTemplate->GetRenderComponent(), bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(RenderComponent.Get());
		}

		SetAutoRefresh(false);
		SetFont(labelTemplate->GetFont());
		SetCharacterSize(labelTemplate->GetCharacterSize());
		SetWrapText(labelTemplate->GetWrapText());
		SetClampText(labelTemplate->GetClampText());
		SetLineSpacing(labelTemplate->GetLineSpacing());
		FirstLineOffset = labelTemplate->FirstLineOffset;
		SetVerticalAlignment(labelTemplate->GetVerticalAlignment());
		SetHorizontalAlignment(labelTemplate->GetHorizontalAlignment());
		SetAutoRefresh(true);
	}
}

void LabelComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	InitializeRenderComponent();
	CalculateMaxNumLines();
}

void LabelComponent::HandleAbsTransformChange ()
{
	Super::HandleAbsTransformChange();

	if (bAutoRefresh)
	{
		RefreshText();
	}
}

void LabelComponent::CreateLabelWithinScrollbar (ScrollbarComponent* viewer, GuiEntity*& outLabelOwner, LabelComponent*& outLabelComp)
{
	CHECK(viewer != nullptr) //A ScrollbarComponent must be specified.

	outLabelOwner = GuiEntity::CreateObject();
	outLabelOwner->SetAutoSizeVertical(true);
	outLabelOwner->SetEnableFractionScaling(false);
	outLabelOwner->SetGuiSizeToOwningScrollbar(Vector2(1.f, -1.f));

	outLabelComp = LabelComponent::CreateObject();
	if (outLabelOwner->AddComponent(outLabelComp))
	{
		outLabelComp->SetAutoRefresh(false);
		outLabelComp->SetClampText(false);
		outLabelComp->SetWrapText(true);
		outLabelComp->SetEnableFractionScaling(true);
		outLabelComp->SetSize(Vector2(1.f, 1.f));
		outLabelComp->SetPosition(Vector2::ZeroVector);
		outLabelComp->SetAutoSizeVertical(true);
		outLabelComp->SetAutoSizeHorizontal(false);
		outLabelComp->SetAutoRefresh(true);
		viewer->SetViewedObject(outLabelOwner);
	}
	else
	{
		//AddComponent destroyed the label, explicitly set it to nullptr
		outLabelComp = nullptr;
		outLabelOwner->Destroy();
		outLabelOwner = nullptr;
	}
}

void LabelComponent::SetFont (const Font* newFont)
{
	CurrentFont = newFont;

	if (RenderComponent != nullptr)
	{
		RenderComponent->SetTextFont(CurrentFont.Get());
	}

	CalculateMaxNumLines();

	if (bAutoRefresh)
	{
		RefreshText();
	}
}

void LabelComponent::SetCharacterSize (INT newCharacterSize)
{
	CharacterSize = newCharacterSize;

	if (RenderComponent != nullptr)
	{
		RenderComponent->SetFontSize(CharacterSize);
	}

	if (bAutoRefresh)
	{
		RefreshText();
	}
}

void LabelComponent::SetText (const DString& newContent)
{
	//Reset label's text
	ContainerUtils::Empty(Content);
	Content.push_back(newContent); //Place everything on first line
	ClampedText = DString::EmptyString;

	if (bAutoRefresh)
	{
		RefreshText();
	}
}

void LabelComponent::RefreshText ()
{
	ResetContent();
	CalculateMaxNumLines();
	ParseNewLineCharacters();

	if (AutoSizeHorizontal)
	{
		ApplyHorizontalAutoSize();
	}
	else
	{
		WrapText();
	}

	if (AutoSizeVertical)
	{
		ApplyVerticalAutoSize();
	}
	else
	{
		ClampText(); //This function updates the ContentBody and ContentTail based on the truncated text
	}

	if (RenderComponent != nullptr)
	{
		//Reset render component
		RenderComponent->DeleteAllText();

		RenderContent();

		AlignVertically();
		AlignHorizontally();
	}
}

void LabelComponent::SetAutoRefresh (bool bNewAutoRefresh)
{
	if (bAutoRefresh != bNewAutoRefresh)
	{
		bAutoRefresh = bNewAutoRefresh;
		if (bAutoRefresh)
		{
			RefreshText();
		}
	}
}

void LabelComponent::SetWrapText (bool bNewWrapText)
{
	bWrapText = bNewWrapText;

	if (bAutoRefresh)
	{
		RefreshText();
	}
}

void LabelComponent::SetClampText (bool bNewClampText)
{
	bClampText = bNewClampText;

	if (bClampText && bAutoRefresh)
	{
		ClampText();
	}
}

void LabelComponent::SetAutoSizeHorizontal (bool newAutoSizeHorizontal)
{
	AutoSizeHorizontal = newAutoSizeHorizontal;

	if (bAutoRefresh && AutoSizeHorizontal)
	{
		RefreshText();
	}
}

void LabelComponent::SetAutoSizeVertical (bool newAutoSizeVertical)
{
	AutoSizeVertical = newAutoSizeVertical;

	if (bAutoRefresh && AutoSizeVertical)
	{
		RefreshText();
	}
}

void LabelComponent::SetLineSpacing (FLOAT newLineSpacing)
{
	LineSpacing = newLineSpacing;

	INT oldMaxNumLines = MaxNumLines;
	CalculateMaxNumLines();

	if (!bAutoRefresh)
	{
		return;
	}

	if (oldMaxNumLines != MaxNumLines)
	{
		RefreshText(); //Do a full refresh since more text can be revealed or clamped.
	}
	else
	{
		AlignVertically();
	}
}

void LabelComponent::SetVerticalAlignment (eVerticalAlignment newVerticalAlignment)
{
	VerticalAlignment = newVerticalAlignment;

	if (bAutoRefresh)
	{
		AlignVertically();
	}
}

void LabelComponent::SetHorizontalAlignment (eHorizontalAlignment newHorizontalAlignment)
{
	HorizontalAlignment = newHorizontalAlignment;

	if (bAutoRefresh)
	{
		AlignHorizontally();
	}
}

void LabelComponent::SetRenderComponent (TextRenderComponent* newRenderComponent)
{
	if (RenderComponent != nullptr)
	{
		RenderComponent->Destroy();
	}

	RenderComponent = newRenderComponent;
	if (RenderComponent != nullptr)
	{
		if (AddComponent(RenderComponent.Get()))
		{
			RenderComponent->SetTextFont(CurrentFont.Get());
			RenderComponent->SetFontSize(CharacterSize);
		}
	}
}

const Font* LabelComponent::GetFont () const
{
	return CurrentFont.Get();
}

INT LabelComponent::GetCharacterSize () const
{
	return CharacterSize;
}

bool LabelComponent::GetAutoRefresh () const
{
	return bAutoRefresh;
}

bool LabelComponent::GetWrapText () const
{
	return bWrapText;
}

bool LabelComponent::GetClampText () const
{
	return bClampText;
}

bool LabelComponent::GetAutoSizeHorizontal () const
{
	return AutoSizeHorizontal;
}

bool LabelComponent::GetAutoSizeVertical () const
{
	return AutoSizeVertical;
}

FLOAT LabelComponent::GetLineSpacing () const
{
	return LineSpacing;
}

FLOAT LabelComponent::GetFirstLineOffset () const
{
	return FirstLineOffset;
}

DString LabelComponent::GetContent () const
{
	DString results = DString::EmptyString;

	for (UINT_TYPE i = 0; i < Content.size(); ++i)
	{
		results += Content.at(i);
	}

	return results;
}

void LabelComponent::GetContent (std::vector<DString>& outContent) const
{
	outContent = Content;
}

std::vector<DString>& LabelComponent::EditContent ()
{
	return Content;
}

const std::vector<DString>& LabelComponent::ReadContent () const
{
	return Content;
}

FLOAT LabelComponent::GetLineHeight () const
{
	return CharacterSize.ToFLOAT() + LineSpacing;
}

DString LabelComponent::GetLine (INT lineIdx) const
{
	if (!ContainerUtils::IsValidIndex(Content, lineIdx))
	{
		return DString::EmptyString;
	}

	return Content.at(lineIdx.ToUnsignedInt());
}

LabelComponent::eVerticalAlignment LabelComponent::GetVerticalAlignment () const
{
	return VerticalAlignment;
}

LabelComponent::eHorizontalAlignment LabelComponent::GetHorizontalAlignment () const
{
	return HorizontalAlignment;
}

INT LabelComponent::GetMaxNumLines () const
{
	return MaxNumLines;
}

TextRenderComponent* LabelComponent::GetRenderComponent () const
{
	return RenderComponent.Get();
}

void LabelComponent::InitializeRenderComponent ()
{
	TextRenderComponent* newRenderComponent = dynamic_cast<TextRenderComponent*>(RenderComponentClass->GetDefaultObject()->CreateObjectOfMatchingClass());
	CHECK(newRenderComponent != nullptr)
	SetRenderComponent(newRenderComponent);
}

void LabelComponent::ResetContent ()
{
	for (size_t i = 1; i < Content.size(); ++i)
	{
		Content[0] += std::move(Content[i]);
	}

	if (!ContainerUtils::IsEmpty(Content))
	{
		Content.resize(1); //Delete lines 2-n from vector

		//Append any clamped text to the first line.
		Content.at(0) += std::move(ClampedText);
		//std::move doesn't zero out the memory. Make sure we reassign ClampedText to an empty string.
	}

	ClampedText = DString::EmptyString;
}

bool LabelComponent::ParseNewLineCharacters ()
{
	if (RenderComponent == nullptr)
	{
		return false; //Can't fit all the text here since there are no defined boundaries to render the text in.
	}

	for (UINT_TYPE i = 0; i < Content.size(); i++)
	{
		INT curCharIndex = Content.at(i).ReadString().find_first_of('\n');

		//Exit loop if no new line characters were found, or if it's at the end
		if (curCharIndex == std::string::npos || curCharIndex >= Content.at(i).Length() - 1)
		{
			return (RenderComponent->ReadTexts().size() <= MaxNumLines);
		}

		//Transfer text after the charIndex to the next line
		Content.push_back(Content.at(i).SubString(curCharIndex + 1));
		Content.at(i) = Content.at(i).SubString(0,curCharIndex);
	}
		
	return (Content.size() <= MaxNumLines);
}

void LabelComponent::WrapText ()
{
	if (!bWrapText)
	{
		return;
	}

	std::vector<char> wrapCharacters;
	wrapCharacters.push_back(' ');
	wrapCharacters.push_back('-');

	const FLOAT width = (ReadCachedAbsSize().X > 0.f) ? ReadCachedAbsSize().X : ReadSize().X; //If abs size is not yet computed, try local size.

	for (UINT_TYPE i = 0; i < Content.size(); i++)
	{
		INT charIndex = CurrentFont->FindCharNearWidthLimit(Content.at(i), CharacterSize, width);

		if (charIndex >= Content.at(i).Length() - 1 || charIndex <= 0)
		{
			continue; //Either All characters in this line is within width or the first character is beyond width.
		}

		//Find the place to insert a new line character (usually between words)
		INT wrapPosition = FindPreviousCharacter(wrapCharacters, i, charIndex.Value);
		if (wrapPosition == INT_INDEX_NONE)
		{
			//Was unable to find a good place to wrap text.  Wrap at the character that went beyond boundaries.
			wrapPosition = charIndex;
		}
		else
		{
			//Increment by one to keep the current character at the previous line
			wrapPosition++;
		}

		//Divide the string into two parts
		Content.insert(Content.begin() + i + 1, Content.at(i).SubString(wrapPosition));
		Content.at(i) = Content.at(i).SubString(0, wrapPosition - 1);
	}
}

void LabelComponent::ClampText ()
{
	if (!bClampText || Content.size() <= 0)
	{
		return;
	}

	if (bWrapText)
	{
		//Figure out how many lines needs to be clamped.
		INT numClampedLines = INT(Content.size()) - MaxNumLines;
		if (numClampedLines > 0) //If Content size is greater than the permitted number of lines
		{
			DString oldClamp = std::move(ClampedText);
			ClampedText = DString::EmptyString; //std::move doesn't zero out the memory

			for (INT curLine = MaxNumLines; curLine < INT(Content.size()); ++curLine)
			{
				ClampedText += std::move(Content.at(curLine.ToUnsignedInt()));
			}
			ClampedText += oldClamp;

			//Efficiently truncate the content vector
			Content.resize(MaxNumLines.ToUnsignedInt());
		}
	}
	else //!bWrapText
	{
		if (Content.at(Content.size() - 1).Length() <= 0)
		{
			//Nothing to clamp for an empty string
			return;
		}

		//Simply remove the text beyond width from the last line.  90% of the time there will only be one line of text for unwrapped label components.
		//For multi-lined unwrapped label components, it's assumed that the predetermined line breaks are there for a particular reason and will not truncate that.
		INT charIdx = CurrentFont->FindCharNearWidthLimit(Content.at(Content.size() - 1), CharacterSize, ReadCachedAbsSize().X);

		//If charIdx is in middle of Content
		if (charIdx > 0 && charIdx < ContainerUtils::GetLast(Content).Length() - 1)
		{
			ClampedText = ContainerUtils::GetLast(Content).SubString(charIdx + 1);
			Content.at(Content.size() - 1) = ContainerUtils::GetLast(Content).SubString(0, charIdx);
		}
	}
}

void LabelComponent::ApplyHorizontalAutoSize ()
{
	if (!AutoSizeHorizontal)
	{
		return;
	}
	
	FLOAT widestLine = 0.f;
	for (UINT_TYPE i = 0; i < Content.size(); ++i)
	{
		widestLine = Utils::Max(CurrentFont->CalculateStringWidth(Content.at(i), CharacterSize), widestLine);
	}

	SetSize(Vector2(widestLine, ReadSize().Y));
}

void LabelComponent::ApplyVerticalAutoSize ()
{
	if (!AutoSizeVertical)
	{
		return;
	}

	INT numLines(Content.size());
	SetSize(Vector2(ReadSize().X, numLines.ToFLOAT() * (CharacterSize.ToFLOAT() + LineSpacing)));
}

void LabelComponent::RenderContent ()
{
	if (CurrentFont == nullptr)
	{
		return;
	}

	for (UINT_TYPE i = 0; i < Content.size(); i++)
	{
		TextRenderComponent::STextData& newLineData = RenderComponent->CreateTextInstance(Content.at(i), CurrentFont.Get(), CharacterSize.ToUnsignedInt32());
		newLineData.DrawOffset.y = ((CharacterSize.ToFLOAT() + LineSpacing) * FLOAT::MakeFloat(i)).Value;
	}
}

void LabelComponent::AlignVertically ()
{
	FLOAT lastLinePos = ReadCachedAbsPosition().Y + ((CharacterSize.ToFLOAT() + LineSpacing) * FLOAT::MakeFloat(RenderComponent->ReadTexts().size()));
	FLOAT unusedSpace = (ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y) - lastLinePos.Value;
	FirstLineOffset = 0.f; //Distance between the top of this component's yPos and the first Text's yPos.

	//Clamp unused space in case it's negative (when the characters are larger than the component, itself
	unusedSpace = Utils::Max<FLOAT>(0.f, unusedSpace);
	if (AutoSizeVertical)
	{
		//For auto sized components, the text should fill the component from top to bottom.  Always align top in this case.
		unusedSpace = 0.f;
	}

	switch(VerticalAlignment)
	{
		case(VA_Top):
			break;
		case(VA_Center):
			FirstLineOffset = unusedSpace / 2;
			break;

		case(VA_Bottom):
			FirstLineOffset = unusedSpace;
			break;
	}

	for (UINT_TYPE i = 0; i < RenderComponent->EditTexts().size(); i++)
	{
		RenderComponent->EditTexts().at(i).DrawOffset.y = (FirstLineOffset + ((CharacterSize.ToFLOAT() + LineSpacing) * FLOAT::MakeFloat(i))).Value;
	}
}

void LabelComponent::AlignHorizontally ()
{
	if (CurrentFont == nullptr)
	{
		return;
	}

	//Use faster/simpler algorithm when aligning text to the left
	if (HorizontalAlignment == HA_Left)
	{
		for (UINT_TYPE i = 0; i < RenderComponent->EditTexts().size(); i++)
		{
			RenderComponent->EditTexts().at(i).DrawOffset.x = 0.f;
		}

		return;
	}

	for (UINT_TYPE i = 0; i < Content.size() && i < RenderComponent->EditTexts().size(); i++)
	{
		FLOAT lineWidth = CurrentFont->CalculateStringWidth(Content.at(i), CharacterSize);
		FLOAT unusedSpace = ReadCachedAbsSize().X - lineWidth;
		unusedSpace = Utils::Max<FLOAT>(unusedSpace, 0.f); //Avoid negative values (this could happen with unwrapped lines going beyond width).
		FLOAT horizontalShift = 0.f;

		switch (HorizontalAlignment)
		{
			case(HA_Center):
				horizontalShift = unusedSpace / 2.f;
				break;

			case(HA_Right):
				horizontalShift = unusedSpace;
				break;
		}

		RenderComponent->EditTexts().at(i).DrawOffset.x = horizontalShift.Value;
	}
}

INT LabelComponent::FindPreviousCharacter (const std::vector<char>& targetCharacters, UINT_TYPE textLine, INT startPosition) const
{
	if (Content.at(textLine).Length() <= 0)
	{
		return INT_INDEX_NONE;
	}

	if (startPosition < 0)
	{
		startPosition = Content.at(textLine).Length() - 1;
	}

	StringIterator iter(&Content.at(textLine));
	for (INT i = 0; i < startPosition; i++)
	{
		++iter;
	}

	INT charIdx = startPosition;
	while (true)
	{
		for (UINT_TYPE j = 0; j < targetCharacters.size(); j++)
		{
			if (targetCharacters.at(j) == *iter)
			{
				return charIdx;
			}
		}

		if (iter.IsAtBeginning())
		{
			break;
		}
		--iter;
		charIdx--;
	}

	return INT_INDEX_NONE;
}

void LabelComponent::CalculateMaxNumLines ()
{
	INT oldMaxLines = MaxNumLines;

	if (RenderComponent == nullptr)
	{
		MaxNumLines = 1;
	}
	else
	{
		FLOAT result = ReadCachedAbsSize().Y / (CharacterSize.ToFLOAT() + LineSpacing);
		result = trunc(result.Value);
		MaxNumLines = Utils::Max<INT>(1, result.ToINT());
	}
}
SD_END