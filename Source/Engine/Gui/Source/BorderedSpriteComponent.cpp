/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BorderedSpriteComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, BorderedSpriteComponent, SD, SpriteComponent)

void BorderedSpriteComponent::InitProps ()
{
	Super::InitProps();

	TopBorder = nullptr;
	RightBorder = nullptr;
	BottomBorder = nullptr;
	LeftBorder = nullptr;
	TopRightCorner = nullptr;
	BottomRightCorner = nullptr;
	BottomLeftCorner = nullptr;
	TopLeftCorner = nullptr;

	FillColor = Color(128, 128, 128);
	BorderThickness = 8;
}

void BorderedSpriteComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const BorderedSpriteComponent* spriteTemplate = dynamic_cast<const BorderedSpriteComponent*>(objTemplate);
	if (spriteTemplate != nullptr)
	{
		TopBorder = spriteTemplate->TopBorder;
		RightBorder = spriteTemplate->RightBorder;
		BottomBorder = spriteTemplate->BottomBorder;
		LeftBorder = spriteTemplate->LeftBorder;
		TopRightCorner = spriteTemplate->TopRightCorner;
		BottomRightCorner = spriteTemplate->BottomRightCorner;
		BottomLeftCorner = spriteTemplate->BottomLeftCorner;
		TopLeftCorner = spriteTemplate->TopLeftCorner;

		FillColor = spriteTemplate->FillColor;
		BorderThickness = spriteTemplate->BorderThickness;
	}
}

void BorderedSpriteComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	//Scene transforms aren't supported due to the way the borders are rendered (ignoring rotation and depth).
	CHECK_INFO(dynamic_cast<const PlanarTransform*>(GetOwnerTransform()) != nullptr, TXT("BorderedSpriteComponents only supports PlanarTransforms."))

	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);
	sf::Vector2f fullSize = projectionData.GetFinalSize();

	//Draw the fill texture first to render it behind the borders.
	if (Sprite != nullptr && !InvisibleTexture)
	{
		Super::Render(renderTarget, camera);
	}
	else if (FillColor.Source.a > 0)
	{
		sf::RectangleShape background;
		background.setPosition(projectionData.Position);
		background.setScale(fullSize);
		background.setRotation(projectionData.Rotation);
		background.setOrigin(projectionData.Pivot);
		background.setSize(sf::Vector2f(1.f, 1.f));
		background.setFillColor(FillColor.Source);
		renderTarget->Draw(background);
	}

	//Draw borders
	if (TopBorder != nullptr)
	{
		Vector2 borderSpriteSize;
		TopBorder->GetDimensions(OUT borderSpriteSize);

		sf::Sprite topSprite;
		topSprite.setTexture(*TopBorder->GetTextureResource());
		topSprite.setPosition(projectionData.Position.x + BorderThickness.Value, projectionData.Position.y);
		topSprite.setScale(((fullSize.x - BorderThickness * 2.f)/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
		renderTarget->Draw(topSprite);
	}

	if (RightBorder != nullptr)
	{
		Vector2 borderSpriteSize;
		RightBorder->GetDimensions(OUT borderSpriteSize);

		sf::Sprite rightSprite;
		rightSprite.setTexture(*RightBorder->GetTextureResource());
		rightSprite.setPosition((projectionData.Position.x + fullSize.x - BorderThickness).Value, projectionData.Position.y + BorderThickness.Value);
		rightSprite.setScale((BorderThickness/borderSpriteSize.X).Value, ((fullSize.y - BorderThickness * 2.f)/borderSpriteSize.Y).Value);
		renderTarget->Draw(rightSprite);
	}

	if (BottomBorder != nullptr)
	{
		Vector2 borderSpriteSize;
		BottomBorder->GetDimensions(OUT borderSpriteSize);

		sf::Sprite bottomSprite;
		bottomSprite.setTexture(*BottomBorder->GetTextureResource());

		bottomSprite.setPosition(projectionData.Position.x + BorderThickness.Value, (projectionData.Position.y + fullSize.y - BorderThickness).Value);
		bottomSprite.setScale(((fullSize.x - BorderThickness * 2)/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
		renderTarget->Draw(bottomSprite);
	}

	if (LeftBorder != nullptr)
	{
		Vector2 borderSpriteSize;
		LeftBorder->GetDimensions(OUT borderSpriteSize);

		sf::Sprite leftSprite;
		leftSprite.setTexture(*LeftBorder->GetTextureResource());
		leftSprite.setPosition(projectionData.Position.x, (projectionData.Position.y + BorderThickness).Value);
		leftSprite.setScale((BorderThickness/borderSpriteSize.X).Value, ((fullSize.y - BorderThickness * 2.f)/borderSpriteSize.Y).Value);
		renderTarget->Draw(leftSprite);
	}

	//Draw corners
	if (TopRightCorner != nullptr)
	{
		Vector2 borderSpriteSize;
		TopRightCorner->GetDimensions(OUT borderSpriteSize);

		sf::Sprite topRightSprite;
		topRightSprite.setTexture(*TopRightCorner->GetTextureResource());
		topRightSprite.setPosition((projectionData.Position.x + fullSize.x - BorderThickness).Value, projectionData.Position.y);
		topRightSprite.setScale((BorderThickness.Value/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
		renderTarget->Draw(topRightSprite);
	}

	if (BottomRightCorner != nullptr)
	{
		Vector2 borderSpriteSize;
		BottomRightCorner->GetDimensions(OUT borderSpriteSize);

		sf::Sprite bottomRightSprite;
		bottomRightSprite.setTexture(*BottomRightCorner->GetTextureResource());
		bottomRightSprite.setPosition((projectionData.Position.x + fullSize.x - BorderThickness).Value, (projectionData.Position.y + fullSize.y - BorderThickness).Value);
		bottomRightSprite.setScale((BorderThickness/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
		renderTarget->Draw(bottomRightSprite);
	}

	if (BottomLeftCorner != nullptr)
	{
		Vector2 borderSpriteSize;
		BottomLeftCorner->GetDimensions(OUT borderSpriteSize);

		sf::Sprite bottomLeftSprite;
		bottomLeftSprite.setTexture(*BottomLeftCorner->GetTextureResource());
		bottomLeftSprite.setPosition(projectionData.Position.x, (projectionData.Position.y + fullSize.y - BorderThickness).Value);
		bottomLeftSprite.setScale((BorderThickness/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
		renderTarget->Draw(bottomLeftSprite);
	}

	if (TopLeftCorner != nullptr)
	{
		Vector2 borderSpriteSize;
		TopLeftCorner->GetDimensions(OUT borderSpriteSize);

		sf::Sprite topLeftSprite;
		topLeftSprite.setTexture(*TopLeftCorner->GetTextureResource());
		topLeftSprite.setPosition(projectionData.Position.x, projectionData.Position.y);
		topLeftSprite.setScale((BorderThickness/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
		renderTarget->Draw(topLeftSprite);
	}
}
SD_END