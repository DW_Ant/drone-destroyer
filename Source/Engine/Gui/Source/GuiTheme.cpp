/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiTheme.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, GuiTheme, SD, Object)

const DString GuiTheme::DEFAULT_STYLE_NAME = TXT("DefaultStyle");

GuiTheme::SStyleInfo::SStyleInfo () :
	Name(DString::EmptyString)
{

}

void GuiTheme::InitProps ()
{
	Super::InitProps();

	DefaultStyleIdx = 0;
}

void GuiTheme::Destroyed ()
{
	while (Styles.size() > 0)
	{
		for (GuiComponent* uiTemplate : Styles.at(0).Templates)
		{
			uiTemplate->Destroy();
		}

		Styles.erase(Styles.begin());
	}

	Super::Destroyed();
}

GuiTheme::SStyleInfo GuiTheme::CreateStyleFrom (const GuiTheme::SStyleInfo& copyObj)
{
	SStyleInfo newStyle;

	newStyle.Name = copyObj.Name;
	for (GuiComponent* uiTemplate : copyObj.Templates)
	{
		GuiComponent* cpyTemplate = uiTemplate->CreateObjectOfMatchingClass();
		CHECK(cpyTemplate != nullptr)
		cpyTemplate->CopyPropertiesFrom(uiTemplate);
		newStyle.Templates.push_back(cpyTemplate);
	}

	return newStyle;
}

void GuiTheme::InitializeTheme ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	//Mouse Pointer Icons
	TextPointer = localTexturePool->FindTexture(TXT("Interface.MouseIcons.CursorText"));
	ResizePointerVertical = localTexturePool->FindTexture(TXT("Interface.MouseIcons.CursorResizeVertical"));
	ResizePointerHorizontal = localTexturePool->FindTexture(TXT("Interface.MouseIcons.CursorResizeHorizontal"));
	ResizePointerDiagonalTopLeft = localTexturePool->FindTexture(TXT("Interface.MouseIcons.CursorResizeTopLeft"));
	ResizePointerDiagonalBottomLeft = localTexturePool->FindTexture(TXT("Interface.MouseIcons.CursorResizeBottomLeft"));
	ScrollbarPointer = localTexturePool->FindTexture(TXT("Interface.MouseIcons.CursorScrollbarPan"));

	//Frame component textures
	FrameFill = localTexturePool->FindTexture(TXT("Interface.DefaultFrameComponentFill"));
	FrameBorderTop = localTexturePool->FindTexture(TXT("Interface.FrameBorderTop"));
	FrameBorderRight = localTexturePool->FindTexture(TXT("Interface.FrameBorderRight"));
	FrameBorderBottom = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottom"));
	FrameBorderLeft = localTexturePool->FindTexture(TXT("Interface.FrameBorderLeft"));
	FrameBorderTopRight = localTexturePool->FindTexture(TXT("Interface.FrameBorderTopRight"));
	FrameBorderBottomRight = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottomRight"));
	FrameBorderBottomLeft = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottomLeft"));
	FrameBorderTopLeft = localTexturePool->FindTexture(TXT("Interface.FrameBorderTopLeft"));

	CheckboxTexture = localTexturePool->FindTexture(TXT("Interface.Checkbox"));

	//Button component textures
	ButtonSingleSpriteBackground = localTexturePool->FindTexture(TXT("Interface.Buttons.TextureTestStates"));
	ButtonFill = localTexturePool->FindTexture(TXT("Interface.DefaultFrameComponentFill"));
	ButtonBorderTop = localTexturePool->FindTexture(TXT("Interface.FrameBorderTop"));
	ButtonBorderRight = localTexturePool->FindTexture(TXT("Interface.FrameBorderRight"));
	ButtonBorderBottom = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottom"));
	ButtonBorderLeft = localTexturePool->FindTexture(TXT("Interface.FrameBorderLeft"));
	ButtonBorderTopRight = localTexturePool->FindTexture(TXT("Interface.FrameBorderTopRight"));
	ButtonBorderBottomRight = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottomRight"));
	ButtonBorderBottomLeft = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottomLeft"));
	ButtonBorderTopLeft = localTexturePool->FindTexture(TXT("Interface.FrameBorderTopLeft"));
		
	//Scrollbar component textures
	ScrollbarUpButton = localTexturePool->FindTexture(TXT("Interface.Buttons.ScrollUpButton"));
	ScrollbarDownButton = localTexturePool->FindTexture(TXT("Interface.Buttons.ScrollDownButton"));
	ScrollbarLeftButton = localTexturePool->FindTexture(TXT("Interface.Buttons.ScrollLeftButton"));
	ScrollbarRightButton = localTexturePool->FindTexture(TXT("Interface.Buttons.ScrollRightButton"));
	ScrollbarZoomButton = localTexturePool->FindTexture(TXT("Interface.Buttons.ScrollZoomButton"));
	ScrollbarMiddleMouseScrollAnchor = localTexturePool->FindTexture(TXT("Interface.ScrollbarMiddleMouseSprite"));
	ScrollbarMouseAnchorStationary = localTexturePool->FindTexture(TXT("Interface.ScrollbarMouseAnchorStationary"));
	ScrollbarMouseAnchorPanUp = localTexturePool->FindTexture(TXT("Interface.ScrollbarMouseAnchorPanUp"));
	ScrollbarMouseAnchorPanUpRight = localTexturePool->FindTexture(TXT("Interface.ScrollbarMouseAnchorPanUpRight"));
	ScrollbarMouseAnchorPanRight = localTexturePool->FindTexture(TXT("Interface.ScrollbarMouseAnchorPanRight"));
	ScrollbarMouseAnchorPanDownRight = localTexturePool->FindTexture(TXT("Interface.ScrollbarMouseAnchorPanDownRight"));
	ScrollbarMouseAnchorPanDown = localTexturePool->FindTexture(TXT("Interface.ScrollbarMouseAnchorPanDown"));
	ScrollbarMouseAnchorPanDownLeft = localTexturePool->FindTexture(TXT("Interface.ScrollbarMouseAnchorPanDownLeft"));
	ScrollbarMouseAnchorPanLeft = localTexturePool->FindTexture(TXT("Interface.ScrollbarMouseAnchorPanLeft"));
	ScrollbarMouseAnchorPanUpLeft = localTexturePool->FindTexture(TXT("Interface.ScrollbarMouseAnchorPanUpLeft"));
	ScrollbarTrackFill = localTexturePool->FindTexture(TXT("Interface.ScrollbarThumbFill"));
	ScrollbarTrackBorderTop = localTexturePool->FindTexture(TXT("Interface.ScrollbarThumbBorderTop"));
	ScrollbarTrackBorderRight = localTexturePool->FindTexture(TXT("Interface.ScrollbarThumbBorderRight"));
	ScrollbarTrackBorderBottom = localTexturePool->FindTexture(TXT("Interface.ScrollbarThumbBorderBottom"));
	ScrollbarTrackBorderLeft = localTexturePool->FindTexture(TXT("Interface.ScrollbarThumbBorderLeft"));
	ScrollbarTrackBorderTopRight = localTexturePool->FindTexture(TXT("Interface.ScrollbarThumbBorderTopRight"));
	ScrollbarTrackBorderBottomRight = localTexturePool->FindTexture(TXT("Interface.ScrollbarThumbBorderBottomRight"));
	ScrollbarTrackBorderBottomLeft = localTexturePool->FindTexture(TXT("Interface.ScrollbarThumbBorderBottomLeft"));
	ScrollbarTrackBorderTopLeft = localTexturePool->FindTexture(TXT("Interface.ScrollbarThumbBorderTopLeft"));

	//Dropdown component textures
	DropdownCollapsedButton = localTexturePool->FindTexture(TXT("Interface.Buttons.CollapsedButton"));
	DropdownExpandedButton = localTexturePool->FindTexture(TXT("Interface.Buttons.ExpandedButton"));

	//Tree list component textures
	TreeListAddButton = localTexturePool->FindTexture(TXT("Interface.Buttons.TreeListAddButton"));
	TreeListSubtractButton = localTexturePool->FindTexture(TXT("Interface.Buttons.TreeListSubtractButton"));

	//Text field component resources
	HighlightingShader.first = TXT("TextHighlighter.frag");
	HighlightingShader.second = sf::Shader::Type::Fragment;

	GuiFont = FontPool::FindFontPool()->FindFont(TXT("DefaultFont"));
}

void GuiTheme::InitializeStyles ()
{
	SStyleInfo defaultStyle;
	defaultStyle.Name = DEFAULT_STYLE_NAME;
	Styles.push_back(defaultStyle);

	SetupStyle(Styles.at(Styles.size() - 1));
}

const GuiComponent* GuiTheme::FindTemplate (const DString& styleName, const DClass* templateClass) const
{
	const SStyleInfo* style = FindStyle(styleName);
	if (style != nullptr)
	{
		return FindTemplate(style, templateClass);
	}

	return nullptr;
}

const GuiComponent* GuiTheme::FindTemplate (const SStyleInfo* style, const DClass* templateClass)
{
	for (GuiComponent* uiTemplate : style->Templates)
	{
		if (uiTemplate->StaticClass() == templateClass)
		{
			return uiTemplate;
		}
	}

	return nullptr;
}

GuiComponent* GuiTheme::FindTemplate (SStyleInfo* style, const DClass* templateClass)
{
	for (GuiComponent* uiTemplate : style->Templates)
	{
		if (uiTemplate->StaticClass() == templateClass)
		{
			return uiTemplate;
		}
	}

	return nullptr;
}

const GuiTheme::SStyleInfo* GuiTheme::FindStyle (const DString& styleName) const
{
	for (UINT_TYPE i = 0; i < Styles.size(); ++i)
	{
		if (Styles.at(i).Name == styleName)
		{
			return &Styles.at(i);
		}
	}

	GuiLog.Log(LogCategory::LL_Warning, TXT("The Gui Theme does not have a style named %s."), styleName);
	return nullptr;
}

void GuiTheme::SetDefaultStyle (const DString& styleName)
{
	for (UINT_TYPE i = 0; i < Styles.size(); ++i)
	{
		if (Styles.at(i).Name == styleName)
		{
			DefaultStyleIdx = INT(i);
			return;
		}
	}

	DefaultStyleIdx = INT_INDEX_NONE;
}

GuiTheme* GuiTheme::GetGuiTheme ()
{
	GuiEngineComponent* localGuiEngine = GuiEngineComponent::Find();
	if (localGuiEngine != nullptr)
	{
		return localGuiEngine->GetGuiTheme();
	}

	return nullptr;
}

const GuiTheme::SStyleInfo* GuiTheme::GetDefaultStyle () const
{
	if (ContainerUtils::IsValidIndex(Styles, DefaultStyleIdx))
	{
		return &Styles.at(DefaultStyleIdx.ToUnsignedInt());
	}

	return nullptr;
}

void GuiTheme::SetupStyle (SStyleInfo& outNewStyle)
{
	//ButtonComponent
	{
		ButtonComponent* buttonTemplate = ButtonComponent::CreateObject();
		buttonTemplate->SetSize(Vector2(32.f, 32.f));
		buttonTemplate->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());

		FrameComponent* buttonBackground = buttonTemplate->GetBackground();
		if (buttonBackground != nullptr)
		{
			buttonBackground->SetBorderThickness(0.f);
			buttonBackground->SetLockedFrame(true);
			buttonBackground->RenderComponent->SetSpriteTexture(ButtonSingleSpriteBackground.Get());
			buttonBackground->RenderComponent->TopBorder = nullptr;
			buttonBackground->RenderComponent->TopRightCorner = nullptr;
			buttonBackground->RenderComponent->RightBorder = nullptr;
			buttonBackground->RenderComponent->BottomRightCorner = nullptr;
			buttonBackground->RenderComponent->BottomBorder = nullptr;
			buttonBackground->RenderComponent->BottomLeftCorner = nullptr;
			buttonBackground->RenderComponent->LeftBorder = nullptr;
			buttonBackground->RenderComponent->TopLeftCorner = nullptr;
		}

		outNewStyle.Templates.push_back(buttonTemplate);
	}
	
	//CheckboxComponent
	{
		CheckboxComponent* checkboxTemplate = CheckboxComponent::CreateObject();
		checkboxTemplate->SetCheckboxRightSide(true);
		checkboxTemplate->SetSize(Vector2(64.f, 24.f));

		LabelComponent* checkboxCaption = checkboxTemplate->CaptionComponent.Get();
		if (checkboxCaption != nullptr)
		{
			checkboxCaption->SetAutoRefresh(false);
			checkboxCaption->SetFont(GuiFont.Get());
			checkboxCaption->SetCharacterSize(16);
			checkboxCaption->SetWrapText(true);
			checkboxCaption->SetClampText(true);
			checkboxCaption->SetVerticalAlignment(LabelComponent::VA_Center);
			checkboxCaption->SetHorizontalAlignment(LabelComponent::HA_Left);
			checkboxCaption->SetAutoRefresh(true);
		}

		FrameComponent* checkboxSprite = checkboxTemplate->CheckboxSprite.Get();
		if (checkboxSprite != nullptr)
		{
			checkboxSprite->SetBorderThickness(0.f);
			checkboxSprite->SetLockedFrame(true);
			checkboxSprite->RenderComponent->TopBorder = nullptr;
			checkboxSprite->RenderComponent->RightBorder = nullptr;
			checkboxSprite->RenderComponent->BottomBorder = nullptr;
			checkboxSprite->RenderComponent->LeftBorder = nullptr;
			checkboxSprite->RenderComponent->TopRightCorner = nullptr;
			checkboxSprite->RenderComponent->BottomRightCorner = nullptr;
			checkboxSprite->RenderComponent->BottomLeftCorner = nullptr;
			checkboxSprite->RenderComponent->TopLeftCorner = nullptr;
			checkboxSprite->RenderComponent->SetSpriteTexture(CheckboxTexture.Get());
		}

		outNewStyle.Templates.push_back(checkboxTemplate);
	}

	//FrameComponent
	{
		FrameComponent* frameTemplate = FrameComponent::CreateObject();
		frameTemplate->RenderComponent->TopBorder = FrameBorderTop;
		frameTemplate->RenderComponent->TopRightCorner = FrameBorderTopRight;
		frameTemplate->RenderComponent->RightBorder = FrameBorderRight;
		frameTemplate->RenderComponent->BottomRightCorner = FrameBorderBottomRight;
		frameTemplate->RenderComponent->BottomBorder = FrameBorderBottom;
		frameTemplate->RenderComponent->BottomLeftCorner = FrameBorderBottomLeft;
		frameTemplate->RenderComponent->LeftBorder = FrameBorderLeft;
		frameTemplate->RenderComponent->TopLeftCorner = FrameBorderTopLeft;
		frameTemplate->RenderComponent->SetSpriteTexture(FrameFill.Get());
		frameTemplate->SetBorderThickness(8.f);
		frameTemplate->SetLockedFrame(true);
		outNewStyle.Templates.push_back(frameTemplate);
	}

	//ScrollbarTrackComponent
	{
		ScrollbarTrackComponent* trackTemplate = ScrollbarTrackComponent::CreateObject();
		if (trackTemplate->TrackColor == nullptr)
		{
			trackTemplate->TrackColor = SolidColorRenderComponent::CreateObject();
			trackTemplate->AddComponent(trackTemplate->TrackColor.Get());
		}
		trackTemplate->TravelDelay = 0.75f;
		trackTemplate->TravelInterval = 0.2f;
		trackTemplate->SetScaleThumb(true);
		trackTemplate->TrackColor->SolidColor = Color(96, 96, 96, 255);

		if (trackTemplate->TravelTrackColor == nullptr)
		{
			trackTemplate->TravelTrackColor = SolidColorRenderComponent::CreateObject();
			trackTemplate->AddComponent(trackTemplate->TravelTrackColor.Get());
		}
		trackTemplate->TravelTrackColor->SolidColor = Color(48, 48, 48, 255);

		if (trackTemplate->Thumb == nullptr)
		{
			trackTemplate->Thumb = ButtonComponent::CreateObject();
			trackTemplate->AddComponent(trackTemplate->Thumb.Get());
		}
		
		if (trackTemplate->Thumb->GetBackground() == nullptr)
		{
			FrameComponent* thumbFrame = FrameComponent::CreateObject();
			if (trackTemplate->Thumb->AddComponent(thumbFrame))
			{
				trackTemplate->Thumb->SetBackground(thumbFrame);
			}
		}
		trackTemplate->Thumb->GetBackground()->SetBorderThickness(2);
		trackTemplate->Thumb->GetBackground()->SetLockedFrame(true);
		
		BorderedSpriteComponent* thumbSprite = trackTemplate->Thumb->GetRenderComponent();
		CHECK(thumbSprite != nullptr);
		thumbSprite->SetSpriteTexture(ScrollbarTrackFill.Get());
		thumbSprite->TopBorder = ScrollbarTrackBorderTop;
		thumbSprite->RightBorder = ScrollbarTrackBorderRight;
		thumbSprite->BottomBorder = ScrollbarTrackBorderBottom;
		thumbSprite->LeftBorder = ScrollbarTrackBorderLeft;
		thumbSprite->TopRightCorner = ScrollbarTrackBorderTopRight;
		thumbSprite->BottomRightCorner = ScrollbarTrackBorderBottomRight;
		thumbSprite->BottomLeftCorner = ScrollbarTrackBorderBottomLeft;
		thumbSprite->TopLeftCorner = ScrollbarTrackBorderTopLeft;
		thumbSprite->FillColor = Color(64, 64, 64, 255);

		outNewStyle.Templates.push_back(trackTemplate);
	}

	//ScrollbarComponent_Deprecated
	{
		FLOAT transformLength = 12.f;
		ScrollbarComponent_Deprecated* scrollbarTemplate = ScrollbarComponent_Deprecated::CreateObject();
		scrollbarTemplate->SetSize(Vector2(transformLength, transformLength * 4.f));

		if (scrollbarTemplate->MiddleMouseSprite == nullptr)
		{
			scrollbarTemplate->MiddleMouseSprite = SpriteComponent::CreateObject();
			scrollbarTemplate->AddComponent(scrollbarTemplate->MiddleMouseSprite.Get());
		}
		scrollbarTemplate->ScrollJumpInterval = 3;
		scrollbarTemplate->bEnableMiddleMouseScrolling = true;
		scrollbarTemplate->MiddleMouseScrollIntervalRange = Range<FLOAT>(0.05f, 1.f);
		scrollbarTemplate->MiddleMouseHoldThreshold = 0.75f;
		scrollbarTemplate->ContinuousScrollDelay = 0.75f;
		scrollbarTemplate->ContinuousInterval = 0.2f;
		scrollbarTemplate->bClampsMouseWhenScrolling = true;
		scrollbarTemplate->SetHideWhenInsufficientScrollPos(false);
		scrollbarTemplate->MiddleMouseSprite->SetSpriteTexture(ScrollbarMiddleMouseScrollAnchor.Get());

		if (scrollbarTemplate->TopScrollButton == nullptr)
		{
			scrollbarTemplate->TopScrollButton = ButtonComponent::CreateObject();
			scrollbarTemplate->AddComponent(scrollbarTemplate->TopScrollButton.Get());
		}
		scrollbarTemplate->TopScrollButton->SetSize(Vector2(transformLength, transformLength));
		//scrollbarTemplate->TopScrollButton->GetRenderComponent()->SetSpriteTexture(ScrollbarTopButton.Get());
		scrollbarTemplate->TopScrollButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());

		if (scrollbarTemplate->BottomScrollButton == nullptr)
		{
			scrollbarTemplate->BottomScrollButton = ButtonComponent::CreateObject();
			scrollbarTemplate->AddComponent(scrollbarTemplate->BottomScrollButton.Get());
		}
		scrollbarTemplate->BottomScrollButton->SetSize(Vector2(transformLength, transformLength));
		//scrollbarTemplate->BottomScrollButton->GetRenderComponent()->SetSpriteTexture(ScrollbarBottomButton.Get());
		scrollbarTemplate->BottomScrollButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());

		//Use the theme's scrollbar track to initialize this track
		const ScrollbarTrackComponent* trackTemplate = dynamic_cast<const ScrollbarTrackComponent*>(FindTemplate(&outNewStyle, ScrollbarTrackComponent::SStaticClass()));
		CHECK(trackTemplate != nullptr)
		if (scrollbarTemplate->Track == nullptr)
		{
			scrollbarTemplate->Track = ScrollbarTrackComponent::CreateObject();
			scrollbarTemplate->AddComponent(scrollbarTemplate->Track.Get());
		}
		scrollbarTemplate->Track->SetSize(Vector2(transformLength, scrollbarTemplate->ReadSize().Y - (transformLength * 2)));

		outNewStyle.Templates.push_back(scrollbarTemplate);
	}

	//LabelComponent
	{
		LabelComponent* labelTemplate = LabelComponent::CreateObject();
		labelTemplate->SetSize(Vector2(64.f, 16.f));
		labelTemplate->SetAutoRefresh(false);
		labelTemplate->SetFont(GuiFont.Get());
		labelTemplate->SetCharacterSize(16);
		labelTemplate->SetWrapText(true);
		labelTemplate->SetClampText(true);
		labelTemplate->SetLineSpacing(0.f);
		labelTemplate->SetVerticalAlignment(LabelComponent::VA_Top);
		labelTemplate->SetHorizontalAlignment(LabelComponent::HA_Left);
		labelTemplate->SetAutoRefresh(true);

		TextRenderComponent* labelRender = labelTemplate->GetRenderComponent();
		if (labelRender != nullptr)
		{
			labelRender->SetTextShader(nullptr);
		}

		outNewStyle.Templates.push_back(labelTemplate);
	}

	//TextFieldComponent
	{
		TextFieldComponent* textFieldTemplate = TextFieldComponent::CreateObject();

		textFieldTemplate->MaxNumCharacters = -1;
		textFieldTemplate->DragScrollRate = 0.2f;
		textFieldTemplate->SetEditable(true);
		textFieldTemplate->SetSelectable(true);

		textFieldTemplate->SetAutoRefresh(false);
		textFieldTemplate->SetFont(GuiFont.Get());
		textFieldTemplate->SetCharacterSize(16);
		textFieldTemplate->SetWrapText(true);
		textFieldTemplate->SetClampText(true);
		textFieldTemplate->SetLineSpacing(0.f);
		textFieldTemplate->SetVerticalAlignment(LabelComponent::VA_Top);
		textFieldTemplate->SetHorizontalAlignment(LabelComponent::HA_Left);
		textFieldTemplate->SetAutoRefresh(true);

		TextRenderComponent* textRender = textFieldTemplate->GetRenderComponent();
		if (textRender != nullptr)
		{
			textRender->SetTextShader(Shader::CreateShader(HighlightingShader.first, HighlightingShader.second));
		}

		outNewStyle.Templates.push_back(textFieldTemplate);
	}

	//ListBoxComponent
	{
		ListBoxComponent* listTemplate = ListBoxComponent::CreateObject();
		listTemplate->SetMaxNumSelected(0);
		listTemplate->SetAutoDeselect(false);
		listTemplate->SetReadOnly(false);
		listTemplate->SetSelectionColor(Color(0x666666));
		listTemplate->SetHoverColor(Color(0x1db9b6));

		FrameComponent* listBackground = listTemplate->GetBackground();
		if (listBackground == nullptr)
		{
			listBackground = FrameComponent::CreateObject();
			listTemplate->SetBackground(listBackground);
		}

		listBackground->SetLockedFrame(true);
		listBackground->SetBorderThickness(2.f);
		BorderedSpriteComponent* backgroundSprite = listBackground->RenderComponent.Get();
		if (backgroundSprite == nullptr)
		{
			backgroundSprite = BorderedSpriteComponent::CreateObject();
			if (listBackground->AddComponent(backgroundSprite))
			{
				listBackground->RenderComponent = backgroundSprite;
			}
		}

		backgroundSprite->TopBorder = FrameBorderTop;
		backgroundSprite->RightBorder = FrameBorderRight;
		backgroundSprite->BottomBorder = FrameBorderBottom;
		backgroundSprite->LeftBorder = FrameBorderLeft;
		backgroundSprite->TopRightCorner = FrameBorderTopRight;
		backgroundSprite->BottomRightCorner = FrameBorderBottomRight;
		backgroundSprite->BottomLeftCorner = FrameBorderBottomLeft;
		backgroundSprite->TopLeftCorner = FrameBorderTopLeft;
		backgroundSprite->SetSpriteTexture(FrameFill.Get());

		ScrollbarComponent_Deprecated* listScrollbar = listTemplate->GetScrollbar();
		if (listScrollbar == nullptr)
		{
			listScrollbar = ScrollbarComponent_Deprecated::CreateObject();
			listTemplate->SetScrollbar(listScrollbar);
		}

		const ScrollbarComponent_Deprecated* scrollbarTemplate = dynamic_cast<const ScrollbarComponent_Deprecated*>(FindTemplate(&outNewStyle, ScrollbarComponent_Deprecated::SStaticClass()));
		if (scrollbarTemplate != nullptr)
		{
			listScrollbar->SetHideWhenInsufficientScrollPos(true);
		}

		LabelComponent* listText = listTemplate->GetItemListText();
		if (listText == nullptr)
		{
			listText = LabelComponent::CreateObject();
			listTemplate->SetItemListText(listText);
		}

		outNewStyle.Templates.push_back(listTemplate);
	}

	//DropdownComponent
	{
		DropdownComponent* dropdownTemplate = DropdownComponent::CreateObject();
		dropdownTemplate->SetSize(Vector2(96.f, 64.f));

		FLOAT expandButtonLength = 24.f;
		
		//Adjust the expand button size to be equal to the scrollbar's width
		if (dropdownTemplate->GetExpandMenu() != nullptr && dropdownTemplate->GetExpandMenu()->GetScrollbar() != nullptr)
		{
			ScrollbarComponent_Deprecated* listScrollbar = dropdownTemplate->GetExpandMenu()->GetScrollbar();
			listScrollbar->SetSize(Vector2(expandButtonLength, listScrollbar->ReadSize().Y));

			if (dropdownTemplate->GetExpandButton() != nullptr)
			{
				dropdownTemplate->GetExpandButton()->SetSize(Vector2(expandButtonLength, expandButtonLength));
			}

			FrameComponent* selectedBackground = dropdownTemplate->GetSelectedObjBackground();
			if (selectedBackground != nullptr)
			{
				selectedBackground->SetSize(Vector2(selectedBackground->ReadSize().X, expandButtonLength));
				selectedBackground->RenderComponent->RightBorder = nullptr;
				selectedBackground->SetBorderThickness(4.f);

				LabelComponent* selectedText = dropdownTemplate->GetSelectedItemLabel();
				if (selectedText != nullptr)
				{
					selectedText->SetCharacterSize(16);
					selectedText->SetHorizontalAlignment(LabelComponent::HA_Center);
				}
			}
		}

		outNewStyle.Templates.push_back(dropdownTemplate);
	}

	//TreeListComponent
	{
		TreeListComponent* treeTemplate = TreeListComponent::CreateObject();
		treeTemplate->BrowseJumpAmount = 10;
		treeTemplate->IndentSize = 14.f;
		treeTemplate->BranchHeight = 16.f;
		treeTemplate->ConnectionThickness = 4.f;
		treeTemplate->ConnectionColor = sf::Color::Black;

		treeTemplate->SetExpandButtonTexture(TreeListAddButton.Get());
		treeTemplate->SetCollapseButtonTexture(TreeListSubtractButton.Get());

		SolidColorRenderComponent* selectedColor = treeTemplate->GetSelectedBar();
		if (selectedColor == nullptr)
		{
			selectedColor = SolidColorRenderComponent::CreateObject();
			treeTemplate->SetSelectedBar(selectedColor);
		}

		selectedColor->SolidColor = sf::Color::Cyan;

		SolidColorRenderComponent* hoverColor = treeTemplate->GetHoverBar();
		if (hoverColor == nullptr)
		{
			hoverColor = SolidColorRenderComponent::CreateObject();
			treeTemplate->SetHoverBar(hoverColor);
		}

		hoverColor->SolidColor = Color(200, 200, 200, 96);

		ScrollbarComponent_Deprecated* treeScrollbar = treeTemplate->GetListScrollbar();
		if (treeScrollbar == nullptr)
		{
			treeScrollbar = ScrollbarComponent_Deprecated::CreateObject();
			treeTemplate->SetListScrollbar(treeScrollbar);
		}

		treeScrollbar->SetHideWhenInsufficientScrollPos(true);

		outNewStyle.Templates.push_back(treeTemplate);
	}
}
SD_END