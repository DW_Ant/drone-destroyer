/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiTester.cpp
=====================================================================
*/

#include "GuiClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, GuiTester, SD, GuiEntity)

void GuiTester::InitProps ()
{
	Super::InitProps();

	StageDimensions = Rectangle<FLOAT>(192.f, 32.f, 792.f, 544.f, Rectangle<FLOAT>::CS_XRightYDown);

	StageFrame = nullptr;
	PanelButtonCounter = 0;

	MaxButtonsPerPage = 9;
	ButtonBarPageIdx = 0;
	NextButtonBarPage = nullptr;
	PrevButtonBarPage = nullptr;

	TestWindowHandle = nullptr;
	TestDrawLayer = nullptr;
	TestWindowInput = nullptr;
	TestWindowMouse = nullptr;

	ListBoxStrings.push_back(TXT("Apple"));
	ListBoxStrings.push_back(TXT("Banana"));
	ListBoxStrings.push_back(TXT("Grape"));
	ListBoxStrings.push_back(TXT("Orange"));
	ListBoxStrings.push_back(TXT("Pear"));
	ListBoxStrings.push_back(TXT("Strawberry"));

	for (INT i = 0; i < 20; i++)
	{
		DropdownNumbers.push_back(i);
	}
}

void GuiTester::ConstructUI ()
{
	InitializeStageFrame();
	PanelButtonCounter = 0;
	CreateBaseButtons();
}

void GuiTester::Destroyed ()
{
	ClearStageComponents(); //Destroy any external entities such as GuiEntities.

	//The Gui Window destroyed the draw layer

	if (TestWindowInput != nullptr)
	{
		TestWindowInput->Destroy();
	}

	if (TestWindowMouse != nullptr)
	{
		TestWindowMouse->Destroy();
	}

	Super::Destroyed();
}

void GuiTester::BeginUnitTest ()
{
	GraphicsEngineComponent* localGraphicsEngine = GraphicsEngineComponent::Find();
	CHECK(localGraphicsEngine != nullptr)

	//Create external window that'll display and broadcast input for this test
	TestWindowHandle = Window::CreateObject();
	TestWindowHandle->SetResource(new sf::RenderWindow(sf::VideoMode(800, 600), "Gui Unit Test"));
	TestWindowHandle->GetResource()->setMouseCursorVisible(false);
	TestWindowHandle->GetTick()->SetTickInterval(0.0166f); //~60 frames per second
	TestWindowHandle->GetPollingTickComponent()->SetTickInterval(0.0166f);
	TestWindowHandle->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, GuiTester, HandleWindowEvent, void, const sf::Event&));

	//Create DrawLayer for the external window
	TestDrawLayer = GuiDrawLayer::CreateObject();
	TestDrawLayer->SetDrawPriority(0);
	TestDrawLayer->RegisterMenu(this);
	TestWindowHandle->RegisterDrawLayer(TestDrawLayer.Get());

	TestWindowInput = InputBroadcaster::CreateObject();
	TestWindowMouse = MousePointer::CreateObject();
	InputUtils::SetupInputForWindow(TestWindowHandle.Get(), TestWindowInput.Get(), TestWindowMouse.Get());
	TestDrawLayer->RegisterSingleComponent(TestWindowMouse->GetMouseIcon()); //Register this mouse to the test window
	SetupInputComponent(TestWindowInput.Get(), 100);

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleTick, void, FLOAT));
	}
}

void GuiTester::InitializeStageFrame ()
{
	if (StageFrame != nullptr)
	{
		return;
	}

	StageFrame = FrameComponent::CreateObject();
	if (!AddComponent(StageFrame.Get()))
	{
		StageFrame = nullptr;
		return;
	}

	StageFrame->SetLockedFrame(true);
	StageFrame->SetBorderThickness(1.f);
	StageFrame->RenderComponent->ClearSpriteTexture();
	StageFrame->RenderComponent->FillColor = Color(48, 48, 48);
	StageFrame->SetPosition(Vector2(StageDimensions.Left, StageDimensions.Top));
	StageFrame->SetSize(Vector2(StageDimensions.GetWidth(), StageDimensions.GetHeight()));
}

void GuiTester::CreateBaseButtons ()
{
	ButtonComponent* frameButton = AddPanelButton();
	if (frameButton != nullptr)
	{
		frameButton->SetCaptionText(TXT("Frame Component"));
		frameButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleFrameButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* labelButton = AddPanelButton();
	if (labelButton != nullptr)
	{
		labelButton->SetCaptionText(TXT("Label Component"));
		labelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleLabelButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* autoSizeLabelButton = AddPanelButton();
	if (autoSizeLabelButton != nullptr)
	{
		autoSizeLabelButton->SetCaptionText(TXT("Auto-sized Label Component"));
		autoSizeLabelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleAutoSizeLabelButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* checkboxButton = AddPanelButton();
	if (checkboxButton != nullptr)
	{
		checkboxButton->SetCaptionText(TXT("Checkbox Component"));
		checkboxButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleCheckboxButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* buttonButton = AddPanelButton();
	if (buttonButton != nullptr)
	{
		buttonButton->SetCaptionText(TXT("Button Component"));
		buttonButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* scrollbarButton = AddPanelButton();
	if (scrollbarButton != nullptr)
	{
		scrollbarButton->SetCaptionText(TXT("Scrollbar Component"));
		scrollbarButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* textFieldButton = AddPanelButton();
	if (textFieldButton != nullptr)
	{
		textFieldButton->SetCaptionText(TXT("Text Field Component"));
		textFieldButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleTextFieldButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* listBoxButton = AddPanelButton();
	if (listBoxButton != nullptr)
	{
		listBoxButton->SetCaptionText(TXT("List Box"));
		listBoxButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleListBoxButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* dropdownButton = AddPanelButton();
	if (dropdownButton != nullptr)
	{
		dropdownButton->SetCaptionText(TXT("Dropdown Component"));
		dropdownButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleDropdownButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* guiEntityButton = AddPanelButton();
	if (guiEntityButton != nullptr)
	{
		guiEntityButton->SetCaptionText(TXT("Gui Entities"));
		guiEntityButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleGuiEntityButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* hoverButton = AddPanelButton();
	if (hoverButton != nullptr)
	{
		hoverButton->SetCaptionText(TXT("Hover Component"));
		hoverButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleHoverButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* treeButton = AddPanelButton();
	if (treeButton != nullptr)
	{
		treeButton->SetCaptionText(TXT("Tree List Component"));
		treeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleTreeListButtonClicked, void, ButtonComponent*));
	}

	ButtonComponent* closeButton = AddPanelButton();
	if (closeButton != nullptr)
	{
		closeButton->SetCaptionText(TXT("End UI Test"));
		closeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleCloseClicked, void, ButtonComponent*));
	}

	if (ButtonBar.size() <= MaxButtonsPerPage)
	{
		//No need to create prev/next button since there aren't enough buttons for a second page.
		return;
	}

	//Create next and prev button page
	Vector2 buttonSize = Vector2(32.f, 24.f);
	Vector2 buttonPosition = Vector2(16.f, ReadCachedAbsSize().Y - buttonSize.Y * 2);
	PrevButtonBarPage = ButtonComponent::CreateObject();
	if (AddComponent(PrevButtonBarPage.Get()))
	{
		PrevButtonBarPage->SetPosition(buttonPosition);
		PrevButtonBarPage->SetSize(buttonSize);
		PrevButtonBarPage->GetBackground()->SetBorderThickness(1.f);
		PrevButtonBarPage->SetCaptionText(TXT("<"));
		PrevButtonBarPage->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandlePrevPageClicked, void, ButtonComponent*));

		//Prepare button position for next page
		buttonPosition.X += buttonSize.X + 8.f;
	}
	else
	{
		PrevButtonBarPage = nullptr;
	}


	NextButtonBarPage = ButtonComponent::CreateObject();
	if (AddComponent(NextButtonBarPage.Get()))
	{
		NextButtonBarPage->SetPosition(buttonPosition);
		NextButtonBarPage->SetSize(buttonSize);
		NextButtonBarPage->GetBackground()->SetBorderThickness(1.f);
		NextButtonBarPage->SetCaptionText(TXT(">"));
		NextButtonBarPage->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleNextPageClicked, void, ButtonComponent*));
	}
	else
	{
		NextButtonBarPage = nullptr;
	}
}

void GuiTester::ClearStageComponents ()
{
	for (UINT_TYPE i = 0; i < StageComponents.size(); i++)
	{
		if (StageComponents.at(i) != nullptr)
		{
			StageComponents.at(i)->Destroy();
		}
	}

	for (UINT_TYPE i = 0; i < GuiEntities.size(); i++)
	{
		if (GuiEntities.at(i) != nullptr)
		{
			GuiEntities.at(i)->Destroy();
		}
	}

	ToggleButtons[0] = nullptr;
	ToggleButtons[1] = nullptr;
	ButtonTestCounter = 0;
	EnableToggleCheckbox = nullptr;
	VisibilityToggleCheckbox = nullptr;
	TargetTestCheckbox = nullptr;
	ScrollbarTester = nullptr;
	ScrollbarTesterViewedObject = nullptr;
	ScrollbarTesterLabelTest = nullptr;
	ScrollbarTesterButtonTest = nullptr;
	ScrollbarTesterSizeChangeTest = nullptr;
	ScrollbarTesterDifferentFrameSizeTest = nullptr;
	ScrollbarTesterOuterSizeChangeTest = nullptr;
	ScrollbarTesterNestedScrollbars = nullptr;
	ScrollbarTesterComplexUi = nullptr;
	ScrollbarTesterSizeChangeFrame = nullptr;
	ScrollbarTesterDecreaseSizeButton = nullptr;
	ScrollbarTesterIncreaseSizeButton = nullptr;
	MaxNumSelectableListBox = nullptr;
	ButtonBarDropdown = nullptr;
	SetFocusDropdown = nullptr;
	HoverLabel = nullptr;
	HoverButton = nullptr;
	TreeListClassBrowser = nullptr;

	ContainerUtils::Empty(StageComponents);
	ContainerUtils::Empty(GuiEntities);

	//All Scrollbar test GUI Components are already destroyed when their associated owners were destroyed.
	ContainerUtils::Empty(ScrollbarTestObjects);
}

void GuiTester::ResetScrollbarTest ()
{
	for (UINT_TYPE i = 0; i < ScrollbarTestObjects.size(); ++i)
	{
		ScrollbarTestObjects.at(i)->Destroy();
	}
	ContainerUtils::Empty(ScrollbarTestObjects);

	if (ScrollbarTester != nullptr)
	{
		ScrollbarTester->SetHideControlsWhenFull(false);
		ScrollbarTester->SetAnchorTop(ScrollbarTesterAnchorTop);
		ScrollbarTester->SetAnchorRight(ScrollbarTesterAnchorRight);
		ScrollbarTester->SetAnchorBottom(ScrollbarTesterAnchorBottom);
		ScrollbarTester->SetAnchorLeft(ScrollbarTesterAnchorLeft);

		if (ScrollbarTester->GetFrameCamera() != nullptr)
		{
			//Snap back to top left corner
			ScrollbarTester->GetFrameCamera()->SetPosition(Vector2::ZeroVector);
		}

		//Recompute absolute transform since the viewed GuiEntity may depend on this sprite component's size,
		//and it's possible that scrollbar tester may have changed size from its previous test.
		ScrollbarTester->ComputeAbsTransform();
		ScrollbarTester->GetFrameSpriteTransform()->ComputeAbsTransform();
	}
}

ButtonComponent* GuiTester::AddPanelButton ()
{
	ButtonComponent* result = ButtonComponent::CreateObject();
	if (!AddComponent(result))
	{
		return nullptr;
	}

	Vector2 buttonPosition = Vector2(16.f, 32.f);
	Vector2 buttonSize = Vector2(128.f, 48.f);
	buttonPosition.Y += (56.f * PanelButtonCounter.ToFLOAT());
	result->SetPosition(buttonPosition);
	result->SetSize(buttonSize);
	ButtonBar.push_back(result);
	if (++PanelButtonCounter >= MaxButtonsPerPage)
	{
		//Reset the counter to 0 so that the next button appears on top
		PanelButtonCounter = 0;
	}

	if ((INT(ButtonBar.size() - 1) / MaxButtonsPerPage) != ButtonBarPageIdx)
	{
		//Hide this button since the current button bar page is not viewing this button.
		result->SetVisibility(false);
	}

	return result;
}

void GuiTester::UpdateButtonBarPageIdx ()
{
	for (UINT_TYPE i = 0; i < ButtonBar.size(); i++)
	{
		bool bViewingButton = ((INT(i) / MaxButtonsPerPage) == ButtonBarPageIdx);
		ButtonBar.at(i)->SetVisibility(bViewingButton);
	}
}

void GuiTester::HandleTick (FLOAT deltaSec)
{
	//Noop
}

void GuiTester::HandleWindowEvent (const sf::Event& newWindowEvent)
{
	if (newWindowEvent.type == sf::Event::Closed)
	{
		Destroy();
	}
}

void GuiTester::HandleFrameButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Frame Components!"));
	ClearStageComponents();

	//Simple locked frame with fill texture and borders
	FrameComponent* frame1 = FrameComponent::CreateObject();
	if (!StageFrame->AddComponent(frame1))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach frame component for testing."));
	}
	else
	{
		TexturePool* localTexturePool = TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)

		frame1->SetLockedFrame(true);
		frame1->SetPosition(Vector2(8.f, 8.f));
		frame1->SetSize(Vector2(48.f, 48.f));
		frame1->RenderComponent->SetSpriteTexture(localTexturePool->FindTexture(TXT("DebuggingTexture")));
		frame1->RenderComponent->TopBorder = localTexturePool->FindTexture(TXT("Interface.FrameBorderTop"));
		frame1->RenderComponent->TopRightCorner = localTexturePool->FindTexture(TXT("Interface.FrameBorderTopRight"));
		frame1->RenderComponent->RightBorder = localTexturePool->FindTexture(TXT("Interface.FrameBorderRight"));
		frame1->RenderComponent->BottomRightCorner = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottomRight"));
		frame1->RenderComponent->BottomBorder = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottom"));
		frame1->RenderComponent->BottomLeftCorner = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottomLeft"));
		frame1->RenderComponent->LeftBorder = localTexturePool->FindTexture(TXT("Interface.FrameBorderLeft"));
		frame1->RenderComponent->TopLeftCorner = localTexturePool->FindTexture(TXT("Interface.FrameBorderTopLeft"));
		StageComponents.push_back(frame1);
	}

	//Simple moveable frame with fill texture and thick borders
	FrameComponent* frame2 = FrameComponent::CreateObject();
	if (!StageFrame->AddComponent(frame2))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach frame component for testing."));
	}
	else
	{
		TexturePool* localTexturePool = TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)

		frame2->SetLockedFrame(false);
		frame2->SetBorderThickness(16.f);
		frame2->SetPosition(Vector2(80.f, 8.f));
		frame2->SetSize(Vector2(48.f, 48.f));
		frame2->RenderComponent->SetSpriteTexture(localTexturePool->FindTexture(TXT("DebuggingTexture")));
		frame2->RenderComponent->TopBorder = localTexturePool->FindTexture(TXT("Interface.FrameBorderTop"));
		frame2->RenderComponent->TopRightCorner = localTexturePool->FindTexture(TXT("Interface.FrameBorderTopRight"));
		frame2->RenderComponent->RightBorder = localTexturePool->FindTexture(TXT("Interface.FrameBorderRight"));
		frame2->RenderComponent->BottomRightCorner = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottomRight"));
		frame2->RenderComponent->BottomBorder = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottom"));
		frame2->RenderComponent->BottomLeftCorner = localTexturePool->FindTexture(TXT("Interface.FrameBorderBottomLeft"));
		frame2->RenderComponent->LeftBorder = localTexturePool->FindTexture(TXT("Interface.FrameBorderLeft"));
		frame2->RenderComponent->TopLeftCorner = localTexturePool->FindTexture(TXT("Interface.FrameBorderTopLeft"));
		StageComponents.push_back(frame2);
	}

	//locked frame with a moveable frame inside it
	FrameComponent* frame3 = FrameComponent::CreateObject();
	if (!StageFrame->AddComponent(frame3))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach frame component for testing."));
	}
	else
	{
		frame3->SetLockedFrame(true);
		frame3->SetPosition(Vector2(8.f, 80.f));
		frame3->SetSize(Vector2(96.f, 96.f));
		StageComponents.push_back(frame3);

		FrameComponent* frame4 = FrameComponent::CreateObject();
		if (!frame3->AddComponent(frame4))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach %s to %s (frame4 to frame3)."), frame4->ToString(), frame3->ToString());
		}
		else
		{
			frame4->SetLockedFrame(false);
			frame4->SetPosition(Vector2(0.25f, 0.25f));
			frame4->SetSize(Vector2(0.5f, 0.5f));
			StageComponents.push_back(frame4);
		}
	}

	//Test Min/Max Drag Size
	{
		FrameComponent* dragFrameBorder = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(dragFrameBorder))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach drag frame border to stage frame."));
		}
		else
		{
			dragFrameBorder->SetPosition(Vector2(0.55f, 0.02f));
			dragFrameBorder->SetSize(Vector2(0.4f, 0.3f));
			dragFrameBorder->SetLockedFrame(true);
			dragFrameBorder->RenderComponent->ClearSpriteTexture();
			StageComponents.push_back(dragFrameBorder);

			FrameComponent* dragSizeLimitTester = FrameComponent::CreateObject();
			if (!dragFrameBorder->AddComponent(dragSizeLimitTester))
			{
				UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach drag size limit tester frame component to the frame border."));
			}
			else
			{
				dragSizeLimitTester->SetPosition(Vector2(0.15f, 0.15f));
				dragSizeLimitTester->SetSize(Vector2(0.7f, 0.7f));
				dragSizeLimitTester->SetLockedFrame(false);
				StageComponents.push_back(dragSizeLimitTester);

				LabelComponent* dragSizeLabel = LabelComponent::CreateObject();
				if (!dragSizeLimitTester->AddComponent(dragSizeLabel))
				{
					UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach label component to drag size limit tester."));
				}
				else
				{
					dragSizeLabel->SetPosition(Vector2(dragSizeLimitTester->GetBorderThickness(), dragSizeLimitTester->GetBorderThickness()));
					dragSizeLabel->SetSize(Vector2(1.f, 1.f));
					DString dragText = TXT("Drag Size Limit Tester");
					dragSizeLabel->SetText(dragText);
					dragSizeLabel->SetClampText(true);
					dragSizeLabel->SetWrapText(false);

					//Set drag size limits based on label length
					Vector2 minDragSize(dragSizeLabel->GetFont()->CalculateStringWidth(dragText, dragSizeLabel->GetCharacterSize()), dragSizeLabel->GetCharacterSize().ToFLOAT() + 24.f);
					dragSizeLimitTester->SetMinDragSize(minDragSize);
					dragSizeLimitTester->SetMaxDragSize(minDragSize + Vector2(32.f, 64.f));

					StageComponents.push_back(dragSizeLabel);
				}
			}
		}
	}

	//Moveable frame with relative (locked) frames within it.
	{
		FrameComponent* nestedFrameContainer = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(nestedFrameContainer))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach nestedframe container to the stage."));
		}
		else
		{
			nestedFrameContainer->SetLockedFrame(true);
			nestedFrameContainer->RenderComponent->ClearSpriteTexture();
			nestedFrameContainer->SetPosition(Vector2(0.05f, 0.45f));
			nestedFrameContainer->SetSize(Vector2(200.f, 200.f));
			StageComponents.push_back(nestedFrameContainer);

			FrameComponent* nestedFrame = FrameComponent::CreateObject();
			if (!nestedFrameContainer->AddComponent(nestedFrame))
			{
				UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach nestedframe to the container."));
			}
			else
			{
				nestedFrame->SetPosition(Vector2::ZeroVector);
				nestedFrame->SetSize(Vector2(1.f, 1.f));
				nestedFrame->SetLockedFrame(false);
				nestedFrame->SetMinDragSize(Vector2(80.f, 80.f));

				FrameComponent* topLeft = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(topLeft))
				{
					topLeft->SetPosition(Vector2::ZeroVector);
					topLeft->SetSize(Vector2(0.2f, 0.2f));
					topLeft->SetLockedFrame(true);
				}

				FrameComponent* topCenter = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(topCenter))
				{
					topCenter->SetPosition(Vector2(0.4f, 0.f));
					topCenter->SetSize(Vector2(0.2f, 0.2f));
					topCenter->SetLockedFrame(true);
				}

				FrameComponent* topRight = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(topRight))
				{
					topRight->SetPosition(Vector2(0.8f, 0.f));
					topRight->SetSize(Vector2(0.2f, 0.2f));
					topRight->SetLockedFrame(true);
				}

				FrameComponent* middleLeft = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(middleLeft))
				{
					middleLeft->SetPosition(Vector2(0.f, 0.4f));
					middleLeft->SetSize(Vector2(0.2f, 0.2f));
					middleLeft->SetLockedFrame(true);
				}

				FrameComponent* middleCenter = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(middleCenter))
				{
					middleCenter->SetPosition(Vector2(0.4f, 0.4f));
					middleCenter->SetSize(Vector2(0.2f, 0.2f));
					middleCenter->SetLockedFrame(true);
				}

				FrameComponent* middleRight = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(middleRight))
				{
					middleRight->SetPosition(Vector2(0.8f, 0.4f));
					middleRight->SetSize(Vector2(0.2f, 0.2f));
					middleRight->SetLockedFrame(true);
				}

				FrameComponent* bottomLeft = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(bottomLeft))
				{
					bottomLeft->SetPosition(Vector2(0.f, 0.8f));
					bottomLeft->SetSize(Vector2(0.2f, 0.2f));
					bottomLeft->SetLockedFrame(true);
				}

				FrameComponent* bottomCenter = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(bottomCenter))
				{
					bottomCenter->SetPosition(Vector2(0.4f, 0.8f));
					bottomCenter->SetSize(Vector2(0.2f, 0.2f));
					bottomCenter->SetLockedFrame(true);
				}

				FrameComponent* bottomRight = FrameComponent::CreateObject();
				if (nestedFrame->AddComponent(bottomRight))
				{
					bottomRight->SetPosition(Vector2(0.8f, 0.8f));
					bottomRight->SetSize(Vector2(0.2f, 0.2f));
					bottomRight->SetLockedFrame(true);
				}
			}
		}
	}

	//Test anchored frames
	LabelComponent* anchorLabel = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(anchorLabel))
	{
		anchorLabel->SetPosition(Vector2(0.55f, 0.5f));
		anchorLabel->SetSize(Vector2(200.f, 16.f));
		anchorLabel->SetText(TXT("Anchored Frame Components"));
		StageComponents.push_back(anchorLabel);
	}

	FrameComponent* anchorContainer = FrameComponent::CreateObject();
	if (StageFrame->AddComponent(anchorContainer))
	{
		anchorContainer->SetLockedFrame(true);
		anchorContainer->SetPosition(Vector2(0.55f, 0.55f));
		anchorContainer->SetSize(Vector2(200.f, 200.f));
		anchorContainer->SetBorderThickness(2.f);
		StageComponents.push_back(anchorContainer);

		FrameComponent* anchorTester = FrameComponent::CreateObject();
		if (anchorContainer->AddComponent(anchorTester))
		{
			anchorTester->SetPosition(Vector2::ZeroVector);
			anchorTester->SetSize(Vector2(1.f, 1.f));
			anchorTester->SetLockedFrame(false);
			anchorTester->SetMinDragSize(Vector2(150.f, 150.f));

			FrameComponent* topLeft = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(topLeft))
			{
				topLeft->SetPosition(Vector2::ZeroVector);
				topLeft->SetSize(Vector2(0.2f, 0.2f));
				topLeft->SetLockedFrame(true);
				topLeft->SetAnchorLeft(24.f);
				topLeft->SetAnchorTop(24.f);
			}

			FrameComponent* topRight = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(topRight))
			{
				topRight->SetPosition(Vector2(0.8f, 0.f));
				topRight->SetSize(Vector2(0.2f, 0.2f));
				topRight->SetLockedFrame(true);
				topRight->SetAnchorRight(24.f);
				topRight->SetAnchorTop(24.f);
			}

			FrameComponent* bottomLeft = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(bottomLeft))
			{
				bottomLeft->SetPosition(Vector2(0.f, 0.8f));
				bottomLeft->SetSize(Vector2(0.2f, 0.2f));
				bottomLeft->SetLockedFrame(true);
				bottomLeft->SetAnchorLeft(24.f);
				bottomLeft->SetAnchorBottom(24.f);
			}

			FrameComponent* bottomRight = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(bottomRight))
			{
				bottomRight->SetPosition(Vector2(0.8f, 0.8f));
				bottomRight->SetSize(Vector2(0.2f, 0.2f));
				bottomRight->SetLockedFrame(true);
				bottomRight->SetAnchorRight(24.f);
				bottomRight->SetAnchorBottom(24.f);
			}

			FrameComponent* verticalCenter = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(verticalCenter))
			{
				verticalCenter->SetLockedFrame(true);
				verticalCenter->SetAnchorLeft(68.f);
				verticalCenter->SetAnchorRight(68.f);
				verticalCenter->SetAnchorTop(24.f);
				verticalCenter->SetAnchorBottom(24.f);
			}

			FrameComponent* horizontalCenter = FrameComponent::CreateObject();
			if (anchorTester->AddComponent(horizontalCenter))
			{
				horizontalCenter->SetLockedFrame(true);
				horizontalCenter->SetAnchorLeft(24.f);
				horizontalCenter->SetAnchorRight(24.f);
				horizontalCenter->SetAnchorTop(68.f);
				horizontalCenter->SetAnchorBottom(68.f);
			}
		}
	}
}

void GuiTester::HandleLabelButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Label Components!"));
	ClearStageComponents();

	LabelComponent* topLeft = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(topLeft))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach top left label component to stage frame."));
	}
	else
	{
		topLeft->SetSize(Vector2(1.f, 1.f));
		topLeft->SetHorizontalAlignment(LabelComponent::HA_Left);
		topLeft->SetVerticalAlignment(LabelComponent::VA_Top);
		topLeft->SetText(TXT("Top Left"));
		StageComponents.push_back(topLeft);
	}

	LabelComponent* topCenter = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(topCenter))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach top center label component to stage frame."));
	}
	else
	{
		topCenter->SetSize(Vector2(1.f, 1.f));
		topCenter->SetHorizontalAlignment(LabelComponent::HA_Center);
		topCenter->SetVerticalAlignment(LabelComponent::VA_Top);
		topCenter->SetText(TXT("Top Center"));
		StageComponents.push_back(topCenter);
	}


	LabelComponent* topRight = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(topRight))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach top right label component to stage frame."));
	}
	else
	{
		topRight->SetSize(Vector2(1.f, 1.f));
		topRight->SetHorizontalAlignment(LabelComponent::HA_Right);
		topRight->SetVerticalAlignment(LabelComponent::VA_Top);
		topRight->SetText(TXT("Top Right"));
		StageComponents.push_back(topRight);
	}

	LabelComponent* centerRight = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(centerRight))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach center right label component to stage frame."));
	}
	else
	{
		centerRight->SetSize(Vector2(1.f, 1.f));
		centerRight->SetHorizontalAlignment(LabelComponent::HA_Right);
		centerRight->SetVerticalAlignment(LabelComponent::VA_Center);
		centerRight->SetText(TXT("Center Right"));
		StageComponents.push_back(centerRight);
	}

	LabelComponent* bottomRight = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(bottomRight))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach bottom right label component to stage frame."));
	}
	else
	{
		bottomRight->SetSize(Vector2(1.f, 1.f));
		bottomRight->SetHorizontalAlignment(LabelComponent::HA_Right);
		bottomRight->SetVerticalAlignment(LabelComponent::VA_Bottom);
		bottomRight->SetText(TXT("Bottom Right"));
		StageComponents.push_back(bottomRight);
	}

	LabelComponent* bottomCenter = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(bottomCenter))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach bottom center label component to stage frame."));
	}
	else
	{
		bottomCenter->SetSize(Vector2(1.f, 1.f));
		bottomCenter->SetHorizontalAlignment(LabelComponent::HA_Center);
		bottomCenter->SetVerticalAlignment(LabelComponent::VA_Bottom);
		bottomCenter->SetText(TXT("Bottom Center"));
		StageComponents.push_back(bottomCenter);
	}

	LabelComponent* bottomLeft = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(bottomLeft))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach bottom left label component to stage frame."));
	}
	else
	{
		bottomLeft->SetSize(Vector2(1.f, 1.f));
		bottomLeft->SetHorizontalAlignment(LabelComponent::HA_Left);
		bottomLeft->SetVerticalAlignment(LabelComponent::VA_Bottom);
		bottomLeft->SetText(TXT("Bottom Left"));
		StageComponents.push_back(bottomLeft);
	}

	LabelComponent* centerLeft = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(centerLeft))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach center left label component to stage frame."));
	}
	else
	{
		centerLeft->SetSize(Vector2(1.f, 1.f));
		centerLeft->SetHorizontalAlignment(LabelComponent::HA_Left);
		centerLeft->SetVerticalAlignment(LabelComponent::VA_Center);
		centerLeft->SetText(TXT("Center Left"));
		StageComponents.push_back(centerLeft);
	}

	LabelComponent* centerCenter = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(centerCenter))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach centered label component to stage frame."));
	}
	else
	{
		centerCenter->SetSize(Vector2(1.f, 1.f));
		centerCenter->SetHorizontalAlignment(LabelComponent::HA_Center);
		centerCenter->SetVerticalAlignment(LabelComponent::VA_Center);
		centerCenter->SetText(TXT("Centered"));
		StageComponents.push_back(centerCenter);
	}

	LabelComponent* largeText = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(largeText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach large text label component to stage frame."));
	}
	else
	{
		largeText->SetSize(Vector2(256, 96));
		largeText->SetPosition(Vector2(48, 64));
		largeText->SetHorizontalAlignment(LabelComponent::HA_Left);
		largeText->SetVerticalAlignment(LabelComponent::VA_Top);
		largeText->SetText(TXT("Large Text (32)"));
		largeText->SetCharacterSize(32);
		StageComponents.push_back(largeText);
	}

	LabelComponent* mediumText = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(mediumText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach medium text label component to stage frame."));
	}
	else
	{
		mediumText->SetSize(Vector2(384, 96));
		mediumText->SetPosition(Vector2(48, 110));
		mediumText->SetHorizontalAlignment(LabelComponent::HA_Left);
		mediumText->SetVerticalAlignment(LabelComponent::VA_Top);
		mediumText->SetText(TXT("Medium Text (16)"));
		mediumText->SetCharacterSize(16);
		StageComponents.push_back(mediumText);
	}

	LabelComponent* smallText = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(smallText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach small text label component to stage frame."));
	}
	else
	{
		smallText->SetSize(Vector2(256, 96));
		smallText->SetPosition(Vector2(48, 144));
		smallText->SetHorizontalAlignment(LabelComponent::HA_Left);
		smallText->SetVerticalAlignment(LabelComponent::VA_Top);
		smallText->SetText(TXT("Small Text (8)"));
		smallText->SetCharacterSize(8);
		StageComponents.push_back(smallText);
	}

	LabelComponent* clampedText = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(clampedText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach clamped text label component to stage frame."));
	}
	else
	{
		FLOAT clampedTextWidth = 96.f;
		clampedText->SetSize(Vector2(clampedTextWidth, 96));
		clampedText->SetPosition(Vector2(StageFrame->ReadCachedAbsSize().X - clampedTextWidth, 64));
		clampedText->SetHorizontalAlignment(LabelComponent::HA_Left);
		clampedText->SetVerticalAlignment(LabelComponent::VA_Top);
		clampedText->SetWrapText(false);
		clampedText->SetClampText(true);
		clampedText->SetText(TXT("Clamped Text - Clamped Text - Clamped Text - Clamped Text - Clamped Text"));
		StageComponents.push_back(clampedText);
	}

	FrameComponent* autoRefreshFrame = FrameComponent::CreateObject();
	if (!StageFrame->AddComponent(autoRefreshFrame))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach auto refreshing frame component to stage frame."));
	}
	else
	{
		const FLOAT borderThickness = 6.f;
		autoRefreshFrame->SetSize(Vector2(150.f, 96.f));
		autoRefreshFrame->SetPosition(Vector2(0.2f, 0.3f));
		autoRefreshFrame->SetLockedFrame(false);
		autoRefreshFrame->SetBorderThickness(borderThickness);
		StageComponents.push_back(autoRefreshFrame);

		LabelComponent* autoRefreshingText = LabelComponent::CreateObject();
		if (!autoRefreshFrame->AddComponent(autoRefreshingText))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach auto refreshing text label component to the auto refresh frame component."));
		}
		else
		{
			autoRefreshingText->SetAutoRefresh(false);
			autoRefreshingText->SetSize(1.f, 1.f);
			autoRefreshingText->SetAnchorTop(borderThickness);
			autoRefreshingText->SetAnchorRight(borderThickness);
			autoRefreshingText->SetAnchorBottom(borderThickness);
			autoRefreshingText->SetAnchorLeft(borderThickness);
			autoRefreshingText->SetClampText(true);
			autoRefreshingText->SetWrapText(true);
			autoRefreshingText->SetText(TXT("This text should wrap and clamp every time owning frame component changes size."));
			autoRefreshingText->SetAutoRefresh(true);
		}
	}

	LabelComponent* singleSpacedLine = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(singleSpacedLine))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach single spaced clamped text label component to stage frame."));
	}
	else
	{
		FLOAT clampedTextWidth = 192.f;
		singleSpacedLine->SetSize(Vector2(clampedTextWidth, 96));
		singleSpacedLine->SetPosition(Vector2(StageFrame->ReadCachedAbsSize().X - clampedTextWidth, 32.f + (StageFrame->ReadCachedAbsSize().Y * 0.5f)));
		singleSpacedLine->SetHorizontalAlignment(LabelComponent::HA_Left);
		singleSpacedLine->SetVerticalAlignment(LabelComponent::VA_Top);
		singleSpacedLine->SetWrapText(true);
		singleSpacedLine->SetClampText(true);
		singleSpacedLine->SetLineSpacing(0.f);
		singleSpacedLine->SetText(TXT("Clamped Single Line Spacing - Clamped Single Line Spacing - Clamped Single Line Spacing - Clamped Single Line Spacing - Clamped Single Line Spacing."));
		StageComponents.push_back(singleSpacedLine);
	}

	LabelComponent* multiSpacedLine = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(multiSpacedLine))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach multi spaced unclamped text label component to stage frame."));
	}
	else
	{
		FLOAT clampedTextWidth = 160.f;
		multiSpacedLine->SetSize(Vector2(clampedTextWidth, 128));
		multiSpacedLine->SetPosition(Vector2((StageFrame->ReadCachedAbsSize().X * 0.5f) - clampedTextWidth, 32.f + (StageFrame->ReadCachedAbsSize().Y * 0.5f)));
		multiSpacedLine->SetHorizontalAlignment(LabelComponent::HA_Left);
		multiSpacedLine->SetVerticalAlignment(LabelComponent::VA_Top);
		multiSpacedLine->SetWrapText(true);
		multiSpacedLine->SetClampText(false);
		multiSpacedLine->SetLineSpacing(multiSpacedLine->GetCharacterSize().ToFLOAT());
		multiSpacedLine->SetText(TXT("Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing."));
		StageComponents.push_back(multiSpacedLine);
	}

	LabelComponent* newLineLabel = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(newLineLabel))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach new line character test label component to stage frame."));
	}
	else
	{
		newLineLabel->SetSize(Vector2(96, 128));
		newLineLabel->SetPosition(Vector2((StageFrame->ReadCachedAbsSize().X * 0.67f), (StageFrame->ReadCachedAbsSize().Y * 0.25f)));
		newLineLabel->SetHorizontalAlignment(LabelComponent::HA_Left);
		newLineLabel->SetVerticalAlignment(LabelComponent::VA_Top);
		newLineLabel->SetWrapText(false);
		newLineLabel->SetClampText(false);
		newLineLabel->SetText(TXT("New Line \n character.\n"));
		StageComponents.push_back(newLineLabel);
	}
}

void GuiTester::HandleAutoSizeLabelButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Auto Sizing Label Components!"));
	ClearStageComponents();

	std::function<void(LabelComponent*)> addFrameTo = [](LabelComponent* labelToAddFrameTo)
	{
		FrameComponent* newFrame = FrameComponent::CreateObject();
		if (labelToAddFrameTo->AddComponent(newFrame))
		{
			newFrame->SetPosition(Vector2(0.f, 0.f));
			newFrame->SetSize(Vector2(1.f, 1.f));
			newFrame->SetLockedFrame(true);
			BorderedSpriteComponent* renderedComp = newFrame->RenderComponent.Get();
			if (renderedComp != nullptr)
			{
				renderedComp->ClearSpriteTexture();
				renderedComp->TopBorder = nullptr;
				renderedComp->RightBorder = nullptr;
				renderedComp->BottomBorder = nullptr;
				renderedComp->LeftBorder = nullptr;
				renderedComp->TopRightCorner = nullptr;
				renderedComp->BottomRightCorner = nullptr;
				renderedComp->BottomLeftCorner = nullptr;
				renderedComp->TopLeftCorner = nullptr;
				renderedComp->FillColor = Color(16, 16, 16, 128);
			}
		}
	};

	FLOAT numLines = 0.f;
	const FLOAT lineHeight = dynamic_cast<const LabelComponent*>(LabelComponent::SStaticClass()->GetDefaultObject())->GetCharacterSize().ToFLOAT();

	LabelComponent* description = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(description))
	{
		description->SetAutoRefresh(false);
		description->SetPosition(Vector2(0.f, lineHeight * numLines));
		description->SetSize(Vector2(0.f, lineHeight * 2));
		description->SetAutoSizeHorizontal(true);
		description->SetClampText(false);
		description->SetText(TXT("These label components are auto sized where their size is\nautomatically adjusted based on the text."));
		numLines += 2.f;
		description->SetAutoRefresh(true);
		addFrameTo(description);
		StageComponents.push_back(description);
	}

	++numLines; //Add a blank line

	LabelComponent* horizontalTest = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(horizontalTest))
	{
		horizontalTest->SetAutoRefresh(false);
		horizontalTest->SetPosition(Vector2(0.f, lineHeight * numLines));
		horizontalTest->SetSize(Vector2(0.f, lineHeight * 3.f));
		horizontalTest->SetAutoSizeHorizontal(true);
		horizontalTest->SetClampText(true);
		horizontalTest->SetText(TXT("This label is scaled horizontally automatically based on it's longest line.\nThe shorter lines do not influence the scale.\nThe fourth line is clamped.\nYou should not be able to read this line."));
		numLines += 3.f;
		horizontalTest->SetAutoRefresh(true);
		addFrameTo(horizontalTest);
		StageComponents.push_back(horizontalTest);
	}

	++numLines; //Add a blank line

	LabelComponent* unclampedText = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(unclampedText))
	{
		unclampedText->SetAutoRefresh(false);
		unclampedText->SetPosition(Vector2(0.f, lineHeight * numLines));
		unclampedText->SetSize(Vector2(0.f, lineHeight * 2.f));
		unclampedText->SetAutoSizeHorizontal(true);
		unclampedText->SetClampText(false);
		unclampedText->SetText(TXT("This label is scaled horizontally.\nThis is also unclamped, which would make the line below visible.\nEven though this line is outside range, the unclamped property allows overflow."));
		numLines += 3.f; //Size is 2 to test text overflow, but set numLines to 3 to prevent the next LabelComponent from overlapping with this label.
		unclampedText->SetAutoRefresh(true);
		addFrameTo(unclampedText);
		StageComponents.push_back(unclampedText);
	}

	++numLines; //Add a blank line

	LabelComponent* verticalTest = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(verticalTest))
	{
		verticalTest->SetAutoRefresh(false);
		verticalTest->SetPosition(Vector2(0.f, lineHeight * numLines));
		verticalTest->SetSize(Vector2(0.5f, 0.f));
		verticalTest->SetAutoSizeVertical(true);
		verticalTest->SetWrapText(false);
		verticalTest->SetHorizontalAlignment(LabelComponent::HA_Center);
		verticalTest->SetText(TXT("This label is scaled vertically.\nThis label also has wrap text to false\nwhich may cause text to go beyond the width of the component.\nHorizontal alignment is based on component width.\nLine 5."));
		numLines += 5.f;
		verticalTest->SetAutoRefresh(true);
		addFrameTo(verticalTest);
		StageComponents.push_back(verticalTest);
	}

	++numLines; //Add a blank line

	LabelComponent* verticalWrapText = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(verticalWrapText))
	{
		verticalWrapText->SetAutoRefresh(false);
		verticalWrapText->SetPosition(Vector2(0.f, lineHeight * numLines));
		verticalWrapText->SetSize(Vector2(0.4f, 0.f));
		verticalWrapText->SetAutoSizeVertical(true);
		verticalWrapText->SetWrapText(true);
		verticalWrapText->SetHorizontalAlignment(LabelComponent::HA_Right);
		verticalWrapText->SetText(TXT("This label is scaled vertically.\nThis label also has wrap text to true which may cause text to move to the next line.\nThis is right aligned.\nLine 4."));
		numLines += 6.f;
		verticalWrapText->SetAutoRefresh(true);
		addFrameTo(verticalWrapText);
		StageComponents.push_back(verticalWrapText);
	}

	++numLines; //Add a blank line

	LabelComponent* autoSizeText = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(autoSizeText))
	{
		autoSizeText->SetAutoRefresh(false);
		autoSizeText->SetPosition(Vector2(0.f, lineHeight * numLines));
		autoSizeText->SetSize(Vector2(0.f, 0.f));
		autoSizeText->SetAutoSizeVertical(true);
		autoSizeText->SetAutoSizeHorizontal(true);
		autoSizeText->SetHorizontalAlignment(LabelComponent::HA_Center);
		autoSizeText->SetText(TXT("This label is automically scaled vertically and horizontally.\nIt's a combination where the label width is determined by the widest line such as this one,\nand the height of the label component is determined based on the number of lines.\nWrapping and clamping does not apply."));
		numLines += 4.f;
		autoSizeText->SetAutoRefresh(true);
		addFrameTo(autoSizeText);
		StageComponents.push_back(autoSizeText); 
	}
}

void GuiTester::HandleButtonButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Button Components!"));
	ClearStageComponents();

	Texture* textureStates = GuiTheme::GetGuiTheme()->ButtonSingleSpriteBackground.Get();
	FLOAT buttonHeight = 24.f;
	FLOAT buttonWidth = 164.f;
	FLOAT buttonInterval = 8.f;
	FLOAT buttonPosition = 16.f;

	ButtonComponent* basicButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(basicButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach basic button component to stage frame."));
	}
	else
	{
		basicButton->SetSize(Vector2(buttonWidth, buttonHeight));
		basicButton->SetPosition(Vector2(16, buttonPosition));
		basicButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		basicButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		basicButton->ReplaceStateComponent(nullptr);
		buttonPosition += buttonHeight + buttonInterval;
		basicButton->SetCaptionText("No State Button");
		StageComponents.push_back(basicButton);
	}

	Vector2 noCaptionPosition(16, buttonPosition);
	ButtonComponent* noCaption = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(noCaption))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach no caption button component to stage frame."));
	}
	else
	{
		noCaption->SetSize(Vector2(buttonWidth, buttonHeight));
		noCaption->SetPosition(noCaptionPosition);
		noCaption->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		noCaption->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		noCaption->CaptionComponent->Destroy();
		noCaption->CaptionComponent = nullptr;
		StageComponents.push_back(noCaption);
	}

	LabelComponent* tip = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(tip))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach label component about the captionless button component to stage frame."));
	}
	else
	{
		tip->SetSize(Vector2(buttonWidth, buttonHeight));
		tip->SetPosition(Vector2(noCaptionPosition.X + buttonWidth + 16, noCaptionPosition.Y));
		tip->SetText(TXT("<---- No Caption"));
		StageComponents.push_back(tip);
	}

	ButtonComponent* multiStateButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(multiStateButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach multi-state button component to stage frame."));
	}
	else
	{
		multiStateButton->SetSize(Vector2(buttonWidth, buttonHeight));
		multiStateButton->SetPosition(Vector2(16, buttonPosition));
		multiStateButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		multiStateButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		multiStateButton->SetCaptionText(TXT("Multi-state Button"));
		multiStateButton->GetBackground()->RenderComponent->SetSpriteTexture(textureStates);
		multiStateButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		StageComponents.push_back(multiStateButton);
	}

	ButtonComponent* enabledButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(enabledButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach enabled button component to stage frame."));
	}
	else
	{
		enabledButton->SetSize(Vector2(buttonWidth, buttonHeight));
		enabledButton->SetPosition(Vector2(16, buttonPosition));
		enabledButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		enabledButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		enabledButton->SetCaptionText(TXT("Enabled Button"));
		enabledButton->SetEnabled(true);
		enabledButton->GetBackground()->RenderComponent->SetSpriteTexture(textureStates);
		enabledButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		StageComponents.push_back(enabledButton);
	}

	ButtonComponent* disabledButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(disabledButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach disabled button component to stage frame."));
	}
	else
	{
		disabledButton->SetSize(Vector2(buttonWidth, buttonHeight));
		disabledButton->SetPosition(Vector2(16, buttonPosition));
		disabledButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		disabledButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		disabledButton->SetCaptionText(TXT("Disabled Button"));
		disabledButton->SetEnabled(false);
		disabledButton->GetBackground()->RenderComponent->SetSpriteTexture(textureStates);
		disabledButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		StageComponents.push_back(disabledButton);
	}

	ButtonComponent* toggledButton1 = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(toggledButton1))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach toggleable button component to stage frame."));
	}
	else
	{
		toggledButton1->SetSize(Vector2(buttonWidth, buttonHeight));
		toggledButton1->SetPosition(Vector2(16, buttonPosition));
		toggledButton1->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		toggledButton1->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestToggleReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		toggledButton1->SetCaptionText(TXT("Toggleable Button"));
		toggledButton1->SetEnabled(true);
		toggledButton1->GetBackground()->RenderComponent->SetSpriteTexture(textureStates);
		toggledButton1->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		ToggleButtons[0] = toggledButton1;
		StageComponents.push_back(toggledButton1);
	}

	ButtonComponent* toggledButton2 = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(toggledButton2))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach toggleable button component to stage frame."));
	}
	else
	{
		toggledButton2->SetSize(Vector2(buttonWidth, buttonHeight));
		toggledButton2->SetPosition(Vector2(16, buttonPosition));
		toggledButton2->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		toggledButton2->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestToggleReleased, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		toggledButton2->SetCaptionText(TXT("Toggleable Button"));
		toggledButton2->SetEnabled(false);
		toggledButton2->GetBackground()->RenderComponent->SetSpriteTexture(textureStates);
		toggledButton2->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		ToggleButtons[1] = toggledButton2;
		StageComponents.push_back(toggledButton2);
	}

	ButtonComponent* outerButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(outerButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach outer button component to stage frame."));
	}
	else
	{
		outerButton->SetSize(Vector2(buttonWidth, buttonHeight * 3));
		outerButton->SetPosition(Vector2(16, buttonPosition));
		outerButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		outerButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		buttonPosition += (buttonHeight * 3) + buttonInterval;
		outerButton->SetCaptionText(TXT("Outer Button"));
		outerButton->CaptionComponent->SetVerticalAlignment(LabelComponent::VA_Top);
		outerButton->GetBackground()->RenderComponent->SetSpriteTexture(textureStates);
		outerButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		StageComponents.push_back(outerButton);

		ButtonComponent* innerButton = ButtonComponent::CreateObject();
		if (!outerButton->AddComponent(innerButton))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach inner button component to outer button."));
		}
		else
		{
			innerButton->SetSize(Vector2(buttonWidth - 12, buttonHeight));
			innerButton->SetPosition(Vector2(8.f, buttonHeight));
			innerButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
			innerButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
			innerButton->SetCaptionText(TXT("Inner Button"));
			innerButton->GetBackground()->RenderComponent->SetSpriteTexture(textureStates);
			innerButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			StageComponents.push_back(innerButton);
		}
	}

	ButtonComponent* colorChangeButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(colorChangeButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach color changing button component to the stage."));
	}
	else
	{
		colorChangeButton->SetSize(Vector2(buttonWidth, buttonHeight));
		colorChangeButton->SetPosition(Vector2(16.f, buttonPosition));
		buttonPosition += buttonHeight + buttonInterval;
		colorChangeButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		colorChangeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestReleased, void, ButtonComponent*));
		colorChangeButton->SetCaptionText(TXT("Solid Color State"));
		colorChangeButton->GetBackground()->RenderComponent->ClearSpriteTexture();
		ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(colorChangeButton->ReplaceStateComponent(ColorButtonState::SStaticClass()));
		if (colorState != nullptr)
		{
			colorState->SetDefaultColor(Color(255, 0, 0));
			colorState->SetDisabledColor(Color(sf::Color::Black));
			colorState->SetHoverColor(Color(255, 48, 48));
			colorState->SetDownColor(Color(200, 0, 0));
		}

		StageComponents.push_back(colorChangeButton);
	}

	ButtonComponent* rightClickButton = ButtonComponent::CreateObject();
	if (!StageFrame->AddComponent(rightClickButton))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach right click button component to stage frame."));
	}
	else
	{
		ButtonTestCounter = 0;
		rightClickButton->SetSize(Vector2(buttonWidth, buttonHeight));
		const Vector2 rightClickButtonPos(16.f, buttonPosition);
		rightClickButton->SetPosition(rightClickButtonPos);
		rightClickButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestPressed, void, ButtonComponent*));
		rightClickButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestIncrementCounter, void, ButtonComponent*));
		rightClickButton->SetButtonReleasedRightClickHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleButtonTestDecrementCounter, void, ButtonComponent*));
		buttonPosition += buttonHeight + buttonInterval;
		rightClickButton->SetCaptionText(TXT("Counter: ") + ButtonTestCounter.ToString());
		StageComponents.push_back(rightClickButton);

		LabelComponent* rightClickTip = LabelComponent::CreateObject();
		if (StageFrame->AddComponent(rightClickTip))
		{
			rightClickTip->SetSize(Vector2(buttonWidth * 3.f, buttonHeight));
			rightClickTip->SetPosition(Vector2(rightClickButtonPos.X + buttonWidth + 16, rightClickButtonPos.Y));
			rightClickTip->SetText(TXT("<--- Left click to increment. Right click to decrement."));
			StageComponents.push_back(rightClickTip);
		}
	}
}

void GuiTester::HandleCheckboxButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Checkbox Components!"));
	ClearStageComponents();

	CheckboxComponent* basicCheckbox = CheckboxComponent::CreateObject();
	if (!StageFrame->AddComponent(basicCheckbox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach basic checkbox component to stage frame."));
	}
	else
	{
		basicCheckbox->SetSize(Vector2(200.f, 32.f));
		basicCheckbox->SetPosition(Vector2(8.f, 8.f));
		basicCheckbox->CaptionComponent->SetText(TXT("Basic Checkbox"));
		basicCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleCheckboxToggle, void, CheckboxComponent*);
		StageComponents.push_back(basicCheckbox);
	}

	CheckboxComponent* reversedCheckbox = CheckboxComponent::CreateObject();
	if (!StageFrame->AddComponent(reversedCheckbox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach reversed checkbox component to stage frame."));
	}
	else
	{
		reversedCheckbox->SetSize(Vector2(200.f, 32.f));
		reversedCheckbox->SetPosition(Vector2(8.f, 80.f));
		reversedCheckbox->CaptionComponent->SetText(TXT("Reversed Checkbox"));
		reversedCheckbox->SetCheckboxRightSide(false);
		reversedCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleCheckboxToggle, void, CheckboxComponent*);
		StageComponents.push_back(reversedCheckbox);
	}

	EnableToggleCheckbox = CheckboxComponent::CreateObject();
	if (!StageFrame->AddComponent(EnableToggleCheckbox.Get()))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach toggle enableness checkbox component to stage frame."));
	}
	else
	{
		EnableToggleCheckbox->SetSize(Vector2(200.f, 32.f));
		EnableToggleCheckbox->SetPosition(Vector2(8.f, 120.f));
		EnableToggleCheckbox->CaptionComponent->SetText(TXT("Set Enabled"));
		EnableToggleCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleToggleEnableCheckboxToggle, void, CheckboxComponent*);
		StageComponents.push_back(EnableToggleCheckbox.Get());
	}

	VisibilityToggleCheckbox = CheckboxComponent::CreateObject();
	if (!StageFrame->AddComponent(VisibilityToggleCheckbox.Get()))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach toggle visibility checkbox component to stage frame."));
	}
	else
	{
		VisibilityToggleCheckbox->SetSize(Vector2(200.f, 32.f));
		VisibilityToggleCheckbox->SetPosition(Vector2(8.f, 160.f));
		VisibilityToggleCheckbox->CaptionComponent->SetText(TXT("Set Visibility"));
		VisibilityToggleCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleToggleVisibilityCheckboxToggle, void, CheckboxComponent*);
		StageComponents.push_back(VisibilityToggleCheckbox.Get());
	}

	TargetTestCheckbox = CheckboxComponent::CreateObject();
	if (!StageFrame->AddComponent(TargetTestCheckbox.Get()))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach target checkbox component to stage frame."));
	}
	else
	{
		TargetTestCheckbox->SetSize(Vector2(200.f, 32.f));
		TargetTestCheckbox->SetPosition(Vector2(8.f, 200.f));
		TargetTestCheckbox->CaptionComponent->SetText(TXT("Target Checkbox"));
		TargetTestCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleCheckboxToggle, void, CheckboxComponent*);
		StageComponents.push_back(TargetTestCheckbox.Get());
	}

	FrameComponent* checkboxContainer = FrameComponent::CreateObject();
	if (!StageFrame->AddComponent(checkboxContainer))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach checkboxContainer to StageFrame."));
	}
	else
	{
		checkboxContainer->SetSize(Vector2(200.f, 32.f));
		checkboxContainer->SetPosition(Vector2(8.f, 240.f));
		checkboxContainer->SetLockedFrame(true);
		checkboxContainer->SetBorderThickness(1.f);
		StageComponents.push_back(checkboxContainer);

		CheckboxComponent* containedCheckbox = CheckboxComponent::CreateObject();
		if (!checkboxContainer->AddComponent(containedCheckbox))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]: Failed to attach containedCheckbox to checkboxContainer."));
		}
		else
		{
			containedCheckbox->SetPosition(Vector2::ZeroVector);
			containedCheckbox->SetSize(Vector2(1.f, 1.f));
			containedCheckbox->CaptionComponent->SetText(TXT("Testing Contained Checkbox"));
			containedCheckbox->CaptionComponent->SetCharacterSize(12);
		}
	}
}

void GuiTester::HandleScrollbarButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar Components!"));
	ClearStageComponents();

	//Create the button components
	const Vector2 buttonSize(96.f, 48.f);
	INT buttonCounter = 0;
	ScrollbarTesterLabelTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterLabelTest.Get()))
	{
		ScrollbarTesterLabelTest->SetAnchorLeft(8.f);
		ScrollbarTesterLabelTest->SetAnchorTop(8.f + (buttonCounter.ToFLOAT() * buttonSize.Y));
		ScrollbarTesterLabelTest->SetSize(buttonSize);
		ScrollbarTesterLabelTest->SetCaptionText(TXT("Scrollable Label"));
		ScrollbarTesterLabelTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterLabelTestClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterLabelTest.Get());
	}

	ScrollbarTesterButtonTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterButtonTest.Get()))
	{
		ScrollbarTesterButtonTest->SetAnchorLeft(8.f);
		ScrollbarTesterButtonTest->SetAnchorTop(8.f + (buttonCounter.ToFLOAT() * (buttonSize.Y + 8.f)));
		ScrollbarTesterButtonTest->SetSize(buttonSize);
		ScrollbarTesterButtonTest->SetCaptionText(TXT("Scrollable Buttons"));
		ScrollbarTesterButtonTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterButtonTestClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterButtonTest.Get());
	}

	ScrollbarTesterSizeChangeTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterSizeChangeTest.Get()))
	{
		ScrollbarTesterSizeChangeTest->SetAnchorLeft(8.f);
		ScrollbarTesterSizeChangeTest->SetAnchorTop(16.f + (buttonCounter.ToFLOAT() * (buttonSize.Y + 8.f)));
		ScrollbarTesterSizeChangeTest->SetSize(buttonSize);
		ScrollbarTesterSizeChangeTest->SetCaptionText(TXT("Adjustable Size Test"));
		ScrollbarTesterSizeChangeTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterSizeChangeClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterSizeChangeTest.Get());
	}

	ScrollbarTesterDifferentFrameSizeTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterDifferentFrameSizeTest.Get()))
	{
		ScrollbarTesterDifferentFrameSizeTest->SetAnchorLeft(8.f);
		ScrollbarTesterDifferentFrameSizeTest->SetAnchorTop(16.f + (buttonCounter.ToFLOAT() * (buttonSize.Y + 8.f)));
		ScrollbarTesterDifferentFrameSizeTest->SetSize(buttonSize);
		ScrollbarTesterDifferentFrameSizeTest->SetCaptionText(TXT("Adjustable Frame Test"));
		ScrollbarTesterDifferentFrameSizeTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterDifferentFrameSizeClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterDifferentFrameSizeTest.Get());
	}

	ScrollbarTesterOuterSizeChangeTest = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterOuterSizeChangeTest.Get()))
	{
		ScrollbarTesterOuterSizeChangeTest->SetAnchorLeft(8.f);
		ScrollbarTesterOuterSizeChangeTest->SetAnchorTop(16.f + (buttonCounter.ToFLOAT() * (buttonSize.Y + 8.f)));
		ScrollbarTesterOuterSizeChangeTest->SetSize(buttonSize);
		ScrollbarTesterOuterSizeChangeTest->SetCaptionText(TXT("Adjustable Scrollbar Size Test"));
		ScrollbarTesterOuterSizeChangeTest->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterOuterSizeChangeClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterOuterSizeChangeTest.Get());
	}

	ScrollbarTesterNestedScrollbars = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterNestedScrollbars.Get()))
	{
		ScrollbarTesterNestedScrollbars->SetAnchorLeft(8.f);
		ScrollbarTesterNestedScrollbars->SetAnchorTop(16.f + (buttonCounter.ToFLOAT() * (buttonSize.Y + 8.f)));
		ScrollbarTesterNestedScrollbars->SetSize(buttonSize);
		ScrollbarTesterNestedScrollbars->SetCaptionText(TXT("Nested Scrollbars"));
		ScrollbarTesterNestedScrollbars->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterNestedScrollbarsClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterNestedScrollbars.Get());
	}

	ScrollbarTesterComplexUi = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTesterComplexUi.Get()))
	{
		ScrollbarTesterComplexUi->SetAnchorLeft(8.f);
		ScrollbarTesterComplexUi->SetAnchorTop(16.f + (buttonCounter.ToFLOAT() * (buttonSize.Y + 8.f)));
		ScrollbarTesterComplexUi->SetSize(buttonSize);
		ScrollbarTesterComplexUi->SetCaptionText(TXT("Complex UI"));
		ScrollbarTesterComplexUi->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterComplexUiClicked, void, ButtonComponent*));
		buttonCounter++;
		StageComponents.push_back(ScrollbarTesterComplexUi.Get());
	}

	//Setup the ViewedObject and scrollbar
	ScrollbarTester = ScrollbarComponent::CreateObject();
	if (StageFrame->AddComponent(ScrollbarTester.Get()))
	{
		ScrollbarTesterAnchorTop = 8.f;
		ScrollbarTesterAnchorRight = 8.f;
		ScrollbarTesterAnchorBottom = 8.f;
		ScrollbarTesterAnchorLeft = buttonSize.X + 16.f;

		ScrollbarTester->SetAnchorTop(ScrollbarTesterAnchorTop);
		ScrollbarTester->SetAnchorRight(ScrollbarTesterAnchorRight);
		ScrollbarTester->SetAnchorBottom(ScrollbarTesterAnchorBottom);
		ScrollbarTester->SetAnchorLeft(ScrollbarTesterAnchorLeft);
		RenderTexture* renderTexture = ScrollbarTester->GetFrameTexture();
		CHECK(renderTexture != nullptr)
		renderTexture->ResetColor = Color(24, 24, 24, 255);
		StageComponents.push_back(ScrollbarTester.Get());

		ScrollbarTesterViewedObject = GuiEntity::CreateObject();
		ScrollbarTesterViewedObject->SetAutoSizeHorizontal(true);
		ScrollbarTesterViewedObject->SetAutoSizeVertical(true);
		ScrollbarTesterViewedObject->SetEnableFractionScaling(false);
		ScrollbarTester->SetViewedObject(ScrollbarTesterViewedObject.Get());
	}
}

void GuiTester::HandleTextFieldButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Text Field Components!"));
	ClearStageComponents();

	TextFieldComponent* uninteractableField = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(uninteractableField))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach uninteractable text field to stage frame."));
	}
	else
	{
		uninteractableField->SetSize(Vector2(256, 64));
		uninteractableField->SetPosition(Vector2(StageFrame->GetBorderThickness(), StageFrame->GetBorderThickness()));
		uninteractableField->SetEnableFractionScaling(false);
		uninteractableField->SetEditable(false);
		uninteractableField->SetSelectable(false);
		uninteractableField->SetText(TXT("This text is not selectable or editable.  However this text should still be scrollable whenever it needs to.\n\nLine break test."));
		StageComponents.push_back(uninteractableField);
	}

	TextFieldComponent* selectableField = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(selectableField))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach selectable text field to stage frame."));
	}
	else
	{
		selectableField->SetSize(Vector2(256, 64));
		selectableField->SetPosition(Vector2(StageFrame->GetBorderThickness() + 300.f, StageFrame->GetBorderThickness()));
		selectableField->SetEnableFractionScaling(false);
		selectableField->SetEditable(false);
		selectableField->SetSelectable(true);
		selectableField->SetText(TXT("This text is selectable, but this is not editable.  You should be able to set cursor and highlight positions to this field, and press Ctrl+C to copy the selected text, and be able to paste the text to an external program."));
		StageComponents.push_back(selectableField);
	}

	TextFieldComponent* editableText = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(editableText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach editable text field to stage frame."));
	}
	else
	{
		editableText->SetSize(Vector2(256, 64));
		editableText->SetPosition(Vector2(StageFrame->GetBorderThickness(), StageFrame->GetBorderThickness() + 128.f));
		editableText->SetEnableFractionScaling(false);
		editableText->SetEditable(true);
		editableText->SetSelectable(true);
		editableText->SetText(TXT("This text is selectable and editable.  You can click on the text field to set the cursor position.  You can use the arrows to navigate the cursor around.  You also have copy, paste, and cut functionality."));
		StageComponents.push_back(editableText);
	}

	TextFieldComponent* limitedText = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(limitedText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach limited text field to stage frame."));
	}
	else
	{
		limitedText->SetSize(Vector2(256, 64));
		limitedText->SetPosition(Vector2(StageFrame->GetBorderThickness() + 300.f, StageFrame->GetBorderThickness() + 128.f));
		limitedText->SetEditable(true);
		limitedText->SetSelectable(true);
		limitedText->MaxNumCharacters = 64;
		limitedText->SetText(TXT("This editable Text Field has a 64 character limit."));
		StageComponents.push_back(limitedText);
	}

	//TODO:  Implement forbidden characters (no new lines for this instance)
#if 0
	TextFieldComponent* singleLineText = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(singleLineText))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach single line text field to stage frame."));
	}
	else
	{
		singleLineText->SetSize(Vector2(256, singleLineText->GetCharacterSize().ToFLOAT() + 4.f));
		singleLineText->SetPosition(Vector2(StageFrame->GetBorderThickness(), StageFrame->GetBorderThickness() + 224.f));
		singleLineText->SetEditable(true);
		singleLineText->SetSelectable(true);
		singleLineText->SetSingleLine(true);
		singleLineText->GetTextScrollbar()->SetHideWhenInsufficientScrollPos(true);
		singleLineText->SetText(TXT("This text field has a single line."));
		StageComponents.push_back(singleLineText);
	}
#endif

	TextFieldComponent* stressTest = TextFieldComponent::CreateObject();
	if (!StageFrame->AddComponent(stressTest))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach stress test text field to stage frame."));
	}
	else
	{
		stressTest->SetSize(Vector2(256.f, 256.f));
		stressTest->SetPosition(Vector2(StageFrame->GetBorderThickness() + 300.f, StageFrame->GetBorderThickness() + 224.f));
		stressTest->SetEditable(true);
		stressTest->SetSelectable(true);
		stressTest->SetText(TXT("\tFour score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal. \
		\n\tNow we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this. \
		\n\tBut, in a larger sense, we can not dedicate-we can not consecrate-we can not hallow-this ground. The brave men, living and dead, who struggled here, have consecrated it, far above our poor power to add or detract. The world will little note, nor long remember what we say here, but it can never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the great task remaining before us-that from these honored dead we take increased devotion to that cause for which they gave the last full measure of devotion-that we here highly resolve that these dead shall not have died in vain-that this nation, under God, shall have a new birth of freedom-and that government of the people, by the people, for the people, shall not perish from the earth."));
		StageComponents.push_back(stressTest);
	}
}

void GuiTester::HandleListBoxButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying List Box Components!"));
	ClearStageComponents();

	std::vector<GuiDataElement<DString>> listContent;
	for (UINT_TYPE i = 0; i < ListBoxStrings.size(); i++)
	{
		listContent.push_back(GuiDataElement<DString>(ListBoxStrings.at(i), ListBoxStrings.at(i)));
	}

	ListBoxComponent* basicListBox = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(basicListBox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach basic list box to stage frame."));
	}
	else
	{
		basicListBox->SetSize(Vector2(160, 200));
		basicListBox->SetPosition(Vector2(8, 32));

		basicListBox->SetList(listContent);
		basicListBox->OnItemSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleBasicListBoxOptionSelected, void, INT);
		basicListBox->OnItemDeselected = SDFUNCTION_1PARAM(this, GuiTester, HandleBasicListBoxOptionDeselected, void, INT);
		StageComponents.push_back(basicListBox);
	}

	ListBoxComponent* scrollbarListBox = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(scrollbarListBox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach scrollbar list box to stage frame."));
	}
	else
	{
		scrollbarListBox->SetSize(Vector2(160, 200));
		scrollbarListBox->SetPosition(Vector2(176, 32));

		std::vector<GuiDataElement<INT>> listContent;
		for (INT i = 0; i < 50; i++)
		{
			listContent.push_back(GuiDataElement<INT>(i, i.ToString()));
		}
		scrollbarListBox->SetList(listContent);
		StageComponents.push_back(scrollbarListBox);
	}

	ListBoxComponent* readOnlyBox = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(readOnlyBox))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach read only list box to stage frame."));
	}
	else
	{
		readOnlyBox->SetSize(Vector2(160, 200));
		readOnlyBox->SetPosition(Vector2(344, 32));

		readOnlyBox->SetList(listContent);
		readOnlyBox->SetReadOnly(true);

		readOnlyBox->OnItemSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleReadOnlyBoxOptionSelected, void, INT);
		readOnlyBox->OnItemDeselected = SDFUNCTION_1PARAM(this, GuiTester, HandleReadOnlyBoxOptionDeselected, void, INT);
		StageComponents.push_back(readOnlyBox);

		LabelComponent* description = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(description))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach read only List Box's description label to stage frame."));
		}
		else
		{
			description->SetSize(Vector2(160, 16));
			description->SetPosition(readOnlyBox->ReadPosition() - Vector2(0, 16));
			description->SetClampText(true);
			description->SetWrapText(false);
			description->SetText(TXT("Read Only"));
			StageComponents.push_back(description);
		}
	}

	MaxNumSelectableListBox = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(MaxNumSelectableListBox.Get()))
	{
		MaxNumSelectableListBox = nullptr;
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach list box with a limit of selectable items to stage frame."));
	}
	else
	{
		MaxNumSelectableListBox->SetSize(Vector2(160, 200));
		MaxNumSelectableListBox->SetPosition(Vector2(8, 264));

		MaxNumSelectableListBox->SetList(listContent);
		MaxNumSelectableListBox->SetMaxNumSelected(3);

		MaxNumSelectableListBox->OnItemSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleMaxNumSelectedBoxOptionSelected, void, INT);
		MaxNumSelectableListBox->OnItemDeselected = SDFUNCTION_1PARAM(this, GuiTester, HandleMaxNumSelectedBoxOptionDeselected, void, INT);
		StageComponents.push_back(MaxNumSelectableListBox.Get());

		LabelComponent* description = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(description))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach max num selectable items List Box's description label to stage frame."));
		}
		else
		{
			description->SetSize(Vector2(160, 16));
			description->SetPosition(MaxNumSelectableListBox->ReadPosition() - Vector2(0, 16));
			description->SetClampText(true);
			description->SetWrapText(false);
			description->SetText(DString::CreateFormattedString(TXT("Selection limit = %s"), MaxNumSelectableListBox->GetMaxNumSelected()));
			StageComponents.push_back(description);
		}
	}

	ListBoxComponent* autoDeselect = ListBoxComponent::CreateObject();
	if (!StageFrame->AddComponent(autoDeselect))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach auto deselect list box to stage frame."));
	}
	else
	{
		autoDeselect->SetSize(Vector2(160, 200));
		autoDeselect->SetPosition(Vector2(176, 264));

		autoDeselect->SetList(listContent);
		autoDeselect->SetAutoDeselect(true);

		autoDeselect->OnItemSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleAutoDeselectBoxOptionSelected, void, INT);
		autoDeselect->OnItemDeselected = SDFUNCTION_1PARAM(this, GuiTester, HandleAutoDeselectBoxOptionDeselected, void, INT);
		StageComponents.push_back(autoDeselect);

		LabelComponent* description = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(description))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach auto deselect List Box's description label to stage frame."));
		}
		else
		{
			description->SetSize(Vector2(160, 16));
			description->SetPosition(autoDeselect->ReadPosition() - Vector2(0, 16));
			description->SetClampText(true);
			description->SetWrapText(false);
			description->SetText(TXT("Auto deselects"));
			StageComponents.push_back(description);
		}
	}
}

void GuiTester::HandleDropdownButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Dropdown Components!"));
	ClearStageComponents();

	DropdownComponent* basicDropdown = DropdownComponent::CreateObject();
	if (!StageFrame->AddComponent(basicDropdown))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach basic dropdown to stage frame."));
	}
	else
	{
		basicDropdown->SetSize(Vector2(256, 128));
		basicDropdown->SetPosition(Vector2(48, 48));
		basicDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleBasicDropdownOptionClicked, void, INT);
		basicDropdown->SetNoSelectedItemText(TXT("<Nothing selected>"));

		//Only add a few options since the basic dropdown is not testing the scrollbar component
		for (UINT_TYPE i = 0; i < 4 && i < DropdownNumbers.size(); i++)
		{
			GuiDataElement<INT> newEntry;
			newEntry.Data = DropdownNumbers.at(i);
			newEntry.LabelText = TXT("Option:  ") + DropdownNumbers.at(i).ToString();
			basicDropdown->GetExpandMenu()->AddOption(newEntry);
		}

		StageComponents.push_back(basicDropdown);
	}

	DropdownComponent* fullBasicDropdown = DropdownComponent::CreateObject();
	if (!StageFrame->AddComponent(fullBasicDropdown))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach full basic dropdown to stage frame."));
	}
	else
	{
		fullBasicDropdown->SetSize(Vector2(256, 128));
		fullBasicDropdown->SetPosition(Vector2(48, 175));
		fullBasicDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleFullBasicDropdownOptionClicked, void, INT);

		for (UINT_TYPE i = 0; i < DropdownNumbers.size(); i++)
		{
			GuiDataElement<INT> newEntry;
			newEntry.Data = DropdownNumbers.at(i);
			newEntry.LabelText = TXT("Option:  ") + DropdownNumbers.at(i).ToString();
			fullBasicDropdown->GetExpandMenu()->AddOption(newEntry);
		}

		fullBasicDropdown->SetSelectedItem(4);
		StageComponents.push_back(fullBasicDropdown);
	}

	ButtonBarDropdown = DropdownComponent::CreateObject();
	if (!StageFrame->AddComponent(ButtonBarDropdown.Get()))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach button bar dropdown to stage frame."));
	}
	else
	{
		ButtonBarDropdown->SetSize(Vector2(256, 128));
		ButtonBarDropdown->SetPosition(Vector2(48, 384));
		ButtonBarDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleButtonBarDropdownOptionClicked, void, INT);

		for (UINT_TYPE i = 0; i < ButtonBar.size(); i++)
		{
			GuiDataElement<ButtonComponent*> newEntry;
			newEntry.Data = ButtonBar.at(i).Get();
			newEntry.LabelText = ButtonBar.at(i)->GetCaptionText();
			ButtonBarDropdown->GetExpandMenu()->AddOption(newEntry);
		}

		ButtonBarDropdown->SetSelectedItem(0);
		StageComponents.push_back(ButtonBarDropdown.Get());
	}

	//Testing if the dropdown shrinks in size for having very few options.
	DropdownComponent* shortDropdown = DropdownComponent::CreateObject();
	if (!StageFrame->AddComponent(shortDropdown))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach short dropdown to stage frame."));
	}
	else
	{
		shortDropdown->SetSize(Vector2(256, 128));
		shortDropdown->SetPosition(Vector2(basicDropdown->ReadPosition().X + basicDropdown->ReadSize().X + 16.f, basicDropdown->ReadPosition().Y));

		for (UINT_TYPE i = 0; i < 2; i++)
		{
			GuiDataElement<INT> newEntry;
			newEntry.Data = DropdownNumbers.at(i);
			newEntry.LabelText = TXT("Option:  ") + DropdownNumbers.at(i).ToString();
			shortDropdown->GetExpandMenu()->AddOption(newEntry);
		}

		shortDropdown->SetSelectedItem(0);
		StageComponents.push_back(shortDropdown);
	}
}

void GuiTester::HandleGuiEntityButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying GuiEntities!"));
	ClearStageComponents();

	TestMenuButtonList* buttonTest = TestMenuButtonList::CreateObject();
	CHECK(buttonTest != nullptr)
	buttonTest->SetPosition(Vector2(16, 64));
	buttonTest->SetSize(Vector2(256, 256));
	GuiEntities.push_back(buttonTest);

	TestMenuFocusInterface* focusTest = TestMenuFocusInterface::CreateObject();
	CHECK(focusTest != nullptr)
	focusTest->SetSize(Vector2(256, 256));
	focusTest->SetPosition(Vector2(256, 96));
	GuiEntities.push_back(focusTest);

	//Create UI elements that adjusts the focus
	LabelComponent* setFocusLabel = LabelComponent::CreateObject();
	if (!StageFrame->AddComponent(setFocusLabel))
	{
		UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach set focus label component to stage frame."));
	}
	else
	{
		setFocusLabel->SetSize(Vector2(75.f, 16.f));
		setFocusLabel->SetPosition(Vector2::ZeroVector);
		setFocusLabel->SetHorizontalAlignment(LabelComponent::HA_Left);
		setFocusLabel->SetVerticalAlignment(LabelComponent::VA_Top);
		setFocusLabel->SetText(TXT("Set Focus"));
		StageComponents.push_back(setFocusLabel);

		SetFocusDropdown = DropdownComponent::CreateObject();
		if (!StageFrame->AddComponent(SetFocusDropdown.Get()))
		{
			UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Failed to attach set focus dropdown component to stage frame."));
			SetFocusDropdown = nullptr;
		}
		else
		{
			SetFocusDropdown->SetSize(Vector2(256.f, 256.f));
			SetFocusDropdown->SetPosition(setFocusLabel->ReadPosition() + Vector2(setFocusLabel->ReadCachedAbsSize().X + 16.f, 0.f));
			SetFocusDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleFocusChange, void, INT);
			for (UINT_TYPE i = 0; i < GuiEntities.size(); i++)
			{
				GuiDataElement<GuiEntity*> dropdownEntry(GuiEntities.at(i).Get(), GuiEntities.at(i)->GetName());
				SetFocusDropdown->GetExpandMenu()->AddOption(dropdownEntry);
			}

			FocusedGuiEntityIdx = 0;
			SetFocusDropdown->SetSelectedItem(FocusedGuiEntityIdx);
			GuiEntities.at(FocusedGuiEntityIdx)->GainFocus();
			StageComponents.push_back(SetFocusDropdown.Get());
		}
	}
}

void GuiTester::HandleHoverButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying components with Hover Components!"));
	ClearStageComponents();

	FrameComponent* frame = FrameComponent::CreateObject();
	if (StageFrame->AddComponent(frame))
	{
		frame->SetPosition(Vector2::ZeroVector);
		frame->SetSize(Vector2(128, 128));
		frame->SetLockedFrame(false);
		StageComponents.push_back(frame);

		HoverComponent* frameHover = HoverComponent::CreateObject();
		if (frame->AddComponent(frameHover))
		{
			frameHover->OnRollOver = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverFrameRolledOver, void, MousePointer*, Entity*);
			frameHover->OnRollOut = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverFrameRolledOut, void, MousePointer*, Entity*);
		}
	}

	HoverLabel = LabelComponent::CreateObject();
	if (StageFrame->AddComponent(HoverLabel.Get()))
	{
		HoverLabel->SetSize(Vector2(128, 16));
		//HoverLabel->SnapBottom();
		//HoverLabel->SnapCenterHorizontally();
		HoverLabel->SetText(TXT("Hover over me"));
		StageComponents.push_back(HoverLabel.Get());

		HoverComponent* labelHover = HoverComponent::CreateObject();
		if (HoverLabel->AddComponent(labelHover))
		{
			labelHover->OnRollOver = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverLabelRolledOver, void, MousePointer*, Entity*);
			labelHover->OnRollOut = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverLabelRolledOut, void, MousePointer*, Entity*);
		}
	}

	HoverButton = ButtonComponent::CreateObject();
	if (StageFrame->AddComponent(HoverButton.Get()))
	{
		HoverButton->SetSize(Vector2(128, 16));
		//HoverButton->SnapRight();
		//HoverButton->SnapTop();
		HoverButton->SetCaptionText(TXT("Hover over me"));
		StageComponents.push_back(HoverButton.Get());

		HoverComponent* buttonHover = HoverComponent::CreateObject();
		if (HoverButton->AddComponent(buttonHover))
		{
			buttonHover->OnRollOver = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverButtonRolledOver, void, MousePointer*, Entity*);
			buttonHover->OnRollOut = SDFUNCTION_2PARAM(this, GuiTester, HandleHoverButtonRolledOut, void, MousePointer*, Entity*);
		}
	}
}

void GuiTester::HandleTreeListButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Tree List Components!"));
	ClearStageComponents();

	TreeListClassBrowser = TreeListComponent::CreateObject();
	if (StageFrame->AddComponent(TreeListClassBrowser.Get()))
	{
		TreeListClassBrowser->SetPosition(Vector2::ZeroVector);
		TreeListClassBrowser->SetSize(Vector2(1.f, 1.f));
		TreeListClassBrowser->OnOptionSelected = SDFUNCTION_1PARAM(this, GuiTester, HandleTreeListClassSelected, void, INT);

		//Populate tree data
		std::vector<TreeListComponent::SDataBranch*> branches;
		for (ClassIterator iter(Object::SStaticClass()); iter.SelectedClass != nullptr; iter++)
		{
			TreeListComponent::SDataBranch* curBranch = new TreeListComponent::SDataBranch(new GuiDataElement<const DClass*>(iter.SelectedClass, iter.SelectedClass->ToString()));

			//Find parent branch
			if (iter.SelectedClass->GetSuperClass() != nullptr)
			{
				for (UINT_TYPE i = 0; i < branches.size(); i++)
				{
					GuiDataElement<const DClass*>* otherBranch = dynamic_cast<GuiDataElement<const DClass*>*>(branches.at(i)->Data);
					CHECK(otherBranch != nullptr)
					if (otherBranch->Data == iter.SelectedClass->GetSuperClass())
					{
						TreeListComponent::SDataBranch::LinkBranches(*branches.at(i), *curBranch);
						break;
					}
				}
			}

			branches.push_back(curBranch);
		}

		//Swap the first 10 elements with the last 10 elements to briefly test the sorting function
		for (UINT_TYPE i = 0; i < 10; i++)
		{
			TreeListComponent::SDataBranch* otherBranch = branches.at(branches.size() - 1 - i);
			branches.at(branches.size() - 1 - i) = branches.at(i);
			branches.at(i) = otherBranch;
		}

		//If this function does not correctly sort this tree list, then the tree will not correctly display on UI
		TreeListComponent::SortDataList(branches);

		TreeListClassBrowser->SetTreeList(branches);
		TreeListClassBrowser->ExpandAll();

		//Select own class
		TreeListClassBrowser->SetSelectedItem<const DClass*>(GuiTester::SStaticClass());
		StageComponents.push_back(TreeListClassBrowser.Get());
	}
	else
	{
		TreeListClassBrowser = nullptr;
	}
}

void GuiTester::HandleCloseClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("End UI button was clicked.  Concluding UI test."));

	if (TestWindowHandle != nullptr)
	{
		TestWindowHandle->Destroy();
	}

	Destroy();
}

void GuiTester::HandleNextPageClicked (ButtonComponent* uiComponent)
{
	if (++ButtonBarPageIdx > (INT(ButtonBar.size()) / MaxButtonsPerPage))
	{
		ButtonBarPageIdx = 0;
	}

	UpdateButtonBarPageIdx();
}

void GuiTester::HandlePrevPageClicked (ButtonComponent* uiComponent)
{
	if (--ButtonBarPageIdx < 0)
	{
		ButtonBarPageIdx = INT(ButtonBar.size()) / MaxButtonsPerPage;
	}

	UpdateButtonBarPageIdx();
}

void GuiTester::HandleButtonTestPressed (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("Button \"%s\" was pressed."), uiComponent->GetCaptionText());
}

void GuiTester::HandleButtonTestReleased (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("Button \"%s\" was released."), uiComponent->GetCaptionText());
}

void GuiTester::HandleButtonTestToggleReleased (ButtonComponent* uiComponent)
{
	if (ToggleButtons[0] == uiComponent)
	{
		ToggleButtons[0]->SetEnabled(false);
		ToggleButtons[1]->SetEnabled(true);
	}
	else
	{
		ToggleButtons[0]->SetEnabled(true);
		ToggleButtons[1]->SetEnabled(false);
	}

	HandleButtonTestReleased(uiComponent);
}

void GuiTester::HandleButtonTestIncrementCounter (ButtonComponent* uiComponent)
{
	++ButtonTestCounter;
	if (uiComponent != nullptr)
	{
		uiComponent->SetCaptionText(TXT("Counter: ") + ButtonTestCounter.ToString());
	}

	HandleButtonTestReleased(uiComponent);
}

void GuiTester::HandleButtonTestDecrementCounter (ButtonComponent* uiComponent)
{
	--ButtonTestCounter;
	if (uiComponent != nullptr)
	{
		uiComponent->SetCaptionText(TXT("Counter: ") + ButtonTestCounter.ToString());
	}
}

void GuiTester::HandleCheckboxToggle (CheckboxComponent* uiComponent)
{
	if (uiComponent->GetChecked())
	{
		UnitTester::TestLog(TestFlags, TXT("The Checkbox (%s) is checked."), uiComponent->CaptionComponent->GetContent());
	}
	else
	{
		UnitTester::TestLog(TestFlags, TXT("The Checkbox (%s) is no longer checked."), uiComponent->CaptionComponent->GetContent());
	}
}

void GuiTester::HandleToggleEnableCheckboxToggle (CheckboxComponent* uiComponent)
{
	CHECK(EnableToggleCheckbox != nullptr && TargetTestCheckbox != nullptr)
	TargetTestCheckbox->SetEnabled(EnableToggleCheckbox->GetChecked());
}

void GuiTester::HandleToggleVisibilityCheckboxToggle (CheckboxComponent* uiComponent)
{
	CHECK(VisibilityToggleCheckbox != nullptr && TargetTestCheckbox != nullptr)
	TargetTestCheckbox->SetVisibility(VisibilityToggleCheckbox->GetChecked());
}

void GuiTester::HandleScrollbarTesterLabelTestClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject != nullptr && ScrollbarTester != nullptr)

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > label test!"));
	ResetScrollbarTest();

	LabelComponent* testLabel = LabelComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(testLabel))
	{
		ScrollbarTesterViewedObject->SetAutoSizeHorizontal(false);
		ScrollbarTesterViewedObject->SetAutoSizeVertical(true);
		ScrollbarTesterViewedObject->SetSize(ScrollbarTester->GetFrameSpriteTransform()->ReadCachedAbsSize());

		testLabel->SetAutoRefresh(false);
		testLabel->SetSize(Vector2(1.f, 1.f));
		testLabel->SetAutoSizeVertical(true);
		testLabel->SetCharacterSize(24);
		testLabel->SetLineSpacing(12.f);
		testLabel->SetWrapText(true);
		testLabel->SetClampText(false);
		testLabel->SetText(TXT("He was...a hero. Not the hero we deserved - the hero we needed. Nothing less than a knight, shining... \
\nThey'll have to hunt you. \
\nYou'll hunt me.  You'll condemn me.  Set the dogs on me.  Because that what needs to happen.  Because sometimes truth isn't good enough.  Sometimes people deserve more.  Sometimes people deserve their faith rewarded. \
\n \
\n \
\nWhy is he running, Dad? \
\nBecause we have to chase him. \
\nHe didn't anything wrong. \
\nBecause he's the hero Gotham deserves, but not the one it needs right now. So we'll hunt him. Because he can take it. Because he's not our hero. He's a silent guardian. A watchful protector. A Dark Knight. \
\n \
\n\t-The Dark Knight"));
		testLabel->SetAutoRefresh(true);
		ScrollbarTestObjects.push_back(testLabel);
	}
}

void GuiTester::HandleScrollbarTesterButtonTestClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject != nullptr)

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > button test!"));
	ResetScrollbarTest();

	const Vector2 buttonSize(165.f, 32.f);
	const FLOAT buttonSpacing(8.f);
	const INT numRows(12);
	const INT numCols(8);

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(false);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(false);
	ScrollbarTesterViewedObject->SetSize(((buttonSize.X + buttonSpacing) * numCols.ToFLOAT() + buttonSpacing), ((buttonSize.Y + buttonSpacing) * numRows.ToFLOAT()) + buttonSpacing);

	INT buttonIdx = 0;
	for (INT row = 0; row < numRows; row++)
	{
		for (INT col = 0; col < numCols; col++)
		{
			ButtonComponent* newButton = ButtonComponent::CreateObject();
			if (ScrollbarTesterViewedObject->AddComponent(newButton))
			{
				newButton->SetPosition((buttonSize.X * col.ToFLOAT()) + buttonSpacing * (col + 1).ToFLOAT(), (buttonSize.Y * row.ToFLOAT()) + buttonSpacing * (row + 1).ToFLOAT());
				newButton->SetSize(buttonSize);
				newButton->SetCaptionText(TXT("Scrollable Button ") + buttonIdx.ToString());
				newButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterTempButtonClicked, void, ButtonComponent*));
				ScrollbarTestObjects.push_back(newButton);
				buttonIdx++;
			}
		}
	}
}

void GuiTester::HandleScrollbarTesterTempButtonClicked (ButtonComponent* uiComponent)
{
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  \"%s\" release event invoked."), uiComponent->GetCaptionText());
}

void GuiTester::HandleScrollbarTesterSizeChangeClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject != nullptr)

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > adjustable size test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(true);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(true);

	CheckboxComponent* hideWhenFullCheckbox = CheckboxComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(hideWhenFullCheckbox))
	{
		hideWhenFullCheckbox->SetAnchorLeft(8.f);
		hideWhenFullCheckbox->SetAnchorTop(8.f);
		hideWhenFullCheckbox->SetSize(Vector2(256.f, 24.f));

		if (hideWhenFullCheckbox->CaptionComponent != nullptr)
		{
			hideWhenFullCheckbox->CaptionComponent->SetText(TXT("Hide Scrollbar When Full"));
		}

		if (ScrollbarTester != nullptr)
		{
			hideWhenFullCheckbox->SetChecked(ScrollbarTester->GetHideControlsWhenFull());
		}

		hideWhenFullCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterToggleHideWhenFull, void, CheckboxComponent*);
		ScrollbarTestObjects.push_back(hideWhenFullCheckbox);
	}

	ScrollbarTesterSizeChangeFrame = FrameComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(ScrollbarTesterSizeChangeFrame.Get()))
	{
		ScrollbarTesterSizeChangeFrame->SetAnchorLeft(8.f);
		ScrollbarTesterSizeChangeFrame->SetAnchorTop(96.f);
		ScrollbarTesterSizeChangeFrame->SetLockedFrame(true);
		ScrollbarTesterSizeChangeFrame->SetBorderThickness(4.f);
		ScrollbarTesterSizeChangeFrame->SetSize(512.f, 512.f);
		ScrollbarTesterSizeChangeFrame->SetEnableFractionScaling(false);
		ScrollbarTestObjects.push_back(ScrollbarTesterSizeChangeFrame.Get());

		TickComponent* frameTick = TickComponent::CreateObject(TICK_GROUP_GUI);
		if (ScrollbarTesterSizeChangeFrame->AddComponent(frameTick))
		{
			frameTick->SetTickHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterSizeChangerTick, void, FLOAT));
		}
	}

	ScrollbarTesterDecreaseSizeButton = ButtonComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(ScrollbarTesterDecreaseSizeButton.Get()))
	{
		ScrollbarTesterDecreaseSizeButton->SetPosition(8.f, 40.f);
		ScrollbarTesterDecreaseSizeButton->SetSize(Vector2(32.f, 32.f));
		ScrollbarTesterDecreaseSizeButton->SetCaptionText(TXT("-"));
		ScrollbarTesterDecreaseSizeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterDecreaseSizeClicked, void, ButtonComponent*));

		ScrollbarTesterDecreaseSizeButton->GetBackground()->RenderComponent->ClearSpriteTexture();
		ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(ScrollbarTesterDecreaseSizeButton->ReplaceStateComponent(ColorButtonState::SStaticClass()));
		if (colorState != nullptr)
		{
			colorState->SetDefaultColor(Color(255, 0, 0));
			colorState->SetDisabledColor(Color(sf::Color::Black));
			colorState->SetHoverColor(Color(255, 48, 48));
			colorState->SetDownColor(Color(200, 0, 0));
		}

		ScrollbarTestObjects.push_back(ScrollbarTesterDecreaseSizeButton.Get());
	}

	ScrollbarTesterIncreaseSizeButton = ButtonComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(ScrollbarTesterIncreaseSizeButton.Get()))
	{
		ScrollbarTesterIncreaseSizeButton->SetPosition(48.f, 40.f);
		ScrollbarTesterIncreaseSizeButton->SetSize(Vector2(32.f, 32.f));
		ScrollbarTesterIncreaseSizeButton->SetCaptionText(TXT("+"));
		ScrollbarTesterIncreaseSizeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterIncreaseSizeClicked, void, ButtonComponent*));

		ScrollbarTesterIncreaseSizeButton->GetBackground()->RenderComponent->ClearSpriteTexture();
		ColorButtonState* colorState = dynamic_cast<ColorButtonState*>(ScrollbarTesterIncreaseSizeButton->ReplaceStateComponent(ColorButtonState::SStaticClass()));
		if (colorState != nullptr)
		{
			colorState->SetDefaultColor(Color(255, 0, 0));
			colorState->SetDisabledColor(Color(sf::Color::Black));
			colorState->SetHoverColor(Color(255, 48, 48));
			colorState->SetDownColor(Color(200, 0, 0));
		}

		ScrollbarTestObjects.push_back(ScrollbarTesterIncreaseSizeButton.Get());
	}
}

void GuiTester::HandleScrollbarTesterToggleHideWhenFull (CheckboxComponent* uiComponent)
{
	if (ScrollbarTester != nullptr)
	{
		ScrollbarTester->SetHideControlsWhenFull(uiComponent->GetChecked());
	}
}

void GuiTester::HandleScrollbarTesterDecreaseSizeClicked (ButtonComponent* uiComponent)
{
	const FLOAT sizeDecreaseAmount(8.f);
	if (ScrollbarTesterSizeChangeFrame != nullptr)
	{
		Vector2 newSize = ScrollbarTesterSizeChangeFrame->GetSize();
		newSize.X -= sizeDecreaseAmount;
		newSize.Y -= sizeDecreaseAmount;

		newSize.X = Utils::Max(newSize.X, sizeDecreaseAmount);
		newSize.Y = Utils::Max(newSize.Y, sizeDecreaseAmount);

		ScrollbarTesterSizeChangeFrame->SetSize(newSize);
	}
}

void GuiTester::HandleScrollbarTesterIncreaseSizeClicked (ButtonComponent* uiComponent)
{
	const FLOAT sizeIncreaseAmount(8.f);
	if (ScrollbarTesterSizeChangeFrame != nullptr)
	{
		Vector2 newSize = ScrollbarTesterSizeChangeFrame->GetSize();
		newSize.X += sizeIncreaseAmount;
		newSize.Y += sizeIncreaseAmount;

		ScrollbarTesterSizeChangeFrame->SetSize(newSize);
	}
}

void GuiTester::HandleScrollbarTesterSizeChangerTick (FLOAT deltaSec)
{
	if (ScrollbarTester == nullptr || ScrollbarTester->GetFrameCamera() == nullptr)
	{
		return;
	}

	const Vector2 frameTopLeftCorner = (ScrollbarTester->GetFrameCamera()->ReadPosition() - (ScrollbarTester->GetFrameCamera()->ReadViewExtents() * 0.5f));

	//Reposition the buttons to be attach to the top left corner of camera
	if (ScrollbarTesterDecreaseSizeButton != nullptr)
	{
		ScrollbarTesterDecreaseSizeButton->SetPosition(Vector2(8.f, 40.f) + frameTopLeftCorner);
	}

	if (ScrollbarTesterIncreaseSizeButton != nullptr)
	{
		ScrollbarTesterIncreaseSizeButton->SetPosition(Vector2(48.f, 40.f) + frameTopLeftCorner);
	}
}

void GuiTester::HandleScrollbarTesterDifferentFrameSizeClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject != nullptr && ScrollbarTester != nullptr && ScrollbarTester->GetFrameCamera() != nullptr)

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > Different Frame Size test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(true);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(true);
	ScrollbarTesterViewedObject->SetSize(ScrollbarTester->GetFrameCamera()->ReadViewExtents());

	ButtonComponent* toggleHorizontal = ButtonComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(toggleHorizontal))
	{
		toggleHorizontal->SetAnchorTop(8.f);
		toggleHorizontal->SetAnchorLeft(8.f);
		toggleHorizontal->SetSize(Vector2(128.f, 48.f));
		toggleHorizontal->SetCaptionText(TXT("Toggle Horizontal Size"));
		toggleHorizontal->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterToggleHalfHorizontalButtonClicked, void, ButtonComponent*));
		ScrollbarTestObjects.push_back(toggleHorizontal);
	}

	ButtonComponent* toggleVertical = ButtonComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(toggleVertical))
	{
		CHECK(toggleHorizontal != nullptr)
		toggleVertical->SetAnchorTop(8.f);
		toggleVertical->SetAnchorLeft(16.f + toggleHorizontal->ReadSize().X);
		toggleVertical->SetSize(Vector2(128.f, 48.f));
		toggleVertical->SetCaptionText(TXT("Toggle Vertical Size"));
		toggleVertical->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterToggleHalfVertialButtonClicked, void, ButtonComponent*));
		ScrollbarTestObjects.push_back(toggleVertical);
	}

	LabelComponent* label = LabelComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(label))
	{
		label->SetAutoRefresh(false);
		label->SetSize(ScrollbarTester->GetFrameSpriteTransform()->ReadCachedAbsSize());
		label->SetAutoSizeHorizontal(false);
		label->SetAutoSizeVertical(true);
		label->SetPosition(Vector2(0.f, 64.f));
		label->SetWrapText(true);
		label->SetClampText(false);
		label->SetText(TXT("Look at the state we're in \n \
This docile apathy \n \
We fight and stumble aimlessly \n \
\n \
We broke our promises \n \
One too many times \n \
\n \
When hope is a ghost \n \
Fear survive \n \
\n \
I'm just gonna sit here \n \
I'm just gonna stay here for awhile. \n \
Need to catch my breath here \n \
What do we do now \n \
\n \
Look at the empty stares \n \
of once mighty men \n \
Fallen and fragile \n \
so unsure \n \
\n \
Who do we look to now \n \
How can we trust again \n \
When hope is a ghost \n \
Fear survives \n \
\n \
I'm just gonna sit here \n \
I'm just gonna stay here for awhile \n \
Need to catch my breath here \n \
What do we do now \n \
\n \
\n \
Hope Is a Ghost - Angelflare\n "));
		label->SetAutoRefresh(true);
		ScrollbarTestObjects.push_back(label);
	}

	//Clear anchors so its size may be adjusted
	ScrollbarTester->SetAnchorTop(-1.f);
	ScrollbarTester->SetAnchorRight(-1.f);
	ScrollbarTester->SetAnchorBottom(-1.f);
	ScrollbarTester->SetSize(0.75f, 0.8f);
	ScrollbarTester->SetHideControlsWhenFull(true);
}

void GuiTester::HandleScrollbarTesterToggleHalfHorizontalButtonClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTester != nullptr)

	Vector2 newSize(ScrollbarTester->ReadSize());
	newSize.X = (ScrollbarTester->ReadSize().X >= 0.75f) ? 0.375f : 0.75f;

	ScrollbarTester->SetSize(newSize);
}

void GuiTester::HandleScrollbarTesterToggleHalfVertialButtonClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTester != nullptr)

	Vector2 newSize(ScrollbarTester->ReadSize());
	newSize.Y = (ScrollbarTester->ReadSize().Y >= 0.8f) ? 0.4f : 0.8f;

	ScrollbarTester->SetSize(newSize);
}

void GuiTester::HandleScrollbarTesterOuterSizeChangeClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject != nullptr && ScrollbarTester != nullptr && ScrollbarTester->GetFrameCamera() != nullptr)

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > outer size change test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(false);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(false);
	ScrollbarTesterViewedObject->SetSize(ScrollbarTester->GetFrameCamera()->ReadViewExtents());

	FrameComponent* lockedFrame = FrameComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(lockedFrame))
	{
		lockedFrame->SetPosition(Vector2::ZeroVector);
		lockedFrame->SetSize(Vector2(1.f, 1.f));
		lockedFrame->SetBorderThickness(1.f);
		lockedFrame->SetLockedFrame(true);
		ScrollbarTestObjects.push_back(lockedFrame);

		FrameComponent* adjustableFrame = FrameComponent::CreateObject();
		if (lockedFrame->AddComponent(adjustableFrame))
		{
			adjustableFrame->SetPosition(Vector2(0.25f, 0.25f));
			adjustableFrame->SetSize(Vector2(0.5f, 0.5f));
			adjustableFrame->SetBorderThickness(6.f);
			adjustableFrame->SetMinDragSize(Vector2(48.f, 48.f));
			adjustableFrame->SetLockedFrame(false);

			ScrollbarComponent* innerScrollbar = ScrollbarComponent::CreateObject();
			if (adjustableFrame->AddComponent(innerScrollbar))
			{
				innerScrollbar->SetAnchorTop(adjustableFrame->GetBorderThickness());
				innerScrollbar->SetAnchorRight(adjustableFrame->GetBorderThickness());
				innerScrollbar->SetAnchorBottom(adjustableFrame->GetBorderThickness());
				innerScrollbar->SetAnchorLeft(adjustableFrame->GetBorderThickness());

				GuiEntity* innerEntity = GuiEntity::CreateObject();
				innerEntity->SetAutoSizeHorizontal(false);
				innerEntity->SetAutoSizeHorizontal(false);
				innerEntity->SetPosition(Vector2::ZeroVector);
				innerEntity->SetSize(1024.f, 1024.f);
				innerScrollbar->SetViewedObject(innerEntity);

				PlanarTransformComponent* innerSpriteTransform = PlanarTransformComponent::CreateObject();
				if (innerEntity->AddComponent(innerSpriteTransform))
				{
					innerSpriteTransform->SetPosition(Vector2::ZeroVector);
					innerSpriteTransform->SetSize(Vector2(1.f, 1.f));

					SpriteComponent* innerSprite = SpriteComponent::CreateObject();
					if (innerSpriteTransform->AddComponent(innerSprite))
					{
						TexturePool* localTexturePool = TexturePool::FindTexturePool();
						CHECK(localTexturePool != nullptr)

						innerSprite->SetSpriteTexture(localTexturePool->FindTexture(TXT("Interface.DefaultFrameComponentFill")));
					}
				}
			}
		}
	}
}

void GuiTester::HandleScrollbarTesterNestedScrollbarsClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject != nullptr && ScrollbarTester != nullptr && ScrollbarTester->GetFrameCamera() != nullptr)

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > nested scrollbar test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(false);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(false);
	ScrollbarTesterViewedObject->SetSize(ScrollbarTester->GetFrameCamera()->GetViewExtents() * 1.25f);

	ScrollbarComponent* leftScrollbar = ScrollbarComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(leftScrollbar))
	{
		leftScrollbar->SetAnchorTop(32.f);
		leftScrollbar->SetAnchorLeft(32.f);
		leftScrollbar->SetAnchorBottom(32.f);
		leftScrollbar->SetSize(0.4f, 1.f);
		ScrollbarTestObjects.push_back(leftScrollbar);

		GuiEntity* leftViewedObject = GuiEntity::CreateObject();
		leftViewedObject->SetAutoSizeHorizontal(true);
		leftViewedObject->SetAutoSizeVertical(true);
		leftScrollbar->SetViewedObject(leftViewedObject);
		
		LabelComponent* leftLabel = LabelComponent::CreateObject();
		if (leftViewedObject->AddComponent(leftLabel))
		{
			leftLabel->SetAutoRefresh(false);
			leftLabel->SetAutoSizeHorizontal(true);
			leftLabel->SetAutoSizeVertical(true);
			leftLabel->SetLineSpacing(6.f);
			leftLabel->SetHorizontalAlignment(LabelComponent::HA_Center);
			
			DString labelText = TXT("This is a long line to test horizontal scrolling.  The Scrollbar on the right is testing input redirection to see even if a nested Scrollbar can relay input to its ViewedObject in the correct coordinate space.");
			for (INT i = 0; i < 32; ++i)
			{
				labelText += TXT("\nLine ") + i.ToString();
			}

			leftLabel->SetText(labelText);
			leftLabel->SetAutoRefresh(true);
		}
	}

	ScrollbarComponent* rightScrollbar = ScrollbarComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(rightScrollbar))
	{
		rightScrollbar->SetAnchorTop(32.f);
		rightScrollbar->SetAnchorRight(32.f);
		rightScrollbar->SetAnchorBottom(32.f);
		rightScrollbar->SetSize(0.4f, 1.f);
		ScrollbarTestObjects.push_back(rightScrollbar);

		GuiEntity* rightViewedObject = GuiEntity::CreateObject();
		rightViewedObject->SetAutoSizeHorizontal(false);
		rightViewedObject->SetAutoSizeVertical(true);
		rightViewedObject->SetSize(108.f, 1.f);
		rightScrollbar->SetViewedObject(rightViewedObject);

		std::vector<DString> foodList({TXT("Spaghetti"), TXT("Pizza"), TXT("Cereal"), TXT("Taco"), TXT("Hamburger"),
			TXT("Hotdog"), TXT("Bacon"), TXT("Cheese"), TXT("Potato"), TXT("Carrot"), TXT("Salad"), TXT("Bread"),
			TXT("Chili Pepper"), TXT("Omnomberry")});
		const Vector2 checkboxSize(150.f, 32.f);
		const FLOAT checkboxSpacing(12.f);

		for (UINT_TYPE i = 0; i < foodList.size(); ++i)
		{
			CheckboxComponent* subCheckbox = CheckboxComponent::CreateObject();
			if (rightViewedObject->AddComponent(subCheckbox))
			{
				subCheckbox->SetPosition(checkboxSpacing, (checkboxSpacing * FLOAT::MakeFloat(i+1)) + (checkboxSize.Y * FLOAT::MakeFloat(i)));
				subCheckbox->SetSize(checkboxSize);
				
				if (subCheckbox->CaptionComponent != nullptr)
				{
					subCheckbox->CaptionComponent->SetText(foodList.at(i));
				}

				subCheckbox->OnChecked = SDFUNCTION_1PARAM(this, GuiTester, HandleScrollbarTesterToggleFoodItem, void, CheckboxComponent*);
			}
		}
	}
}

void GuiTester::HandleScrollbarTesterToggleFoodItem (CheckboxComponent* uiComponent)
{
	if (uiComponent == nullptr || uiComponent->CaptionComponent == nullptr)
	{
		UnitTestLog.Log(LogCategory::LL_Warning, TXT("A Checkbox is checked in nested scrollbar test.  However, the callback's uiComponent is either null, or its caption component is null."));
		return;
	}

	DString checkedSuffix = (uiComponent->GetChecked()) ? TXT("now checked.") : TXT("no longer checked.");
	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  %s is %s"), uiComponent->CaptionComponent->GetContent(), checkedSuffix);
}

void GuiTester::HandleScrollbarTesterComplexUiClicked (ButtonComponent* uiComponent)
{
	CHECK(ScrollbarTesterViewedObject != nullptr)

	UnitTester::TestLog(TestFlags, TXT("[GuiTester]:  Displaying Scrollbar test > complex UI test!"));
	ResetScrollbarTest();

	ScrollbarTesterViewedObject->SetAutoSizeHorizontal(true);
	ScrollbarTesterViewedObject->SetAutoSizeVertical(true);

	FLOAT posY = 16.f;
	const FLOAT uiPadding = 16.f;
	LabelComponent* title = LabelComponent::CreateObject();
	if (ScrollbarTesterViewedObject->AddComponent(title))
	{
		title->SetAutoRefresh(false);
		title->SetPosition(Vector2(posY, posY));
		title->SetSize(Vector2(512.f, 48.f));
		title->SetCharacterSize(48);
		title->SetClampText(true);
		title->SetWrapText(false);
		title->SetText(TXT("Complicated UI Test"));
		title->SetAutoRefresh(true);
		ScrollbarTestObjects.push_back(title);

		posY += title->ReadSize().Y + uiPadding;
	}

	const std::vector<DString> labelTexts(
	{
		TXT("This test displays the scrollbar rendering a GuiEntity that contains numerous UI components of various types such as labels, buttons, nested components, and other scrollbars."),
		TXT("These UI components repeat a few times to test UI components in various scrolling positions."),
		TXT("Some tips regarding scrollbar controls.\nHolding the ctrl, shift, and alt keys multiplies the scrolling speed.\nYou can scroll the scrollbar by clicking on the arrow buttons on the sides.\nYou can also scroll it by dragging the thumb, or clicking on the tracks between the buttons.\nYou can also use the mouse wheel to scroll of course.\nLastly, you can also scroll the scrollbar using the middle mouse button.")
	});
	const UINT_TYPE numRepeats = labelTexts.size();

	for (UINT_TYPE i = 0; i < numRepeats; ++i)
	{
		LabelComponent* description = LabelComponent::CreateObject();
		if (ScrollbarTesterViewedObject->AddComponent(description))
		{
			description->SetAutoRefresh(false);
			description->SetPosition(Vector2(0.f, posY));
			description->SetSize(256.f, 1.f);
			description->SetAutoSizeVertical(true);
			description->SetWrapText(true);
			description->SetClampText(false);
			description->SetText(labelTexts.at(i));
			description->SetAutoRefresh(true);
			ScrollbarTestObjects.push_back(description);

			posY += description->ReadSize().Y + uiPadding;
		}

		ButtonComponent* testButton = ButtonComponent::CreateObject();
		if (ScrollbarTesterViewedObject->AddComponent(testButton))
		{
			testButton->SetPosition(Vector2(8.f, posY));
			testButton->SetSize(Vector2(96.f, 32.f));
			testButton->SetCaptionText(TXT("Button that does nothing"));
			ScrollbarTestObjects.push_back(testButton);

			posY += testButton->ReadSize().Y + uiPadding;
		}

		FrameComponent* testFrame = FrameComponent::CreateObject();
		if (ScrollbarTesterViewedObject->AddComponent(testFrame))
		{
			testFrame->SetPosition(0.f, posY);
			testFrame->SetSize(768.f, 256.f);
			testFrame->SetLockedFrame(true);
			testFrame->SetBorderThickness(2.f);
			ScrollbarTestObjects.push_back(testFrame);

			posY += testFrame->ReadSize().Y + uiPadding;

			FrameComponent* adjustableFrame = FrameComponent::CreateObject();
			if (testFrame->AddComponent(adjustableFrame))
			{
				adjustableFrame->SetPosition(Vector2::ZeroVector);
				adjustableFrame->SetSize(testFrame->ReadSize() * 0.5f);
				adjustableFrame->SetBorderThickness(6.f);
				adjustableFrame->SetLockedFrame(false);
				if (adjustableFrame->RenderComponent != nullptr)
				{
					adjustableFrame->RenderComponent->ClearSpriteTexture();
					adjustableFrame->RenderComponent->FillColor = Color(25, 50, 0);
				}

				ButtonComponent* innerButton = ButtonComponent::CreateObject();
				if (adjustableFrame->AddComponent(innerButton))
				{
					innerButton->SetPosition(0.4f, 0.4f);
					innerButton->SetSize(0.2f, 0.2f);
					innerButton->SetCaptionText("Inner Button");
					if (innerButton->GetRenderComponent() != nullptr)
					{
						innerButton->GetRenderComponent()->ClearSpriteTexture();
					}
					ColorButtonState* buttonState = dynamic_cast<ColorButtonState*>(innerButton->ReplaceStateComponent(ColorButtonState::SStaticClass()));
					if (buttonState != nullptr)
					{
						buttonState->SetDefaultColor(Color(100, 50, 50));
						buttonState->SetHoverColor(Color(50, 100, 50));
						buttonState->SetDownColor(Color(50, 50, 100));
					}
				}
			}
		}

		ScrollbarComponent* scrollbarLabel = ScrollbarComponent::CreateObject();
		if (ScrollbarTesterViewedObject->AddComponent(scrollbarLabel))
		{
			scrollbarLabel->SetPosition(0.f, posY);
			scrollbarLabel->SetSize(256.f, 256.f);
			scrollbarLabel->SetHideControlsWhenFull(true);

			GuiEntity* scrollbarLabelViewedObject = nullptr;
			LabelComponent* scrollbarLabelContent = nullptr;
			LabelComponent::CreateLabelWithinScrollbar(scrollbarLabel, OUT scrollbarLabelViewedObject, OUT scrollbarLabelContent);
			CHECK(scrollbarLabelViewedObject != nullptr && scrollbarLabelContent != nullptr)

			scrollbarLabelContent->SetAutoRefresh(false);
			scrollbarLabelContent->SetText(TXT("Line 1\nLine 2\nLine 3\nLine 4\nLine 5\nLine 6\nLine 7\nLine 8\nLine 9\nLine 10\nLine 11\nLine 12\nLine 13\nLine 14\nLine 15\nLine 16"));
			scrollbarLabelContent->SetLineSpacing(4.f);
			scrollbarLabelContent->SetCharacterSize(16);
			scrollbarLabelContent->SetAutoRefresh(true);

			posY += scrollbarLabel->ReadSize().Y + uiPadding;
			ScrollbarTestObjects.push_back(scrollbarLabel);
		}
	}
}

void GuiTester::HandleBasicListBoxOptionSelected (INT selectedIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Selected new item from basic List Box component:  %s"), ListBoxStrings.at(selectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleBasicListBoxOptionDeselected (INT deselectedIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Deselected item from basic List Box component:  %s"), ListBoxStrings.at(deselectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleReadOnlyBoxOptionSelected (INT selectedIdx)
{
	Engine::FindEngine()->FatalError(TXT("GuiTester:  List Box component test failed.  User was still able to select an item from a read only List Box."));
	Destroy();
}

void GuiTester::HandleReadOnlyBoxOptionDeselected (INT deselectedIdx)
{
	Engine::FindEngine()->FatalError(TXT("GuiTester:  List Box component test failed.  User was still able to deselect an item from a read only List Box."));
	Destroy();
}

void GuiTester::HandleMaxNumSelectedBoxOptionSelected (INT selectedIdx)
{
	CHECK(MaxNumSelectableListBox != nullptr)
	UnitTester::TestLog(TestFlags, TXT("Selected item from List Box with max selection limit:  %s.  Total number of selected items:  %s"), ListBoxStrings.at(selectedIdx.ToUnsignedInt()), INT(MaxNumSelectableListBox->ReadSelectedItems().size()));

	if (MaxNumSelectableListBox->ReadSelectedItems().size() > MaxNumSelectableListBox->GetMaxNumSelected())
	{
		Engine::FindEngine()->FatalError(TXT("GuiTester:  User was able to select more items than the List Box's item selection limit (") + MaxNumSelectableListBox->GetMaxNumSelected().ToString() + TXT(")."));
		Destroy();
	}
}

void GuiTester::HandleMaxNumSelectedBoxOptionDeselected (INT deselectedIdx)
{
	CHECK(MaxNumSelectableListBox != nullptr)
	UnitTester::TestLog(TestFlags, TXT("Deselected item from List Box with max selection limit:  %s."), ListBoxStrings.at(deselectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleAutoDeselectBoxOptionSelected (INT selectedIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Auto deselecting List Box selected new item:  %s."), ListBoxStrings.at(selectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleAutoDeselectBoxOptionDeselected (INT deselectedIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Auto deselecting List Box deselected item:  %s."), ListBoxStrings.at(deselectedIdx.ToUnsignedInt()));
}

void GuiTester::HandleBasicDropdownOptionClicked (INT newOptionIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Basic dropdown component selected new option index:  %s"), newOptionIdx);
}

void GuiTester::HandleFullBasicDropdownOptionClicked (INT newOptionIdx)
{
	UnitTester::TestLog(TestFlags, TXT("Full basic dropdown component selected new option index:  %s"), newOptionIdx);
}

void GuiTester::HandleButtonBarDropdownOptionClicked (INT newOptionIdx)
{
	CHECK(ButtonBarDropdown != nullptr)

	GuiDataElement<ButtonComponent*>* selectedData = ButtonBarDropdown->GetSelectedGuiDataElement<ButtonComponent*>();
	ButtonComponent* selectedButton = selectedData->Data;
	if (selectedButton == nullptr)
	{
		Engine::FindEngine()->FatalError(TXT("GuiTester:  Dropdown component test failed.  Failed to retrieve button component instance after selecting a new item."));
		Destroy();
		return;
	}

	UnitTester::TestLog(TestFlags, TXT("Button bar dropdown component selected a button component (%s) with text caption equal to \"%s\"."), selectedButton->ToString(), selectedButton->GetCaptionText());
}

void GuiTester::HandleFocusChange (INT newOptionIdx)
{
	CHECK(SetFocusDropdown != nullptr)

	if (FocusedGuiEntityIdx != UINT_INDEX_NONE)
	{
		//Deselect the previous GuiEntity
		GuiEntities.at(FocusedGuiEntityIdx)->LoseFocus();
	}

	GuiDataElement<GuiEntity*>* selectedData = SetFocusDropdown->GetSelectedGuiDataElement<GuiEntity*>();
	GuiEntity* selectedGui = selectedData->Data;
	if (selectedGui == nullptr)
	{
		UnitTestLog.Log(LogCategory::LL_Warning, TXT("Failed to select GuiEntity from Set Focus dropdown component."));
		return;
	}

	selectedGui->GainFocus();
}

void GuiTester::HandleHoverFrameRolledOver (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Frame hover component rolled over."));
}

void GuiTester::HandleHoverFrameRolledOut (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Frame hover component rolled out."));
}

void GuiTester::HandleHoverLabelRolledOver (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Label hover component rolled over."));
	HoverLabel->SetText(TXT("Rolled Over"));
}

void GuiTester::HandleHoverLabelRolledOut (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Label hover component rolled out."));
	HoverLabel->SetText(TXT("Rolled Out"));
}

void GuiTester::HandleHoverButtonRolledOver (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Button hover component rolled over."));
	HoverButton->SetCaptionText(TXT("Rolled Over"));
}

void GuiTester::HandleHoverButtonRolledOut (MousePointer* mouse, Entity* hoverOwner)
{
	UnitTester::TestLog(TestFlags, TXT("Button hover component rolled out."));
	HoverButton->SetCaptionText(TXT("Rolled Out"));
}

void GuiTester::HandleTreeListClassSelected (INT newOptionIdx)
{
	CHECK(TreeListClassBrowser != nullptr)

	GuiDataElement<const DClass*>* selectedItem = TreeListClassBrowser->GetSelectedDataElement<const DClass*>();
	if (selectedItem != nullptr && selectedItem->Data != nullptr)
	{
		UnitTester::TestLog(TestFlags, TXT("Selected class:  %s"), selectedItem->Data->ToString());
	}
	else
	{
		UnitTester::TestLog(TestFlags, TXT("Cleared class selection."));
	}
}
SD_END

#endif