/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ListBoxComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, ListBoxComponent, SD, GuiComponent)

void ListBoxComponent::InitProps ()
{
	Super::InitProps();

	MaxNumSelected = 0;
	bAutoDeselect = false;
	bReadOnly = false;
	BrowseIndex = INT_INDEX_NONE;
	SelectionColor = Color(0x666666);
	HoverColor = Color(0xbdb9b6);

	Background = nullptr;
	Scrollbar = nullptr;
	ItemListText = nullptr;
	HighlightHoverRender = nullptr;
	HighlightOwner = nullptr;

	bNeedColorTransformCalc = false;
}

void ListBoxComponent::BeginObject ()
{
	Super::BeginObject();

	RefreshComponentTransforms();
}

void ListBoxComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ListBoxComponent* listTemplate = dynamic_cast<const ListBoxComponent*>(objTemplate);
	if (listTemplate != nullptr)
	{
		SetMaxNumSelected(listTemplate->GetMaxNumSelected());
		SetAutoDeselect(listTemplate->GetAutoDeselect());
		SetReadOnly(listTemplate->IsReadOnly());
		SetSelectionColor(listTemplate->GetSelectionColor());
		SetHoverColor(listTemplate->GetHoverColor());

		bool bCreatedObj;
		Background = ReplaceTargetWithObjOfMatchingClass(Background.Get(), listTemplate->GetBackground(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(Background.Get());
		}

		if (Background != nullptr)
		{
			Background->CopyPropertiesFrom(listTemplate->GetBackground());
		}

		Scrollbar = ReplaceTargetWithObjOfMatchingClass(Scrollbar.Get(), listTemplate->GetScrollbar(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			if (AddComponent(Scrollbar.Get()))
			{
				Scrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, ListBoxComponent, HandleScrollPositionChanged, void, INT));
				Scrollbar->OnToggleVisibility = SDFUNCTION(this, ListBoxComponent, HandleScrollbarToggleVisibility, void);
			}
		}

		if (Scrollbar != nullptr)
		{
			Scrollbar->CopyPropertiesFrom(listTemplate->GetScrollbar());
		}

		ItemListText = ReplaceTargetWithObjOfMatchingClass(ItemListText.Get(), listTemplate->GetItemListText(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(ItemListText.Get());
		}

		if (ItemListText != nullptr)
		{
			ItemListText->CopyPropertiesFrom(listTemplate->GetItemListText());
		}
	}
}

bool ListBoxComponent::CanBeFocused () const
{
	return (!bReadOnly && IsVisible());
}

void ListBoxComponent::GainFocus ()
{
	FocusInterface::GainFocus();

	SetBrowseIndex(0);
}

void ListBoxComponent::LoseFocus ()
{
	FocusInterface::LoseFocus();

	SetBrowseIndex(-1);
}

bool ListBoxComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	CHECK(!bReadOnly) //Shouldn't gain focus if this component is read only

	/**
	 * Controls:
	 * Up/Down arrows moves browse index between 0 - [Size()-1]
	 * Enter toggles selection.
	 */
	if (keyEvent.type != sf::Event::KeyReleased)
	{
		return true;
	}

	switch(keyEvent.key.code)
	{
		case(sf::Keyboard::Up):
			SetBrowseIndex(Utils::Max<INT>(BrowseIndex - 1, 0));
			break;

		case(sf::Keyboard::Down):
			SetBrowseIndex(Utils::Min<INT>(BrowseIndex + 1, List.size() - 1));
			break;

		case(sf::Keyboard::Return):
			SetItemSelectionByIdx(BrowseIndex, !IsItemSelectedByIndex(BrowseIndex));
			break;
	}

	return true;
}

bool ListBoxComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	return true;
}

void ListBoxComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	InitializeBackground();
	InitializeScrollbar();
	InitializeItemListText();
	InitializeHighlightBar();
}

void ListBoxComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (bReadOnly || !IsVisible() || HighlightOwner == nullptr)
	{
		return;
	}

	INT lineIdx = GetLineIdxFromCoordinates(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
	if (lineIdx == INT_INDEX_NONE)
	{
		SetBrowseIndex(INT_INDEX_NONE);
		return;
	}

	if (Scrollbar != nullptr && !Scrollbar->IsIndexVisible(lineIdx))
	{
		//The hovered item is beyond scroll position (ie:  small space between last item and item beyond scroll pos).
		SetBrowseIndex(INT_INDEX_NONE);
		return;
	}

	if (BrowseIndex != lineIdx)
	{
		bNeedColorTransformCalc = true;
		SetBrowseIndex(lineIdx);
	}
}

bool ListBoxComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (bReadOnly || !IsVisible() || !IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))))
	{
		return false;
	}

	if (eventType == sf::Event::MouseButtonReleased)
	{
		INT lineIdx = GetLineIdxFromCoordinates(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
		if (lineIdx == INT_INDEX_NONE)
		{
			return true;
		}

		//Toggle item selection
		SetItemSelectionByIdx(lineIdx, !IsItemSelectedByIndex(lineIdx));
	}

	return true;
}

void ListBoxComponent::HandleSizeChange ()
{
	Super::HandleSizeChange();

	RefreshComponentTransforms();
}

void ListBoxComponent::Destroyed ()
{
	ClearList();

	Super::Destroyed();
}

bool ListBoxComponent::IsItemSelectedByIndex (INT index) const
{
	for (UINT_TYPE i = 0; i < SelectedItems.size(); i++)
	{
		if (SelectedItems.at(i) == index)
		{
			return true;
		}
	}

	return false;
}

void ListBoxComponent::ClearSelection ()
{
	if (SelectedItems.size() > 0)
	{
		if (OnItemDeselected.IsBounded())
		{
			for (INT selectedItem : SelectedItems)
			{
				OnItemDeselected(selectedItem);
			}
		}

		ContainerUtils::Empty(SelectedItems);
		CalculateColorTransforms();
	}
}

void ListBoxComponent::SelectAll ()
{
	if (bAutoDeselect)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot select all items on a ListBoxComponent (%s) that automatically deselect items upon selection."), ToString());
		return;
	}

	if (MaxNumSelected > 0)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot select all items on a ListBoxComponent (%s) with a max number of selected items constraint."), ToString());
		return;
	}

	if (SelectedItems.size() == List.size())
	{
		return; //Everything is already selected
	}

	for (UINT_TYPE i = 0; i < List.size(); i++)
	{
		//SelectedItems must be a sorted list
		if (i >= SelectedItems.size() || SelectedItems.at(i) != i)
		{
			//Found missing item
			SelectedItems.insert(SelectedItems.begin() + i, INT(i));

			if (OnItemSelected.IsBounded())
			{
				OnItemSelected(INT(i));
			}
		}
	}

	//Sanity check - make sure there aren't any duplicated values
	if (SelectedItems.size() > List.size())
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Error detected when selecting all items in ListBoxComponent.  There are more selected items than there are options.  This may cause rendering issues."));
		GuiLog.Log(LogCategory::LL_Warning, TXT("The following values are marked selected more than once..."));
		for (UINT_TYPE i = 0; i < List.size(); i++)
		{
			UINT_TYPE numItems = ContainerUtils::CountNumItems(SelectedItems, INT(i));
			if (numItems > 1)
			{
				GuiLog.Log(LogCategory::LL_Warning, TXT("    %s"), INT(i));
			}
		}
	}

	CalculateColorTransforms();
}

void ListBoxComponent::SelectItemsByIdx (const std::vector<INT>& itemIndices)
{
	if (bAutoDeselect)
	{
		ClearSelection();
	}

	for (UINT_TYPE i = 0; i < itemIndices.size(); i++)
	{
		if (MaxNumSelected > 0 && SelectedItems.size() >= MaxNumSelected)
		{
			break;
		}

		if (itemIndices.at(i) < 0 || itemIndices.at(i) >= List.size())
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to select item by index (%s) since that index is beyond the limits of the list.  The List's length is %s"), itemIndices.at(i), INT(List.size()));
			continue;
		}

		UINT_TYPE itemIdx = ContainerUtils::FindInVector(SelectedItems, itemIndices.at(i));
		if (itemIdx == UINT_INDEX_NONE)
		{
			SelectedItems.push_back(itemIndices.at(i));
			if (OnItemSelected.IsBounded())
			{
				OnItemSelected(itemIndices.at(i));
			}
		}
	}

	//Ensure the SelectedItems vector is sorted in ascending order to make it easier to group color components together.
	sort(SelectedItems.begin(), SelectedItems.end());
	CalculateColorTransforms();
}

void ListBoxComponent::SelectItemsByIdx (INT selectedItemIdx)
{
	bool bItemAlreadySelected = false;

	for (INT oldSelectedItem : SelectedItems)
	{
		if (oldSelectedItem != selectedItemIdx)
		{
			if (OnItemDeselected.IsBounded())
			{
				OnItemDeselected(oldSelectedItem);
			}
		}
		else
		{
			bItemAlreadySelected = true;
		}
	}

	ContainerUtils::Empty(SelectedItems);
	SelectedItems.push_back(selectedItemIdx);

	if (!bItemAlreadySelected && OnItemSelected.IsBounded())
	{
		OnItemSelected(selectedItemIdx);
	}

	CalculateColorTransforms();
}

void ListBoxComponent::SetItemSelectionByIdx (INT itemIdx, bool bSelected)
{
	bool bItemVisible = (Scrollbar == nullptr || Scrollbar->IsIndexVisible(itemIdx));

	UINT_TYPE selectedItemIdx = ContainerUtils::FindInVector(SelectedItems, itemIdx);
	if (selectedItemIdx != UINT_INDEX_NONE && !bSelected)
	{
		if (OnItemDeselected.IsBounded())
		{
			OnItemDeselected(itemIdx);
		}

		SelectedItems.erase(SelectedItems.begin() + selectedItemIdx);

		if (bItemVisible)
		{
			CalculateColorTransforms();
		}
	}
	else if (selectedItemIdx == UINT_INDEX_NONE && bSelected)
	{
		if (bAutoDeselect)
		{
			ClearSelection();
		}

		if (MaxNumSelected <= 0 || SelectedItems.size() < MaxNumSelected)
		{
			//Find where in the vector the SelectedItem should be inserted
			bool bInserted = false;
			for (UINT_TYPE i = 0; i < SelectedItems.size(); i++)
			{
				if (SelectedItems.at(i) >= itemIdx)
				{
					SelectedItems.insert(SelectedItems.begin() + i, itemIdx);
					bInserted = true;
					break;
				}
			}

			if (!bInserted)
			{
				SelectedItems.push_back(itemIdx);
			}

			if (bItemVisible)
			{
				CalculateColorTransforms();
			}

			if (OnItemSelected.IsBounded())
			{
				OnItemSelected(itemIdx);
			}
		}
	}
}

void ListBoxComponent::SetBrowseIndex (INT newBrowseIndex)
{
	BrowseIndex = newBrowseIndex;
	if (BrowseIndex != INT_INDEX_NONE && Scrollbar != nullptr && !Scrollbar->IsIndexVisible(BrowseIndex))
	{
		if (BrowseIndex < Scrollbar->GetScrollPosition())
		{
			Scrollbar->SetScrollPosition(BrowseIndex - Scrollbar->ScrollJumpInterval);
		}
		else
		{
			Scrollbar->SetScrollPosition(BrowseIndex + Scrollbar->ScrollJumpInterval - Scrollbar->GetNumVisibleScrollPositions());
		}
	}

	//Don't need to recalculate color transforms if the scrollbar changed scroll position.
	if (bNeedColorTransformCalc)
	{
		CalculateColorTransforms();
	}

	RenderHighlightHoverOverBrowsedItem();
}

void ListBoxComponent::SetList (const std::vector<BaseGuiDataElement*>& newList)
{
	//Same as ClearSelection without calculating color transforms
	if (OnItemDeselected.IsBounded())
	{
		for (INT selectedItemIdx : SelectedItems)
		{
			OnItemDeselected(selectedItemIdx);
		}
	}

	ContainerUtils::Empty(SelectedItems);
	List = newList;

	//Adjusting the scrollbar will update the text and color transforms.
	Scrollbar->SetMaxScrollPosition(List.size());
	Scrollbar->SetScrollPosition(0);
}

void ListBoxComponent::SetMaxNumSelected (INT newMaxNumSelected)
{
	MaxNumSelected = newMaxNumSelected;
	if (MaxNumSelected > 0 && MaxNumSelected < SelectedItems.size())
	{
		ClearSelection();
	}
}

void ListBoxComponent::SetAutoDeselect (bool bNewAutoDeselect)
{
	if (SelectedItems.size() > 1 && bNewAutoDeselect)
	{
		ClearSelection();
	}

	bAutoDeselect = bNewAutoDeselect;
}

void ListBoxComponent::SetReadOnly (bool bNewReadOnly)
{
	bReadOnly = bNewReadOnly;
}

void ListBoxComponent::SetBackground (FrameComponent* newBackground)
{
	if (Background != nullptr)
	{
		Background->Destroy();
	}

	Background = newBackground;
	if (Background != nullptr)
	{
		AddComponent(Background.Get());
	}
}

void ListBoxComponent::SetScrollbar (ScrollbarComponent_Deprecated* newScrollbar)
{
	if (Scrollbar != nullptr)
	{
		Scrollbar->Destroy();
	}

	Scrollbar = newScrollbar;
	if (Scrollbar != nullptr && AddComponent(Scrollbar.Get()))
	{
		Scrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, ListBoxComponent, HandleScrollPositionChanged, void, INT));
	}
}

void ListBoxComponent::SetItemListText (LabelComponent* newItemListText)
{
	if (ItemListText != nullptr)
	{
		ItemListText->Destroy();
	}

	ItemListText = newItemListText;
	if (ItemListText != nullptr)
	{
		AddComponent(ItemListText.Get());
	}

	if (OnListSizeChanged.IsBounded())
	{
		OnListSizeChanged.Execute();
	}

	UpdateTextContent();
}

void ListBoxComponent::SetSelectionColor (Color newSelectionColor)
{
	SelectionColor = newSelectionColor;

	for (UINT_TYPE i = 0; i < HighlightSelections.size(); i++)
	{
		HighlightSelections.at(i).RenderComp->SolidColor = SelectionColor;
	}
}

void ListBoxComponent::SetHoverColor (Color newHoverColor)
{
	HoverColor = newHoverColor;

	if (HighlightHoverRender != nullptr)
	{
		HighlightHoverRender->SolidColor = HoverColor;
	}
}

void ListBoxComponent::ClearList ()
{
	ClearSelection();

	//Destroy the allocated GuiDataElement objects
	for (UINT_TYPE i = 0; i < List.size(); i++)
	{
		delete List.at(i);
	}

	ContainerUtils::Empty(List);

	if (OnListSizeChanged.IsBounded())
	{
		OnListSizeChanged.Execute();
	}

	UpdateTextContent();
	CalculateColorTransforms();
}

void ListBoxComponent::InitializeBackground ()
{
	Background = FrameComponent::CreateObject();
	if (AddComponent(Background.Get()))
	{
		Background->SetLockedFrame(true);
	}
}

void ListBoxComponent::InitializeScrollbar ()
{
	CHECK(Background != nullptr)

	Scrollbar = ScrollbarComponent_Deprecated::CreateObject();
	if (Background->AddComponent(Scrollbar.Get()))
	{
		Scrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, ListBoxComponent, HandleScrollPositionChanged, void, INT));
		Scrollbar->OnToggleVisibility = SDFUNCTION(this, ListBoxComponent, HandleScrollbarToggleVisibility, void);
	}
}

void ListBoxComponent::InitializeItemListText ()
{
	CHECK(Background != nullptr && Scrollbar != nullptr)

	ItemListText = LabelComponent::CreateObject();
	if (Background->AddComponent(ItemListText.Get()))
	{
		ItemListText->SetClampText(true);
		ItemListText->SetWrapText(false);

		Scrollbar->SetMaxScrollPosition(List.size());
		Scrollbar->SetNumVisibleScrollPositions(ItemListText->GetMaxNumLines());
		UpdateTextContent();
	}
}

void ListBoxComponent::InitializeHighlightBar ()
{
	CHECK(ItemListText != nullptr)

	HighlightOwner = GuiComponent::CreateObject();
	if (ItemListText->AddComponent(HighlightOwner.Get()))
	{
		HighlightOwner->SetSize(Vector2::ZeroVector);
		HighlightHoverRender = SolidColorRenderComponent::CreateObject();
		if (HighlightOwner->AddComponent(HighlightHoverRender.Get()))
		{
			HighlightHoverRender->SolidColor = HoverColor;
		}
	}
}

void ListBoxComponent::RefreshComponentTransforms ()
{
	bNeedColorTransformCalc = true;

	if (Background != nullptr)
	{
		Background->SetSize(Vector2(1.f, 1.f));
	}

	if (Scrollbar != nullptr)
	{
		Scrollbar->SetSize(Vector2(Scrollbar->ReadSize().X, ReadCachedAbsSize().Y));
		Scrollbar->SetPosition(Vector2(ReadCachedAbsSize().X - Scrollbar->ReadCachedAbsSize().X, 0.f));
	}

	FLOAT widthDeduction = (Scrollbar != nullptr && Scrollbar->IsVisible()) ? Scrollbar->ReadCachedAbsSize().X : 0.f;
	widthDeduction += (Background->GetBorderThickness() * 2);
	CHECK(ItemListText != nullptr)
	ItemListText->SetSize(Vector2(ReadCachedAbsSize().X - widthDeduction, ReadCachedAbsSize().Y - Background->GetBorderThickness() * 2));
	ItemListText->SetPosition(Vector2(Background->GetBorderThickness(), Background->GetBorderThickness()));
	Scrollbar->SetNumVisibleScrollPositions(ItemListText->GetMaxNumLines());
	UpdateTextContent();

	if (bNeedColorTransformCalc)
	{
		CalculateColorTransforms();
	}
}

void ListBoxComponent::CalculateColorTransforms ()
{
	bNeedColorTransformCalc = false;

	for (UINT_TYPE i = 0; i < HighlightSelections.size(); i++)
	{
		HighlightSelections.at(i).RenderOwner->Destroy();
	}

	ContainerUtils::Empty(HighlightSelections);

	INT firstSelectedItem = INT_INDEX_NONE;
	if (Scrollbar != nullptr)
	{
		for (UINT_TYPE i = 0; i < SelectedItems.size(); i++)
		{
			//Identify which selected item is first visible
			if (Scrollbar->IsIndexVisible(SelectedItems.at(i)))
			{
				firstSelectedItem = i;
				break;
			}
		}
	}
	else if (SelectedItems.size() > 0)
	{
		firstSelectedItem = 0;
	}

	if (firstSelectedItem == INT_INDEX_NONE)
	{
		return; //None of the selected items are visible
	}

	for (INT selectedItem = firstSelectedItem; selectedItem < SelectedItems.size();)
	{
		//Is this still within scrollbar visibility?
		if (Scrollbar != nullptr && !Scrollbar->IsIndexVisible(SelectedItems.at(selectedItem.ToUnsignedInt())))
		{
			break; //Everything else is beyond scrollbar
		}

		Rectangle<FLOAT> lineRegion = GetItemRegion(SelectedItems.at(selectedItem.ToUnsignedInt()));

		INT lineHeight = 1;
		for (INT i = selectedItem + 1; i < SelectedItems.size(); i++)
		{
			//Is next selected item visible?
			if (Scrollbar != nullptr && !Scrollbar->IsIndexVisible(SelectedItems.at(i.ToUnsignedInt())))
			{
				break;
			}

			//Is next item adjacent to current selected item?
			if (SelectedItems.at(i.ToUnsignedInt()) - SelectedItems.at(i.ToUnsignedInt()-1) == 1)
			{
				lineHeight++;
			}
			else
			{
				break; //found gap
			}
		}

		//Construct the render component and its transformation
		SHighlightSelectionInfo selectionInfo;
		selectionInfo.RenderOwner = GuiComponent::CreateObject();
		if (AddComponent(selectionInfo.RenderOwner.Get()))
		{
			selectionInfo.RenderOwner->SetPosition(Vector2(lineRegion.Left, lineRegion.Top));
			selectionInfo.RenderOwner->SetSize(Vector2(lineRegion.GetWidth(), lineRegion.GetHeight() * lineHeight.ToFLOAT()));
			selectionInfo.RenderComp = SolidColorRenderComponent::CreateObject();
			if (selectionInfo.RenderOwner->AddComponent(selectionInfo.RenderComp.Get()))
			{
				selectionInfo.RenderComp->SolidColor = SelectionColor;
				selectionInfo.NumLines = lineHeight;
				HighlightSelections.push_back(selectionInfo);
			}
			else
			{
				selectionInfo.RenderOwner->Destroy();
			}
		}

		//Skip over the adjacent selected lines since the expanded line region already covers those lines.
		selectedItem += lineHeight;
	}
}

void ListBoxComponent::RenderHighlightHoverOverBrowsedItem ()
{
	CHECK(HighlightOwner != nullptr && ItemListText != nullptr)
	if (BrowseIndex == INT_INDEX_NONE)
	{
		//Set size to be zero instead of setting render visibility to avoid conflict with GuiComponent's SetVisibility.
		HighlightOwner->SetSize(Vector2::ZeroVector);
		return;
	}

	Rectangle<FLOAT> lineRegion = GetItemRegion(BrowseIndex);
	if (lineRegion.IsEmpty())
	{
		HighlightOwner->SetSize(Vector2::ZeroVector);
		return;
	}

	//Convert from absolute space to HighlightHover space
	Vector2 hoverPos = Vector2(lineRegion.Left, lineRegion.Top);
	hoverPos -= ItemListText->GetCachedAbsPosition();

	HighlightOwner->SetSize(Vector2(lineRegion.GetWidth(), lineRegion.GetHeight()));
	HighlightOwner->SetPosition(hoverPos);
}

void ListBoxComponent::UpdateTextContent ()
{
	INT startPos = 0;
	if (Scrollbar != nullptr)
	{
		startPos = Scrollbar->GetScrollPosition();
		Scrollbar->SetMaxScrollPosition(List.size());

		if (startPos > List.size())
		{
			Scrollbar->SetScrollPosition(0); //Scroll position is greater than new list size.  Snap to top.
			return; //Return early since the scrollbar's change scroll position's callback calls this function.
		}
	}

	CHECK(ItemListText != nullptr && startPos <= INT(List.size()))

	DString newContent = DString::EmptyString;
	for (INT i = startPos; i < List.size(); i++)
	{
		if (Scrollbar != nullptr && !Scrollbar->IsIndexVisible(i))
		{
			break; //Reached the scrollbar's visible limit.
		}

		DString curLine = List.at(i.ToUnsignedInt())->GetLabelText();
		INT endChar = ItemListText->GetFont()->FindCharNearWidthLimit(curLine, ItemListText->GetCharacterSize(), ItemListText->ReadCachedAbsSize().X);
		if (endChar > 0 && endChar < curLine.Length() - 1)
		{
			curLine = curLine.SubString(0, endChar + 1);
		}

		newContent += curLine + TXT("\n");
	}

	ItemListText->SetText(newContent);
}

INT ListBoxComponent::GetLineIdxFromCoordinates (FLOAT xPos, FLOAT yPos) const
{
	CHECK(ItemListText != nullptr)

	Vector2 relativeCoords(xPos, yPos);
	relativeCoords -= ItemListText->ReadCachedAbsPosition();

	//Did user click within bounds
	if (relativeCoords.X < 0 || relativeCoords.X > ItemListText->ReadCachedAbsSize().X)
	{
		return INT_INDEX_NONE;
	}
	else if (relativeCoords.Y < 0 || relativeCoords.Y > ItemListText->ReadCachedAbsSize().Y)
	{
		return INT_INDEX_NONE;
	}

	INT lineIdx = (relativeCoords.Y / ItemListText->GetLineHeight()).ToINT();
	if (Scrollbar != nullptr)
	{
		lineIdx += Scrollbar->GetScrollPosition();
	}

	//Min to handle case when user clicked beyond the last item, but still within component boundaries.
	return Utils::Min<INT>(lineIdx, List.size() - 1);
}

Rectangle<FLOAT> ListBoxComponent::GetItemRegion (INT lineIdx) const
{
	CHECK(ItemListText != nullptr)

	if (Scrollbar != nullptr && !Scrollbar->IsIndexVisible(lineIdx))
	{
		return Rectangle<FLOAT>(Rectangle<FLOAT>::CS_XRightYDown); //Return empty rectangle
	}

	INT relativeIdx = (Scrollbar != nullptr) ? lineIdx - Scrollbar->GetScrollPosition() : lineIdx;

	Rectangle<FLOAT> region(Rectangle<FLOAT>::CS_XRightYDown);
	region.Top = ItemListText->ReadCachedAbsPosition().Y + (relativeIdx.ToFLOAT() * ItemListText->GetLineHeight());
	region.Bottom = region.Top + ItemListText->GetLineHeight();
	region.Left = ItemListText->ReadCachedAbsPosition().X;
	region.Right = region.Left + ItemListText->ReadCachedAbsSize().X;

	return region;
}

void ListBoxComponent::HandleScrollPositionChanged (INT newScrollIndex)
{
	UpdateTextContent();
	CalculateColorTransforms();
}

void ListBoxComponent::HandleScrollbarToggleVisibility ()
{
	//Need to refresh transforms since the width of the label component changed to make space for scrollbar.
	RefreshComponentTransforms();
}

SD_END