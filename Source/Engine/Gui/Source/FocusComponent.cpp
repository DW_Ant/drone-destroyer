/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FocusComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, FocusComponent, SD, EntityComponent)

void FocusComponent::InitProps ()
{
	Super::InitProps();

	SelectedEntityIdx = INT_INDEX_NONE;
	Input = nullptr;
}

void FocusComponent::BeginObject ()
{
	Super::BeginObject();

	InitializeInput();
}

void FocusComponent::SelectNextEntity ()
{
	if (TabOrder.size() <= 0)
	{
		ClearSelection();
		return;
	}

	FocusInterface* selectedEntity = GetSelectedEntity();
	if (selectedEntity != nullptr)
	{
		selectedEntity->LoseFocus();
	}

	do
	{
		if ((++SelectedEntityIdx).ToUnsignedInt() >= TabOrder.size())
		{
			SelectedEntityIdx = 0;
		}

		FocusInterface* newSelectedEntity = GetSelectedEntity();
		if (newSelectedEntity == selectedEntity)
		{
			break; //Went full loop (all other entities are currently not focusable), reselect old entity
		}
	}
	while (!GetSelectedEntity()->CanBeFocused());

	selectedEntity = GetSelectedEntity();
	if (selectedEntity != nullptr)
	{
		selectedEntity->GainFocus();
	}
}

void FocusComponent::ClearSelection ()
{
	FocusInterface* selectedEntity = GetSelectedEntity();
	if (selectedEntity != nullptr)
	{
		selectedEntity->LoseFocus();
	}

	SelectedEntityIdx = INT_INDEX_NONE;
}

FocusInterface* FocusComponent::GetSelectedEntity () const
{
	if (SelectedEntityIdx >= 0 && SelectedEntityIdx.ToUnsignedInt() < TabOrder.size())
	{
		return TabOrder.at(SelectedEntityIdx.ToUnsignedInt());
	}

	return nullptr;
}

InputComponent* FocusComponent::GetInput () const
{
	return Input.Get();
}

void FocusComponent::InitializeInput ()
{
	Input = InputComponent::CreateObject();
	if (!AddComponent(Input.Get()))
	{
		Input = nullptr;
		return;
	}

	Input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, FocusComponent, HandleInput, bool, const sf::Event&);
	Input->CaptureTextDelegate = SDFUNCTION_1PARAM(this, FocusComponent, HandleText, bool, const sf::Event&);
}

bool FocusComponent::HandleInput (const sf::Event& keyEvent)
{
	if (keyEvent.type == sf::Event::KeyPressed && keyEvent.key.code == sf::Keyboard::Tab)
	{
		if (GetSelectedEntity() == nullptr || GetSelectedEntity()->CanTabOut())
		{
			SelectNextEntity();
			return true;
		}
	}

	FocusInterface* selectedEntity = GetSelectedEntity();
	if (selectedEntity != nullptr)
	{
		return selectedEntity->CaptureFocusedInput(keyEvent);
	}

	return false;
}

bool FocusComponent::HandleText (const sf::Event& keyEvent)
{
	FocusInterface* selectedEntity = GetSelectedEntity();
	if (selectedEntity != nullptr)
	{
		return selectedEntity->CaptureFocusedText(keyEvent);
	}

	return false;
}
SD_END