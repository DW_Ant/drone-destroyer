/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HoverComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, HoverComponent, SD, GuiComponent)

void HoverComponent::InitProps ()
{
	Super::InitProps();

	bIsHoveringOwner = false;
}

void HoverComponent::ProcessMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ProcessMouseMove(mouse, sfmlEvent, deltaMove);

	EvaluateHoverState(mouse, sfmlEvent);
}

bool HoverComponent::GetIsHovering () const
{
	return bIsHoveringOwner;
}

void HoverComponent::EvaluateHoverState (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent)
{
	bool bOldHovering = bIsHoveringOwner;
	bIsHoveringOwner = IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));

	if (bOldHovering != bIsHoveringOwner)
	{
		//Hover state change detected, invoke callback
		if (bIsHoveringOwner && OnRollOver.IsBounded())
		{
			OnRollOver(mouse, GetOwner());
		}
		else if (!bIsHoveringOwner && OnRollOut.IsBounded())
		{
			OnRollOut(mouse, GetOwner());
		}
	}
}
SD_END