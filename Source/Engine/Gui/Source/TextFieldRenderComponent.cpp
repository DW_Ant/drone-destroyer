/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFieldRenderComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, TextFieldRenderComponent, SD, TextRenderComponent)

void TextFieldRenderComponent::InitProps ()
{
	Super::InitProps();

	OwningTextField = nullptr;
	CursorBlinkInterval = 0.75f;
	CursorColor = sf::Color::White;
	CursorWidth = 1.5f;
	CursorHeightMultiplier = 1.25f;
	MoveDetectionDuration = 1.f;
	bForceRecalcCursorCoordinates = true;

	SolidCursorRenderEndTime = -1.f;

	LocalEngine = nullptr;
	LastCursorMoveTime = -1.f;
	LastCursorPosition = -1;
}

void TextFieldRenderComponent::BeginObject ()
{
	Super::BeginObject();

	LocalEngine = Engine::FindEngine();
	CHECK(LocalEngine != nullptr)
}

void TextFieldRenderComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const TextFieldRenderComponent* textTemplate = dynamic_cast<const TextFieldRenderComponent*>(objTemplate);
	if (textTemplate != nullptr)
	{
		CursorBlinkInterval = textTemplate->CursorBlinkInterval;
		CursorColor = textTemplate->CursorColor;
		CursorWidth = textTemplate->CursorWidth;
		MoveDetectionDuration = textTemplate->MoveDetectionDuration;
		bForceRecalcCursorCoordinates = true;
	}
}

bool TextFieldRenderComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (dynamic_cast<TextFieldComponent*>(ownerCandidate) == nullptr)
	{
		return false;
	}

	return Super::CanBeAttachedTo(ownerCandidate);
}

void TextFieldRenderComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	Super::Render(renderTarget, camera); //Render text

	CHECK(renderTarget != nullptr)
	if (OwningTextField == nullptr || ContainerUtils::IsEmpty(Texts))
	{
		return;
	}

	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	INT highlightEndPos = OwningTextField->GetHighlightEndPosition();
	INT cursorPos = OwningTextField->GetCursorPosition();
	if (highlightEndPos < 0 && cursorPos >= 0) //render cursor blinker
	{
		if (CursorBlinkInterval <= 0)
		{
			return; //invisible cursor
		}

		if (LastProjectedPosition != projectionData.Position || bForceRecalcCursorCoordinates || LastCursorPosition != cursorPos)
		{
			LastProjectedPosition = projectionData.Position;
			LastCursorPosition = cursorPos;
			LastCursorMoveTime = LocalEngine->GetElapsedTime();
			CursorCoordinates = FindCursorCoordinates(projectionData, LastCursorPosition);
			bForceRecalcCursorCoordinates = false;
		}

		if (CursorCoordinates.x < 0 && CursorCoordinates.y < 0)
		{
			return; //Don't render cursor since it's at text beyond scroll position.
		}

		bool bSolidCursor = false;
		if (SolidCursorRenderEndTime > 0)
		{
			bSolidCursor = (LocalEngine->GetElapsedTime() < SolidCursorRenderEndTime);
			if (!bSolidCursor)
			{
				SolidCursorRenderEndTime = -1.f; //disable solid cursor
			}
		}

		bool bRecentlyMoved = ((LocalEngine->GetElapsedTime() - LastCursorMoveTime) < MoveDetectionDuration);
		if (bSolidCursor || bRecentlyMoved || sin((LocalEngine->GetElapsedTime().Value * PI) / CursorBlinkInterval.Value) > 0)
		{
			sf::Vector2f cursorSize;
			cursorSize.x = CursorWidth.Value;
			cursorSize.y = static_cast<float>(Texts.at(0).SfmlInstance->getCharacterSize());
			cursorSize.y *= CursorHeightMultiplier;

			sf::RectangleShape cursor;
			cursor.setSize(cursorSize);
			cursor.setPosition(CursorCoordinates);
			cursor.setFillColor(CursorColor);
			renderTarget->Draw(cursor);
		}
	}
}

void TextFieldRenderComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwningTextField = dynamic_cast<TextFieldComponent*>(newOwner);
}

void TextFieldRenderComponent::ComponentDetached ()
{
	OwningTextField = nullptr;

	Super::ComponentDetached();
}

void TextFieldRenderComponent::PauseBlinker (FLOAT renderTime)
{
	SolidCursorRenderEndTime = LocalEngine->GetElapsedTime() + renderTime;
}

sf::Vector2f TextFieldRenderComponent::FindCursorCoordinates (const Transformation::SScreenProjectionData& projectionData, INT cursorIndex)
{
	CHECK(cursorIndex >= 0 && OwningTextField->GetFont() != nullptr)

	INT accumulatedPos = 0;
	size_t lineIdx = 0;
	while (lineIdx < OwningTextField->ReadContent().size())
	{
		if (accumulatedPos + OwningTextField->ReadContent().at(lineIdx).Length() < cursorIndex)
		{
			//+1 to include cursor position at end of line.
			accumulatedPos += OwningTextField->ReadContent().at(lineIdx).Length() + 1;
		}
		else
		{
			//Found the line that contains the cursor.
			break;
		}

		++lineIdx;
	}

	if (lineIdx >= OwningTextField->ReadContent().size())
	{
		return sf::Vector2f(-1.f, -1.f);
	}

	//Compute the yPosition
	FLOAT posY = (FLOAT::MakeFloat(lineIdx) * OwningTextField->GetLineHeight()) + OwningTextField->GetFirstLineOffset();
	posY *= projectionData.Scale.y;

	//Compute the xPosition
	FLOAT posX = OwningTextField->GetFont()->CalculateStringWidth(OwningTextField->ReadContent().at(lineIdx), OwningTextField->GetCharacterSize(), 0, cursorIndex - accumulatedPos);
	posX *= projectionData.Scale.x;

	return (projectionData.Position + sf::Vector2f(posX.Value, posY.Value));
}
SD_END