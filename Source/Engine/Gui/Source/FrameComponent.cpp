/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FrameComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, FrameComponent, SD, GuiComponent)

/* Various flags that are associated with a EBorder type. */
const INT FrameComponent::HorizontalBorderFlag = 1;
const INT FrameComponent::VerticalBorderFlag = 2;

void FrameComponent::InitProps ()
{
	Super::InitProps();

	ConsumesInput = false;

	bLockedFrame = false;
	MinDragSize = Vector2(0.f, 0.f);
	MaxDragSize = Vector2(MAXFLOAT, MAXFLOAT);
	HorGrabbedBorder = B_None;
	VertGrabbedBorder = B_None;
	SetBorderThickness(3.f);
	RenderComponent = nullptr;
	HoverFlags = 0;
	IconOverridePointer = nullptr;
	IconOverrideTexture = nullptr;
}

void FrameComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const FrameComponent* frameTemplate = dynamic_cast<const FrameComponent*>(objTemplate);
	if (frameTemplate != nullptr)
	{
		bool bCreatedObj;
		RenderComponent = ReplaceTargetWithObjOfMatchingClass(RenderComponent.Get(), frameTemplate->RenderComponent.Get(), bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(RenderComponent.Get());
		}

		if (RenderComponent != nullptr)
		{
			RenderComponent->CopyPropertiesFrom(frameTemplate->RenderComponent.Get());
		}

		SetBorderThickness(frameTemplate->GetBorderThickness());
		SetLockedFrame(frameTemplate->GetLockedFrame());
		SetMinDragSize(frameTemplate->MinDragSize);
		SetMaxDragSize(frameTemplate->MaxDragSize);
	}
}

void FrameComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	//Setup default properties in case default style does not initialize them.
	RenderComponent = BorderedSpriteComponent::CreateObject();
	if (AddComponent(RenderComponent.Get()))
	{
		RenderComponent->SetSpriteTexture(GuiTheme::GetGuiTheme()->FrameFill.Get());	
		RenderComponent->TopBorder = GuiTheme::GetGuiTheme()->FrameBorderTop;
		RenderComponent->RightBorder = GuiTheme::GetGuiTheme()->FrameBorderRight;
		RenderComponent->BottomBorder = GuiTheme::GetGuiTheme()->FrameBorderBottom;
		RenderComponent->LeftBorder = GuiTheme::GetGuiTheme()->FrameBorderLeft;
		RenderComponent->TopRightCorner = GuiTheme::GetGuiTheme()->FrameBorderTopRight;
		RenderComponent->BottomRightCorner = GuiTheme::GetGuiTheme()->FrameBorderBottomRight;
		RenderComponent->BottomLeftCorner = GuiTheme::GetGuiTheme()->FrameBorderBottomLeft;
		RenderComponent->TopLeftCorner = GuiTheme::GetGuiTheme()->FrameBorderTopLeft;
		RenderComponent->BorderThickness = BorderThickness;
	}
}

void FrameComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (HoverFlags > 0)
	{
		IconOverridePointer = mouse;
		mouse->ConditionallyEvaluateMouseIconOverride(SDFUNCTION_2PARAM(this, FrameComponent, HandleMouseIconOverride, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
	}

	if (bLockedFrame)
	{
		return;
	}

	//Update mouse pointer if hovering over borders
	if (HorGrabbedBorder == B_None && VertGrabbedBorder == B_None)
	{
		//Update the mouse icon if necessary
		IconOverrideTexture = ShouldOverrideMouseIcon(mouse, sfmlEvent);
		if (IconOverrideTexture != nullptr)
		{
			mouse->PushMouseIconOverride(IconOverrideTexture.Get(), SDFUNCTION_2PARAM(this, FrameComponent, HandleMouseIconOverride, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
		}

		return;
	}

	// User is grabbing the borders.  Reposition and/or rescale the borders.
	CalculateTransformationChanges(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
}

void FrameComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	//Release grabbing borders
	if (eventType == sf::Event::MouseButtonReleased)
	{
		if (HorGrabbedBorder != B_None || VertGrabbedBorder != B_None)
		{
			HorGrabbedBorder = B_None;
			VertGrabbedBorder = B_None;
		}
	}
}

bool FrameComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (!IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))))
	{
		return false;
	}

	//Frames only listen to left mouse button
	if (sfmlEvent.button != sf::Mouse::Left || eventType != sf::Event::MouseButtonPressed)
	{
		//Consume all input when clicking inside this frame
		return ConsumesInput;
	}

	if (bLockedFrame)
	{
		//Consume all input when clicking inside this frame
		return ConsumesInput;
	}

	const Vector2 finalCoordinates = GetCachedAbsPosition();
	StartDragBorders = Rectangle<FLOAT>(GetCachedAbsPosition().X, GetCachedAbsPosition().Y, ReadCachedAbsPosition().X + ReadCachedAbsSize().X, ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y, Rectangle<FLOAT>::CS_XRightYDown);
	OwnerAbsBorders = (GetRelativeTo() != nullptr) ? Rectangle<FLOAT>(GetRelativeTo()->GetCachedAbsPosition().X, GetRelativeTo()->GetCachedAbsPosition().Y,
		GetRelativeTo()->ReadCachedAbsPosition().X + GetRelativeTo()->ReadCachedAbsSize().X, GetRelativeTo()->ReadCachedAbsPosition().Y + GetRelativeTo()->ReadCachedAbsSize().Y, Rectangle<FLOAT>::CS_XRightYDown) :
		Rectangle<FLOAT>(0.f, 0.f, MAXFLOAT, MAXFLOAT, Rectangle<FLOAT>::CS_XRightYDown);

	//check vertical borders
	if (FLOAT::MakeFloat(sfmlEvent.x) <= finalCoordinates.X + BorderThickness)
	{
		VertGrabbedBorder = B_Left;
		ClampMouseX.Min = OwnerAbsBorders.Left;
		ClampMouseX.Max = StartDragBorders.Right - (BorderThickness * 2.f);

		//Apply drag size limits
		ClampMouseX.Max = Utils::Min(ClampMouseX.Max, StartDragBorders.Right - MinDragSize.X);
		ClampMouseX.Min = Utils::Max(ClampMouseX.Min, StartDragBorders.Right - MaxDragSize.X);
	}
	else if (FLOAT::MakeFloat(sfmlEvent.x) >= ReadCachedAbsPosition().X + ReadCachedAbsSize().X - BorderThickness)
	{
		VertGrabbedBorder = B_Right;
		ClampMouseX.Min = StartDragBorders.Left + (BorderThickness * 2.f);
		ClampMouseX.Max = OwnerAbsBorders.Right;

		//Apply drag limits
		ClampMouseX.Min = Utils::Max(ClampMouseX.Min, StartDragBorders.Left + MinDragSize.X);
		ClampMouseX.Max = Utils::Min(ClampMouseX.Max, StartDragBorders.Left + MaxDragSize.X);
	}

	//check horizontal borders
	if (FLOAT::MakeFloat(sfmlEvent.y) <= finalCoordinates.Y + BorderThickness)
	{
		HorGrabbedBorder = B_Top;

		ClampMouseY.Min = OwnerAbsBorders.Top;
		ClampMouseY.Max = StartDragBorders.Bottom - (BorderThickness * 2.f);

		//Apply drag size limits
		ClampMouseY.Max = Utils::Min(ClampMouseY.Max, StartDragBorders.Bottom - MinDragSize.Y);
		ClampMouseY.Min = Utils::Max(ClampMouseY.Min, StartDragBorders.Bottom - MaxDragSize.Y);
	}
	else if (FLOAT::MakeFloat(sfmlEvent.y) >= ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y - BorderThickness)
	{
		HorGrabbedBorder = B_Bottom;
		ClampMouseY.Min = StartDragBorders.Top + (BorderThickness * 2.f);
		ClampMouseY.Max = OwnerAbsBorders.Bottom;

		//Apply drag limits
		ClampMouseY.Min = Utils::Max(ClampMouseY.Min, StartDragBorders.Top + MinDragSize.Y);
		ClampMouseY.Max = Utils::Min(ClampMouseY.Max, StartDragBorders.Top + MaxDragSize.Y);
	}

	return (ConsumesInput || VertGrabbedBorder != B_None || HorGrabbedBorder != B_None);
}

void FrameComponent::Destroyed ()
{
	//Remove callback in case this was destroyed while user was hovering over it
	if (IconOverridePointer != nullptr)
	{
		IconOverridePointer->RemoveIconOverride(SDFUNCTION_2PARAM(this, FrameComponent, HandleMouseIconOverride, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
	}

	Super::Destroyed();
}

void FrameComponent::SetLockedFrame (bool bNewLockedFrame)
{
	bLockedFrame = bNewLockedFrame;

	if (bLockedFrame)
	{
		//Release mouse controls
		HorGrabbedBorder = B_None;
		VertGrabbedBorder = B_None;
		HoverFlags = 0;
	}
}

void FrameComponent::SetMinDragSize (const Vector2& newMinDragSize)
{
	MinDragSize = newMinDragSize;
}

void FrameComponent::SetMaxDragSize (const Vector2& newMaxDragSize)
{
	MaxDragSize = newMaxDragSize;
}

void FrameComponent::SetBorderThickness (FLOAT newBorderThickness)
{
	BorderThickness = Utils::Max(FLOAT(1.f), newBorderThickness);

	if (RenderComponent != nullptr)
	{
		RenderComponent->BorderThickness = BorderThickness;
	}
}

bool FrameComponent::GetLockedFrame () const
{
	return bLockedFrame;
}

FLOAT FrameComponent::GetBorderThickness () const
{
	return BorderThickness;
}

Texture* FrameComponent::ShouldOverrideMouseIcon (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent)
{
	if (bLockedFrame)
	{
		IconOverridePointer = nullptr;
		return nullptr;
	}

	if (HorGrabbedBorder != B_None || VertGrabbedBorder != B_None)
	{
		return IconOverrideTexture.Get();
	}

	INT oldHoverFlags = HoverFlags;
	HoverFlags = 0;

	if (!IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))))
	{
		IconOverridePointer = nullptr;
		return nullptr; //not overriding pointer
	}

	FLOAT mousePosX = FLOAT::MakeFloat(sfmlEvent.x);
	FLOAT mousePosY = FLOAT::MakeFloat(sfmlEvent.y);

	const Vector2 finalCoordinates = GetCachedAbsPosition();

	//check vertical borders
	bool bHoveringVerticalBorder = (mousePosX <= finalCoordinates.X + BorderThickness || mousePosX >= ReadCachedAbsPosition().X + ReadCachedAbsSize().X - BorderThickness);
	bool bHoveringHorizontalBorder = (mousePosY <= finalCoordinates.Y + BorderThickness || mousePosY >= ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y - BorderThickness);

	if (bHoveringVerticalBorder)
	{
		HoverFlags |= VerticalBorderFlag;
	}

	if (bHoveringHorizontalBorder)
	{
		HoverFlags |= HorizontalBorderFlag;
	}

	if (HoverFlags == 0)
	{
		IconOverridePointer = nullptr;
		return nullptr;
	}

	if (HoverFlags != oldHoverFlags)
	{
		if (bHoveringVerticalBorder && bHoveringHorizontalBorder)
		{
			//Check if diagonal pointer should move in the \ or / direction
			bool bUpperLeft = ((mousePosX <= finalCoordinates.X + BorderThickness && mousePosY <= finalCoordinates.Y + BorderThickness) ||
				(mousePosX >= ReadCachedAbsPosition().X + ReadCachedAbsSize().X - BorderThickness && mousePosY >= ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y - BorderThickness));

			IconOverrideTexture = (bUpperLeft) ? GuiTheme::GetGuiTheme()->ResizePointerDiagonalTopLeft.Get() : GuiTheme::GetGuiTheme()->ResizePointerDiagonalBottomLeft.Get();
		}
		else if ((HoverFlags & HorizontalBorderFlag) != 0)
		{
			IconOverrideTexture = GuiTheme::GetGuiTheme()->ResizePointerVertical.Get();
		}
		else if ((HoverFlags & VerticalBorderFlag) != 0)
		{
			IconOverrideTexture = GuiTheme::GetGuiTheme()->ResizePointerHorizontal.Get();
		}
	}

	return IconOverrideTexture.Get();
}

void FrameComponent::CalculateTransformationChanges (FLOAT mousePosX, FLOAT mousePosY)
{
	//Ensure the mouse positions aren't negative (ie:  dragging beyond window borders)
	mousePosX = Utils::Max<FLOAT>(mousePosX, 0.f);
	mousePosY = Utils::Max<FLOAT>(mousePosY, 0.f);

	Range<FLOAT> posXLimits;
	Range<FLOAT> posYLimits;
	Range<FLOAT> sizeXLimits;
	Range<FLOAT> sizeYLimits;
	CalculateTransformationLimits(posXLimits, posYLimits, sizeXLimits, sizeYLimits);

	Vector2 newSize = GetCachedAbsSize();
	Vector2 newPos = GetCachedAbsPosition();

	//handle vertical borders
	if (VertGrabbedBorder == B_Left)
	{
		newPos.X = Utils::Clamp(mousePosX, posXLimits.Min, posXLimits.Max);
		newSize.X = Utils::Clamp(StartDragBorders.Right - newPos.X, sizeXLimits.Min, sizeXLimits.Max);
	}
	else if (VertGrabbedBorder == B_Right)
	{
		newSize.X = Utils::Clamp(mousePosX - StartDragBorders.Left, sizeXLimits.Min, sizeXLimits.Max);
	}

	//handle horizontal borders
	if (HorGrabbedBorder == B_Top)
	{
		newPos.Y = Utils::Clamp(mousePosY, posYLimits.Min, posYLimits.Max);
		newSize.Y = Utils::Clamp(StartDragBorders.Bottom - newPos.Y, sizeYLimits.Min, sizeYLimits.Max);
	}
	else if (HorGrabbedBorder == B_Bottom)
	{
		newSize.Y = Utils::Clamp(mousePosY - StartDragBorders.Top, sizeYLimits.Min, sizeYLimits.Max);
	}

	Vector2 relativeTo = (GetRelativeTo() != nullptr) ? GetRelativeTo()->GetCachedAbsPosition() : Vector2::ZeroVector;

	//Snap positions to 0 if it's close to 0 to prevent the transform from converting from abs to relative.
	{
		if ((newPos.X - relativeTo.X) <= 1.f)
		{
			newPos.X = relativeTo.X;
		}

		if ((newPos.Y - relativeTo.Y) <= 1.f)
		{
			newPos.Y = relativeTo.Y;
		}
	}

	SetSize(newSize);
	SetPosition(newPos - relativeTo);
}

void FrameComponent::CalculateTransformationLimits (Range<FLOAT>& outAbsCoordinatesXLimits, Range<FLOAT>& outAbsCoordinatesYLimits, Range<FLOAT>& outSizeXLimits, Range<FLOAT>& outSizeYLimits) const
{
	if (VertGrabbedBorder == B_Left)
	{
		outSizeXLimits.Min = BorderThickness * 2.f;
		outSizeXLimits.Max = StartDragBorders.Right - OwnerAbsBorders.Left;
	}
	else if (VertGrabbedBorder == B_Right)
	{
		outSizeXLimits.Min = ClampMouseX.Min - StartDragBorders.Left;
		outSizeXLimits.Max = ClampMouseX.Max - StartDragBorders.Left;
	}
	outAbsCoordinatesXLimits.Min = ClampMouseX.Min;
	outAbsCoordinatesXLimits.Max = ClampMouseX.Max;

	if (HorGrabbedBorder == B_Top)
	{
		outSizeYLimits.Min = BorderThickness * 2.f;
		outSizeYLimits.Max = StartDragBorders.Bottom - OwnerAbsBorders.Top;
	}
	else if (HorGrabbedBorder == B_Bottom)
	{
		outSizeYLimits.Min = ClampMouseY.Min - StartDragBorders.Top;
		outSizeYLimits.Max = ClampMouseY.Max - StartDragBorders.Top;
	}
	
	outAbsCoordinatesYLimits.Min = ClampMouseY.Min;
	outAbsCoordinatesYLimits.Max = ClampMouseY.Max;
}

Texture* FrameComponent::HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent)
{
	if (GetPendingDelete())
	{
		IconOverridePointer = nullptr;
		return nullptr;
	}

	return ShouldOverrideMouseIcon(mouse, sfmlEvent);
}
SD_END