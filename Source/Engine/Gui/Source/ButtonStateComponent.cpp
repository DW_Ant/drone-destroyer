/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ButtonStateComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(SD, ButtonStateComponent, SD, EntityComponent)

void ButtonStateComponent::InitProps ()
{
	Super::InitProps();

	OwningButton = nullptr;
}

void ButtonStateComponent::BeginObject ()
{
	Super::BeginObject();

	SetDefaultAppearance();
}

bool ButtonStateComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	if (ownerCandidate == nullptr)
	{
		return true;
	}
		
	return (dynamic_cast<ButtonComponent*>(ownerCandidate) != nullptr);
}

void ButtonStateComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwningButton = dynamic_cast<ButtonComponent*>(newOwner);
	SetDefaultAppearance();
}

void ButtonStateComponent::SetDefaultAppearance ()
{
	
}

void ButtonStateComponent::SetDisableAppearance ()
{
	
}

void ButtonStateComponent::SetHoverAppearance ()
{
	
}

void ButtonStateComponent::SetDownAppearance ()
{
	
}

void ButtonStateComponent::RefreshState ()
{
	if (OwningButton == nullptr)
	{
		return;
	}

	if (!OwningButton->GetEnabled())
	{
		SetDisableAppearance();
	}
	else if (OwningButton->GetPressedDown())
	{
		SetDownAppearance();
	}
	else if (OwningButton->GetHovered())
	{
		SetHoverAppearance();
	}
	else
	{
		SetDefaultAppearance();
	}
}

ButtonComponent* ButtonStateComponent::GetOwningButton () const
{
	return OwningButton.Get();
}

SpriteComponent* ButtonStateComponent::GetRenderComponent () const
{
	ButtonComponent* owningButton = GetOwningButton();
	if (owningButton != nullptr && owningButton->GetBackground() != nullptr)
	{
		return owningButton->GetBackground()->RenderComponent.Get();
	}

	return nullptr;
}
SD_END