/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DropdownComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, DropdownComponent, SD, GuiComponent)

void DropdownComponent::InitProps ()
{
	Super::InitProps();

	SelectedItemIdx = UINT_INDEX_NONE;
	NoSelectedItemText = DString::EmptyString;
	bExpanded = false;

	ExpandMenu = nullptr;
	ExpandButton = nullptr;
	ButtonTextureCollapsed = nullptr;
	ButtonTextureExpanded = nullptr;
	SelectedObjBackground = nullptr;
	SelectedItemLabel = nullptr;
	ListLabel = nullptr;

	DragToggleThresholdTime = 0.5f;
	bIgnoreEnterRelease = false;
	SearchString = DString::EmptyString;
	SearchStringTimestamp = 0.f;
	SearchStringClearTime = 1.f;
}

void DropdownComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const DropdownComponent* dropdownTemplate = dynamic_cast<const DropdownComponent*>(objTemplate);
	if (dropdownTemplate != nullptr)
	{
		bool bCreatedObj;
		ListBoxComponent* newExpandMenu = ReplaceTargetWithObjOfMatchingClass(ExpandMenu.Get(), dropdownTemplate->GetExpandMenu(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetExpandMenu(newExpandMenu);
		}

		if (ExpandMenu != nullptr)
		{
			ExpandMenu->CopyPropertiesFrom(dropdownTemplate->GetExpandMenu());
		}

		ButtonComponent* newExpandButton = ReplaceTargetWithObjOfMatchingClass(ExpandButton.Get(), dropdownTemplate->GetExpandButton(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetExpandButton(newExpandButton);
		}

		if (ExpandButton != nullptr)
		{
			ExpandButton->CopyPropertiesFrom(dropdownTemplate->GetExpandButton());
		}

		SetButtonTextureCollapsed(dropdownTemplate->GetButtonTextureCollapsed());
		SetButtonTextureExpanded(dropdownTemplate->GetButtonTextureExpanded());
		
		FrameComponent* newSelectedObjBackground = ReplaceTargetWithObjOfMatchingClass(SelectedObjBackground.Get(), dropdownTemplate->GetSelectedObjBackground(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetSelectedObjBackground(newSelectedObjBackground);
		}

		if (SelectedObjBackground != nullptr)
		{
			SelectedObjBackground->CopyPropertiesFrom(dropdownTemplate->GetSelectedObjBackground());
		}

		LabelComponent* newSelectedItemLabel = ReplaceTargetWithObjOfMatchingClass(SelectedItemLabel.Get(), dropdownTemplate->GetSelectedItemLabel(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetSelectedItemLabel(newSelectedItemLabel);
		}

		if (SelectedItemLabel != nullptr)
		{
			SelectedItemLabel->CopyPropertiesFrom(dropdownTemplate->GetSelectedItemLabel());
		}

		LabelComponent* newListLabel = ReplaceTargetWithObjOfMatchingClass(ListLabel.Get(), dropdownTemplate->GetListLabel(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			SetListLabel(newListLabel);
		}

		if (ListLabel != nullptr)
		{
			ListLabel->CopyPropertiesFrom(dropdownTemplate->GetListLabel());
		}
	}
}

bool DropdownComponent::CanBeFocused () const
{
	return IsVisible();
}

void DropdownComponent::LoseFocus ()
{
	FocusInterface::LoseFocus();

	Collapse(); //clear selection
}

bool DropdownComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	if (ExpandMenu == nullptr)
	{
		return false;
	}

	if (keyEvent.key.code == sf::Keyboard::Return)
	{
		if (!bExpanded)
		{
			if (ExpandButton != nullptr)
			{
				if (keyEvent.type == sf::Event::KeyPressed)
				{
					ExpandButton->SetButtonDown(true, true);
					bIgnoreEnterRelease = true; //Ignore next release key to prevent it from immediately collapsing the menu.
				}
			}

			return true;
		}
		else //Expanded
		{
			if (keyEvent.type == sf::Event::KeyReleased && ExpandMenu->GetBrowseIndex() != INT_INDEX_NONE && !bIgnoreEnterRelease)
			{
				ExpandMenu->SetItemSelectionByIdx(ExpandMenu->GetBrowseIndex(), !ExpandMenu->IsItemSelectedByIndex(ExpandMenu->GetBrowseIndex()));
				if (ExpandButton != nullptr)
				{
					ExpandButton->SetButtonUp(true, true);
				}
			}

			bIgnoreEnterRelease = false;
			return true;
		}
	}

	if (bExpanded && keyEvent.type == sf::Event::KeyPressed)
	{
		if (keyEvent.key.code == sf::Keyboard::Up)
		{
			INT newIndex = ExpandMenu->GetBrowseIndex() - 1;
			if (newIndex < 0)
			{
				newIndex = ExpandMenu->ReadList().size() - 1;
			}

			ExpandMenu->SetBrowseIndex(newIndex);
			return true;
		}
		else if (keyEvent.key.code == sf::Keyboard::Down)
		{
			INT newIndex = ExpandMenu->GetBrowseIndex() + 1;
			if (newIndex >= ExpandMenu->ReadList().size())
			{
				newIndex = 0;
			}

			ExpandMenu->SetBrowseIndex(newIndex);
			return true;
		}
	}

	return false;
}

bool DropdownComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	if (keyEvent.text.unicode == '\r' || //Don't conflict with CaptureFocusInput (The SetSelectedItem later in this function would cause the dropdown to collapse)
			keyEvent.text.unicode == '\t')
	{
		return false;
	}

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)

	if (localEngine->GetElapsedTime() - SearchStringTimestamp > SearchStringClearTime)
	{
		SearchString = TXT("");
	}

	DString newEntry = DString(sf::String(keyEvent.text.unicode));

	SearchString += newEntry;
	SearchStringTimestamp = localEngine->GetElapsedTime();

	UINT_TYPE bestIndex = 0;
	INT mostMatches = 1;

	for (UINT_TYPE i = 0; i < ExpandMenu->ReadList().size(); i++)
	{
		INT numMatches = ExpandMenu->ReadList().at(i)->GetLabelText().FindFirstMismatchingIdx(SearchString, DString::CC_IgnoreCase);

		if (numMatches == INT_INDEX_NONE) //Found a matching string
		{
			bestIndex = i;
			break;
		}

		if (INT(numMatches) > mostMatches)
		{
			mostMatches = numMatches;
			bestIndex = i;
		}
	}

	if (bestIndex != SelectedItemIdx)
	{
		SetSelectedItem(bestIndex);
	}

	return true;
}

void DropdownComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	ButtonTextureCollapsed = GuiTheme::GetGuiTheme()->DropdownCollapsedButton;
	ButtonTextureExpanded = GuiTheme::GetGuiTheme()->DropdownExpandedButton;

	InitializeSelectedObjBackground();
	InitializeExpandButton();
	InitializeSelectedItemLabel();
	InitializeExpandMenu();
	RefreshComponentTransforms();
}

void DropdownComponent::HandleSizeChange ()
{
	Super::HandleSizeChange();

	RefreshComponentTransforms();
}

void DropdownComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (eventType != sf::Event::MouseButtonReleased)
	{
		return;
	}

	//Collapse this menu if the user clicks outside of menu
	if (bExpanded && ExpandMenu != nullptr && !ExpandMenu->IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))) && //These checks are to see if the user clicked outside the option list.
			(Engine::FindEngine()->GetElapsedTime() - PrevExpandTime >= DragToggleThresholdTime || eventType == sf::Event::MouseButtonPressed)) //These conditions check to see if the user clicked outside the bounds for a toggle action.  The mouse press event check is to handle spastic handling (clicking again immediately outside after expanding).
	{
		Collapse();
	}
}

void DropdownComponent::Destroyed ()
{
	ClearList(); //Need to deallocate the list of pointers

	Super::Destroyed();
}

void DropdownComponent::Expand ()
{
	if (ExpandMenu == nullptr)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("%s cannot be expanded since it does not have a ListBoxComponent."), ToString());
		return;
	}

	bExpanded = true;
	ExpandMenu->SetBrowseIndex(0);
	PrevExpandTime = Engine::FindEngine()->GetElapsedTime();
	ExpandMenu->SetVisibility(true);

	if (ExpandButton != nullptr && ExpandButton->GetRenderComponent() != nullptr && ButtonTextureExpanded != nullptr)
	{
		ExpandButton->GetRenderComponent()->SetSpriteTexture(ButtonTextureExpanded.Get());
	}
}

void DropdownComponent::Collapse ()
{
	if (ExpandMenu == nullptr)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("%s cannot be collapsed since it does not have a ListBoxComponent."), ToString());
		return;
	}

	bExpanded = false;
	ExpandMenu->SetBrowseIndex(INT_INDEX_NONE);
	ExpandMenu->SetVisibility(false);

	if (ExpandButton != nullptr && ExpandButton->GetRenderComponent() != nullptr && ButtonTextureCollapsed != nullptr)
	{
		ExpandButton->GetRenderComponent()->SetSpriteTexture(ButtonTextureCollapsed.Get());
	}
}

bool DropdownComponent::IsExpanded () const
{
	return bExpanded;
}

FLOAT DropdownComponent::GetCollapsedHeight () const
{
	if (SelectedObjBackground == nullptr)
	{
		return 0.f;
	}

	return SelectedObjBackground->ReadCachedAbsSize().Y;
}

void DropdownComponent::SetSelectedItem (UINT_TYPE itemIndex)
{
	SelectedItemIdx = itemIndex;
	if (SelectedItemLabel == nullptr)
	{
		return;
	}

	if (SelectedItemIdx > ExpandMenu->ReadList().size())
	{
		SelectedItemLabel->SetText(NoSelectedItemText);
		return;
	}

	SelectedItemLabel->SetText(ExpandMenu->ReadList().at(SelectedItemIdx)->GetLabelText());
	Collapse();
}

void DropdownComponent::SetNoSelectedItemText (const DString& newNoSelectedItemText)
{
	NoSelectedItemText = newNoSelectedItemText;
	if (SelectedItemIdx == UINT_INDEX_NONE && SelectedItemLabel != nullptr)
	{
		SelectedItemLabel->SetText(NoSelectedItemText);
	}
}

void DropdownComponent::ClearList ()
{
	if (ExpandMenu != nullptr)
	{
		ExpandMenu->ClearList();
	}

	SelectedItemIdx = UINT_INDEX_NONE;
	Collapse();

	if (SelectedItemLabel != nullptr)
	{
		SelectedItemLabel->SetText(NoSelectedItemText);
	}
}

void DropdownComponent::SetExpandMenu (ListBoxComponent* newExpandMenu)
{
	if (ExpandMenu != nullptr)
	{
		ExpandMenu->Destroy();
	}

	if (newExpandMenu != nullptr && AddComponent(newExpandMenu))
	{
		ExpandMenu = newExpandMenu;
		ExpandMenu->OnItemSelected = SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandItemSelected, void, INT);
		ExpandMenu->OnListSizeChanged = SDFUNCTION(this, DropdownComponent, HandleExpandListChanged, void);
	}
}

void DropdownComponent::SetExpandButton (ButtonComponent* newExpandButton)
{
	if (ExpandButton != nullptr)
	{
		ExpandButton->Destroy();
	}

	if (newExpandButton != nullptr && AddComponent(newExpandButton))
	{
		ExpandButton = newExpandButton;
		ExpandButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandButtonClicked, void, ButtonComponent*));
	}
}

void DropdownComponent::SetButtonTextureCollapsed (Texture* newButtonTextureCollapsed)
{
	ButtonTextureCollapsed = newButtonTextureCollapsed;
	if (ExpandButton != nullptr && ExpandButton->GetRenderComponent() != nullptr && !bExpanded)
	{
		ExpandButton->GetRenderComponent()->SetSpriteTexture(ButtonTextureCollapsed.Get());
	}
}

void DropdownComponent::SetButtonTextureExpanded (Texture* newButtonTextureExpanded)
{
	ButtonTextureExpanded = newButtonTextureExpanded;
	if (ExpandButton != nullptr && ExpandButton->GetRenderComponent() != nullptr && bExpanded)
	{
		ExpandButton->GetRenderComponent()->SetSpriteTexture(ButtonTextureExpanded.Get());
	}
}

void DropdownComponent::SetSelectedObjBackground (FrameComponent* newSelectedObjBackground)
{
	if (SelectedObjBackground != nullptr)
	{
		SelectedObjBackground->Destroy();
	}

	if (newSelectedObjBackground != nullptr && AddComponent(newSelectedObjBackground))
	{
		SelectedObjBackground = newSelectedObjBackground;
	}
}

void DropdownComponent::SetSelectedItemLabel (LabelComponent* newSelectedItemLabel)
{
	if (SelectedItemLabel != nullptr)
	{
		SelectedItemLabel->Destroy();
	}

	if (newSelectedItemLabel != nullptr && SelectedObjBackground != nullptr && SelectedObjBackground->AddComponent(newSelectedItemLabel))
	{
		SelectedItemLabel = newSelectedItemLabel;
	}
}

void DropdownComponent::SetListLabel (LabelComponent* newListLabel)
{
	if (ListLabel != nullptr)
	{
		ListLabel->Destroy();
	}

	if (newListLabel != nullptr && AddComponent(newListLabel))
	{
		ListLabel = newListLabel;
	}
}

DString DropdownComponent::GetNoSelectedItemText () const
{
	return NoSelectedItemText;
}

ButtonComponent* DropdownComponent::GetExpandButton () const
{
	return ExpandButton.Get();
}

Texture* DropdownComponent::GetButtonTextureCollapsed () const
{
	return ButtonTextureCollapsed.Get();
}

Texture* DropdownComponent::GetButtonTextureExpanded () const
{
	return ButtonTextureExpanded.Get();
}

FrameComponent* DropdownComponent::GetSelectedObjBackground () const
{
	return SelectedObjBackground.Get();
}

LabelComponent* DropdownComponent::GetSelectedItemLabel () const
{
	return SelectedItemLabel.Get();
}

LabelComponent* DropdownComponent::GetListLabel () const
{
	return ListLabel.Get();
}

void DropdownComponent::InitializeExpandButton ()
{
	ExpandButton = ButtonComponent::CreateObject();
	if (!AddComponent(ExpandButton.Get()))
	{
		ExpandButton = nullptr;
		return;
	}

	ExpandButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
	if (ExpandButton->GetRenderComponent() != nullptr)
	{
		ExpandButton->GetRenderComponent()->SetSpriteTexture(ButtonTextureCollapsed.Get());
	}

	Vector2 buttonSize = Vector2(32.f, 32.f);
	ExpandButton->SetSize(buttonSize);
	ExpandButton->SetPosition(Vector2(ReadCachedAbsSize().X - buttonSize.X, 0.f)); //Snap to top right corner

	if (ExpandButton->CaptionComponent != nullptr)
	{
		ExpandButton->CaptionComponent->Destroy();
	}
	
	ExpandButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandButtonClicked, void, ButtonComponent*));
}

void DropdownComponent::InitializeSelectedObjBackground ()
{
	SelectedObjBackground = FrameComponent::CreateObject();
	if (!AddComponent(SelectedObjBackground.Get()))
	{
		SelectedObjBackground = nullptr;
		return;
	}

	SelectedObjBackground->SetLockedFrame(true);
}

void DropdownComponent::InitializeSelectedItemLabel ()
{
	CHECK(SelectedObjBackground != nullptr && ExpandButton != nullptr)

	SelectedItemLabel = LabelComponent::CreateObject();
	if (!SelectedObjBackground->AddComponent(SelectedItemLabel.Get()))
	{
		SelectedItemLabel = nullptr;
		return;
	}

	SelectedItemLabel->SetClampText(true);
	SelectedItemLabel->SetWrapText(false);
	SelectedItemLabel->SetText(NoSelectedItemText);
}

void DropdownComponent::InitializeExpandMenu ()
{
	ExpandMenu = ListBoxComponent::CreateObject();
	if (AddComponent(ExpandMenu.Get()))
	{
		if (ExpandMenu->GetBackground() != nullptr)
		{
			ExpandMenu->GetBackground()->RenderComponent->TopBorder = nullptr;
			ExpandMenu->SetVisibility(IsExpanded());
			ExpandMenu->SetAutoDeselect(true);
			ExpandMenu->OnItemSelected = SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandItemSelected, void, INT);
			ExpandMenu->OnListSizeChanged = SDFUNCTION(this, DropdownComponent, HandleExpandListChanged, void);
		}
	}
}

void DropdownComponent::RefreshComponentTransforms ()
{
	if (ExpandButton != nullptr)
	{
		ExpandButton->SetPosition(Vector2(ReadCachedAbsSize().X - ExpandButton->ReadCachedAbsSize().X, 0.f));
	}

	if (SelectedObjBackground != nullptr)
	{
		SelectedObjBackground->SetSize(Vector2(ReadCachedAbsSize().X - ExpandButton->ReadCachedAbsSize().X, SelectedObjBackground->ReadSize().Y));
		SelectedObjBackground->SetPosition(Vector2::ZeroVector);
	}

	if (SelectedItemLabel != nullptr)
	{
		FLOAT borderThickness = SelectedObjBackground->GetBorderThickness();

		SelectedItemLabel->SetSize(Vector2(SelectedObjBackground->ReadCachedAbsSize().X - borderThickness, SelectedObjBackground->ReadCachedAbsSize().Y - (borderThickness * 2)));
		SelectedItemLabel->SetPosition(Vector2(borderThickness, borderThickness));
	}

	RefreshExpandMenuTransform();
}

void DropdownComponent::RefreshExpandMenuTransform ()
{
	if (ExpandMenu != nullptr)
	{
		FLOAT expandHeight = ReadCachedAbsSize().Y - SelectedObjBackground->ReadCachedAbsSize().Y;
		if (SelectedItemLabel != nullptr)
		{
			//Shrink the expand menu if there are few options to choose from.
			expandHeight = Utils::Min((SelectedItemLabel->GetLineHeight()) * FLOAT::MakeFloat(ExpandMenu->ReadList().size()) + (ExpandMenu->GetBackground()->GetBorderThickness() * 2), expandHeight);
		}

		ExpandMenu->SetSize(Vector2(ReadCachedAbsSize().X, expandHeight));
		ExpandMenu->SetPosition(Vector2(0.f, SelectedObjBackground->ReadCachedAbsSize().Y));
	}
}

void DropdownComponent::HandleExpandButtonClicked (ButtonComponent* uiComponent)
{
	(bExpanded) ? Collapse() : Expand();
}

void DropdownComponent::HandleExpandListChanged ()
{
	RefreshExpandMenuTransform();
}

void DropdownComponent::HandleExpandItemSelected (INT selectedItemIdx)
{
	SetSelectedItem(selectedItemIdx.ToUnsignedInt());
	if (OnOptionSelected.IsBounded())
	{
		OnOptionSelected(selectedItemIdx);
	}
}
SD_END