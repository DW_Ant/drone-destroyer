/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TestMenuButtonList.cpp
=====================================================================
*/

#include "GuiClasses.h"

#ifdef DEBUG_MODE

SD_BEGIN
IMPLEMENT_CLASS(SD, TestMenuButtonList, SD, GuiEntity)

void TestMenuButtonList::InitProps ()
{
	Super::InitProps();

	MenuFrame = nullptr;
}

void TestMenuButtonList::LoseFocus ()
{
	Super::LoseFocus();

	GuiLog.Log(LogCategory::LL_Debug, TXT("%s has lost focus."), ToString());
}

void TestMenuButtonList::GainFocus ()
{
	Super::GainFocus();

	GuiLog.Log(LogCategory::LL_Debug, TXT("%s has gained focus."), ToString());
}

void TestMenuButtonList::ConstructUI ()
{
	MenuFrame = FrameComponent::CreateObject();
	if (AddComponent(MenuFrame.Get()))
	{
		MenuFrame->SetPosition(Vector2::ZeroVector);
		MenuFrame->SetSize(Vector2(1.f, 1.f));
		MenuFrame->SetLockedFrame(true);

		for (UINT_TYPE i = 0; i < 10; i++)
		{
			ButtonComponent* newButton = InitializeButton();
			CHECK(newButton != nullptr)

			newButton->SetCaptionText(TXT("Button ") + INT(i + 1).ToString());

			//Disable button 5 to test if the focus component will skip it
			if (i == 4)
			{
				newButton->SetEnabled(false);
			}
		}

		Vector2 buttonSize = Vector2(0.6f, 16.f);
		FLOAT buttonSpacing = 8.f;
		for (UINT_TYPE i = 0; i < ButtonList.size(); i++)
		{
			ButtonList.at(i)->SetSize(buttonSize);
			ButtonList.at(i)->SetPosition(Vector2(0.f, ((buttonSize.Y + buttonSpacing) * FLOAT::MakeFloat(i)) + MenuFrame->GetBorderThickness()));
		}
	}
}

void TestMenuButtonList::InitializeFocusComponent ()
{
	Super::InitializeFocusComponent();
	CHECK(Focus != nullptr)

	for (UINT_TYPE i = 0; i < ButtonList.size(); i++)
	{
		Focus->TabOrder.push_back(ButtonList.at(i).Get());
	}
}

ButtonComponent* TestMenuButtonList::InitializeButton ()
{
	ButtonComponent* newButton = ButtonComponent::CreateObject();
	if (!MenuFrame->AddComponent(newButton))
	{
		return nullptr;
	}

	if (newButton->GetRenderComponent() != nullptr)
	{
		newButton->GetRenderComponent()->SetSpriteTexture(GuiTheme::GetGuiTheme()->ButtonSingleSpriteBackground.Get());
	}

	newButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
	newButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, TestMenuButtonList, HandleButtonReleased, void, ButtonComponent*));
	ButtonList.push_back(newButton);

	return newButton;
}

void TestMenuButtonList::HandleButtonReleased (ButtonComponent* uiComponent)
{
	GuiLog.Log(LogCategory::LL_Debug, TXT("Button \"%s\" was released."), uiComponent->GetCaptionText());
}
SD_END

#endif