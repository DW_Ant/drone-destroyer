/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, GuiComponent, SD, EntityComponent)

void GuiComponent::InitProps ()
{
	Super::InitProps();

	PropagateStyle = true;
}

void GuiComponent::BeginObject ()
{
	Super::BeginObject();

	InitializeComponents();

	ApplyStyle(DString::EmptyString);
}

bool GuiComponent::AddComponent (EntityComponent* newComponent, bool bLogOnFail, bool bDeleteOnFail)
{
	bool success = Super::AddComponent(newComponent, bLogOnFail, bDeleteOnFail);
	if (success && PropagateStyle && !StyleName.IsEmpty())
	{
		GuiComponent* guiComp = dynamic_cast<GuiComponent*>(newComponent);
		if (guiComp != nullptr)
		{
			guiComp->ApplyStyle(StyleName);
		}
	}

	return success;
}

void GuiComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	CHECK(objTemplate != this) //It's pointless for a GuiComponent to copy properties from self.

	const GuiComponent* uiTemplate = dynamic_cast<const GuiComponent*>(objTemplate);
	if (uiTemplate != nullptr)
	{
		CopyPlanarTransform(uiTemplate); //Copy transform data
	}
}

bool GuiComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	//Owner must implement the PlanarTransform.
	return (Super::CanBeAttachedTo(ownerCandidate) && dynamic_cast<const PlanarTransform*>(ownerCandidate) != nullptr);
}

void GuiComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	const PlanarTransform* ownerTransform = dynamic_cast<const PlanarTransform*>(newOwner);
	CHECK(ownerTransform != nullptr) //CanBeAttachedTo should have prevented this.
	SetRelativeTo(ownerTransform);
}

void GuiComponent::ApplyStyle (const DString& styleName)
{
	if (!styleName.IsEmpty() && styleName == StyleName)
	{
		return; //Already using this style
	}

	GuiEngineComponent* guiEngine = GuiEngineComponent::Find();
	CHECK(guiEngine != nullptr)

	StyleName = DString::EmptyString;
	GuiTheme* curTheme = guiEngine->GetGuiTheme();
	if (curTheme == nullptr)
	{
		return;
	}

	const GuiTheme::SStyleInfo* defaultStyle = curTheme->GetDefaultStyle();
	const GuiTheme::SStyleInfo* style = defaultStyle;
	if (!styleName.IsEmpty())
	{
		style = curTheme->FindStyle(styleName);
		if (style == nullptr) //Fallback to default
		{
			style = defaultStyle;
		}
	}

	if (style != nullptr)
	{
		const GuiComponent* uiTemplate = GuiTheme::FindTemplate(style, StaticClass());
		if (uiTemplate != nullptr)
		{
			CopyPropertiesFrom(uiTemplate);
		}

		if (style != defaultStyle)
		{
			StyleName = styleName;
#if 0 //Don't this this since components attached after this component will revert settings.
			for (ComponentIterator iter(this, true); iter.GetSelectedComponent() != nullptr; ++iter)
			{
				if (GuiComponent* gui = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent()))
				{
					gui->ApplyStyle(StyleName);
				}
			}
#endif
		}
	}
}

bool GuiComponent::IsOutermostGuiComponent () const
{
	EntityComponent* nextOwner = dynamic_cast<EntityComponent*>(GetOwner());
	while (nextOwner != nullptr)
	{
		if (dynamic_cast<GuiComponent*>(nextOwner) != nullptr)
		{
			return false; //Another GuiComponent is closer to the entity than this component
		}

		nextOwner = dynamic_cast<EntityComponent*>(nextOwner->GetOwner());
	}

	return true; //No owning components are Gui Components
}

bool GuiComponent::IsInnermostGuiComponent (bool bOnlyImmediateComponents) const
{
	for (ComponentIterator iter(this, !bOnlyImmediateComponents); iter.GetSelectedComponent() != nullptr; iter++)
	{
		if (dynamic_cast<GuiComponent*>(iter.GetSelectedComponent()) != nullptr)
		{
			return false;
		}
	}

	return true;
}

bool GuiComponent::ProcessInput (const sf::Event& evnt)
{
	return ProcessInput_Implementation(evnt, false);
}

bool GuiComponent::ProcessText (const sf::Event& evnt)
{
	return ProcessText_Implementation(evnt, false);
}

void GuiComponent::ProcessMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	if (!AcceptsMouseEvents(sfmlEvent.x, sfmlEvent.y))
	{
		return;
	}

	for (UINT_TYPE i = 0; i < ReadComponents().size(); i++)
	{
		GuiComponent* guiComponent = dynamic_cast<GuiComponent*>(ReadComponents().at(i).Get());
		if (guiComponent != nullptr)
		{
			guiComponent->ProcessMouseMove(mouse, sfmlEvent, deltaMove);
		}
	}

	ExecuteMouseMove(mouse, sfmlEvent, deltaMove);
}

bool GuiComponent::ProcessMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	return ProcessMouseClick_Implementation(mouse, sfmlEvent, eventType, false);
}

bool GuiComponent::ProcessMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	return ProcessMouseWheelMove_Implementation(mouse, sfmlEvent, false);
}

void GuiComponent::InitializeComponents ()
{
	//Noop
}

bool GuiComponent::ProcessInput_Implementation (const sf::Event& evnt, bool consumedEvnt)
{
	if (!AcceptsInputEvent(evnt))
	{
		return false;
	}

	//Invoke children before parent since the children are drawn over the parent.
	for (size_t i = 0; i < ReadComponents().size(); i++)
	{
		GuiComponent* subComponent = dynamic_cast<GuiComponent*>(ReadComponents().at(i).Get());
		if (subComponent != nullptr)
		{
			subComponent->ExecuteInput(evnt);
			if (!consumedEvnt && subComponent->ProcessInput(evnt))
			{
				consumedEvnt = true;
			}
		}
	}

	ExecuteInput(evnt);
	return (consumedEvnt || ExecuteConsumableInput(evnt));
}

bool GuiComponent::ProcessText_Implementation (const sf::Event& evnt, bool consumedEvnt)
{
	if (!AcceptsInputEvent(evnt))
	{
		return false;
	}

	//Invoke children before parent since the children are drawn over the parent.
	for (size_t i = 0; i < ReadComponents().size(); i++)
	{
		GuiComponent* subComponent = dynamic_cast<GuiComponent*>(ReadComponents().at(i).Get());
		if (subComponent != nullptr)
		{
			subComponent->ExecuteText(evnt);
			if (!consumedEvnt && subComponent->ProcessText(evnt))
			{
				consumedEvnt = true;
			}
		}
	}

	ExecuteText(evnt);
	return (consumedEvnt || ExecuteConsumableText(evnt));
}

bool GuiComponent::ProcessMouseClick_Implementation (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType, bool consumedEvent)
{
	if (!AcceptsMouseEvents(sfmlEvent.x, sfmlEvent.y))
	{
		return false;
	}

	//Invoke children before parent since the children are drawn over the parent.
	for (UINT_TYPE i = 0; i < ReadComponents().size(); i++)
	{
		GuiComponent* subComponent = dynamic_cast<GuiComponent*>(ReadComponents().at(i).Get());
		if (subComponent != nullptr)
		{
			subComponent->ExecuteMouseClick(mouse, sfmlEvent, eventType); //always invoke PreMouseClick regardless if the event was consumed.
			if (!consumedEvent && subComponent->ProcessMouseClick(mouse, sfmlEvent, eventType))
			{
				consumedEvent = true;
			}
		}
	}

	ExecuteMouseClick(mouse, sfmlEvent, eventType);
	return (consumedEvent || ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType));
}

bool GuiComponent::ProcessMouseWheelMove_Implementation (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent, bool consumedEvent)
{
	if (!AcceptsMouseEvents(sfmlEvent.x, sfmlEvent.y))
	{
		return false;
	}

	for (UINT_TYPE i = 0; i < ReadComponents().size(); i++)
	{
		GuiComponent* subComponent = dynamic_cast<GuiComponent*>(ReadComponents().at(i).Get());
		if (subComponent != nullptr)
		{
			subComponent->ExecuteMouseWheelMove(mouse, sfmlEvent);
			if (!consumedEvent && subComponent->ProcessMouseWheelMove(mouse, sfmlEvent))
			{
				consumedEvent = true;
			}
		}
	}

	ExecuteMouseWheelMove(mouse, sfmlEvent);
	return (consumedEvent || ExecuteConsumableMouseWheelMove(mouse, sfmlEvent));
}

void GuiComponent::ExecuteInput (const sf::Event& evnt)
{
	//Noop
}

void GuiComponent::ExecuteText (const sf::Event& evnt)
{
	//Noop
}

void GuiComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	//Noop
}

void GuiComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	//Noop
}

void GuiComponent::ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	//Noop
}

bool GuiComponent::ExecuteConsumableInput (const sf::Event& evnt)
{
	return false;
}

bool GuiComponent::ExecuteConsumableText (const sf::Event& evnt)
{
	return false;
}

bool GuiComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	return false;
}

bool GuiComponent::ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	return false;
}

bool GuiComponent::AcceptsInputEvent (const sf::Event& evnt) const
{
	return true;
}

bool GuiComponent::AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const
{
	return true;
}

void GuiComponent::HandleSizeChange ()
{
	//Noop
}

void GuiComponent::HandlePositionChange (const Vector2& deltaMove)
{
	//Noop
}
SD_END