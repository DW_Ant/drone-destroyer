/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScrollbarComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, ScrollbarComponent, SD, GuiComponent)

void ScrollbarComponent::InitProps ()
{
	Super::InitProps();

	InitialPanDistance = 24.f;
	ShiftSpeedMultiplier = 2.f;
	AltSpeedMultiplier = 0.5f;
	ScrollSpeed = 450.f;
	TravelTrackSpeed = 1500.f;
	WheelSpeedAccumulationRate = 24.f;
	AnchorPanSpeed.Min = 4.f;
	AnchorPanSpeed.Max = 2048.f;
	AnchorDistanceLimits.Min = 8.f;
	AnchorDistanceLimits.Max = 512.f;
	MinToggleTime = 0.25f;
	HoldToDragTime = 0.5f;
	ZoomRange.Min = 0.1f;
	ZoomRange.Max = 10.f;
	ZoomSnapValues = {0.25f, 0.5f, 0.75f};
	ZoomSnapTolerance = 0.1f;
	ZoomSpeed = 0.25f;
	ZoomSensitivity = 0.1f;

	ViewedObject = nullptr;
	FrameTexture = nullptr;
	FrameCamera = nullptr;
	FrameSpriteTransform = nullptr;
	FrameSprite = nullptr;
	ScrollUpButton = nullptr;
	ScrollDownButton = nullptr;
	ScrollLeftButton = nullptr;
	ScrollRightButton = nullptr;
	VerticalTrackTransform = nullptr;
	HorizontalTrackTransform = nullptr;
	VerticalTrack = nullptr;
	HorizontalTrack = nullptr;
	TravelTrackTransform = nullptr;
	TravelTrack = nullptr;
	VerticalThumbTransform = nullptr;
	HorizontalThumbTransform = nullptr;
	VerticalThumb = nullptr;
	HorizontalThumb = nullptr;
	MiddleMouseAnchorTransform = nullptr;
	MiddleMouseAnchor = nullptr;
	ZoomButton = nullptr;
	ZoomFeedbackTransform = nullptr;
	ZoomLabel = nullptr;
	Mouse = nullptr;
	Tick = nullptr;

	AnchorPosition = Vector2(-1.f, -1.f);
	AnchoredMouseIcon = nullptr;
	AnchorIconRefreshTimeInterval = 0.1f;
	AnchorIconTime = -1.f;
	StartAnchorTime = 1.f;
	HideControlsWhenFull = false;
	EnabledTrackColor = Color(64, 64, 64, 255);
	EnabledThumbColor = Color(150, 150, 150, 255);
	DisabledThumbColor = Color(48, 48, 48, 255);
	ThumbDragColor = Color(128, 128, 128, 255);
	ZoomEnabled = true;
	HorizontalControlState = CS_Uninitialized;
	VerticalControlState = CS_Uninitialized;
	CameraPanVelocity = Vector2::ZeroVector;
	CameraDestination = Vector2(-1.f, -1.f);
	DesiredZoom = -1.f;
	TravelTrackDirection = TTD_None;
	OriginalCamPanVelocity = Vector2::ZeroVector;
	TransformDetectionThreshold = 2.f;

	ScrollbarThickness = 18.f;
	IsHoldingScrollButton = false;
	VerticalThumbHoldOffset = -1.f;
	HorizontalThumbHoldOffset = -1.f;
	SettingMousePosition = false;

	CamLatestPos = Vector2(-1.f, -1.f);
	SpriteLatestSize = Vector2(-1.f, -1.f);
	ObjLatestSize = Vector2(-1.f, -1.f);
	CamLatestZoom = -1.f;
}

void ScrollbarComponent::BeginObject ()
{
	Super::BeginObject();

	GuiTheme* localTheme = GuiTheme::GetGuiTheme();
	CHECK(localTheme != nullptr)

	FrameTexture = RenderTexture::CreateObject();
	FrameTexture->CreateResource(1024, 1024, false);

	FrameCamera = PlanarCamera::CreateObject();
	CHECK(FrameCamera != nullptr)
	FrameCamera->OnZoomChanged.RegisterHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleZoomChanged, void, FLOAT));

	//Most of the cases the Scrollbar will be rendering another GuiEntity.  Create a GuiDrawLayer.
	//If there's a need to use a non-GuiEntity for the viewed object, then the user can edit the draw layer.
	GuiDrawLayer* guiDrawLayer = GuiDrawLayer::CreateObject();
	guiDrawLayer->SetDrawPriority(GuiDrawLayer::MAIN_OVERLAY_PRIORITY);
	FrameTexture->EditDrawLayers().push_back(RenderTarget::SDrawLayerCamera(guiDrawLayer, FrameCamera.Get()));
	FrameTexture->SetDestroyCamerasOnDestruction(true);
	FrameTexture->SetDestroyDrawLayersOnDestruction(true);

	FrameSpriteTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(FrameSpriteTransform.Get()))
	{
		if (HideControlsWhenFull)
		{
			//Sprite should fill entire component
			FrameSpriteTransform->SetAnchorLeft(0.f);
			FrameSpriteTransform->SetAnchorTop(0.f);
			FrameSpriteTransform->SetAnchorRight(-1.f);
			FrameSpriteTransform->SetAnchorBottom(-1.f);
			FrameSpriteTransform->SetSize(1.f, 1.f);
		}
		else
		{
			//Set sprite to not hide under scrollbars since the scrollbars are always visible.
			FrameSpriteTransform->SetAnchorLeft(0.f);
			FrameSpriteTransform->SetAnchorTop(0.f);
			FrameSpriteTransform->SetAnchorRight(ScrollbarThickness);
			FrameSpriteTransform->SetAnchorBottom(ScrollbarThickness);
		}

		FrameSprite = SpriteComponent::CreateObject();
		if (FrameSpriteTransform->AddComponent(FrameSprite.Get()))
		{
			FrameSprite->SetSpriteTexture(FrameTexture.Get());
		}
	}

	//Scroll buttons
	{
		ScrollUpButton = ButtonComponent::CreateObject();
		if (AddComponent(ScrollUpButton.Get()))
		{
			ScrollUpButton->CaptionComponent->Destroy();
			ScrollUpButton->SetSize(ScrollbarThickness, ScrollbarThickness);

			//Snap top right
			ScrollUpButton->SetAnchorTop(0.f);
			ScrollUpButton->SetAnchorRight(0.f);
			ScrollUpButton->SetEnableFractionScaling(false);

			ScrollUpButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleScrollUpButtonPressed, void, ButtonComponent*));

			ScrollUpButton->GetRenderComponent()->SetSpriteTexture(localTheme->ScrollbarUpButton.Get());
		}

		ScrollDownButton = ButtonComponent::CreateObject();
		if (AddComponent(ScrollDownButton.Get()))
		{
			ScrollDownButton->CaptionComponent->Destroy();
			ScrollDownButton->SetSize(ScrollbarThickness, ScrollbarThickness);

			//Snap bottom right but leaving enough space for zoom button in corner
			ScrollDownButton->SetAnchorBottom(ScrollbarThickness);
			ScrollDownButton->SetAnchorRight(0.f);
			ScrollDownButton->SetEnableFractionScaling(false);

			ScrollDownButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleScrollDownButtonPressed, void, ButtonComponent*));

			ScrollDownButton->GetRenderComponent()->SetSpriteTexture(localTheme->ScrollbarDownButton.Get());
		}

		ScrollLeftButton = ButtonComponent::CreateObject();
		if (AddComponent(ScrollLeftButton.Get()))
		{
			ScrollLeftButton->CaptionComponent->Destroy();
			ScrollLeftButton->SetSize(ScrollbarThickness, ScrollbarThickness);

			//Snap bottom left
			ScrollLeftButton->SetAnchorBottom(0.f);
			ScrollLeftButton->SetAnchorLeft(0.f);
			ScrollLeftButton->SetEnableFractionScaling(false);

			ScrollLeftButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleScrollLeftButtonPressed, void, ButtonComponent*));

			ScrollLeftButton->GetRenderComponent()->SetSpriteTexture(localTheme->ScrollbarLeftButton.Get());
		}

		ScrollRightButton = ButtonComponent::CreateObject();
		if (AddComponent(ScrollRightButton.Get()))
		{
			ScrollRightButton->CaptionComponent->Destroy();
			ScrollRightButton->SetSize(ScrollbarThickness, ScrollbarThickness);

			//Snap bottom right but leaving enough space for zoom button in corner
			ScrollRightButton->SetAnchorBottom(0.f);
			ScrollRightButton->SetAnchorRight(ScrollbarThickness);
			ScrollRightButton->SetEnableFractionScaling(false);

			ScrollRightButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleScrollRightButtonPressed, void, ButtonComponent*));

			ScrollRightButton->GetRenderComponent()->SetSpriteTexture(localTheme->ScrollbarRightButton.Get());
		}
	}

	//Track related components
	{
		VerticalTrackTransform = PlanarTransformComponent::CreateObject();
		if (AddComponent(VerticalTrackTransform))
		{
			VerticalTrackTransform->SetSize(ScrollbarThickness, ScrollbarThickness); //Anchor properties will set the height.
			VerticalTrackTransform->SetAnchorTop(ScrollbarThickness);
			VerticalTrackTransform->SetAnchorRight(0.f);
			VerticalTrackTransform->SetAnchorBottom(ScrollbarThickness * 2.f);
			VerticalTrackTransform->SetEnableFractionScaling(false);
		}
		else
		{
			//Non-DPointers must be set to null if destroyed.
			VerticalTrackTransform = nullptr;
		}

		HorizontalTrackTransform = PlanarTransformComponent::CreateObject();
		if (AddComponent(HorizontalTrackTransform))
		{
			HorizontalTrackTransform->SetSize(ScrollbarThickness, ScrollbarThickness); //Anchor properties will set the width.
			HorizontalTrackTransform->SetAnchorLeft(ScrollbarThickness);
			HorizontalTrackTransform->SetAnchorRight(ScrollbarThickness * 2.f);
			HorizontalTrackTransform->SetAnchorBottom(0.f);
			HorizontalTrackTransform->SetEnableFractionScaling(false);
		}
		else
		{
			//Non-DPointers must be set to null if destroyed.
			HorizontalTrackTransform = nullptr;
		}

		CHECK(VerticalTrackTransform != nullptr)
		CHECK(HorizontalTrackTransform != nullptr)

		VerticalTrack = SolidColorRenderComponent::CreateObject();
		if (VerticalTrackTransform->AddComponent(VerticalTrack.Get()))
		{
			VerticalTrack->SolidColor = EnabledTrackColor;
		}

		HorizontalTrack = SolidColorRenderComponent::CreateObject();
		if (HorizontalTrackTransform->AddComponent(HorizontalTrack.Get()))
		{
			HorizontalTrack->SolidColor = EnabledTrackColor;
		}

		TravelTrackTransform = PlanarTransformComponent::CreateObject();
		if (AddComponent(TravelTrackTransform))
		{
			TravelTrackTransform->SetEnableFractionScaling(false);
			//The travel track transform is computed based on mouse movements and thumb position.
		}
		else
		{
			//Non DPointers must be set to null when destroyed.
			TravelTrackTransform = nullptr;
		}

		CHECK(TravelTrackTransform != nullptr)

		TravelTrack = SolidColorRenderComponent::CreateObject();
		if (TravelTrackTransform->AddComponent(TravelTrack.Get()))
		{
			TravelTrack->SetVisibility(false);
			TravelTrack->SolidColor = Color(48, 48, 48, 255);
		}

		VerticalThumbTransform = PlanarTransformComponent::CreateObject();
		if (VerticalTrackTransform->AddComponent(VerticalThumbTransform))
		{
			//Fill the track
			VerticalThumbTransform->SetSize(1.f, 1.f);
			VerticalThumbTransform->SetPosition(0.f, 0.f);
		}
		else
		{
			VerticalThumbTransform = nullptr;
		}

		HorizontalThumbTransform = PlanarTransformComponent::CreateObject();
		if (HorizontalTrackTransform->AddComponent(HorizontalThumbTransform))
		{
			//Fill the track
			HorizontalThumbTransform->SetSize(1.f, 1.f);
			HorizontalThumbTransform->SetPosition(0.f, 0.f);
		}
		else
		{
			HorizontalThumbTransform = nullptr;
		}

		CHECK(VerticalThumbTransform != nullptr)
		CHECK(HorizontalThumbTransform != nullptr)

		VerticalThumb = SolidColorRenderComponent::CreateObject();
		if (VerticalThumbTransform->AddComponent(VerticalThumb.Get()))
		{
			VerticalThumb->SolidColor = EnabledThumbColor;
		}

		HorizontalThumb = SolidColorRenderComponent::CreateObject();
		if (HorizontalThumbTransform->AddComponent(HorizontalThumb.Get()))
		{
			HorizontalThumb->SolidColor = EnabledThumbColor;
		}
	}

	MiddleMouseAnchorTransform = PlanarTransformComponent::CreateObject();
	if (AddComponent(MiddleMouseAnchorTransform))
	{
		MiddleMouseAnchorTransform->SetSize(32.f, 32.f);
		MiddleMouseAnchorTransform->SetPivot(MiddleMouseAnchorTransform->ReadSize() * 0.5f);
	}
	else
	{
		MiddleMouseAnchorTransform = nullptr;
	}

	CHECK(MiddleMouseAnchorTransform != nullptr)
	MiddleMouseAnchor = SpriteComponent::CreateObject();
	if (MiddleMouseAnchorTransform->AddComponent(MiddleMouseAnchor.Get()))
	{
		MiddleMouseAnchor->SetVisibility(false);
		MiddleMouseAnchor->SetSpriteTexture(localTheme->ScrollbarMiddleMouseScrollAnchor.Get());
	}

	//Zoom related components
	{
		ZoomButton = ButtonComponent::CreateObject();
		if (AddComponent(ZoomButton.Get()))
		{
			ZoomButton->SetSize(ScrollbarThickness, ScrollbarThickness);

			ZoomButton->SetVisibility(IsZoomEnabled());

			//Snap bottom right
			ZoomButton->SetAnchorRight(0.f);
			ZoomButton->SetAnchorBottom(0.f);
			if (ZoomButton->CaptionComponent != nullptr && ZoomButton->CaptionComponent->GetRenderComponent() != nullptr)
			{
				ZoomButton->SetCaptionText(FrameCamera->GetZoom().ToFormattedString(1, 2, FLOAT::eTruncateMethod::TM_RoundUp));
				ZoomButton->CaptionComponent->SetVerticalAlignment(LabelComponent::VA_Center);
				ZoomButton->CaptionComponent->SetCharacterSize(8);
				ZoomButton->CaptionComponent->GetRenderComponent()->SetFontColor(sf::Color::Black);
			}

			ZoomButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleZoomButtonPressed, void, ButtonComponent*));
			ZoomButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleZoomButtonReleased, void, ButtonComponent*));
			ZoomButton->SetButtonReleasedRightClickHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleZoomButtonRightClickReleased, void, ButtonComponent*));

			ZoomButton->GetRenderComponent()->SetSpriteTexture(localTheme->ScrollbarZoomButton.Get());
		}

		ZoomFeedbackTransform = PlanarTransformComponent::CreateObject();
		if (AddComponent(ZoomFeedbackTransform))
		{
			ZoomFeedbackTransform->SetSize(64.f, 64.f);

			//Snap to top left corner of zoom button
			ZoomFeedbackTransform->SetAnchorRight(ScrollbarThickness);
			ZoomFeedbackTransform->SetAnchorBottom(ScrollbarThickness);
			ZoomFeedbackTransform->SetVisibility(false);
		}
		else
		{
			ZoomFeedbackTransform = nullptr;
		}

		CHECK(ZoomFeedbackTransform != nullptr)

		ZoomLabel = LabelComponent::CreateObject();
		if (ZoomFeedbackTransform->AddComponent(ZoomLabel.Get()))
		{
			ZoomLabel->SetAutoRefresh(false);
			ZoomLabel->SetWrapText(false);
			ZoomLabel->SetClampText(true);
			ZoomLabel->SetText(TXT("%s"));
			ZoomLabel->SetVerticalAlignment(LabelComponent::eVerticalAlignment::VA_Center);
			ZoomLabel->SetHorizontalAlignment(LabelComponent::eHorizontalAlignment::HA_Center);
			ZoomLabel->SetAutoRefresh(true);
		}
	}

	Tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(Tick.Get()))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleTick, void, FLOAT));
	}
}

void ScrollbarComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ScrollbarComponent* scrollbarTemplate = dynamic_cast<const ScrollbarComponent*>(objTemplate);
	if (scrollbarTemplate != nullptr)
	{
		InitialPanDistance = scrollbarTemplate->InitialPanDistance;
		ShiftSpeedMultiplier = scrollbarTemplate->ShiftSpeedMultiplier;
		AltSpeedMultiplier = scrollbarTemplate->AltSpeedMultiplier;
		ScrollSpeed = scrollbarTemplate->ScrollSpeed;
		TravelTrackSpeed = scrollbarTemplate->TravelTrackSpeed;
		WheelSpeedAccumulationRate = scrollbarTemplate->WheelSpeedAccumulationRate;
		AnchorPanSpeed = scrollbarTemplate->AnchorPanSpeed;
		MinToggleTime = scrollbarTemplate->MinToggleTime;
		HoldToDragTime = scrollbarTemplate->HoldToDragTime;
		ZoomRange = scrollbarTemplate->ZoomRange;
		ZoomSnapValues = scrollbarTemplate->ZoomSnapValues;
		ZoomSnapTolerance = scrollbarTemplate->ZoomSnapTolerance;
		ZoomSpeed = scrollbarTemplate->ZoomSpeed;
		ZoomSensitivity = scrollbarTemplate->ZoomSensitivity;

		bool createdObj;
		//Up button
		ScrollUpButton = ReplaceTargetWithObjOfMatchingClass(ScrollUpButton.Get(), scrollbarTemplate->ScrollUpButton.Get(), OUT createdObj);
		if (createdObj)
		{
			AddComponent(ScrollUpButton.Get());
		}

		if (ScrollUpButton != nullptr)
		{
			ScrollUpButton->CopyPropertiesFrom(scrollbarTemplate->ScrollUpButton.Get());
		}

		//Down button
		ScrollDownButton = ReplaceTargetWithObjOfMatchingClass(ScrollDownButton.Get(), scrollbarTemplate->ScrollDownButton.Get(), OUT createdObj);
		if (createdObj)
		{
			AddComponent(ScrollDownButton.Get());
		}

		if (ScrollDownButton != nullptr)
		{
			ScrollDownButton->CopyPropertiesFrom(scrollbarTemplate->ScrollDownButton.Get());
		}

		//Left button
		ScrollLeftButton = ReplaceTargetWithObjOfMatchingClass(ScrollLeftButton.Get(), scrollbarTemplate->ScrollLeftButton.Get(), OUT createdObj);
		if (createdObj)
		{
			AddComponent(ScrollLeftButton.Get());
		}

		if (ScrollLeftButton != nullptr)
		{
			ScrollLeftButton->CopyPropertiesFrom(scrollbarTemplate->ScrollLeftButton.Get());
		}

		//Right button
		ScrollRightButton = ReplaceTargetWithObjOfMatchingClass(ScrollRightButton.Get(), scrollbarTemplate->ScrollRightButton.Get(), OUT createdObj);
		if (createdObj)
		{
			AddComponent(ScrollRightButton.Get());
		}

		if (ScrollRightButton != nullptr)
		{
			ScrollRightButton->CopyPropertiesFrom(scrollbarTemplate->ScrollRightButton.Get());
		}

		//Vertical Track
		VerticalTrack = ReplaceTargetWithObjOfMatchingClass(VerticalTrack.Get(), scrollbarTemplate->VerticalTrack.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(VerticalTrackTransform != nullptr)
			VerticalTrackTransform->AddComponent(VerticalTrack.Get());
		}

		if (VerticalTrack != nullptr)
		{
			VerticalTrack->CopyPropertiesFrom(scrollbarTemplate->VerticalTrack.Get());
		}

		//Horizontal Track
		HorizontalTrack = ReplaceTargetWithObjOfMatchingClass(HorizontalTrack.Get(), scrollbarTemplate->HorizontalTrack.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(HorizontalTrackTransform != nullptr)
			HorizontalTrackTransform->AddComponent(HorizontalTrack.Get());
		}

		if (HorizontalTrack != nullptr)
		{
			HorizontalTrack->CopyPropertiesFrom(scrollbarTemplate->HorizontalTrack.Get());
		}

		//Travel Track
		TravelTrack = ReplaceTargetWithObjOfMatchingClass(TravelTrack.Get(), scrollbarTemplate->TravelTrack.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(TravelTrackTransform != nullptr)
			TravelTrackTransform->AddComponent(TravelTrack.Get());
		}

		if (TravelTrack != nullptr)
		{
			TravelTrack->CopyPropertiesFrom(scrollbarTemplate->TravelTrack.Get());
		}

		//Vertical Thumb
		VerticalThumb = ReplaceTargetWithObjOfMatchingClass(VerticalThumb.Get(), scrollbarTemplate->VerticalThumb.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(VerticalThumbTransform != nullptr)
			VerticalThumbTransform->AddComponent(VerticalThumb.Get());
		}

		if (VerticalThumb != nullptr)
		{
			VerticalThumb->CopyPropertiesFrom(scrollbarTemplate->VerticalThumb.Get());
		}

		//Horizontal Thumb
		HorizontalThumb = ReplaceTargetWithObjOfMatchingClass(HorizontalThumb.Get(), scrollbarTemplate->HorizontalThumb.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(HorizontalThumbTransform != nullptr)
			HorizontalThumbTransform->AddComponent(HorizontalThumb.Get());
		}

		if (HorizontalThumb != nullptr)
		{
			HorizontalThumb->CopyPropertiesFrom(scrollbarTemplate->HorizontalThumb.Get());
		}

		//Middle Mouse Anchor
		MiddleMouseAnchor = ReplaceTargetWithObjOfMatchingClass(MiddleMouseAnchor.Get(), scrollbarTemplate->MiddleMouseAnchor.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(MiddleMouseAnchorTransform != nullptr)
			MiddleMouseAnchorTransform->AddComponent(MiddleMouseAnchor.Get());
		}

		if (MiddleMouseAnchor != nullptr)
		{
			MiddleMouseAnchor->CopyPropertiesFrom(scrollbarTemplate->MiddleMouseAnchor.Get());
		}

		//Zoom Button
		ZoomButton = ReplaceTargetWithObjOfMatchingClass(ZoomButton.Get(), scrollbarTemplate->ZoomButton.Get(), OUT createdObj);
		if (createdObj)
		{
			AddComponent(ZoomButton.Get());
		}

		if (ZoomButton != nullptr)
		{
			ZoomButton->CopyPropertiesFrom(scrollbarTemplate->ZoomButton.Get());
		}

		//Zoom Label
		ZoomLabel = ReplaceTargetWithObjOfMatchingClass(ZoomLabel.Get(), scrollbarTemplate->ZoomLabel.Get(), OUT createdObj);
		if (createdObj)
		{
			CHECK(ZoomFeedbackTransform != nullptr)
			ZoomFeedbackTransform->AddComponent(ZoomLabel.Get());
		}

		if (ZoomLabel != nullptr)
		{
			ZoomLabel->CopyPropertiesFrom(scrollbarTemplate->ZoomLabel.Get());
		}

		AnchorIconRefreshTimeInterval = scrollbarTemplate->AnchorIconRefreshTimeInterval;
		SetHideControlsWhenFull(scrollbarTemplate->HideControlsWhenFull);
		EnabledTrackColor = scrollbarTemplate->EnabledTrackColor;
		EnabledThumbColor = scrollbarTemplate->EnabledThumbColor;
		DisabledThumbColor = scrollbarTemplate->DisabledThumbColor;
		ThumbDragColor = scrollbarTemplate->ThumbDragColor;
		SetZoomEnabled(scrollbarTemplate->IsZoomEnabled());
		HorizontalControlState = scrollbarTemplate->HorizontalControlState;
		VerticalControlState = scrollbarTemplate->VerticalControlState;
		TransformDetectionThreshold = scrollbarTemplate->TransformDetectionThreshold;

		CamLatestPos = scrollbarTemplate->CamLatestPos;
		SpriteLatestSize = scrollbarTemplate->SpriteLatestSize;
		ObjLatestSize = scrollbarTemplate->ObjLatestSize;
		CamLatestZoom = scrollbarTemplate->CamLatestZoom;
	}
}

bool ScrollbarComponent::ProcessInput (const sf::Event& evnt)
{
	if (IsVisible() && ViewedObject != nullptr)
	{
		return ViewedObject->ExecuteScrollableInterfaceInput(evnt);
	}

	return false;
}

bool ScrollbarComponent::ProcessText (const sf::Event& evnt)
{
	if (IsVisible() && ViewedObject != nullptr)
	{
		return ViewedObject->ExecuteScrollableInterfaceText(evnt);
	}

	return false;
}

void ScrollbarComponent::ProcessMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	//Relay mouse event to ViewedObject
	const Vector2 mousePosition(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));

	if (IsVisible())
	{
		if (ViewedObject != nullptr && FrameSpriteTransform->IsWithinBounds(mousePosition))
		{
			const Vector2 frameMousePos = CalcViewedFramePosition(mousePosition);

			sf::Event::MouseMoveEvent frameMouseMove;
			frameMouseMove.x = frameMousePos.X.ToINT().ToInt32();
			frameMouseMove.y = frameMousePos.Y.ToINT().ToInt32();
			ViewedObject->ExecuteScrollableInterfaceMouseMove(mouse, frameMouseMove, deltaMove);
		}
	}

	//Pass to GUIComponent::ProcessMouseMove to handle the GUIComponent input chain (such as ExecuteMouseMove)
	Super::ProcessMouseMove(mouse, sfmlEvent, deltaMove);
}

bool ScrollbarComponent::ProcessMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	bool consumedInput = false;

	//Relay mouse event to ViewedObject.
	if (IsVisible())
	{
		const Vector2 mousePosition(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
		if (ViewedObject != nullptr && FrameSpriteTransform->IsWithinBounds(mousePosition))
		{
			Vector2 frameMousePos = CalcViewedFramePosition(mousePosition);

			sf::Event::MouseButtonEvent frameEvent;
			frameEvent.button = sfmlEvent.button;
			frameEvent.x = frameMousePos.X.ToINT().ToInt32();
			frameEvent.y = frameMousePos.Y.ToINT().ToInt32();
			consumedInput = ViewedObject->ExecuteScrollableInterfaceMouseClick(mouse, frameEvent, eventType);
		}
	}

	//Calling ProcessMouseClick_Implementation is intended.  We're overriding the consumedInput flag to prevent consumable input
	//events from being invoked if the ViewedObject already consumed input.
	return Super::ProcessMouseClick_Implementation(mouse, sfmlEvent, eventType, consumedInput);
}

bool ScrollbarComponent::ProcessMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	bool consumedInput = false;

	if (IsVisible())
	{
		//Relay mouse event to ViewedObject
		const Vector2 mousePosition(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
		if (ViewedObject != nullptr && FrameSpriteTransform->IsWithinBounds(mousePosition))
		{
			//Relay mouse event to the ViewedObject
			Vector2 framePosition = CalcViewedFramePosition(mousePosition);
		
			sf::Event::MouseWheelScrollEvent frameEvent;
			frameEvent.wheel = sfmlEvent.wheel;
			frameEvent.delta = sfmlEvent.delta;
			frameEvent.x = framePosition.X.ToINT().ToInt32();
			frameEvent.y = framePosition.Y.ToINT().ToInt32();
			consumedInput = ViewedObject->ExecuteScrollableInterfaceMouseWheelMove(mouse, frameEvent);
		}
	}

	//Calling ProcessMouseWheelMove_Implementation is intended.  We're overriding the consumedInput flag
	//to prevent consumable input events from being invoked if the ViewedObject already consumed input.
	return Super::ProcessMouseWheelMove_Implementation(mouse, sfmlEvent, consumedInput);
}

void ScrollbarComponent::Destroyed ()
{
	DestroyViewedObject();

	if (FrameCamera != nullptr)
	{
		FrameCamera->Destroy();
	}

	if (FrameTexture != nullptr)
	{
		FrameTexture->Destroy();
	}

	if (Mouse != nullptr && AnchorPosition.X >= 0.f && AnchorPosition.Y >= 0.f)
	{
		Mouse->RemoveIconOverride(SDFUNCTION_2PARAM(this, ScrollbarComponent, HandleMouseIconOverride, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
		Mouse->SetSize(OriginalMouseSize);
	}

	Super::Destroyed();
}

void ScrollbarComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	CHECK(FrameCamera != nullptr)
	if (ViewedObject == nullptr || !IsVisible())
	{
		return;
	}

	Mouse = mouse;
	const Vector2 mousePosition(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));

	if (VerticalThumbHoldOffset >= 0.f)
	{
		//Vertical thumb is held.  Set the camera's vertical position.
		CHECK(VerticalTrackTransform != nullptr && VerticalThumbTransform != nullptr)

		//Identify the min/max values the camera can be placed vertically.
		Vector2 minPos;
		Vector2 maxPos;
		GetCamPositionLimits(OUT minPos, OUT maxPos);
		
		//Identify a value between 0 and 1 where the mouse is positioned relative to the vertial bar.
		FLOAT translationAlpha = (mousePosition.Y - VerticalThumbHoldOffset - VerticalTrackTransform->ReadCachedAbsPosition().Y) / (VerticalTrackTransform->ReadCachedAbsSize().Y - VerticalThumbTransform->ReadCachedAbsSize().Y);
		translationAlpha = Utils::Clamp<FLOAT>(translationAlpha, 0.f, 1.f);

		//Compute where the camera should be placed based on the translationAlpha.
		FLOAT vertPos = Utils::Lerp(translationAlpha, minPos.Y, maxPos.Y);

		FrameCamera->SetPosition(FrameCamera->ReadPosition().X, vertPos);
	}
	else if (HorizontalThumbHoldOffset >= 0.f)
	{
		//Horizontal thumb is held.  Set the camera's horizontal position.
		CHECK(HorizontalTrackTransform != nullptr && HorizontalThumbTransform != nullptr)

		//Identify the min/max values the camera can be placed horizontally.
		Vector2 minPos;
		Vector2 maxPos;
		GetCamPositionLimits(OUT minPos, OUT maxPos);

		//Identify a value between 0 and 1 where the mouse is positioned relative to the horizontal bar.
		FLOAT translationAlpha = (mousePosition.X - HorizontalThumbHoldOffset - HorizontalTrackTransform->ReadCachedAbsPosition().X) / (HorizontalTrackTransform->ReadCachedAbsSize().X - HorizontalThumbTransform->ReadCachedAbsSize().X);
		translationAlpha = Utils::Clamp<FLOAT>(translationAlpha, 0.f, 1.f);

		//Compute where the camera should be placed based on the translationAlpha.
		FLOAT horPos = Utils::Lerp(translationAlpha, minPos.X, maxPos.X);

		FrameCamera->SetPosition(horPos, FrameCamera->ReadPosition().Y);
	}

	if (AnchorPosition.X >= 0.f && AnchorPosition.Y >= 0.f)
	{
		//The middle mouse panning is active.  Adjust the camera velocity based on relative position to anchor.
		Vector2 relPos = mousePosition - AnchorPosition;

		//Compute X speed
		if (FLOAT::Abs(relPos.X) < AnchorDistanceLimits.Min)
		{
			CameraPanVelocity.X = 0.f;
		}
		else
		{
			//Compute the magnitude
			if (AnchorDistanceLimits.Min != AnchorDistanceLimits.Max)
			{
				FLOAT anchorRatio = Utils::Lerp(FLOAT::Abs(relPos.X) / AnchorDistanceLimits.Difference(), AnchorDistanceLimits.Min, AnchorDistanceLimits.Max);
				anchorRatio = Utils::Min(anchorRatio, AnchorPanSpeed.Max);
				anchorRatio /= AnchorDistanceLimits.Difference();
				CameraPanVelocity.X = Utils::Lerp(anchorRatio, AnchorPanSpeed.Min, AnchorPanSpeed.Max);
			}
			else
			{
				CameraPanVelocity.X = AnchorPanSpeed.Max; //No distance difference between min and max.  Assume max velocity.
			}

			if (relPos.X < 0.f)
			{
				//Reverse direction
				CameraPanVelocity.X *= -1.f;
			}
		}

		//Compute Y speed
		if (FLOAT::Abs(relPos.Y) < AnchorDistanceLimits.Min)
		{
			CameraPanVelocity.Y = 0.f;
		}
		else
		{
			//Compute the magnitude
			if (AnchorDistanceLimits.Min != AnchorDistanceLimits.Max)
			{
				FLOAT anchorRatio = Utils::Lerp(FLOAT::Abs(relPos.Y) / AnchorDistanceLimits.Difference(), AnchorDistanceLimits.Min, AnchorDistanceLimits.Max);
				anchorRatio = Utils::Min(anchorRatio, AnchorPanSpeed.Max);
				anchorRatio /= AnchorDistanceLimits.Difference();
				CameraPanVelocity.Y = Utils::Lerp(anchorRatio, AnchorPanSpeed.Min, AnchorPanSpeed.Max);
			}
			else
			{
				CameraPanVelocity.Y = AnchorPanSpeed.Max; //No distance difference between min and max.  Assume max velocity.
			}

			if (relPos.Y < 0.f)
			{
				//Reverse direction
				CameraPanVelocity.Y *= -1.f;
			}
		}
	}
}

void ScrollbarComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (!IsVisible())
	{
		return;
	}

	Vector2 mousePosition(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));

	if (eventType == sf::Event::MouseButtonReleased)
	{
		IsHoldingScrollButton = false;

		if (TravelTrackDirection != TTD_None)
		{
			//Release travel track movement
			TravelTrackDirection = TTD_None;
			CameraPanVelocity = Vector2::ZeroVector;

			if (TravelTrack != nullptr)
			{
				TravelTrack->SetVisibility(false);
			}
		}
		else if (VerticalThumbHoldOffset >= 0.f || HorizontalThumbHoldOffset >= 0.f)
		{
			//Release thumb movement
			VerticalThumbHoldOffset = -1.f;
			HorizontalThumbHoldOffset = -1.f;
			CameraPanVelocity = Vector2::ZeroVector;

			if (VerticalThumb != nullptr && VerticalThumbTransform != nullptr)
			{
				VerticalThumb->SolidColor = (VerticalThumbTransform->ReadSize().Y < 1.f) ? EnabledThumbColor : DisabledThumbColor;
			}

			if (HorizontalThumb != nullptr && HorizontalThumbTransform != nullptr)
			{
				HorizontalThumb->SolidColor = (HorizontalThumbTransform->ReadSize().X < 1.f) ? EnabledThumbColor : DisabledThumbColor;
			}
		}
		else if (AnchorPosition.X >= 0.f && AnchorPosition.Y >= 0.f)
		{
			Engine* localEngine = Engine::FindEngine();
			CHECK(localEngine != nullptr)

			//Identify if middle mouse should toggle or release.
			if (localEngine->GetElapsedTime() - StartAnchorTime > MinToggleTime)
			{
				//Held long enough.  Release anchor.
				AnchorPosition.X = -1.f;
				AnchorPosition.Y = -1.f;
				MiddleMouseAnchor->SetVisibility(false);

				CameraPanVelocity = Vector2::ZeroVector;
			}
			else
			{
				//Not held long enough.  It's now a toggle.  User must click middle mouse wheel again to release.
			}
		}
		else if (ZoomFeedbackTransform != nullptr && ZoomFeedbackTransform->IsVisible())
		{
			//Release zoom controls.
			ZoomFeedbackTransform->SetVisibility(false);
		}
	}
}

bool ScrollbarComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (!IsVisible())
	{
		return false;
	}

	Vector2 mousePosition = Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
	if (!IsWithinBounds(mousePosition))
	{
		return false;
	}

	if (sfmlEvent.button == sf::Mouse::Left && eventType == sf::Event::MouseButtonPressed)
	{
		//Thumbs take priority over the track.
		if (VerticalThumbTransform != nullptr && VerticalThumbTransform->ReadSize().Y < 1.f && VerticalThumbTransform->IsWithinBounds(mousePosition))
		{
			//User clicked on vertical thumb
			VerticalThumbHoldOffset = mousePosition.Y - VerticalThumbTransform->ReadCachedAbsPosition().Y;
			VerticalThumb->SolidColor = ThumbDragColor;
			CameraDestination = Vector2(-1.f, -1.f); //Interrupt camera destination since mouse is dictating movement.
			CameraPanVelocity = Vector2::ZeroVector;
			return true;
		}
		else if (HorizontalThumbTransform != nullptr && HorizontalThumbTransform->ReadSize().X < 1.f && HorizontalThumbTransform->IsWithinBounds(mousePosition))
		{
			//User clicked on horizontal thumb
			HorizontalThumbHoldOffset = mousePosition.X - HorizontalThumbTransform->ReadCachedAbsPosition().X;
			HorizontalThumb->SolidColor = ThumbDragColor;
			CameraDestination = Vector2(-1.f, -1.f); //Interrupt camera destination since mouse is dictating movement.
			CameraPanVelocity = Vector2::ZeroVector;
			return true;
		}
		//Test if the track is clicked.
		else if (VerticalTrackTransform != nullptr && VerticalThumbTransform != nullptr &&
			VerticalThumbTransform->ReadSize().Y < 1.f && VerticalTrackTransform->IsWithinBounds(mousePosition))
		{
			CHECK(FrameCamera != nullptr)

			//Vertical track is pressed
			TravelTrackDirection = (mousePosition.Y < VerticalThumbTransform->ReadCachedAbsPosition().Y) ? TTD_Up : TTD_Down;
			CameraPanVelocity = Vector2(0.f, TravelTrackSpeed);
			Vector2 initialPanDistMultiplier(0.f, 1.f);
			if (TravelTrackDirection == TTD_Up)
			{
				CameraPanVelocity.Y *= -1.f;
				initialPanDistMultiplier.Y *= -1.f;
			}

			OriginalCamPanVelocity = CameraPanVelocity;
			CameraDestination = FrameCamera->ReadPosition() + (initialPanDistMultiplier * InitialPanDistance);
			ClampCamPosition(OUT CameraDestination);
			TravelTrack->SetVisibility(true);

			//Set the travel track to be over the vertical bar.  The vertical position/scale is computed on Tick.
			TravelTrackTransform->SetPosition(VerticalTrackTransform->ReadPosition().X, TravelTrackTransform->ReadPosition().Y);
			TravelTrackTransform->SetSize(VerticalTrackTransform->ReadCachedAbsSize().X, TravelTrackTransform->ReadSize().Y);

			return true;
		}
		else if (HorizontalTrackTransform != nullptr && HorizontalThumbTransform != nullptr &&
			HorizontalThumbTransform->ReadSize().X < 1.f && HorizontalTrackTransform->IsWithinBounds(mousePosition))
		{
			CHECK(FrameCamera != nullptr)

			//Horizontal track is pressed
			TravelTrackDirection = (mousePosition.X < HorizontalThumbTransform->ReadCachedAbsPosition().X) ? TTD_Left : TTD_Right;
			CameraPanVelocity = Vector2(TravelTrackSpeed, 0.f);
			Vector2 initialPanDistMultiplier(1.f, 0.f);
			if (TravelTrackDirection == TTD_Left)
			{
				CameraPanVelocity.X *= -1.f;
				initialPanDistMultiplier.X *= -1.f;
			}

			OriginalCamPanVelocity = CameraPanVelocity;
			CameraDestination = FrameCamera->ReadPosition() + (initialPanDistMultiplier * InitialPanDistance);
			ClampCamPosition(OUT CameraDestination);
			TravelTrack->SetVisibility(true);

			//Set the travel track to be over the horizontal bar.  The horizontal position/scale is computed on Tick.
			TravelTrackTransform->SetPosition(TravelTrackTransform->ReadPosition().X, HorizontalTrackTransform->ReadPosition().Y);
			TravelTrackTransform->SetSize(TravelTrackTransform->ReadSize().X, HorizontalTrackTransform->ReadCachedAbsSize().Y);

			return true;
		}
	}

	if (sfmlEvent.button == sf::Mouse::Middle && eventType == sf::Event::MouseButtonPressed && IsScrollingEnabled())
	{
		//If not already anchored
		if (AnchorPosition.X < 0.f && AnchorPosition.Y < 0.f && FrameSpriteTransform->IsWithinBounds(mousePosition))
		{
			//Set mouse anchor
			AnchorPosition = mousePosition;

			Engine* localEngine = Engine::FindEngine();
			CHECK(localEngine != nullptr)
			StartAnchorTime = localEngine->GetElapsedTime();

			bool isAlreadyRegistered = mouse->IsIconOverrideRegistered(SDFUNCTION_2PARAM(this, ScrollbarComponent, HandleMouseIconOverride, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
			if (!isAlreadyRegistered)
			{
				//Update mouse icon
				AnchoredMouseIcon = CalculateAnchoredMouseIcon(mousePosition);
				AnchorIconTime = localEngine->GetElapsedTime();
				mouse->PushMouseIconOverride(AnchoredMouseIcon.Get(), SDFUNCTION_2PARAM(this, ScrollbarComponent, HandleMouseIconOverride, Texture*, MousePointer*, const sf::Event::MouseMoveEvent&));
			}

			if (MiddleMouseAnchor != nullptr)
			{
				MiddleMouseAnchor->SetVisibility(true);
			}

			if (MiddleMouseAnchorTransform != nullptr)
			{
				MiddleMouseAnchorTransform->SetPosition(mousePosition - ReadCachedAbsPosition());

				if (!isAlreadyRegistered)
				{
					OriginalMouseSize = mouse->GetSize();
					mouse->SetSize(MiddleMouseAnchorTransform->ReadSize());
				}
			}

			return true;
		}
	}

	return false;
}

bool ScrollbarComponent::ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	if (Super::ExecuteConsumableMouseWheelMove(mouse, sfmlEvent))
	{
		return true;
	}

	if (!IsVisible())
	{
		return false;
	}

	Vector2 mousePosition = Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
	if (FrameSpriteTransform == nullptr || !FrameSpriteTransform->IsWithinBounds(mousePosition))
	{
		//Not scrolling over the frame
		return false;
	}

	if (FrameCamera == nullptr)
	{
		return false;
	}

	if (VerticalThumbHoldOffset >= 0.f || HorizontalThumbHoldOffset >= 0.f)
	{
		//User is dragging the thumbs.  Ignore mouse wheel, but still consume the event.
		return true;
	}

	//Process mouse wheel input
	if (InputBroadcaster::GetCtrlHeld() && ZoomEnabled) //zoom the camera
	{
		bool zoomingIn = (sfmlEvent.delta > 0);
		if (DesiredZoom > 0.f)
		{
			//Check if the current movement should be interrupted
			bool cameraZoomingIn = (FrameCamera->GetZoom() > DesiredZoom);
			if (cameraZoomingIn != zoomingIn)
			{
				DesiredZoom = FrameCamera->GetZoom();
			}
		}
		else
		{
			DesiredZoom = FrameCamera->GetZoom();
		}

		FLOAT multiplier = (zoomingIn) ? 1.f : -1.f;
		DesiredZoom += (sqrt(DesiredZoom.Value) * ZoomSensitivity * multiplier);
	}
	else //Pan the camera
	{
		bool shouldResetMomentum = (CameraPanVelocity == Vector2::ZeroVector); //Reset if not moving
		if (CameraPanVelocity != Vector2::ZeroVector) //If moving
		{
			//Identify if we're changing directions
			Vector2 desiredVelocity(0.f, -sfmlEvent.delta * ScrollSpeed);
			shouldResetMomentum = (CameraPanVelocity.Dot(desiredVelocity) < 0.f);
		}

		if (shouldResetMomentum) //If not moving or changing directions
		{
			//Interrupt current movement and reset velocity/destination
			CameraPanVelocity = Vector2(0.f, -sfmlEvent.delta * ScrollSpeed);
			const Vector2 deltaMove(0.f, -sfmlEvent.delta * (InitialPanDistance / FrameCamera->GetZoom()));
			CameraDestination = FrameCamera->ReadPosition() + deltaMove;
			ClampCamPosition(OUT CameraDestination);
		}
		else //Already moving and moving in same direction as mouse wheel input
		{
			//Slightly increase speed and expand destination
			CameraPanVelocity.Y += (WheelSpeedAccumulationRate * -sfmlEvent.delta);
			CameraDestination.Y += (-sfmlEvent.delta * (InitialPanDistance / FrameCamera->GetZoom()));
			ClampCamPosition(OUT CameraDestination);
		}
	}

	return true;
}

void ScrollbarComponent::DestroyViewedObject ()
{
	if (ViewedObject != nullptr)
	{
		ViewedObject->RemoveScrollableObject();
		ViewedObject = nullptr;
	}
}

bool ScrollbarComponent::IsScrollingEnabled () const
{
	if (VerticalThumbTransform != nullptr && VerticalThumbTransform->ReadSize().Y < 1.f)
	{
		return true;
	}
	
	if (HorizontalThumbTransform != nullptr && HorizontalThumbTransform->ReadSize().X < 1.f)
	{
		return true;
	}

	return false;
}

void ScrollbarComponent::SetViewedObject (ScrollableInterface* newViewedObject)
{
	if (ViewedObject != nullptr)
	{
		//Unregister the ViewedObject from the Scrollbar's DrawLayer
		ViewedObject->UnregisterFromDrawLayer(FrameTexture.Get());
	}

	ViewedObject = newViewedObject;
	if (ViewedObject != nullptr)
	{
		ViewedObject->RegisterToDrawLayer(this, FrameTexture.Get());
		ViewedObject->InitializeScrollableInterface(this);
	}
}

void ScrollbarComponent::SetHideControlsWhenFull (bool newHideControlsWhenFull)
{
	HideControlsWhenFull = newHideControlsWhenFull;

	if (FrameSpriteTransform != nullptr)
	{
		if (HideControlsWhenFull)
		{
			//Disable anchoring and set the sprite to fill the entire component (can hide behind bars when invisible)
			FrameSpriteTransform->SetSize(1.f, 1.f);
			FrameSpriteTransform->SetAnchorLeft(0.f);
			FrameSpriteTransform->SetAnchorTop(0.f);
			FrameSpriteTransform->SetAnchorRight(-1.f);
			FrameSpriteTransform->SetAnchorBottom(-1.f);
		}
		else
		{
			//The scroll bars will never hide.  Ensure the sprite is not behind those bars.
			FrameSpriteTransform->SetAnchorLeft(0.f);
			FrameSpriteTransform->SetAnchorTop(0.f);
			FrameSpriteTransform->SetAnchorRight(ScrollbarThickness);
			FrameSpriteTransform->SetAnchorBottom(ScrollbarThickness);
		}
	}

	//Set all controls to be visible and enabled.  The RefreshScrollButtons will conditionally reset their flags
	if (ScrollUpButton != nullptr)
	{
		ScrollUpButton->SetVisibility(true);
		ScrollUpButton->SetEnabled(true);
	}

	if (ScrollDownButton != nullptr)
	{
		ScrollDownButton->SetVisibility(true);
		ScrollDownButton->SetEnabled(true);
	}

	if (VerticalTrack != nullptr)
	{
		VerticalTrack->SetVisibility(true);
	}

	if (VerticalThumb != nullptr)
	{
		VerticalThumb->SetVisibility(true);
	}

	if (ScrollLeftButton != nullptr)
	{
		ScrollLeftButton->SetVisibility(true);
		ScrollLeftButton->SetEnabled(true);
	}

	if (ScrollRightButton != nullptr)
	{
		ScrollRightButton->SetVisibility(true);
		ScrollRightButton->SetEnabled(true);
	}

	if (HorizontalTrack != nullptr)
	{
		HorizontalTrack->SetVisibility(true);
	}

	if (HorizontalThumb != nullptr)
	{
		HorizontalThumb->SetVisibility(true);
	}

	//Force recompute visibility/enabledness
	HorizontalControlState = CS_Uninitialized;
	VerticalControlState = CS_Uninitialized;
	const Vector2 sizeRatio = (SpriteLatestSize / ObjLatestSize);
	RefreshScrollButtonConditions((sizeRatio.Y < 1.f), (sizeRatio.X < 1.f));
}

void ScrollbarComponent::SetEnabledTrackColor (Color newEnabledTrackColor)
{
	EnabledTrackColor = newEnabledTrackColor;
	if (VerticalTrack != nullptr)
	{
		VerticalTrack->SolidColor = EnabledTrackColor;
	}

	if (HorizontalTrack != nullptr)
	{
		HorizontalTrack->SolidColor = EnabledTrackColor;
	}
}

void ScrollbarComponent::SetEnabledThumbColor (Color newEnabledThumbColor)
{
	EnabledThumbColor = newEnabledThumbColor;

	//If thumb is enabled and not dragged
	if (VerticalThumb != nullptr && VerticalThumbHoldOffset < 0.f &&
		VerticalThumbTransform != nullptr && VerticalThumbTransform->ReadSize().Y < 1.f)
	{
		VerticalThumb->SolidColor = EnabledThumbColor;
	}

	if (HorizontalThumb != nullptr && HorizontalThumbHoldOffset < 0.f &&
		HorizontalThumbTransform != nullptr && HorizontalThumbTransform->ReadSize().X < 1.f)
	{
		HorizontalThumb->SolidColor = EnabledThumbColor;
	}
}

void ScrollbarComponent::SetDisabledThumbColor (Color newDisabledThumbColor)
{
	DisabledThumbColor = newDisabledThumbColor;

	//If thumb is disabled
	if (VerticalThumb != nullptr && VerticalThumbTransform != nullptr && VerticalThumbTransform->ReadSize().Y >= 1.f)
	{
		VerticalThumb->SolidColor = DisabledThumbColor;
	}

	if (HorizontalThumb != nullptr && HorizontalThumbTransform != nullptr && HorizontalThumbTransform->ReadSize().X >= 1.f)
	{
		HorizontalThumb->SolidColor = DisabledThumbColor;
	}
}

void ScrollbarComponent::SetThumbDragColor (Color newThumbDragColor)
{
	ThumbDragColor = newThumbDragColor;

	//If thumb is enabled and dragged
	if (VerticalThumb != nullptr && VerticalThumbHoldOffset >= 0.f &&
		VerticalThumbTransform != nullptr && VerticalThumbTransform->ReadSize().Y < 1.f)
	{
		VerticalThumb->SolidColor = ThumbDragColor;
	}

	if (HorizontalThumb != nullptr && HorizontalThumbHoldOffset >= 0.f &&
		HorizontalThumbTransform != nullptr && HorizontalThumbTransform->ReadSize().X < 1.f)
	{
		HorizontalThumb->SolidColor = ThumbDragColor;
	}
}

void ScrollbarComponent::SetZoomEnabled (bool newZoomEnabled)
{
	if (newZoomEnabled != ZoomEnabled)
	{
		ZoomEnabled = newZoomEnabled;
		if (ZoomButton != nullptr)
		{
			ZoomButton->SetVisibility(ZoomEnabled);
		}
	}
}

ScrollableInterface* ScrollbarComponent::GetViewedObject () const
{
	return ViewedObject;
}

RenderTexture* ScrollbarComponent::GetFrameTexture () const
{
	return FrameTexture.Get();
}

PlanarCamera* ScrollbarComponent::GetFrameCamera () const
{
	return FrameCamera.Get();
}

SpriteComponent* ScrollbarComponent::GetFrameSprite () const
{
	return FrameSprite.Get();
}

PlanarTransformComponent* ScrollbarComponent::GetFrameSpriteTransform () const
{
	return FrameSpriteTransform.Get();
}

ButtonComponent* ScrollbarComponent::GetScrollUpButton () const
{
	return ScrollUpButton.Get();
}

ButtonComponent* ScrollbarComponent::GetScrollDownButton () const
{
	return ScrollDownButton.Get();
}

ButtonComponent* ScrollbarComponent::GetScrollLeftButton () const
{
	return ScrollLeftButton.Get();
}

ButtonComponent* ScrollbarComponent::GetScrollRightButton () const
{
	return ScrollRightButton.Get();
}

SolidColorRenderComponent* ScrollbarComponent::GetVerticalTrack () const
{
	return VerticalTrack.Get();
}

SolidColorRenderComponent* ScrollbarComponent::GetHorizontalTrack () const
{
	return HorizontalTrack.Get();
}

SolidColorRenderComponent* ScrollbarComponent::GetTravelTrack () const
{
	return TravelTrack.Get();
}

SolidColorRenderComponent* ScrollbarComponent::GetVerticalThumb () const
{
	return VerticalThumb.Get();
}

SolidColorRenderComponent* ScrollbarComponent::GetHorizontalThumb () const
{
	return HorizontalThumb.Get();
}

SpriteComponent* ScrollbarComponent::GetMiddleMouseAnchor () const
{
	return MiddleMouseAnchor.Get();
}

ButtonComponent* ScrollbarComponent::GetZoomButton () const
{
	return ZoomButton.Get();
}

LabelComponent* ScrollbarComponent::GetZoomLabel () const
{
	return ZoomLabel.Get();
}

void ScrollbarComponent::UpdateZoom (FLOAT deltaSec)
{
	if (DesiredZoom <= 0.f || FrameCamera == nullptr)
	{
		return;
	}

	FLOAT curZoom = FrameCamera->GetZoom();

	//Compute where the camera zoom resides in the x^2 graph.
	FLOAT graphPos = sqrt(FrameCamera->GetZoom().Value);

	FLOAT multiplier = 1.f;
	if (curZoom > DesiredZoom)
	{
		multiplier *= -1.f; //Move left along X-axis instead
	}

	//Move along x-axis
	graphPos += (deltaSec * multiplier);
	if (graphPos <= 0.f)
	{
		DesiredZoom = -1.f; //Stop Zooming
		FrameCamera->SetZoom(ZoomRange.Min);
		return;
	}

	//Identify the new Zoom value based on the x position
	FLOAT newZoom = pow(graphPos.Value, 2);
	if (FLOAT::Abs(DesiredZoom - newZoom) <= ZoomSnapTolerance)
	{
		newZoom = DesiredZoom;
		DesiredZoom = -1.f; //Stop Zooming
	}

	if (!ZoomRange.ContainsValue(newZoom))
	{
		DesiredZoom = -1.f; //Stop Zooming
		newZoom = ZoomRange.GetClampedValue(newZoom);
	}

	FrameCamera->SetZoom(newZoom);
}

Vector2 ScrollbarComponent::CalcViewedFramePosition (const Vector2& absPosition) const
{
	CHECK(FrameSpriteTransform != nullptr && FrameCamera != nullptr)

	Vector2 frameCoordinates(absPosition);

	//Set coordinates relative to frame's top left corner.  The TopLeft corner is (0,0) in frame space.
	frameCoordinates -= FrameSpriteTransform->ReadCachedAbsPosition();

	frameCoordinates /= FrameCamera->GetZoom();
	frameCoordinates += (FrameCamera->ReadCachedAbsPosition() - (FrameCamera->GetZoomedExtents() * 0.5f));

	return frameCoordinates;
}

void ScrollbarComponent::GetCamPositionLimits (Vector2& outMinPos, Vector2& outMaxPos) const
{
	CHECK(FrameCamera != nullptr && ViewedObject != nullptr)

	Vector2 viewSize;
	ViewedObject->GetTotalSize(OUT viewSize);

	const Vector2 camHalfExtents = FrameCamera->GetZoomedExtents() * 0.5f;

	outMinPos = camHalfExtents;
	outMaxPos = viewSize - camHalfExtents;
}

void ScrollbarComponent::ClampCamPosition (Vector2& outClampedPosition) const
{
	if (FrameCamera != nullptr && ViewedObject != nullptr)
	{
		Vector2 minPos;
		Vector2 maxPos;
		GetCamPositionLimits(OUT minPos, OUT maxPos);

		outClampedPosition.ClampInline(minPos, maxPos);
	}
}

void ScrollbarComponent::UpdateTravelTrackVelocity (const Vector2& mousePos)
{
	//Restore velocity in case all of the conditions below fail.
	CameraPanVelocity = OriginalCamPanVelocity;

	//Identify if the mouse cursor is at the oposite direction the travel track is travelling.  If so, stop the camera's movement.
	switch (TravelTrackDirection)
	{
		case(TTD_Up):
			CHECK(VerticalThumbTransform != nullptr)
			//If below vertical thumb's top edge
			if (mousePos.Y > VerticalThumbTransform->ReadCachedAbsPosition().Y)
			{
				CameraPanVelocity = Vector2::ZeroVector;
			}

			break;

		case(TTD_Down):
			CHECK(VerticalThumbTransform != nullptr)
			//If above vertical thumb's bottom edge
			if (mousePos.Y < VerticalThumbTransform->ReadCachedAbsPosition().Y + VerticalThumbTransform->ReadCachedAbsSize().Y)
			{
				CameraPanVelocity = Vector2::ZeroVector;
			}

			break;

		case(TTD_Left):
			CHECK(HorizontalThumbTransform != nullptr)
			//If right of horizontal thumb's left edge
			if (mousePos.X > HorizontalThumbTransform->ReadCachedAbsPosition().X)
			{
				CameraPanVelocity = Vector2::ZeroVector;
			}

			break;

		case(TTD_Right):
			CHECK(HorizontalThumbTransform != nullptr)
			//If left of horizontal thumb's right edge
			if (mousePos.X < HorizontalThumbTransform->ReadCachedAbsPosition().X + HorizontalThumbTransform->ReadCachedAbsSize().X)
			{
				CameraPanVelocity = Vector2::ZeroVector;
			}

			break;
	}
}

void ScrollbarComponent::CalculateTravelTrackTransform (const Vector2& mousePos)
{
	CHECK(TravelTrackTransform != nullptr)
	Vector2 relativePos(mousePos - ReadCachedAbsPosition());

	//Ensure the position is within scrollable view bounds.
	relativePos.X = Utils::Clamp<FLOAT>(relativePos.X, ScrollbarThickness, ReadCachedAbsSize().X - (ScrollbarThickness * 2.f));
	relativePos.Y = Utils::Clamp<FLOAT>(relativePos.Y, ScrollbarThickness, ReadCachedAbsSize().Y - (ScrollbarThickness * 2.f));

	switch (TravelTrackDirection)
	{
		case(TTD_Up):
		{
			CHECK(VerticalThumbTransform != nullptr)

			//Reference abs coordinates since the thumb position is using scalar values (0-1).
			const FLOAT thumbTopEdge = VerticalThumbTransform->ReadCachedAbsPosition().Y - ReadCachedAbsPosition().Y;
			if (relativePos.Y < thumbTopEdge)
			{
				TravelTrackTransform->SetSize(TravelTrackTransform->ReadSize().X, thumbTopEdge - relativePos.Y);
				TravelTrackTransform->SetPosition(TravelTrackTransform->ReadPosition().X, relativePos.Y);
			}
			else
			{
				//Mouse is below the top border while the thumb is moving up.  Hide travel track.
				TravelTrackTransform->SetSize(TravelTrackTransform->ReadSize().X, 0.f);
			}

			break;
		}
		case(TTD_Down):
		{
			CHECK(VerticalThumbTransform != nullptr)

			//Reference abs coordinates since the thumb position is using scalar values (0-1).
			const FLOAT thumbBottomEdge = VerticalThumbTransform->ReadCachedAbsPosition().Y - ReadCachedAbsPosition().Y + VerticalThumbTransform->ReadCachedAbsSize().Y;
			if (relativePos.Y > thumbBottomEdge)
			{
				TravelTrackTransform->SetSize(TravelTrackTransform->ReadSize().X, relativePos.Y - thumbBottomEdge);
				TravelTrackTransform->SetPosition(TravelTrackTransform->ReadPosition().X, thumbBottomEdge);
			}
			else
			{
				//Mouse is above bottom border while the thumb is moving down.  Hide the travel track transform.
				TravelTrackTransform->SetSize(TravelTrackTransform->ReadSize().X, 0.f);
			}

			break;
		}
		case(TTD_Left):
		{
			CHECK(HorizontalThumbTransform != nullptr)

			const FLOAT thumbLeftEdge = HorizontalThumbTransform->ReadCachedAbsPosition().X - ReadCachedAbsPosition().X;
			if (relativePos.X < thumbLeftEdge)
			{
				TravelTrackTransform->SetSize(thumbLeftEdge - relativePos.X, TravelTrackTransform->ReadSize().Y);
				TravelTrackTransform->SetPosition(relativePos.X, TravelTrackTransform->ReadPosition().Y);
			}
			else
			{
				//Mouse is right of thumb while it's moving left.  Hide travel track.
				TravelTrackTransform->SetSize(0.f, TravelTrackTransform->ReadSize().Y);
			}

			break;
		}
		case(TTD_Right):
		{
			CHECK(HorizontalThumbTransform != nullptr)

			const FLOAT thumbRightEdge = HorizontalThumbTransform->ReadCachedAbsPosition().X - ReadCachedAbsPosition().X + HorizontalThumbTransform->ReadCachedAbsSize().X;
			if (relativePos.X > thumbRightEdge)
			{
				TravelTrackTransform->SetSize(relativePos.X - thumbRightEdge, TravelTrackTransform->ReadSize().Y);
				TravelTrackTransform->SetPosition(thumbRightEdge, TravelTrackTransform->ReadPosition().Y);
			}
			else
			{
				//Mouse is left of thumb while it's moving right.  Hide the travel track.
				TravelTrackTransform->SetSize(0.f, TravelTrackTransform->ReadSize().Y);
			}

			break;
		}
		
	}
}

void ScrollbarComponent::CalculateThumbTransforms ()
{
	//Check if any changes are detected
	if (FrameCamera == nullptr || FrameSpriteTransform == nullptr || ViewedObject == nullptr)
	{
		return;
	}

	const std::function<bool(const Vector2&, const Vector2&)> isChangeDetected = [&](const Vector2& a, const Vector2& b)
	{
		return (FLOAT::Abs(a.X - b.X) >= TransformDetectionThreshold ||
			FLOAT::Abs(a.Y - b.Y) >= TransformDetectionThreshold);
	};

	const Vector2& camPos = FrameCamera->ReadCachedAbsPosition();
	const Vector2& spriteSize = FrameSpriteTransform->ReadCachedAbsSize();
	Vector2 objSize;
	ViewedObject->GetTotalSize(OUT objSize);
	if (FrameCamera->GetZoom().IsCloseTo(CamLatestZoom, FLOAT_TOLERANCE) && !isChangeDetected(CamLatestPos, camPos) &&
		!isChangeDetected(SpriteLatestSize, spriteSize) && !isChangeDetected(objSize, ObjLatestSize))
	{
		//Nothing changed.  Don't bother recomputing transforms.
		return;
	}

	//A change is detected.  Update transforms.
	CamLatestPos = camPos;
	SpriteLatestSize = spriteSize;
	ObjLatestSize = objSize;
	CamLatestZoom = FrameCamera->GetZoom();

	//Calculate thumb positions and sizes
	const Vector2 sizeRatio = (FrameCamera->GetZoomedExtents() / objSize);

	//Find top left corner of viewable cam region.  Divide by objSize to get a value between 0 to objSize-regionSize.
	const Vector2 thumbPos = ((camPos - (FrameCamera->GetZoomedExtents() * 0.5f)) / objSize);
	if (VerticalThumbTransform != nullptr)
	{
		VerticalThumbTransform->SetSize(VerticalThumbTransform->ReadSize().X, Utils::Min<FLOAT>(sizeRatio.Y, 1.f));
		VerticalThumbTransform->SetPosition(VerticalThumbTransform->ReadPosition().X, thumbPos.Y);

		//Update colors based on if there's something to scroll or not
		if (VerticalThumb != nullptr && VerticalThumbHoldOffset < 0.f) //Don't update color if user is dragging thumb.
		{
			VerticalThumb->SolidColor = (sizeRatio.Y >= 1.f) ? DisabledThumbColor : EnabledThumbColor;
		}
	}

	if (HorizontalThumbTransform != nullptr)
	{
		HorizontalThumbTransform->SetSize(Utils::Min<FLOAT>(sizeRatio.X, 1.f), HorizontalThumbTransform->ReadSize().Y);
		HorizontalThumbTransform->SetPosition(thumbPos.X, HorizontalThumbTransform->ReadPosition().Y);

		//Update colors based on if there's something to scroll or not
		if (HorizontalThumb != nullptr && HorizontalThumbHoldOffset < 0.f)
		{
			HorizontalThumb->SolidColor = (sizeRatio.X >= 1.f) ? DisabledThumbColor : EnabledThumbColor;
		}
	}

	//Either enable or disable panning controls based on if there are content to scroll.
	RefreshScrollButtonConditions((sizeRatio.Y < 1.f), (sizeRatio.X < 1.f));
}

void ScrollbarComponent::RefreshScrollButtonConditions (bool isVertScrollingEnabled, bool isHorScrollingEnabled)
{
	bool updateHorizontal = (HorizontalControlState == CS_Uninitialized ||
		(HorizontalControlState == CS_Disabled && isHorScrollingEnabled) ||
		(HorizontalControlState == CS_Enabled && !isHorScrollingEnabled));

	bool updateVertical = (VerticalControlState == CS_Uninitialized ||
		(VerticalControlState == CS_Disabled && isVertScrollingEnabled) ||
		(VerticalControlState == CS_Enabled && !isVertScrollingEnabled));

	HorizontalControlState = (isHorScrollingEnabled) ? CS_Enabled : CS_Disabled;
	VerticalControlState = (isVertScrollingEnabled) ? CS_Enabled : CS_Disabled;

	if (HideControlsWhenFull)
	{
		if (updateVertical)
		{
			if (ScrollUpButton != nullptr)
			{
				ScrollUpButton->SetVisibility(isVertScrollingEnabled);
			}

			if (ScrollDownButton != nullptr)
			{
				ScrollDownButton->SetVisibility(isVertScrollingEnabled);
			}

			if (VerticalTrack != nullptr)
			{
				VerticalTrack->SetVisibility(isVertScrollingEnabled);
			}

			if (VerticalThumb != nullptr)
			{
				VerticalThumb->SetVisibility(isVertScrollingEnabled);
			}

			if (FrameSpriteTransform != nullptr)
			{
				const FLOAT rightAnchor = (isVertScrollingEnabled) ? ScrollbarThickness : 0.f;
				FrameSpriteTransform->SetAnchorRight(rightAnchor);
			}
		}

		if (updateHorizontal)
		{
			if (ScrollLeftButton != nullptr)
			{
				ScrollLeftButton->SetVisibility(isHorScrollingEnabled);
			}

			if (ScrollRightButton != nullptr)
			{
				ScrollRightButton->SetVisibility(isHorScrollingEnabled);
			}

			if (HorizontalTrack != nullptr)
			{
				HorizontalTrack->SetVisibility(isHorScrollingEnabled);
			}

			if (HorizontalThumb != nullptr)
			{
				HorizontalThumb->SetVisibility(isHorScrollingEnabled);
			}

			if (FrameSpriteTransform != nullptr)
			{
				const FLOAT bottomAnchor = (isHorScrollingEnabled) ? ScrollbarThickness : 0.f;
				FrameSpriteTransform->SetAnchorBottom(bottomAnchor);
			}
		}
	}
	else //Disable controls when full
	{
		if (updateVertical)
		{
			if (ScrollUpButton != nullptr)
			{
				ScrollUpButton->SetEnabled(isVertScrollingEnabled);
			}

			if (ScrollDownButton != nullptr)
			{
				ScrollDownButton->SetEnabled(isVertScrollingEnabled);
			}
		}

		if (updateHorizontal)
		{
			if (ScrollLeftButton != nullptr)
			{
				ScrollLeftButton->SetEnabled(isHorScrollingEnabled);
			}

			if (ScrollRightButton != nullptr)
			{
				ScrollRightButton->SetEnabled(isHorScrollingEnabled);
			}
		}
	}
}

void ScrollbarComponent::RefreshSpriteTextureRegion ()
{
	if (FrameSpriteTransform == nullptr || FrameSprite == nullptr)
	{
		return;
	}

	//Don't set draw coordinates if the size is not yet computed.
	if (FrameSpriteTransform->ReadCachedAbsSize().X <= 0 || FrameSpriteTransform->ReadCachedAbsSize().Y <= 0)
	{
		return;
	}

	FrameSprite->SetDrawCoordinates(0, 0, FrameSpriteTransform->ReadCachedAbsSize().X.ToINT(), FrameSpriteTransform->ReadCachedAbsSize().Y.ToINT());

	//Ensure the camera's ViewExtents is equal to the size of the sprite size to help with alignment.
	if (FrameCamera != nullptr && FrameCamera->GetZoomedExtents() != FrameSpriteTransform->ReadCachedAbsSize())
	{
		Vector2 oldSize = FrameCamera->GetZoomedExtents();
		FrameCamera->SetViewExtents(FrameSpriteTransform->ReadCachedAbsSize());
		if (ViewedObject != nullptr)
		{
			ViewedObject->HandleScrollbarFrameSizeChange(oldSize, FrameCamera->GetZoomedExtents());
		}
	}
}

Vector2 ScrollbarComponent::CalculateRelativeMousePosition (const Vector2& absMousePos) const
{
	Vector2 relativeMousePos = absMousePos;

	//If this ScrollbarComponent is rendered inside a RenderTarget rather than a window, calculate where the mouse pointer is on in relation to RenderTarget
	if (const RenderTarget* rootRenderTarget = FindRootRenderTarget())
	{
		Vector2 pixelPos = Mouse->ReadMousePosition() - rootRenderTarget->GetWindowCoordinates();

		//If camera is not provided, then assume it's already aligned (top left corner of RootRenderTarget)
		if (const PlanarCamera* planarExternalCam = dynamic_cast<const PlanarCamera*>(FindExternalCamera()))
		{
			//Offset location based on camera position
			Vector3 projectedPoint;
			Rotator projectedDirection;
			planarExternalCam->CalculatePixelProjection(FrameSpriteTransform->ReadCachedAbsSize(), pixelPos, OUT projectedDirection, OUT projectedPoint);
			relativeMousePos = projectedPoint.ToVector2();
		}
	}

	return relativeMousePos;
}

Texture* ScrollbarComponent::CalculateAnchoredMouseIcon (const Vector2& mousePos) const
{
	GuiTheme* localTheme = GuiTheme::GetGuiTheme();
	CHECK(localTheme != nullptr);

	const Vector2 relativeDist(mousePos - AnchorPosition);
	if (FLOAT::Abs(relativeDist.X) <= AnchorDistanceLimits.Min && FLOAT::Abs(relativeDist.Y) <= AnchorDistanceLimits.Min)
	{
		return localTheme->ScrollbarMouseAnchorStationary.Get();
	}

	//0 = stationary.  -1 = left/up.  1 = right/down
	int verticalDirection = 0;
	int horizontalDirection = 0;

	//Check vertical direction
	if (relativeDist.Y < -AnchorDistanceLimits.Min)
	{
		verticalDirection = -1; //up
	}
	else if (relativeDist.Y > AnchorDistanceLimits.Min)
	{
		verticalDirection = 1; //Down
	}

	//Check horizontal direction
	if (relativeDist.X < -AnchorDistanceLimits.Min)
	{
		horizontalDirection = -1; //left
	}
	else if (relativeDist.X > AnchorDistanceLimits.Min)
	{
		horizontalDirection = 1; //right
	}

	if (verticalDirection < 0 && horizontalDirection == 0)
	{
		return localTheme->ScrollbarMouseAnchorPanUp.Get();
	}
	else if (verticalDirection < 0 && horizontalDirection > 0)
	{
		return localTheme->ScrollbarMouseAnchorPanUpRight.Get();
	}
	else if (verticalDirection == 0 && horizontalDirection > 0)
	{
		return localTheme->ScrollbarMouseAnchorPanRight.Get();
	}
	else if (verticalDirection > 0 && horizontalDirection > 0)
	{
		return localTheme->ScrollbarMouseAnchorPanDownRight.Get();
	}
	else if (verticalDirection > 0 && horizontalDirection == 0)
	{
		return localTheme->ScrollbarMouseAnchorPanDown.Get();
	}
	else if (verticalDirection > 0 && horizontalDirection < 0)
	{
		return localTheme->ScrollbarMouseAnchorPanDownLeft.Get();
	}
	else if (verticalDirection == 0 && horizontalDirection < 0)
	{
		return localTheme->ScrollbarMouseAnchorPanLeft.Get();
	}
	else if (verticalDirection < 0 && horizontalDirection < 0)
	{
		return localTheme->ScrollbarMouseAnchorPanUpLeft.Get();
	}

	return nullptr;
}

void ScrollbarComponent::HandleScrollUpButtonPressed (ButtonComponent* uiComponent)
{
	if (FrameCamera != nullptr)
	{
		CameraPanVelocity = Vector2(0.f, -ScrollSpeed);
		CameraDestination = FrameCamera->ReadCachedAbsPosition() + Vector2(0, -InitialPanDistance);
		IsHoldingScrollButton = true;
	}
}

void ScrollbarComponent::HandleScrollDownButtonPressed (ButtonComponent* uiComponent)
{
	if (FrameCamera != nullptr)
	{
		CameraPanVelocity = Vector2(0.f, ScrollSpeed);
		CameraDestination = FrameCamera->ReadCachedAbsPosition() + Vector2(0, InitialPanDistance);
		IsHoldingScrollButton = true;
	}
}

void ScrollbarComponent::HandleScrollLeftButtonPressed (ButtonComponent* uiComponent)
{
	if (FrameCamera != nullptr)
	{
		CameraPanVelocity = Vector2(-ScrollSpeed, 0.f);
		CameraDestination = FrameCamera->ReadCachedAbsPosition() + Vector2(-InitialPanDistance, 0.f);
		IsHoldingScrollButton = true;
	}
}

void ScrollbarComponent::HandleScrollRightButtonPressed (ButtonComponent* uiComponent)
{
	if (FrameCamera != nullptr)
	{
		CameraPanVelocity = Vector2(ScrollSpeed, 0.f);
		CameraDestination = FrameCamera->ReadCachedAbsPosition() + Vector2(InitialPanDistance, 0.f);
		IsHoldingScrollButton = true;
	}
}

void ScrollbarComponent::HandleZoomButtonPressed (ButtonComponent* uiComponent)
{
	HandleZoomButtonRightClickReleased(uiComponent);

	//Disabled until TextFieldComponents are working
#if 0
	if (ZoomFeedbackTransform != nullptr)
	{
		ZoomFeedbackTransform->SetVisibility(true);

		SettingMousePosition = true; //Ignore this mouse jump
		MousePointer::SetAbsMousePosition(ZoomFeedbackTransform->ReadCachedAbsPosition() + (ZoomFeedbackTransform->ReadCachedAbsSize() * 0.5f));
		SettingMousePosition = false;
	}

	if (ZoomLabel != nullptr && FrameCamera != nullptr)
	{
		const FLOAT camZoom = FrameCamera->GetZoom() * 100.f; //Convert decimal to percentage
		ZoomLabel->SetText(camZoom.ToString() + TXT("%"));
	}
#endif
}

void ScrollbarComponent::HandleZoomButtonReleased (ButtonComponent* uiComponent)
{
	//Noop
}

void ScrollbarComponent::HandleZoomButtonRightClickReleased (ButtonComponent* uiComponent)
{
	if (FrameCamera != nullptr)
	{
		FrameCamera->SetZoom(1.f);
	}
}

Texture* ScrollbarComponent::HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& mouseEvnt)
{
	if (AnchorPosition.X < 0.f || AnchorPosition.Y < 0.f)
	{
		AnchoredMouseIcon = nullptr;
	}
	else
	{
		Engine* localEngine = Engine::FindEngine();
		CHECK(localEngine != nullptr)

		if (localEngine->GetElapsedTime() - AnchorIconTime >= AnchorIconRefreshTimeInterval)
		{
			Vector2 relativeMousePos = CalculateRelativeMousePosition(mouse->ReadCachedAbsPosition());
			AnchoredMouseIcon = CalculateAnchoredMouseIcon(relativeMousePos);
			AnchorIconTime = localEngine->GetElapsedTime();
		}
	}

	if (AnchoredMouseIcon == nullptr)
	{
		//About to remove mouse override, restore mouse pointer size.
		mouse->SetSize(OriginalMouseSize);
	}

	return AnchoredMouseIcon.Get();
}

void ScrollbarComponent::HandleZoomChanged (FLOAT newZoom)
{
	if (ZoomButton != nullptr)
	{
		ZoomButton->SetCaptionText(newZoom.ToFormattedString(1, 2, FLOAT::eTruncateMethod::TM_RoundUp));
	}
}

void ScrollbarComponent::HandleTick (FLOAT deltaSec)
{
	if (FrameCamera != nullptr)
	{
		Vector2 newPos = FrameCamera->ReadPosition();
		if (newPos == CameraDestination && !IsHoldingScrollButton)
		{
			CameraPanVelocity = Vector2::ZeroVector; //Arrived at destination, stop moving camera
			CameraDestination = Vector2(-1.f, -1.f);
		}
		else if (CameraPanVelocity != Vector2::ZeroVector)
		{
			//Apply camera velocity
			FLOAT velocityMultiplier = 1.f;
			if (InputBroadcaster::GetShiftHeld())
			{
				velocityMultiplier *= ShiftSpeedMultiplier;
			}

			if (InputBroadcaster::GetAltHeld())
			{
				velocityMultiplier *= AltSpeedMultiplier;
			}

			newPos += ((CameraPanVelocity * velocityMultiplier * deltaSec) / FrameCamera->GetZoom());
		}

		UpdateZoom(deltaSec);

		//Always clamp position in case ViewedObject changed size or Sprite changed dimensions.
		ClampCamPosition(OUT newPos);
		FrameCamera->SetPosition(newPos);
		FrameCamera->ComputeAbsTransform(); //Needed for thumb transforms later in this Tick function

		//Identify if the camera has arrived to destination
		if (!IsHoldingScrollButton && TravelTrackDirection == TTD_None &&
			CameraPanVelocity != Vector2::ZeroVector && CameraDestination.X >= 0.f && CameraDestination.Y >= 0.f)
		{
			Vector2 direction = CameraPanVelocity;
			direction.Normalize();

			Vector2 relativeDest = CameraDestination - FrameCamera->ReadCachedAbsPosition();
			if (!relativeDest.IsEmpty())
			{
				relativeDest.Normalize();

				//If the camera is near/past the destination or suddenly changed direction.  Stop moving and cancel destination.
				if (direction.Dot(relativeDest) < 0.f)
				{
					CameraDestination = Vector2(-1.f, -1.f);
					CameraPanVelocity = Vector2::ZeroVector;
				}
			}
		}
	}

	CalculateThumbTransforms();

	if (TravelTrackDirection != TTD_None && Mouse != nullptr)
	{
		Vector2 relativeMousePos = CalculateRelativeMousePosition(Mouse->ReadCachedAbsPosition());
		CalculateTravelTrackTransform(relativeMousePos);
		UpdateTravelTrackVelocity(relativeMousePos);
	}

	RefreshSpriteTextureRegion();
}
SD_END