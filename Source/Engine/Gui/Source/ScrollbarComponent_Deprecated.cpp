/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScrollbarComponent_Deprecated.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, ScrollbarComponent_Deprecated, SD, GuiComponent)

void ScrollbarComponent_Deprecated::InitProps ()
{
	Super::InitProps();

	Track = nullptr;
	MiddleMouseSpriteOwner = nullptr;
	MiddleMouseSprite = nullptr;
	PanningMiddleMouseCursorOwner = nullptr;
	PanningMiddleMouseCursor = nullptr;
	TopScrollButton = nullptr;
	BottomScrollButton = nullptr;
	ScrollJumpInterval = 3;
	bEnableMiddleMouseScrolling = true;
	MiddleMouseScrollIntervalRange = Range<FLOAT>(0.05f, 1.f);
	MiddleMouseHoldThreshold = 0.75f;
	ContinuousScrollDelay = 0.75f;
	ContinuousInterval = 0.2f;
	bClampsMouseWhenScrolling = true;

	ScrollPosition = 0;
	MaxScrollPosition = 0;
	NumVisibleScrollPositions = 1;
	bEnabled = true;
	bHideWhenInsufficientScrollPos = true;
	bHoldingScrollButtons = false;
	MiddleMousePosition = Vector2(-1, -1);
	MiddleMouseTimeStamp = -1;
	OnScrollPositionChanged = nullptr;
	InteractingMouse = nullptr;

	MiddleMousePanDirection = 0;
	MiddleMousePanTimeRemaining = -1.f;
	ButtonPanTimeRemaining = -1.f;
	bClampingMousePointer = false;
	bScrollbarVisible = true;
}

void ScrollbarComponent_Deprecated::BeginObject ()
{
	Super::BeginObject();

	//Refresh the scroll bar for draw positions and track scale
	SetNumVisibleScrollPositions(NumVisibleScrollPositions);
	SetMaxScrollPosition(MaxScrollPosition);
	SetScrollPosition(0);
}

void ScrollbarComponent_Deprecated::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ScrollbarComponent_Deprecated* scrollbarTemplate = dynamic_cast<const ScrollbarComponent_Deprecated*>(objTemplate);
	if (scrollbarTemplate != nullptr)
	{
		ScrollJumpInterval = scrollbarTemplate->ScrollJumpInterval;
		bEnableMiddleMouseScrolling = scrollbarTemplate->bEnableMiddleMouseScrolling;
		MiddleMouseScrollIntervalRange = scrollbarTemplate->MiddleMouseScrollIntervalRange;
		MiddleMouseHoldThreshold = scrollbarTemplate->MiddleMouseHoldThreshold;
		ContinuousScrollDelay = scrollbarTemplate->ContinuousScrollDelay;
		ContinuousInterval = scrollbarTemplate->ContinuousInterval;
		bClampsMouseWhenScrolling = scrollbarTemplate->bClampsMouseWhenScrolling;
		SetHideWhenInsufficientScrollPos(scrollbarTemplate->GetHideWhenInsufficientScrollPos());

		bool bCreatedObj;
		MiddleMouseSprite = ReplaceTargetWithObjOfMatchingClass(MiddleMouseSprite.Get(), scrollbarTemplate->MiddleMouseSprite.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(MiddleMouseSprite.Get());
		}

		if (MiddleMouseSprite != nullptr)
		{
			MiddleMouseSprite->CopyPropertiesFrom(scrollbarTemplate->MiddleMouseSprite.Get());
		}

		TopScrollButton = ReplaceTargetWithObjOfMatchingClass(TopScrollButton.Get(), scrollbarTemplate->TopScrollButton.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			if (AddComponent(TopScrollButton.Get()))
			{
				TopScrollButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleScrollButtonPressed, void, ButtonComponent*));
				TopScrollButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleScrollButtonReleased, void, ButtonComponent*));
			}
		}

		if (TopScrollButton != nullptr)
		{
			TopScrollButton->CopyPropertiesFrom(scrollbarTemplate->TopScrollButton.Get());
		}

		BottomScrollButton = ReplaceTargetWithObjOfMatchingClass(BottomScrollButton.Get(), scrollbarTemplate->BottomScrollButton.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			if (AddComponent(BottomScrollButton.Get()))
			{
				BottomScrollButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleScrollButtonPressed, void, ButtonComponent*));
				BottomScrollButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleScrollButtonReleased, void, ButtonComponent*));
			}
		}

		if (BottomScrollButton != nullptr)
		{
			BottomScrollButton->CopyPropertiesFrom(scrollbarTemplate->BottomScrollButton.Get());
		}

		Track = ReplaceTargetWithObjOfMatchingClass(Track.Get(), scrollbarTemplate->Track.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			if (AddComponent(Track.Get()))
			{
				Track->SetThumbChangedPositionHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleTrackChangedPosition, void, INT));
			}
		}

		if (Track != nullptr)
		{
			Track->CopyPropertiesFrom(scrollbarTemplate->Track.Get());
		}
	}
}

void ScrollbarComponent_Deprecated::InitializeComponents ()
{
	Super::InitializeComponents();

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, HandleTick, void, FLOAT));
	}

	MiddleMouseSpriteOwner = GuiComponent::CreateObject();
	if (AddComponent(MiddleMouseSpriteOwner.Get()))
	{
		MiddleMouseSprite = SpriteComponent::CreateObject();
		if (MiddleMouseSpriteOwner->AddComponent(MiddleMouseSprite.Get()))
		{
			MiddleMouseSprite->SetSpriteTexture(GuiTheme::GetGuiTheme()->ScrollbarMiddleMouseScrollAnchor.Get());
		}

		MiddleMouseSpriteOwner->SetSize(MiddleMouseSprite->GetTextureSize());
		MiddleMouseSpriteOwner->SetVisibility(false);
	}

	InitializeScrollButtons();
	InitializeTrackComponent();
}

void ScrollbarComponent_Deprecated::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	if (PanningMiddleMouseCursor == nullptr || PanningMiddleMouseCursorOwner == nullptr)
	{
		return;
	}

	PanningMiddleMouseCursorOwner->SetPosition(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));

	INT oldPanDirection = MiddleMousePanDirection;
	FLOAT deltaPos = MiddleMousePosition.Y - FLOAT::MakeFloat(sfmlEvent.y);

	if (deltaPos < -8)
	{
		MiddleMousePanDirection = -1;
	}
	else if (deltaPos > 8)
	{
		MiddleMousePanDirection = 1;
	}
	else
	{
		MiddleMousePanDirection = 0;
	}
		
	if (MiddleMousePanDirection != oldPanDirection)
	{
		switch(MiddleMousePanDirection.Value)
		{
			case(-1):
				PanningMiddleMouseCursor->SetSubDivision(1, 2, 0, 1);
				break;
			case(0):
				PanningMiddleMouseCursor->SetSubDivision(1, 1, 0, 0);
				break;
			case(1):
				PanningMiddleMouseCursor->SetSubDivision(1, 2, 0, 0);
				break;
		}
	}
}

void ScrollbarComponent_Deprecated::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	if (bEnabled && bEnableMiddleMouseScrolling && sfmlEvent.button == sf::Mouse::Middle &&
			IsVisible() && eventType == sf::Event::MouseButtonReleased)
	{
		if (Engine::FindEngine()->GetElapsedTime() - MiddleMouseTimeStamp > MiddleMouseHoldThreshold)
		{
			//The middle mouse button was held long enough to be considered a "hold to move" action.
			SetMiddleMousePosition(mouse, Vector2(-1, -1));
		}
	}
}

bool ScrollbarComponent_Deprecated::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	//Only listen to middle mouse
	if (!bEnabled || sfmlEvent.button != sf::Mouse::Middle || !IsVisible())
	{
		return false;
	}

	if (!bEnableMiddleMouseScrolling || eventType != sf::Event::MouseButtonPressed)
	{
		return false;
	}

	GuiComponent* guiOwner = dynamic_cast<GuiComponent*>(Owner.Get());
	if (guiOwner != nullptr && guiOwner->IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))))
	{
		if (MiddleMousePosition.X >= 0 || MiddleMousePosition.Y >= 0)
		{
			//Toggle off
			SetMiddleMousePosition(mouse, Vector2(-1, -1));
			return true;				
		}

		MiddleMouseTimeStamp = Engine::FindEngine()->GetElapsedTime();
		SetMiddleMousePosition(mouse, Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));

		return true;
	}

	return false;
}

bool ScrollbarComponent_Deprecated::ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	if (Super::ExecuteConsumableMouseWheelMove(mouse, sfmlEvent))
	{
		return true;
	}

	if (!bEnabled || !IsVisible())
	{
		return false;
	}

	//Only mouse wheel events are relevant when the mouse cursor is over the owner.
	GuiComponent* guiOwner = dynamic_cast<GuiComponent*>(Owner.Get());
	if (guiOwner != nullptr && guiOwner->IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))))
	{
		INT scrollMultiplier = -1;
		if (InputBroadcaster::GetCtrlHeld())
		{
			scrollMultiplier *= NumVisibleScrollPositions;
		}
			
		IncrementScrollPosition(FLOAT(sfmlEvent.delta).ToINT() * scrollMultiplier);
		return true;
	}

	return false;
}

bool ScrollbarComponent_Deprecated::AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const
{
	if (!bEnabled)
	{
		return false;
	}

	GuiComponent* guiOwner = dynamic_cast<GuiComponent*>(GetOwner());
	return (Super::AcceptsMouseEvents(mousePosX, mousePosY) || !guiOwner || guiOwner->IsWithinBounds(Vector2(FLOAT::MakeFloat(mousePosX), FLOAT::MakeFloat(mousePosY))));
}

void ScrollbarComponent_Deprecated::HandleSizeChange ()
{
	Super::HandleSizeChange();

	if (TopScrollButton != nullptr)
	{
		TopScrollButton->SetSize(Vector2(ReadSize().X, ReadSize().X));
		TopScrollButton->SetPosition(Vector2::ZeroVector);

		if (BottomScrollButton != nullptr)
		{
			BottomScrollButton->SetSize(TopScrollButton->ReadSize());
			BottomScrollButton->SetPosition(Vector2(0.f, ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y));
		}

		if (Track != nullptr)
		{
			Track->SetSize(ReadSize() - Vector2(0.f, TopScrollButton->ReadSize().Y * 2));
			Track->SetPosition(Vector2(0.f, TopScrollButton->ReadCachedAbsSize().Y));
		}
	}
}

void ScrollbarComponent_Deprecated::Destroyed ()
{
	bClampingMousePointer = false;

	if (MiddleMousePosition.X >= 0 || MiddleMousePosition.Y >= 0)
	{
		//restore mouse pointer changes
		SetMiddleMousePosition(InteractingMouse.Get(), Vector2(-1,-1));
	}

	Super::Destroyed();
}

bool ScrollbarComponent_Deprecated::IsIndexVisible (const INT index) const
{
	return (index >= ScrollPosition && index < ScrollPosition + NumVisibleScrollPositions && index < (MaxScrollPosition + NumVisibleScrollPositions));
}

void ScrollbarComponent_Deprecated::IncrementScrollPosition (INT amountToJump)
{
	SetScrollPosition(ScrollPosition + amountToJump);
}

void ScrollbarComponent_Deprecated::SetScrollPositionChanged (std::function<void(INT newScrollPosition)> newHandler)
{
	OnScrollPositionChanged = newHandler;
}

void ScrollbarComponent_Deprecated::SetScrollPosition (INT newScrollPosition)
{
	newScrollPosition = Utils::Clamp<INT>(newScrollPosition, 0, MaxScrollPosition - NumVisibleScrollPositions);
	if (newScrollPosition == ScrollPosition)
	{
		return; //No need to update scroll position if the values did not change.
	}

	ScrollPosition = newScrollPosition;

	if (Track != nullptr)
	{
		Track->RefreshScrollbarTrack();
	}

	if (OnScrollPositionChanged != nullptr)
	{
		OnScrollPositionChanged(ScrollPosition);
	}
}

void ScrollbarComponent_Deprecated::SetMaxScrollPosition (INT newMaxScrollPosition)
{
	MaxScrollPosition = Utils::Max<INT>(newMaxScrollPosition, 0);
	EvaluateScrollButtonEnabledness();
	if (bHideWhenInsufficientScrollPos)
	{
		EvaluateScrollVisibility();
	}

	if (Track != nullptr)
	{
		//This will toggle the thumb's visibility if NumVisibleScrollPositions <= MaxScrollPosition
		Track->RefreshScrollbarTrack();
	}
}

void ScrollbarComponent_Deprecated::SetNumVisibleScrollPositions (INT newNumVisibleScrollPositions)
{
	NumVisibleScrollPositions = Utils::Max<INT>(newNumVisibleScrollPositions, 1);
	EvaluateScrollButtonEnabledness();
	if (bHideWhenInsufficientScrollPos)
	{
		EvaluateScrollVisibility();
	}
		
	if (Track != nullptr)
	{
		Track->RefreshScrollbarTrack();
	}
}

void ScrollbarComponent_Deprecated::SetEnabled (bool bNewEnabled)
{
	bEnabled = bNewEnabled;

	if (Track != nullptr)
	{
		Track->SetEnabled(bNewEnabled);
	}

	EvaluateScrollButtonEnabledness();

	if (!bEnabled)
	{
		SetMiddleMousePosition(InteractingMouse.Get(), Vector2(-1, -1));
	}
}

void ScrollbarComponent_Deprecated::SetHideWhenInsufficientScrollPos (bool bNewHideWhenInsufficientScrollPos)
{
	bHideWhenInsufficientScrollPos = bNewHideWhenInsufficientScrollPos;
	EvaluateScrollVisibility();
}

void ScrollbarComponent_Deprecated::SetMiddleMousePosition (MousePointer* mouse, const Vector2& newPosition)
{
	if (newPosition == MiddleMousePosition || mouse == nullptr)
	{
		return;
	}

	MiddleMousePosition = newPosition;
	if (MiddleMouseSprite != nullptr && MiddleMouseSpriteOwner != nullptr)
	{
		MiddleMouseSpriteOwner->SetPosition(newPosition);
		MiddleMouseSpriteOwner->SetPivot(MiddleMouseSprite->GetTextureSize() * 0.5f);
		MiddleMouseSpriteOwner->SetVisibility(newPosition.X >= 0 && newPosition.Y >= 0);
	}

	if (MiddleMousePosition.X < 0 && MiddleMousePosition.Y < 0)
	{
		//Restore original mouse cursor
		mouse->SetMouseVisibility(true);
		InteractingMouse = nullptr;
		bClampingMousePointer = false;
		if (PanningMiddleMouseCursor != nullptr)
		{
			RemoveComponent(PanningMiddleMouseCursor.Get());
			PanningMiddleMouseCursor->Destroy();
			PanningMiddleMouseCursor = nullptr;
		}

		//Disable middle mouse pan timer
		MiddleMousePanTimeRemaining = -1.f;
	}
	else
	{
		//We hide the mouse cursor to implement additional functionality (such as overriding appearance even when hovering over other components and subdivisions).
		mouse->SetMouseVisibility(false);
		InteractingMouse = mouse;
			
		//clamp the mouse pointer so that it cannot go beyond the parent component's boundaries
		GuiComponent* parentComponent = dynamic_cast<GuiComponent*>(GetOwner());
		if (parentComponent != nullptr && bClampsMouseWhenScrolling)
		{
			const Vector2& parentCoordinates = parentComponent->ReadCachedAbsPosition();
			Rectangle<INT> limitRegion(parentCoordinates.X.ToINT(), parentCoordinates.Y.ToINT(), parentCoordinates.X.ToINT() + parentComponent->ReadCachedAbsSize().X.ToINT(), parentCoordinates.Y.ToINT() + parentComponent->ReadCachedAbsSize().Y.ToINT(), Rectangle<INT>::CS_XRightYDown);
			mouse->PushPositionLimit(limitRegion, SDFUNCTION_2PARAM(this, ScrollbarComponent_Deprecated, HandleLimitCallback, bool, MousePointer*, const sf::Event::MouseMoveEvent&));
			bClampingMousePointer = true;
		}

		InitializePanningMiddleMouse(mouse);

		MiddleMousePanTimeRemaining = CalculateMiddleMouseMoveInterval();
	}
}

void ScrollbarComponent_Deprecated::SetScrollButtonHeight (FLOAT newScrollButtonHeight)
{	
	if (TopScrollButton != nullptr)
	{
		TopScrollButton->SetSize(Vector2(ReadSize().X, newScrollButtonHeight));
	}

	if (BottomScrollButton != nullptr)
	{
		BottomScrollButton->SetSize(Vector2(ReadSize().X, newScrollButtonHeight));
	}

	if (Track != nullptr)
	{
		//Ensure the track component is between the two buttons
		Track->SetSize(Vector2(ReadSize().X, ReadSize().Y - (newScrollButtonHeight * 2.f)));
		Track->SetPosition(Vector2(0.f, newScrollButtonHeight));
	}
}

INT ScrollbarComponent_Deprecated::GetScrollPosition () const
{
	return ScrollPosition;
}

INT ScrollbarComponent_Deprecated::GetMaxScrollPosition () const
{
	return MaxScrollPosition;
}

INT ScrollbarComponent_Deprecated::GetLastVisibleScrollPosition () const
{
	return ScrollPosition + NumVisibleScrollPositions - 1;
}
	
INT ScrollbarComponent_Deprecated::GetNumVisibleScrollPositions () const
{
	return NumVisibleScrollPositions;
}
	
bool ScrollbarComponent_Deprecated::GetEnabled () const
{
	return bEnabled;
}

bool ScrollbarComponent_Deprecated::GetHideWhenInsufficientScrollPos () const
{
	return bHideWhenInsufficientScrollPos;
}

Vector2 ScrollbarComponent_Deprecated::GetMiddleMousePosition () const
{
	return MiddleMousePosition;
}

void ScrollbarComponent_Deprecated::InitializeScrollButtons ()
{
	LabelComponent* buttonCaption = nullptr;
	if (TopScrollButton == nullptr)
	{
		TopScrollButton = ButtonComponent::CreateObject();
		if (AddComponent(TopScrollButton.Get()))
		{
			if (TopScrollButton->GetRenderComponent() != nullptr)
			{
				//TopScrollButton->GetRenderComponent()->SetSpriteTexture(GuiTheme::GetGuiTheme()->ScrollbarTopButton.Get());
			}

			TopScrollButton->SetSize(Vector2(ReadSize().X, ReadSize().X));
			TopScrollButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			TopScrollButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, ScrollbarComponent_Deprecated::HandleScrollButtonPressed, void, ButtonComponent*));
			TopScrollButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, ScrollbarComponent_Deprecated::HandleScrollButtonReleased, void, ButtonComponent*));

			buttonCaption = TopScrollButton->CaptionComponent.Get();
			if (buttonCaption != nullptr)
			{
				buttonCaption->Destroy();
				buttonCaption = nullptr;
			}
		}
	}

	if (BottomScrollButton == nullptr)
	{
		BottomScrollButton = ButtonComponent::CreateObject();
		if (AddComponent(BottomScrollButton.Get()))
		{
			if (BottomScrollButton->GetRenderComponent() != nullptr)
			{
				//BottomScrollButton->GetRenderComponent()->SetSpriteTexture(GuiTheme::GetGuiTheme()->ScrollbarBottomButton.Get());
			}

			BottomScrollButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			BottomScrollButton->SetSize(Vector2(ReadSize().X, ReadSize().X));
			BottomScrollButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, ScrollbarComponent_Deprecated::HandleScrollButtonPressed, void, ButtonComponent*));
			BottomScrollButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarComponent_Deprecated, ScrollbarComponent_Deprecated::HandleScrollButtonReleased, void, ButtonComponent*));

			buttonCaption = BottomScrollButton->CaptionComponent.Get();
			if (buttonCaption != nullptr)
			{
				buttonCaption->Destroy();
				buttonCaption = nullptr;
			}
		}
	}

	EvaluateScrollButtonEnabledness();
}

void ScrollbarComponent_Deprecated::InitializeTrackComponent ()
{
	if (Track == nullptr)
	{
		Track = ScrollbarTrackComponent::CreateObject();
		Track->SetSize(Vector2(ReadSize().X, ReadSize().Y - (TopScrollButton->ReadCachedAbsSize().Y * 2.f)));
		Track->SetPosition(Vector2(0.f, TopScrollButton->ReadCachedAbsSize().Y));
		Track->SetThumbChangedPositionHandler(std::bind(&ScrollbarComponent_Deprecated::HandleTrackChangedPosition, this, std::placeholders::_1));
		AddComponent(Track.Get());
	}
}

void ScrollbarComponent_Deprecated::InitializePanningMiddleMouse (MousePointer* mouse)
{
	PanningMiddleMouseCursorOwner = GuiComponent::CreateObject();
	if (AddComponent(PanningMiddleMouseCursorOwner.Get()))
	{
		PanningMiddleMouseCursor = SpriteComponent::CreateObject();
		if (PanningMiddleMouseCursorOwner->AddComponent(PanningMiddleMouseCursor.Get()))
		{
			Texture* scrollbarPointer = GuiTheme::GetGuiTheme()->ScrollbarPointer.Get();
			PanningMiddleMouseCursor->SetSpriteTexture(scrollbarPointer);

			Vector2 spriteSize;
			scrollbarPointer->GetDimensions(OUT spriteSize);
			PanningMiddleMouseCursorOwner->SetSize(spriteSize * Vector2(1.f, 0.5f)); //flatten the sprite to be half size horizontally
			PanningMiddleMouseCursorOwner->SetPosition(mouse->GetMousePosition());

			//Set the pivot point to be centered based on sprite size
			PanningMiddleMouseCursorOwner->SetPivot(spriteSize * 0.5f);
		}
		else
		{
			PanningMiddleMouseCursorOwner->Destroy();
		}
	}

	MiddleMousePanDirection = 0;
}

void ScrollbarComponent_Deprecated::PanByMiddleMouse ()
{
	if (MiddleMousePosition.X < 0 && MiddleMousePosition.Y < 0)
	{
		return;
	}

	if (InteractingMouse == nullptr)
	{
		return;
	}

	if (MiddleMousePanDirection != 0) //If the user is not hovering over middle mouse position
	{
		FLOAT mousePosX;
		FLOAT mousePosY;
		InteractingMouse->GetMousePosition(mousePosX, mousePosY);

		INT scrollDirection = (mousePosY > MiddleMousePosition.Y) ? 1 : -1;
		IncrementScrollPosition(ScrollJumpInterval * scrollDirection);
	}

	MiddleMousePanTimeRemaining = CalculateMiddleMouseMoveInterval();
}

void ScrollbarComponent_Deprecated::PanByButtonPress ()
{
	//User must have pressed the scroll button somehow.  This shouldn't happen unless one of the buttons are destroyed.
	CHECK(TopScrollButton != nullptr && BottomScrollButton != nullptr)

	INT scrollAmount = ScrollJumpInterval;
	if (TopScrollButton->GetPressedDown())
	{
		//Reverse direction
		scrollAmount *= -1;
	}
	else if (!BottomScrollButton->GetPressedDown())
	{
		//Neither button is held
		return;
	}

	IncrementScrollPosition(scrollAmount);
	ButtonPanTimeRemaining = ContinuousInterval;
}

void ScrollbarComponent_Deprecated::EvaluateScrollButtonEnabledness ()
{
	return; //may occasionally crash on shutdown. This class is deprecated anyways. (It's because the texture resouce for the button state could be null if the theme is destroyed after the texture pool.
	if (TopScrollButton != nullptr)
	{
		TopScrollButton->SetEnabled(bEnabled && NumVisibleScrollPositions < MaxScrollPosition);
	}

	if (BottomScrollButton != nullptr)
	{
		BottomScrollButton->SetEnabled(bEnabled && NumVisibleScrollPositions < MaxScrollPosition);
	}
}

void ScrollbarComponent_Deprecated::EvaluateScrollVisibility ()
{
	bool bShouldBeVisible = (!bHideWhenInsufficientScrollPos || NumVisibleScrollPositions < MaxScrollPosition);
	if (bShouldBeVisible != bScrollbarVisible)
	{
		bScrollbarVisible = bShouldBeVisible;
		SetVisibility(bShouldBeVisible);

		if (OnToggleVisibility.IsBounded())
		{
			OnToggleVisibility();
		}
	}
}

FLOAT ScrollbarComponent_Deprecated::CalculateMiddleMouseMoveInterval ()
{
	CHECK(InteractingMouse != nullptr)
	if (MiddleMouseScrollIntervalRange.IsConverged())
	{
		//No use calculating the interval if the min/max are equal
		return (MiddleMouseScrollIntervalRange.Min);
	}

	INT mousePosX;
	INT mousePosY;
	InteractingMouse->GetMousePosition(mousePosX, mousePosY);

	FLOAT relativeDistance = 0.f;
	FLOAT distRatio;
	if (MiddleMousePanDirection < 0) //scrolling down
	{
		relativeDistance = mousePosY.ToFLOAT() - MiddleMousePosition.Y;
		distRatio = relativeDistance / (ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y - MiddleMousePosition.Y);
	}
	else if (MiddleMousePanDirection > 0) //scrolling up
	{
		relativeDistance = mousePosY.ToFLOAT() - MiddleMousePosition.Y;
		distRatio = relativeDistance / (ReadCachedAbsPosition().Y - MiddleMousePosition.Y);
	}
	else
	{
		return 0.5f; //check periodically
	}

	//safety check to avoid setting timer to 0 seconds
	distRatio = Utils::Clamp<FLOAT>(distRatio, 0.01f, 0.99f);

	return Utils::Lerp(1-distRatio, MiddleMouseScrollIntervalRange.Min, MiddleMouseScrollIntervalRange.Max);
}

void ScrollbarComponent_Deprecated::HandleScrollButtonPressed (ButtonComponent* uiComponent)
{
	bHoldingScrollButtons = true;
	INT scrollDirection = (uiComponent == TopScrollButton) ? -1 : 1;
	IncrementScrollPosition(ScrollJumpInterval * scrollDirection);
	ButtonPanTimeRemaining = ContinuousScrollDelay;
}

void ScrollbarComponent_Deprecated::HandleScrollButtonReleased (ButtonComponent* uiComponent)
{
	bHoldingScrollButtons = false;
	ButtonPanTimeRemaining = -1.f; //Disable button pan timer
}

void ScrollbarComponent_Deprecated::HandleTrackChangedPosition (INT newScrollPosition)
{
	//Do SetScrollPosition without refreshing the track component
	newScrollPosition = Utils::Clamp<INT>(newScrollPosition, 0, MaxScrollPosition - NumVisibleScrollPositions);
	if (newScrollPosition == ScrollPosition)
	{
		return;
	}

	ScrollPosition = newScrollPosition;

	if (OnScrollPositionChanged != nullptr)
	{
		OnScrollPositionChanged(ScrollPosition);
	}
}

bool ScrollbarComponent_Deprecated::HandleLimitCallback (MousePointer* mouse, const sf::Event::MouseMoveEvent& mouseEvent)
{
	return (bClampingMousePointer);
}

void ScrollbarComponent_Deprecated::HandleTick (FLOAT deltaSec)
{
	if (InteractingMouse != nullptr && MiddleMousePanTimeRemaining > 0.f)
	{
		MiddleMousePanTimeRemaining -= deltaSec;
		if (MiddleMousePanTimeRemaining <= 0.f)
		{
			PanByMiddleMouse();
		}
	}

	if (ButtonPanTimeRemaining > 0.f)
	{
		ButtonPanTimeRemaining -= deltaSec;
		if (ButtonPanTimeRemaining <= 0.f)
		{
			PanByButtonPress();
		}
	}
}
SD_END