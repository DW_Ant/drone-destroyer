/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScrollbarTrackComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, ScrollbarTrackComponent, SD, GuiComponent)

void ScrollbarTrackComponent::InitProps ()
{
	Super::InitProps();

	OwningScrollbar = nullptr;
	TrackColor = nullptr;
	TravelTrackColorOwner = nullptr;
	TravelTrackColor = nullptr;
	Thumb = nullptr;
	TravelDelay = 0.75f;
	TravelInterval = 0.2f;

	bEnabled = true;
	bScaleThumb = true;
	DraggingThumbPointerOffset = -1;
	TravelDirection = 0;
	TravelTimeRemaining = -1.f;
	OnThumbChangedPosition = nullptr;
	RelevantMouse = nullptr;
}

void ScrollbarTrackComponent::BeginObject ()
{
	Super::BeginObject();

	RefreshScrollbarTrack();
}

void ScrollbarTrackComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ScrollbarTrackComponent* trackTemplate = dynamic_cast<const ScrollbarTrackComponent*>(objTemplate);
	if (trackTemplate != nullptr)
	{
		TravelDelay = trackTemplate->TravelDelay;
		TravelInterval = trackTemplate->TravelInterval;
		SetScaleThumb(trackTemplate->GetScaleThumb());

		bool bCreatedObj;
		TrackColor = ReplaceTargetWithObjOfMatchingClass(TrackColor.Get(), trackTemplate->TrackColor.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(TrackColor.Get());
		}
		
		if (TrackColor != nullptr)
		{
			TrackColor->CopyPropertiesFrom(trackTemplate->TrackColor.Get());
		}

		TravelTrackColor = ReplaceTargetWithObjOfMatchingClass(TravelTrackColor.Get(), trackTemplate->TravelTrackColor.Get(), OUT bCreatedObj);
		if (bCreatedObj)
		{
			AddComponent(TravelTrackColor.Get());
		}

		if (TravelTrackColor != nullptr)
		{
			TravelTrackColor->CopyPropertiesFrom(trackTemplate->TravelTrackColor.Get());
		}

		Thumb = ReplaceTargetWithObjOfMatchingClass(Thumb.Get(), trackTemplate->Thumb.Get(), bCreatedObj);
		if (bCreatedObj && AddComponent(Thumb.Get()))
		{
			Thumb->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarTrackComponent, HandleThumbPressed, void, ButtonComponent*));
			Thumb->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarTrackComponent, HandleThumbReleased, void, ButtonComponent*));
		}

		if (Thumb != nullptr)
		{
			Thumb->CopyPropertiesFrom(trackTemplate->Thumb.Get());
		}
	}
}

void ScrollbarTrackComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwningScrollbar = dynamic_cast<ScrollbarComponent_Deprecated*>(newOwner);
}

void ScrollbarTrackComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	TickComponent* tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, ScrollbarTrackComponent, HandleTick, void, FLOAT));
	}

	TrackColor = SolidColorRenderComponent::CreateObject();
	if (AddComponent(TrackColor.Get()))
	{
		TrackColor->SolidColor = sf::Color(96, 96, 96, 255);
	}

	TravelTrackColorOwner = GuiComponent::CreateObject();
	if (AddComponent(TravelTrackColorOwner.Get()))
	{
		TravelTrackColor = SolidColorRenderComponent::CreateObject();
		if (TravelTrackColorOwner->AddComponent(TravelTrackColor.Get()))
		{
			TravelTrackColorOwner->SetVisibility(false);
			TravelTrackColor->SolidColor = sf::Color(48, 48, 48, 255);
		}
		else
		{
			TravelTrackColorOwner->Destroy();
		}
	}

	InitializeThumb();
}

void ScrollbarTrackComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	RelevantMouse = mouse;

	if (TravelDirection != 0)
	{
		if (TravelTrackColor != nullptr)
		{
			CalculateTravelTrackAttributes(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
		}
		return;
	}

	if (DraggingThumbPointerOffset < 0 || Thumb == nullptr)
	{
		return;
	}

	FLOAT newThumbPosY = (sfmlEvent.y - DraggingThumbPointerOffset).ToFLOAT();
	Thumb->SetPosition(Vector2(ReadPosition().X, newThumbPosY));
	OnThumbChangedPosition(CalculateScrollIndex());
}

bool ScrollbarTrackComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	if (eventType == sf::Event::MouseButtonReleased)
	{
		if (DraggingThumbPointerOffset < 0 && TravelDirection == 0)
		{
			return false;
		}

		DraggingThumbPointerOffset = -1;
		TravelDirection = 0;

		if (TravelTrackColorOwner != nullptr)
		{
			TravelTrackColorOwner->SetVisibility(false);
		}

		return true;
	}

	if (sfmlEvent.button != sf::Mouse::Left)
	{
		return false;
	}

	if (!bEnabled || !IsVisible() || !IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))) || Thumb == nullptr)
	{
		return false;
	}

	TravelDirection = (sfmlEvent.y > Thumb->ReadCachedAbsPosition().Y.ToINT()) ? -1 : 1;

	if (TravelTrackColorOwner != nullptr)
	{
		TravelTrackColorOwner->SetVisibility(true);
		CalculateTravelTrackAttributes(INT(sfmlEvent.x).ToFLOAT(), INT(sfmlEvent.y).ToFLOAT());
	}
	
	TravelTimeRemaining = TravelDelay;
	return true;
}

bool ScrollbarTrackComponent::AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const
{
	return (bEnabled && Super::AcceptsMouseEvents(mousePosX, mousePosY));
}

void ScrollbarTrackComponent::HandleSizeChange ()
{
	Super::HandleSizeChange();

	//Need to readjust scaling, position, and position clamps for the thumb.
	RefreshScrollbarTrack();
}

void ScrollbarTrackComponent::RefreshScrollbarTrack ()
{
	if (RelevantMouse == nullptr)
	{
		return;
	}

	if (Thumb != nullptr)
	{
		CalculateThumbScale();
		CalculateThumbPositionClamps();
		CalculateThumbPosition();
		ReevaluateThumbVisibility();
	}

	INT mousePosX;
	INT mousePosY;
	RelevantMouse->GetMousePosition(mousePosX, mousePosY);

	CalculateTravelTrackAttributes(mousePosX.ToFLOAT(), mousePosY.ToFLOAT());
}

void ScrollbarTrackComponent::SetThumbChangedPositionHandler (std::function<void(INT newScrollIndex)> newHandler)
{
	OnThumbChangedPosition = newHandler;
}

void ScrollbarTrackComponent::SetEnabled (bool bNewEnabled)
{
	bEnabled = bNewEnabled;

	if (!bEnabled)
	{
		if (TravelDirection != 0)
		{
			TravelTimeRemaining = -1.f;
		}
		TravelDirection = 0;
			
		if (TravelTrackColorOwner != nullptr)
		{
			TravelTrackColorOwner->SetVisibility(false);
		}

		DraggingThumbPointerOffset = -1;
	}

	ReevaluateThumbVisibility();
}

void ScrollbarTrackComponent::SetScaleThumb (bool bNewScaleThumb)
{
	bScaleThumb = bNewScaleThumb;

	if (!bScaleThumb && Thumb != nullptr && Thumb->GetRenderComponent() != nullptr)
	{
		Vector2 textureSize = Thumb->GetRenderComponent()->GetTextureSize();

		//restore default scale
		Thumb->SetSize(textureSize);
	}
	else
	{
		CalculateThumbScale();
	}
}

bool ScrollbarTrackComponent::ReversedDirections () const
{
	if (RelevantMouse == nullptr)
	{
		return false;
	}

	FLOAT mousePosX;
	FLOAT mousePosY;
	RelevantMouse->GetMousePosition(mousePosX, mousePosY);

	//bReversedDirection becomes true when a user clicks above/below the thumb, then switches sides as the thumb travels.
	bool bReversedDir = (TravelDirection > 0 && mousePosY > Thumb->ReadCachedAbsPosition().Y); //mouse below the thumb as thumb travels up
	bReversedDir = bReversedDir || (TravelDirection < 0 && mousePosY < Thumb->ReadCachedAbsPosition().Y + Thumb->ReadCachedAbsSize().Y); //mouse above thumb as thumb travels down

	return bReversedDir;
}

bool ScrollbarTrackComponent::GetEnabled () const
{
	return bEnabled;
}

bool ScrollbarTrackComponent::GetScaleThumb () const
{
	return bScaleThumb;
}

void ScrollbarTrackComponent::InitializeThumb ()
{
	if (Thumb == nullptr)
	{
		Thumb = ButtonComponent::CreateObject();
		if (AddComponent(Thumb.Get()))
		{
			if (Thumb->GetRenderComponent() != nullptr)
			{
				Thumb->GetBackground()->RenderComponent->SetSpriteTexture(GuiTheme::GetGuiTheme()->ScrollbarTrackFill.Get());
				Thumb->GetBackground()->RenderComponent->TopBorder = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderTop;
				Thumb->GetBackground()->RenderComponent->RightBorder = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderRight;
				Thumb->GetBackground()->RenderComponent->BottomBorder = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderBottom;
				Thumb->GetBackground()->RenderComponent->LeftBorder = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderLeft;
				Thumb->GetBackground()->RenderComponent->TopRightCorner = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderTopRight;
				Thumb->GetBackground()->RenderComponent->BottomRightCorner = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderBottomRight;
				Thumb->GetBackground()->RenderComponent->BottomLeftCorner = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderBottomLeft;
				Thumb->GetBackground()->RenderComponent->TopLeftCorner = GuiTheme::GetGuiTheme()->ScrollbarTrackBorderTopLeft;
				Thumb->GetBackground()->SetBorderThickness(2);
				Thumb->GetBackground()->RenderComponent->FillColor = Color(64, 64, 64, 255);
			}

			Thumb->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, ScrollbarTrackComponent, ScrollbarTrackComponent::HandleThumbPressed, void, ButtonComponent*));
			Thumb->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, ScrollbarTrackComponent, ScrollbarTrackComponent::HandleThumbReleased, void, ButtonComponent*));

			LabelComponent* buttonCaption = Thumb->CaptionComponent.Get();
			if (buttonCaption != nullptr)
			{
				buttonCaption->Destroy();
			}
		}
	}
}

void ScrollbarTrackComponent::TravelByThumbDirection ()
{
	if (TravelDirection == 0 || OwningScrollbar == nullptr)
	{
		//Thumb is no longer panning
		return;
	}

	if (!ReversedDirections())
	{
		OwningScrollbar->IncrementScrollPosition(OwningScrollbar->GetNumVisibleScrollPositions() * TravelDirection * -1);
	}

	TravelTimeRemaining = TravelInterval;
}

void ScrollbarTrackComponent::ReevaluateThumbVisibility ()
{
	if (OwningScrollbar != nullptr)
	{
		Thumb->SetVisibility(bEnabled && OwningScrollbar->GetNumVisibleScrollPositions() < OwningScrollbar->GetMaxScrollPosition());
	}
}

void ScrollbarTrackComponent::CalculateThumbPositionClamps ()
{
	//Ensure the thumb remains between the two buttons.
	//TODO:  Identify alternatives to clamping the thumb
	//FLOAT thumbHeight = Thumb->ReadCachedAbsSize().Y;
	//Thumb->SetAbsCoordinatesYClamps(Range<FLOAT>(0.f, GetTransform()->GetCurrentSizeY() - thumbHeight));
}

void ScrollbarTrackComponent::CalculateThumbPosition ()
{
	if (OwningScrollbar == nullptr)
	{
		return;
	}

	if (OwningScrollbar->GetMaxScrollPosition() <= 0)
	{
		//There's nothing to scroll here (the Thumb should be invisible at this point)
		return;
	}

	FLOAT percentScrolled = (OwningScrollbar->GetScrollPosition().ToFLOAT() / OwningScrollbar->GetMaxScrollPosition().ToFLOAT());
	FLOAT thumbAbsPosY = (percentScrolled * (ReadSize().Y));
	Thumb->SetPosition(Vector2(0.f, thumbAbsPosY));
}

void ScrollbarTrackComponent::CalculateThumbScale ()
{
	if (!bScaleThumb || OwningScrollbar == nullptr)
	{
		return;
	}

	if (OwningScrollbar->GetMaxScrollPosition() <= 0)
	{
		//There's nothing to scroll here
		return;
	}

	FLOAT scaleRatio = Utils::Clamp<FLOAT>((OwningScrollbar->GetNumVisibleScrollPositions().ToFLOAT() / OwningScrollbar->GetMaxScrollPosition().ToFLOAT()), 0.05f, 1.f);
	FLOAT newSize = (scaleRatio * ReadCachedAbsSize().Y);
	Thumb->SetSize(Vector2(ReadSize().X, newSize));
}

INT ScrollbarTrackComponent::CalculateScrollIndex ()
{
	if (Thumb == nullptr)
	{
		return 0;
	}

	FLOAT thumbPosY = Thumb->ReadCachedAbsPosition().Y;
	FLOAT percentScrolled = thumbPosY / ReadCachedAbsSize().Y;

	//TODO:  It's impossible to scroll for very small thumb due to the 5% minimum scale requirement.
	return FLOAT(round((OwningScrollbar->GetMaxScrollPosition().ToFLOAT() * percentScrolled).Value)).ToINT();
}

bool ScrollbarTrackComponent::CalculateTravelTrackAttributes (FLOAT mousePosX, FLOAT mousePosY)
{
	CHECK(TravelTrackColorOwner != nullptr)

	bool bStopped = (TravelDirection == 0 || ReversedDirections());
	TravelTrackColorOwner->SetVisibility(!bStopped);

	if (bStopped)
	{
		//Thumb finished traveling to mouse pointer
		return false;
	}

	//clamp mousePosY to be within track size
	mousePosY = Utils::Clamp(mousePosY, ReadCachedAbsPosition().Y, ReadCachedAbsPosition().Y + ReadCachedAbsSize().Y);

	if (TravelDirection > 0) //scrolling up
	{
		FLOAT absSizeY = Thumb->ReadCachedAbsPosition().Y - mousePosY;
		TravelTrackColorOwner->SetSize(Vector2(ReadSize().X, absSizeY));
		TravelTrackColorOwner->SetPosition(Vector2(ReadCachedAbsPosition().X, mousePosY));
	}
	else //scrolling down
	{
		FLOAT absSizeY = mousePosY - (Thumb->ReadCachedAbsPosition().Y + Thumb->ReadCachedAbsSize().Y);
		TravelTrackColorOwner->SetSize(Vector2(ReadSize().X, absSizeY));
		TravelTrackColorOwner->SetPosition(Vector2(ReadCachedAbsPosition().X, Thumb->ReadCachedAbsPosition().Y + Thumb->ReadCachedAbsSize().Y));
	}

	return true;
}

void ScrollbarTrackComponent::HandleThumbPressed (ButtonComponent* uiComponent)
{
	CHECK(uiComponent != nullptr)
	if (RelevantMouse == nullptr)
	{
		return;
	}

	INT mousePosX;
	INT mousePosY;
	RelevantMouse->GetMousePosition(mousePosX, mousePosY);

	DraggingThumbPointerOffset = mousePosY - uiComponent->ReadCachedAbsPosition().Y.ToINT();
}

void ScrollbarTrackComponent::HandleThumbReleased (ButtonComponent* uiComponent)
{
	DraggingThumbPointerOffset = -1;
}

void ScrollbarTrackComponent::HandleTick (FLOAT deltaSec)
{
	if (TravelTimeRemaining > 0.f)
	{
		TravelTimeRemaining -= deltaSec;
		if (TravelTimeRemaining <= 0.f)
		{
			TravelByThumbDirection();
		}
	}
}
SD_END