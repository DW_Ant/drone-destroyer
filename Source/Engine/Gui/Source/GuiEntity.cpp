/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiEntity.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, GuiEntity, SD, Entity)

const FLOAT GuiEntity::DEFAULT_AUTO_SIZE_CHECK_FREQUENCY = 0.67f;

void GuiEntity::InitProps ()
{
	Super::InitProps();

	Focus = nullptr;
	Input = nullptr;
	AutoSizeHorizontal = false;
	AutoSizeVertical = false;
	GuiSizeToOwningScrollbar = Vector2(-1.f, -1.f);
}

void GuiEntity::BeginObject ()
{
	Super::BeginObject();

	Tick = TickComponent::CreateObject(TICK_GROUP_GUI);
	if (AddComponent(Tick.Get()))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, GuiEntity, HandleTick, void, FLOAT));
		Tick->SetTickInterval(DEFAULT_AUTO_SIZE_CHECK_FREQUENCY);
		Tick->SetTicking(AutoSizeHorizontal || AutoSizeVertical);
	}

	ConstructUI();
	InitializeFocusComponent();
}

bool GuiEntity::AddComponent (EntityComponent* newComponent, bool bLogOnFail, bool bDeleteOnFail)
{
	bool success = Super::AddComponent(newComponent, bLogOnFail, bDeleteOnFail);
	if (success && !StyleName.IsEmpty())
	{
		if (GuiComponent* guiComp = dynamic_cast<GuiComponent*>(newComponent))
		{
			guiComp->ApplyStyle(StyleName);
		}
	}

	return success;
}

void GuiEntity::RegisterToDrawLayer (ScrollbarComponent* scrollbar, RenderTexture* drawLayerOwner)
{
	std::vector<RenderTarget::SDrawLayerCamera>& drawLayers = drawLayerOwner->EditDrawLayers();

	//Find the GuiEntity draw layer
	for (UINT_TYPE i = 0; i < drawLayers.size(); ++i)
	{
		GuiDrawLayer* guiLayer = dynamic_cast<GuiDrawLayer*>(drawLayers.at(i).Layer);
		if (guiLayer != nullptr)
		{
			guiLayer->RegisterMenu(this);
			return;
		}
	}

	//GuiLayer is not found.  Add one to the texture.
	GuiDrawLayer* newLayer = GuiDrawLayer::CreateObject();
	drawLayerOwner->RegisterDrawLayer(newLayer, scrollbar->GetFrameCamera());
}

void GuiEntity::UnregisterFromDrawLayer (RenderTexture* drawLayerOwner)
{
	std::vector<RenderTarget::SDrawLayerCamera>& drawLayers = drawLayerOwner->EditDrawLayers();

	for (UINT_TYPE i = 0; i < drawLayers.size(); ++i)
	{
		GuiDrawLayer* guiLayer = dynamic_cast<GuiDrawLayer*>(drawLayers.at(i).Layer);
		if (guiLayer != nullptr)
		{
			guiLayer->UnregisterMenu(this);
			return;
		}
	}
}

void GuiEntity::GetTotalSize (Vector2& outSize) const
{
	outSize = GetCachedAbsSize();
}

void GuiEntity::HandleScrollbarFrameSizeChange (const Vector2& oldFrameSize, const Vector2& newFrameSize)
{
	Vector2 newSize = ReadSize();
	bool sizeChanged = false;

	if (GuiSizeToOwningScrollbar.X > 0.f)
	{
		newSize.X = (newFrameSize.X * GuiSizeToOwningScrollbar.X);
		sizeChanged = true;
	}

	if (GuiSizeToOwningScrollbar.Y > 0.f)
	{
		newSize.Y = (newFrameSize.Y * GuiSizeToOwningScrollbar.Y);
		sizeChanged = true;
	}

	if (sizeChanged)
	{
		SetSize(newSize);
	}
}

bool GuiEntity::ExecuteScrollableInterfaceInput (const sf::Event& evnt)
{
	return HandleInput(evnt);
}

bool GuiEntity::ExecuteScrollableInterfaceText (const sf::Event& evnt)
{
	return HandleText(evnt);
}

void GuiEntity::ExecuteScrollableInterfaceMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	HandleMouseMove(mouse, sfmlEvent, deltaMove);
}

bool GuiEntity::ExecuteScrollableInterfaceMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	return HandleMouseClick(mouse, sfmlEvent, eventType);
}

bool GuiEntity::ExecuteScrollableInterfaceMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	return HandleMouseWheelMove(mouse, sfmlEvent);
}

void GuiEntity::RemoveScrollableObject ()
{
	Destroy();
}

void GuiEntity::RegisterToMainWindow (bool registerToMainRender, bool registerToMainInput, INT inputPriority)
{
	if (registerToMainRender)
	{
		GuiEngineComponent* localGuiEngine = GuiEngineComponent::Find();
		CHECK(localGuiEngine != nullptr)

		GuiDrawLayer* mainOverlay = localGuiEngine->GetMainOverlay();
		if (mainOverlay != nullptr)
		{
			mainOverlay->RegisterMenu(this);
		}
		else
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to register %s to the Main Overlay since the local GUI Engine Component does not have a GUI draw layer."), ToString());
		}
	}

	if (registerToMainInput)
	{
		InputEngineComponent* localInput = InputEngineComponent::Find();
		CHECK(localInput != nullptr)

		InputBroadcaster* mainBroadcaster = localInput->GetMainBroadcaster();
		if (mainBroadcaster != nullptr)
		{
			SetupInputComponent(mainBroadcaster, inputPriority);
		}
		else
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("Failed to register %s to the main input broadcaster since the local InputEngineComponent does not have an input broadcaster bound to the main window."), ToString());
		}
	}
}

void GuiEntity::LoseFocus ()
{
	if (Focus != nullptr)
	{
		Focus->ClearSelection();
		if (InputComponent* focusInput = Focus->GetInput())
		{
			focusInput->SetInputEnabled(false);
		}
	}
}

void GuiEntity::GainFocus ()
{
	if (Focus != nullptr)
	{
		Focus->SelectNextEntity();
		if (InputComponent* focusInput = Focus->GetInput())
		{
			focusInput->SetInputEnabled(true);
		}
	}
}

void GuiEntity::SetupInputComponent (InputBroadcaster* broadcaster, INT inputPriority)
{
	if (Input != nullptr)
	{
		Input->Destroy();
	}

	if (broadcaster == nullptr)
	{
		return;
	}

	Input = InputComponent::CreateObject();
	if (AddComponent(Input.Get()))
	{
		broadcaster->AddInputComponent(Input.Get(), inputPriority);
		Input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, GuiEntity, HandleInput, bool, const sf::Event&);
		Input->CaptureTextDelegate = SDFUNCTION_1PARAM(this, GuiEntity, HandleText, bool, const sf::Event&);
		Input->MouseMoveDelegate = SDFUNCTION_3PARAM(this, GuiEntity, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
		Input->MouseClickDelegate = SDFUNCTION_3PARAM(this, GuiEntity, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
		Input->MouseWheelScrollDelegate = SDFUNCTION_2PARAM(this, GuiEntity, HandleMouseWheelMove, bool, MousePointer*, const sf::Event::MouseWheelScrollEvent&);
	}
}

void GuiEntity::RefreshEntitySize ()
{
	if (!AutoSizeHorizontal && !AutoSizeVertical)
	{
		//Auto sizing is disabled
		return;
	}

	Vector2 newSize(1.f, 1.f);

	//Only iterate through immediate planar transform components to identify size.
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		PlanarTransform* transform = dynamic_cast<PlanarTransform*>(iter.GetSelectedComponent());
		if (transform != nullptr)
		{
			//Ignore transforms that scale relative to its owner (since their owner size is scaled based on its components).
			if (!transform->GetEnableFractionScaling() || transform->ReadSize().X > 1.f)
			{
				newSize.X = Utils::Max(newSize.X, transform->ReadPosition().X + transform->ReadSize().X);
			}

			if (!transform->GetEnableFractionScaling() || transform->ReadSize().Y > 1.f)
			{
				newSize.Y = Utils::Max(newSize.Y, transform->ReadPosition().Y + transform->ReadSize().Y);
			}
		}
	}

	//Reset certain axis of the new size if their corresponding AutoSizing flag is disabled.
	if (!AutoSizeHorizontal)
	{
		newSize.X = ReadSize().X;
	}

	if (!AutoSizeVertical)
	{
		newSize.Y = ReadSize().Y;
	}

	SetSize(newSize);

	if (Tick != nullptr)
	{
		Tick->ResetAccumulatedTickTime();
	}
}

void GuiEntity::SetAutoSizeHorizontal (bool newAutoSizeHorizontal)
{
	AutoSizeHorizontal = newAutoSizeHorizontal;
	if (Tick != nullptr)
	{
		Tick->SetTicking(AutoSizeHorizontal || AutoSizeVertical);
	}
}

void GuiEntity::SetAutoSizeVertical (bool newAutoSizeVertical)
{
	AutoSizeVertical = newAutoSizeVertical;
	if (Tick != nullptr)
	{
		Tick->SetTicking(AutoSizeHorizontal || AutoSizeVertical);
	}
}

void GuiEntity::SetGuiSizeToOwningScrollbar (const Vector2& newGuiSizeToOwningScrollbar)
{
	GuiSizeToOwningScrollbar = newGuiSizeToOwningScrollbar;
}

void GuiEntity::SetStyleName (const DString& newStyleName)
{
	StyleName = newStyleName;
}

FocusComponent* GuiEntity::GetFocus () const
{
	return Focus.Get();
}

FocusInterface* GuiEntity::GetFocusedObj () const
{
	return (Focus != nullptr) ? Focus->GetSelectedEntity() : nullptr;
}

void GuiEntity::ConstructUI ()
{
	//Noop
}

void GuiEntity::InitializeFocusComponent ()
{
	Focus = FocusComponent::CreateObject();
	if (AddComponent(Focus.Get()))
	{
		if (InputComponent* focusInput = Focus->GetInput())
		{
			focusInput->SetInputEnabled(false); //Wait until the GuiEntity is focused before enabling.
		}
	}
}

bool GuiEntity::HandleInput (const sf::Event& evnt)
{
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		//Relay input events to root GuiComponents only (root components relays input events to its children if needed).
		if (GuiComponent* guiComp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent()))
		{
			if (guiComp->ProcessInput(evnt))
			{
				return true;
			}
		}
	}

	return false;
}

bool GuiEntity::HandleText (const sf::Event& evnt)
{
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		//Relay input events to root GuiComponents only (root components relays input events to its children if needed).
		if (GuiComponent* guiComp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent()))
		{
			if (guiComp->ProcessText(evnt))
			{
				return true;
			}
		}
	}

	return false;
}

void GuiEntity::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		//Relay input events to root GuiComponents only (root components relays input events to its children if needed).
		GuiComponent* guiComp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent());
		if (guiComp != nullptr)
		{
			guiComp->ProcessMouseMove(mouse, sfmlEvent, deltaMove);
		}
	}
}

bool GuiEntity::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		//Relay input events to root GuiComponents only (root components relays input events to its children if needed).
		GuiComponent* guiComp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent());
		if (guiComp != nullptr && guiComp->ProcessMouseClick(mouse, sfmlEvent, eventType))
		{
			return true;
		}
	}

	return false;
}

bool GuiEntity::HandleMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
{
	for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		//Relay input events to root GuiComponents only (root components relays input events to its children if needed).
		GuiComponent* guiComp = dynamic_cast<GuiComponent*>(iter.GetSelectedComponent());
		if (guiComp != nullptr && guiComp->ProcessMouseWheelMove(mouse, sfmlEvent))
		{
			return true;
		}
	}

	return false;
}

void GuiEntity::HandleTick (FLOAT deltaSec)
{
	RefreshEntitySize();
}
SD_END