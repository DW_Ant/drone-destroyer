/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ButtonComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, ButtonComponent, SD, GuiComponent)

void ButtonComponent::InitProps ()
{
	Super::InitProps();

	CaptionComponent = nullptr;

	StateComponent = nullptr;
	Background = nullptr;
	bEnabled = true;
	bPressedDown = false;
	bHovered = false;
}

void ButtonComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const ButtonComponent* buttonTemplate = dynamic_cast<const ButtonComponent*>(objTemplate);
	if (buttonTemplate != nullptr)
	{
		if (GetButtonStateComponent() == nullptr && buttonTemplate->GetButtonStateComponent() != nullptr)
		{
			//This component is missing a state component and the template has one
			ButtonStateComponent* newState = ReplaceStateComponent(buttonTemplate->GetButtonStateComponent()->StaticClass());
			newState->CopyPropertiesFrom(buttonTemplate->GetButtonStateComponent());
		}
		else if (buttonTemplate->GetButtonStateComponent() == nullptr)
		{
			//This component has a button state component but template does not.
			ReplaceStateComponent(nullptr);
		}
		else if (buttonTemplate->GetButtonStateComponent()->StaticClass() != GetButtonStateComponent()->StaticClass())
		{
			//Both have state components but the classes are different.
			ButtonStateComponent* newState = ReplaceStateComponent(buttonTemplate->GetButtonStateComponent()->StaticClass());
			newState->CopyPropertiesFrom(buttonTemplate->GetButtonStateComponent());
		}

		bool bCreatedObject;
		CaptionComponent = ReplaceTargetWithObjOfMatchingClass(CaptionComponent.Get(), buttonTemplate->CaptionComponent.Get(), OUT bCreatedObject);
		if (bCreatedObject)
		{
			AddComponent(CaptionComponent.Get());
		}

		if (CaptionComponent != nullptr)
		{
			CaptionComponent->CopyPropertiesFrom(buttonTemplate->CaptionComponent.Get());
		}

		Background = ReplaceTargetWithObjOfMatchingClass(GetBackground(), buttonTemplate->GetBackground(), OUT bCreatedObject);
		if (bCreatedObject && AddComponent(Background.Get()) && Background->RenderComponent != nullptr)
		{
			Background->RenderComponent->OnTextureChangedCallbacks.RegisterHandler(SDFUNCTION(this, ButtonComponent, HandleSpriteTextureChanged, void));
		}

		if (Background != nullptr)
		{
			Background->CopyPropertiesFrom(buttonTemplate->GetBackground());
		}
	}
}

bool ButtonComponent::CanBeFocused () const
{
	return GetEnabled() && IsVisible();
}

void ButtonComponent::LoseFocus ()
{
	FocusInterface::LoseFocus();

	if (bPressedDown) //Cancel out of selection (Pressing tab while Return key is held)
	{
		SetButtonUp(false, false);
	}
}

bool ButtonComponent::CaptureFocusedInput (const sf::Event& keyEvent)
{
	if (keyEvent.key.code == sf::Keyboard::Return)
	{
		if (keyEvent.type == sf::Event::KeyPressed)
		{
			SetButtonDown(true, true);
			return true;
		}
		else if (keyEvent.type == sf::Event::KeyReleased)
		{
			SetButtonUp(true, true);
			return true;
		}
	}

	return false;
}

bool ButtonComponent::CaptureFocusedText (const sf::Event& keyEvent)
{
	return false;
}

void ButtonComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	InitializeBackground();
	InitializeCaptionComponent();
}

void ButtonComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
{
	Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

	bool bNewHovered = IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));

	if (bNewHovered != bHovered)
	{
		bHovered = bNewHovered;
		if (StateComponent != nullptr)
		{
			if (bHovered)
			{
				(bPressedDown) ? StateComponent->SetDownAppearance() : StateComponent->SetHoverAppearance();
			}
			else
			{
				StateComponent->SetDefaultAppearance();
			}
		}
	}
}

void ButtonComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);

	bool isRelevant = (sfmlEvent.button == sf::Mouse::Left || (sfmlEvent.button == sf::Mouse::Right && HasRightClickHandler()));
	if (!isRelevant || !IsVisible())
	{
		return;
	}

	if (bPressedDown && eventType == sf::Event::MouseButtonReleased)
	{
		SetButtonUp(IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))), sfmlEvent.button == sf::Mouse::Left);
	}
}

bool ButtonComponent::ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	if (Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType))
	{
		return true;
	}

	//Buttons only care for left mouse button (or right mouse button if there's a function bound to it)
	bool isRelevant = (sfmlEvent.button == sf::Mouse::Left || (sfmlEvent.button == sf::Mouse::Right && HasRightClickHandler()));
	if (!isRelevant || !IsVisible())
	{
		return false;
	}

	if (!bPressedDown && eventType == sf::Event::MouseButtonPressed && IsWithinBounds(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y))))
	{
		SetButtonDown(true, sfmlEvent.button == sf::Mouse::Left);
		return true;
	}

	return false;
}

bool ButtonComponent::AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const
{
	return (bEnabled && Super::AcceptsMouseEvents(mousePosX, mousePosY));
}

void ButtonComponent::SetButtonDown (bool bInvokeDelegate, bool isLeftClick)
{
	if (StateComponent != nullptr)
	{
		StateComponent->SetDownAppearance();
	}

	if (bInvokeDelegate)
	{
		if (isLeftClick && OnButtonPressed.IsBounded())
		{
			OnButtonPressed(this);
		}
		else if (!isLeftClick && OnButtonPressedRightClick.IsBounded())
		{
			OnButtonPressedRightClick(this);
		}
	}

	bPressedDown = true;
}

void ButtonComponent::SetButtonUp (bool bInvokeDelegate, bool isLeftClick)
{
	if (StateComponent != nullptr)
	{
		//Restore button state appearance regardless if the button was released beyond bounds or not
		(bHovered) ? StateComponent->SetHoverAppearance() : StateComponent->SetDefaultAppearance();
	}

	bPressedDown = false;

	if (bInvokeDelegate)
	{
		if (isLeftClick && OnButtonReleased.IsBounded())
		{
			OnButtonReleased(this);
		}
		else if (!isLeftClick && OnButtonReleasedRightClick.IsBounded())
		{
			OnButtonReleasedRightClick(this);
		}
	}
}

void ButtonComponent::SetButtonPressedHandler (SDFunction<void, ButtonComponent*> newHandler)
{
	OnButtonPressed = newHandler;
}

void ButtonComponent::SetButtonReleasedHandler (SDFunction<void, ButtonComponent*> newHandler)
{
	OnButtonReleased = newHandler;
}

void ButtonComponent::SetButtonPressedRightClickHandler (SDFunction<void, ButtonComponent*> newHandler)
{
	OnButtonPressedRightClick = newHandler;
}

void ButtonComponent::SetButtonReleasedRightClickHandler (SDFunction<void, ButtonComponent*> newHandler)
{
	OnButtonReleasedRightClick = newHandler;
}

void ButtonComponent::SetCaptionText (DString newText)
{
	if (CaptionComponent != nullptr)
	{
		CaptionComponent->SetText(newText);
	}
}

void ButtonComponent::SetBackground (FrameComponent* newBackground)
{
	if (Background != nullptr)
	{
		Background->Destroy();
	}

	Background = newBackground;
	if (Background != nullptr)
	{
		AddComponent(Background.Get());
	}
}

void ButtonComponent::SetEnabled (bool bNewEnabled)
{
	bEnabled = bNewEnabled;
	bHovered = false;
	bPressedDown = false;

	if (StateComponent != nullptr)
	{
		if (bEnabled)
		{
			(bHovered) ? StateComponent->SetHoverAppearance() : StateComponent->SetDefaultAppearance();
		}
		else
		{
			StateComponent->SetDisableAppearance();
		}
	}
}

ButtonStateComponent* ButtonComponent::ReplaceStateComponent (const DClass* newButtonStateClass)
{
	if (newButtonStateClass == nullptr)
	{
		//Simply destroy the old
		if (StateComponent != nullptr)
		{
			StateComponent->Destroy();
			StateComponent = nullptr;
		}

		return nullptr;
	}

	const ButtonStateComponent* defaultButtonState = dynamic_cast<const ButtonStateComponent*>(newButtonStateClass->GetDefaultObject());
	if (!defaultButtonState)
	{
		return nullptr;
	}

	ButtonStateComponent* oldButtonState = StateComponent.Get();
	StateComponent = defaultButtonState->CreateObjectOfMatchingClass();
	if (StateComponent == nullptr)
	{
		StateComponent = oldButtonState;
		return nullptr;
	}

	if (!AddComponent(StateComponent.Get()))
	{
		StateComponent->Destroy();
		StateComponent = oldButtonState;
		return nullptr;
	}

	if (oldButtonState != nullptr)
	{
		RemoveComponent(oldButtonState);
		oldButtonState->Destroy();
	}

	StateComponent->RefreshState();
	return StateComponent.Get();
}

bool ButtonComponent::GetEnabled () const
{
	return bEnabled;
}

bool ButtonComponent::GetHovered () const
{
	return bHovered;
}

bool ButtonComponent::GetPressedDown () const
{
	return bPressedDown;
}

DString ButtonComponent::GetCaptionText () const
{
	if (CaptionComponent != nullptr)
	{
		return CaptionComponent->GetContent();
	}

	return DString::EmptyString;
}

ButtonStateComponent* ButtonComponent::GetButtonStateComponent () const
{
	return StateComponent.Get();
}

FrameComponent* ButtonComponent::GetBackground () const
{
	return Background.Get();
}

BorderedSpriteComponent* ButtonComponent::GetRenderComponent () const
{
	if (Background != nullptr)
	{
		return Background->RenderComponent.Get();
	}

	return nullptr;
}

void ButtonComponent::InitializeBackground ()
{
	Background = FrameComponent::CreateObject();

	if (AddComponent(Background.Get()))
	{
		Background->SetLockedFrame(true);
		Background->SetBorderThickness(0);

		if (Background->RenderComponent != nullptr)
		{
			//Clear all borders since it's going to be a simple sprite
			Background->RenderComponent->TopBorder = nullptr;
			Background->RenderComponent->RightBorder = nullptr;
			Background->RenderComponent->BottomBorder = nullptr;
			Background->RenderComponent->LeftBorder = nullptr;
			Background->RenderComponent->TopRightCorner = nullptr;
			Background->RenderComponent->BottomRightCorner = nullptr;
			Background->RenderComponent->BottomLeftCorner = nullptr;
			Background->RenderComponent->TopLeftCorner = nullptr;
			Background->RenderComponent->SetSpriteTexture(GuiTheme::GetGuiTheme()->ButtonFill.Get());
			Background->RenderComponent->OnTextureChangedCallbacks.RegisterHandler(SDFUNCTION(this, ButtonComponent, HandleSpriteTextureChanged, void));
		}
	}
}

void ButtonComponent::InitializeCaptionComponent ()
{
	FLOAT borderThickness = (Background != nullptr) ? Background->GetBorderThickness() : 0;

	CaptionComponent = LabelComponent::CreateObject();
	GuiComponent* owningComponent = this;
	if (Background != nullptr)
	{
		//The text's owning component should be the background (if any) for draw priority reasons.
		owningComponent = Background.Get();
	}

	if (owningComponent->AddComponent(CaptionComponent.Get()))
	{
		CaptionComponent->SetAutoRefresh(false);

		CaptionComponent->SetSize(Vector2(1.f, 1.f));
		CaptionComponent->SetPosition(Vector2::ZeroVector);

		CaptionComponent->SetLineSpacing(0);
		CaptionComponent->SetHorizontalAlignment(LabelComponent::HA_Center);
		CaptionComponent->SetVerticalAlignment(LabelComponent::VA_Center);
		CaptionComponent->SetAutoRefresh(true);

		SetCaptionText(TXT("Button"));
	}
}

void ButtonComponent::HandleSpriteTextureChanged ()
{
	if (StateComponent != nullptr)
	{
		StateComponent->RefreshState();
	}
}
SD_END