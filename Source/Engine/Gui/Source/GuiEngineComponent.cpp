/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiEngineComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(SD, GuiEngineComponent)

GuiEngineComponent::GuiEngineComponent () : EngineComponent()
{
	DefaultThemeClass = GuiTheme::SStaticClass();
	MainOverlay = nullptr;
}

void GuiEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	Engine* localEngine = Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_GUI, TICK_GROUP_PRIORITY_GUI);

	MainOverlay = GuiDrawLayer::CreateObject();
	MainOverlay->SetDrawPriority(GuiDrawLayer::MAIN_OVERLAY_PRIORITY);
}

void GuiEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	GraphicsEngineComponent* graphicsEngine = GraphicsEngineComponent::Find();
	CHECK(graphicsEngine != nullptr)
	Window* mainWindow = graphicsEngine->GetPrimaryWindow();
	CHECK(mainWindow != nullptr)
	mainWindow->RegisterDrawLayer(MainOverlay.Get());

	ImportGuiEssentialTextures();
	ImportGuiEssentialFonts();

	if (!DefaultThemeClass->IsChildOf(GuiTheme::SStaticClass()))
	{
		Engine::FindEngine()->FatalError(DString::FormatString(TXT("Failed to initialize GuiEngineComponent.  The class of DefaultThemeClass (%s) is not a GuiTheme."), {DefaultThemeClass->ToString()}));
		return;
	}

	RegisteredGuiTheme = dynamic_cast<GuiTheme*>(DefaultThemeClass->GetDefaultObject()->CreateObjectOfMatchingClass());
	RegisteredGuiTheme->InitializeTheme();
	RegisteredGuiTheme->InitializeStyles();
}

void GuiEngineComponent::ShutdownComponent ()
{
	if (RegisteredGuiTheme != nullptr)
	{
		RegisteredGuiTheme->Destroy();
	}

	if (MainOverlay != nullptr)
	{
		MainOverlay->Destroy();
	}

	Super::ShutdownComponent();
}

std::vector<const DClass*> GuiEngineComponent::GetPreInitializeDependencies () const
{
	std::vector<const DClass*> dependencies = Super::GetPreInitializeDependencies();

	//Depends on GraphicsEngineComponent since the GuiEngineComponent registers the MainOverlay.
	dependencies.push_back(GraphicsEngineComponent::SStaticClass());

	return dependencies;
}

void GuiEngineComponent::SetDefaultThemeClass (const DClass* newDefaultThemeClass)
{
	if (!newDefaultThemeClass->IsChildOf(GuiTheme::SStaticClass()))
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot assign %s to the GuiEngine's DefaultThemeClass since it's not a Gui Theme class."), newDefaultThemeClass->ToString());
		return;
	}

	if (RegisteredGuiTheme != nullptr)
	{
		GuiLog.Log(LogCategory::LL_Warning, TXT("Cannot assign the default theme class to %s since the theme is already assigned.  You can only call this function prior to InitializeComponent."), newDefaultThemeClass->ToString());
		return;
	}

	DefaultThemeClass = newDefaultThemeClass;
}

void GuiEngineComponent::SetGuiTheme (GuiTheme* newGuiTheme)
{
	RegisteredGuiTheme = newGuiTheme;
}

GuiTheme* GuiEngineComponent::GetGuiTheme () const
{
	return RegisteredGuiTheme.Get();
}

void GuiEngineComponent::ImportGuiEssentialTextures ()
{
	TexturePool* localTexturePool = TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	//Textures for BorderedSpriteComponent
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("FrameBorderTop.jpg")), TXT("Interface.FrameBorderTop"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("FrameBorderRight.jpg")), TXT("Interface.FrameBorderRight"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("FrameBorderBottom.jpg")), TXT("Interface.FrameBorderBottom"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("FrameBorderLeft.jpg")), TXT("Interface.FrameBorderLeft"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("FrameBorderTopRight.jpg")), TXT("Interface.FrameBorderTopRight"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("FrameBorderBottomRight.jpg")), TXT("Interface.FrameBorderBottomRight"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("FrameBorderBottomLeft.jpg")), TXT("Interface.FrameBorderBottomLeft"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("FrameBorderTopLeft.jpg")), TXT("Interface.FrameBorderTopLeft"));

	//CheckboxComponent
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("Checkbox.png")), TXT("Interface.Checkbox"));

	//ButtonComponent
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Buttons"), TXT("TextureTestStates.jpg")), TXT("Interface.Buttons.TextureTestStates"));

	//FrameComponent
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY, TXT("TextureTest.jpg")), TXT("Interface.DefaultFrameComponentFill"));

	//ScrollbarComponent
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Buttons"), TXT("ScrollUpButton.jpg")), TXT("Interface.Buttons.ScrollUpButton"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Buttons"), TXT("ScrollDownButton.jpg")), TXT("Interface.Buttons.ScrollDownButton"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Buttons"), TXT("ScrollLeftButton.jpg")), TXT("Interface.Buttons.ScrollLeftButton"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Buttons"), TXT("ScrollRightButton.jpg")), TXT("Interface.Buttons.ScrollRightButton"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Buttons"), TXT("ScrollZoomButton.jpg")), TXT("Interface.Buttons.ScrollZoomButton"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarMiddleMouseSprite.png")), TXT("Interface.ScrollbarMiddleMouseSprite"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarMouseAnchorStationary.png")), TXT("Interface.ScrollbarMouseAnchorStationary"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarMouseAnchorPanUp.png")), TXT("Interface.ScrollbarMouseAnchorPanUp"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarMouseAnchorPanUpRight.png")), TXT("Interface.ScrollbarMouseAnchorPanUpRight"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarMouseAnchorPanRight.png")), TXT("Interface.ScrollbarMouseAnchorPanRight"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarMouseAnchorPanDownRight.png")), TXT("Interface.ScrollbarMouseAnchorPanDownRight"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarMouseAnchorPanDown.png")), TXT("Interface.ScrollbarMouseAnchorPanDown"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarMouseAnchorPanDownLeft.png")), TXT("Interface.ScrollbarMouseAnchorPanDownLeft"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarMouseAnchorPanLeft.png")), TXT("Interface.ScrollbarMouseAnchorPanLeft"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarMouseAnchorPanUpLeft.png")), TXT("Interface.ScrollbarMouseAnchorPanUpLeft"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarThumbFill.jpg")), TXT("Interface.ScrollbarThumbFill"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarThumbBorderBottom.jpg")), TXT("Interface.ScrollbarThumbBorderBottom"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarThumbBorderBottomLeft.jpg")), TXT("Interface.ScrollbarThumbBorderBottomLeft"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarThumbBorderBottomRight.jpg")), TXT("Interface.ScrollbarThumbBorderBottomRight"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarThumbBorderLeft.jpg")), TXT("Interface.ScrollbarThumbBorderLeft"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarThumbBorderRight.jpg")), TXT("Interface.ScrollbarThumbBorderRight"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarThumbBorderTop.jpg")), TXT("Interface.ScrollbarThumbBorderTop"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarThumbBorderTopLeft.jpg")), TXT("Interface.ScrollbarThumbBorderTopLeft"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface"), TXT("ScrollbarThumbBorderTopRight.jpg")), TXT("Interface.ScrollbarThumbBorderTopRight"));

	//DropdownComponent
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Buttons"), TXT("CollapsedButton.jpg")), TXT("Interface.Buttons.CollapsedButton"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Buttons"), TXT("ExpandedButton.jpg")), TXT("Interface.Buttons.ExpandedButton"));

	//Tree List
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Buttons"), TXT("TreeListAddButton.png")), TXT("Interface.Buttons.TreeListAddButton"));
	localTexturePool->CreateAndImportTexture(PrimitiveFileAttributes(Texture::TEXTURE_DIRECTORY / TXT("Interface") / TXT("Buttons"), TXT("TreeListSubtractButton.png")), TXT("Interface.Buttons.TreeListSubtractButton"));
}

void GuiEngineComponent::ImportGuiEssentialFonts ()
{
	//FontPool::FindFontPool()->CreateAndImportFont(TXT("QuattrocentoSans-Regular.ttf"), TXT("DefaultFont"));
}
SD_END