/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiDrawLayer.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, GuiDrawLayer, SD, PlanarDrawLayer)

const INT GuiDrawLayer::MAIN_OVERLAY_PRIORITY = 500;

void GuiDrawLayer::RenderDrawLayer (RenderTarget* renderTarget, Camera* cam)
{
	PlanarCamera* planarCam = dynamic_cast<PlanarCamera*>(cam);
	if (planarCam != nullptr)
	{
		//Notify the camera to calculate its absolute coordinates.
		planarCam->ComputeAbsTransform();
	}

	PrepareRegisteredMenus();

	for (UINT_TYPE i = 0; i < RegisteredMenus.size(); ++i)
	{
		RegisteredMenus.at(i)->ComputeAbsTransform();
		if (RegisteredMenus.at(i)->IsVisible() && RegisteredMenus.at(i)->IsWithinView(renderTarget, planarCam))
		{
			RenderGuiEntity(RegisteredMenus.at(i), renderTarget, planarCam);
		}
	}

	//Render loose components (such as the mouse) over the UI menus.
	RenderComponents(renderTarget, planarCam);
}

void GuiDrawLayer::HandleGarbageCollection ()
{
	Super::HandleGarbageCollection();

	PurgeExpiredMenus();
}

void GuiDrawLayer::PrepareRegisteredMenus ()
{
	PurgeExpiredMenus();

	std::sort(RegisteredMenus.begin(), RegisteredMenus.end(), [](GuiEntity* a, GuiEntity* b)
	{
		return (a->GetDepth() > b->GetDepth());
	});
}

void GuiDrawLayer::RegisterMenu (GuiEntity* newMenu)
{
#if ENABLE_COMPLEX_CHECKING
	//Make sure the menu is not already registered.
	CHECK(ContainerUtils::FindInVector(RegisteredMenus, newMenu) == INT_INDEX_NONE)
#endif

	RegisteredMenus.push_back(newMenu);
}

void GuiDrawLayer::UnregisterMenu (GuiEntity* target)
{
	//TODO:  Add Protections where we cannot unregister menu if it's in middle of render cycle

	UINT_TYPE menuIdx = ContainerUtils::FindInVector(RegisteredMenus, target);
	if (menuIdx != UINT_INDEX_NONE)
	{
		RegisteredMenus.erase(RegisteredMenus.begin() + menuIdx);
	}
}

void GuiDrawLayer::PurgeExpiredMenus ()
{
	//Remove empty/destroyed menus from list
	UINT_TYPE i = 0;
	while (i < RegisteredMenus.size())
	{
		if (!VALID_OBJECT(RegisteredMenus.at(i)))
		{
			RegisteredMenus.erase(RegisteredMenus.begin() + i);
			continue;
		}

		++i;
	}
}

void GuiDrawLayer::RenderGuiEntity (const GuiEntity* menu, RenderTarget* renderTarget, const Camera* camera) const
{
	for (ComponentIterator iter(menu, true); iter.GetSelectedComponent() != nullptr; iter++)
	{
		iter.bRecursive = true;

		EntityComponent* curComponent = iter.GetSelectedComponent();
		PlanarTransform* selectedComp = dynamic_cast<PlanarTransform*>(curComponent);
		if (selectedComp != nullptr)
		{
			//Refresh the transform's absolute coordinates
			selectedComp->ComputeAbsTransform();
			selectedComp->MarkProjectionDataDirty();

			if (!selectedComp->IsWithinView(renderTarget, camera))
			{
				//Skip to next sibling component
				iter.bRecursive = false;
				continue;
			}
		}

		//Check if the component is visible (must be executed after ComputeAbsTransform just in case external classes depends on that transform being updated.
		if (!curComponent->IsVisible())
		{
			//Skip to next sibling component
			iter.bRecursive = false;
			continue;
		}

		//Even though RenderComponents are not planar transforms (since they are agnostic to Planar and Scene transforms),
		//Their owners are required to implement a transform coordinate system, and that transform was checked in an earlier iteration.
		RenderComponent* renderComp = dynamic_cast<RenderComponent*>(iter.GetSelectedComponent());
		if (renderComp != nullptr)
		{
			renderComp->Render(renderTarget, camera);
		}
	}
}
SD_END