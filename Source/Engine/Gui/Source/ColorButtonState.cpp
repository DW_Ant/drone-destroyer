/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ColorButtonState.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, ColorButtonState, SD, ButtonStateComponent)

void ColorButtonState::CopyPropertiesFrom (const CopiableObjectInterface* cpy)
{
	const ColorButtonState* castCpyObj = dynamic_cast<const ColorButtonState*>(cpy);
	if (castCpyObj != nullptr)
	{
		DefaultColor = castCpyObj->DefaultColor;
		DisabledColor = castCpyObj->DisabledColor;
		HoverColor = castCpyObj->HoverColor;
		DownColor = castCpyObj->DownColor;
		RefreshState();
	}
}

void ColorButtonState::SetDefaultAppearance ()
{
	Super::SetDefaultAppearance();

	SetButtonColor(DefaultColor);
}

void ColorButtonState::SetDisableAppearance ()
{
	Super::SetDisableAppearance();

	SetButtonColor(DisabledColor);
}

void ColorButtonState::SetHoverAppearance ()
{
	Super::SetHoverAppearance();

	SetButtonColor(HoverColor);
}

void ColorButtonState::SetDownAppearance ()
{
	Super::SetDownAppearance();

	SetButtonColor(DownColor);
}

void ColorButtonState::SetDefaultColor (Color newDefaultColor)
{
	DefaultColor = newDefaultColor;

	RefreshState();
}

void ColorButtonState::SetDisabledColor (Color newDisabledColor)
{
	DisabledColor = newDisabledColor;

	RefreshState();
}

void ColorButtonState::SetHoverColor (Color newHoverColor)
{
	HoverColor = newHoverColor;

	RefreshState();
}

void ColorButtonState::SetDownColor (Color newDownColor)
{
	DownColor = newDownColor;

	RefreshState();
}

void ColorButtonState::SetButtonColor (Color newColorFill)
{
	BorderedSpriteComponent* sprite = dynamic_cast<BorderedSpriteComponent*>(GetRenderComponent());
	if (sprite != nullptr)
	{
		sprite->FillColor = newColorFill;
	}
}

SD_END