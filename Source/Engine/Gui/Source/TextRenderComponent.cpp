/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextRenderComponent.cpp
=====================================================================
*/

#include "GuiClasses.h"

SD_BEGIN
IMPLEMENT_CLASS(SD, TextRenderComponent, SD, RenderComponent)

void TextRenderComponent::InitProps ()
{
	Super::InitProps();

	TextShader = nullptr;
	FontColor = sf::Color::White;
	RenderState = sf::RenderStates::Default;
	TextFont = nullptr;
	FontSize = 12;
}

void TextRenderComponent::CopyPropertiesFrom (const CopiableObjectInterface* objTemplate)
{
	Super::CopyPropertiesFrom(objTemplate);

	const TextRenderComponent* textTemplate = dynamic_cast<const TextRenderComponent*>(objTemplate);
	if (textTemplate != nullptr)
	{
		SetTextShader(textTemplate->GetTextShader());
		SetTextFont(textTemplate->GetTextFont());
		SetFontSize(textTemplate->GetFontSize());
	}
}

void TextRenderComponent::Render (RenderTarget* renderTarget, const Camera* camera)
{
	const Vector2 renderTargetSize = renderTarget->GetSize();

	const Transformation::SScreenProjectionData& projectionData = GetOwnerTransform()->GetProjectionData(renderTarget, camera);

	for (UINT_TYPE i = 0; i < Texts.size(); i++)
	{
		const sf::Vector2f drawPosition = (Texts.at(i).DrawOffset * projectionData.Scale) + projectionData.Position;
		if (Texts.at(i).SfmlInstance != nullptr && drawPosition.y + (FontSize.ToFLOAT() * projectionData.Scale.y) >= 0.f)
		{
			if (drawPosition.y >= renderTargetSize.Y)
			{
				break; //Beyond viewable region
			}

			Texts.at(i).SfmlInstance->setPosition(drawPosition);
			Texts.at(i).SfmlInstance->setScale(projectionData.Scale); //Note: We only use scale instead of size since the sf objects have their own base size through the font size.
			renderTarget->Draw(*(Texts.at(i).SfmlInstance), RenderState);
		}
	}
}

void TextRenderComponent::Destroyed ()
{	
	if (TextShader != nullptr)
	{
		TextShader->Destroy();
		TextShader = nullptr;
	}

	DeleteAllText();

	Super::Destroyed();
}

TextRenderComponent::STextData& TextRenderComponent::CreateTextInstance (const DString& textContent, const Font* font, uint32 fontSize)
{
	sf::Text* newLine = new sf::Text(textContent.ToSfmlString(), *(font->GetFontResource()), fontSize);
	newLine->setFillColor(FontColor);

	STextData newData;
	newData.SfmlInstance = newLine;
	newData.DrawOffset = sf::Vector2f(0.f, 0.f);
	Texts.push_back(newData);

	return Texts.at(Texts.size() - 1);
}

void TextRenderComponent::DeleteAllText ()
{
	for (UINT_TYPE i = 0; i < Texts.size(); i++)
	{
		if (Texts.at(i).SfmlInstance != nullptr)
		{
			delete Texts.at(i).SfmlInstance;
		}
	}

	ContainerUtils::Empty(Texts);
}

void TextRenderComponent::RegisterOnShaderChangedCallback (SDFunction<void> newOnShaderChangedCallback)
{
#ifdef DEBUG_MODE
	for (UINT_TYPE i = 0; i < OnShaderChangedCallbacks.size(); i++)
	{
		if (OnShaderChangedCallbacks.at(i) == newOnShaderChangedCallback)
		{
			GuiLog.Log(LogCategory::LL_Warning, TXT("%s is already registered to %s.  Having duplicate entries would cause unreliable callback unregistration.  This check is only checked in debug builds, and may produce different results in release builds."), newOnShaderChangedCallback, ToString());
			return;
		}
	}
#endif

	OnShaderChangedCallbacks.push_back(newOnShaderChangedCallback);
}

void TextRenderComponent::UnregisterOnShaderChangedCallback (SDFunction<void> oldOnShaderChangedCallback)
{
	for (UINT_TYPE i = 0; i < OnShaderChangedCallbacks.size(); i++)
	{
		if (OnShaderChangedCallbacks.at(i) == oldOnShaderChangedCallback)
		{
			OnShaderChangedCallbacks.erase(OnShaderChangedCallbacks.begin() + i);
			return;
		}
	}

	GuiLog.Log(LogCategory::LL_Warning, TXT("Unable to unregister %s from %s since %s was not found in the callbacks vector."), oldOnShaderChangedCallback, ToString(), oldOnShaderChangedCallback);
}

void TextRenderComponent::SetTextShader (Shader* newTextShader)
{
	if (TextShader != nullptr)
	{
		TextShader->Destroy();
	}

	TextShader = newTextShader;
	RefreshTextShader();

	for (UINT_TYPE i = 0; i < OnShaderChangedCallbacks.size(); i++)
	{
		OnShaderChangedCallbacks.at(i).Execute();
	}
}

void TextRenderComponent::SetFontColor (sf::Color newFontColor)
{
	if (FontColor == newFontColor)
	{
		return;
	}

	FontColor = newFontColor;
	for (UINT_TYPE i = 0; i < Texts.size(); ++i)
	{
		if (Texts.at(i).SfmlInstance != nullptr)
		{
			Texts.at(i).SfmlInstance->setFillColor(FontColor);
		}
	}
}

void TextRenderComponent::SetTextFont (const Font* newTextFont)
{
	TextFont = newTextFont;
	for (UINT_TYPE i = 0; i < Texts.size(); i++)
	{
		if (Texts.at(i).SfmlInstance != nullptr)
		{
			Texts.at(i).SfmlInstance->setFont(*TextFont->GetFontResource());
		}
	}

	RefreshTextShader();
}

void TextRenderComponent::SetFontSize (INT newFontSize)
{
	FontSize = newFontSize;
	for (UINT_TYPE i = 0; i < Texts.size(); i++)
	{
		if (Texts.at(i).SfmlInstance != nullptr)
		{
			Texts.at(i).SfmlInstance->setCharacterSize(FontSize.ToUnsignedInt32());
		}
	}

	RefreshTextShader();
}

Shader* TextRenderComponent::GetTextShader () const
{
	return TextShader.Get();
}

const Font* TextRenderComponent::GetTextFont () const
{
	return TextFont.Get();
}

INT TextRenderComponent::GetFontSize () const
{
	return FontSize;
}

void TextRenderComponent::RefreshTextShader ()
{
	if (TextShader != nullptr && TextFont != nullptr)
	{
		CHECK(TextShader->SFShader != nullptr)
		TextShader->SFShader->setUniform(SHADER_BASE_TEXTURE, TextFont->GetFontResource()->getTexture(FontSize.ToUnsignedInt32()));
		RenderState = TextShader->SFShader;
	}
	else
	{
		RenderState = sf::RenderStates::Default;
	}
}
SD_END