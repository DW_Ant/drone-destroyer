/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScrollbarTrackComponent.h
  A component that's responsible for managing the track between the
  top and bottom buttons for the scroll bar.
=====================================================================
*/

#pragma once

#include "FrameComponent.h"

SD_BEGIN
class ButtonComponent;
class ScrollbarComponent_Deprecated;

class GUI_API ScrollbarTrackComponent : public GuiComponent
{
	DECLARE_CLASS(ScrollbarTrackComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Reference to the scrollbar this track component is affecting. */
	DPointer<ScrollbarComponent_Deprecated> OwningScrollbar;

	/* Component responsible for rendering the full track the thumb will travel through. */
	DPointer<SolidColorRenderComponent> TrackColor;

	/* Component responsible for drawing a solid color to represent the thumb's travel path. */
	DPointer<GuiComponent> TravelTrackColorOwner;
	DPointer<SolidColorRenderComponent> TravelTrackColor;

	/* Reference to the button component that's the marker that slides through the track. */
	DPointer<ButtonComponent> Thumb;

	/* The user must press and hold on the track for this long before the thumb will continuously travel. */
	FLOAT TravelDelay;

	/* When traveling continuously, this is the time interval for the thumb to shift again. */
	FLOAT TravelInterval;

protected:
	/* True if the scroll bar may be interacted. */
	bool bEnabled;

	/* If true, then the thumb will scale vertically based on how many scroll position
	indices over the total number of scroll positions. */
	bool bScaleThumb;

	/* Set whenever the pointer clicked on the thumb.  This is the distance from the cursor
	and the thumb's top border.  If negative, then the thumb is not being dragged. */
	INT DraggingThumbPointerOffset;

	/* If greater than 0, then the user must have clicked above the thumb, and the thumb is now traveling
	up to the mouse pointer.  If less than 0, then the user must have clicked below the thumb, and the
	thumb is now traveling down to the mouse pointer.  If 0, the thumb is not traveling. */
	INT TravelDirection;

	/* Time remaining before the thumb shifts again.  Disabled if negative. */
	FLOAT TravelTimeRemaining;

	/* Function callback whenever the thumb changed position */
	std::function<void(INT newScrollIndex)> OnThumbChangedPosition;

	/* Mouse that hovered over this component. */
	DPointer<MousePointer> RelevantMouse;


	/*
	=====================
	  Inherited	
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void AttachTo (Entity* newOwner) override;

protected:
	virtual void InitializeComponents () override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const override;
	virtual void HandleSizeChange () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Notifies the track to update the scroll position and scale based on the owning scrollbar's current position.
	 */
	virtual void RefreshScrollbarTrack();

	virtual void SetThumbChangedPositionHandler (std::function<void(INT newScrollIndex)> newHandler);

	virtual void SetEnabled (bool bNewEnabled);
	virtual void SetScaleThumb (bool bNewScaleThumb);

	/**
	 * Returns true if the user moved the mouse pointer in the other direction from where the thumb is travelling
	 * without releasing the mouse button.  For example, this returns true if the user clicks on the track above the thumb,
	 * then as the thumb travels upward, the user moves the mouse pointer below the thumb.
	 */
	virtual bool ReversedDirections () const;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool GetEnabled () const;
	virtual bool GetScaleThumb () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Hook to initialize default properties for the thumb.
	 */
	virtual void InitializeThumb ();

	/**
	 * Moves the thumb either up or down based on where the user held the cursor on the track.
	 * The thumb will not pass the mouse cursor's position.
	 */
	virtual void TravelByThumbDirection ();

	/**
	 * Considers all factors that determine if the thumb should be visible or not.  The thumb will
	 * not be visible if this is disabled or if the numVisibleScrollPositions is greater than total scroll positions.
	 */
	virtual void ReevaluateThumbVisibility ();

	/**
	 * Recalculates the clamped bounds the Thumb may travel to.
	 */
	virtual void CalculateThumbPositionClamps ();

	/**
	 * Calculates the thumb's vertical position based on the owning scroll bar's scroll position
	 * relative to the total number of scroll positions.
	 */
	virtual void CalculateThumbPosition ();

	/**
	 * If bScaleThumb is true, then this calculates the thumb's vertical scale based on the owning
	 * scroll bar's number of visible scroll positions over the total number of scroll positions.
	 */
	virtual void CalculateThumbScale ();

	/**
	 * Returns the scroll index based on the thumb's relative position in the track.
	 */
	virtual INT CalculateScrollIndex ();

	/**
	 * Calculates the TravelTrack size and position based on the given mouse coordinates and TravelDirection.
	 * Returns false if the thumb is near the mouse position and stopped traveling.
	 */
	virtual bool CalculateTravelTrackAttributes (FLOAT mousePosX, FLOAT mousePosY);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleThumbPressed (ButtonComponent* uiComponent);
	virtual void HandleThumbReleased (ButtonComponent* uiComponent);
	virtual void HandleTick (FLOAT deltaSec);
};
SD_END