/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HoverComponent.h
  Component that'll continuously evaluate the owning Entity's hover state, and invokes
  a callback whenever its hover state changes.
=====================================================================
*/

#pragma once

#include "Gui.h"

#include "GuiComponent.h"

SD_BEGIN
class GUI_API HoverComponent : public GuiComponent
{
	DECLARE_CLASS(HoverComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback to invoke whenever the mouse is no longer hovering over its owner. */
	SDFunction<void, MousePointer*, Entity*> OnRollOut;

	/* Callback to invoke whenever the mouse is now hovering over its owner. */
	SDFunction<void, MousePointer*, Entity*> OnRollOver;

protected:
	/* Becomes true if the mouse is currently hovering over Owner. */
	bool bIsHoveringOwner;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void ProcessMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool GetIsHovering () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void EvaluateHoverState (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent);
};
SD_END