/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiClasses.h
  Contains all header includes for the Gui module.
  
  CMake generated this header file to automatically list all header files.  When editing the include list, please
  edit the GuiClasses.h.in file.  Otherwise, CMake may overwrite your changes when generating project files.
=====================================================================
*/

#pragma once

#define MODULE_GUI 1

#include "BorderedSpriteComponent.h"
#include "ButtonComponent.h"
#include "ButtonStateComponent.h"
#include "CheckboxComponent.h"
#include "ColorButtonState.h"
#include "DropdownComponent.h"
#include "FocusComponent.h"
#include "FocusInterface.h"
#include "FrameComponent.h"
#include "Gui.h"
#include "GuiComponent.h"
#include "GuiDataElement.h"
#include "GuiDrawLayer.h"
#include "GuiEngineComponent.h"
#include "GuiEntity.h"
#include "GuiTester.h"
#include "GuiTheme.h"
#include "GuiUnitTester.h"
#include "HoverComponent.h"
#include "LabelComponent.h"
#include "ListBoxComponent.h"
#include "ScrollableInterface.h"
#include "ScrollbarComponent.h"
#include "ScrollbarComponent_Deprecated.h"
#include "ScrollbarTrackComponent.h"
#include "SingleSpriteButtonState.h"
#include "TestMenuButtonList.h"
#include "TestMenuFocusInterface.h"
#include "TextFieldComponent.h"
#include "TextFieldRenderComponent.h"
#include "TextRenderComponent.h"
#include "TreeBranchIterator.h"
#include "TreeListComponent.h"

