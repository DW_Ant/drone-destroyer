/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SingleSpriteButtonState.h
  A Button State Component that uses segments of a single texture to
  display various states of the button.

  The button's sprite component should be divided into 1x4 sections.
  Top section is the button's default state.
  Second from top is the button's disabled state.
  Third from top is the button's hover state.
  Bottom section is the button's down state.
=====================================================================
*/

#pragma once

#include "ButtonStateComponent.h"

SD_BEGIN
class GUI_API SingleSpriteButtonState : public ButtonStateComponent
{
	DECLARE_CLASS(SingleSpriteButtonState)


	/*
	=====================
	  Inherited	
	=====================
	*/

public:
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* cpy) override;
	virtual void SetDefaultAppearance () override;
	virtual void SetDisableAppearance () override;
	virtual void SetHoverAppearance () override;
	virtual void SetDownAppearance () override;
};
SD_END