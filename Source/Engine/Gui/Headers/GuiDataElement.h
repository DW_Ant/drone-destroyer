/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiDataElement.h
  An object representing information of an object that can be displayed within Gui containers
  such as dropdowns and tables.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

SD_BEGIN
/**
 * Base class for GuiDataElement so the non templated variables may point to templated types.
 */
class BaseGuiDataElement
{
	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If not empty, this is the text to display on the UI component to represent this object. */
	DString LabelText;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual DString GetLabelText () const
	{
		return LabelText;
	}
};

template <class T>
class GuiDataElement : public BaseGuiDataElement
{


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The represented object that could be a DProperty, DClass, Object, etc... */
	T Data;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	GuiDataElement ()
	{
		LabelText = TXT("");
	}

	GuiDataElement (T inData, const DString& inLabelText)
	{
		Data = inData;
		LabelText = inLabelText;
	}

	GuiDataElement (const GuiDataElement& inCopyData)
	{
		Data = inCopyData.Data;
		LabelText = inCopyData.LabelText;
	}

	virtual ~GuiDataElement ()
	{
		
	}


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual DString GetLabelText () const override
	{
		return LabelText;
	}
};
SD_END