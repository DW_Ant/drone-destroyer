/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiTester.h
  Class that'll instantiate various UI Components so that the user
  may test their functionality without external interference.
=====================================================================
*/

#pragma once

#include "Gui.h"
#include "GuiEntity.h"

#ifdef DEBUG_MODE

SD_BEGIN
class GuiComponent;
class GuiEntity;
class TreeListComponent;

class GUI_API GuiTester : public GuiEntity
{
	DECLARE_CLASS(GuiTester)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	Rectangle<FLOAT> StageDimensions;

	UnitTester::EUnitTestFlags TestFlags;

protected:
	/* List of Gui Components that are outside of staging area (cleans up at destroy). */
	std::vector<DPointer<GuiComponent>> BaseComponents;

	/* List of Gui Components that are inside staging area (cleans up at stage change). */
	std::vector<DPointer<GuiComponent>> StageComponents;

	/* Component that makes up the stage's frame. */
	DPointer<FrameComponent> StageFrame;

	/* Buttons that make up the button bar.  This list may be used for testing lists (such as dropdown components). */
	std::vector<DPointer<ButtonComponent>> ButtonBar;

	/* Maximum number of buttons on the ButtonBar is displayed at any given time. */
	INT MaxButtonsPerPage;

	/* Which section of the ButtonBar is the user currently viewing. */
	INT ButtonBarPageIdx;

	DPointer<ButtonComponent> NextButtonBarPage;
	DPointer<ButtonComponent> PrevButtonBarPage;

	/* For the button test, these are the two buttons that'll be toggling each other. */
	DPointer<ButtonComponent> ToggleButtons[2];
	INT ButtonTestCounter;

	/* For the Checkbox test, this is to toggle visibility and enabled flags on TargetTestCheckbox. */
	DPointer<CheckboxComponent> EnableToggleCheckbox;
	DPointer<CheckboxComponent> VisibilityToggleCheckbox;
	DPointer<CheckboxComponent> TargetTestCheckbox;

	/* For the Scrollbar test, this is the Scrollbar that'll be viewing various GuiComponents based on the button the user pressed. */
	DPointer<ScrollbarComponent> ScrollbarTester;
	FLOAT ScrollbarTesterAnchorTop;
	FLOAT ScrollbarTesterAnchorRight;
	FLOAT ScrollbarTesterAnchorBottom;
	FLOAT ScrollbarTesterAnchorLeft;
	DPointer<GuiEntity> ScrollbarTesterViewedObject;
	DPointer<ButtonComponent> ScrollbarTesterLabelTest;
	DPointer<ButtonComponent> ScrollbarTesterButtonTest;
	DPointer<ButtonComponent> ScrollbarTesterSizeChangeTest;
	DPointer<ButtonComponent> ScrollbarTesterDifferentFrameSizeTest;
	DPointer<ButtonComponent> ScrollbarTesterOuterSizeChangeTest;
	DPointer<ButtonComponent> ScrollbarTesterNestedScrollbars;
	DPointer<ButtonComponent> ScrollbarTesterComplexUi;
	DPointer<ButtonComponent> ScrollbarTesterDecreaseSizeButton;
	DPointer<ButtonComponent> ScrollbarTesterIncreaseSizeButton;
	DPointer<FrameComponent> ScrollbarTesterSizeChangeFrame;
	std::vector<GuiComponent*> ScrollbarTestObjects; //List of GUI Components to remove when changing scrollbar tests.

	/* For the ListBox test, this is a list of strings the List Box components will be referencing. */
	std::vector<DString> ListBoxStrings;

	/* For the ListBox test, this List Box has a limit in how many items can be selected. */
	DPointer<ListBoxComponent> MaxNumSelectableListBox;

	/* For the dropdown test, this is a list of numbers the numeric dropdown menus will be referencing. */
	std::vector<INT> DropdownNumbers;

	/* For the dropdown test, this is the dropdown component that reports the button bar content. */
	DPointer<DropdownComponent> ButtonBarDropdown;

	/* List of GuiEntities instantiated for the GuiEntity test. */
	std::vector<DPointer<GuiEntity>> GuiEntities;

	/* Index of the GuiEntities list that is currently focused. */
	UINT_TYPE FocusedGuiEntityIdx;

	/* For the GuiEntity test, this is the dropdown that selects which GuiEntity should be focused. */
	DPointer<DropdownComponent> SetFocusDropdown;

	/* For the HoverComponent test, this is the label component that will be adjusting its text based on if the user is hovering over it or not. */
	DPointer<LabelComponent> HoverLabel;

	/* For the HoverComponent test, this is the button component that'll be adjusting its text based on if the user is hovering over it or not. */
	DPointer<ButtonComponent> HoverButton;

	/* For the TreeList test, this is the class browser tree list that'll notify the user which class they've selected. */
	DPointer<TreeListComponent> TreeListClassBrowser;

	/* Window handle that'll be rendering objects from this test. */
	DPointer<Window> TestWindowHandle;

	/* Draw Layer that'll render UI elements to the TestWindowHandle. */
	DPointer<GuiDrawLayer> TestDrawLayer;

	/* Input-related objects associated with the TestWindowHandle. */
	DPointer<InputBroadcaster> TestWindowInput;
	DPointer<MousePointer> TestWindowMouse;

private:
	/* Counter for the button panels (external from stage). */
	INT PanelButtonCounter;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void BeginUnitTest ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeStageFrame ();

	/**
	 * Creates and initializes all UI components that make up the components external to the testing stage.
	 */
	virtual void CreateBaseButtons ();

	virtual void ClearStageComponents ();

	/**
	 * Resets all Entities associated with the Scrollbar's test.
	 */
	virtual void ResetScrollbarTest ();

	/**
	 * Creates and initializes a button to the options panel.
	 */
	virtual ButtonComponent* AddPanelButton ();

	/**
	 * Sets the relevant buttons on the button bar to be visible based on the current page idx.
	 */
	virtual void UpdateButtonBarPageIdx ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (FLOAT deltaSec);
	virtual void HandleWindowEvent (const sf::Event& newWindowEvent);

	virtual void HandleFrameButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleLabelButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleAutoSizeLabelButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleButtonButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleCheckboxButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleTextFieldButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleListBoxButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleDropdownButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleGuiEntityButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleHoverButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleTreeListButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleCloseClicked (ButtonComponent* uiComponent);
	virtual void HandleNextPageClicked (ButtonComponent* uiComponent);
	virtual void HandlePrevPageClicked (ButtonComponent* uiComponent);

	virtual void HandleButtonTestPressed (ButtonComponent* uiComponent);
	virtual void HandleButtonTestReleased (ButtonComponent* uiComponent);
	virtual void HandleButtonTestToggleReleased (ButtonComponent* uiComponent);
	virtual void HandleButtonTestIncrementCounter (ButtonComponent* uiComponent);
	virtual void HandleButtonTestDecrementCounter (ButtonComponent* uiComponent);

	virtual void HandleCheckboxToggle (CheckboxComponent* uiComponent);
	virtual void HandleToggleEnableCheckboxToggle (CheckboxComponent* uiComponent);
	virtual void HandleToggleVisibilityCheckboxToggle (CheckboxComponent* uiComponent);

	virtual void HandleScrollbarTesterLabelTestClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterButtonTestClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterTempButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterSizeChangeClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterToggleHideWhenFull (CheckboxComponent* uiComponent);
	virtual void HandleScrollbarTesterDecreaseSizeClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterIncreaseSizeClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterSizeChangerTick (FLOAT deltaSec);
	virtual void HandleScrollbarTesterDifferentFrameSizeClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterToggleHalfHorizontalButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterToggleHalfVertialButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterOuterSizeChangeClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterNestedScrollbarsClicked (ButtonComponent* uiComponent);
	virtual void HandleScrollbarTesterToggleFoodItem (CheckboxComponent* uiComponent);
	virtual void HandleScrollbarTesterComplexUiClicked (ButtonComponent* uiComponent);

	virtual void HandleBasicListBoxOptionSelected (INT selectedIdx);
	virtual void HandleBasicListBoxOptionDeselected (INT deselectedIdx);
	virtual void HandleReadOnlyBoxOptionSelected (INT selectedIdx);
	virtual void HandleReadOnlyBoxOptionDeselected (INT deselectedIdx);
	virtual void HandleMaxNumSelectedBoxOptionSelected (INT selectedIdx);
	virtual void HandleMaxNumSelectedBoxOptionDeselected (INT deselectedIdx);
	virtual void HandleAutoDeselectBoxOptionSelected (INT selectedIdx);
	virtual void HandleAutoDeselectBoxOptionDeselected (INT deselectedIdx);

	virtual void HandleBasicDropdownOptionClicked (INT newOptionIdx);
	virtual void HandleFullBasicDropdownOptionClicked (INT newOptionIdx);
	virtual void HandleButtonBarDropdownOptionClicked (INT newOptionIdx);

	virtual void HandleFocusChange (INT newOptionIdx);

	virtual void HandleHoverFrameRolledOver (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleHoverFrameRolledOut (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleHoverLabelRolledOver (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleHoverLabelRolledOut (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleHoverButtonRolledOver (MousePointer* mouse, Entity* hoverOwner);
	virtual void HandleHoverButtonRolledOut (MousePointer* mouse, Entity* hoverOwner);

	virtual void HandleTreeListClassSelected (INT newOptionIdx);
};
SD_END

#endif //debug mode