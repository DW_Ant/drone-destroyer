/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiComponent.h
  Contains base functionality for all UI Components.

  GuiComponents assume that the PlanarTransform they derive from are only drawn to a single
  RenderTarget since it doesn't make sense to have a UI element drawn on two windows, and the
  user is able to interact with either one of them.

  With this assumption, the GuiComponent often access the transform's abs position and size.
=====================================================================
*/

#pragma once

#include "Gui.h"
#include "GuiTheme.h"

//TODO:  Implement anchor object to replace Snapping functions, and to remove HandleSizeChange functions

SD_BEGIN
class GUI_API GuiComponent : public EntityComponent, public CopiableObjectInterface, public PlanarTransform
{
	DECLARE_CLASS(GuiComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* If true, this component will automatically stylize any sub GuiComponents that matches this component's style when
	they attach themselves to this component. This only works when ApplyStyle function is called. */
	bool PropagateStyle;

private:
	/* Name of the style this component is using. This is updated when ApplyStyle is called. Becomes empty if a default style or no style is used. */
	DString StyleName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool AddComponent (EntityComponent* newComponent, bool bLogOnFail = true, bool bDeleteOnFail = true) override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

protected:
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;
	virtual void AttachTo (Entity* newOwner) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Replaces copiable properties with the given style's template that matches this class.
	 * This method should be called before editing any properties since this function will overwrite them.
	 * If the given styleName is empty, or if the style name is not found, it'll fallback to the theme's default style.
	 */
	virtual void ApplyStyle (const DString& styleName);

	/**
	 * Returns true if this GuiComponent is the GuiComponent closest to the owning entity within the scope of local component chain (see diagram below).
	 * Entity
	 *		OtherComponent
	 *			GuiComponent <---- Outermost
	 *			GuiComponent <---- Outermost
	 *				SubGuiComponent <---- Not outermost
	 *					SubGuiComponent <---- Innermost
	 *			GuiComponent
	 *				Component
	 *					SubGuiComponent <---- Innermost
	 *		GuiComponent <---- Outermost
	 */
	virtual bool IsOutermostGuiComponent () const;

	/**
	 * Returns true if this component does not have any Gui sub components.
	 * If bOnlyImmediateComponents is true, then it'll only consider Gui components that this component directly owns (ignores sub components of this component's sub components).
	 */
	virtual bool IsInnermostGuiComponent (bool bOnlyImmediateComponents = false) const;

	/**
	 * Relays input events to the execution sequence.  If the input event is not consumed, then it'll proceed to
	 * sub components.  Nonconsumable events are always passed to subcomponents before consumeable events are passed in.
	 * The first consumed input may prevent other components from receiving consumable events.
	 */
	virtual bool ProcessInput (const sf::Event& evnt);
	virtual bool ProcessText (const sf::Event& evnt);
	virtual void ProcessMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
	virtual bool ProcessMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual bool ProcessMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Instantiates various internal components that makes up this component's functionality.
	 * This function assumes there is no theme/style available, and should initialize default
	 * component behavior should no UI template be available.
	 */
	virtual void InitializeComponents ();

	/**
	 * Actual implementation of ProcessMouse events.  This is a similar function signature to the InputComponent's callback signatures.
	 * Only added param is the consumedEvent flag which determines the default flag if the consumable input events should be invoked or not.
	 * The use of that flag is needed for input injection (such as the ScrollbarComponent relaying input responses from its viewed object).
	 */
	virtual bool ProcessInput_Implementation (const sf::Event& evnt, bool consumedEvnt);
	virtual bool ProcessText_Implementation (const sf::Event& evnt, bool consumedEvnt);
	virtual bool ProcessMouseClick_Implementation (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType, bool consumedEvent);
	virtual bool ProcessMouseWheelMove_Implementation (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent, bool consumedEvent);

	/**
	 * Event notification that there was an input event.
	 * This event cannot be consumed, and this is invoked prior to consumable input events.
	 */
	virtual void ExecuteInput (const sf::Event& evnt);
	virtual void ExecuteText (const sf::Event& evnt);
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual void ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

	/**
	 * Event notification to respond to input events.  Innermost input components will receive input priority over outermost components.
	 * Should this function return true, then the input event will not continue to other input components.
	 */
	virtual bool ExecuteConsumableInput (const sf::Event& evnt);
	virtual bool ExecuteConsumableText (const sf::Event& evnt);
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual bool ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

	/**
	 * Returns true if this Text/Input event should be passed to this component and its subcomponents.
	 * For MouseEvents, see AcceptsMouseEvents.
	 */
	virtual bool AcceptsInputEvent (const sf::Event& evnt) const;

	/**
	 * Returns true if the mouse events should be passed to this component and its subcomponents.
	 */
	virtual bool AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	/**
	 * Invoked whenever its Transformation changed in size.
	 */
	virtual void HandleSizeChange ();

	/**
	 * Invoked whenever its Transformation changed in position.
	 * It's not necessary to update sub component's position based on this component's position if the sub components' transforms are set relative to this component's transform.
	 */
	virtual void HandlePositionChange (const Vector2& deltaMove);
};
SD_END