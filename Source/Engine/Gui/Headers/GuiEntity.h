/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiEntity.h
  Entity is a simple container that may possess a collection of GuiComponents.
  These Entities are allowed to be registered to the GuiDrawLayer for consistent rendering patterns.

  If focused, this Entity also relays focused input to sub components.
=====================================================================
*/

#pragma once

#include "Gui.h"
#include "ScrollableInterface.h"

SD_BEGIN
class GuiComponent;
class FocusComponent;
class FocusInterface;

class GUI_API GuiEntity : public Entity, public PlanarTransform, public ScrollableInterface
{
	DECLARE_CLASS(GuiEntity)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Default frequency when a GuiEntity reevaluates its size when AutoSize is true (in seconds).
	This can be overriden by accessing this Entity's Tick component. */
	static const FLOAT DEFAULT_AUTO_SIZE_CHECK_FREQUENCY;

	/* Component that'll pass input events to focused entity.  This also handles tab ordering. */
	DPointer<FocusComponent> Focus;

	/* Input that passes in events to the GuiComponents. */
	DPointer<InputComponent> Input;

	/* Becomes true if this Entity is automatically adjusting its size based on its component size.
	This is only available when all immediate components are using absolute size rather than relative size.
	When this is true, this Entity will continuously iterate through all immediate Planar Components, and will
	automatically adjust its size based on the largest component.  Relative sized components are ignored. */
	bool AutoSizeHorizontal;
	bool AutoSizeVertical;

	/* Only applicable for GuiEntities rendered inside a scrollbar. This is the size this GuiEntity will take relative
	to the size of the scrollbar's frame size. 0.5 means half the size of the frame. 2 = twice the frame's size.
	If not a positive number, then these values are ignored.
	These are only applied whenever the Scrollbar's frame size changes. */
	Vector2 GuiSizeToOwningScrollbar;

	/* Tick Component that'll determine the frequency this Entity will recompute its size (only active when
	AutoSize is true. */
	DPointer<TickComponent> Tick;

	/* If not empty, the GuiEntity will apply the theme's style to any GuiComponents that attaches themselves to this Entity.
	Setting this variable will NOT automatically apply the style to existing components. */
	DString StyleName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool AddComponent (EntityComponent* newComponent, bool bLogOnFail = true, bool bDeleteOnFail = true) override;

	virtual void RegisterToDrawLayer (ScrollbarComponent* scrollbar, RenderTexture* drawLayerOwner) override;
	virtual void UnregisterFromDrawLayer (RenderTexture* drawLayerOwner) override;
	virtual void GetTotalSize (Vector2& outSize) const override;
	virtual void HandleScrollbarFrameSizeChange (const Vector2& oldFrameSize, const Vector2& newFrameSize) override;
	virtual bool ExecuteScrollableInterfaceInput (const sf::Event& evnt) override;
	virtual bool ExecuteScrollableInterfaceText (const sf::Event& evnt) override;
	virtual void ExecuteScrollableInterfaceMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual bool ExecuteScrollableInterfaceMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteScrollableInterfaceMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;
	virtual void RemoveScrollableObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets up this GuiEntity to be rendered and accept input from the main window.
	 * @param registerToMainRender If true, then this GuiEntity will register to the local GuiEngineComponent's MainOverlay draw layer.
	 * @param registerToMainInput If true, then this GuiEntity will subscribe to the input broadcaster affiliated with the main window.
	 * @param inputPriority If registering to the main window's input broadcaster, this input priority is used to determine the input
	 * order this entity will receive input events relative to other InputComponents.
	 */
	virtual void RegisterToMainWindow (bool registerToMainRender = true, bool registerToMainInput = true, INT inputPriority = 100);

	/**
	 * Removes focus from this GuiEntity and its components.
	 */
	virtual void LoseFocus ();

	/**
	 * Adds focus to this GuiEntity and its components.
	 * Gaining focus for one Entity does not lose focus for other Entities to allow multiple
	 * independent Entities to have focus if needed.
	 */
	virtual void GainFocus ();

	/**
	 * Creates or replaces the Input component that's subscribed to the specified InputBroadcaster.
	 * This InputComponent will relay input events to this Entity's GuiComponents.
	 */
	virtual void SetupInputComponent (InputBroadcaster* broadcaster, INT inputPriority);

	/**
	 * Iterates through all components to identify how large this Entity should be.
	 * Normally this is invoked automatically based on its Tick interval, but this can be invoked directly
	 * for force evaluation immediately.
	 * Only available when AutoSize is true.
	 */
	virtual void RefreshEntitySize ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetAutoSizeHorizontal (bool newAutoSizeHorizontal);
	virtual void SetAutoSizeVertical (bool newAutoSizeVertical);
	virtual void SetGuiSizeToOwningScrollbar (const Vector2& newGuiSizeToOwningScrollbar);
	virtual void SetStyleName (const DString& newStyleName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	// Returns the component that selects which UI element is focused
	FocusComponent* GetFocus () const;

	// Returns the focused object
	FocusInterface* GetFocusedObj () const;

	DEFINE_ACCESSOR(bool, AutoSizeHorizontal);
	DEFINE_ACCESSOR(bool, AutoSizeVertical);
	DEFINE_ACCESSOR(Vector2, GuiSizeToOwningScrollbar);

	inline TickComponent* GetTick () const
	{
		return Tick.Get();
	}

	DEFINE_ACCESSOR(DString, StyleName)


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Construct GuiComponents that'll assemble this UI instance.
	 */
	virtual void ConstructUI ();

	/**
	 * Creates and initializes the focus component.  This is where the tab order of the focus component is specified.
	 */
	virtual void InitializeFocusComponent ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

	virtual bool HandleInput (const sf::Event& evnt);
	virtual bool HandleText (const sf::Event& evnt);
	virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
	virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual bool HandleMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);
	virtual void HandleTick (FLOAT deltaSec);
};
SD_END