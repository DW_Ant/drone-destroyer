/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ListBoxComponent.h
  Gui Component that lists items as text vertically.
  
  This Gui component has the ability to invoke callbacks whenever the user selects
  or deselects an item.  Additionally, this has parameters to support multi-selection.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"
#include "FocusInterface.h"
#include "GuiDataElement.h"

SD_BEGIN
class FrameComponent;
class ScrollbarComponent_Deprecated;
class LabelComponent;

class GUI_API ListBoxComponent : public GuiComponent, public FocusInterface
{
	DECLARE_CLASS(ListBoxComponent)


	/*
	=====================
	  Datatypes
	=====================
	*/

protected:
	struct SHighlightSelectionInfo
	{
	public:
		/* Dummy GuiComponent that defines the transform for the render component. */
		DPointer<GuiComponent> RenderOwner;

		/* The color component that's drawn over the text.  This component may render 1 or more lines based
		on how many consecutive selected items there are. */
		DPointer<SolidColorRenderComponent> RenderComp;

		/* How many lines the renderer is suppose to cover for the current scroll position. */
		INT NumLines;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback to invoke whenever the user selects an item.  This is invoked after the item is added to SelectedItemList. */
	SDFunction<void, INT /*selectedItemIdx*/> OnItemSelected;

	/* Callback to invoke whenever the user deselects an item.  This is invoked before the item is removed from the SelectedItemList. */
	SDFunction<void, INT /*deselectedItemIdx*/> OnItemDeselected;

	/* Callback to invoke whenever the list content changed in size. */
	SDFunction<void> OnListSizeChanged;

protected:
	/* List of available options.  Each option may have a pointer to an object but does not check if the object is valid. 
	   The list, itself, is a pointer to BaseGuiDataElement in order to support polymorphic types for casting. */
	std::vector<BaseGuiDataElement*> List;

	/* If greater than 0, then this is the maxium number of items the user can select simultaneously. */
	INT MaxNumSelected;

	/* If true, then all selected items will be automatically deselected when the user clicks on a new item. */
	bool bAutoDeselect;

	/* If true, the item list cannot be selected and is for reading only.  This only prevents user from selecting/deselection options via mouse clicks.
	   Selecting items are still allowed if the program automatically selects items based on external events. */
	bool bReadOnly;

	/* An ordered (in ascending order) list of item indices that are selected. */
	std::vector<INT> SelectedItems;

	/* Index that shifts along the List (moving the highlight bar) without actually selecting the item.  If negative, then nothing is getting browsed. */
	INT BrowseIndex;

	DPointer<FrameComponent> Background;
	DPointer<ScrollbarComponent_Deprecated> Scrollbar;

	/* Text that renders the list. */
	DPointer<LabelComponent> ItemListText;

	/* Color component that'll be rendering the user's mouse hover. */
	DPointer<SolidColorRenderComponent> HighlightHoverRender;
	DPointer<GuiComponent> HighlightOwner;

	/* Color components that'll be rendering a highlight over each selected item. */
	std::vector<SHighlightSelectionInfo> HighlightSelections;

	/* Color to use for the selected items' highlight bars. */
	Color SelectionColor;

	/* Color to use for the hover bar. */
	Color HoverColor;

private:
	/* Becomes true if the color transforms needs to be recomputed (to avoid redundant color transform calculations). */
	bool bNeedColorTransformCalc;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

	virtual bool CanBeFocused () const override;
	virtual void GainFocus () override;
	virtual void LoseFocus () override;
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;

protected:
	virtual void InitializeComponents () override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual void HandleSizeChange () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the list item (by index) is selected.
	 */
	virtual bool IsItemSelectedByIndex (INT index) const;

	/**
	 * Ensures that none of the items within the list are selected.
	 */
	virtual void ClearSelection ();

	/**
	 * Ensures that all of the items within the list are selected.
	 */
	virtual void SelectAll ();

	/**
	 * Selects the specified items by their index values.
	 */
	virtual void SelectItemsByIdx (const std::vector<INT>& itemIndices);
	virtual void SelectItemsByIdx (INT selectedItemIdx);

	/**
	 * Sets an item by matching index to be either selected or deselected state.
	 */
	virtual void SetItemSelectionByIdx (INT itemIdx, bool bSelected);

	virtual void SetBrowseIndex (INT newBrowseIndex);
	virtual void SetList (const std::vector<BaseGuiDataElement*>& newList);
	virtual void SetMaxNumSelected (INT newMaxNumSelected);
	virtual void SetAutoDeselect (bool bNewAutoDeselect);
	virtual void SetReadOnly (bool bNewReadOnly);
	virtual void SetBackground (FrameComponent* newBackground);
	virtual void SetScrollbar (ScrollbarComponent_Deprecated* newScrollbar);
	virtual void SetItemListText (LabelComponent* newItemListText);
	virtual void SetSelectionColor (Color newSelectionColor);
	virtual void SetHoverColor (Color newHoverColor);

	/**
	 * Deallocates the list of pointers, and clears text and selections.
	 */
	virtual void ClearList ();


	/*
	=====================
	  Accessors
	=====================
	*/

	DEFINE_ACCESSOR(INT, BrowseIndex)
	DEFINE_ACCESSOR(std::vector<BaseGuiDataElement*>, List)
	inline const std::vector<BaseGuiDataElement*>& ReadList () const
	{
		return List;
	}

	DEFINE_ACCESSOR(INT, MaxNumSelected)
	inline bool IsReadOnly () const
	{
		return bReadOnly;
	}

	inline bool GetAutoDeselect () const
	{
		return bAutoDeselect;
	}
	
	DEFINE_ACCESSOR(std::vector<INT>, SelectedItems)
	inline const std::vector<INT>& ReadSelectedItems () const
	{
		return SelectedItems;
	}

	inline FrameComponent* GetBackground () const
	{
		return Background.Get();
	}

	inline ScrollbarComponent_Deprecated* GetScrollbar () const
	{
		return Scrollbar.Get();
	}

	inline LabelComponent* GetItemListText () const
	{
		return ItemListText.Get();
	}

	inline SolidColorRenderComponent* GetHighlightHoverRender () const
	{
		return HighlightHoverRender.Get();
	}

	DEFINE_ACCESSOR(Color, SelectionColor)
	DEFINE_ACCESSOR(Color, HoverColor)


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeBackground ();
	virtual void InitializeScrollbar ();
	virtual void InitializeItemListText ();
	virtual void InitializeHighlightBar ();

	/**
	 * Recalculates each components of the dropdown such in a way it maintains its structure.
	 */
	virtual void RefreshComponentTransforms ();

	/**
	 * Refreshes the color bars' transforms based on the scroll position.
	 */
	virtual void CalculateColorTransforms ();

	/**
	 * Updates the hover highlight transform to be over the current browse position.
	 */
	virtual void RenderHighlightHoverOverBrowsedItem ();

	/**
	 * Calculates the text to display on the label component (based on the current scroll position).
	 */
	virtual void UpdateTextContent ();

	/**
	 * Returns the line index the given mouse coordinates is over.  The line index is relative to the
	 * scroll position where the absolute line index equals return value + current scroll position.
	 * Returns negative if the coordinates are either beyond the transform region, or not over any item.
	 */
	virtual INT GetLineIdxFromCoordinates (FLOAT xPos, FLOAT yPos) const;

	/**
	 * Returns the rectangular region that covers the specified list item index.
	 * @Param lineIdx:  The item index in absolute terms where 0 is the first item regardless of scroll position.
	 * Returns a rectangle region in absolute coordinates.  Becomes empty if specified index doesn't
	 * exist or is currently not visible (due to scroll position).
	 * The rectangle are in UI coordinate system where positive X is right and positive Y is down.
	 */
	virtual Rectangle<FLOAT> GetItemRegion (INT lineIdx) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleScrollPositionChanged (INT newScrollIndex);
	virtual void HandleScrollbarToggleVisibility ();


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Adds an item to the list.  The pointer to the object is not validated.  If an object is deleted,
	 * it's the calling object's responsibility to clear the list and repopulate it.
	 */
	template <class T>
	void AddOption (const GuiDataElement<T>& inNewDataElement)
	{
		List.push_back(new GuiDataElement<T>(inNewDataElement));

		if (OnListSizeChanged.IsBounded())
		{
			OnListSizeChanged.Execute();
		}

		UpdateTextContent();
	}

	template <class T>
	void SetList (const std::vector<GuiDataElement<T>>& newList)
	{
		ClearList();

		for (UINT_TYPE i = 0; i < newList.size(); i++)
		{
			List.push_back(new GuiDataElement<T>(newList.at(i)));
		}

		if (OnListSizeChanged.IsBounded())
		{
			OnListSizeChanged.Execute();
		}

		UpdateTextContent();
	}

	template <class T>
	void GetSelectedDataElements (std::vector<GuiDataElement<T>>& outSelectedItems) const
	{
		outSelectedItems.empty();

		for (UINT_TYPE i = 0; i < SelectedItems.size(); i++)
		{
			outSelectedItems.push_back(List.at(SelectedItems.ToUnsignedInt()));
		}
	}

	/**
	 * Returns if a matching list item is selected.
	 */
	template <class T>
	bool IsItemSelected (const T& targetItem) const
	{
		for (UINT_TYPE i = 0; i < SelectedItems.size(); i++)
		{
			if (List.at(SelectedItems.at(i)) == targetItem)
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Select all data items within the list that matches the specified data element.
	 * Any other element will be deselected.
	 */
	template <class T>
	void SetSelectedItems (T targetData)
	{
		std::vector<INT> newItemSelection;

		for (UINT_TYPE i = 0; i < List.size(); i++)
		{
			if (static_cast<GuiDataElement<T>*>(List.at(i))->Data == targetData)
			{
				newItemSelection.push_back(INT(i));
			}
		}

		SelectItemsByIdx(newItemSelection);
	}

	/**
	 * Deselects all items except for the first item found in list that matches the specified data element.
	 */
	template <class T>
	void SetSelectedItem (T target)
	{
		for (UINT_TYPE i = 0; i < List.size(); i++)
		{
			if (static_cast<GuiDataElement<T>*>(List.at(i))->Data == target)
			{
				SelectItemsByIdx(INT(i));
				break;
			}
		}
	}
};
SD_END