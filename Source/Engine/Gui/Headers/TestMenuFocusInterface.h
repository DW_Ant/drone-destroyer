/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TestMenuFocusInterface.h
  Object that'll be testing GuiEntities.  This entity will be testing various UI elements that
  implement the FocusInterface.
=====================================================================
*/

#pragma once

#include "GuiEntity.h"

#ifdef DEBUG_MODE

SD_BEGIN
class FrameComponent;
class ButtonComponent;
class TreeListComponent;
class TextFieldComponent;

class GUI_API TestMenuFocusInterface : public GuiEntity
{
	DECLARE_CLASS(TestMenuFocusInterface)


	/*
	=====================
	  Datatypes
	=====================
	*/

protected:
	struct SPageComponents
	{
	public:
		std::vector<GuiComponent*> Components;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	INT PageIdx;

	std::vector<SPageComponents> PageData;
	DPointer<FrameComponent> MenuFrame;
	DPointer<ButtonComponent> NextPage;
	DPointer<ButtonComponent> PrevPage;

	DPointer<ButtonComponent> FocusButton;
	DPointer<DropdownComponent> FocusDropdown;
	DPointer<TextFieldComponent> FocusTextField;
	DPointer<ListBoxComponent> FocusListBox;
	DPointer<TreeListComponent> FocusTreeList;

private:
	std::vector<DString> ListBoxEntries;
	std::vector<DString> DropdownEntries;

	std::vector<DString> TreeListEntries;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void LoseFocus () override;
	virtual void GainFocus () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Sets all focus component's visibility flags based on the active page.
	 */
	virtual void RefreshPageVisibility ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleNextPageReleased (ButtonComponent* uiComponent);
	virtual void HandlePrevPageReleased (ButtonComponent* uiComponent);
	virtual void HandleButtonReleased (ButtonComponent* uiComponent);
	virtual void HandleDropdownOptionSelected (INT newIdx);
	virtual void HandleListBoxItemSelected (INT selectedIdx);
	virtual void HandleListBoxItemDeselected (INT deselectedIdx);
	virtual void HandleTreeListOptionSelected (INT optionIdx);
};
SD_END

#endif