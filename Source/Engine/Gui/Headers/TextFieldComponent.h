/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFieldComponent.h
  A component where the user may manpulate the text of a LabelComponent.
  This implements text selection for copy/pasting, and implements inserting text at cursor.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"
#include "FocusInterface.h"

SD_BEGIN
class GUI_API TextFieldComponent : public LabelComponent, public FocusInterface
{
	DECLARE_CLASS(TextFieldComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines the character limit in how many characters could the user enter into the text.  This only limits user input. Ignored if negative. */
	INT MaxNumCharacters;

	/* Rate (in seconds) the scroll bar moves up/down when dragging over text beyond current scroll position. */
	FLOAT DragScrollRate;

	/* Event to invoke whenever text input is pressed while editing text.  If the handler returns true, then the input is consumed before the TextFieldComponent processes it. */
	SDFunction<bool, const sf::Event&> OnEdit;

	/* Event to invoke whenever the user entered text input inside this field. If it returns true, then the TextField will process the text. */
	SDFunction<bool, const DString& /*txt*/> OnAllowTextInput;

	/* Event to broadcast whenever the user pressed 'enter' to apply changes.
	If this is bound, then the user cannot add new lines to this text field since it acts like it's applying changes instead. */
	SDFunction<void, TextFieldComponent* /*uiComponent*/> OnReturn;

protected:
	/* If true, then the user may edit the text of this field. */
	bool bEditable;

	/* If true, then the user may drag over text for highlighting and copying. */
	bool bSelectable;

	/* Index position where the cursor resides.  This also determines the "pivot" point when dragging for highlighting.
	This ranges from 0 to ContentLength where position 0 is before the first character, and position ContentLength is after last character.
	Note:  there are two cursor positions between each line.  One is at the end, and the other is found at the beginning of the next line. */
	INT CursorPosition;

	/* Line at which the CursorPosition may be located.  This may include lines beyond scroll position.  Becomes negative if CursorPosition is not set. */
	INT ActiveLineIndex;

	/* Other end of the selected text.  Text between this value and the CursorPosition is highlighted.  Disabled if negative. */
	INT HighlightEndPosition;

	/* MousePointer that needs its mouse icon restored. */
	DPointer<MousePointer> IconOverrideMouse;

private:
	/* If true, then the next text event detected while this component is focused, is ignored. */
	bool bIgnoreNextFocusedTextEvent;

	/* True if the user is dragging the mouse over the text for highlighting. */
	bool bDraggingOverText;

	/* Last time the scroll bar moved because the user dragged beyond the visible scroll length. */
	FLOAT LastDragScrollTime;

	/* Becomes a valid texture if this text field is currently overriding the mouse pointer's icon.
	This is typically true when it's hovered. */
	DPointer<Texture> OverridingMouseIcon;

	/* Becomes true if the latest key was pressed down with the control key held (to make the release order of Ctrl+Key independent).
	For example, pressing Ctrl then C then releasing Ctrl before releasing C should still register the C key as a copy command. */
	bool bIsCtrlCmd;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool CanBeFocused () const override;
	virtual bool CanTabOut () const override;
	virtual void GainFocus () override;
	virtual void LoseFocus () override;
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;

	virtual void SetRenderComponent (TextRenderComponent* newRenderComponent) override;

protected:
	virtual void InitializeComponents () override;

	virtual bool ExecuteConsumableInput (const sf::Event& evnt) override;
	virtual bool ExecuteConsumableText (const sf::Event& evnt) override;
	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the current cursor position is visible.  Returns false if cursor position is not set.
	 */
	virtual bool IsCursorVisible () const;

	/**
	 * Inserts the new text at the current cursor position.
	 */
	virtual void InsertTextAtCursor (DString insertedText);

	virtual void SetEditable (bool bNewEditable);

	virtual void SetSelectable (bool bNewSelectable);

	//TODO: Fix bug where there are extra cursor positions around new line characters.
	/**
	 * Updates the cursor position and the scroll position if necessary.
	 * Automatically clamps the newPosition to the length of the text.
	 */
	virtual void SetCursorPosition (INT newPosition);
	virtual void SetHighlightEndPosition (INT newPosition);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool GetEditable () const;
	virtual bool GetSelectable () const;
	DEFINE_ACCESSOR(INT, CursorPosition)
	DEFINE_ACCESSOR(INT, HighlightEndPosition)

	/**
	 * This returns the cursor's text position.  Different from CursorPosition where there are extra
	 * cursor positions between each line.  This function returns the text index position the cursor may be found.
	 */
	virtual INT GetTextPosition () const;
	virtual INT GetTextPosition (INT cursorIdx, INT lineIdx) const;

	/**
	 * Returns the text section that's selected.
	 */
	virtual DString GetSelectedText () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the mouse event to see if it should start overriding the mouse icon.
	 */
	virtual void UpdateMousePointerIcon (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent);

	/**
	 * Finds the cursor index position based on the given absolute mouse coordinates.
	 * Returns negative values if the specified coordinates are out of bounds.
	 */
	virtual INT FindCursorPos (INT absX, INT absY);

	/**
	 * Returns the total number of cursor positions within the text body content.
	 * If bFullText is true, then it'll return the total number of cursor positions within the full text from heady, body, and tail.
	 */
	virtual INT CountNumCursorPositions (bool bFullText) const;

	/**
	 * Calculates and returns the x,y position in absolute space at the specified cursor index.
	 * Returns negative values if the cursor position is not visible due to scroll position.
	 */
	virtual Vector2 CalcCharPosition (INT cursorIdx) const;

	/**
	 * Retrieves the cursor index position relative to the line the given global cursor index may be found.
	 * @Param cursorIdx:  cursor index position in Content.
	 * @Param outLocalCursorIdx:  cursor index position relative to the current line.
	 * Returns the line index the cursor may be found.  The line index is an index of the full text array (includes head, body, and tail).
	 * Returns -1 if the given cursorIdx is not within full text range.
	 */
	virtual INT GetCursorLineData (INT cursorIdx, INT& outLocalCursorIdx) const;
	virtual INT GetCursorLineData (INT cursorIdx) const;

	/**
	 * Calculates the ActiveLineIndex based on the current CursorPosition.
	 */
	virtual void CalculateActiveLineIndex ();

	/**
	 * Deletes the selected text from the text field.
	 */
	virtual void DeleteSelectedText ();

	/**
	 * Updates the highlight cursor position before the cursor position is about to jump.
	 * In short, it simply sets the highlight cursor position to cursor position if there is no selection.
	 * Does nothing if highlight cursor position is already set.
	 * If bHighlight is false, then it deselects highlighted text.
	 */
	virtual void UpdateHighlightPositionPriorToMove (bool bShouldHighlight);

	/**
	 * Updates the Highlight shader parameters based on this component's highlighting state.
	 */
	virtual void UpdateHighlightShaderParameters ();

	/**
	 * Converts the given vector coordinates from Sand Dune's coordinate system to GLSL coordinates.
	 * In detail, GLSL's positive Y-axis moves from bottom to top, while Sand Dune's positive Y-axis moves from top to down.
	 * This inverts the Y-axis based on this component's absolute size.
	 */
	virtual void SDtoGLSLvector (Vector2& outGLSLvector) const;

	/**
	 * Notifies the text render component to briefly pause the cursor's blinking behavior where
	 * The cursor will solid.
	 */
	virtual void PauseBlinker ();

	/**
	 * Moves the cursor to the beginning of the previous word.
	 */
	virtual void JumpCursorToPrevWord ();

	/**
	 * Moves the cursor to the beginning of the next word.
	 */
	virtual void JumpCursorToNextWord ();

	/**
	 * Moves the cursor to the beginning of the line.
	 */
	virtual void JumpCursorToBeginningLine ();

	/**
	 * Moves the cursor to the end of the line.
	 */
	virtual void JumpCursorToEndLine ();

	/**
	 * Moves the cursor up/down the text field relative to the current cursor position.
	 * If deltaLines is negative, then it'll move up.  Otherwise, it'll move down the Text Field.
	 */
	virtual void JumpCursorToLine (INT deltaLines);

	virtual void JumpCursorToBeginning ();
	virtual void JumpCursorToEnd ();

	virtual bool ExecuteTextFieldInput (const sf::Event& keyEvent);
	virtual bool ExecuteTextFieldText (const sf::Event& keyEvent);


	/*
	=====================
	  Event Handlers
	=====================
	*/

	/**
	 * Callback to check if this TextField should continue overriding the mouse pointer.
	 * Returns nullptr if this entity should no longer override the pointer.
	 */
	virtual Texture* HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent);

	virtual void HandleShaderChanged ();
};
SD_END