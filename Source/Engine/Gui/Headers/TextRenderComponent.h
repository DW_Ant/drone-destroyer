/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextRenderComponent.h
  A component that communicates with SFML to render
  text with specified parameters.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GUI_API TextRenderComponent : public RenderComponent
{
	DECLARE_CLASS(TextRenderComponent)


	/*
	=====================
	  Data types
	=====================
	*/

public:
	struct STextData
	{
		sf::Text* SfmlInstance;
		sf::Vector2f DrawOffset;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Reference to all instantiated SFML Text objects.  Each instance represents a new line.
	   Editing this text's font size and font should be handled through this TextRenderComponent instead
	   so that its shader may adapt appropriately. */
	std::vector<STextData> Texts;

	/* Special affect that's applied over the text. */
	DPointer<Shader> TextShader;

	sf::Color FontColor;

	/* Render state that'll is used how the text is drawn to the render target. */
	sf::RenderStates RenderState;

	/* Font that's applied to sf Text instances. */
	DPointer<const Font> TextFont;

	/* Character size applied to the sf Text instances. */
	INT FontSize;

	/* List of callbacks to invoke whenever this component updates its shader. */
	std::vector<SDFunction<void>> OnShaderChangedCallbacks;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Constructs a new Text instance that is initialized with the given parameters, and registers the text instance to the DrawOffset list.
	 * Returns a reference to the newly created text data.
	 */
	virtual STextData& CreateTextInstance (const DString& textContent, const Font* font, uint32 fontSize);

	/**
	 * Deletes all text pointers and clears the Texts vector.
	 * This also clears their corresponding DrawOffsets.
	 */
	virtual void DeleteAllText ();

	virtual void RegisterOnShaderChangedCallback (SDFunction<void> newOnShaderChangedCallback);
	virtual void UnregisterOnShaderChangedCallback (SDFunction<void> oldOnShaderChangedCallback);

	virtual void SetTextShader (Shader* newTextShader);
	virtual void SetFontColor (sf::Color newFontColor);
	virtual void SetTextFont (const Font* newTextFont);
	virtual void SetFontSize (INT newFontSize);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline std::vector<STextData>& EditTexts ()
	{
		return Texts;
	}

	inline const std::vector<STextData>& ReadTexts () const
	{
		return Texts;
	}

	virtual Shader* GetTextShader () const;
	DEFINE_ACCESSOR(sf::Color, FontColor)
	virtual const Font* GetTextFont () const;
	virtual INT GetFontSize () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates any shader parameters for the current applied shader, and may update the
	 * texts' render states based on current shader, font, and character size.
	 */
	virtual void RefreshTextShader ();
};
SD_END