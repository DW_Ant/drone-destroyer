/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TestMenuButtonList.h
  Object that'll be testing GuiEntities.  This entity will be testing tab order, and will be testing
  unfocusable buttons.
=====================================================================
*/

#pragma once

#include "GuiEntity.h"

#ifdef DEBUG_MODE

SD_BEGIN
class FrameComponent;
class ButtonComponent;

class GUI_API TestMenuButtonList : public GuiEntity
{
	DECLARE_CLASS(TestMenuButtonList)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	DPointer<FrameComponent> MenuFrame;

	std::vector<DPointer<ButtonComponent>> ButtonList;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void LoseFocus () override;
	virtual void GainFocus () override;

protected:
	virtual void ConstructUI () override;
	virtual void InitializeFocusComponent () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual ButtonComponent* InitializeButton ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleButtonReleased (ButtonComponent* uiComponent);
};
SD_END

#endif