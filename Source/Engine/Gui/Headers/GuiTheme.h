/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiTheme.h
  A class that references resources that should persist throughout multiple Gui components
  even if the textures are used within one class (to have a consistent feel throughout the application).
  Subclassed themes may have additional texture types, and also additional shared
  aesthetics across UI Components (such as sounds).

  In summary:
  A single theme may hold multiple styles that vary from one another. Typically an app sticks with one theme.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GuiComponent;

class GUI_API GuiTheme : public Object
{
	DECLARE_CLASS(GuiTheme)


	/*
	=====================
	  Data types
	=====================
	*/

public:
	struct SStyleInfo
	{
		/* Name that uniquely identifies this style. */
		DString Name;

		/* List of UI templates other UI components of identical classes will use to copy appearance properties from. */
		std::vector<GuiComponent*> Templates;

		SStyleInfo ();
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const DString DEFAULT_STYLE_NAME;

	/* Various mouse pointer icon overrides. */
	DPointer<Texture> TextPointer;
	DPointer<Texture> ResizePointerVertical;
	DPointer<Texture> ResizePointerHorizontal;
	DPointer<Texture> ResizePointerDiagonalTopLeft;
	DPointer<Texture> ResizePointerDiagonalBottomLeft;
	DPointer<Texture> ScrollbarPointer; //Overrides mouse pointer when using middle mouse to scroll.  The sprite is assumed to have 3 subdivisions (scroll up, no scroll, and scroll down).

	/* Various frame textures. */
	DPointer<Texture> FrameFill;
	DPointer<Texture> FrameBorderTop;
	DPointer<Texture> FrameBorderRight;
	DPointer<Texture> FrameBorderBottom;
	DPointer<Texture> FrameBorderLeft;
	DPointer<Texture> FrameBorderTopRight;
	DPointer<Texture> FrameBorderBottomRight;
	DPointer<Texture> FrameBorderBottomLeft;
	DPointer<Texture> FrameBorderTopLeft;

	DPointer<Texture> CheckboxTexture;

	/* Various button textures. */
	DPointer<Texture> ButtonSingleSpriteBackground; //Sprite to use for single sprite button components (using subdivisions)
	DPointer<Texture> ButtonFill; //For stretchable buttons (framed buttons), the background is filled with this sprite.
	DPointer<Texture> ButtonBorderTop;
	DPointer<Texture> ButtonBorderRight;
	DPointer<Texture> ButtonBorderBottom;
	DPointer<Texture> ButtonBorderLeft;
	DPointer<Texture> ButtonBorderTopRight;
	DPointer<Texture> ButtonBorderBottomRight;
	DPointer<Texture> ButtonBorderBottomLeft;
	DPointer<Texture> ButtonBorderTopLeft;

	/* Various scrollbar textures. */
	DPointer<Texture> ScrollbarUpButton;
	DPointer<Texture> ScrollbarDownButton;
	DPointer<Texture> ScrollbarLeftButton;
	DPointer<Texture> ScrollbarRightButton;
	DPointer<Texture> ScrollbarZoomButton;
	DPointer<Texture> ScrollbarMiddleMouseScrollAnchor; //sprite to display where the middle mouse button was pressed
	DPointer<Texture> ScrollbarMouseAnchorStationary;
	DPointer<Texture> ScrollbarMouseAnchorPanUp;
	DPointer<Texture> ScrollbarMouseAnchorPanUpRight;
	DPointer<Texture> ScrollbarMouseAnchorPanRight;
	DPointer<Texture> ScrollbarMouseAnchorPanDownRight;
	DPointer<Texture> ScrollbarMouseAnchorPanDown;
	DPointer<Texture> ScrollbarMouseAnchorPanDownLeft;
	DPointer<Texture> ScrollbarMouseAnchorPanLeft;
	DPointer<Texture> ScrollbarMouseAnchorPanUpLeft;

	//TODO:  Delete ScrollbarTrack* properties
	DPointer<Texture> ScrollbarTrackFill;
	DPointer<Texture> ScrollbarTrackBorderTop;
	DPointer<Texture> ScrollbarTrackBorderRight;
	DPointer<Texture> ScrollbarTrackBorderBottom;
	DPointer<Texture> ScrollbarTrackBorderLeft;
	DPointer<Texture> ScrollbarTrackBorderTopRight;
	DPointer<Texture> ScrollbarTrackBorderBottomRight;
	DPointer<Texture> ScrollbarTrackBorderBottomLeft;
	DPointer<Texture> ScrollbarTrackBorderTopLeft;

	/* Various dropdown textures. */
	DPointer<Texture> DropdownCollapsedButton;
	DPointer<Texture> DropdownExpandedButton;

	/* Various tree list textures. */
	DPointer<Texture> TreeListAddButton;
	DPointer<Texture> TreeListSubtractButton;

	/* Path to the shader used upon text used for the TextFieldComponent.  This variable assumes that the shader defines the following variables:
	bool bIsHighlighting:  Toggles shader on and off
	vec2 FirstHighlightPos:  absolute position at which the top left corner of the starting highlight position.
	vec2 LastHighlightPos:  Absolute position at which the top right corner of the end highlight position.
	float LineHeight:  Notifies the shader the spacing between two lines (includes character height and blank space).
	int WindowHeight:  Size of the window handle the text is drawn to (in pixels).
	Each TextFieldComponent will have their own shader instances.
	*/
	std::pair<DString, sf::Shader::Type> HighlightingShader;

	DPointer<Font> GuiFont;

protected:
	/* Index to the style list that is the default style newly created UI Components will mirror their default appearances from.
	Becomes negative if there isn't a default style selected. */
	INT DefaultStyleIdx;

	/* List of styles UI components can retrieve their appearance info from. */
	std::vector<SStyleInfo> Styles;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and returns a new StyleInfo that contains a copy of the copyObj's Templates.
	 * This is essentially a deep copy where all Gui Template mirrored their properites from copyObj's templates.
	 */
	static SStyleInfo CreateStyleFrom (const SStyleInfo& copyObj);

	/**
	 * Imports various resources such as textures and fonts UI elements may access.
	 */
	virtual void InitializeTheme ();

	/**
	 * Creates all UI styles that UI components may access to mirror their appearance properties from.
	 */
	virtual void InitializeStyles ();

	/**
	 * Retrieves a reference to the UI template with matching class.
	 * @param styleName Name of the style collection to search for the template in.
	 * @param templateClass UIComponent's class that must match the template's class.  Parent and subclasses are excluded.
	 */
	virtual const GuiComponent* FindTemplate (const DString& styleName, const DClass* templateClass) const;
	static const GuiComponent* FindTemplate (const SStyleInfo* style, const DClass* templateClass);
	static GuiComponent* FindTemplate (SStyleInfo* style, const DClass* templateClass);

	/**
	 * Searches through the style list and returns the data struct with matching name (case sensitive).
	 * Complexity is linear.
	 */
	const SStyleInfo* FindStyle (const DString& styleName) const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetDefaultStyle (const DString& styleName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/**
	 * Retrieves the Gui Theme from the GuiEngineComponent associated with the calling/local thread.
	 */
	static GuiTheme* GetGuiTheme ();

	/**
	 * Return the style info that GuiComponents will reference when they're instantiated.
	 */
	virtual const SStyleInfo* GetDefaultStyle () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Instantiates various UI Components to be the templates for the specified style.
	 */
	virtual void SetupStyle (SStyleInfo& outNewStyle);
};
SD_END