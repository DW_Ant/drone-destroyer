/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiEngineComponent.h
  Loads default resources such as fonts and icons.  This also instantiates
  global Gui objects such as themes and a mouse pointer.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GuiTheme;
class GuiDrawLayer;

class GUI_API GuiEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(GuiEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* UI Theme to spawn when this component initializes. */
	const DClass* DefaultThemeClass;

	/* The active GuiTheme that determines the overall appearance for all UI. */
	DPointer<GuiTheme> RegisteredGuiTheme;

	/* UI layer that'll be rendering general menus over the main window. */
	DPointer<GuiDrawLayer> MainOverlay;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	GuiEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual std::vector<const DClass*> GetPreInitializeDependencies () const override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * Replaces the default Gui theme with the specified theme.  This function must be called prior to
	 * InitializeComponent.  The DClass must be a type GuiTheme.
	 */
	virtual void SetDefaultThemeClass (const DClass* newDefaultThemeClass);

	virtual void SetGuiTheme (GuiTheme* newGuiTheme);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual GuiTheme* GetGuiTheme () const;
	
	inline GuiDrawLayer* GetMainOverlay () const
	{
		return MainOverlay.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ImportGuiEssentialTextures ();
	virtual void ImportGuiEssentialFonts ();
};
SD_END