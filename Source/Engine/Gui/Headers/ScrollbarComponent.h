/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScrollbarComponent.h
  A component that can manage UI elements within a frame.  Useful when there are
  UI components that are far larger than the viewable space.

  The scrollbar frame is a Render Texture that renders more components.  The scrollbar,
  itself, relays input events to those components.  If those events are not consumed, then
  the scrollbar handles the event.
  
  The Scrollbar is also responsible for managing the camera perspective for the Render Texture.
  It contains controls and input handlers for adjusting camera position and size.

  The ScrollbarComponent can only communicate with objects that implement the ScrollableInterface.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

SD_BEGIN
class ButtonComponent;
class LabelComponent;
class ScrollableInterface;

class GUI_API ScrollbarComponent : public GuiComponent
{
	DECLARE_CLASS(ScrollbarComponent)


	/*
	=====================
	  Datatypes
	=====================
	*/

protected:
	/**
	 * Enumeration that defines which direction the thumb is traveling.
	 */
	enum ETravelTrackDirection
	{
		TTD_None,
		TTD_Up,
		TTD_Down,
		TTD_Left,
		TTD_Right
	};

	enum EControlState
	{
		CS_Uninitialized,
		CS_Disabled,
		CS_Enabled
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
#pragma region "configurable variables"
	/* When clicking on a track or pan button, this determines how much the camera moves initially
	before it continuously pans.
	This is for users who tap on controls rather than holding on to them. */
	FLOAT InitialPanDistance;

	/* The speed multiplier when control/alt/shift is held. */
	FLOAT ShiftSpeedMultiplier;
	FLOAT AltSpeedMultiplier;

	/* The speed the camera pans when the vertical/horizontal scroll buttons are held (in pixels per second). */
	FLOAT ScrollSpeed;

	/* The speed the camera pans when the vertical/horizontal tracks are held (in pixels per second). */
	FLOAT TravelTrackSpeed;

	/* The amount of velocity increase (in pixels per second) if more mouse wheel input was sent in the same direction as camera momentum. */
	FLOAT WheelSpeedAccumulationRate;

	/* Min/Max range in how many pixels per second the camera pans using the middle mouse. */
	Range<FLOAT> AnchorPanSpeed;

	/* The min/max distance the mouse pointer must be away from anchor position to achieve min/max panning speed.
	If the mouse distance is less than minimum, then the camera will stop panning.  Units are in pixels.
	The distances are calculated independently with X axis and Y axis. */
	Range<FLOAT> AnchorDistanceLimits;

	/* When anchoring, this is the time that must elapse to change a hold to drag to a toggle. */
	FLOAT MinToggleTime;

	/* When holding the panning buttons or the track, this is the amount of time needed to elapse before
	it continuously pans (in seconds). */
	FLOAT HoldToDragTime;

	/* Min/Max zoom values for the FrameCamera. */
	Range<FLOAT> ZoomRange;

	/* List of zoom values that the camera may snap to when dragging near their values. */
	std::vector<FLOAT> ZoomSnapValues;

	/* If the camera's zoom value is within this threshold to any snap value, then it'll snap to the closest
	snap value. */
	FLOAT ZoomSnapTolerance;

	/* Determines how quickly the camera can zoom towards DesiredZoom. The formula it uses is y=x^2 where x is the amount of work (mouse movement or wheel),
	and y is the zoom amount. This variable determines how quickly the zoom moves along the x-axis (in 1 x-unit per second). */
	FLOAT  ZoomSpeed;

	/* Determines the zoom amount per mouse wheel Formula NewDesiredZoom=sqrt(DesiredZoom)*ZoomSensitivity. */
	FLOAT ZoomSensitivity;
#pragma endregion

protected:
#pragma region "Objects"
	/* The scrollable object the frame is viewing. */
	ScrollableInterface* ViewedObject;

	/* The Render Texture that'll be rendering the external UI components.  The size of the texture
	is based on the total dimensions of the external UI components. */
	DPointer<RenderTexture> FrameTexture;

	/* The camera that'll be panning/zooming based on this component's controls. */
	DPointer<PlanarCamera> FrameCamera;

	/* The sprite that's rendering the frame. */
	DPointer<PlanarTransformComponent> FrameSpriteTransform;
	DPointer<SpriteComponent> FrameSprite;

	DPointer<ButtonComponent> ScrollUpButton;
	DPointer<ButtonComponent> ScrollDownButton;
	DPointer<ButtonComponent> ScrollLeftButton;
	DPointer<ButtonComponent> ScrollRightButton;

	/* The transforms for the track components. */
	PlanarTransformComponent* VerticalTrackTransform;
	PlanarTransformComponent* HorizontalTrackTransform;

	/* The track that the thumb traverses through. */
	DPointer<SolidColorRenderComponent> VerticalTrack;
	DPointer<SolidColorRenderComponent> HorizontalTrack;

	/* The highlight over one of the thumb tracks that represents where the thumb is traveling to. */
	PlanarTransformComponent* TravelTrackTransform;
	DPointer<SolidColorRenderComponent> TravelTrack;

	PlanarTransformComponent* VerticalThumbTransform;
	PlanarTransformComponent* HorizontalThumbTransform;

	/* The thumb that can be dragged around to pan.  The size of the thumb reflects the size of the external
	UI components within the frame relative to the size of the FrameSprite. */
	DPointer<SolidColorRenderComponent> VerticalThumb;
	DPointer<SolidColorRenderComponent> HorizontalThumb;

	PlanarTransformComponent* MiddleMouseAnchorTransform;

	/* The sprite that's displays the center of the middle mouse anchor when the user presses the middle mouse
	button over the frame. */
	DPointer<SpriteComponent> MiddleMouseAnchor;

	/* Button that toggles the zoom controls when pressed/released. */
	DPointer<ButtonComponent> ZoomButton;

	PlanarTransformComponent* ZoomFeedbackTransform;

	/* The label that describes the zoom level. */
	DPointer<LabelComponent> ZoomLabel;

	/* The mouse pointer that most recently interacted with this component. */
	DPointer<MousePointer> Mouse;

	DPointer<TickComponent> Tick;
#pragma endregion

	/* Position where the middle mouse anchor is placed (in absolute coordinates).
	Becomes negative if the scrollbar is not in anchored mode. */
	Vector2 AnchorPosition;

	/* The latest texture computed when overriding mouse icon. */
	DPointer<Texture> AnchoredMouseIcon;

	/* Determines how often the mouse icon is computed (in seconds). */
	FLOAT AnchorIconRefreshTimeInterval;

	/* Timestamp when the anchor icon was last computed. */
	FLOAT AnchorIconTime;

	/* Timestamp when the anchor was placed (used to differentiate hold to drag vs toggle drag). */
	FLOAT StartAnchorTime;

	/* If true, then the vertical/horizontal scrollbars are full when the render target is able to view the full
	width/height of the Sub UI.  Otherwise, they will become disabled when there's nothing to pan. */
	bool HideControlsWhenFull;

	/* Color to use on the track when scrolling controls are enabled. */
	Color EnabledTrackColor;

	/* Color to use on the thumb when scrolling controls are enabled. */
	Color EnabledThumbColor;

	/* Color to use on the thumb when scrolling controls are disabled.  Note:  There is no disabled track color since the
	thumb covers the entire track when it's disabled. */
	Color DisabledThumbColor;

	/* Color to use when the thumb is pressed (being dragged). */
	Color ThumbDragColor;

	/* If true, then zoom controls are enabled and the zoom components are visible. */ 
	bool ZoomEnabled;

	/* Control states used to detect state changes (if it needs to change control visibility/enabledness). */
	EControlState HorizontalControlState;
	EControlState VerticalControlState;

	/* Velocity the camera is moving (in pixels per second).  This is shared with anchor movement, held scroll buttons,
	track traversal.  This is the base speed.  The keyboard modifiers (ctrl, alt, shift) are not considered here. */
	Vector2 CameraPanVelocity;

	/* The position the camera is heading towards.  May adjust CameraPanVelocity to ensure the camera reaches here.
	If negative, then the camera is not heading towards this position.  This is in pixels in frame coordinate space. */
	Vector2 CameraDestination;

	/* The zoom 'destination' the camera zoom is moving towards. If negative, then the camera is not zooming anywhere. */
	FLOAT DesiredZoom;

	/* Direction where one of the thumbs are moving towards. */
	ETravelTrackDirection TravelTrackDirection;

	/* When panning by holding the track, this is the original velocity the camera is moving.  The camera velocity can be adjusted
	based on mouse cursor's relative position to the thumb, and the velocity may need to be restored based on user mouse placement. */
	Vector2 OriginalCamPanVelocity;

	/* When comparing last known camera position and frame size to the current, it'll compare against this value to see
	if the difference is greater than this threshold.  If so, then it'll recalculate thumb transforms. */
	FLOAT TransformDetectionThreshold;

private:
	/* Determines the thickness of the scrollbar. */
	FLOAT ScrollbarThickness;

	/* Becomes true if any of the scroll buttons are held. */
	bool IsHoldingScrollButton;

	/* When a thumb is pressed, this is the click offset the user clicked on the thumb.  If negative, then the user isn't holding on a thumb.
	For vertical thumb, this is the number of pixels away from its top.  For horizontal, this is number of pixels away from its left. */
	FLOAT VerticalThumbHoldOffset;
	FLOAT HorizontalThumbHoldOffset;

	/* Becomes true if this scrollbar is resetting the mouse position while zooming. */
	bool SettingMousePosition;

	/* Cached variables used to determine if thumb transforms needs to be updated. */
	Vector2 CamLatestPos;
	Vector2 SpriteLatestSize;
	Vector2 ObjLatestSize;
	FLOAT CamLatestZoom;

	/* The sprite size of the mouse cursor before the middle mouse anchor overwrote its size. */
	Vector2 OriginalMouseSize;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool ProcessInput (const sf::Event& evnt) override;
	virtual bool ProcessText (const sf::Event& evnt) override;
	virtual void ProcessMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual bool ProcessMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ProcessMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;

protected:
	virtual void Destroyed () override;

	virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual bool ExecuteConsumableMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Destroys the viewed object, and clears the reference to that object.
	 */
	void DestroyViewedObject ();

	/**
	 * Returns true if either the vertical or horizontal scrollbars can scroll.
	 * Returns false if both axis cannot scroll due to ViewedObject size relative to FrameSprite size.
	 */
	virtual bool IsScrollingEnabled () const;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	/**
	 * Replaces the viewed object with the specified one.  This does not delete the old view object.
	 * Use DestroyViewedObject to destroy the object.
	 */
	void SetViewedObject (ScrollableInterface* newViewedObject);
	void SetHideControlsWhenFull (bool newHideControlsWhenFull);

	virtual void SetEnabledTrackColor (Color newEnabledTrackColor);
	virtual void SetEnabledThumbColor (Color newEnabledThumbColor);
	virtual void SetDisabledThumbColor (Color newDisabledThumbColor);
	virtual void SetThumbDragColor (Color newThumbDragColor);
	virtual void SetZoomEnabled (bool newZoomEnabled);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(bool, HideControlsWhenFull)
	DEFINE_ACCESSOR(Color, EnabledTrackColor)
	DEFINE_ACCESSOR(Color, EnabledThumbColor)
	DEFINE_ACCESSOR(Color, DisabledThumbColor)
	DEFINE_ACCESSOR(Color, ThumbDragColor)

	inline bool IsZoomEnabled () const
	{
		return ZoomEnabled;
	}

	ScrollableInterface* GetViewedObject () const;
	RenderTexture* GetFrameTexture () const;
	PlanarCamera* GetFrameCamera () const;
	SpriteComponent* GetFrameSprite () const;
	PlanarTransformComponent* GetFrameSpriteTransform () const;
	ButtonComponent* GetScrollUpButton () const;
	ButtonComponent* GetScrollDownButton () const;
	ButtonComponent* GetScrollLeftButton () const;
	ButtonComponent* GetScrollRightButton () const;
	SolidColorRenderComponent* GetVerticalTrack () const;
	SolidColorRenderComponent* GetHorizontalTrack () const;
	SolidColorRenderComponent* GetTravelTrack () const;
	SolidColorRenderComponent* GetVerticalThumb () const;
	SolidColorRenderComponent* GetHorizontalThumb () const;
	SpriteComponent* GetMiddleMouseAnchor () const;
	ButtonComponent* GetZoomButton () const;
	LabelComponent* GetZoomLabel () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Computes the camera's Zoom based on the DesiredZoom and the ZoomSpeed.
	 */
	virtual void UpdateZoom (FLOAT deltaSec);

	/**
	 * Returns coordinates that's relative to the ViewedObject.
	 * @param absPosition - Position in absolute space (relative to top left corner of window).
	 * @return The coordinates in ViewedFrame space.
	 */
	virtual Vector2 CalcViewedFramePosition (const Vector2& absPosition) const;

	/**
	 * Returns the min and maximum scrollable region for the frame camera.
	 * This is in absolute units in the ViewedObject space.
	 * This function considers the sprite's viewable space, and the camera's view extents.
	 */
	virtual void GetCamPositionLimits (Vector2& outMinPos, Vector2& outMaxPos) const;

	/**
	 * Adjusts the given camera position parameter to ensure that the position is within the allowable scrollable region.
	 * This considers the sprite's viewable space.
	 * This function is independent from camera's current position.
	 */
	virtual void ClampCamPosition (Vector2& outClampedPosition) const;

	/**
	 * While the camera is panning (due to track is traveling), this evaluates the mouse relative
	 * position to a scroll thumb.  This adjusts the camera velocity if the mouse is in oposite direction
	 * of travel track direction.
	 */
	virtual void UpdateTravelTrackVelocity (const Vector2& mousePos);

	/**
	 * Computes TravelTrack's transform is updated based on mouse position and direction.
	 */
	virtual void CalculateTravelTrackTransform (const Vector2& mousePos);

	/**
	 * Evaluates the scrollable region and the size of the view frame, and recomputes the vertical/horizontal
	 * thumb sizes.  This also refreshes the thumbs' colors based on the scrollbar's "enabledness".
	 * Doesn't update anything if the camera and size remains the same since last calculation.
	 */
	virtual void CalculateThumbTransforms ();

	/**
	 * Sets the button visibility/enabledness based on if this scrollbar is scrollable.
	 */
	virtual void RefreshScrollButtonConditions (bool isVertScrollingEnabled, bool isHorScrollingEnabled);

	/**
	 * Checks the viewable region of the sprite that displays ViewedObject.
	 * Updates the sprite so that viewed object is not scaled.
	 */
	virtual void RefreshSpriteTextureRegion ();

	/**
	 * Computes the mouse position relative to the scrollbar's draw position and camera location.
	 * Most of the cases, this is simply the mouse position relative to top left corner of window.
	 * But this also handles edge cases where if this scrollbar is displayed in a RenderTexture, it'll
	 * be relative to the top left corner of where that render texture is displayed in the window.
	 * If a camera location is associated with that RenderTexture, it'll also translate the mouse
	 * based on the camera's relative location to the origin of the RenderTexture's displayed scene.
	 */
	virtual Vector2 CalculateRelativeMousePosition (const Vector2& absMousePos) const;

	/**
	 * Computes the anchored mouse icon based on the mouse position relative to anchor position.
	 */
	virtual Texture* CalculateAnchoredMouseIcon (const Vector2& mousePos) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleScrollUpButtonPressed (ButtonComponent* uiComponent);
	virtual void HandleScrollDownButtonPressed (ButtonComponent* uiComponent);
	virtual void HandleScrollLeftButtonPressed (ButtonComponent* uiComponent);
	virtual void HandleScrollRightButtonPressed (ButtonComponent* uiComponent);
	virtual void HandleZoomButtonPressed (ButtonComponent* uiComponent);
	virtual void HandleZoomButtonReleased (ButtonComponent* uiComponent);
	virtual void HandleZoomButtonRightClickReleased (ButtonComponent* uiComponent);
	virtual Texture* HandleMouseIconOverride (MousePointer* mouse, const sf::Event::MouseMoveEvent& mouseEvnt);
	virtual void HandleZoomChanged (FLOAT newZoom);
	virtual void HandleTick (FLOAT deltaSec);
};
SD_END