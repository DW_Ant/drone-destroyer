/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DropdownComponent.h
  A component that expands to a list of objects, and the user can specify which object to select.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

#include "GuiDataElement.h"
#include "FocusInterface.h"

SD_BEGIN
class FrameComponent;
class LabelComponent;
class ButtonComponent;
class ScrollbarComponent_Deprecated;
class ListBoxComponent;

class GUI_API DropdownComponent : public GuiComponent, public FocusInterface
{
	DECLARE_CLASS(DropdownComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback to invoke whenever an option was selected where the parameter is the index of the list item. */
	SDFunction<void, INT> OnOptionSelected;

protected:
	/* List of available options in expansion menu. */
	DPointer<ListBoxComponent> ExpandMenu;

	UINT_TYPE SelectedItemIdx;

	/* Text to display on selected item field when no item is selected. */
	DString NoSelectedItemText;

	/* Becomes true if the list is expanded and visible. */
	bool bExpanded;

	DPointer<ButtonComponent> ExpandButton;

	/* Texture to use for the expand button while the dropdown is collapsed. */
	DPointer<Texture> ButtonTextureCollapsed;

	/* Texture to use for the expand button while the dropdown is expanded. */
	DPointer<Texture> ButtonTextureExpanded;

	DPointer<FrameComponent> SelectedObjBackground;

	DPointer<LabelComponent> SelectedItemLabel;
	DPointer<LabelComponent> ListLabel;

private:
	/* Timestamp at which this dropdown menu expanded. */
	FLOAT PrevExpandTime;

	/* Threshold that differentiates a drag select and a toggle select.  If the user tapped the dropdown arrow, the menu remains expanded.
	If the user held the mouse button beyond this value in seconds, then the menu will hide as soon as the user releases the button. */
	FLOAT DragToggleThresholdTime;

	/* If true, then the next enter release key event is ignored. */
	bool bIgnoreEnterRelease;

	/* For focused dropdowns, this is the search string the user is currently entering. */
	DString SearchString;

	/* Timestamp when the latest character was added to SearchString. */
	FLOAT SearchStringTimestamp;

	/* Maximum time to elapse before the search string clears. */
	FLOAT SearchStringClearTime;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool CanBeFocused () const override;
	virtual void LoseFocus () override;
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;

protected:
	virtual void InitializeComponents () override;
	virtual void HandleSizeChange () override;
	virtual void ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void Expand ();
	virtual void Collapse ();
	bool IsExpanded () const;

	/**
	 * Retrieves the height of this focus component while it's collapsed (in absolute space).
	 */
	virtual FLOAT GetCollapsedHeight () const;

	/**
	 * Sets the selected item by index value.
	 */
	virtual void SetSelectedItem (UINT_TYPE itemIndex);
	virtual void SetNoSelectedItemText (const DString& newNoSelectedItemText);
	virtual void ClearList ();

	virtual void SetExpandMenu (ListBoxComponent* newExpandMenu);
	virtual void SetExpandButton (ButtonComponent* newExpandButton);
	virtual void SetButtonTextureCollapsed (Texture* newButtonTextureCollapsed);
	virtual void SetButtonTextureExpanded (Texture* newButtonTextureExpanded);
	virtual void SetSelectedObjBackground (FrameComponent* newSelectedObjBackground);
	virtual void SetSelectedItemLabel (LabelComponent* newSelectedItemLabel);
	virtual void SetListLabel (LabelComponent* newListLabel);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline ListBoxComponent* GetExpandMenu () const
	{
		return ExpandMenu.Get();
	}

	virtual DString GetNoSelectedItemText () const;

	ButtonComponent* GetExpandButton () const;
	Texture* GetButtonTextureCollapsed () const;
	Texture* GetButtonTextureExpanded () const;
	FrameComponent* GetSelectedObjBackground () const;
	LabelComponent* GetSelectedItemLabel () const;
	LabelComponent* GetListLabel () const;


	/*
	=====================
	  Implementation
	=====================
	*/
		
protected:
	virtual void InitializeExpandButton ();
	virtual void InitializeSelectedObjBackground ();
	virtual void InitializeSelectedItemLabel ();
	virtual void InitializeExpandMenu ();

	/**
	 * Recalculates each components of the dropdown such in a way it maintains its structure.
	 */
	virtual void RefreshComponentTransforms ();
	virtual void RefreshExpandMenuTransform ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleExpandButtonClicked (ButtonComponent* uiComponent);
	virtual void HandleExpandListChanged ();
	virtual void HandleExpandItemSelected (INT selectedItemIdx);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	template <class T>
	GuiDataElement<T>* GetSelectedGuiDataElement () const
	{
		if (ExpandMenu != nullptr)
		{
			const std::vector<INT>& selectedItems = ExpandMenu->ReadSelectedItems();
			if (selectedItems.size() > 0)
			{
				return dynamic_cast<GuiDataElement<T>*>(ExpandMenu->ReadList().at(selectedItems.at(0).ToUnsignedInt()));
			}
		}

		return nullptr;
	}

	template <class T>
	T GetSelectedItem () const
	{
		GuiDataElement<T>* dataElement = GetSelectedGuiDataElement<T>();
		if (dataElement != nullptr)
		{
			return dataElement->Data;
		}

		return nullptr;
	}

	/**
	 * Attempts to select the data item within the table that matches the specified data element.
	 */
	template <class T>
	void SetSelectedItem (T targetData)
	{
		if (ExpandMenu != nullptr)
		{
			ExpandMenu->SetSelectedItem(targetData);
		}
	}
};
SD_END