/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GuiDrawLayer.h

  A DrawLayer for UI overlays where the draw order of registered Entities is based on EntityComponents
  where subcomponents are rendered over their owners.

  This DrawLayer interprets transformation objects as data relative to the screenspace and to the immediate
  owning Entity.  The screen space is first defined before kicking off the iterator.  It'll then
  sort all registered GuiEntities based on their transform's Z-value where GuiEntities with higher Z-values
  are rendered over other GuiEntities with lesser Z-values.

  For each GuiEntity, the iterator will step through its components where sub components are rendered over
  their owners.  The component's transform data is interpretted as either absolute or scale multipliers.

  For Translation:
  Values ranging from 0-1 implies that the Component is positioned relative to their owner where 0 is
  left aligned, 0.5 is centered, and 1 is right aligned.  Right aligned implying that the left border of the
  component meets the right border of the Owner.
  
  Values ranging above 1 implies that the component is positioned at an offset relative to the owning component.
  The offset is determined based on the transform's translation.  For example if the translation is (32, 64),
  then the component's top left corner is rendered 32 pixels to the left and 64 pixels below the owning component's
  top left corner.

  For Scaling:
  Values ranging from 0-1 act like multipliers to the owning component's scale where 0 is no scaling, 0.5 is half
  scale, and 1.0 is full scale.  If the owning component is 512 pixels wide, and the component's scaling is 0.75,
  then the resulting size of the component is 384 pixels wide.

  Values ranging above 1 are interpretted as absolute size.  If a component's transform scale is (64, 32), then
  the component will be 64 pixels wide and 32 pixels tall regardless of the size of its owners.

  Culling:
  The GuiDrawLayer only culls components that are invisible.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GuiEntity;

class GUI_API GuiDrawLayer : public PlanarDrawLayer
{
	DECLARE_CLASS(GuiDrawLayer)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Priority value that determines the draw order the main UI overlay renders relative to other draw layers. */
	static const INT MAIN_OVERLAY_PRIORITY;

protected:
	/* List of GuiEntities to render through each Tick. */
	std::vector<GuiEntity*> RegisteredMenus;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void RenderDrawLayer (RenderTarget* renderTarget, Camera* cam) override;

protected:
	virtual void HandleGarbageCollection () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers the specified menu to the list of menus to render.
	 */
	virtual void RegisterMenu (GuiEntity* newMenu);

	/**
	 * Iterates through the registered menus and removes it from the list if found.
	 */
	virtual void UnregisterMenu (GuiEntity* target);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Sifts through the RegisteredMenus to identify the order the Entities should render.
	 * Sorts the vector of RegisteredMenu based on their transform Z-order.
	 * This function will also clear all empty and destroyed RegisteredMenus.
	 */
	virtual void PrepareRegisteredMenus ();

	/**
	 * Iterates through the list of registered menu, and removes them from the list if they are
	 * destroyed or empty.
	 */
	virtual void PurgeExpiredMenus ();

	/**
	 * Iterates through the specified GuiEntity to render all of its GuiComponents.
	 */
	virtual void RenderGuiEntity (const GuiEntity* menu, RenderTarget* renderTarget, const Camera* camera) const;
};
SD_END