/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Gui.h
  Contains important file includes and definitions for the Gui module.
=====================================================================
*/

#pragma once

#include "Engine\Core\Headers\CoreClasses.h"
#include "Engine\SfmlGraphics\Headers\SfmlGraphicsClasses.h"
#include "Engine\Graphics\Headers\GraphicsClasses.h"
#include "Engine\Input\Headers\InputClasses.h"

#if !(MODULE_CORE)
#error The Gui module requires the Core module.
#endif

#if !(MODULE_SFML_GRAPHICS)
#error The Gui module requires the SFML Graphics module.
#endif

#if !(MODULE_GRAPHICS)
#error The Gui module requires the Graphics module.
#endif

#if !(MODULE_INPUT)
#error The Gui module requires the Input module.
#endif

#ifdef PLATFORM_WINDOWS
	#ifdef GUI_EXPORT
		#define GUI_API __declspec(dllexport)
	#else
		#define GUI_API __declspec(dllimport)
	#endif
#else
	#define GUI_API
#endif

#define TICK_GROUP_GUI "Gui"
#define TICK_GROUP_PRIORITY_GUI 5000

SD_BEGIN
extern LogCategory GUI_API GuiLog;
SD_END