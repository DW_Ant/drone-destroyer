/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LabelComponent.h
  A component that renders text to the screen within a bounding box.
  This class handles word wrapping, alignment, and truncating the text.
=====================================================================
*/

#pragma once

#include "GuiComponent.h"

SD_BEGIN
class TextRenderComponent;

class GUI_API LabelComponent : public GuiComponent
{
	DECLARE_CLASS(LabelComponent)


	/*
	=====================
	  Data types
	=====================
	*/

public:
	enum eHorizontalAlignment
	{
		HA_Right,
		HA_Left,
		HA_Center
	};

	enum eVerticalAlignment
	{
		VA_Top,
		VA_Center,
		VA_Bottom
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Reference to the current font that's applied to all text instances for the render component.
	All new text instances will be created with using this font. */
	DPointer<const Font> CurrentFont;

	/* Current character size that's applied to all text instances for the render component.
	All new text instances will be created with using this character size. */
	INT CharacterSize;

	/* If true, then this component will automatically apply changes as soon as properties change. */
	bool bAutoRefresh;

	/* If true, then the LabelComponent will automatically move text to a new line
	whenever the text reached beyond the label component's boundaries.  This is not applicable if bSingleLine is true. */
	bool bWrapText;

	/* If true, then the text will be truncated if it exceeds the bounding box. */
	bool bClampText;

	/* If true, then this LabelComponent will automatically scale horizontally to fit the width of the text content.  This takes
	precedence over the WrapText property and words will not wrap to the next line. */
	bool AutoSizeHorizontal;

	/* If true then this LabelComponent will automatically scale vertically to fit the height (number of lines).  This takes
	precedence over the ClampText property and characters will not truncate. */
	bool AutoSizeVertical;

	/* Determines the distance between lines. */
	FLOAT LineSpacing;

	/* The amount of text displaced for vertical alignment purposes. This is in number of pixels. */
	FLOAT FirstLineOffset;

	/* Content that's set to the instanced SFML's Text class.  Each element within this vector represents a single line. */
	std::vector<DString> Content;

	/* Text that was truncated due to clamped text. This is used to prevent data loss in case the component size grows again. */
	DString ClampedText;

	/* Text vertical alignment to the UI component's transformation. */
	eVerticalAlignment VerticalAlignment;

	/* Text horizontal alignment to the UI component's transformation.  This is not applicable if bSingleLine is true. */
	eHorizontalAlignment HorizontalAlignment;

	/* Determines how many sf::Text instances may fit within boundaries considering font size and line spacing. */
	INT MaxNumLines;

	/* Reference to the render component that draws the text. */
	DPointer<TextRenderComponent> RenderComponent;

	/* Class used to instantiate a render component (must be set before BeginObject is called). */
	const DClass* RenderComponentClass;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;

protected:
	virtual void InitializeComponents () override;
	virtual void HandleAbsTransformChange () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Convenient function that creates a LabelComponent within a GuiEntity that's viewed
	 * within the given ScrollbarComponent.
	 */
	static void CreateLabelWithinScrollbar (ScrollbarComponent* viewer, GuiEntity*& outLabelOwner, LabelComponent*& outLabelComp);

	/**
	 * Updates all Text instances to use this new font, and all new instances will use this font, too.
	 */
	virtual void SetFont (const Font* newFont);

	/**
	 * Updates all Text instances to use this character size, and all new instances will use this character size, too.
	 */
	virtual void SetCharacterSize (INT newCharacterSize);
	
	/**
	 * Overrides the current Content with the given new string.
	 * This will also notify the LabelComponent to word wrap, clamp text, and align text.
	 */
	virtual void SetText (const DString& newContent);

	/**
	 * Calculates MaxNumLines, and reapplies word wrapping, alignment changes, and clamping.
	 * You should invoke this function if you made any changes to the RenderComponent's Text
	 * that may have changed any spacing/size (such as fonts and character size).
	 */
	virtual void RefreshText ();

	virtual void SetAutoRefresh (bool bNewAutoRefresh);

	virtual void SetWrapText (bool bNewWrapText);
	virtual void SetClampText (bool bNewClampText);
	virtual void SetAutoSizeHorizontal (bool newAutoSizeHorizontal);
	virtual void SetAutoSizeVertical (bool newAutoSizeVertical);
	virtual void SetLineSpacing (FLOAT newLineSpacing);

	virtual void SetVerticalAlignment (eVerticalAlignment newVerticalAlignment);
	virtual void SetHorizontalAlignment (eHorizontalAlignment newHorizontalAlignment);

	virtual void SetRenderComponent (TextRenderComponent* newRenderComponent);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual const Font* GetFont () const;
	virtual INT GetCharacterSize () const;
	virtual bool GetAutoRefresh () const;
	virtual bool GetWrapText () const;
	virtual bool GetClampText () const;
	virtual bool GetAutoSizeHorizontal () const;
	virtual bool GetAutoSizeVertical () const;
	virtual FLOAT GetLineSpacing () const;
	virtual FLOAT GetFirstLineOffset () const;
	virtual DString GetContent () const;
	virtual void GetContent (std::vector<DString>& outContent) const;
	virtual std::vector<DString>& EditContent ();
	virtual const std::vector<DString>& ReadContent () const;

	/**
	 * Returns character size + line spacing.
	 */
	virtual FLOAT GetLineHeight () const;

	/**
	 * Retrieves the line of matching index of ContentBody.
	 * Returns an empty string if the specified line index is beyond the content's number of lines.
	 */
	virtual DString GetLine (INT lineIdx) const;

	virtual eVerticalAlignment GetVerticalAlignment () const;
	virtual eHorizontalAlignment GetHorizontalAlignment () const;

	virtual INT GetMaxNumLines () const;

	virtual TextRenderComponent* GetRenderComponent () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeRenderComponent ();

	/**
	 * Moves all text in the content vector to the first element. All clamp text is also appended to the first element.
	 * Used to reset the content array to its original state before it was wrapped and clamped.
	 */
	virtual void ResetContent ();

	/**
	 * Iterates through all characters.  Every time a new line character is encountered, the text
	 * after the new line character is then transfered to the next Text instance within the render component.
	 * If there isn't enough space for a new line (vertical absolute size is not large enough), then
	 * this function will not add another line (and the remaining text will go beyond the last text's boundaries.
	 * Returns true if all text instances fit within the component's boundaries.
	 */
	virtual bool ParseNewLineCharacters ();

	/**
	 * Repositions any newline characters to ensure the text fits within the box.
	 */
	virtual void WrapText ();

	/**
	 * Removes text that extends beyond the bounding box.
	 * For performance purposes, this only considers the last line.
	 * Naturally, WrapText would have already taken care of characters beyond the width's boundaries.
	 * This function also updates the ContentTail and ContentBody based on the text that was truncated.
	 */
	virtual void ClampText ();

	/**
	 * Iterates through each line to identify the widest line.
	 * Then this function adjusts this component's size X attribute to fit the widest line.
	 */
	virtual void ApplyHorizontalAutoSize ();

	/**
	 * Resizes this component so that all lines fit in this label component.
	 */
	virtual void ApplyVerticalAutoSize ();

	/**
	 * Copies over the ContentBody vector to the RenderComponent's text vector.
	 */
	virtual void RenderContent ();

	/**
	 * Positions each text in the render component so that the text's vertical placement matches current alignment.
	 */
	virtual void AlignVertically ();

	/**
	 * Positions each text in the render component so that the text's horizontal placement matches current alignment.
	 */
	virtual void AlignHorizontally ();

	/**
	 * Iterates backwards through the line of text starting from startPosition (defaults to end of line)
	 * and returns the character index that first matches any of the given targetCharacters.
	 * Returns INT_INDEX_NONE, if none of the characters were found within that line.
	 */
	virtual INT FindPreviousCharacter (const std::vector<char>& targetCharacters, UINT_TYPE textLine, INT startPosition = INT_INDEX_NONE) const;

	/**
	 * Calculates how many sf::Text instances may fit within this label component, and updates the 
	 * MaxNumLines variable.  This function considers font size and line spacing.
	 */
	virtual void CalculateMaxNumLines ();
};
SD_END