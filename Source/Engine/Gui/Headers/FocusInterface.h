/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FocusInterface.h
  An interface that declares all essential functions needed to communicate with a FocusComponent.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GUI_API FocusInterface
{


	/*
	=====================
	  Properties
	=====================
	*/

private:
	bool FocusInterface_bHasFocus = false;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true, if the focus component is allowed to focus on this Entity.
	 */
	virtual bool CanBeFocused () const;

	/**
	 * Returns true if this focused object's current state allows the user to tab out.
	 * Can the user press tab to focus the next object?
	 */
	virtual bool CanTabOut () const;

	/**
	 * Called whenever a FocusComponent is now focusing on this Entity.
	 */
	virtual void GainFocus ();

	/**
	 * Called whenever a FocusComponent is no longer focusing on this Entity.
	 */
	virtual void LoseFocus ();

	/**
	 * Similar to InputComponent, this function consumes the input event if returns true.
	 */
	virtual bool CaptureFocusedInput (const sf::Event& keyEvent) = 0;
	virtual bool CaptureFocusedText (const sf::Event& keyEvent) = 0;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual bool HasFocus () const;
};
SD_END