/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ColorButtonState.h
  A component that adjusts the owning button's fill color based on the
  button's state.
=====================================================================
*/

#pragma once

#include "ButtonStateComponent.h"

SD_BEGIN

class GUI_API ColorButtonState : public ButtonStateComponent
{
	DECLARE_CLASS(ColorButtonState)


	/*
	=====================
	 Properties
	=====================
	*/

protected:
	/* Various colors to use on the button based on its state. */
	Color DefaultColor;
	Color DisabledColor;
	Color HoverColor;
	Color DownColor;


	/*
	=====================
	 Inherited	
	=====================
	*/

public:
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* cpy) override;
	virtual void SetDefaultAppearance () override;
	virtual void SetDisableAppearance () override;
	virtual void SetHoverAppearance () override;
	virtual void SetDownAppearance () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	void SetDefaultColor (Color newDefaultColor);
	void SetDisabledColor (Color newDisabledColor);
	void SetHoverColor (Color newHoverColor);
	void SetDownColor (Color newDownColor);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(Color, DefaultColor);
	DEFINE_ACCESSOR(Color, DisabledColor);
	DEFINE_ACCESSOR(Color, HoverColor);
	DEFINE_ACCESSOR(Color, DownColor);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Sets the owning button's color fill to the specified value.
	 */
	virtual void SetButtonColor (Color newColorFill);
};
SD_END