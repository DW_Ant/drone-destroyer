/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFieldRenderComponent.h
  A render component that renders the cursor blinker that represents the cursor position.
=====================================================================
*/

#pragma once

#include "TextRenderComponent.h"

SD_BEGIN
class TextFieldComponent;

class GUI_API TextFieldRenderComponent : public TextRenderComponent
{
	DECLARE_CLASS(TextFieldRenderComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Reference to TextField that's responsible for placing the cursor positions. */
	DPointer<TextFieldComponent> OwningTextField;

	/* Determines the time interval the cursor will toggle visibility. */
	FLOAT CursorBlinkInterval;

	/* Parameters that adjusts how the cursor appears. */
	sf::Color CursorColor;
	FLOAT CursorWidth;

	/* Multiplies the cursor's vertical length by this value. */
	FLOAT CursorHeightMultiplier;

	/* Determines how long the cursor should render solid before blinking again after the cursor position recently changed.
	Specifying negative values would have the cursor blink regardless if the cursor moved recently. */
	FLOAT MoveDetectionDuration;

	/* If true, then this component will recalculate the cursor draw coordinates on next Tick regardless if the cursor changed position or not.
	This flag will reset to false after each tick. */
	bool bForceRecalcCursorCoordinates;

protected:
	/* Time when the cursor can resume blinking behavior.  If negative, then this is disabled. */
	FLOAT SolidCursorRenderEndTime;

private:
	/* Engine this component is registered to. */
	Engine* LocalEngine;

	/* Used to detect cursor movement.  The cursor should always be visible if it recently moved. */
	FLOAT LastCursorMoveTime;

	/* Latest cursor position this component detected (used for detecting cursor movement). */
	INT LastCursorPosition;

	/* Cached results from FindCursorCoordinates that's associated with the latest cursor position. */
	sf::Vector2f CursorCoordinates;

	/* The last projected position the cursor position was computed. Forces an update if the position changed. */
	sf::Vector2f LastProjectedPosition;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Temporarily pauses the cursor's blinking behavior so that it renders for the given renderTime.
	 * @Param renderTime:  Number of seconds the cursor will render for before returning to blinking behavior.
	 */
	virtual void PauseBlinker (FLOAT renderTime);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Given the cursor position, this function will find the absolute coordinates where the cursor should be drawn.
	 * This will return negative values if the cursor position should not be rendered.
	 */
	virtual sf::Vector2f FindCursorCoordinates (const Transformation::SScreenProjectionData& projectionData, INT cursorIndex);
};
SD_END