/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScrollableInterface.h
  A class that implements this interface gain the capability to be viewed in a scrollbar.

  This interface declares the functions a ScrollbarComponent may invoke.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class ScrollbarComponent;

class GUI_API ScrollableInterface
{


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * This object is now rendered inside the given Scrollbar.  Nothing is really needed to be executed
	 * in this function unless there's a need for this object to hold a reference to the scrollbar
	 * (such as there's a need to move the camera to a spot after a particular event).
	 */
	virtual void InitializeScrollableInterface (ScrollbarComponent* scrollbarOwner) {}

	/**
	 * Instructs this object to register itself to a draw layer within the specified RenderTexture.
	 * If the texture does not have the correct DrawLayer, then it's expected that this object creates
	 * the appropriate DrawLayer.
	 */
	virtual void RegisterToDrawLayer (ScrollbarComponent* scrollbar, RenderTexture* drawLayerOwner) = 0;

	/**
	 * The ScrollbarComponent is about to view a different object.  This object should unregister
	 * itself from the draw layer.
	 */
	virtual void UnregisterFromDrawLayer (RenderTexture* drawLayerOwner) = 0;

	/**
	 * Used to determine the amount to scroll.  This is checked every frame.  Computed size values should be
	 * cached to a variable for performance reasons.
	 */
	virtual void GetTotalSize (Vector2& outSize) const = 0;

	/**
	 * Invoked whenever the scrollbar's frame size changed to the new dimensions (in absolute pixels).
	 */
	virtual void HandleScrollbarFrameSizeChange (const Vector2& oldFrameSize, const Vector2& newFrameSize) {}

	/**
	 * Various input methods passed in from the owning Scrollbar component.
	 * If the consumable input events return true, then the input event will not be relayed further down the input list.
	 */
	virtual bool ExecuteScrollableInterfaceInput (const sf::Event& evnt) { return false; }
	virtual bool ExecuteScrollableInterfaceText (const sf::Event& evnt) { return false; }
	virtual void ExecuteScrollableInterfaceMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) {}
	virtual bool ExecuteScrollableInterfaceMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) {return false;}
	virtual bool ExecuteScrollableInterfaceMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) {return false;}

	/**
	 * The scrollbar no longer needs this object, and the memory allocated for this object should be restored
	 * (Examples:  Invoke Object::Destroy() or call delete this)
	 * The scrollbar will no longer reference this object when this function finishes executing.
	 */
	virtual void RemoveScrollableObject () = 0;
};
SD_END