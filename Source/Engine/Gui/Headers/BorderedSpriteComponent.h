/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BorderedSpriteComponent.h
  A component that draws stretched borders around the sprite.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class GUI_API BorderedSpriteComponent : public SpriteComponent
{
	DECLARE_CLASS(BorderedSpriteComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Reference to the resources that draws the borders. */
	DPointer<Texture> TopBorder;
	DPointer<Texture> RightBorder;
	DPointer<Texture> BottomBorder;
	DPointer<Texture> LeftBorder;

	/* Corner images. */
	DPointer<Texture> TopRightCorner;
	DPointer<Texture> BottomRightCorner;
	DPointer<Texture> BottomLeftCorner;
	DPointer<Texture> TopLeftCorner;

	/* If Sprite is nullptr, then the interior is filled with this color. */
	Color FillColor;

	/* Determines how thick to draw the borders. */
	FLOAT BorderThickness;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void CopyPropertiesFrom (const CopiableObjectInterface* objTemplate) override;
	virtual void Render (RenderTarget* renderTarget, const Camera* camera) override;
};
SD_END