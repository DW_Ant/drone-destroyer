/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FocusComponent.h
  Component that passes redirected input events to a targeted Entity, and handles tabbing through Entities.
=====================================================================
*/

#pragma once

#include "Gui.h"

SD_BEGIN
class FocusInterface;

class GUI_API FocusComponent : public EntityComponent
{
	DECLARE_CLASS(FocusComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Entities to iterate whenever the tab key was pressed.  This vector is unchecked.  Should an object be destroyed, should also remove itself from this vector.*/
	std::vector<FocusInterface*> TabOrder;

protected:
	/* Index of TabOrder that determines the current selected entity.  Becomes INT_INDEX_NONE if nothing is selected. */
	INT SelectedEntityIdx;

	DPointer<InputComponent> Input;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
		Methods
	=====================
	*/

public:
	/**
	 * Finds the next entity to be selected based on the TabOrder.  Resets back to the beginning if it reached the end of list.
	 */
	virtual void SelectNextEntity ();

	/**
	 * Ensures this component is not selecting an Entity.
	 */
	virtual void ClearSelection ();


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual FocusInterface* GetSelectedEntity () const;
	virtual InputComponent* GetInput () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void InitializeInput ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleInput (const sf::Event& keyEvent);
	virtual bool HandleText (const sf::Event& keyEvent);
};
SD_END