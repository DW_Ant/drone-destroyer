/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LoggerEngineComponent.h
  This component is responsible for setting up the external window
  that'll capture and display log messages.  This component is also
  responsible for managing log files.
=====================================================================
*/

#pragma once

#include "Logger.h"

SD_BEGIN
class LOGGER_API LoggerEngineComponent : public EngineComponent
{
	DECLARE_ENGINE_COMPONENT(LoggerEngineComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const Directory LOG_DIRECTORY;

	/* Log names applied to file names without extension. */
	DString BaseLogName;

protected:
	/* Handle to the object that's responsible writing data to the logs. */
	DPointer<TextFileWriter> LogFile;

	/* List of log statements to be recorded (that were logged before the LogFile was initialized). */
	std::vector<DString> PendingLogMsgs;

	/* Determines number of days a log file may reside before it expires (in days).
	If negative, then log files does not expire. */
	INT LogLifeSpan;

	/* Timestamp to display on the log file names when saving a copy of the log file.
	Available macros are:  %name (BaseLogName), %year, %month, %day, %hour, %minute, %second */
	DString TimestampFormat;

	/* Header to display before every log message.  The following macros are substituted:
	%type (Log Type), %frame (Frame count), %time (elapsed time), %year, %month, %day, %hour, %minute, %second */
	DString LogHeaderFormat;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	LoggerEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitializeComponent () override;
	virtual void FormatLog (const LogCategory* category, LogCategory::ELogLevel logLevel, OUT DString& msg) override;
	virtual void ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg) override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Searches through all log files, and purges any old log files.
	 */
	virtual void PurgeOldLogFiles ();

	/**
	 * Setups the LogWriter to start writing entries to a particular log file.
	 */
	virtual void SetupLogWriter ();

	/**
	 * Formats the header message for the log message.
	 */
	virtual void FormatLogHeader (const DString& logHeader, LogCategory::ELogLevel logLevel, OUT DString& outFullMessage);

	/**
	 * Saves a copy of the log file with a timestamp in the file name.
	 */
	virtual void PreserveLogFile ();

	/**
	 * Generates a file name using the current timestamp.
	 */
	virtual void GenerateFileName (DString& outFileName);
};
SD_END