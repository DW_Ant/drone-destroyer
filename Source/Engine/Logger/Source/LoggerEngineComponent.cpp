/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LoggerEngineComponent.cpp
=====================================================================
*/

#include "LoggerClasses.h"

SD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(SD, LoggerEngineComponent)

const Directory LoggerEngineComponent::LOG_DIRECTORY(Directory::BASE_DIRECTORY / TXT("Logs"));

LoggerEngineComponent::LoggerEngineComponent () : Super()
{
	BaseLogName = PROJECT_NAME;
	LogFile = nullptr;
	LogLifeSpan = -1; //Default to negative in case the config file was not opened, and doesn't cause archived logs to be deleted.
	TimestampFormat = TXT("%name-Backup %month-%day-%year %hour.%minute.%second");
	LogHeaderFormat = TXT("[%type]:  ");
}

void LoggerEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	//Retrieve config files
	ConfigWriter* configWriter = ConfigWriter::CreateObject();
	if (configWriter != nullptr)
	{
		configWriter->OpenFile(Directory::CONFIG_DIRECTORY.ReadDirectoryPath() + DString(TXT("Logger.ini")), false);
		if (!configWriter->IsFileOpened())
		{
			LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to open configuration file for LoggerEngineComponent.  Using default values."));
		}
		else
		{
			LogLifeSpan = configWriter->GetProperty<INT>(TXT("LoggerEngineComponent"), TXT("LogLifeSpan"));
			TimestampFormat = configWriter->GetPropertyText(TXT("LoggerEngineComponent"), TXT("TimestampFormat"));
			LogHeaderFormat = configWriter->GetPropertyText(TXT("LoggerEngineComponent"), TXT("LogHeaderFormat"));
		}

		configWriter->Destroy();
	}

	PurgeOldLogFiles();
	SetupLogWriter();
	LoggerLog.Log(LogCategory::LL_Log, TXT("LoggerEngineComponent initialized.  SandDune version is %s."), Version::GetVersionNumber());
}

void LoggerEngineComponent::FormatLog (const LogCategory* category, LogCategory::ELogLevel logLevel, OUT DString& msg)
{
	FormatLogHeader(category->GetLogTitle(), logLevel, msg);
}

void LoggerEngineComponent::ProcessLog (const LogCategory* category, LogCategory::ELogLevel logLevel, const DString& formattedMsg)
{
	if ((category->GetUsageFlags() & LogCategory::FLAG_LOG_FILE) > 0)
	{
		//Record to file writer
		if (LogFile != nullptr)
		{
			LogFile->AddTextEntry(formattedMsg);
		}
		else
		{
			//Push log message to vector to be later recorded whenever LogFile was initialized.
			PendingLogMsgs.push_back(formattedMsg);
		}
	}
}

void LoggerEngineComponent::ShutdownComponent ()
{
	if (LogFile != nullptr)
	{
		//Bid the user good well before departing. . .
		LoggerLog.Log(LogCategory::LL_Log, TXT("Engine is properly shutting down.  Closing log file - Good Bye!"));

		//Write out the remaining buffer to the file
		LogFile->WriteToFile(true);
		LogFile->Destroy();
		LogFile = nullptr;
		PreserveLogFile();
	}

	Super::ShutdownComponent();
}

void LoggerEngineComponent::PurgeOldLogFiles ()
{
	if (LogLifeSpan <= 0)
	{
		return; //Files don't expire
	}

	DateTime today;
	today.SetToCurrentTime();
	INT todayDays = today.CalculateTotalDays().ToINT();

	std::vector<PrimitiveFileAttributes> pendingDeleteFiles;
	for (FileIterator fileIter(LOG_DIRECTORY, FileIterator::INCLUDE_FILES); !fileIter.FinishedIterating(); fileIter++)
	{
		FileAttributes selectedAttributes(fileIter.GetSelectedAttributes());

		//Only consider .log files
		if (!selectedAttributes.GetFileExists() || selectedAttributes.GetFileExtension().Compare(TXT("log"), DString::CC_IgnoreCase) != 0)
		{
			continue;
		}

		//Can't delete read only files
		if (selectedAttributes.GetReadOnly())
		{
			continue;
		}

		if (selectedAttributes.GetLastModifiedDate().CalculateTotalDays().ToINT() + LogLifeSpan < todayDays)
		{
			//Mark for deletion.  Don't delete actually here since iterator is still referencing file.
			pendingDeleteFiles.push_back(selectedAttributes);
		}
	}

	for (UINT_TYPE i = 0; i < pendingDeleteFiles.size(); i++)
	{
		LoggerLog.Log(LogCategory::LL_Log, TXT("Purging old log file:  %s"), pendingDeleteFiles.at(i).GetName(false, true));
		if (!OS_DeleteFile(pendingDeleteFiles.at(i)))
		{
			LoggerLog.Log(LogCategory::LL_Warning, TXT("Failed to purge old log file:  %s"), pendingDeleteFiles.at(i).GetName(false, true));
		}
	}
}

void LoggerEngineComponent::SetupLogWriter ()
{
	LogFile = TextFileWriter::CreateObject();

	if (LogFile == nullptr)
	{
		LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to instantiate a TextFileWriter object.  The LoggerEngineComponent will be unable to write messages to the system log."));
		return;
	}

	if (!LogFile->OpenFile(LOG_DIRECTORY.ReadDirectoryPath() + BaseLogName + TXT(".Log"), true))
	{
		LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to open:  %s%s.Log!"), Directory::BASE_DIRECTORY, BaseLogName);
		return;
	}

	for (UINT_TYPE i = 0; i < PendingLogMsgs.size(); i++)
	{
		LogFile->AddTextEntry(PendingLogMsgs.at(i));
	}
	PendingLogMsgs.clear();
}

void LoggerEngineComponent::FormatLogHeader (const DString& logHeader, LogCategory::ELogLevel logLevel, OUT DString& outFullMessage)
{
	DString header = LogHeaderFormat;

	Engine* localEngine = Engine::FindEngine();
	header.ReplaceInline(TXT("%type"), logHeader, DString::CC_CaseSensitive);
	header.ReplaceInline(TXT("%frame"), DString::MakeString(localEngine->GetFrameCounter()), DString::CC_CaseSensitive);
	header.ReplaceInline(TXT("%level"), LogCategory::LogLevelToString(logLevel), DString::CC_CaseSensitive);

	const clock_t& startTime = localEngine->GetStartTime();
	clock_t deltaTime = clock() - startTime;
	header.ReplaceInline(TXT("%time"), DString::MakeString(deltaTime * 0.001f), DString::CC_CaseSensitive);

	if (header.Find(TXT("%"), 0, DString::CC_CaseSensitive) >= 0)
	{
		DateTime now;
		now.SetToCurrentTime();
		header = now.FormatStringPadded(header);
	}

	outFullMessage = header + outFullMessage;
}

void LoggerEngineComponent::PreserveLogFile ()
{
	DString newFileName;
	GenerateFileName(newFileName);

	if (!OS_CopyFile(PrimitiveFileAttributes(LOG_DIRECTORY, BaseLogName + TXT(".Log")), LOG_DIRECTORY, newFileName, true))
	{
		LoggerLog.Log(LogCategory::LL_Warning, TXT("Unable to save a copy of %s.Log to %s.  The logs from this session will not be archived."), BaseLogName, newFileName);
	}
}

void LoggerEngineComponent::GenerateFileName (DString& outFileName)
{
	DateTime today;
	today.SetToCurrentTime();

	outFileName = TimestampFormat;
	outFileName = today.FormatStringPadded(outFileName);
	outFileName.ReplaceInline(TXT("%name"), BaseLogName, DString::CC_IgnoreCase);
}
SD_END