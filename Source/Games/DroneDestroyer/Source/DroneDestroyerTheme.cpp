/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DroneDestroyerTheme.cpp
=====================================================================
*/

#include "DroneDestroyerTheme.h"

DD_BEGIN

IMPLEMENT_CLASS(DD, DroneDestroyerTheme, SD, GuiTheme)

const SD::DString DroneDestroyerTheme::DRONE_DESTROYER_STYLE_NAME = TXT("DroneDestroyerTheme");
const SD::DString DroneDestroyerTheme::SCOREBOARD_STYLE_NAME = TXT("DroneDestroyerScoreboard");

void DroneDestroyerTheme::InitializeTheme ()
{
	Super::InitializeTheme();

	SD::FontPool* localFontPool = SD::FontPool::FindFontPool();
	CHECK(localFontPool != nullptr)

	GuiFont = localFontPool->FindFont(TXT("PixelDigivolve"), true);
}

void DroneDestroyerTheme::InitializeStyles ()
{
	Super::InitializeStyles();

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	const SStyleInfo* defaultStyle = FindStyle(DEFAULT_STYLE_NAME);
	if (defaultStyle != nullptr)
	{
		SStyleInfo defaultDroneTheme = CreateStyleFrom(*defaultStyle);
		defaultDroneTheme.Name = DRONE_DESTROYER_STYLE_NAME;
		Styles.push_back(defaultDroneTheme);

		//Initialize button style
		{
			SD::ButtonComponent* buttonTemplate = dynamic_cast<SD::ButtonComponent*>(FindTemplate(&defaultDroneTheme, SD::ButtonComponent::SStaticClass()));
			CHECK(buttonTemplate != nullptr)

			SD::BorderedSpriteComponent* buttonSprite = buttonTemplate->GetRenderComponent();
			if (buttonSprite != nullptr)
			{
				buttonSprite->ClearSpriteTexture();
			}

			SD::ColorButtonState* colorState = dynamic_cast<SD::ColorButtonState*>(buttonTemplate->ReplaceStateComponent(SD::ColorButtonState::SStaticClass()));
			CHECK(colorState != nullptr)
			colorState->SetDefaultColor(SD::Color(128, 128, 128, 200));
			colorState->SetHoverColor(SD::Color(145, 145, 145, 200));
			colorState->SetDownColor(SD::Color(96, 96, 96, 200));
			colorState->SetDisabledColor(SD::Color(32, 32, 32, 200));
		}

		//Initialize Checkbox
		{
			SD::CheckboxComponent* checkboxTemplate = dynamic_cast<SD::CheckboxComponent*>(FindTemplate(&defaultDroneTheme, SD::CheckboxComponent::SStaticClass()));
			CHECK(checkboxTemplate != nullptr)

			if (checkboxTemplate->CheckboxSprite != nullptr && checkboxTemplate->CheckboxSprite->RenderComponent != nullptr)
			{
				checkboxTemplate->CheckboxSprite->RenderComponent->SetSpriteTexture(localTexturePool->FindTexture(TXT("DroneDestroyer.Interface.Checkbox")));
			}
		}

		SStyleInfo scoreboardTheme = CreateStyleFrom(defaultDroneTheme);
		scoreboardTheme.Name = SCOREBOARD_STYLE_NAME;
		Styles.push_back(scoreboardTheme);

		//Initialize scoreboard frame style
		{
			SD::FrameComponent* frameTemplate = dynamic_cast<SD::FrameComponent*>(FindTemplate(&scoreboardTheme, SD::FrameComponent::SStaticClass()));
			CHECK(frameTemplate != nullptr)

			frameTemplate->SetBorderThickness(1.f);
			frameTemplate->SetLockedFrame(true);
			if (frameTemplate->RenderComponent != nullptr)
			{
				frameTemplate->RenderComponent->TopBorder = nullptr;
				frameTemplate->RenderComponent->RightBorder = nullptr;
				frameTemplate->RenderComponent->BottomBorder = nullptr;
				frameTemplate->RenderComponent->LeftBorder = nullptr;
				frameTemplate->RenderComponent->TopRightCorner = nullptr;
				frameTemplate->RenderComponent->BottomRightCorner = nullptr;
				frameTemplate->RenderComponent->BottomLeftCorner = nullptr;
				frameTemplate->RenderComponent->TopLeftCorner = nullptr;
				frameTemplate->RenderComponent->ClearSpriteTexture();
				frameTemplate->RenderComponent->FillColor = SD::Color(64, 64, 64, 128); //Mostly transparent dark grey
			}

			SD::LabelComponent* labelTemplate = dynamic_cast<SD::LabelComponent*>(FindTemplate(&scoreboardTheme, SD::LabelComponent::SStaticClass()));
			CHECK(labelTemplate != nullptr)

			labelTemplate->SetCharacterSize(24);
		}
	}
}

DD_END