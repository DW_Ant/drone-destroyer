/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MusicPlayer.cpp
=====================================================================
*/

#include "MusicPlayer.h"
#include "Sound.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, MusicPlayer, SD, Object)

void MusicPlayer::Destroyed ()
{
	if (Music.getStatus() != sf::SoundSource::Stopped)
	{
		Music.stop();
	}

	Super::Destroyed();
}

void MusicPlayer::PlayMusic (const SD::DString& musicName)
{
	if (Music.getStatus() != sf::SoundSource::Stopped)
	{
		Music.stop();
	}

	if (musicName.IsEmpty())
	{
		return;
	}

	if (!Music.openFromFile((Sound::SOUND_DIRECTORY.ToString() + musicName).ToCString()))
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot play %s. Could not open from file."), musicName);
		return;
	}

	Music.setLoop(true);
	Music.play();
}

void MusicPlayer::SetMusicVolume (SD::FLOAT newVolume)
{
	Music.setVolume(newVolume.Value * 100.f);
}
DD_END