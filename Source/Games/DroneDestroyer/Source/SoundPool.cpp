/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SoundPool.cpp
=====================================================================
*/

#include "SoundPool.h"
#include "Sound.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, SoundPool, SD, ResourcePool)
IMPLEMENT_RESOURCE_POOL(Sound)

void SoundPool::BeginObject ()
{
	Super::BeginObject();

	RegisterResourcePool();
}

void SoundPool::ReleaseResources ()
{
	for (UINT_TYPE i = 0; i < ImportedResources.size(); i++)
	{
		ImportedResources.at(i).Sound->Destroy();
	}

	SD::ContainerUtils::Empty(OUT ImportedResources);
}

void SoundPool::Destroyed ()
{
	RemoveResourcePool();

	Super::Destroyed();
}

Sound* SoundPool::CreateAndImportSound (const SD::DString& fileName, const SD::DString& soundName)
{
	if (!IsValidName(soundName))
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to create sound since the specified name (%s) is either invalid or taken."), soundName);
		return nullptr;
	}

	sf::SoundBuffer* newBuffer = new sf::SoundBuffer();
	{
		SD::SfmlOutputStream sfmlOutput;

		//Import sound
		if (!newBuffer->loadFromFile(fileName.ToCString()))
		{
			DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to create sound.  Could not load %s.  %s"), fileName, sfmlOutput.ReadOutput());
			DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Current working directory is: %s"), SD::Directory::GetWorkingDirectory().ToString());
			delete newBuffer;
			return nullptr;
		}
	}

	Sound* result = Sound::CreateObject();
	if (result == nullptr)
	{
		delete newBuffer;
		return nullptr;
	}

	result->SetResource(newBuffer);

	if (!ImportSound(result, soundName))
	{
		//Failed to import.  destroy sound
		result->Destroy();
		return nullptr;
	}

	return result;
}
DD_END