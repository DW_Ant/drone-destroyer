/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  UserSettings.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "UserSettings.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, UserSettings, SD, Object)

void UserSettings::InitProps ()
{
	Super::InitProps();

	DefaultDynamicCam = true;
	DynamicCam = DefaultDynamicCam;

	DefaultCursorName = TXT("DroneDestroyer.Interface.Crosshair_White");
	CursorName = DefaultCursorName;

	DefaultGameVolume = 15;
	GameVolume = DefaultGameVolume;	
	DefaultMusicVolume = 10;
	MusicVolume = DefaultMusicVolume;

	ConfigFile = (SD::Directory::CONFIG_DIRECTORY / TXT("Generated")).ReadDirectoryPath() + TXT("UserSettings.ini");
	ConfigSection = TXT("UserSettings");
}

UserSettings* UserSettings::GetUserSettings ()
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	return localDroneEngine->GetSettings();
}

void UserSettings::LoadUserSettings ()
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(ConfigFile, false))
	{
		config->Destroy();
		return;
	}

	SD::DString dynCamStr = config->GetPropertyText(ConfigSection, TXT("DynamicCamera"));
	if (!dynCamStr.IsEmpty())
	{
		DynamicCam = dynCamStr.ToBool();
	}

	CursorName = config->GetPropertyText(ConfigSection, TXT("CursorName"));
	if (CursorName.IsEmpty())
	{
		CursorName = DefaultCursorName;
	}

	SD::DString gameVol = config->GetPropertyText(ConfigSection, TXT("GameVolume"));
	if (!gameVol.IsEmpty())
	{
		GameVolume = SD::INT(std::atoi(gameVol.ToCString()));
	}

	SD::DString musicVol = config->GetPropertyText(ConfigSection, TXT("MusicVolume"));
	if (!musicVol.IsEmpty())
	{
		MusicVolume = SD::INT(std::atoi(musicVol.ToCString()));
	}

	config->Destroy();
}

void UserSettings::ResetToDefaults ()
{
	DynamicCam = DefaultDynamicCam;
	CursorName = DefaultCursorName;
	GameVolume = DefaultGameVolume;
	MusicVolume = DefaultMusicVolume;
}

void UserSettings::SaveUserSettings ()
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(ConfigFile, false))
	{
		config->Destroy();
		return;
	}

	config->SaveProperty<SD::BOOL>(ConfigSection, TXT("DynamicCamera"), DynamicCam);
	config->SavePropertyText(ConfigSection, TXT("CursorName"), CursorName);
	config->SaveProperty<SD::INT>(ConfigSection, TXT("GameVolume"), GameVolume);
	config->SaveProperty<SD::INT>(ConfigSection, TXT("MusicVolume"), MusicVolume);

	config->SaveConfig();
	config->Destroy();
}

void UserSettings::SetDynamicCam (bool newDynamicCam)
{
	DynamicCam = newDynamicCam;
}

void UserSettings::SetCursorName (const SD::DString& newCursorName)
{
	CursorName = newCursorName;
}

void UserSettings::SetGameVolume (SD::INT newGameVolume)
{
	GameVolume = newGameVolume;
}

void UserSettings::SetMusicVolume (SD::INT newMusicVolume)
{
	MusicVolume = newMusicVolume;
}
DD_END