/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BotSoulComponent.cpp
=====================================================================
*/

#include "BotSoulComponent.h"
#include "Character.h"
#include "PlayerSoul.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, BotSoulComponent, SD, EntityComponent)

void BotSoulComponent::InitProps ()
{
	Super::InitProps();

	//We randomize the process interval to scatter processing time with the other bots.
	ProcessInterval = 3.f + SD::RandomUtils::RandRange(-0.5f, 0.5f);
}

void BotSoulComponent::BeginObject ()
{
	Super::BeginObject();

	SD::TickComponent* tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(tick))
	{
		tick->SetTickInterval(ProcessInterval);
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, BotSoulComponent, HandleTick, void, SD::FLOAT));
	}
}

bool BotSoulComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	return (dynamic_cast<PlayerSoul*>(ownerCandidate) != nullptr);
}

void BotSoulComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwningSoul = dynamic_cast<PlayerSoul*>(newOwner);
}

void BotSoulComponent::ComponentDetached ()
{
	OwningSoul = nullptr;

	Super::ComponentDetached();
}

void BotSoulComponent::EvaluateSituation ()
{
	CHECK(OwningSoul != nullptr)

	if (!OwningSoul->IsAlive() && !OwningSoul->GetIsEliminated())
	{
		if (OwningSoul->RequestBody() && OwningSoul->GetBody() != nullptr)
		{
			OwningSoul->GetBody()->InitializeBotComponent();
		}
	}
}

void BotSoulComponent::HandleTick (SD::FLOAT deltaSec)
{
	EvaluateSituation();
}

DD_END