/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Scoreboard_Deathmatch.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "Deathmatch.h"
#include "Scoreboard_Deathmatch.h"
#include "ScoreComponent_Deathmatch.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Scoreboard_Deathmatch, DD, Scoreboard)

void Scoreboard_Deathmatch::InitializeGameLimitLabel ()
{
	Super::InitializeGameLimitLabel();

	if (GameLimitLabel != nullptr)
	{
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr)

		if (Deathmatch* deathmatch = dynamic_cast<Deathmatch*>(localDroneEngine->GetGame()))
		{
			SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
			CHECK(translator != nullptr)
			SD::DString summaryText = translator->TranslateText(TXT("DeathmatchSummary"), TRANSLATION_FILE, TranslationSectionName);
			summaryText.ReplaceInline(TXT("%s"), deathmatch->GetKillLimit().ToString(), SD::DString::CC_IgnoreCase);
			GameLimitLabel->SetText(summaryText);
		}
	}
}

void Scoreboard_Deathmatch::InitializeHeaderLabel (SD::FrameComponent* headerFrame)
{
	Super::InitializeHeaderLabel(headerFrame);

	if (headerFrame != nullptr)
	{
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		SD::LabelComponent* killsLabel = SD::LabelComponent::CreateObject();
		if (headerFrame->AddComponent(killsLabel))
		{
			killsLabel->SetAutoRefresh(false);
			killsLabel->SetPosition(SD::Vector2(0.6f, 0.f));
			killsLabel->SetSize(SD::Vector2(0.2f, 1.f));
			killsLabel->SetWrapText(false);
			killsLabel->SetClampText(false);
			killsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			killsLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			killsLabel->SetText(translator->TranslateText(TXT("Kills"), TRANSLATION_FILE, TranslationSectionName));
			killsLabel->SetAutoRefresh(true);
		}

		SD::LabelComponent* deathsLabel = SD::LabelComponent::CreateObject();
		if (headerFrame->AddComponent(deathsLabel))
		{
			deathsLabel->SetAutoRefresh(false);
			deathsLabel->SetPosition(SD::Vector2(0.8f, 0.f));
			deathsLabel->SetSize(SD::Vector2(0.2f, 1.f));
			deathsLabel->SetWrapText(false);
			deathsLabel->SetClampText(false);
			deathsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			deathsLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			deathsLabel->SetText(translator->TranslateText(TXT("Deaths"), TRANSLATION_FILE, TranslationSectionName));
			deathsLabel->SetAutoRefresh(true);
		}
	}
}

const ScoreComponent* Scoreboard_Deathmatch::GetScoreComponentClass () const
{
	return dynamic_cast<const ScoreComponent_Deathmatch*>(ScoreComponent_Deathmatch::SStaticClass()->GetDefaultObject());
}

DD_END