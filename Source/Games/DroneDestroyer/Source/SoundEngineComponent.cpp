/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SoundEngineComponent.cpp
=====================================================================
*/

#include "MusicPlayer.h"
#include "Sound.h"
#include "SoundEngineComponent.h"
#include "SoundPlayer.h"
#include "SoundPool.h"

DD_BEGIN
IMPLEMENT_ENGINE_COMPONENT(DD, SoundEngineComponent)

const size_t SoundEngineComponent::SOUND_ENGINE_IDX(1);

const SD::DString SoundEngineComponent::SOUND_CLICK_HEAVY(TXT("Click_Heavy"));
const SD::DString SoundEngineComponent::SOUND_CLICK_LIGHT(TXT("Click_Light"));
const SD::DString SoundEngineComponent::SOUND_CLICK_NORMAL(TXT("Click_Normal"));
const SD::DString SoundEngineComponent::SOUND_TYPING(TXT("Typing"));

SoundEngineComponent::SoundEngineComponent () : EngineComponent()
{
	//Noop
}

void SoundEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	//Create the various resource pools
	if (SoundPool::FindSoundPool() == nullptr)
	{
		SoundPool::CreateObject();
	}
}

void SoundEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_DD_NO_PAUSE, TICK_GROUP_PRIORITY_DD_NO_PAUSE);

	Player = SoundPlayer::CreateObject();
	CHECK(Player != nullptr)

	Music = MusicPlayer::CreateObject();
	CHECK(Music != nullptr)

	ImportGameSounds();
}

void SoundEngineComponent::ShutdownComponent ()
{
	if (Player != nullptr)
	{
		Player->Destroy();
	}

	if (Music != nullptr)
	{
		Music->Destroy();
	}

	SoundPool* localSoundPool = SoundPool::FindSoundPool();
	if (localSoundPool != nullptr)
	{
		localSoundPool->Destroy();
	}

	Super::ShutdownComponent();
}

SoundPlayer* SoundEngineComponent::GetPlayer () const
{
	return Player.Get();
}

MusicPlayer* SoundEngineComponent::GetMusic () const
{
	return Music.Get();
}

void SoundEngineComponent::ImportGameSounds ()
{
	SoundPool* localSoundPool = SoundPool::FindSoundPool();
	CHECK(localSoundPool != nullptr)

	//User interface sounds
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Click_Heavy.wav"), SOUND_CLICK_HEAVY);
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Click_Light.wav"), SOUND_CLICK_LIGHT);
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Click_Normal.wav"), SOUND_CLICK_NORMAL);
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Typing.wav"), SOUND_TYPING);
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Game_Start.wav"), TXT("Game_Start"));

	//Character sounds
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Enemy_Damage.wav"), TXT("Enemy_Damage"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Enemy_Death1.wav"), TXT("Enemy_Death1"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Enemy_Death2.wav"), TXT("Enemy_Death2"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Enemy_Death3.wav"), TXT("Enemy_Death3"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Enemy_Death4.wav"), TXT("Enemy_Death4"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Enemy_Death5.wav"), TXT("Enemy_Death5"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("PlayerDeathShort.wav"), TXT("PlayerDeathShort"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Player_Damage.wav"), TXT("Player_Damage"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Player_Low Health.wav"), TXT("Player_Low Health"));

	//Weapon sounds
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Enemy_Shoot.wav"), TXT("Enemy_Shoot"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Impact_Bullet.wav"), TXT("Impact_Bullet"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("Pickup_Weapon.wav"), TXT("Pickup_Weapon"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("W_MachineGun.wav"), TXT("W_MachineGun"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("W_NoAmmo.wav"), TXT("W_NoAmmo"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("W_Pistol.wav"), TXT("W_Pistol"));
	localSoundPool->CreateAndImportSound(Sound::SOUND_DIRECTORY.ToString() + TXT("W_Shotgun.wav"), TXT("W_Shotgun"));
}
DD_END