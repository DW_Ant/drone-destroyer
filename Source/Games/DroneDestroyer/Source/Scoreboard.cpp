/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Scoreboard.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "DroneDestroyerTheme.h"
#include "GameRules.h"
#include "PlayerSoul.h"
#include "Scoreboard.h"
#include "ScoreComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Scoreboard, SD, GuiEntity)

void Scoreboard::InitProps ()
{
	Super::InitProps();

	TranslationSectionName = TXT("Scoreboard");
	GameLimitLabel = nullptr;
	ShouldInitializeHeaders = true;
}

void Scoreboard::BeginObject ()
{
	Super::BeginObject();

	//Scoreboards are invisible by default.
	SetVisibility(false);
}

void Scoreboard::ConstructUI ()
{
	StyleName = DroneDestroyerTheme::SCOREBOARD_STYLE_NAME;

	Super::ConstructUI();

	SD::FrameComponent* gameLimitFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(gameLimitFrame))
	{
		gameLimitFrame->SetPosition(SD::Vector2(0.25f, 0.05f));
		gameLimitFrame->SetSize(SD::Vector2(0.5f, 0.1f));
		if (gameLimitFrame->RenderComponent != nullptr)
		{
			gameLimitFrame->RenderComponent->FillColor.Source.a = 16; //barely visible
		}

		GameLimitLabel = SD::LabelComponent::CreateObject();
		if (gameLimitFrame->AddComponent(GameLimitLabel))
		{
			GameLimitLabel->SetAutoRefresh(false);
			GameLimitLabel->SetPosition(SD::Vector2::ZeroVector);
			GameLimitLabel->SetSize(SD::Vector2(1.f, 1.f));
			GameLimitLabel->SetWrapText(true);
			GameLimitLabel->SetClampText(true);
			GameLimitLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			GameLimitLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			GameLimitLabel->SetAutoRefresh(true);
			InitializeGameLimitLabel();
		}
	}

	SD::FLOAT entryPos = 0.2f;
	SD::FLOAT entryHeight = 0.75f;
	if (ShouldInitializeHeaders)
	{
		SD::FrameComponent* headerFrame = SD::FrameComponent::CreateObject();
		if (AddComponent(headerFrame))
		{
			headerFrame->SetPosition(SD::Vector2(0.125f, 0.2f));
			headerFrame->SetSize(SD::Vector2(0.75f, 0.1f));
			entryHeight -= headerFrame->ReadSize().Y;
			entryPos += headerFrame->ReadSize().Y;
			InitializeHeaderLabel(headerFrame);
		}
	}

	InitializeEntries(SD::Vector2(0.125f, entryPos), SD::Vector2(0.75f, entryHeight));
}

void Scoreboard::UpdatePlayerFields (GameRules* rules)
{
	bool playerFound = false;

	size_t i;
	for (i = 0; i < rules->ReadRankedPlayers().size(); ++i)
	{
		if (!SD::ContainerUtils::IsValidIndex(Entries, i))
		{
			//No more entries available. The players at the bottom of the list are simply not on the scoreboard.
			break;
		}

		Entries.at(i)->UpdateInformation((i+1), rules->ReadRankedPlayers().at(i).Get());
		if (!rules->ReadRankedPlayers().at(i)->GetIsBot())
		{
			playerFound = true;
		}
	}

	if (!playerFound && i > 0)
	{
		--i; //Incase the player was at the last unchecked index
		while (i < rules->ReadRankedPlayers().size())
		{
			if (!rules->ReadRankedPlayers().at(i)->GetIsBot())
			{
				if (!rules->ReadRankedPlayers().at(i)->GetIsSpectator())
				{
					SD::ContainerUtils::GetLast(Entries)->UpdateInformation((i+1), rules->ReadRankedPlayers().at(i).Get());
				}

				break;
			}

			++i;
		}
	}
}

void Scoreboard::InitializeGameLimitLabel ()
{
	//Noop
}

void Scoreboard::InitializeHeaderLabel (SD::FrameComponent* headerFrame)
{
	//Default to showing rank and player name. Subclasses will determine what to display on the right side.
	SD::LabelComponent* playerName = SD::LabelComponent::CreateObject();
	if (headerFrame->AddComponent(playerName))
	{
		playerName->SetAutoRefresh(false);
		playerName->SetPosition(SD::Vector2(0.1f, 0.f));
		playerName->SetSize(SD::Vector2(0.5f, 1.f));
		playerName->SetWrapText(false);
		playerName->SetClampText(false);
		playerName->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
		playerName->SetVerticalAlignment(SD::LabelComponent::VA_Center);

		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		playerName->SetText(translator->TranslateText(TXT("PlayerName"), TRANSLATION_FILE, TranslationSectionName));
		playerName->SetAutoRefresh(true);
	}
}

const ScoreComponent* Scoreboard::GetScoreComponentClass () const
{
	return dynamic_cast<const ScoreComponent*>(ScoreComponent::SStaticClass()->GetDefaultObject());
}

void Scoreboard::InitializeEntries (const SD::Vector2& startingPosition, const SD::Vector2& allowedSpace)
{
	SD::FLOAT entryHeight = 0.05f;
	SD::FLOAT spacing = 0.01f;
	size_t maxEntries = SD::FLOAT::RoundDown(allowedSpace.Y / (entryHeight + spacing)).ToINT().ToUnsignedInt();

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)

	bool halfSize = (localDroneEngine->GetGame()->GetNumBots() > SD::INT(maxEntries));
	if (halfSize)
	{
		entryHeight *= 0.5f;
		spacing *= 0.5f;
		maxEntries *= 2;
	}

	SD::FLOAT drawHeight = startingPosition.Y;
	for (size_t i = 0; i < localDroneEngine->GetGame()->ReadRankedPlayers().size() && i < maxEntries; ++i)
	{
		if (localDroneEngine->GetGame()->ReadRankedPlayers().at(i)->GetIsSpectator())
		{
			//Ignore spectators
			maxEntries++;
			continue;
		}

		ScoreComponent* playerEntry = GetScoreComponentClass()->CreateObjectOfMatchingClass();
		if (AddComponent(playerEntry))
		{
			playerEntry->SetPosition(SD::Vector2(startingPosition.X, drawHeight));
			playerEntry->SetSize(SD::Vector2(allowedSpace.X, entryHeight));
			drawHeight += entryHeight + spacing;
			if (halfSize)
			{
				playerEntry->SetFontSize(GetScoreComponentClass()->GetFontSize() / 2);
			}

			Entries.push_back(playerEntry);
		}
	}

	UpdatePlayerFields(localDroneEngine->GetGame());
}

DD_END