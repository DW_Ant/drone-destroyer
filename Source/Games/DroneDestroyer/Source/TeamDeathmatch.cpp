/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TeamDeathmatch.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "Hud.h"
#include "HudTeamDeathmatch.h"
#include "PlayerSoul.h"
#include "Scoreboard_TeamDeathmatch.h"
#include "TeamDeathmatch.h"
#include "TimeLimitComponent.h"
#include "TimeLimitHudComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, TeamDeathmatch, DD, GameRules)

void TeamDeathmatch::InitProps ()
{
	Super::InitProps();

	KillLimit = 50;
	TimeLimit = -1.f;
	InSuddenDeath = false;
	TimeComponent = nullptr;

	TeamColors.push_back(SD::Color(128, 0, 0));
	TeamColors.push_back(SD::Color(0, 0, 128));
	TeamColors.push_back(SD::Color(0, 128, 0));
	TeamColors.push_back(SD::Color(128, 128, 0));
	TeamColors.push_back(SD::Color(128, 0, 128));
	TeamColors.push_back(SD::Color(0, 128, 128));
	TeamNames.push_back(TXT("Red"));
	TeamNames.push_back(TXT("Blue"));
	TeamNames.push_back(TXT("Green"));
	TeamNames.push_back(TXT("Yellow"));
	TeamNames.push_back(TXT("Magenta"));
	TeamNames.push_back(TXT("Cyan"));

	IconTextureName = TXT("DroneDestroyer.Interface.TeamDeathmatch");
	DescriptionKey = TXT("TeamDeathmatch");
	ConfigName = TXT("TeamDeathmatch");

	GameSettings.push_back(SGameRulesOption(TXT("KillLimit"), KillLimit));
	GameSettings.push_back(SGameRulesOption(TXT("TimeLimit"), 5));
	GameSettings.push_back(SGameRulesOption(TXT("NumTeams"), 2));
}

bool TeamDeathmatch::CanBeSelected () const
{
	return true;
}

SD::INT TeamDeathmatch::GetNumTeams () const
{
	return SD::INT(Teams.size());
}

void TeamDeathmatch::ApplyGameSettings (const std::vector<SD::DString>& newSettings)
{
	CHECK(newSettings.size() == 3)
	KillLimit = std::atoi(newSettings.at(0).ToCString());
	TimeLimit = std::stof(newSettings.at(1).ToCString()) * 60.f;
	SD::INT numTeams = SD::Utils::Clamp<SD::INT>(std::stoi(newSettings.at(2).ToCString()), 1, SD::INT(TeamColors.size()));

	SD::ContainerUtils::Empty(OUT Teams);
	for (SD::INT i = 0; i < numTeams; ++i)
	{
		STeam newTeam;
		newTeam.TeamNum = i;
		newTeam.TeamScore = 0;
		newTeam.TeamColor = TeamColors.at(i.ToUnsignedInt());
		newTeam.TeamName = TeamNames.at(i.ToUnsignedInt());

		Teams.push_back(newTeam);
	}
}

void TeamDeathmatch::InitializeGame ()
{
	//Add players to their corresponding teams.
	for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
	{
		if (soul->GetIsSpectator())
		{
			continue;
		}

		if (SD::ContainerUtils::IsValidIndex(Teams, soul->GetTeamNumber()))
		{
			Teams.at(soul->GetTeamNumber().ToUnsignedInt()).Members.push_back(soul);
		}
	}

	Super::InitializeGame();

	TimeComponent = TimeLimitComponent::CreateObject();
	if (AddComponent(TimeComponent))
	{
		TimeComponent->OnExpired = SDFUNCTION(this, TeamDeathmatch, HandleTimeLimitExpired, void);
		TimeComponent->SetTimeLimit(TimeLimit);
	}
	else
	{
		TimeComponent = nullptr;
	}

	if (ScoreboardInstance != nullptr)
	{
		ScoreboardInstance->UpdatePlayerFields(this);
	}
}

void TeamDeathmatch::AddSpectator (PlayerSoul* newSpectator)
{
	Super::AddSpectator(newSpectator);

	size_t teamIdx = newSpectator->GetTeamNumber().ToUnsignedInt();
	if (SD::ContainerUtils::IsValidIndex(Teams, teamIdx))
	{
		for (size_t i = 0; i < Teams.at(teamIdx).Members.size(); ++i)
		{
			if (Teams.at(teamIdx).Members.at(i).Get() == newSpectator)
			{
				Teams.at(teamIdx).Members.erase(Teams.at(teamIdx).Members.begin() + i);
				break;
			}
		}
	}
}

Character* TeamDeathmatch::CreateCharacter (PlayerSoul* soul)
{
	Character* newChar = Super::CreateCharacter(soul);
	if (newChar != nullptr)
	{
		if (SD::ContainerUtils::IsValidIndex(Teams, soul->GetTeamNumber()))
		{
			SD::SceneTransformComponent* auraTransform = SD::SceneTransformComponent::CreateObject();
			if (newChar->AddComponent(auraTransform))
			{
				auraTransform->EditTranslation().Z = -5.f;

				SD::SolidColorRenderComponent* teamAura = SD::SolidColorRenderComponent::CreateObject();
				if (auraTransform->AddComponent(teamAura))
				{
					teamAura->CenterOrigin = true;
					teamAura->Shape = SD::SolidColorRenderComponent::ST_Circle;
					teamAura->BaseSize = newChar->GetCollisionRadius() * 2.5f;
					teamAura->SolidColor = Teams.at(soul->GetTeamNumber().ToUnsignedInt()).TeamColor;
					teamAura->SolidColor.Source.a = 160;
				
					DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(teamAura);
				}
			}
		}
	}

	return newChar;
}

void TeamDeathmatch::AddComponentsToHud (Hud* playerHud)
{
	Super::AddComponentsToHud(playerHud);

	if (playerHud != nullptr)
	{
		if (TimeComponent != nullptr)
		{
			TimeLimitHudComponent* timeHud = TimeLimitHudComponent::CreateObject();
			if (playerHud->AddComponent(timeHud))
			{
				timeHud->BindClockTo(TimeComponent);
			}
		}

		HudScore = HudTeamDeathmatch::CreateObject();
		if (playerHud->AddComponent(HudScore.Get()))
		{
			//Place above the health
			HudScore->SetPosition(SD::Vector2::ZeroVector);
			HudScore->SetSize(SD::Vector2(1.f, 1.f));
			HudScore->InitializeTeams(ReadTeams());
		}
	}
}

void TeamDeathmatch::ScoreKill (Character* killed, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg)
{
	Super::ScoreKill(killed, killer, killedBy, deathMsg);

	//Increment the associated team's score
	if (killer != nullptr && killed != nullptr && killed->GetSoul() != nullptr && killer != killed->GetSoul())
	{
		STeam& killerTeam = Teams.at(killer->GetTeamNumber().ToUnsignedInt());
		killerTeam.TeamScore++;

		if (HudScore != nullptr)
		{
			//Update score for the player
			PlayerSoul* human = nullptr;
			if (!killer->GetIsBot())
			{
				human = killer;
			}
			else if (killed->IsHuman())
			{
				human = killed->GetSoul();
			}

			HudScore->UpdateScores(ReadTeams(), human);
			if (ScoreboardInstance != nullptr)
			{
				ScoreboardInstance->UpdatePlayerFields(this);
			}
		}

		if (InSuddenDeath || killerTeam.TeamScore >= KillLimit)
		{
			SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
			CHECK(translator != nullptr);

			SD::DString translationKey = (InSuddenDeath) ? TXT("SuddenDeathKill") : TXT("ReachKillLimit");
			SD::DString endGameMsg = translator->TranslateText(translationKey, TRANSLATION_FILE, TXT("TeamDeathmatch"));
			endGameMsg.ReplaceInline(TXT("%t"), killerTeam.TeamName, SD::DString::CC_IgnoreCase);
			endGameMsg.ReplaceInline(TXT("%s"), killer->ReadPlayerName(), SD::DString::CC_IgnoreCase);

			SD::Vector3 camLocation(SD::Vector3::ZeroVector);
			if (killer->GetBody() != nullptr)
			{
				camLocation = killer->GetBody()->ReadAbsTranslation();
			}
			else if (killed != nullptr)
			{
				camLocation = killed->ReadAbsTranslation();
			}

			ExecuteVictorySequence(endGameMsg, camLocation);
		}
	}
}

void TeamDeathmatch::SortScores ()
{
	Super::SortScores();

	for (STeam& team : Teams)
	{
		std::sort(team.Members.begin(), team.Members.end(), [](const SD::DPointer<PlayerSoul>& a, const SD::DPointer<PlayerSoul>& b)
		{
			//Prioritize on kills
			if (a->GetNumKills() != b->GetNumKills())
			{
				return (a->GetNumKills() > b->GetNumKills());
			}
		
			//Otherwise sort by number of deaths
			return (a->GetNumDeaths() < b->GetNumDeaths());
		});
	}
}

const Scoreboard* TeamDeathmatch::GetScoreboardClass () const
{
	return dynamic_cast<const Scoreboard_TeamDeathmatch*>(Scoreboard_TeamDeathmatch::SStaticClass()->GetDefaultObject());
}

bool TeamDeathmatch::CanCreateBody (PlayerSoul* requester)
{
	if (!Super::CanCreateBody(requester))
	{
		return false;
	}

	return !InSuddenDeath;
}

void TeamDeathmatch::SuddenDeath ()
{
	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	//Handle case if there are zero or one teams
	if (Teams.size() < 2)
	{
		SD::DString endGameMsg;
		endGameMsg = translator->TranslateText(TXT("TimeIsUp"), TRANSLATION_FILE, TXT("TeamDeathmatch"));
		ExecuteVictorySequence(endGameMsg, SD::Vector3::ZeroVector);
		return;
	}

	//Look at the high scoring team
	bool highScoreTied = false;
	SD::INT highScore = 0;
	const STeam* highScoreTeam = nullptr;
	for (const STeam& team : Teams)
	{
		if (team.TeamScore == highScore)
		{
			highScoreTied = true;
		}
		else if (team.TeamScore > highScore)
		{
			highScoreTied = false;
			highScore = team.TeamScore;
			highScoreTeam = &team;
		}
	}

	if (!highScoreTied && highScoreTeam != nullptr)
	{
		SD::Vector3 camLocation(SD::Vector3::ZeroVector);
		//Find a player alive. Since this is sorted, it'll prioritize the highest scoring team member.
		for (size_t i = 0; i < highScoreTeam->Members.size(); ++i)
		{
			if (highScoreTeam->Members.at(i)->IsAlive())
			{
				camLocation = highScoreTeam->Members.at(i)->GetBody()->ReadAbsTranslation();
				break;
			}
		}

		SD::DString endMsg = translator->TranslateText(TXT("TimeLimitReached"), TRANSLATION_FILE, TXT("TeamDeathmatch"));
		endMsg.ReplaceInline(TXT("%t"), highScoreTeam->TeamName, SD::DString::CC_IgnoreCase);
		ExecuteVictorySequence(endMsg, camLocation);
		return;
	}

	//There's a tie. Kill everyone that's not at the tied teams
	for (STeam team : Teams)
	{
		if (team.TeamScore >= highScore)
		{
			//This team has tied with the high score. They will carry to sudden death
			continue;
		}

		for (size_t i = 0; i < team.Members.size(); ++i)
		{
			if (team.Members.at(i)->IsAlive())
			{
				team.Members.at(i)->GetBody()->Kill(999, nullptr, nullptr, TXT("SuddenDeath"));
			}
		}
	}

	InSuddenDeath = true; //Disables respawning. Also grants victory to the team that makes the next kill.
}

void TeamDeathmatch::HandleTimeLimitExpired ()
{
	SuddenDeath();
}

DD_END