/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GameRules.cpp
=====================================================================
*/

#include "Arena.h"
#include "BotDialectComponent.h"
#include "DroneDestroyerEngineComponent.h"
#include "GameModifier.h"
#include "GameRules.h"
#include "HandGun.h"
#include "Hud.h"
#include "HudMessage.h"
#include "Msg_Killed.h"
#include "Msg_KilledBy.h"
#include "PlayerSoul.h"
#include "PlayerStart.h"
#include "Projectile.h"
#include "Scoreboard.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, GameRules, SD, Entity)

void GameRules::InitProps ()
{
	Super::InitProps();

	IconTextureName = SD::DString::EmptyString;
	DescriptionKey = TXT("Unknown");
	ConfigName = TXT("GameRules");
	FirstPlayer = nullptr;
	GameDifficulty = Medium;
	NumBots = 0;
	BotDialect = nullptr;
	MaxBotsForDialect = 24;
	UseStandardBotDialectKillMsgs = true;
	UseStandardGreetings = true;
	ForceRespawn = false;
	ScoreboardInstance = nullptr;
	HasEnded = false;

	EndGameCamZoom.StartZoom = 0.05f;
	EndGameCamZoom.EndZoom = 0.01f;
	EndGameCamZoom.ZoomDelay = 8.f;
	EndGameCamZoom.ZoomTime = 22.f;
	EndGameCamZoom.EndGameTime = -1.f;
	EndGameCamZoom.Tick = nullptr;
}

void GameRules::Destroyed ()
{
	DestroyGameModifiers();

	if (EndGameCamZoom.Tick != nullptr)
	{
		EndGameCamZoom.Tick->Destroy();
		EndGameCamZoom.Tick = nullptr;
	}

	if (ScoreboardInstance != nullptr)
	{
		ScoreboardInstance->Destroy();
		ScoreboardInstance = nullptr;
	}

	PlayerSoul* curSoul = FirstPlayer;
	FirstPlayer = nullptr; //In order to prevent iterating through the loop every time we destroy the soul, simply null out the leading pointer to the linked list before purging souls.

	while (curSoul != nullptr)
	{
		PlayerSoul* next = curSoul->NextSoul;
		curSoul->Destroy();
		curSoul = next;
	}

	Super::Destroyed();
}

SD::DString GameRules::GetDifficultyName (EGameDifficulty difficulty)
{
	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	const SD::DString sectionName(TXT("GameRules"));
	switch (difficulty)
	{
		case(Pacifist):
			return translator->TranslateText(TXT("Pacifist"), TRANSLATION_FILE, sectionName);

		case(Novice):
			return translator->TranslateText(TXT("Novice"), TRANSLATION_FILE, sectionName);

		case(Easy):
			return translator->TranslateText(TXT("Easy"), TRANSLATION_FILE, sectionName);

		case(Medium):
			return translator->TranslateText(TXT("Medium"), TRANSLATION_FILE, sectionName);

		case(Hard):
			return translator->TranslateText(TXT("Hard"), TRANSLATION_FILE, sectionName);
	}

	return TXT("Unknown difficulty");
}

bool GameRules::CanBeSelected () const
{
	return false;
}

SD::INT GameRules::GetNumTeams () const
{
	return 0;
}

void GameRules::ApplyGameSettings (const std::vector<SD::DString>& newSettings)
{
	//Noop
}

void GameRules::AddGameModifier (const GameModifier* gameModCdo)
{
	GameModifiers.push_back(gameModCdo->CreateObjectOfMatchingClass());
}

void GameRules::DestroyGameModifiers ()
{
	for (GameModifier* gameMod : GameModifiers)
	{
		gameMod->Destroy();
	}
	SD::ContainerUtils::Empty(OUT GameModifiers);
}

void GameRules::InitializeGame ()
{
	if (NumBots <= MaxBotsForDialect)
	{
		BotDialect = BotDialectComponent::CreateObject();
		if (AddComponent(BotDialect))
		{
			InitializeBotDialect();
		}
	}

	if (GetScoreboardClass() != nullptr)
	{
		ScoreboardInstance = GetScoreboardClass()->CreateObjectOfMatchingClass();
		ScoreboardInstance->RegisterToMainWindow();
	}

	if (BotDialect != nullptr)
	{
		//Find the human
		PlayerSoul* human = nullptr;
		for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
		{
			if (!soul->GetIsBot())
			{
				if (!soul->GetIsSpectator())
				{
					human = soul;
				}

				break;
			}
		}

		if (human != nullptr)
		{
			//Have the bots greet
			SD::FLOAT msgChance = 2.f; //First few bots always greet
			for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
			{
				if (soul == human)
				{
					continue;
				}

				if (SD::RandomUtils::fRand() < msgChance)
				{
					BotDialect->SayGreetingFrom(soul, human);
					msgChance *= 0.75f; //Reduce the chance to greet for other bots
				}
			}
		}
	}
}

void GameRules::AddComponentsToHud (Hud* playerHud)
{
	//Noop
}

void GameRules::RegisterNewSoul (PlayerSoul* newSoul)
{
	newSoul->NextSoul = FirstPlayer;
	FirstPlayer = newSoul;

	if (!newSoul->GetIsSpectator())
	{
		RankedPlayers.push_back(newSoul);
	}
}

void GameRules::AddSpectator (PlayerSoul* newSpectator)
{
	for (size_t i = 0; i < RankedPlayers.size(); ++i)
	{
		if (RankedPlayers.at(i).Get() == newSpectator)
		{
			RankedPlayers.erase(RankedPlayers.begin() + i);
			return;
		}
	}
}

void GameRules::RemoveSoul (PlayerSoul* oldSoul)
{
	for (PlayerSoul* soul = FirstPlayer; soul != nullptr; soul = soul->NextSoul)
	{
		if (soul->NextSoul == oldSoul)
		{
			soul->NextSoul = oldSoul->NextSoul;
			break;
		}
	}
}

Character* GameRules::CreateCharacter (PlayerSoul* soul)
{
	if (!CanCreateBody(soul))
	{
		return nullptr;
	}

	Character* newCharacter = Character::CreateObject();
	SD::Vector3 spawnLocation;
	SD::Rotator spawnRotation;
	if (!FindSpawnLocation(newCharacter, soul, OUT spawnLocation, OUT spawnRotation))
	{
		newCharacter->Destroy();
		return nullptr;
	}

	newCharacter->SetTranslation(spawnLocation);
	newCharacter->SetRotation(spawnRotation);
	newCharacter->InitializePhysics();

	GiveDefaultInventoryTo(newCharacter);
	for (GameModifier* gameMod : GameModifiers)
	{
		gameMod->ModifyPlayer(newCharacter);
	}

	return newCharacter;
}

void GameRules::ScoreKill (Character* killed, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg)
{
	CHECK(killed != nullptr) //Can't call ScoreKill if no one was killed
	PlayerSoul* killedSoul = killed->GetSoul();

	//Handle kill credit
	if (killer != nullptr && killer != killedSoul)
	{
		if (killer != nullptr)
		{
			killer->IncrementKillCount();

			//"You killed Player"
			if (!killer->GetIsBot() && killedSoul != nullptr)
			{
				HudMessage::CreateAndRegisterMsgToHud(Msg_Killed::SStaticClass(), killedSoul->GetPlayerName());
			}

			//"You were killed by Player"
			if (killed->IsHuman())
			{
				HudMessage::CreateAndRegisterMsgToHud(Msg_KilledBy::SStaticClass(), killer->GetPlayerName());
			}
		}
	}

	//Handle death credit
	if (killedSoul != nullptr)
	{
		killedSoul->IncrementDeathCount();

		if (killer == killedSoul)
		{
			killedSoul->DecrementKillCount();
		}
	}

	UpdateScores();

	//Broadcast a message
	if (!deathMsg.IsEmpty() && AllowDeathMessageFor(killed))
	{
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr)

		Hud* hud = localDroneEngine->GetPlayerHud();
		if (hud != nullptr)
		{
			SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
			CHECK(translator != nullptr)

			SD::DString killerName(TXT("Someone"));
			if (killer != nullptr)
			{
				killerName = killer->GetPlayerName();
			}

			SD::DString victimName(TXT("Someone"));
			if (killed != nullptr && killed->GetSoul() != nullptr)
			{
				victimName = killed->GetSoul()->GetPlayerName();
			}

			SD::DString formattedStr = translator->TranslateText(deathMsg, TRANSLATION_FILE, TXT("DeathMessages"));
			formattedStr.ReplaceInline(TXT("%k"), killerName, SD::DString::CC_IgnoreCase);
			formattedStr.ReplaceInline(TXT("%v"), victimName, SD::DString::CC_IgnoreCase);
			hud->AddMessageFeed(formattedStr);
		}
	}

	if (BotDialect != nullptr && GameDifficulty > EGameDifficulty::Pacifist && killed != nullptr && killer != nullptr && killedBy != nullptr)
	{
		SD::FLOAT chance = (NumBots > (MaxBotsForDialect / 2)) ? 0.25f : 0.5f;
		if (killed->IsHuman() || !killer->GetIsBot())
		{
			//If the human is involved. Always taunt or complain
			chance = 1.f;
		}
		
		if (SD::RandomUtils::fRand() < chance)
		{
			BotDialect->SayDeathMessage(killed->GetSoul(), killer, killedBy->GetWeap());
		}
	}
}

void GameRules::SetGameDifficulty (EGameDifficulty newGameDifficulty)
{
	GameDifficulty = newGameDifficulty;
}

void GameRules::SetNumBots (SD::INT newNumBots)
{
	NumBots = SD::Utils::Max<SD::INT>(newNumBots, 0);
}

const SD::DString& GameRules::ReadDescription () const
{
	if (Description.IsEmpty())
	{
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		Description = translator->TranslateText(DescriptionKey, TRANSLATION_FILE, TXT("GameRules"));
	}

	return Description;
}

const Scoreboard* GameRules::GetScoreboardClass () const
{
	return nullptr;
}

void GameRules::InitializeBotDialect ()
{
	CHECK(BotDialect != nullptr)

	if (!UseStandardGreetings && !UseStandardBotDialectKillMsgs)
	{
		return;
	}

	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile((SD::Directory::CONFIG_DIRECTORY).ToString() + TXT("BotDialect.ini"), false))
	{
		config->Destroy();
		return;
	}

	if (UseStandardGreetings)
	{
		std::vector<SD::DString> greetings;
		if (GameDifficulty == EGameDifficulty::Pacifist)
		{
			config->GetArrayValues(TXT("BotMessages"), TXT("PacifistGreeting"), OUT greetings);
		}
		else
		{
			config->GetArrayValues(TXT("BotMessages"), TXT("BotGreeting"), OUT greetings);
		}

		BotDialect->AddGreetings(greetings);
	}

	if (UseStandardBotDialectKillMsgs)
	{
		std::vector<SD::DString> msgs;
		config->GetArrayValues(TXT("BotMessages"), TXT("BotDeathMsg"), OUT msgs);
		BotDialect->AddDeaths(msgs);

		SD::ContainerUtils::Empty(OUT msgs);
		config->GetArrayValues(TXT("BotMessages"), TXT("BotTaunt"), OUT msgs);
		BotDialect->AddTaunts(msgs);
	}

	config->Destroy();
}

bool GameRules::CanCreateBody (PlayerSoul* requester)
{
	return true;
}

bool GameRules::FindSpawnLocation (Character* body, PlayerSoul* soul, SD::Vector3& outLocation, SD::Rotator& outRotation) const
{
	outLocation = SD::Vector3::ZeroVector;
	outRotation = SD::Rotator::ZERO_ROTATOR;

	//Iterate through all player starts
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)
	Arena* environment = localDroneEngine->GetEnvironment();

	if (environment != nullptr)
	{
		std::vector<PlayerStart*> playerStarts;
		for (SD::Entity* entity : environment->ReadArenaEntities())
		{
			if (PlayerStart* playerStart = dynamic_cast<PlayerStart*>(entity))
			{
				playerStarts.push_back(playerStart);
			}
		}

		if (!SD::ContainerUtils::IsEmpty(playerStarts))
		{
			//Pick a random start
			PlayerStart* chosenOne = playerStarts.at(SD::RandomUtils::Rand(SD::INT(playerStarts.size())).ToUnsignedInt());
			if (chosenOne != nullptr)
			{
				outLocation = chosenOne->ReadTranslation();
				outRotation = chosenOne->ReadRotation();
			}
		}
	}

	outLocation.Z = DroneDestroyerEngineComponent::DRAW_ORDER_CHARACTERS;

	return true;
}

void GameRules::GiveDefaultInventoryTo (Character* newCharacter)
{
	CHECK(newCharacter != nullptr)

	HandGun* starterPistol = HandGun::CreateObject();
	starterPistol->GiveTo(newCharacter);
	starterPistol->InitializePhysics();
}

bool GameRules::AllowDeathMessageFor (Character* killed) const
{
	return true;
}

void GameRules::UpdateScores ()
{
	SortScores();
	if (ScoreboardInstance != nullptr)
	{
		ScoreboardInstance->UpdatePlayerFields(this);
	}
}

void GameRules::SortScores ()
{
	//Remove any spectators or destroyed souls
	size_t i = 0;
	while (i < RankedPlayers.size())
	{
		if (RankedPlayers.at(i) == nullptr || RankedPlayers.at(i)->GetIsSpectator())
		{
			RankedPlayers.erase(RankedPlayers.begin() + i);
			continue;
		}

		++i;
	}
}

void GameRules::ExecuteVictorySequence (const SD::DString& endGameMsg, const SD::Vector3& camLocation)
{
	HasEnded = true;

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetMainCamera() != nullptr && localDroneEngine->GetPlayerHud() != nullptr)

	SD::TopDownCamera* cam = localDroneEngine->GetMainCamera();
	if (!camLocation.IsEmpty() && cam != nullptr)
	{
		cam->SetRelativeTo(nullptr);
		cam->SetTranslation(camLocation);
		cam->EditTranslation().Z = localDroneEngine->DRAW_ORDER_CAMERA;
		cam->Zoom = EndGameCamZoom.StartZoom;

		EndGameCamZoom.Tick = SD::TickComponent::CreateObject(TICK_GROUP_DD_NO_PAUSE);
		if (cam->AddComponent(EndGameCamZoom.Tick))
		{
			EndGameCamZoom.Tick->SetTickHandler(SDFUNCTION_1PARAM(this, GameRules, HandleCamZoomTick, void, SD::FLOAT));
		}
		else
		{
			EndGameCamZoom.Tick = nullptr;
		}
	}

	SD::Engine* owningEngine = localDroneEngine->GetOwningEngine();
	CHECK(owningEngine != nullptr)
	EndGameCamZoom.EndGameTime = owningEngine->GetElapsedTime();

	localDroneEngine->SetPausedGame(true);

	if (!endGameMsg.IsEmpty())
	{
		localDroneEngine->GetPlayerHud()->FlashEndGameMessage(endGameMsg);
		localDroneEngine->GetPlayerHud()->OnEndGameMsgFadeOut = SDFUNCTION(this, GameRules, HandleEndGameMsgFadeOutFinished, void);
	}
}

void GameRules::HandleCamZoomTick (SD::FLOAT deltaSec)
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetMainCamera() != nullptr)

	SD::Engine* localEngine = localDroneEngine->GetOwningEngine();
	CHECK(localEngine != nullptr)

	if (localEngine->GetElapsedTime() - EndGameCamZoom.EndGameTime <= EndGameCamZoom.ZoomDelay)
	{
		return;
	}

	SD::FLOAT alpha = ((localEngine->GetElapsedTime() - EndGameCamZoom.EndGameTime) - EndGameCamZoom.ZoomDelay) / EndGameCamZoom.ZoomTime;
	if (alpha >= 1.f)
	{
		//We're done zooming
		localDroneEngine->GetMainCamera()->Zoom = EndGameCamZoom.EndZoom;
		EndGameCamZoom.Tick->Destroy();
		EndGameCamZoom.Tick = nullptr;
		return;
	}

	SD::FLOAT newZoom = SD::Utils::Lerp(alpha, EndGameCamZoom.StartZoom, EndGameCamZoom.EndZoom);
	localDroneEngine->GetMainCamera()->Zoom = newZoom;
}

void GameRules::HandleEndGameMsgFadeOutFinished ()
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	//Show scoreboard
	if (ScoreboardInstance != nullptr)
	{
		ScoreboardInstance->SetVisibility(true);
	}

	//Set the hud's visibility to the opposite of the scoreboard
	if (Hud* playerHud = localDroneEngine->GetPlayerHud())
	{
		playerHud->SetVisibility(false);
		playerHud->OnEndGameMsgFadeOut.ClearFunction();
	}
}
DD_END