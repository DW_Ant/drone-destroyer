/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlasmaProjectile.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "PlasmaProjectile.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, PlasmaProjectile, DD, Projectile)

void PlasmaProjectile::InitProps ()
{
	Super::InitProps();

	Damage = 3;
	VelocityMagnitude = 2800.f; //28 meters per second
	LifeSpan = 3.f;
	ImpactSoundVolume = 0.1f; //This weapon is spammy. Reduce volume
	DeathMessageKey = TXT("Plasma");
}

void PlasmaProjectile::InitializeRenderComponents ()
{
	SD::SpriteComponent* sprite = SD::SpriteComponent::CreateObject();
	if (AddComponent(sprite))
	{
		SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)

		sprite->SetSpriteTexture(localTexturePool->FindTexture(TXT("DroneDestroyer.Enemies.Enemies6")));
		sprite->CenterOrigin = true;
		sprite->EditBaseSize() = SD::Vector2(CollisionRadius * 5.f, CollisionRadius * 5.f);
		DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(sprite);
	}
}
DD_END