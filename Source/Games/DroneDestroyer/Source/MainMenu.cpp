/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MainMenu.cpp
=====================================================================
*/

#include "MainMenu.h"
#include "MenuPanel_Title.h"
#include "MenuPanelComponent.h"
#include "SoundEngineComponent.h"
#include "SoundMessenger.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, MainMenu, SD, GuiEntity)

void MainMenu::ConstructUI ()
{
	Super::ConstructUI();

	PushPanel(dynamic_cast<const MenuPanel_Title*>(MenuPanel_Title::SStaticClass()->GetDefaultObject()));
}

bool MainMenu::HandleInput (const sf::Event& evnt)
{
	//Give priority to sub components
	bool consumedEvent = Super::HandleInput(evnt);

	if (!consumedEvent)
	{
		if (evnt.key.code == sf::Keyboard::Escape && evnt.type == sf::Event::KeyReleased)
		{
			consumedEvent = true;
			NavigateBack();
		}
	}

	return consumedEvent;
}

void MainMenu::Destroyed ()
{
	//Save and pop each panel
	while (!SD::ContainerUtils::IsEmpty(Panels))
	{
		SD::ContainerUtils::GetLast(Panels)->SavePanelState();
		SD::ContainerUtils::GetLast(Panels)->Destroy();
		Panels.pop_back();
	}

	Super::Destroyed();
}

void MainMenu::NavigateBack ()
{
	if (Panels.size() > 1)
	{
		if (SoundMessenger* msg = SoundMessenger::GetMessenger())
		{
			msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_LIGHT);
		}

		SD::ContainerUtils::GetLast(Panels)->SavePanelState();
		SD::ContainerUtils::GetLast(Panels)->Destroy();
		Panels.pop_back();
		SD::ContainerUtils::GetLast(Panels)->SetVisibility(true);
	}
}

MenuPanelComponent* MainMenu::PushPanel (const MenuPanelComponent* menuPanelCdo)
{
	MenuPanelComponent* newPanel = menuPanelCdo->CreateObjectOfMatchingClass();
	if (AddComponent(newPanel))
	{
		if (!SD::ContainerUtils::IsEmpty(Panels))
		{
			SD::ContainerUtils::GetLast(Panels)->SetVisibility(false);
		}

		Panels.push_back(newPanel);
		newPanel->RestorePanel();
	}
	else
	{
		newPanel = nullptr;
	}

	return newPanel;
}

DD_END