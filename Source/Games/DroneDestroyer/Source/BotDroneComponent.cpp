/*
/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BotDroneComponent.cpp
=====================================================================
*/

#include "BotDroneComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, BotDroneComponent, DD, BotCharacterComponent)

void BotDroneComponent::InitProps ()
{
	Super::InitProps();

	CombatSightRadius = 0.f;
	PrioritizePickupsWhenRoaming = false;
	CheckForObscuredEnemies = false;

	//Significantly increase intervals to reduce processing usage
	ProcessNavigationInterval *= 3.f;
	ProcessCombatInterval *= 2.f;
	ProcessPerceptionInterval *= 2.f;
	NavigationReach *= 2.f; //Wide approximation. These things don't follow collision so why not?
}

bool BotDroneComponent::IsAbleToFight () const
{
	return (OwningBody != nullptr);
}

void BotDroneComponent::ProcessStuckCheck ()
{
	//Drones can't get stuck since they ignore geometry. Do nothing here.
}

SD::Vector3 BotDroneComponent::NavigateFromLost ()
{
	if (!SD::ContainerUtils::IsEmpty(PastDestinations))
	{
		//Since bots ignore collision, it's safe to assume they can always find they're way back to where they were before they got lost.
		return PastDestinations.at(PastDestinationIdx);
	}

	//Fly to the origin at least. Although 0,0,0 is a valid destination, we slightly shift it so it's easier to debug.
	return SD::Vector3(1.f, 0.f, 0.f);

}
DD_END