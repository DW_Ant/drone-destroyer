/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Extinction.cpp
=====================================================================
*/

#include "BotDialectComponent.h"
#include "Drone.h"
#include "DroneDestroyerEngineComponent.h"
#include "DroneWeapon_Plasma.h"
#include "DroneWeapon_PulseRifle.h"
#include "Extinction.h"
#include "Hud.h"
#include "HudExtinction.h"
#include "HudMessage.h"
#include "Msg_Eliminated.h"
#include "Msg_Countdown.h"
#include "NavigationComponent.h"
#include "PlayerSoul.h"
#include "TimeLimitComponent.h"
#include "TimeLimitHudComponent.h"
#include "Scoreboard_Extinction.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Extinction, DD, GameRules)

void Extinction::InitProps ()
{
	Super::InitProps();

	Wave = 1;
	StartingWave = Wave;
	MaxWave = 12;
	NumLivesPerWave = 1;
	BaseNumDrones = 20;
	NumExtraDronesPerWave = 15;
	MaxDronesAtOnce = 100;
	NumDronesRemaining = 1;
	WaveTimeLimit = 300;
	DroneSpawnInterval = 2.f;
	LastDroneSpawnTime = -1.f;
	WaveInterval = 15;
	WaveIntervalTimeRemaining = WaveInterval;

	TimeComponent = nullptr;
	WaveTimeLimitComp = nullptr;
	GameHud = nullptr;
	WaveIntervalTick = nullptr;
	SpawnDroneTick = nullptr;
	PurgeDroneTick = nullptr;
	DroneSprites = nullptr;

	DronePurgeLimit = 5;

	IconTextureName = TXT("DroneDestroyer.Interface.Extinction");
	DescriptionKey = TXT("Extinction");
	ConfigName = TXT("Extinction");
	UseStandardBotDialectKillMsgs = false;
	UseStandardGreetings = false;
	MaxBotsForDialect = 32; //A lot of bots can chat since they're not chatting at each death.
	ForceRespawn = true; //Can't camp by staying dead since the waves can time out.

	GameSettings.push_back(SGameRulesOption(TXT("NumLives"), NumLivesPerWave));
	GameSettings.push_back(SGameRulesOption(TXT("NumWaves"), MaxWave));
	GameSettings.push_back(SGameRulesOption(TXT("NumDrones"), BaseNumDrones));
	GameSettings.push_back(SGameRulesOption(TXT("DronesPerWave"), NumExtraDronesPerWave));
	GameSettings.push_back(SGameRulesOption(TXT("DroneSpawnInterval"), DroneSpawnInterval.ToINT()));
	GameSettings.push_back(SGameRulesOption(TXT("MaxDrones"), MaxDronesAtOnce));
	GameSettings.push_back(SGameRulesOption(TXT("WaveTimeLimit"), WaveTimeLimit));
	GameSettings.push_back(SGameRulesOption(TXT("StartingWave"), StartingWave));
}

bool Extinction::CanBeSelected () const
{
	return true;
}

SD::INT Extinction::GetNumTeams () const
{
	return 1;
}

void Extinction::ApplyGameSettings (const std::vector<SD::DString>& newSettings)
{
	Super::ApplyGameSettings(newSettings);

	CHECK(newSettings.size() == 8) //Extinction is expecting 8 settings
	NumLivesPerWave = SD::Utils::Max<SD::INT>(std::atoi(newSettings.at(0).ToCString()), 1);
	MaxWave = SD::Utils::Max<SD::INT>(std::atoi(newSettings.at(1).ToCString()), 1);
	BaseNumDrones = SD::Utils::Max<SD::INT>(std::atoi(newSettings.at(2).ToCString()), 1);
	NumExtraDronesPerWave = std::atoi(newSettings.at(3).ToCString());
	DroneSpawnInterval = std::stof(newSettings.at(4).ToCString());
	MaxDronesAtOnce = SD::Utils::Clamp<SD::INT>(std::atoi(newSettings.at(5).ToCString()), 1, 500);
	WaveTimeLimit = SD::Utils::Max<SD::INT>(std::atoi(newSettings.at(6).ToCString()), 30);
	StartingWave = SD::Utils::Clamp<SD::INT>(std::atoi(newSettings.at(7).ToCString()), 1, MaxWave);
}

void Extinction::InitializeGame ()
{
	Super::InitializeGame();

	WaveTimeLimitComp = TimeLimitComponent::CreateObject();
	if (AddComponent(WaveTimeLimitComp))
	{
		WaveTimeLimitComp->OnExpired = SDFUNCTION(this, Extinction, HandleWaveLimitExpired, void);
	}
	else
	{
		WaveTimeLimitComp = nullptr;
	}

	WaveIntervalTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(WaveIntervalTick))
	{
		WaveIntervalTick->SetTickHandler(SDFUNCTION_1PARAM(this, Extinction, HandleWaveInterval, void, SD::FLOAT));
		WaveIntervalTick->SetTickInterval(1.f);
	}
	else
	{
		WaveIntervalTick = nullptr;
	}

	SpawnDroneTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(SpawnDroneTick))
	{
		SpawnDroneTick->SetTickInterval(DroneSpawnInterval);
		SpawnDroneTick->SetTicking(false);
		SpawnDroneTick->SetTickHandler(SDFUNCTION_1PARAM(this, Extinction, HandleSpawnDrone, void, SD::FLOAT));
	}
	else
	{
		SpawnDroneTick = nullptr;
	}

	PurgeDroneTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(PurgeDroneTick))
	{
		PurgeDroneTick->SetTickInterval(1.f);
		PurgeDroneTick->SetTicking(false);
		PurgeDroneTick->SetTickHandler(SDFUNCTION_1PARAM(this, Extinction, HandlePurgeDrones, void, SD::FLOAT));
	}
	else
	{
		PurgeDroneTick = nullptr;
	}

	SD::SceneTransformComponent* spriteTransform = SD::SceneTransformComponent::CreateObject();
	if (AddComponent(spriteTransform))
	{
		spriteTransform->SetTranslation(SD::Vector3::ZeroVector);
		spriteTransform->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_DRONES;

		DroneSprites = SD::InstancedSpriteComponent::CreateObject();
		if (spriteTransform->AddComponent(DroneSprites))
		{
			SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
			CHECK(localTexturePool != nullptr)

			DroneSprites->SetSpriteTexture(localTexturePool->FindTexture(TXT("DroneDestroyer.Enemies.Enemies_V2")));
			DroneSprites->SetNumSubDivisions(6, 1);

			DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(DroneSprites);
		}
		else
		{
			DroneSprites = nullptr;
		}
	}

	Wave = (StartingWave - 1); //Set one below starting wave since NextWave is going to increment this value.
	NextWave();
}

void Extinction::AddComponentsToHud (Hud* playerHud)
{
	Super::AddComponentsToHud(playerHud);

	if (playerHud != nullptr)
	{
		if (WaveTimeLimitComp != nullptr)
		{
			TimeLimitHudComponent* timeHud = TimeLimitHudComponent::CreateObject();
			if (playerHud->AddComponent(timeHud))
			{
				timeHud->BindClockTo(WaveTimeLimitComp);
			}
		}

		GameHud = HudExtinction::CreateObject();
		if (playerHud->AddComponent(GameHud))
		{
			//Place above the health
			GameHud->SetPosition(SD::Vector2(0.f, 0.f));
			GameHud->SetSize(SD::Vector2(1.f, 1.f));

			//Update the hud to reflect the game state
			for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
			{
				if (!soul->GetIsBot())
				{
					GameHud->UpdateLabels(this, soul);
					break;
				}
			}
		}
		else
		{
			GameHud = nullptr;
		}
	}
}

void Extinction::ScoreKill (Character* killed, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg)
{
	Super::ScoreKill(killed, killer, killedBy, deathMsg);

	if (killed != nullptr && killed->GetSoul() != nullptr)
	{
		//A player was killed. Check if this is the end of the game.
		if (killed->GetSoul()->GetLivesRemaining() <= 0)
		{
			//That was their last life. Eliminate them
			killed->GetSoul()->SetIsEliminated(true);
			killed->GetSoul()->DecrementLivesCount();
			HudMessage::CreateAndRegisterMsgToHud(Msg_Eliminated::SStaticClass(), killed->GetSoul()->GetPlayerName());

			if (BotDialect != nullptr && !killed->IsHuman())
			{
				BotDialect->SayEliminationMessage(killed->GetSoul());
			}
		}
		else
		{
			killed->GetSoul()->DecrementLivesCount();
		}

		SD::Vector3 camLocation = (killed != nullptr) ? killed->GetAbsTranslation() : SD::Vector3::ZeroVector;
		CheckLossCondition(camLocation);
		UpdateScores();
	}

	PlayerSoul* humanSoul = nullptr;
	if (killer != nullptr && !killer->GetIsBot())
	{
		humanSoul = killer;
	}

	if (GameHud != nullptr)
	{
		GameHud->UpdateLabels(this, humanSoul);
	}
}

bool Extinction::AllowDeathMessageFor (Character* killed) const
{
	if (!Super::AllowDeathMessageFor(killed))
	{
		return false;
	}

	return (dynamic_cast<Drone*>(killed) == nullptr);
}

void Extinction::Destroyed ()
{
	//Remove all drones 
	for (size_t i = 0; i < Drones.size(); ++i)
	{
		Drones.at(i)->Destroy();
	}
	SD::ContainerUtils::Empty(OUT Drones);

	Super::Destroyed();
}

const Scoreboard* Extinction::GetScoreboardClass () const
{
	return dynamic_cast<const Scoreboard_Extinction*>(Scoreboard_Extinction::SStaticClass()->GetDefaultObject());
}

void Extinction::InitializeBotDialect ()
{
	Super::InitializeBotDialect();

	CHECK(BotDialect != nullptr)

	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile((SD::Directory::CONFIG_DIRECTORY).ToString() + TXT("BotDialect.ini"), false))
	{
		config->Destroy();
		return;
	}

	std::vector<SD::DString> msgs;
	config->GetArrayValues(TXT("BotMessages"), TXT("InvasionBotDeathMsg"), OUT msgs);
	BotDialect->AddEliminationMsgs(msgs);
	config->Destroy();
}

bool Extinction::CanCreateBody (PlayerSoul* requester)
{
	if (!Super::CanCreateBody(requester))
	{
		return false;
	}

	return (requester != nullptr && !requester->GetIsEliminated());
}

bool Extinction::FindSpawnLocation (Character* body, PlayerSoul* soul, SD::Vector3& outLocation, SD::Rotator& outRotation) const
{
	if (Drone* drone = dynamic_cast<Drone*>(body))
	{
		//Permit drones to spawn everywhere
		if (SD::ContainerUtils::IsEmpty(DroneSpawnLocations))
		{
			DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
			CHECK(localDroneEngine != nullptr && localDroneEngine->GetEnvironment() != nullptr)

			const std::vector<SD::Entity*> arenaEntities = localDroneEngine->GetEnvironment()->ReadArenaEntities();
			for (SD::Entity* entity : arenaEntities)
			{
				NavigationComponent* navComp = dynamic_cast<NavigationComponent*>(entity->FindSubComponent(NavigationComponent::SStaticClass(), false));
				if (navComp != nullptr)
				{
					DroneSpawnLocations.push_back(navComp);
				}
			}
		}

		if (!SD::ContainerUtils::IsEmpty(DroneSpawnLocations))
		{
			NavigationComponent* chosenNode = DroneSpawnLocations.at(SD::RandomUtils::Rand(SD::INT(DroneSpawnLocations.size())).ToUnsignedInt());
			CHECK(chosenNode->GetSceneTransform() != nullptr)
			outLocation = chosenNode->GetSceneTransform()->ReadTranslation();
			outRotation = chosenNode->GetSceneTransform()->ReadRotation();
			return true;
		}
	}

	return Super::FindSpawnLocation(body, soul, outLocation, outRotation);
}

void Extinction::SortScores ()
{
	Super::SortScores();

	std::sort(RankedPlayers.begin(), RankedPlayers.end(), [](const SD::DPointer<PlayerSoul>& a, const SD::DPointer<PlayerSoul>& b)
	{
		return (a->GetNumKills() > b->GetNumKills());
	});
}

void Extinction::RemoveDrone (Drone* oldDrone)
{
	if (oldDrone != nullptr)
	{
		//Mark a particular index that a slot has opened up.
		EmptyDroneSpriteIndices.push_back(oldDrone->GetSpriteIdx());

		//Find the vertices associated with that drone, and set the texture coordinates to the same to make it invisible.
		if (DroneSprites != nullptr && (oldDrone->GetSpriteIdx() * 6) + 5 < DroneSprites->EditVertices().getVertexCount())
		{
			sf::VertexArray& sfVertices = DroneSprites->EditVertices();
			for (size_t i = (oldDrone->GetSpriteIdx() * 6); i < (oldDrone->GetSpriteIdx() + 1) * 6; ++i)
			{
				DroneSprites->EditVertices()[i].texCoords.x = 0.f;
				DroneSprites->EditVertices()[i].texCoords.y = 0.f;
			}
		}

		SD::ContainerUtils::RemoveItem(OUT Drones, oldDrone);
		if (SD::ContainerUtils::IsEmpty(Drones) && NumDronesRemaining <= 0)
		{
			NextWave(); //<--- This also updates the game hud
		}
		else if (GameHud != nullptr)
		{
			GameHud->UpdateLabels(this, nullptr);
		}
	}
}

void Extinction::NextWave ()
{
	if (Wave == MaxWave)
	{
		//Players have beaten the last wave.
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		SD::Vector3 camLocation(SD::Vector3::ZeroVector);
		//Snap the camera to look at the high scorer that happens to be alive.
		for (SD::DPointer<PlayerSoul>& soul : RankedPlayers)
		{
			if (soul->IsAlive())
			{
				camLocation = soul->GetBody()->ReadAbsTranslation();
				break; //RankedPlayers are sorted by high score. Break as soon as we find a living player.
			}
		}

		ExecuteVictorySequence(translator->TranslateText(TXT("Victory"), TRANSLATION_FILE, TXT("Extinction")), camLocation);
		return;
	}

	/* Iterate through all players, refill their lives and respawn the dead. */
	PlayerSoul* humanSoul = nullptr;
	for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
	{
		soul->SetIsEliminated(false);
		soul->SetLivesRemaining(NumLivesPerWave - 1);

		if (!soul->GetIsBot())
		{
			humanSoul = soul;
		}
	}

	//Increment the wave and start the sequence
	++Wave;
	LoadTemplates();
	NumDronesRemaining = BaseNumDrones + (NumExtraDronesPerWave * (Wave-1));
	if (WaveTimeLimitComp != nullptr)
	{
		WaveTimeLimitComp->SetTimeLimit((WaveTimeLimit + WaveInterval).ToFLOAT());
	}

	if (WaveIntervalTick != nullptr)
	{
		WaveIntervalTick->SetTicking(true);
		WaveIntervalTimeRemaining = WaveInterval;
	}
	
	if (SpawnDroneTick != nullptr)
	{
		SpawnDroneTick->SetTicking(false);
	}

	if (PurgeDroneTick != nullptr)
	{
		PurgeDroneTick->SetTicking(false);
	}

	if (GameHud != nullptr)
	{
		GameHud->UpdateLabels(this, humanSoul);
	}
}

void Extinction::LoadTemplates ()
{
	SD::ContainerUtils::Empty(OUT DroneTemplates);
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(SD::Directory::CONFIG_DIRECTORY.ReadDirectoryPath() + TXT("Extinction.ini"), false))
	{
		config->Destroy();
		return;
	}

	std::vector<SD::DString> droneTemplates;
	config->GetArrayValues(TXT("DroneTemplates"), TXT("Wave_") + Wave.ToString(), OUT droneTemplates);

	if (SD::ContainerUtils::IsEmpty(droneTemplates))
	{
		//Wave is beyond the configured values. Refer to the infinite wave.
		SD::DString infWave = config->GetPropertyText(TXT("DroneTemplates"), TXT("InfiniteWave"));
		config->GetArrayValues(TXT("DroneTemplates"), infWave, OUT droneTemplates);
	}

	for (size_t i = 0; i < droneTemplates.size(); ++i)
	{
		SDroneConfiguration newTemplate;
		newTemplate.SpriteIdx = config->GetProperty<SD::INT>(droneTemplates.at(i), TXT("SpriteIdx")).ToInt32();
		SD::DString difficultyText = config->GetPropertyText(droneTemplates.at(i), TXT("SkillLevel"));
		newTemplate.SkillLevel = GameDifficulty;
		if (!difficultyText.IsEmpty())
		{
			if (difficultyText.Compare(TXT("Pacifist"), SD::DString::CC_IgnoreCase) == 0)
			{
				newTemplate.SkillLevel = EGameDifficulty::Pacifist;
			}
			else if (difficultyText.Compare(TXT("Novice"), SD::DString::CC_IgnoreCase) == 0)
			{
				newTemplate.SkillLevel = EGameDifficulty::Novice;
			}
			else if (difficultyText.Compare(TXT("Easy"), SD::DString::CC_IgnoreCase) == 0)
			{
				newTemplate.SkillLevel = EGameDifficulty::Easy;
			}
			else if (difficultyText.Compare(TXT("Medium"), SD::DString::CC_IgnoreCase) == 0)
			{
				newTemplate.SkillLevel = EGameDifficulty::Medium;
			}
			else if (difficultyText.Compare(TXT("Hard"), SD::DString::CC_IgnoreCase) == 0)
			{
				newTemplate.SkillLevel = EGameDifficulty::Hard;
			}
		}

		newTemplate.MaxHealth = config->GetProperty<SD::INT>(droneTemplates.at(i), TXT("MaxHealth"));
		newTemplate.VelocityMagnitude = config->GetProperty<SD::FLOAT>(droneTemplates.at(i), TXT("MovementSpeed"));
		newTemplate.WeaponClass = DroneWeapon_PulseRifle::SStaticClass();
		SD::DString weaponText = config->GetPropertyText(droneTemplates.at(i), TXT("Weapon"));
		if (!weaponText.IsEmpty())
		{
			//No need to check for pulse rifle since it defaults to that.
			if (weaponText.Compare(TXT("Plasma"), SD::DString::CC_IgnoreCase) == 0)
			{
				newTemplate.WeaponClass = DroneWeapon_Plasma::SStaticClass();
			}
		}

		DroneTemplates.push_back(newTemplate);
	}

	config->Destroy();
}

void Extinction::SpawnDrone ()
{
	Drone* newDrone = Drone::CreateObject();
	CHECK(newDrone != nullptr)

	SD::Vector3 spawnLocation;
	SD::Rotator spawnRotation;
	if (FindSpawnLocation(newDrone, nullptr, OUT spawnLocation, OUT spawnRotation))
	{
		newDrone->SetTranslation(spawnLocation);
		newDrone->SetRotation(spawnRotation);
	}

	size_t chosenIdx = UINT_MAX;
	if (!SD::ContainerUtils::IsEmpty(DroneTemplates))
	{
		chosenIdx = SD::RandomUtils::Rand(SD::INT(DroneTemplates.size())).ToUnsignedInt();	
	}

	int spriteIdx = (chosenIdx < UINT_MAX) ? DroneTemplates.at(chosenIdx).SpriteIdx : 0;
	AddDroneSprite(newDrone, spriteIdx);
	newDrone->SetDroneSprite(DroneSprites);
	newDrone->InitializeBotComponent();
	newDrone->InitializePhysics();

	if (chosenIdx < UINT_MAX)
	{
		newDrone->AdjustDroneStrength(DroneTemplates.at(chosenIdx).SkillLevel, DroneTemplates.at(chosenIdx).MaxHealth, DroneTemplates.at(chosenIdx).VelocityMagnitude, DroneTemplates.at(chosenIdx).WeaponClass);
	}

	Drones.push_back(newDrone);
	--NumDronesRemaining;
	if (NumDronesRemaining <= 0 && SpawnDroneTick != nullptr)
	{
		SpawnDroneTick->SetTicking(false);
	}

	if (GameHud != nullptr)
	{
		GameHud->UpdateLabels(this, nullptr);
	}
}

void Extinction::AddDroneSprite (Drone* newDrone, SD::INT subDivisionIdx)
{
	if (DroneSprites != nullptr && newDrone != nullptr)
	{
		SD::InstancedSpriteComponent::SSpriteInstance spriteInstance;

		spriteInstance.SubDivisionIdxX = subDivisionIdx.ToInt32();
		spriteInstance.SubDivisionIdxY = 1;

		//The Drone will set the size, position, and rotation every frame.

		size_t spriteIdx;
		if (SD::ContainerUtils::IsEmpty(EmptyDroneSpriteIndices))
		{
			DroneSprites->EditSpriteInstances().push_back(spriteInstance);
			spriteIdx = DroneSprites->ReadSpriteInstances().size() - 1;
		}
		else
		{
			spriteIdx = SD::ContainerUtils::GetLast(EmptyDroneSpriteIndices);
			DroneSprites->EditSpriteInstances().at(spriteIdx) = spriteInstance;
			EmptyDroneSpriteIndices.pop_back();
		}

		//Generate the six sf vertices for this one drone. This also sets the texture coordinates for those vertices.
		DroneSprites->RegenerateVertices({spriteIdx});
		newDrone->SetSpriteIdx(spriteIdx);
	}
}

void Extinction::CheckLossCondition (const SD::Vector3& camLocation)
{
	for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
	{
		if (!soul->GetIsEliminated() && !soul->GetIsSpectator())
		{
			//There's a player alive.
			return;
		}
	}

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	ExecuteVictorySequence(translator->TranslateText(TXT("Defeat"), TRANSLATION_FILE, TXT("Extinction")), camLocation);
}

void Extinction::HandleWaveInterval (SD::FLOAT deltaSec)
{
	WaveIntervalTimeRemaining--;
	if (WaveIntervalTimeRemaining <= 0)
	{
		//Start spawning drones
		if (WaveIntervalTick != nullptr)
		{
			WaveIntervalTick->SetTicking(false);
		}

		if (SpawnDroneTick != nullptr)
		{
			SpawnDroneTick->SetTicking(true);
		}

		return;
	}
	else if (WaveIntervalTimeRemaining <= 10)
	{
		//Display countdown warning on hud
		HudMessage::CreateAndRegisterMsgToHud(Msg_Countdown::SStaticClass(), WaveIntervalTimeRemaining);
	}
}

void Extinction::HandleSpawnDrone (SD::FLOAT deltaSec)
{
	if (SD::INT(Drones.size()) >= MaxDronesAtOnce)
	{
		return;
	}

	if (NumDronesRemaining <= 0)
	{
		return;
	}

	SpawnDrone();
}

void Extinction::HandleWaveLimitExpired ()
{
	NumDronesRemaining = 0;
	if (SpawnDroneTick != nullptr)
	{
		SpawnDroneTick->SetTicking(false);
	}

	if (PurgeDroneTick != nullptr)
	{
		PurgeDroneTick->SetTicking(true);
	}
}

void Extinction::HandlePurgeDrones (SD::FLOAT deltaSec)
{
	for (SD::INT i = 0; i < DronePurgeLimit && !SD::ContainerUtils::IsEmpty(Drones); ++i)
	{
		//Drones will remove themselves from the list when they're killed.
		//Remove the tail end since it's faster to erase a vector from the end than the front.
		SD::ContainerUtils::GetLast(Drones)->Kill(999, nullptr, nullptr, TXT("SuddenDeath"));
	}
}
DD_END