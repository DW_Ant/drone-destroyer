/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlayerSoul.cpp
=====================================================================
*/

#include "BotSoulComponent.h"
#include "Character.h"
#include "DroneDestroyerEngineComponent.h"
#include "GameRules.h"
#include "HealthComponent.h"
#include "Hud.h"
#include "MovementComponent.h"
#include "NavigationComponent.h"
#include "PauseMenu.h"
#include "PlayerSoul.h"
#include "Scoreboard.h"
#include "SoundEngineComponent.h"
#include "SoundMessenger.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, PlayerSoul, SD, Entity)

const SD::INT PlayerSoul::INPUT_PRIORITY(500); //Lower than Menu and Character input
const SD::INT PlayerSoul::PAUSED_INPUT_PRIORITY(800); //Higher than character, lower than Gui

void PlayerSoul::InitProps ()
{
	Super::InitProps();

	RespawnTime = 2.f;

	PlayerName = TXT("Player");
	TeamNumber = 0;
	NumKills = 0;
	NumDeaths = 0;
	LivesRemaining = 0;
	IsEliminated = false;
	IsSpectator = false;
	IsBot = true;
	NextSoul = nullptr;
	SpectatedSoul = nullptr;

	LastKilledTime = -RespawnTime; //Allow instant respawn.
	DefaultCamPanSpeed = 500.f; //5 meters per second
	DefaultCamZoom = 1.f;
}

void PlayerSoul::BeginObject ()
{
	Super::BeginObject();

	DroneDestroyerEngineComponent* droneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(droneEngine != nullptr)

	GameRules* curGame = droneEngine->GetGame();
	if (curGame == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Instantiated a PlayerSoul without an active game. This soul will not be registered to a linked list."));
		return;
	}

	curGame->RegisterNewSoul(this);
}

void PlayerSoul::Destroyed ()
{
	DroneDestroyerEngineComponent* droneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(droneEngine != nullptr)

	if (Body != nullptr)
	{
		Body->Destroy();
	}

	GameRules* curGame = droneEngine->GetGame();
	if (curGame != nullptr)
	{
		curGame->RemoveSoul(this);
	}

	if (PauseUi != nullptr)
	{
		PauseUi->Destroy();
	}

	Super::Destroyed();
}

bool PlayerSoul::IsAlive () const
{
	return (Body != nullptr && Body->IsAlive());
}

bool PlayerSoul::RequestBody ()
{
	if (IsAlive() || IsSpectator)
	{
		//Can only control one body
		return false;
	}

	DroneDestroyerEngineComponent* droneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(droneEngine != nullptr)

	SD::Engine* engine = droneEngine->GetOwningEngine();
	CHECK(engine != nullptr)
	if (engine->GetElapsedTime() - LastKilledTime < RespawnTime)
	{
		//Died too recently to spawn a new body
		return false;
	}

	GameRules* game = droneEngine->GetGame();
	if (game == nullptr)
	{
		return false;
	}

	Body = game->CreateCharacter(this);
	if (Body != nullptr)
	{
		Body->SetSoul(this);
		if (OnPossessBody.IsBounded())
		{
			OnPossessBody(Body.Get());
		}

		if (SpectatedSoul != nullptr)
		{
			//No longer spectating something if we're actively playing now.
			SpectatedSoul->OnPossessBody.ClearFunction();
			SpectatedSoul = nullptr;
		}

		if (!IsBot)
		{
			//Snap view to watch this character
			AttachCameraTo(Body.Get());
			Body->InitializeInputComponent();

			//Add a component to this body so that this player can control that character.
			MovementComponent* characterMovement = Body->GetMovement();
			if (characterMovement != nullptr)
			{
				characterMovement->InitializeInputComponent();
			}
		}
	}

	return (Body != nullptr);
}

void PlayerSoul::InitializeInputComponent ()
{
	IsBot = false;

	SD::InputComponent* input = SD::InputComponent::CreateObject();
	if (AddComponent(input))
	{
		input->SetInputPriority(INPUT_PRIORITY);
		input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, PlayerSoul, HandleInput, bool, const sf::Event&);
		input->MouseClickDelegate = SDFUNCTION_3PARAM(this, PlayerSoul, HandleMouseClick, bool, SD::MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
		input->MouseWheelScrollDelegate = SDFUNCTION_2PARAM(this, PlayerSoul, HandleMouseWheel, bool, SD::MousePointer*, const sf::Event::MouseWheelScrollEvent&);
	}

	SD::InputComponent* pauseInput = SD::InputComponent::CreateObject();
	if (AddComponent(pauseInput))
	{
		pauseInput->SetInputPriority(PAUSED_INPUT_PRIORITY);
		pauseInput->MouseClickDelegate = SDFUNCTION_3PARAM(this, PlayerSoul, HandlePausedMouseClick, bool, SD::MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
	}

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetMainCamera() != nullptr && localDroneEngine->GetGame() != nullptr)

	CamMovementComp = MovementComponent::CreateObject();
	if (localDroneEngine->GetMainCamera()->AddComponent(CamMovementComp.Get()))
	{
		CamMovementComp->SetLockedRotation(true);
		CamMovementComp->SetActive(true);
		CamMovementComp->SetVelocityMagnitude(DefaultCamPanSpeed);
		CamMovementComp->InitializeInputComponent();
		DefaultCamZoom = localDroneEngine->GetMainCamera()->Zoom;
	}

	if (!IsSpectator && localDroneEngine->GetGame()->GetForceRespawn())
	{
		SD::TickComponent* forceRespawnTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
		if (AddComponent(forceRespawnTick))
		{
			forceRespawnTick->SetTickInterval(2.5f);
			forceRespawnTick->SetTickHandler(SDFUNCTION_1PARAM(this, PlayerSoul, HandleForceRespawnTick, void, SD::FLOAT));
		}
	}
}

void PlayerSoul::InitializeBotComponent ()
{
	BotSoulComponent* bot = BotSoulComponent::CreateObject();
	AddComponent(bot);
}

bool PlayerSoul::CanHarm (HealthComponent* health) const
{
	if (TeamNumber < 0)
	{
		return true;
	}

	if (health != nullptr)
	{
		if (Character* otherBody = dynamic_cast<Character*>(health->GetOwner()))
		{
			if (PlayerSoul* victimSoul = otherBody->GetSoul())
			{
				//Damage anyone in a different team or damage yourself
				return (victimSoul->TeamNumber != TeamNumber || victimSoul == this);
			}
		}
	}

	return true;
}

void PlayerSoul::IncrementKillCount ()
{
	++NumKills;
}

void PlayerSoul::DecrementKillCount ()
{
	--NumKills;
}

void PlayerSoul::IncrementDeathCount ()
{
	++NumDeaths;

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	LastKilledTime = localEngine->GetElapsedTime();
}

void PlayerSoul::DecrementLivesCount ()
{
	LivesRemaining--;
}

void PlayerSoul::DetachFromBody ()
{
	Body = nullptr;
}

void PlayerSoul::SetPlayerName (const SD::DString& newPlayerName)
{
	PlayerName = newPlayerName;

#ifdef DEBUG_MODE
	DebugName = PlayerName;
#endif
}

void PlayerSoul::SetIsSpectator (bool newIsSpectator)
{
	IsSpectator = newIsSpectator;

	if (IsSpectator && Body != nullptr)
	{
		Body->Kill(1, this, nullptr, TXT("Unknown"));
	}

	if (IsSpectator)
	{
		//Notify the game that there is a spectator
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)
		localDroneEngine->GetGame()->AddSpectator(this);
	}
}

void PlayerSoul::ViewNextPlayer ()
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)

	if (SpectatedSoul != nullptr)
	{
		SpectatedSoul->OnPossessBody.ClearFunction();
	}

	PlayerSoul* newSoul = (SpectatedSoul != nullptr && SpectatedSoul->NextSoul != nullptr) ? SpectatedSoul->NextSoul : localDroneEngine->GetGame()->GetFirstPlayer();
	SD::INT numBots = localDroneEngine->GetGame()->GetNumBots(); //Used to detect a full cycle (eg: if there aren't any more bots to cycle through
	for (SD::INT counter = 0; counter < numBots; ++counter)
	{
		if (newSoul != this && !newSoul->GetIsEliminated() && !newSoul->GetIsSpectator())
		{
			SpectatedSoul = newSoul;
			break;
		}

		newSoul = newSoul->NextSoul;
		if (newSoul == nullptr)
		{
			//Cycle back to the beginning
			newSoul = localDroneEngine->GetGame()->GetFirstPlayer();
		}
	}

	if (SpectatedSoul != nullptr && SpectatedSoul->GetBody() != nullptr)
	{
		SpectatedSoul->OnPossessBody = SDFUNCTION_1PARAM(this, PlayerSoul, HandleSoulPossessBody, void, Character*);
		AttachCameraTo(SpectatedSoul->GetBody());
	}
}

void PlayerSoul::AttachCameraTo (Character* target)
{
	DroneDestroyerEngineComponent* droneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(droneEngine != nullptr)
	if (droneEngine->GetMainCamera() != nullptr)
	{
		droneEngine->GetMainCamera()->SetRelativeTo(target->GetAttachedCameraTransform());
		droneEngine->GetMainCamera()->SetTranslation(SD::Vector3(0.f, 0.f, DroneDestroyerEngineComponent::DRAW_ORDER_CAMERA));

		//Reset zoom back to its defaults if controlling a character
		if (target == Body)
		{
			if (const SD::TopDownCamera* cdo = dynamic_cast<const SD::TopDownCamera*>(droneEngine->GetMainCamera()->GetDefaultObject()))
			{
				droneEngine->GetMainCamera()->Zoom = cdo->Zoom;
				if (CamMovementComp != nullptr)
				{
					CamMovementComp->SetVelocityMagnitude(DefaultCamPanSpeed);
				}
			}
		}

		if (CamMovementComp != nullptr)
		{
			CamMovementComp->SetActive(false);
		}
	}

	Hud* hud = droneEngine->GetPlayerHud();
	CHECK(hud != nullptr)
	bool isActivePlayer = (Body.Get() == target);
	hud->SetHudMode(isActivePlayer);
	if (!isActivePlayer && target->GetSoul() != nullptr)
	{
		hud->SetViewedPlayer(target->GetSoul()->GetPlayerName());
	}

}

void PlayerSoul::BeginFreeCamMode ()
{
	if (SpectatedSoul != nullptr)
	{
		SpectatedSoul->OnPossessBody.ClearFunction();
		SpectatedSoul = nullptr;
	}

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	if (SD::SceneCamera* cam = localDroneEngine->GetMainCamera())
	{
		//Free camera mode
		cam->SetRelativeTo(nullptr);
		cam->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_CAMERA;
	}

	if (CamMovementComp != nullptr)
	{
		CamMovementComp->SetActive(true);
	}

	Hud* playerHud = localDroneEngine->GetPlayerHud();
	CHECK(playerHud != nullptr)
	playerHud->SetViewedPlayer(SD::DString::EmptyString);
}

void PlayerSoul::HandleForceRespawnTick (SD::FLOAT deltaSec)
{
	RequestBody();
}

bool PlayerSoul::HandleInput (const sf::Event& evnt)
{
	bool consumeEvent = false;
	if (evnt.type == sf::Event::EventType::KeyPressed || evnt.type == sf::Event::EventType::KeyReleased)
	{
		switch(evnt.key.code)
		{
			case(sf::Keyboard::F1):
			{
				//Toggle scoreboard
				consumeEvent = true;
				if (evnt.type == sf::Event::EventType::KeyPressed)
				{
					DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
					CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)

					bool scoreboardVisible = false;
					if (Scoreboard* score = localDroneEngine->GetGame()->GetScoreboardInstance())
					{
						scoreboardVisible = !score->IsVisible();
						score->SetVisibility(scoreboardVisible);
					}

					//Set the hud's visibility to the opposite of the scoreboard
					if (Hud* playerHud = localDroneEngine->GetPlayerHud())
					{
						playerHud->SetVisibility(!scoreboardVisible);
					}
				}

				break;
			}
			case(sf::Keyboard::Escape):
			{
				//Pause the game and open the escape menu
				consumeEvent = true;

				if (evnt.type == sf::Event::EventType::KeyReleased)
				{
					DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
					CHECK(localDroneEngine != nullptr)
					localDroneEngine->SetPausedGame(true);

					if (SoundMessenger* msg = SoundMessenger::GetMessenger())
					{
						msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
					}

					PauseUi = PauseMenu::CreateObject();
					PauseUi->RegisterToMainWindow(true, true, PAUSED_INPUT_PRIORITY + 5);
				}

				break;
			}
			case(sf::Keyboard::Pause):
			{
				//Toggle pause
				consumeEvent = true;

				if (evnt.type == sf::Event::EventType::KeyReleased && PauseUi == nullptr)
				{
					DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
					CHECK(localDroneEngine != nullptr)

					if (localDroneEngine->GetGame() != nullptr && !localDroneEngine->GetGame()->GetHasEnded())
					{
						if (SoundMessenger* msg = SoundMessenger::GetMessenger())
						{
							msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
						}

						localDroneEngine->TogglePause();
					}
				}

				break;
			}
			case(sf::Keyboard::P):
			{
				//Toggle Pathing
				consumeEvent = true;
#ifdef DEBUG_MODE
				if (evnt.type == sf::Event::EventType::KeyPressed)
				{
					NavigationComponent::DebugLinesVisible = !NavigationComponent::DebugLinesVisible;
					DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
					CHECK(localDroneEngine != nullptr)
					if (localDroneEngine->GetEnvironment() != nullptr)
					{
						for (NavigationComponent* nav = localDroneEngine->GetEnvironment()->GetFirstNavigation(); nav != nullptr; nav = nav->GetNextNavigation())
						{
							nav->SetVisibility(NavigationComponent::DebugLinesVisible);
						}
					}
				}
#endif
				break;
			}
		}
	}

	return consumeEvent;
}

bool PlayerSoul::HandleMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	bool consumeEvent = false;

	if (sfmlEvent.button == sf::Mouse::Button::Left)
	{
		consumeEvent = true;
		if (eventType == sf::Event::EventType::MouseButtonPressed)
		{
			if (!RequestBody())
			{
				DroneDestroyerEngineComponent* droneEngine = DroneDestroyerEngineComponent::Find();
				CHECK(droneEngine != nullptr)

				SD::Engine* engine = droneEngine->GetOwningEngine();
				CHECK(engine != nullptr)
				if (engine->GetElapsedTime() - LastKilledTime >= RespawnTime)
				{
					//Only view the next player to have the player watch their corpse for a few seconds before switching.
					ViewNextPlayer();
				}
			}
		}
	}
	else if (sfmlEvent.button == sf::Mouse::Button::Right && (GetIsSpectator() || GetIsEliminated()))
	{
		consumeEvent = true;
		if (eventType == sf::Event::EventType::MouseButtonPressed)
		{
			BeginFreeCamMode();
		}
	}

	return consumeEvent;
}

bool PlayerSoul::HandlePausedMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	bool consumeEvent = false;
	if (sfmlEvent.button == sf::Mouse::Button::Left)
	{
		DroneDestroyerEngineComponent* droneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(droneEngine != nullptr)
		if (droneEngine->IsPausingGame())
		{
			//Consume event even if the game has ended
			consumeEvent = true;

			if (eventType == sf::Event::EventType::MouseButtonPressed && PauseUi == nullptr && droneEngine->GetGame() != nullptr && !droneEngine->GetGame()->GetHasEnded())
			{
				if (SoundMessenger* msg = SoundMessenger::GetMessenger())
				{
					msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
				}

				droneEngine->SetPausedGame(false);
			}
		}
	}

	return consumeEvent;
}

bool PlayerSoul::HandleMouseWheel (SD::MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& wheelEvent)
{
	bool consumeEvent = false;

	if (wheelEvent.wheel == sf::Mouse::VerticalWheel && wheelEvent.delta != 0.f)
	{
		DroneDestroyerEngineComponent* droneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(droneEngine != nullptr)

		if (SD::TopDownCamera* camera = droneEngine->GetMainCamera())
		{
			camera->Zoom += wheelEvent.delta * 0.1f * camera->Zoom;
			camera->Zoom = SD::Utils::Clamp<SD::FLOAT>(camera->Zoom, 0.002f, 0.1f);

			if (CamMovementComp != nullptr)
			{
				CamMovementComp->SetVelocityMagnitude(DefaultCamPanSpeed / (camera->Zoom / DefaultCamZoom));
			}
			
			consumeEvent = true;
		}
	}

	return consumeEvent;
}

void PlayerSoul::HandleSoulPossessBody (Character* newBody)
{
	AttachCameraTo(newBody);
}

DD_END