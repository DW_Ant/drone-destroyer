/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SoundPlayer.cpp
=====================================================================
*/

#include "MusicPlayer.h"
#include "Sound.h"
#include "SoundEngineComponent.h"
#include "SoundPlayer.h"
#include "SoundPool.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, SoundPlayer, SD, Entity)

const size_t SoundPlayer::MAX_SOUND_INSTANCES(128);

void SoundPlayer::InitProps ()
{
	Super::InitProps();

	GameVolumeMultiplier = 1.f;
	MusicVolumeMultiplier = 1.f;

	Receiver = nullptr;
	VolLevelReceiver = nullptr;
	NextSoundIndex = 0;
}

void SoundPlayer::BeginObject ()
{
	Super::BeginObject();

	Sounds.resize(MAX_SOUND_INSTANCES);

	SD::TickComponent* tick = SD::TickComponent::CreateObject(TICK_GROUP_DD_NO_PAUSE);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, SoundPlayer, HandleTick, void, SD::FLOAT));
	}
}

void SoundPlayer::SetReceiver (SD::ThreadedComponent* newReceiver)
{
	CHECK(Receiver == nullptr) //Don't replace the old one since that one is already connected to a threaded component.
	Receiver = newReceiver;
}

void SoundPlayer::SetVolLevelReceiver (SD::ThreadedComponent* newVolLevelReceiver)
{
	CHECK(VolLevelReceiver == nullptr)
	VolLevelReceiver = newVolLevelReceiver;
}

void SoundPlayer::ProcessNewData ()
{
	SoundPool* localSoundPool = SoundPool::FindSoundPool();
	CHECK(localSoundPool != nullptr && Receiver != nullptr)

	SD::DataBuffer& buffer = Receiver->ReadExternalData();
	std::vector<std::pair<SD::DString, SD::FLOAT>> soundsToPlay;
	while (!buffer.IsReaderAtEnd())
	{
		SD::BOOL isMusic;
		buffer >> isMusic;

		SD::DString newSound;
		buffer >> newSound;

		SD::FLOAT soundVol;
		buffer >> soundVol;

		if (isMusic)
		{
			//Normally we would move this outside the loop, but it's HIGHLY unlikely to have multiple music requests within the same buffer.
			SoundEngineComponent* localSoundEngine = SoundEngineComponent::Find();
			CHECK(localSoundEngine != nullptr && localSoundEngine->GetMusic() != nullptr)
			localSoundEngine->GetMusic()->PlayMusic(newSound);
		}
		else if (GameVolumeMultiplier > 0.f)
		{
			soundsToPlay.push_back(std::pair<SD::DString, SD::FLOAT>(newSound, soundVol));
		}
	}
	
	buffer.EmptyBuffer();

	for (size_t i = 0; i < soundsToPlay.size(); ++i)
	{
		Sound* sound = localSoundPool->FindSound(soundsToPlay.at(i).first, true);
		if (sound != nullptr && sound->GetBuffer() != nullptr)
		{
			if (Sounds.at(NextSoundIndex).getStatus() == sf::SoundSource::Playing)
			{
				Sounds.at(NextSoundIndex).stop();
			}

			Sounds.at(NextSoundIndex).setBuffer(*sound->GetBuffer());
			Sounds.at(NextSoundIndex).setVolume((soundsToPlay.at(i).second * GameVolumeMultiplier * 100.f).Value); //SFML ranges from 0-100. DroneDestroyer ranges from 0-1
			Sounds.at(NextSoundIndex).play();
			++NextSoundIndex;
			NextSoundIndex %= MAX_SOUND_INSTANCES;
		}
	}
}

void SoundPlayer::ProcessNewVolumeLevels ()
{
	CHECK(VolLevelReceiver != nullptr)

	SD::DataBuffer& externalData = VolLevelReceiver->ReadExternalData();

	while (!externalData.IsReaderAtEnd())
	{
		SD::BOOL isMusic;
		externalData >> isMusic;

		if (isMusic)
		{
			externalData >> MusicVolumeMultiplier;
			MusicVolumeMultiplier = SD::Utils::Clamp<SD::FLOAT>(MusicVolumeMultiplier, 0.f, 1.f);

			SoundEngineComponent* localSoundEngine = SoundEngineComponent::Find();
			CHECK(localSoundEngine != nullptr && localSoundEngine->GetMusic() != nullptr)
			localSoundEngine->GetMusic()->SetMusicVolume(MusicVolumeMultiplier);
		}
		else
		{
			externalData >> GameVolumeMultiplier;
			GameVolumeMultiplier = SD::Utils::Clamp<SD::FLOAT>(GameVolumeMultiplier, 0.f, 1.f);
		}
	}

	externalData.EmptyBuffer();
}

void SoundPlayer::HandleTick (SD::FLOAT deltaSec)
{
	if (VolLevelReceiver != nullptr && VolLevelReceiver->ReadExternalData().GetNumBytes() > 0)
	{
		ProcessNewVolumeLevels();
	}

	if (Receiver != nullptr && Receiver->ReadExternalData().GetNumBytes() > 0)
	{
		ProcessNewData();
	}
}
DD_END