/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HudExtinction.cpp
=====================================================================
*/

#include "Extinction.h"
#include "HudExtinction.h"
#include "PlayerSoul.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, HudExtinction, SD, GuiComponent)

void HudExtinction::InitProps ()
{
	Super::InitProps();

	SurvivorsLabel = nullptr;
	DronesLabel = nullptr;
	WaveLabel = nullptr;
	LivesRemainingLabel = nullptr;
	KillsLabel = nullptr;
}

void HudExtinction::BeginObject ()
{
	Super::BeginObject();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	WavePrefix = translator->TranslateText(TXT("Wave"), TRANSLATION_FILE, TXT("Hud"));
	LivesPrefix = translator->TranslateText(TXT("LivesRemaining"), TRANSLATION_FILE, TXT("Hud"));
	KillsPrefix = translator->TranslateText(TXT("Kills"), TRANSLATION_FILE, TXT("Hud"));
}

void HudExtinction::InitializeComponents ()
{
	Super::InitializeComponents();

	SD::TexturePool* texturePool = SD::TexturePool::FindTexturePool();
	CHECK(texturePool != nullptr)

	SD::FrameComponent* gameStatusFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(gameStatusFrame))
	{
		gameStatusFrame->SetPosition(SD::Vector2(0.f, 0.1f)); //Below the time limit component
		gameStatusFrame->SetSize(SD::Vector2(0.2f, 0.2f));
		gameStatusFrame->SetAnchorRight(0.f);
		gameStatusFrame->SetBorderThickness(0.f);
		gameStatusFrame->SetLockedFrame(true);
		if (gameStatusFrame->RenderComponent != nullptr)
		{
			gameStatusFrame->RenderComponent->ClearSpriteTexture();
			gameStatusFrame->RenderComponent->FillColor = SD::Color(172, 208, 120, 255);
		}

		SD::INT characterSize = 32;

		WaveLabel = SD::LabelComponent::CreateObject();
		if (gameStatusFrame->AddComponent(WaveLabel))
		{
			WaveLabel->SetAutoRefresh(false);
			WaveLabel->SetPosition(SD::Vector2(0.f, 0.f));
			WaveLabel->SetSize(SD::Vector2(1.f, 0.3f));
			WaveLabel->SetWrapText(false);
			WaveLabel->SetClampText(true);
			WaveLabel->SetCharacterSize(characterSize);
			WaveLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			WaveLabel->SetAutoRefresh(true);

			if (WaveLabel->GetRenderComponent() != nullptr)
			{
				WaveLabel->GetRenderComponent()->SetFontColor(GetNormalColor());
			}
		}
		else
		{
			WaveLabel = nullptr;
		}

		SD::PlanarTransformComponent* droneTransform = SD::PlanarTransformComponent::CreateObject();
		if (gameStatusFrame->AddComponent(droneTransform))
		{
			droneTransform->SetPosition(SD::Vector2(0.f, 0.33f));
			droneTransform->SetSize(SD::Vector2(0.25f, 0.3f));

			SD::SpriteComponent* droneSprite = SD::SpriteComponent::CreateObject();
			if (droneTransform->AddComponent(droneSprite))
			{
				droneSprite->SetSpriteTexture(texturePool->FindTexture(TXT("DroneDestroyer.Enemies.Enemies5")));
			}
		}

		DronesLabel = SD::LabelComponent::CreateObject();
		if (gameStatusFrame->AddComponent(DronesLabel))
		{
			DronesLabel->SetAutoRefresh(false);
			DronesLabel->SetPosition(SD::Vector2(0.3f, 0.33f));
			DronesLabel->SetSize(SD::Vector2(0.75f, 0.3f));
			DronesLabel->SetWrapText(false);
			DronesLabel->SetClampText(false);
			DronesLabel->SetCharacterSize(characterSize);
			DronesLabel->SetAutoRefresh(true);

			if (DronesLabel->GetRenderComponent() != nullptr)
			{
				DronesLabel->GetRenderComponent()->SetFontColor(GetNormalColor());
			}
		}
		else
		{
			DronesLabel = nullptr;
		}

		SD::PlanarTransformComponent* survivorTransform = SD::PlanarTransformComponent::CreateObject();
		if (gameStatusFrame->AddComponent(survivorTransform))
		{
			survivorTransform->SetPosition(SD::Vector2(0.05f, 0.7f));
			survivorTransform->SetSize(SD::Vector2(0.175f, 0.27f));

			SD::SpriteComponent* survivorSprite = SD::SpriteComponent::CreateObject();
			if (survivorTransform->AddComponent(survivorSprite))
			{
				survivorSprite->SetSpriteTexture(texturePool->FindTexture(TXT("DroneDestroyer.Player.BrightPlayer")));
			}
		}

		SurvivorsLabel = SD::LabelComponent::CreateObject();
		if (gameStatusFrame->AddComponent(SurvivorsLabel))
		{
			SurvivorsLabel->SetAutoRefresh(false);
			SurvivorsLabel->SetPosition(SD::Vector2(0.3f, 0.66f));
			SurvivorsLabel->SetSize(SD::Vector2(0.75f, 0.3f));
			SurvivorsLabel->SetWrapText(false);
			SurvivorsLabel->SetClampText(false);
			SurvivorsLabel->SetCharacterSize(characterSize);
			SurvivorsLabel->SetAutoRefresh(true);
		}
		else
		{
			SurvivorsLabel = nullptr;
		}
	}

	SD::FrameComponent* playerStatusFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(playerStatusFrame))
	{
		playerStatusFrame->SetSize(SD::Vector2(0.2f, 0.075f));
		playerStatusFrame->SetPosition(SD::Vector2(0.f, 0.9f - playerStatusFrame->ReadSize().Y));
		playerStatusFrame->SetAnchorLeft(0.f);
		playerStatusFrame->SetBorderThickness(0.f);
		playerStatusFrame->SetLockedFrame(true);
		if (playerStatusFrame->RenderComponent != nullptr)
		{
			playerStatusFrame->RenderComponent->TopBorder = nullptr;
			playerStatusFrame->RenderComponent->RightBorder = nullptr;
			playerStatusFrame->RenderComponent->BottomBorder = nullptr;
			playerStatusFrame->RenderComponent->LeftBorder = nullptr;
			playerStatusFrame->RenderComponent->TopRightCorner = nullptr;
			playerStatusFrame->RenderComponent->BottomRightCorner = nullptr;
			playerStatusFrame->RenderComponent->BottomLeftCorner = nullptr;
			playerStatusFrame->RenderComponent->TopLeftCorner = nullptr;
			playerStatusFrame->RenderComponent->ClearSpriteTexture();
			playerStatusFrame->RenderComponent->FillColor = SD::Color(64, 64, 64, 150);
		}

		SD::INT charSize = 16;

		LivesRemainingLabel = SD::LabelComponent::CreateObject();
		if (playerStatusFrame->AddComponent(LivesRemainingLabel))
		{
			LivesRemainingLabel->SetAutoRefresh(false);
			LivesRemainingLabel->SetPosition(SD::Vector2(0.f, 0.f));
			LivesRemainingLabel->SetSize(SD::Vector2(1.f, 0.5f));
			LivesRemainingLabel->SetWrapText(false);
			LivesRemainingLabel->SetClampText(false);
			LivesRemainingLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			LivesRemainingLabel->SetCharacterSize(charSize);
			LivesRemainingLabel->SetAutoRefresh(true);
		}
		else
		{
			LivesRemainingLabel = nullptr;
		}

		KillsLabel = SD::LabelComponent::CreateObject();
		if (playerStatusFrame->AddComponent(KillsLabel))
		{
			KillsLabel->SetAutoRefresh(false);
			KillsLabel->SetPosition(SD::Vector2(0.f, 0.5f));
			KillsLabel->SetSize(SD::Vector2(1.f, 0.5f));
			KillsLabel->SetWrapText(false);
			KillsLabel->SetClampText(false);
			KillsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			KillsLabel->SetCharacterSize(charSize);
			KillsLabel->SetAutoRefresh(true);

			if (KillsLabel->GetRenderComponent() != nullptr)
			{
				KillsLabel->GetRenderComponent()->SetFontColor(GetNormalColor());
			}
		}
		else
		{
			KillsLabel = nullptr;
		}
	}
}

void HudExtinction::UpdateLabels (Extinction* game, PlayerSoul* playerSoul)
{
	if (WaveLabel != nullptr)
	{
		WaveLabel->SetText(WavePrefix + game->GetWave().ToString());
	}

	if (DronesLabel != nullptr)
	{
		DronesLabel->SetText(SD::INT(game->ReadDrones().size()).ToString());
	}

	if (SurvivorsLabel != nullptr)
	{
		SD::INT numSurvivors = 0;
		const std::vector<SD::DPointer<PlayerSoul>>& rankedPlayers = game->ReadRankedPlayers();
		for (size_t i = 0; i < rankedPlayers.size(); ++i)
		{
			if (!rankedPlayers.at(i)->GetIsEliminated())
			{
				++numSurvivors;
			}
		}

		SurvivorsLabel->SetText(numSurvivors.ToString());
		if (SurvivorsLabel->GetRenderComponent() != nullptr)
		{
			SurvivorsLabel->GetRenderComponent()->SetFontColor((numSurvivors > 1) ? GetNormalColor() : GetUrgentColor());
		}
	}

	if (LivesRemainingLabel != nullptr && playerSoul != nullptr)
	{
		LivesRemainingLabel->SetText(LivesPrefix + playerSoul->GetLivesRemaining().ToString());
		if (LivesRemainingLabel->GetRenderComponent() != nullptr)
		{
			LivesRemainingLabel->GetRenderComponent()->SetFontColor((playerSoul->GetLivesRemaining() > 0) ? GetNormalColor() : GetUrgentColor());
		}
	}

	if (KillsLabel != nullptr && playerSoul != nullptr)
	{
		KillsLabel->SetText(KillsPrefix + playerSoul->GetNumKills().ToString());
	}
}

sf::Color HudExtinction::GetNormalColor ()
{
	return SD::Color::WHITE.Source;
}

sf::Color HudExtinction::GetUrgentColor ()
{
	return sf::Color(200, 0, 0, 255);
}
DD_END