/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Sound.cpp
=====================================================================
*/

#include "Sound.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Sound, SD, Object)

const SD::Directory Sound::SOUND_DIRECTORY(SD::Directory::BASE_DIRECTORY / TXT("Sounds"));

void Sound::InitProps ()
{
	Super::InitProps();

	SoundName = TXT("UnknownSound");
	Buffer = nullptr;
}

SD::DString Sound::GetFriendlyName () const
{
	if (!SoundName.IsEmpty())
	{
		return TXT("Sound:  ") + SoundName;
	}

	return Super::GetFriendlyName();
}

void Sound::Destroyed ()
{
	Release();

	Super::Destroyed();
}

void Sound::Release ()
{
	if (Buffer != nullptr)
	{
		delete Buffer;
		Buffer = nullptr;
	}
}

void Sound::SetResource (sf::SoundBuffer* newBuffer)
{
	Buffer = newBuffer;
}

SD::DString Sound::GetSoundName () const
{
	return SoundName;
}

const sf::SoundBuffer* Sound::GetBuffer () const
{
	return Buffer;
}
DD_END