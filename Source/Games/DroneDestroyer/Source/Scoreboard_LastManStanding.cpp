/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Scoreboard_LastManStanding.cpp
=====================================================================
*/

#include "Scoreboard_LastManStanding.h"
#include "ScoreComponent_LastManStanding.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Scoreboard_LastManStanding, DD, Scoreboard)

void Scoreboard_LastManStanding::InitializeGameLimitLabel ()
{
	Super::InitializeGameLimitLabel();

	if (GameLimitLabel != nullptr)
	{
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		GameLimitLabel->SetText(translator->TranslateText(TXT("LastManStandingSummary"), TRANSLATION_FILE, TranslationSectionName));
	}
}

void Scoreboard_LastManStanding::InitializeHeaderLabel (SD::FrameComponent* headerFrame)
{
	Super::InitializeHeaderLabel(headerFrame);

	if (headerFrame != nullptr)
	{
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		SD::LabelComponent* livesLabel = SD::LabelComponent::CreateObject();
		if (headerFrame->AddComponent(livesLabel))
		{
			livesLabel->SetAutoRefresh(false);
			livesLabel->SetPosition(SD::Vector2(0.6f, 0.f));
			livesLabel->SetSize(SD::Vector2(0.2f, 1.f));
			livesLabel->SetWrapText(false);
			livesLabel->SetClampText(false);
			livesLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			livesLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			livesLabel->SetText(translator->TranslateText(TXT("Lives"), TRANSLATION_FILE, TranslationSectionName));
			livesLabel->SetAutoRefresh(true);
		}

		SD::LabelComponent* killsLabel = SD::LabelComponent::CreateObject();
		if (headerFrame->AddComponent(killsLabel))
		{
			killsLabel->SetAutoRefresh(false);
			killsLabel->SetPosition(SD::Vector2(0.8f, 0.f));
			killsLabel->SetSize(SD::Vector2(0.2f, 1.f));
			killsLabel->SetWrapText(false);
			killsLabel->SetClampText(false);
			killsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			killsLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			killsLabel->SetText(translator->TranslateText(TXT("Kills"), TRANSLATION_FILE, TranslationSectionName));
			killsLabel->SetAutoRefresh(true);
		}
	}
}

const ScoreComponent* Scoreboard_LastManStanding::GetScoreComponentClass () const
{
	return dynamic_cast<const ScoreComponent_LastManStanding*>(ScoreComponent_LastManStanding::SStaticClass()->GetDefaultObject());
}

DD_END