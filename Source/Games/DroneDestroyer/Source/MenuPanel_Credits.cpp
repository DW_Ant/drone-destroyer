/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanel_Credits.cpp
=====================================================================
*/

#include "MainMenu.h"
#include "MenuPanel_Credits.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, MenuPanel_Credits, DD, MenuPanelComponent)

void MenuPanel_Credits::InitProps ()
{
	Super::InitProps();

	ConfigKey = TXT("Credits");

	CreditsOwner = nullptr;
}

void MenuPanel_Credits::RestorePanel ()
{
	//Noop
}

void MenuPanel_Credits::SavePanelState ()
{
	//Noop
}

void MenuPanel_Credits::InitializeComponents ()
{
	Super::InitializeComponents();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	SD::ScrollbarComponent* creditsScroll = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(creditsScroll))
	{
		creditsScroll->SetPosition(SD::Vector2(0.1f, 0.1f));
		creditsScroll->SetSize(SD::Vector2(0.8f, 0.5f));
		creditsScroll->SetZoomEnabled(false);
		creditsScroll->SetHideControlsWhenFull(true);

		SD::LabelComponent* devLabel = nullptr;
		SD::LabelComponent::CreateLabelWithinScrollbar(creditsScroll, OUT CreditsOwner, OUT devLabel);
		if (devLabel != nullptr)
		{
			devLabel->SetAutoRefresh(false);
			devLabel->SetCharacterSize(28);
			devLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			devLabel->SetText(translator->TranslateText(TXT("Credits"), TRANSLATION_FILE, TXT("Credits")));
			devLabel->SetAutoRefresh(true);
		}

	}

	SD::PlanarTransformComponent* sfmlTransform = SD::PlanarTransformComponent::CreateObject();
	if (AddComponent(sfmlTransform))
	{
		sfmlTransform->SetPosition(SD::Vector2(0.f, 0.7f));
		//Reason for odd transform is to preserve the aspect ratio while not going beyond 15% vertical height of UI.
		sfmlTransform->SetSize(SD::Vector2(0.45f, 0.15f));
		
		SD::SpriteComponent* sfmlSprite = SD::SpriteComponent::CreateObject();
		if (sfmlTransform->AddComponent(sfmlSprite))
		{
			sfmlSprite->SetSpriteTexture(localTexturePool->FindTexture(TXT("DroneDestroyer.SfmlLogo")));
		}
	}

	SD::PlanarTransformComponent* box2dTransform = SD::PlanarTransformComponent::CreateObject();
	if (AddComponent(box2dTransform))
	{
		box2dTransform->SetSize(SD::Vector2(0.45f, 0.1125f));
		box2dTransform->SetPosition(SD::Vector2(0.55f, 0.7f + ((sfmlTransform->ReadSize().Y - box2dTransform->ReadSize().Y) / 2)));

		SD::SpriteComponent* box2dSprite = SD::SpriteComponent::CreateObject();
		if (box2dTransform->AddComponent(box2dSprite))
		{
			box2dSprite->SetSpriteTexture(localTexturePool->FindTexture(TXT("DroneDestroyer.Box2dLogo")));
		}
	}

	SD::ButtonComponent* backButton = SD::ButtonComponent::CreateObject();
	if (AddComponent(backButton))
	{
		backButton->SetPosition(SD::Vector2(0.1f, 0.9f));
		backButton->SetSize(SD::Vector2(0.1f, 0.05f));
		backButton->SetCaptionText(translator->TranslateText(TXT("Back"), TRANSLATION_FILE, TXT("Credits")));
		backButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_Credits, HandleBackReleased, void, SD::ButtonComponent*));
	}
}

void MenuPanel_Credits::Destroyed ()
{
	if (CreditsOwner != nullptr)
	{
		CreditsOwner->Destroy();
		CreditsOwner = nullptr;
	}

	Super::Destroyed();
}

void MenuPanel_Credits::HandleBackReleased (SD::ButtonComponent* uiComponent)
{
	MainMenu* owningMenu = GetOwningMenu();
	CHECK(owningMenu != nullptr)
	owningMenu->NavigateBack();
}

DD_END