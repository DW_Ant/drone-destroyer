/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HealthRegeneration.cpp
=====================================================================
*/

#include "HealthComponent.h"
#include "HealthRegeneration.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, HealthRegeneration, SD, EntityComponent)

void HealthRegeneration::InitProps ()
{
	Super::InitProps();

	OwningHealthComp = nullptr;
	Tick = nullptr;
}

void HealthRegeneration::BeginObject ()
{
	Super::BeginObject();

	Tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, HealthRegeneration, HandleHealingTick, void, SD::FLOAT));
	}
}

bool HealthRegeneration::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	return (dynamic_cast<HealthComponent*>(ownerCandidate) != nullptr);
}

void HealthRegeneration::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwningHealthComp = dynamic_cast<HealthComponent*>(newOwner);
	CHECK(OwningHealthComp != nullptr)
}

void HealthRegeneration::ComponentDetached ()
{
	OwningHealthComp = nullptr;

	Super::ComponentDetached();
}

void HealthRegeneration::SetHealPerSecond (SD::FLOAT healPerSec)
{
	if (Tick != nullptr)
	{
		SD::FLOAT newInterval = (healPerSec > 0.f) ? (1.f/healPerSec) : 0.f;
		Tick->SetTickInterval(newInterval);
	}
}

void HealthRegeneration::HandleHealingTick (SD::FLOAT deltaSec)
{
	if (OwningHealthComp != nullptr && OwningHealthComp->GetHealth() > 0)
	{
		OwningHealthComp->SetHealth(OwningHealthComp->GetHealth() + 1);
	}
}

DD_END