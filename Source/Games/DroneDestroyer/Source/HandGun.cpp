/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HandGun.cpp
=====================================================================
*/

#include "HandGun.h"
#include "PistolBullet.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, HandGun, DD, Weapon)

void HandGun::InitProps ()
{
	Super::InitProps();

#ifdef DEBUG_MODE
	DebugName = TXT("HandGun");
#endif

	ItemNameKey = TXT("HandGun");
	ShortNameKey = TXT("HandGunShort");
	PickupImage = TXT("DroneDestroyer.Pickups.Handgun");
	PossessedImage = TXT("DroneDestroyer.Weapons.MachineGun");
	StartingAmmo = 50;
	Ammo = StartingAmmo;
	MaxAmmo = 100;
	FireRate = 0.4f;
	FireSpread = 2.5f;
	ProjectileClass = PistolBullet::SStaticClass();
	ShootSound = TXT("W_Pistol");
	WeaponPriority = 1;
	WeaponSwitchBinds.push_back(sf::Keyboard::Num1);
	WeaponSwitchBinds.push_back(sf::Keyboard::Numpad1);

	SwitchToTime = 0.1f; //Fast draw
	SwitchOutTime = 0.1f; //Fast pull away
}
DD_END