/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HealthComponent.cpp
=====================================================================
*/

#include "Character.h"
#include "HealthComponent.h"
#include "PlayerSoul.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, HealthComponent, SD, EntityComponent)

void HealthComponent::InitProps ()
{
	Super::InitProps();

	MaxHealth = 100;
	Health = MaxHealth;
}

void HealthComponent::TakeDamage (SD::INT damageAmount, PlayerSoul* damagedBy, Projectile* damagedWith)
{
	if (damageAmount <= 0)
	{
		return;
	}

	if (damagedBy != nullptr && !damagedBy->CanHarm(this))
	{
		//damagedBy is not allowed to harm this component.
		return;
	}

	if (OnHandleDamage.IsBounded())
	{
		OnHandleDamage(OUT damageAmount, damagedBy, damagedWith);
	}

	if (damageAmount > 0)
	{
		SetHealth(Health - damageAmount);

#ifdef DEBUG_MODE
		if (damagedBy != nullptr)
		{
			DroneDestroyerLog.Log(SD::LogCategory::LL_Debug, TXT("%s has damaged %s. Health remaining = %s"), damagedBy->GetPlayerName(), GetRootEntity()->DebugName, Health);
		}
#endif

		OnTakeDamage.Broadcast(damageAmount, damagedBy, damagedWith);
	}
}

void HealthComponent::Heal (SD::INT healAmount)
{
	if (healAmount > 0 && Health < MaxHealth)
	{
		SD::INT oldHealth = Health;
		SetHealth(Health + healAmount);

		if (OnHealed.IsBounded())
		{
			OnHealed(Health - oldHealth);
		}
	}
}

void HealthComponent::SetHealth (SD::INT newHealth)
{
	newHealth = SD::Utils::Min(newHealth, MaxHealth);
	if (Health == newHealth)
	{
		return;
	}

	Health = newHealth;
	OnHealthChanged.Broadcast(Health);
}

void HealthComponent::SetMaxHealth (SD::INT newMaxHealth)
{
	if (Health > newMaxHealth)
	{
		SetHealth(newMaxHealth);
	}

	MaxHealth = newMaxHealth;
}

DD_END