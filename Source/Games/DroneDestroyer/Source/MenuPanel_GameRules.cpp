/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanel_GameRules.cpp
=====================================================================
*/

#include <regex>

#include "Arena.h"
#include "DroneDestroyerEngineComponent.h"
#include "GameModifier.h"
#include "GameRules.h"
#include "MainMenu.h"
#include "MenuPanel_GameRules.h"
#include "MenuPanel_Roster.h"
#include "SoundEngineComponent.h"
#include "SoundMessenger.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, MenuPanel_GameRules, DD, MenuPanelComponent)

const SD::INT MenuPanel_GameRules::MAX_NUM_BOTS(150);

void MenuPanel_GameRules::InitProps ()
{
	Super::InitProps();

	ConfigKey = TXT("GameRules");

	SelectedMapIdx = 0;
	SelectedDifficultyIdx = 3; //Default to Easy
	MapPreview = nullptr;
	MapAuthor = nullptr;
	RecommendedNumPlayers = nullptr;
	MapDescription = nullptr;
	MapListEntity = nullptr;
	MapDescriptionEntity = nullptr;
	GameRulesEntity = nullptr;
	RulesSection = nullptr;
	RulesSectionAbsHeight = -1.f;
	GameRulesOptionsScrollbar = nullptr;
	NumBots = nullptr;
}

void MenuPanel_GameRules::RestorePanel ()
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(ConfigFile, false))
	{
		config->Destroy();
		return;
	}

	SD::DString selectedMap = config->GetPropertyText(ConfigKey, TXT("SelectedMap"));
	SelectMap(selectedMap);

	//Game specific settings
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)
	for (size_t i = 0; i < GameRulesOptions.size(); ++i)
	{
		SD::DString prevValue = config->GetPropertyText(ConfigKey, localDroneEngine->GetGame()->ReadConfigName() + TXT("_") + SD::INT(i).ToString());
		if (!prevValue.IsEmpty())
		{
			GameRulesOptions.at(i)->SetText(prevValue);
		}
	}

	SD::DString difficulty = config->GetPropertyText(ConfigKey, TXT("Difficulty"));
	for (SD::ButtonComponent* button : DifficultyButtons)
	{
		if (button->GetCaptionText().Compare(difficulty, SD::DString::CC_IgnoreCase) == 0)
		{
			SelectDifficulty(button);
			break;
		}
	}

	if (NumBots != nullptr)
	{
		SD::INT numBots = config->GetProperty<SD::INT>(ConfigKey, TXT("NumBots"));
		numBots = SD::Utils::Min(MAX_NUM_BOTS, numBots);
		NumBots->SetText(numBots.ToString());
		localDroneEngine->GetGame()->SetNumBots(numBots);
	}

	for (SGameModifier& modifier : Modifiers)
	{
		SD::DString checkboxStr = modifier.Checkbox->CaptionComponent->GetContent();
		modifier.Checkbox->SetChecked(config->GetProperty<SD::BOOL>(ConfigKey, checkboxStr).Value);
	}
	
	config->Destroy();
	EvaluateGameModifierEnableness();
}

void MenuPanel_GameRules::SavePanelState ()
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(ConfigFile, false))
	{
		config->Destroy();
		return;
	}

	config->SavePropertyText(ConfigKey, TXT("SelectedMap"), MapSelection.at(SelectedMapIdx).MapName);
	config->SavePropertyText(ConfigKey, TXT("Difficulty"), DifficultyButtons.at(SelectedDifficultyIdx)->CaptionComponent->GetContent());
	config->SaveProperty<SD::INT>(ConfigKey, TXT("NumBots"), SD::INT(NumBots->GetContent()));

	//Game specific settings
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)
	for (size_t i = 0; i < GameRulesOptions.size(); ++i)
	{
		config->SavePropertyText(ConfigKey, localDroneEngine->GetGame()->ReadConfigName() + TXT("_") + SD::INT(i).ToString(), GameRulesOptions.at(i)->GetContent());
	}

	for (SGameModifier& modifier : Modifiers)
	{
		config->SaveProperty<SD::BOOL>(ConfigKey, modifier.Checkbox->CaptionComponent->GetContent(), modifier.Checkbox->IsChecked());
	}

	config->Destroy();
}

void MenuPanel_GameRules::InitializeComponents ()
{
	Super::InitializeComponents();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	std::function<SD::FrameComponent*(const SD::Vector2& /*pos*/, const SD::Vector2& /*size*/, const SD::DString& translationKey)> createFramedSection([&](const SD::Vector2& framePos, const SD::Vector2& frameSize, const SD::DString& translationKey)
	{
		SD::FrameComponent* result = SD::FrameComponent::CreateObject();
		if (AddComponent(result))
		{
			result->SetLockedFrame(true);
			result->SetPosition(framePos);
			result->SetSize(frameSize);
			result->SetBorderThickness(4.f);
			if (result->RenderComponent != nullptr)
			{
				result->RenderComponent->ClearSpriteTexture();
				result->RenderComponent->FillColor = SD::Color(211, 122, 110, 64);
			}

#if 0
			if (!translationKey.IsEmpty())
			{
				SD::LabelComponent* caption = SD::LabelComponent::CreateObject();
				if (result->AddComponent(caption))
				{
					caption->SetAutoRefresh(false);
					caption->SetWrapText(false);
					caption->SetClampText(false);
					caption->SetText(translator->TranslateText(translationKey, TRANSLATION_FILE, TXT("GameRulesMenu")));
					caption->SetAutoRefresh(true);
				}
			}
#endif
		}
		else
		{
			result = nullptr;
		}

		return result;
	});

	SD::FLOAT sectionMargins = 0.01f;
	SD::FLOAT drawPos = sectionMargins;

	//Create Map Selection portion
	SD::FrameComponent* mapSelection = createFramedSection(SD::Vector2(sectionMargins, drawPos), SD::Vector2(1.f - (sectionMargins * 2.f), 0.4f), TXT("MapSection"));
	if (mapSelection != nullptr)
	{
		SD::FLOAT anchorDist = 8.f;
		drawPos += mapSelection->ReadSize().Y + sectionMargins;

		SD::ScrollbarComponent* selectionScrollbar = SD::ScrollbarComponent::CreateObject();
		if (mapSelection->AddComponent(selectionScrollbar))
		{
			selectionScrollbar->SetPosition(SD::Vector2(0.025f, 0.f));
			selectionScrollbar->SetSize(SD::Vector2(0.45f, 1.f));
			selectionScrollbar->SetAnchorTop(anchorDist);
			selectionScrollbar->SetAnchorLeft(anchorDist);
			selectionScrollbar->SetAnchorBottom(anchorDist);
			selectionScrollbar->SetZoomEnabled(false);
			selectionScrollbar->SetHideControlsWhenFull(true);

			MapListEntity = SD::GuiEntity::CreateObject();
			MapListEntity->SetAutoSizeVertical(true);
			MapListEntity->SetAutoSizeHorizontal(false);
			MapListEntity->SetGuiSizeToOwningScrollbar(SD::Vector2(1.f, -1.f));

			std::vector<SD::DString> mapList;
			GenerateMapList(OUT mapList);
			SD::FLOAT mapButtonHeight = 32.f;
			for (size_t i = 0; i < mapList.size(); ++i)
			{
				SD::ButtonComponent* button = SD::ButtonComponent::CreateObject();
				if (MapListEntity->AddComponent(button))
				{
					button->SetPosition(SD::Vector2(0.f, 8.f + ((mapButtonHeight + 8.f) * SD::FLOAT::MakeFloat(i))));
					button->SetSize(SD::Vector2(0.f, mapButtonHeight));
					button->SetAnchorLeft(8.f);
					button->SetAnchorRight(8.f);
					button->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameRules, HandleMapButtonReleased, void, SD::ButtonComponent*));
					button->SetCaptionText(mapList.at(i));
					MapSelection.push_back(SMapSelection(button, mapList.at(i)));
				}
			}

			selectionScrollbar->SetViewedObject(MapListEntity);
		}

		SD::Vector2 componentPos(0.5f, 0.025f);
		SD::FLOAT mapSectionWidth = 0.5f;
		SD::PlanarTransformComponent* previewTransform = SD::PlanarTransformComponent::CreateObject();
		if (mapSelection->AddComponent(previewTransform))
		{
			previewTransform->SetPosition(SD::Vector2(componentPos.X + (mapSectionWidth * 0.25f), componentPos.Y));
			previewTransform->SetSize(SD::Vector2(mapSectionWidth * 0.5f, 0.5f));
			componentPos.Y += previewTransform->ReadSize().Y + sectionMargins;

			MapPreview = SD::SpriteComponent::CreateObject();
			if (previewTransform->AddComponent(MapPreview))
			{
				//Noop. The texture will be set when a map is selected
			}

			RecommendedNumPlayers = SD::LabelComponent::CreateObject();
			if (previewTransform->AddComponent(RecommendedNumPlayers))
			{
				RecommendedNumPlayers->SetAutoRefresh(false);
				RecommendedNumPlayers->SetPosition(SD::Vector2(0.f, 0.8f));
				RecommendedNumPlayers->SetSize(0.9f, 0.2f);
				RecommendedNumPlayers->SetAnchorLeft(anchorDist);
				RecommendedNumPlayers->SetAnchorRight(anchorDist);
				RecommendedNumPlayers->SetAnchorBottom(anchorDist);

				RecommendedNumPlayers->SetWrapText(false);
				RecommendedNumPlayers->SetClampText(false);
				RecommendedNumPlayers->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
				//Text will be set when map is selected
				RecommendedNumPlayers->SetAutoRefresh(true);
			}
		}

#if 1 //Uncommented since there's the possibility for players to build their own maps.
		MapAuthor = SD::LabelComponent::CreateObject();
		if (mapSelection->AddComponent(MapAuthor))
		{
			MapAuthor->SetAutoRefresh(false);
			MapAuthor->SetPosition(componentPos);
			MapAuthor->SetSize(mapSectionWidth, 0.1f);
			componentPos.Y += MapAuthor->ReadSize().Y;

			MapAuthor->SetWrapText(false);
			MapAuthor->SetClampText(true);
			MapAuthor->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			//Text will be set when map is selected
			MapAuthor->SetAutoRefresh(true);
		}
#endif

		SD::ScrollbarComponent* descriptionScrollbar = SD::ScrollbarComponent::CreateObject();
		if (mapSelection->AddComponent(descriptionScrollbar))
		{
			descriptionScrollbar->SetPosition(componentPos);
			descriptionScrollbar->SetSize(mapSectionWidth, 0.35f);
			descriptionScrollbar->SetAnchorRight(anchorDist);
			descriptionScrollbar->SetAnchorBottom(anchorDist);
			descriptionScrollbar->SetHideControlsWhenFull(true);
			descriptionScrollbar->SetZoomEnabled(false);
			componentPos.Y = 1.f;

			SD::LabelComponent::CreateLabelWithinScrollbar(descriptionScrollbar, OUT MapDescriptionEntity, OUT MapDescription);
			if (MapDescription != nullptr)
			{
				MapDescription->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
				//Text will be set when map is selected
			}
		}
	}

	//Default to the first map just incase the restoration data doesn't have anything.
	if (MapSelection.size() > 0)
	{
		SelectMap(MapSelection.at(0).MapName);
	}

	RulesSection = createFramedSection(SD::Vector2(sectionMargins, drawPos), SD::Vector2(1.f - (sectionMargins * 2.f), 0.25f), TXT("GameRulesSection"));
	if (RulesSection != nullptr)
	{
		drawPos += RulesSection->ReadSize().Y + sectionMargins;

		std::function<SD::TextFieldComponent*(SD::Entity*, const SD::Vector2& /*pos*/, const SD::Vector2& /*size*/, const SD::DString& translationKey, SD::LabelComponent*&)> createNumField([&](SD::Entity* compOwner, const SD::Vector2& position, const SD::Vector2& combinedSize, const SD::DString& translationKey, SD::LabelComponent*& outGeneratedLabel)
		{
			SD::TextFieldComponent* result = nullptr;
			outGeneratedLabel = SD::LabelComponent::CreateObject();
			if (compOwner->AddComponent(outGeneratedLabel))
			{
				outGeneratedLabel->SetAutoRefresh(false);
				outGeneratedLabel->SetPosition(position);
				outGeneratedLabel->SetSize(SD::Vector2(0.75f, 1.f) * combinedSize);
				outGeneratedLabel->SetWrapText(false);
				outGeneratedLabel->SetClampText(true);
				outGeneratedLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Right);
				outGeneratedLabel->SetText(translator->TranslateText(translationKey, TRANSLATION_FILE, TXT("GameRulesMenu")));
				outGeneratedLabel->SetAutoRefresh(true);

				SD::FrameComponent* textBackground = SD::FrameComponent::CreateObject();
				if (compOwner->AddComponent(textBackground))
				{
					textBackground->SetPosition(position + SD::Vector2(outGeneratedLabel->ReadSize().X, 0.f));
					textBackground->SetSize(SD::Vector2(0.15f, 1.f) * combinedSize);
					textBackground->SetLockedFrame(true);
					textBackground->SetBorderThickness(2.f);
					if (textBackground->RenderComponent != nullptr)
					{
						textBackground->RenderComponent->ClearSpriteTexture();
						textBackground->RenderComponent->FillColor = SD::Color::BLACK;
					}

					result = SD::TextFieldComponent::CreateObject();
					if (textBackground->AddComponent(result))
					{
						result->SetAnchorTop(4.f);
						result->SetAnchorRight(4.f);
						result->SetAnchorBottom(4.f);
						result->SetAnchorLeft(4.f);
						result->MaxNumCharacters = 4;
						result->OnAllowTextInput = SDFUNCTION_1PARAM(this, MenuPanel_GameRules, HandleAllowTextInput, bool, const SD::DString&);
						result->OnReturn = SDFUNCTION_1PARAM(this, MenuPanel_GameRules, HandleTextReturn, void, SD::TextFieldComponent*);
					}
					else
					{
						result = nullptr;
					}
				}
			}
			else
			{
				outGeneratedLabel = nullptr;
			}

			return result;
		});

		SD::FLOAT spacing = 0.01f;
		SD::Vector2 fieldSize(0.5f - (spacing * 2.f), 0.2f);
		SD::Vector2 buttonSize(0.5f - (spacing * 2.f), 0.125f);
		SD::FLOAT gameRulesDrawPos = spacing * 2.f;

		//Populate the game-specific rules section
		{
			DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
			CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr && localDroneEngine->GetMainCamera() != nullptr)

			GameRulesOptionsScrollbar = SD::ScrollbarComponent::CreateObject();
			if (RulesSection->AddComponent(GameRulesOptionsScrollbar))
			{
				const std::vector<GameRules::SGameRulesOption>& options = localDroneEngine->GetGame()->ReadGameSettings();

				GameRulesOptionsScrollbar->SetPosition(SD::Vector2(spacing, 0.025f));
				GameRulesOptionsScrollbar->SetSize(SD::Vector2(fieldSize.X, 0.95f - (spacing * 2.f)));
				GameRulesOptionsScrollbar->SetHideControlsWhenFull(true);
				GameRulesOptionsScrollbar->SetZoomEnabled(false);

				GameRulesEntity = SD::GuiEntity::CreateObject();
				GameRulesEntity->SetGuiSizeToOwningScrollbar(SD::Vector2(1.f, -1.f));
				GameRulesEntity->SetAutoSizeVertical(true);
				
				//Figure out the size of the GuiEntity (since this can't use normalize coordinates). This will be set after the cached size has been determined.
				RulesSectionAbsHeight = RulesSection->ReadCachedAbsSize().Y;
				GameRulesOptionsScrollbar->SetViewedObject(GameRulesEntity);

				for (const GameRules::SGameRulesOption& option : options)
				{
					SD::LabelComponent* prompt;
					SD::TextFieldComponent* field = createNumField(GameRulesEntity, SD::Vector2(0.f, gameRulesDrawPos), SD::Vector2(1.f, fieldSize.Y), option.PromptKey, OUT prompt);
					if (field != nullptr)
					{
						GameRulesPrompts.push_back(prompt);
						GameRulesOptions.push_back(field);
						field->SetText(option.DefaultValue.ToString());
						gameRulesDrawPos += fieldSize.Y + spacing;
					}
				}

				SD::TickComponent* ruleHeightTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
				if (RulesSection->AddComponent(ruleHeightTick))
				{
					ruleHeightTick->SetTickInterval(0.1f);
					ruleHeightTick->SetTickHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameRules, HandleTickCheckSectionHeight, void, SD::FLOAT));
				}
			}
			else
			{
				GameRulesOptionsScrollbar = nullptr;
			}
		}

		gameRulesDrawPos = spacing * 2.f;
		SD::LabelComponent* prompt;
		NumBots = createNumField(RulesSection, SD::Vector2(0.5f, gameRulesDrawPos), fieldSize, TXT("NumBots"), OUT prompt);
		if (NumBots != nullptr)
		{
			NumBots->SetText(TXT("1")); //Default to one bot
			gameRulesDrawPos += fieldSize.Y + (spacing * 4.f);
		}

		std::function<SD::ButtonComponent*(GameRules::EGameDifficulty /*difficulty*/)> createDifficultyButton([&](GameRules::EGameDifficulty difficulty)
		{
			SD::ButtonComponent* result = SD::ButtonComponent::CreateObject();
			if (RulesSection->AddComponent(result))
			{
				result->SetPosition(SD::Vector2(0.5f, gameRulesDrawPos));
				result->SetSize(buttonSize);
				gameRulesDrawPos += buttonSize.Y + spacing;

				result->SetCaptionText(GameRules::GetDifficultyName(difficulty));
				result->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameRules, HandleDifficultyButtonReleased, void, SD::ButtonComponent*));
			}
			else
			{
				result = nullptr;
			}

			return result;
		});

		DifficultyButtons.push_back(createDifficultyButton(GameRules::EGameDifficulty::Pacifist));
		DifficultyButtons.push_back(createDifficultyButton(GameRules::EGameDifficulty::Novice));
		DifficultyButtons.push_back(createDifficultyButton(GameRules::EGameDifficulty::Easy));
		DifficultyButtons.push_back(createDifficultyButton(GameRules::EGameDifficulty::Medium));
		DifficultyButtons.push_back(createDifficultyButton(GameRules::EGameDifficulty::Hard));
	}

	SD::FrameComponent* modifiersSection = createFramedSection(SD::Vector2(sectionMargins, drawPos), SD::Vector2(1.f - (sectionMargins * 2.f), 0.25f), TXT("GameModifiersSection"));
	if (modifiersSection != nullptr)
	{
		drawPos += modifiersSection->ReadSize().Y + sectionMargins;

		std::vector<const GameModifier*> modifiers;
		GenerateModifierList(OUT modifiers);

		SD::FLOAT checkboxSpacing = 0.01f;
		SD::FLOAT checkboxPos = checkboxSpacing * 4.f; //Add a top margin
		for (const GameModifier* modifier : modifiers)
		{
			SD::CheckboxComponent* checkbox = SD::CheckboxComponent::CreateObject();
			if (modifiersSection->AddComponent(checkbox))
			{
				checkbox->SetPosition(SD::Vector2(checkboxSpacing, checkboxPos));
				checkbox->SetSize(SD::Vector2(0.35f, 0.15f));
				checkboxPos += checkbox->ReadSize().Y + checkboxSpacing;
				checkbox->CaptionComponent->SetText(modifier->ReadFriendlyName());
				checkbox->OnChecked = SDFUNCTION_1PARAM(this, MenuPanel_GameRules, HandleGameModChecked, void, SD::CheckboxComponent*);
				Modifiers.push_back(SGameModifier(checkbox, modifier));
			}
		}
	}

	//Add navigation buttons
	{
		SD::ButtonComponent* backButton = SD::ButtonComponent::CreateObject();
		if (AddComponent(backButton))
		{
			backButton->SetSize(SD::Vector2(0.25f, 0.05f));
			backButton->SetAnchorBottom(8.f);
			backButton->SetAnchorLeft(8.f);
			backButton->SetCaptionText(translator->TranslateText(TXT("Back"), TRANSLATION_FILE, TXT("GameRulesMenu")));
			backButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameRules, HandleBackButtonReleased, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* nextButton = SD::ButtonComponent::CreateObject();
		if (AddComponent(nextButton))
		{
			nextButton->SetSize(SD::Vector2(0.25f, 0.05f));
			nextButton->SetAnchorBottom(8.f);
			nextButton->SetAnchorRight(8.f);
			nextButton->SetCaptionText(translator->TranslateText(TXT("Next"), TRANSLATION_FILE, TXT("GameRulesMenu")));
			nextButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameRules, HandleNextButtonReleased, void, SD::ButtonComponent*));
		}
	}
}

bool MenuPanel_GameRules::ProcessMouseClick_Implementation (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType, bool consumedEvent)
{
	if (GameRulesOptionsScrollbar != nullptr && !GameRulesOptionsScrollbar->IsWithinBounds(SD::Vector2(SD::FLOAT::MakeFloat(sfmlEvent.x), SD::FLOAT::MakeFloat(sfmlEvent.y))))
	{
		//There's a bug with scrollbar components where they don't pass input events to the ViewedObject if the mouse event is not over them. This is a workaround to auto deselect all text fields within that scrollbar so that they no longer consume input.
		for (SD::TextFieldComponent* gameOption : GameRulesOptions)
		{
			gameOption->SetCursorPosition(-1);
			gameOption->SetHighlightEndPosition(-1);
		}
	}

	return Super::ProcessMouseClick_Implementation(mouse, sfmlEvent, eventType, consumedEvent);
}

void MenuPanel_GameRules::Destroyed ()
{
	if (MapListEntity != nullptr)
	{
		MapListEntity->Destroy();
		MapListEntity = nullptr;
	}

	if (MapDescriptionEntity != nullptr)
	{
		MapDescriptionEntity->Destroy();
		MapDescriptionEntity = nullptr;
	}

	if (GameRulesEntity != nullptr)
	{
		GameRulesEntity->Destroy();
		GameRulesEntity = nullptr;
	}

	Super::Destroyed();
}

void MenuPanel_GameRules::GenerateMapList (std::vector<SD::DString>& outMapList) const
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	CHECK(config != nullptr)
	if (!config->OpenFile((SD::Directory::CONFIG_DIRECTORY).ToString() + TXT("Arenas.ini"), false))
	{
		config->Destroy();
		return;
	}

	config->GetArrayValues<SD::DString>(TXT("Arenas"), TXT("ArenaList"), OUT outMapList);
	config->Destroy();
}

void MenuPanel_GameRules::GenerateModifierList (std::vector<const GameModifier*>& outModifiers) const
{
	for (SD::ClassIterator iter(GameModifier::SStaticClass()); iter.GetSelectedClass() != nullptr; ++iter)
	{
		//Don't include base class
		if (iter.GetSelectedClass() == GameModifier::SStaticClass())
		{
			continue;
		}

		if (const GameModifier* gameMod = dynamic_cast<const GameModifier*>(iter.GetSelectedClass()->GetDefaultObject()))
		{
			outModifiers.push_back(gameMod);
		}
	}

	std::sort(outModifiers.begin(), outModifiers.end(), [](const GameModifier* a, const GameModifier* b)
	{
		return (a->ReadFriendlyName().Compare(b->ReadFriendlyName(), SD::DString::CC_CaseSensitive) < 0);
	});
}

void MenuPanel_GameRules::SelectMap (const SD::DString& selectedMapName)
{
	if (selectedMapName.IsEmpty())
	{
		return;
	}

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	//Ensure everything else but the selected button is enabled
	for (size_t i = 0; i < MapSelection.size(); ++i)
	{
		bool isSelected = MapSelection.at(i).MapName == selectedMapName;
		MapSelection.at(i).Button->SetEnabled(!isSelected);
		if (isSelected)
		{
			SelectedMapIdx = i;
		}
	}

	SD::DString mapPreview;
	SD::Range<SD::INT> recommendedPlayers;
	SD::DString authorName;
	SD::DString descriptionKey;
	Arena::GetArenaMetaData(selectedMapName, OUT mapPreview, OUT recommendedPlayers, OUT authorName, OUT descriptionKey);

	if (MapPreview != nullptr && !mapPreview.IsEmpty())
	{
		SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)
		MapPreview->SetSpriteTexture(localTexturePool->FindTexture(mapPreview));
	}

	if (MapAuthor != nullptr)
	{
		MapAuthor->SetText(SD::DString::CreateFormattedString(translator->TranslateText(TXT("Author"), TRANSLATION_FILE, TXT("GameRulesMenu")), authorName));
	}

	if (RecommendedNumPlayers != nullptr)
	{
		RecommendedNumPlayers->SetText(SD::DString::CreateFormattedString(translator->TranslateText(TXT("RecommendedPlayers"), TRANSLATION_FILE, TXT("GameRulesMenu")), recommendedPlayers.Min, recommendedPlayers.Max));
	}

	if (MapDescription != nullptr)
	{
		MapDescription->SetText(translator->TranslateText(descriptionKey, TRANSLATION_FILE, TXT("Arenas")));
	}
}

void MenuPanel_GameRules::SelectDifficulty (SD::ButtonComponent* uiComponent)
{
	for (size_t i = 0; i < DifficultyButtons.size(); ++i)
	{
		bool isSelected = (uiComponent == DifficultyButtons.at(i));
		DifficultyButtons.at(i)->SetEnabled(!isSelected);
		if (isSelected)
		{
			SelectedDifficultyIdx = i;
		}
	}
}

void MenuPanel_GameRules::EvaluateGameModifierEnableness ()
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)

	for (SGameModifier mod : Modifiers)
	{
		if (mod.Checkbox != nullptr && mod.Modifier != nullptr)
		{
			mod.Checkbox->SetEnabled(mod.Modifier->IsAllowed(localDroneEngine->GetGame()));
		}
	}
}

bool MenuPanel_GameRules::HandleAllowTextInput (const SD::DString& txt)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_TYPING);
	}

	std::regex regex("[0-9]"); //Only accept numbers (not even decimals or negative values)
	std::cmatch match;

	return std::regex_match(txt.ReadString().c_str(), OUT match, regex);
}

void MenuPanel_GameRules::HandleTextReturn (SD::TextFieldComponent* uiComponent)
{
	if (uiComponent == NumBots)
	{
		SD::INT curNumBots = SD::INT(uiComponent->GetContent());
		if (curNumBots > MAX_NUM_BOTS)
		{
			uiComponent->SetText(MAX_NUM_BOTS.ToString());
			curNumBots = MAX_NUM_BOTS;
		}

		//Update the game early to see which game modifiers are enabled.
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)
		localDroneEngine->GetGame()->SetNumBots(curNumBots);
	}

	EvaluateGameModifierEnableness();
}

void MenuPanel_GameRules::HandleMapButtonReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	SelectMap(uiComponent->GetCaptionText());
	EvaluateGameModifierEnableness();
}

void MenuPanel_GameRules::HandleDifficultyButtonReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	SelectDifficulty(uiComponent);
}

void MenuPanel_GameRules::HandleGameModChecked (SD::CheckboxComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, (uiComponent->IsChecked()) ? SoundEngineComponent::SOUND_CLICK_NORMAL : SoundEngineComponent::SOUND_CLICK_LIGHT);
	}
}

void MenuPanel_GameRules::HandleBackButtonReleased (SD::ButtonComponent* uiComponent)
{
	MainMenu* owningMenu = GetOwningMenu();
	CHECK(owningMenu != nullptr)
	owningMenu->NavigateBack();
}

void MenuPanel_GameRules::HandleNextButtonReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_HEAVY);
	}

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr && NumBots != nullptr)
	localDroneEngine->GetGame()->SetNumBots(SD::Utils::Min(MAX_NUM_BOTS, SD::INT(NumBots->GetContent())));

	localDroneEngine->SetPendingArena(MapSelection.at(SelectedMapIdx).MapName);

	std::vector<SD::DString> gameSettingValues;
	for (SD::TextFieldComponent* gameSetting : GameRulesOptions)
	{
		gameSettingValues.push_back(gameSetting->GetContent());
	}
	localDroneEngine->GetGame()->ApplyGameSettings(gameSettingValues);

	for (size_t i = 0; i < DifficultyButtons.size(); ++i)
	{
		//Assume the disabled button is the selected difficulty
		if (!DifficultyButtons.at(i)->GetEnabled())
		{
			GameRules::EGameDifficulty newDifficulty = GameRules::EGameDifficulty::Unknown;
			switch (i)
			{
				case(0):
					newDifficulty = GameRules::EGameDifficulty::Pacifist;
					break;

				case(1):
					newDifficulty = GameRules::EGameDifficulty::Novice;
					break;

				case(2):
					newDifficulty = GameRules::EGameDifficulty::Easy;
					break;

				case(3):
					newDifficulty = GameRules::EGameDifficulty::Medium;
					break;

				case(4):
					newDifficulty = GameRules::EGameDifficulty::Hard;
					break;
			}

			if (newDifficulty != GameRules::EGameDifficulty::Unknown)
			{
				localDroneEngine->GetGame()->SetGameDifficulty(newDifficulty);
			}

			break;
		}
	}

	localDroneEngine->GetGame()->DestroyGameModifiers();
	for (size_t i = 0; i < Modifiers.size(); ++i)
	{
		if (Modifiers.at(i).Checkbox->IsChecked() && Modifiers.at(i).Modifier->IsAllowed(localDroneEngine->GetGame()))
		{
			localDroneEngine->GetGame()->AddGameModifier(Modifiers.at(i).Modifier);
		}
	}


	MainMenu* owningMenu = GetOwningMenu();
	CHECK(owningMenu != nullptr)
	owningMenu->PushPanel(dynamic_cast<const MenuPanel_Roster*>(MenuPanel_Roster::SStaticClass()->GetDefaultObject()));
}

void MenuPanel_GameRules::HandleTickCheckSectionHeight (SD::FLOAT deltaSec)
{
	if (RulesSectionAbsHeight != RulesSection->ReadCachedAbsSize().Y && RulesSection != nullptr && GameRulesEntity != nullptr && NumBots != nullptr)
	{
		RulesSectionAbsHeight = RulesSection->ReadCachedAbsSize().Y;

		SD::FLOAT spacing = 4.f;
		SD::FLOAT optionHeight = NumBots->ReadCachedAbsSize().Y;
		if (SD::FrameComponent* botFrame = dynamic_cast<SD::FrameComponent*>(NumBots->GetOwner()))
		{
			optionHeight = botFrame->ReadCachedAbsSize().Y;
		}

		//Adjust all labels and fields in this GuiEntity to be equal to the NumBots height.
		//Since NumBots is normalized but GameRules are not.
		
		SD::FLOAT curPosY = spacing;
		for (SD::TextFieldComponent* field : GameRulesOptions)
		{
			//The fields are relative to an owning frame component. Adjust that frame's size instead
			if (SD::FrameComponent* fieldFrame = dynamic_cast<SD::FrameComponent*>(field->GetOwner()))
			{
				fieldFrame->SetPosition(fieldFrame->ReadPosition().X, curPosY);
				fieldFrame->SetSize(fieldFrame->ReadSize().X, optionHeight);
				curPosY += fieldFrame->ReadSize().Y + spacing;
			}
		}

		curPosY = spacing;
		for (SD::LabelComponent* prompt : GameRulesPrompts)
		{
			prompt->SetPosition(prompt->ReadPosition().X, curPosY);
			prompt->SetSize(prompt->ReadSize().X, optionHeight);
			curPosY += prompt->ReadSize().Y + spacing;
		}
	}
}
DD_END