/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Scoreboard_Extinction.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "Extinction.h"
#include "Scoreboard_Extinction.h"
#include "ScoreComponent_Extinction.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Scoreboard_Extinction, DD, Scoreboard)

void Scoreboard_Extinction::InitializeGameLimitLabel ()
{
	Super::InitializeGameLimitLabel();

	if (GameLimitLabel != nullptr)
	{
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr)

		if (Extinction* game = dynamic_cast<Extinction*>(localDroneEngine->GetGame()))
		{
			SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
			CHECK(translator != nullptr)
			SD::DString summary = translator->TranslateText(TXT("ExtinctionSummary"), TRANSLATION_FILE, TranslationSectionName);
			summary.ReplaceInline(TXT("%s"), game->GetMaxWave().ToString(), SD::DString::CC_CaseSensitive);
			GameLimitLabel->SetText(summary);
		}
	}
}

void Scoreboard_Extinction::InitializeHeaderLabel (SD::FrameComponent* headerFrame)
{
	Super::InitializeHeaderLabel(headerFrame);

	if (headerFrame != nullptr)
	{
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		SD::INT characterSize = 16;

		SD::LabelComponent* killsLabel = SD::LabelComponent::CreateObject();
		if (headerFrame->AddComponent(killsLabel))
		{
			killsLabel->SetAutoRefresh(false);
			killsLabel->SetPosition(SD::Vector2(0.6f, 0.f));
			killsLabel->SetSize(SD::Vector2(0.133f, 1.f));
			killsLabel->SetWrapText(false);
			killsLabel->SetClampText(false);
			killsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			killsLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			killsLabel->SetCharacterSize(characterSize);
			killsLabel->SetText(translator->TranslateText(TXT("Kills"), TRANSLATION_FILE, TranslationSectionName));
			killsLabel->SetAutoRefresh(true);
		}

		SD::LabelComponent* deathsLabel = SD::LabelComponent::CreateObject();
		if (headerFrame->AddComponent(deathsLabel))
		{
			deathsLabel->SetAutoRefresh(false);
			deathsLabel->SetPosition(SD::Vector2(0.733f, 0.f));
			deathsLabel->SetSize(SD::Vector2(0.133f, 1.f));
			deathsLabel->SetWrapText(false);
			deathsLabel->SetClampText(false);
			deathsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			deathsLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			deathsLabel->SetCharacterSize(characterSize);
			deathsLabel->SetText(translator->TranslateText(TXT("Deaths"), TRANSLATION_FILE, TranslationSectionName));
			deathsLabel->SetAutoRefresh(true);
		}

		SD::LabelComponent* livesLabel = SD::LabelComponent::CreateObject();
		if (headerFrame->AddComponent(livesLabel))
		{
			livesLabel->SetAutoRefresh(false);
			livesLabel->SetPosition(SD::Vector2(0.866f, 0.f));
			livesLabel->SetSize(SD::Vector2(0.133f, 1.f));
			livesLabel->SetWrapText(false);
			livesLabel->SetClampText(false);
			livesLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			livesLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			livesLabel->SetCharacterSize(characterSize);
			livesLabel->SetText(translator->TranslateText(TXT("Lives"), TRANSLATION_FILE, TranslationSectionName));
			livesLabel->SetAutoRefresh(true);
		}
	}
}

const ScoreComponent* Scoreboard_Extinction::GetScoreComponentClass () const
{
	return dynamic_cast<const ScoreComponent_Extinction*>(ScoreComponent_Extinction::SStaticClass()->GetDefaultObject());
}

DD_END