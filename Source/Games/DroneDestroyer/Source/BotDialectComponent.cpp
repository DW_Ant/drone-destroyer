/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BotDialectComponent.cpp
=====================================================================
*/

#include "BotDialectComponent.h"
#include "DroneDestroyerEngineComponent.h"
#include "Hud.h"
#include "PlayerSoul.h"
#include "Weapon.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, BotDialectComponent, SD, EntityComponent)

void BotDialectComponent::InitProps ()
{
	Super::InitProps();

	LettersPerSecond = 12.f;
}

void BotDialectComponent::BeginObject ()
{
	Super::BeginObject();

	SD::TickComponent* tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, BotDialectComponent, HandleTick, void, SD::FLOAT));
		tick->SetTickInterval(0.1f);
	}
}

void BotDialectComponent::SayGreetingFrom (PlayerSoul* soul, PlayerSoul* humanSoul)
{
	if (SD::ContainerUtils::IsEmpty(Greetings) || humanSoul == nullptr || soul == nullptr)
	{
		return;
	}

	SD::DString msg = Greetings.at(SD::RandomUtils::Rand(SD::INT(Greetings.size())).ToUnsignedInt());
	msg.ReplaceInline(TXT("%p"), humanSoul->ReadPlayerName(), SD::DString::CC_IgnoreCase);
	InsertAuthorName(soul, OUT msg);
	PendingMessages.push_back(SPendingMessage(msg, msg.Length().ToFLOAT() / LettersPerSecond));
}

void BotDialectComponent::SayDeathMessage (PlayerSoul* killed, PlayerSoul* killer, const Weapon* killedBy)
{
	if (killed == nullptr || killer == nullptr || killedBy == nullptr)
	{
		return;
	}

	bool isTaunt;
	if (!killed->GetIsBot())
	{
		//A human is killed. Make sure this is a taunt (since we don't want the human conversing with bots).
		isTaunt = true;
	}
	else if (!killer->GetIsBot())
	{
		//A human is the killer. Make sure it's a death message since we don't want human conversing with bots.
		isTaunt = false;
	}
	else
	{
		//Pick either taunt or death message
		isTaunt = (SD::RandomUtils::fRand() > 0.5f);
	}

	SD::DString msg;
	PlayerSoul* author = nullptr;
	PlayerSoul* strReplacer = nullptr;
	if (isTaunt)
	{
		if (SD::ContainerUtils::IsEmpty(KillMessages))
		{
			return;
		}
		msg = KillMessages.at(SD::RandomUtils::Rand(SD::INT(KillMessages.size())).ToUnsignedInt());
		author = killer;
		strReplacer = killed;
	}
	else
	{
		if (SD::ContainerUtils::IsEmpty(DeathMessages))
		{
			return;
		}
		msg = DeathMessages.at(SD::RandomUtils::Rand(SD::INT(DeathMessages.size())).ToUnsignedInt());
		author = killed;
		strReplacer = killer;
	}

	msg.ReplaceInline(TXT("%w"), killedBy->GetShortName(), SD::DString::CC_IgnoreCase);
	msg.ReplaceInline(TXT("%k"), strReplacer->ReadPlayerName(), SD::DString::CC_IgnoreCase);
	InsertAuthorName(author, OUT msg);

	PendingMessages.push_back(SPendingMessage(msg, (msg.Length().ToFLOAT() / LettersPerSecond)));
}

void BotDialectComponent::SayEliminationMessage (PlayerSoul* killed)
{
	if (killed == nullptr || SD::ContainerUtils::IsEmpty(EliminationMessages))
	{
		return;
	}

	SD::DString msg = EliminationMessages.at(SD::RandomUtils::Rand(SD::INT(EliminationMessages.size())).ToUnsignedInt());
	InsertAuthorName(killed, OUT msg);
	PendingMessages.push_back(SPendingMessage(msg, msg.Length().ToFLOAT() / LettersPerSecond));
}

void BotDialectComponent::AddGreetings (const std::vector<SD::DString>& newGreetings)
{
	Greetings = newGreetings;
}

void BotDialectComponent::AddDeaths (const std::vector<SD::DString>& newDeaths)
{
	DeathMessages = newDeaths;
}

void BotDialectComponent::AddTaunts (const std::vector<SD::DString>& newTaunts)
{
	KillMessages = newTaunts;
}
void BotDialectComponent::AddEliminationMsgs (const std::vector<SD::DString>& newEliminationMsgs)
{
	EliminationMessages = newEliminationMsgs;
}

void BotDialectComponent::InsertAuthorName (PlayerSoul* author, SD::DString& outMsg)
{
	outMsg.Insert(0, author->ReadPlayerName() + TXT(":  "));
}

void BotDialectComponent::HandleTick (SD::FLOAT deltaSec)
{
	size_t i = 0;
	while (i < PendingMessages.size())
	{
		PendingMessages.at(i).TimeRemaining -= deltaSec;
		if (PendingMessages.at(i).TimeRemaining <= 0.f)
		{
			DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
			CHECK(localDroneEngine != nullptr)

			if (localDroneEngine->GetPlayerHud() != nullptr)
			{
				localDroneEngine->GetPlayerHud()->AddMessageFeed(PendingMessages.at(i).Message);
			}
			
			PendingMessages.erase(PendingMessages.begin() + i);
			continue;
		}

		++i;
	}
}

DD_END