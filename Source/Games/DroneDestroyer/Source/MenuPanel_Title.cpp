/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanel_Title.cpp
=====================================================================
*/

#include "MainMenu.h"
#include "MenuPanel_GameSelection.h"
#include "MenuPanel_Title.h"
#include "SoundEngineComponent.h"
#include "SoundMessenger.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, MenuPanel_Title, DD, MenuPanelComponent)

void MenuPanel_Title::InitProps ()
{
	Super::InitProps();

	ConfigKey = TXT("TitleScreen");

	Prompt = nullptr;
}

void MenuPanel_Title::RestorePanel ()
{
	//Noop	
}

void MenuPanel_Title::SavePanelState ()
{
	//Noop
}

void MenuPanel_Title::InitializeComponents ()
{
	if (SoundMessenger* soundMsg = SoundMessenger::GetMessenger())
	{
		soundMsg->PlaySound(true, TXT("Music_MainMenu.ogg"), 1.f);
	}

	SD::FrameComponent* background = SD::FrameComponent::CreateObject();
	if (AddComponent(background))
	{
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		background->SetBorderThickness(0.f);
		background->SetLockedFrame(true);
		if (background->RenderComponent != nullptr)
		{
			background->RenderComponent->ClearSpriteTexture();
			background->RenderComponent->FillColor = SD::Color::BLACK;
		}

		SD::LabelComponent* title = SD::LabelComponent::CreateObject();
		if (background->AddComponent(title))
		{
			title->SetAutoRefresh(false);
			title->SetPosition(SD::Vector2(0.f, 0.2f));
			title->SetSize(SD::Vector2(1.f, 0.2f));
			title->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			title->SetCharacterSize(64);
			title->SetWrapText(false);
			title->SetClampText(false);
			title->SetText(translator->TranslateText(TXT("Title"), TRANSLATION_FILE, TXT("TitleScreen")));
			title->SetAutoRefresh(true);
		}

		Prompt = SD::LabelComponent::CreateObject();
		if (background->AddComponent(Prompt ))
		{
			Prompt->SetAutoRefresh(false);
			Prompt->SetPosition(SD::Vector2(0.f, 0.75f));
			Prompt->SetSize(SD::Vector2(1.f, 0.25f));
			Prompt->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			Prompt->SetCharacterSize(32);
			Prompt->SetWrapText(false);
			Prompt->SetClampText(false);
			Prompt->SetText(translator->TranslateText(TXT("TitlePrompt"), TRANSLATION_FILE, TXT("TitleScreen")));
			Prompt->SetAutoRefresh(true);

			SD::TickComponent* blinker = SD::TickComponent::CreateObject(TICK_GROUP_DD);
			if (Prompt->AddComponent(blinker))
			{
				blinker->SetTickHandler(SDFUNCTION_1PARAM(this, MenuPanel_Title, HandleBlink, void, SD::FLOAT));
				blinker->SetTickInterval(1.f);
			}
		}
		else
		{
			Prompt = nullptr;
		}

		SD::LabelComponent* versionLabel = SD::LabelComponent::CreateObject();
		if (background->AddComponent(versionLabel))
		{
			versionLabel->SetAutoRefresh(false);
			versionLabel->SetPosition(SD::Vector2(0.8f, 0.9f));
			versionLabel->SetSize(SD::Vector2(0.175f, 0.05f));
			versionLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Right);
			versionLabel->SetCharacterSize(16);
			versionLabel->SetWrapText(false);
			versionLabel->SetClampText(false);
			versionLabel->SetText(TXT("v1.0"));
			versionLabel->SetAutoRefresh(true);
		}

#ifdef DEBUG_MODE
		SD::LabelComponent* debugLabel = SD::LabelComponent::CreateObject();
		if (background->AddComponent(debugLabel))
		{
			debugLabel->SetAutoRefresh(false);
			debugLabel->SetPosition(SD::Vector2(0.8f, 0.925f));
			debugLabel->SetSize(SD::Vector2(0.175f, 0.05f));
			debugLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Right);
			debugLabel->SetCharacterSize(16);
			debugLabel->SetWrapText(false);
			debugLabel->SetClampText(false);
			debugLabel->SetText(translator->TranslateText(TXT("DebugBuild"), TRANSLATION_FILE, TXT("TitleScreen")));
			debugLabel->SetAutoRefresh(true);
		}
#endif
	}
}

bool MenuPanel_Title::ExecuteConsumableMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
{
	bool consumeEvent = Super::ExecuteConsumableMouseClick(mouse, sfmlEvent, eventType);

	if (!consumeEvent && eventType == sf::Event::MouseButtonReleased)
	{
		//Make sure this menu is still in the foreground
		MainMenu* owningMenu = GetOwningMenu();
		CHECK(owningMenu != nullptr)
		if (owningMenu->GetCurrentPanel() == this) //If any mouse button
		{
			consumeEvent = true;
			Proceed();
		}
	}

	return consumeEvent;
}

bool MenuPanel_Title::ExecuteConsumableInput (const sf::Event& evnt)
{
	bool consumeEvent = Super::ExecuteConsumableInput(evnt);

	if (!consumeEvent && evnt.type == sf::Event::KeyReleased)
	{
		//Make sure this menu is still in the foreground
		MainMenu* owningMenu = GetOwningMenu();
		CHECK(owningMenu != nullptr)
		if (owningMenu->GetCurrentPanel() == this)
		{
			if (evnt.key.code == sf::Keyboard::Space || evnt.key.code == sf::Keyboard::Return)
			{
				consumeEvent = true;
				Proceed();
			}	
		}
	}

	return consumeEvent;
}

void MenuPanel_Title::Proceed ()
{
	if (SoundMessenger* soundMsg = SoundMessenger::GetMessenger())
	{
		soundMsg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_HEAVY);
	}

	MainMenu* menu = GetOwningMenu();
	CHECK(menu != nullptr)
	menu->PushPanel(dynamic_cast<const MenuPanel_GameSelection*>(MenuPanel_GameSelection::SStaticClass()->GetDefaultObject()));
}

void MenuPanel_Title::HandleBlink (SD::FLOAT deltaSec)
{
	if (Prompt != nullptr)
	{
		Prompt->SetVisibility(!Prompt->IsVisible());
	}
}

DD_END