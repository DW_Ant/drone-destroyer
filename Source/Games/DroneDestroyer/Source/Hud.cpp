/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Hud.cpp
=====================================================================
*/

#include "Hud.h"
#include "HudMessage.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Hud, SD, GuiEntity)

void Hud::InitProps ()
{
	Super::InitProps();

	MessageFeedDisplayTime = 6.f;

	AmmoDisplay = nullptr;
	HealthDisplay = nullptr;
	ViewedSoulDisplay = nullptr;
	PauseFrame = nullptr;
	EndGameFrame = nullptr;
	EndGameLabel = nullptr;
	MessageFeed = nullptr;

	EndGameFadeTime = 8.f;
	EndGameTimeStamp = 0.f;
}

void Hud::ConstructUI ()
{
	Super::ConstructUI();

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	SD::FLOAT margin = 4.f;
	SD::INT fontSize = 48;

	//Ammo is displayed in lower right corner
	SD::FrameComponent* ammoFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(ammoFrame))
	{
		ammoFrame->SetAnchorRight(0.f);
		ammoFrame->SetAnchorBottom(0.f);
		ammoFrame->SetSize(SD::Vector2(0.2f, 0.1f));
		ammoFrame->SetBorderThickness(1.f);
		ammoFrame->SetLockedFrame(true);
		ammoFrame->SetVisibility(false);
		PlayerOnlyComponents.push_back(ammoFrame);
		if (ammoFrame->RenderComponent != nullptr)
		{
			ammoFrame->RenderComponent->ClearSpriteTexture();
			ammoFrame->RenderComponent->FillColor = SD::Color(172, 208, 120, 255);
		}

		SD::PlanarTransformComponent* ammoSpriteTransform = SD::PlanarTransformComponent::CreateObject();
		if (ammoFrame->AddComponent(ammoSpriteTransform))
		{
			ammoSpriteTransform->SetAnchorRight(0.f);
			ammoSpriteTransform->SetAnchorTop(0.f);
			ammoSpriteTransform->SetAnchorBottom(0.f);
			ammoSpriteTransform->SetSize(SD::Vector2(0.3f, 1.f));

			SD::SpriteComponent* ammoSprite = SD::SpriteComponent::CreateObject();
			if (ammoSpriteTransform->AddComponent(ammoSprite))
			{
				ammoSprite->SetSpriteTexture(localTexturePool->FindTexture(TXT("DroneDestroyer.Pickups.Ammo")));
			}
		}

		AmmoDisplay = SD::LabelComponent::CreateObject();
		if (ammoFrame->AddComponent(AmmoDisplay))
		{
			AmmoDisplay->SetAutoRefresh(false);
			AmmoDisplay->SetAnchorLeft(margin);
			AmmoDisplay->SetSize(SD::Vector2(0.6f, 1.f));
			AmmoDisplay->SetText(SD::DString::EmptyString);
			AmmoDisplay->SetWrapText(false);
			AmmoDisplay->SetClampText(false);
			AmmoDisplay->SetAutoSizeHorizontal(false);
			AmmoDisplay->SetAutoSizeVertical(false);
			AmmoDisplay->SetCharacterSize(fontSize);
			AmmoDisplay->SetHorizontalAlignment(SD::LabelComponent::HA_Left);
			AmmoDisplay->SetVerticalAlignment(SD::LabelComponent::VA_Top);
			if (AmmoDisplay->GetRenderComponent() != nullptr)
			{
				AmmoDisplay->GetRenderComponent()->SetFontColor(sf::Color::Black);
			}
			AmmoDisplay->SetAutoRefresh(true);
		}
		else
		{
			AmmoDisplay = nullptr;
		}
	}

	//Health is displayed in lower left corner
	SD::FrameComponent* healthFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(healthFrame))
	{
		healthFrame->SetAnchorLeft(0.f);
		healthFrame->SetAnchorBottom(0.f);
		healthFrame->SetSize(SD::Vector2(0.2f, 0.1f));
		healthFrame->SetBorderThickness(1.f);
		healthFrame->SetLockedFrame(true);
		healthFrame->SetVisibility(false);
		PlayerOnlyComponents.push_back(healthFrame);
		if (healthFrame->RenderComponent != nullptr)
		{
			healthFrame->RenderComponent->ClearSpriteTexture();
			healthFrame->RenderComponent->FillColor = SD::Color(172, 208, 120, 255);
		}

		SD::PlanarTransformComponent* healthSpriteTransform = SD::PlanarTransformComponent::CreateObject();
		if (healthFrame->AddComponent(healthSpriteTransform))
		{
			healthSpriteTransform->SetAnchorLeft(0.f);
			healthSpriteTransform->SetAnchorTop(0.f);
			healthSpriteTransform->SetAnchorBottom(0.f);
			healthSpriteTransform->SetSize(SD::Vector2(0.3f, 1.f));

			SD::SpriteComponent* healthSprite = SD::SpriteComponent::CreateObject();
			if (healthSpriteTransform->AddComponent(healthSprite))
			{
				healthSprite->SetSpriteTexture(localTexturePool->FindTexture(TXT("DroneDestroyer.Pickups.Medkit")));
			}
		}

		HealthDisplay = SD::LabelComponent::CreateObject();
		if (healthFrame->AddComponent(HealthDisplay))
		{
			HealthDisplay->SetAutoRefresh(false);
			HealthDisplay->SetAnchorRight(margin);
			HealthDisplay->SetSize(SD::Vector2(0.6f, 1.f));
			HealthDisplay->SetText(SD::DString::EmptyString);
			HealthDisplay->SetWrapText(false);
			HealthDisplay->SetClampText(false);
			HealthDisplay->SetCharacterSize(fontSize);
			HealthDisplay->SetHorizontalAlignment(SD::LabelComponent::HA_Right);
			HealthDisplay->SetVerticalAlignment(SD::LabelComponent::VA_Top);
			if (HealthDisplay->GetRenderComponent() != nullptr)
			{
				HealthDisplay->GetRenderComponent()->SetFontColor(sf::Color::Black);
			}
			HealthDisplay->SetAutoRefresh(true);
		}
		else
		{
			HealthDisplay = nullptr;
		}
	}

	ViewedSoulDisplay = SD::LabelComponent::CreateObject();
	if (AddComponent(ViewedSoulDisplay))
	{
		ViewedSoulDisplay->SetAutoRefresh(false);
		ViewedSoulDisplay->SetPosition(0.f, 0.7f);
		ViewedSoulDisplay->SetSize(0.f, 0.2f);
		ViewedSoulDisplay->SetAnchorLeft(0.f);
		ViewedSoulDisplay->SetAnchorRight(0.f);
		ViewedSoulDisplay->SetWrapText(false);
		ViewedSoulDisplay->SetClampText(true);
		ViewedSoulDisplay->SetCharacterSize(26);
		ViewedSoulDisplay->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
		ViewedSoulDisplay->SetAutoRefresh(true);
		ViewedSoulDisplay->SetVisibility(false);
		SpectatorOnlyComponents.push_back(ViewedSoulDisplay);
	}
	else
	{
		ViewedSoulDisplay = nullptr;
	}

	PauseFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(PauseFrame))
	{
		PauseFrame->SetPosition(SD::Vector2(0.25f, 0.3f));
		PauseFrame->SetSize(SD::Vector2(0.5f, 0.2f));
		PauseFrame->SetBorderThickness(2.f);
		PauseFrame->SetLockedFrame(true);
		PauseFrame->SetVisibility(false);

		if (PauseFrame->RenderComponent != nullptr)
		{
			PauseFrame->RenderComponent->ClearSpriteTexture();
			PauseFrame->RenderComponent->FillColor = SD::Color(32, 32, 32, 150);
		}

		SD::LabelComponent* pauseLabel = SD::LabelComponent::CreateObject();
		if (PauseFrame->AddComponent(pauseLabel))
		{
			pauseLabel->SetAutoRefresh(false);
			pauseLabel->SetPosition(SD::Vector2::ZeroVector);
			pauseLabel->SetSize(SD::Vector2(1.f, 1.f));
			pauseLabel->SetCharacterSize(40);
			pauseLabel->SetWrapText(false);
			pauseLabel->SetClampText(false);
			pauseLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			pauseLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			
			SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
			CHECK(translator != nullptr)
			pauseLabel->SetText(translator->TranslateText(TXT("Pause"), TRANSLATION_FILE, TXT("Hud")));
			pauseLabel->SetAutoRefresh(true);
		}
	}
	else
	{
		PauseFrame = nullptr;
	}

	MessageFeed = SD::LabelComponent::CreateObject();
	if (AddComponent(MessageFeed))
	{
		MessageFeed->SetAutoRefresh(false);
		MessageFeed->SetPosition(SD::Vector2::ZeroVector);
		MessageFeed->SetSize(SD::Vector2(0.67f, 0.2f));
		MessageFeed->SetCharacterSize(12);
		MessageFeed->SetWrapText(false);
		MessageFeed->SetClampText(true);
		MessageFeed->SetAutoRefresh(true);

		SD::TickComponent* tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
		if (MessageFeed->AddComponent(tick))
		{
			tick->SetTickHandler(SDFUNCTION_1PARAM(this, Hud, HandleTick, void, SD::FLOAT));
		}
	}
	else
	{
		MessageFeed = nullptr;
	}
}

void Hud::AddHudMessage (HudMessage* newMsg)
{
	if (AddComponent(newMsg))
	{
		//Find old message and immediately remove it.
		for (size_t i = 0; i < DisplayedMessages.size(); ++i)
		{
			if (DisplayedMessages.at(i)->StaticClass() == newMsg->StaticClass())
			{
				DisplayedMessages.at(i)->OnExpiration.ClearFunction();
				DisplayedMessages.at(i)->Destroy();
				DisplayedMessages.erase(DisplayedMessages.begin() + i);
				break;
			}
		}

		newMsg->OnExpiration = SDFUNCTION_1PARAM(this, Hud, HandleMsgExpired, void, HudMessage*);
		DisplayedMessages.push_back(newMsg);
	}
}

void Hud::AddMessageFeed (const SD::DString& newMsg)
{
	if (MessageFeed == nullptr)
	{
		return;
	}

	SD::INT maxNumLines = MessageFeed->GetMaxNumLines();
	if (StackedMessages.size() >= maxNumLines)
	{
		//Remove the oldest message to make room
		StackedMessages.erase(StackedMessages.begin());
	}

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	StackedMessages.push_back(SStackMessage(newMsg, localEngine->GetElapsedTime()));

	UpdateMessageFeed();
}

void Hud::SetHudMode (bool playerMode)
{
	for (SD::GuiComponent* gui : PlayerOnlyComponents)
	{
		gui->SetVisibility(playerMode);
	}

	for (SD::GuiComponent* gui : SpectatorOnlyComponents)
	{
		gui->SetVisibility(!playerMode);
	}
}

void Hud::SetPauseMode (bool isPauseMode)
{
	if (PauseFrame != nullptr)
	{
		PauseFrame->SetVisibility(isPauseMode);
	}
}

void Hud::FlashEndGameMessage (const SD::DString& endGameMsg)
{
	if (PauseFrame != nullptr)
	{
		//Won't be needing this anymore. We know the game is paused during the end sequence.
		PauseFrame->Destroy();
		PauseFrame = nullptr;
	}

	if (endGameMsg.IsEmpty())
	{
		return;
	}

	//Remove old instances if this was called multiple times for some reason
	if (EndGameLabel != nullptr)
	{
		EndGameLabel->Destroy();
		EndGameLabel = nullptr;
	}

	if (EndGameFrame != nullptr)
	{
		EndGameFrame->Destroy();
		EndGameFrame = nullptr;
	}

	EndGameFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(EndGameFrame))
	{
		EndGameFrame->SetPosition(SD::Vector2(0.15f, 0.3f));
		EndGameFrame->SetSize(SD::Vector2(0.7f, 0.15f));
		EndGameFrame->SetLockedFrame(true);
		EndGameFrame->SetBorderThickness(8.f);
		if (EndGameFrame->RenderComponent != nullptr)
		{
			EndGameFrame->RenderComponent->ClearSpriteTexture();
			EndGameFrame->RenderComponent->FillColor = SD::Color(128, 128, 32, 255);
			EndGameFrame->RenderComponent->TopBorder = nullptr;
			EndGameFrame->RenderComponent->RightBorder = nullptr;
			EndGameFrame->RenderComponent->BottomBorder = nullptr;
			EndGameFrame->RenderComponent->LeftBorder = nullptr;
			EndGameFrame->RenderComponent->TopRightCorner = nullptr;
			EndGameFrame->RenderComponent->BottomRightCorner = nullptr;
			EndGameFrame->RenderComponent->BottomLeftCorner = nullptr;
			EndGameFrame->RenderComponent->TopLeftCorner = nullptr;
		}

		EndGameLabel = SD::LabelComponent::CreateObject();
		if (EndGameFrame->AddComponent(EndGameLabel))
		{
			EndGameLabel->SetAutoRefresh(false);
			EndGameLabel->SetPosition(SD::Vector2::ZeroVector);
			EndGameLabel->SetSize(SD::Vector2(1.f, 1.f));
			EndGameLabel->SetCharacterSize(28);
			EndGameLabel->SetWrapText(true);
			EndGameLabel->SetClampText(true);
			EndGameLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			EndGameLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			EndGameLabel->SetText(endGameMsg);
			EndGameLabel->SetAutoRefresh(true);
		}

		SD::TickComponent* endGameTick = SD::TickComponent::CreateObject(TICK_GROUP_DD_NO_PAUSE);
		if (EndGameFrame->AddComponent(endGameTick))
		{
			endGameTick->SetTickHandler(SDFUNCTION_1PARAM(this, Hud, HandleEndGameTick, void, SD::FLOAT));
		}
	}
	
	//Remove temporary hud messages
	for (HudMessage* msg : DisplayedMessages)
	{
		msg->OnExpiration.ClearFunction();
		msg->Destroy();
	}
	SD::ContainerUtils::Empty(OUT DisplayedMessages);

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	EndGameTimeStamp = localEngine->GetElapsedTime();
}

void Hud::SetAmmoAmount (SD::INT newAmmo)
{
	if (AmmoDisplay != nullptr)
	{
		AmmoDisplay->SetText(newAmmo.ToString());
	}
}

void Hud::SetHealthAmount (SD::INT newHealth)
{
	if (HealthDisplay != nullptr)
	{
		HealthDisplay->SetText(newHealth.ToString());
	}
}

void Hud::SetViewedPlayer (const SD::DString& nameOfViewedPlayer)
{
	if (ViewedSoulDisplay != nullptr)
	{
		if (!nameOfViewedPlayer.IsEmpty())
		{
			SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
			CHECK(translator != nullptr)

			ViewedSoulDisplay->SetText(SD::DString::CreateFormattedString(translator->TranslateText(TXT("ViewedPlayer"), TRANSLATION_FILE, TXT("HudMessage")), nameOfViewedPlayer));
		}
		else
		{
			ViewedSoulDisplay->SetText(SD::DString::EmptyString);
		}
	}
}

void Hud::UpdateMessageFeed ()
{
	if (MessageFeed == nullptr)
	{
		return;
	}

	SD::DString combinedMsg = SD::DString::EmptyString;

	for (const SStackMessage& msg : StackedMessages)
	{
		combinedMsg += msg.Text + '\n';
	}

	MessageFeed->SetText(combinedMsg);
}

void Hud::HandleTick (SD::FLOAT deltaSec)
{
	if (SD::ContainerUtils::IsEmpty(StackedMessages))
	{
		return;
	}

	bool stackChanged = false;
	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)

	//Check if any message needs to be purged
	size_t i = 0;
	while (i < StackedMessages.size())
	{
		if (localEngine->GetElapsedTime() - StackedMessages.at(i).DisplayedTimestamp >= MessageFeedDisplayTime)
		{
			stackChanged = true;
			StackedMessages.erase(StackedMessages.begin() + i);
			continue;
		}
		else
		{
			break; //The list is sorted by time. If the oldest message is not old enough to expire, assume the rest are fine.
		}

		++i;
	}

	if (stackChanged)
	{
		UpdateMessageFeed();
	}
}

void Hud::HandleNewAmmo (SD::INT newAmmoAmount)
{
	SetAmmoAmount(newAmmoAmount);
}

void Hud::HandleNewHealth (SD::INT newHealthAmount)
{
	SetHealthAmount(newHealthAmount);
}

void Hud::HandleMsgExpired (HudMessage* oldMsg)
{
	for (size_t i = 0; i < DisplayedMessages.size(); ++i)
	{
		if (DisplayedMessages.at(i)->StaticClass() == oldMsg->StaticClass())
		{
			DisplayedMessages.erase(DisplayedMessages.begin() + i);
			break;
		}
	}
}

void Hud::HandleEndGameTick (SD::FLOAT deltaSec)
{
	//Shouldn't be ticking if those components are nullptrs
	CHECK(EndGameLabel != nullptr && EndGameFrame != nullptr)

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)

	SD::FLOAT alpha = (localEngine->GetElapsedTime() - EndGameTimeStamp) / EndGameFadeTime;

	if (alpha >= 1.f)
	{
		EndGameFrame->Destroy(); //Destroys the label subcomponent, too
		EndGameFrame = nullptr;
		EndGameLabel = nullptr;

		if (OnEndGameMsgFadeOut.IsBounded())
		{
			OnEndGameMsgFadeOut();
		}

		return;
	}

	sf::Uint8 newAlpha = SD::Utils::Lerp<sf::Uint8>(alpha.Value, 255, 0);
	if (EndGameFrame->RenderComponent != nullptr)
	{
		EndGameFrame->RenderComponent->FillColor.Source.a = newAlpha;
	}

	if (EndGameLabel->GetRenderComponent() != nullptr)
	{
		EndGameLabel->GetRenderComponent()->SetFontColor(sf::Color(255, 255, 255, newAlpha));
	}
}

DD_END