/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LastManStanding.cpp
=====================================================================
*/

#include "Hud.h"
#include "HudMessage.h"
#include "HudLastManStanding.h"
#include "LastManStanding.h"
#include "Msg_Eliminated.h"
#include "PlayerSoul.h"
#include "Scoreboard_LastManStanding.h"
#include "TimeLimitComponent.h"
#include "TimeLimitHudComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, LastManStanding, DD, GameRules)

void LastManStanding::InitProps ()
{
	Super::InitProps();

	NumLives = 10;
	TimeLimit = -1.f;
	TimeComponent = nullptr;

	IconTextureName = TXT("DroneDestroyer.Interface.LastManStanding");
	DescriptionKey = TXT("LastManStanding");
	ConfigName = TXT("LastManStanding");
	ForceRespawn = true; //Can't camp by staying dead

	GameSettings.push_back(SGameRulesOption(TXT("NumLives"), 10));
	GameSettings.push_back(SGameRulesOption(TXT("TimeLimit"), 5));
}

bool LastManStanding::CanBeSelected () const
{
	return true;
}

void LastManStanding::ApplyGameSettings (const std::vector<SD::DString>& newSettings)
{
	CHECK(newSettings.size() == 2) //LastManStanding is expecting 2 settings
	NumLives = std::atoi(newSettings.at(0).ToCString());
	TimeLimit = std::stof(newSettings.at(1).ToCString()) * 60.f;
}

void LastManStanding::InitializeGame ()
{
	Super::InitializeGame();

	TimeComponent = TimeLimitComponent::CreateObject();
	if (AddComponent(TimeComponent))
	{
		TimeComponent->OnExpired = SDFUNCTION(this, LastManStanding, HandleTimeLimitExpired, void);
		TimeComponent->SetTimeLimit(TimeLimit);
	}
	else
	{
		TimeComponent = nullptr;
	}

	//Set the lives remaining for all players
	for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
	{
		if (soul->GetIsSpectator())
		{
			soul->SetIsEliminated(true);
			soul->SetLivesRemaining(-1);
		}
		else
		{
			soul->SetLivesRemaining(NumLives - 1); //The 0th life is not counted here.
			soul->SetIsEliminated(false);
			soul->SetTeamNumber(-1); //Set this character to be hostile towards everyone
		}
	}

	UpdateScores();
}

void LastManStanding::AddComponentsToHud (Hud* playerHud)
{
	Super::AddComponentsToHud(playerHud);

	if (playerHud != nullptr)
	{
		if (TimeComponent != nullptr)
		{
			TimeLimitHudComponent* timeHud = TimeLimitHudComponent::CreateObject();
			if (playerHud->AddComponent(timeHud))
			{
				timeHud->BindClockTo(TimeComponent);
			}
		}

		HudScore = HudLastManStanding::CreateObject();
		if (playerHud->AddComponent(HudScore.Get()))
		{
			//Place above the health
			HudScore->SetPosition(SD::Vector2(0.f, 0.8f));
			HudScore->SetSize(SD::Vector2(0.2f, 0.1f));
			HudScore->SetVisibility(false); //Hides from spectators and start of the match. This will become visible as soon as there's an update.
		}
	}
}

void LastManStanding::ScoreKill (Character* killed, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg)
{
	if (killed != nullptr && killed->GetSoul() != nullptr)
	{
		if (killed->GetSoul()->GetLivesRemaining() <= 0)
		{
			//That was their last life. Eliminate them
			killed->GetSoul()->SetIsEliminated(true);
			killed->GetSoul()->DecrementLivesCount(); //decrement anyways for sorting purposes. Eliminated players should be below players with 0 lives remaining.
			HudMessage::CreateAndRegisterMsgToHud(Msg_Eliminated::SStaticClass(), killed->GetSoul()->GetPlayerName());
		}
		else
		{
			killed->GetSoul()->DecrementLivesCount();
		}
	}

	//Called before Super in order to UpdateScores. Better approach to this would be to implement a pub sub system.
	Super::ScoreKill(killed, killer, killedBy, deathMsg);
}

void LastManStanding::UpdateScores ()
{
	Super::UpdateScores();

	if (HudScore != nullptr)
	{
		HudScore->UpdateScores(ReadRankedPlayers());
	}

	CheckEndGame();
}

void LastManStanding::SortScores ()
{
	Super::SortScores();

	std::sort(RankedPlayers.begin(), RankedPlayers.end(), [](const SD::DPointer<PlayerSoul>& a, const SD::DPointer<PlayerSoul>& b)
	{
		if (a->GetLivesRemaining() != b->GetLivesRemaining())
		{
			return (a->GetLivesRemaining() > b->GetLivesRemaining());
		}
		
		//Otherwise sort by number of kills
		return (a->GetNumKills() > b->GetNumKills());
	});
}

const Scoreboard* LastManStanding::GetScoreboardClass () const
{
	return dynamic_cast<const Scoreboard_LastManStanding*>(Scoreboard_LastManStanding::SStaticClass()->GetDefaultObject());
}

bool LastManStanding::CanCreateBody (PlayerSoul* requester)
{
	if (!Super::CanCreateBody(requester))
	{
		return false;
	}

	return (requester != nullptr && !requester->GetIsEliminated());
}

void LastManStanding::SuddenDeath ()
{
	SD::INT highestLives = 0;

	//Figure out which player(s) has the highest number of lives
	for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
	{
		highestLives = SD::Utils::Max(soul->GetLivesRemaining(), highestLives);
	}

	//Iterate again. This time, anyone that doesn't reach the lives limit are eliminated.
	for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
	{
		if (soul->GetIsEliminated())
		{
			//Already out. Ignore.
			continue;
		}

		SD::INT oldLives = soul->GetLivesRemaining();
		soul->SetLivesRemaining(0);
		if (oldLives < highestLives)
		{
			if (soul->GetBody() != nullptr && soul->GetBody()->IsAlive())
			{
				soul->GetBody()->Kill(999, nullptr, nullptr, TXT("SuddenDeath"));
			}
			else
			{
				soul->SetIsEliminated(true);
				HudMessage::CreateAndRegisterMsgToHud(Msg_Eliminated::SStaticClass(), soul->GetPlayerName());
			}
		}
	}

	UpdateScores();
}

void LastManStanding::CheckEndGame ()
{
	if (HasEnded)
	{
		//No need to check again. The game has ended already.
		return;
	}

	PlayerSoul* survivor = nullptr;
	for (PlayerSoul* soul = FirstPlayer; soul != nullptr; soul = soul->GetNextSoul())
	{
		if (soul->GetIsEliminated() || soul->GetIsSpectator())
		{
			continue;
		}

		if (survivor != nullptr)
		{
			//There are at least two players alive. Do nothing more.
			return;
		}

		survivor = soul;
	}

	//At this point there is either a draw or we have a winner
	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	if (survivor != nullptr)
	{
		SD::Vector3 camLocation(SD::Vector3::ZeroVector);
		if (survivor->GetBody() != nullptr)
		{
			camLocation = survivor->GetBody()->GetAbsTranslation();
		}

		SD::DString endMsg = translator->TranslateText(TXT("EndGame"), TRANSLATION_FILE, TXT("LastManStanding"));
		endMsg.ReplaceInline(TXT("%s"), survivor->ReadPlayerName(), SD::DString::CC_CaseSensitive);
		ExecuteVictorySequence(endMsg, camLocation);
	}
	else
	{
		//Draw game
		ExecuteVictorySequence(translator->TranslateText(TXT("DrawGame"), TRANSLATION_FILE, TXT("LastManStanding")), SD::Vector3::ZeroVector);
	}
}

void LastManStanding::HandleTimeLimitExpired ()
{
	SuddenDeath();
}

DD_END