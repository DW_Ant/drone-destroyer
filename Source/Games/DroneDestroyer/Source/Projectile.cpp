/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Projectile.cpp
=====================================================================
*/

#include "Drone.h"
#include "DroneDestroyerEngineComponent.h"
#include "Character.h"
#include "Extinction.h"
#include "HealthComponent.h"
#include "PlayerSoul.h"
#include "Projectile.h"
#include "ProjectilePhysicsComponent.h"

DD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(DD, Projectile, SD, Entity)

const uint16 Projectile::COLLISION_CATEGORY = 0x00000004;

void Projectile::InitProps ()
{
	Super::InitProps();

	Damage = 10;
	LifeSpan = 5.f;
	CollisionRadius = 10.f;
	Shooter = nullptr;
	VelocityMagnitude = 100.f;
	AllowContinuousCollision = true;
	ImpactSound = TXT("Impact_Bullet");
	ImpactSoundVolume = 0.25f;
	DeathMessageKey = TXT("Unknown");
	DamageDronesOnly = false;
	Physics = nullptr;
}

void Projectile::BeginObject ()
{
	Super::BeginObject();

	InitializeRenderComponents();

	if (LifeSpan > 0.f)
	{
		SD::LifeSpanComponent* lifeSpan = SD::LifeSpanComponent::CreateObject();
		if (AddComponent(lifeSpan))
		{
			lifeSpan->LifeSpan = LifeSpan;
		}
	}
}

void Projectile::InitializeExtinctionPhysics (bool damageDrones)
{
	DamageDronesOnly = damageDrones;

	SD::TickComponent* tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(tick))
	{
		tick->SetTickHandler(SDFUNCTION_1PARAM(this, Projectile, HandleCheapPhysicsTick, void, SD::FLOAT));
	}

	//Since there's no such thing as cover with these kinds of physics, set the life span so that all projectiles can cover up to one screen radius
	//to prevent drones and players from shooting targets beyond the screen.
	SD::FLOAT newDuration = 2100.f / VelocityMagnitude;
	SD::LifeSpanComponent* lifeSpan = dynamic_cast<SD::LifeSpanComponent*>(FindSubComponent(SD::LifeSpanComponent::SStaticClass(), false));
	if (lifeSpan != nullptr)
	{
		lifeSpan->LifeSpan = newDuration;
	}
}

void Projectile::InitializePhysics ()
{
	SD::PhysicsComponent::SComponentInitData initData;
	initData.BodyInit.PhysicsType = SD::PhysicsComponent::PT_Dynamic;
	initData.BodyInit.EnableContinuousCollision = AllowContinuousCollision;
	initData.BodyInit.IsFixedRotation = true;

	b2CircleShape circle;
	circle.m_radius = SD::Box2dUtils::ToBox2dDist(CollisionRadius);
	initData.CollisionShape = &circle;
	initData.Density = 1.f;
	initData.IsBlockingComponent = true;
	initData.CollisionCategories = COLLISION_CATEGORY & SD::PhysicsComponent::COLLISION_FLAG_ENTITY;
	initData.CollisionMasks = COLLISION_CATEGORY; //Do not collide with other projectiles.
	initData.CollisionGroup = 1;
	Physics = SD::PhysicsComponent::CreateAndInitializeComponent<ProjectilePhysicsComponent>(this, initData);

	CHECK(Physics != nullptr)
	Physics->OnCollision = SDFUNCTION_3PARAM(this, Projectile, HandleCollision, void, SD::PhysicsComponent*, b2Contact*, const b2ContactImpulse*);

	SD::Vector3 velocity = ReadRotation().GetDirectionalVector();
	velocity *= VelocityMagnitude;
	Physics->SetVelocity(velocity);

#ifdef DEBUG_MODE
	bool debugPhysics = false;
	if (debugPhysics)
	{
		SD::PhysicsRenderer* renderComp = SD::PhysicsRenderer::CreateObject();
		if (AddComponent(renderComp))
		{
			renderComp->FillColor = SD::Color(255, 0, 0, DroneDestroyerEngineComponent::DebugPhysicsRenderAlpha);
			DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(renderComp);
		}
	}
#endif
}

void Projectile::SetShooter (PlayerSoul* newShooter)
{
	Shooter = newShooter;
}

void Projectile::SetWeap (const Weapon* newWeap)
{
	Weap = newWeap;
}

void Projectile::ProcessCheapPhysics (SD::FLOAT deltaSec)
{
	//Process Velocity
	SD::Vector3 velocity = ReadRotation().GetDirectionalVector() * VelocityMagnitude;
	EditTranslation() = ReadTranslation() + (velocity * deltaSec);

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	if (localDroneEngine->GetGame() == nullptr)
	{
		//Must have exited the game. Still continue forward but don't collide with anything. Let lifespan component purge this.
		return;
	}

	//Process collision
	if (DamageDronesOnly)
	{
		if (Extinction* game = dynamic_cast<Extinction*>(localDroneEngine->GetGame()))
		{
			for (Drone* drone : game->ReadDrones())
			{
				SD::FLOAT distSquared = (drone->ReadTranslation() - ReadTranslation()).CalcDistSquared(false);
				if (distSquared < drone->GetCollisionRadius() * drone->GetCollisionRadius())
				{
					DoDamage(drone->GetHealth());
					Explode();
					return;
				}
			}
		}
	}
	else
	{
		for (PlayerSoul* soul = localDroneEngine->GetGame()->GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
		{
			if (Character* body = soul->GetBody())
			{
				SD::FLOAT distSquared = (body->ReadTranslation() - ReadTranslation()).CalcDistSquared(false);
				if (distSquared < body->GetCollisionRadius() * body->GetCollisionRadius())
				{
					DoDamage(body->GetHealth());
					Explode();
					return;
				}
			}
		}
	}
}

void Projectile::DoDamage (HealthComponent* victim)
{
	if (victim != nullptr)
	{
		victim->TakeDamage(Damage, Shooter.Get(), this);
	}
}

void Projectile::Explode ()
{
	if (!ImpactSound.IsEmpty())
	{
		DroneDestroyerEngineComponent::SPlayLocalSound(ImpactSound, ReadAbsTranslation(), ImpactSoundVolume);
	}

	Destroy();
}

void Projectile::HandleCollision (SD::PhysicsComponent* otherComp, b2Contact* contactData, const b2ContactImpulse* impulse)
{
	if (otherComp != nullptr)
	{
		SD::Entity* otherOwner = otherComp->GetRootEntity();
		if (otherOwner != nullptr)
		{
			HealthComponent* otherHealth = dynamic_cast<HealthComponent*>(otherOwner->FindSubComponent(HealthComponent::SStaticClass(), true));
			if (otherHealth != nullptr)
			{
				DoDamage(otherHealth);
			}
		}
	}

	Explode();
}

void Projectile::HandleCheapPhysicsTick (SD::FLOAT deltaSec)
{
	ProcessCheapPhysics(deltaSec);
}

DD_END