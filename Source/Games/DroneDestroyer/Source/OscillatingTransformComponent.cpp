/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  OscillatingTransformComponent.cpp
=====================================================================
*/

#include "OscillatingTransformComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, OscillatingTransformComponent, SD, SceneTransformComponent)

void OscillatingTransformComponent::InitProps ()
{
	Super::InitProps();

	PrimaryPosition = SD::Vector3::ZeroVector;
	SecondaryPosition = SD::Vector3::ZeroVector;

	CycleTime = 1.f;
	Tick = nullptr;

	RandomTimeOffset = 0.f;
}

void OscillatingTransformComponent::BeginObject ()
{
	Super::BeginObject();

	RandomTimeOffset = SD::RandomUtils::RandRange(0.f, 60.f);

	Tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, OscillatingTransformComponent, HandleTick, void, SD::FLOAT));
	}
	else
	{
		Tick = nullptr;
	}
}

void OscillatingTransformComponent::SetOscillatingActive (bool isActive)
{
	if (Tick != nullptr)
	{
		Tick->SetTicking(isActive);
	}
}

void OscillatingTransformComponent::SetCycleTime (SD::FLOAT newCycleTime)
{
	if (newCycleTime > 0.f)
	{
		CycleTime = newCycleTime;
	}
}

void OscillatingTransformComponent::CalcNewTranslation ()
{
	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr);

	/*
	Formula: Need a value that ranges from 0 to 1 within CycleTime seconds.
	(elapsedTime * PI_FLOAT * 2) - This gets the global time in the infinite wave. And 2pi compresses the wave so that 1 wave length ranges from 0 to 1.
	(divide by CycleTime) - This stretches the wave length so that it'll complete one cycle within that time.
	(sin) - Computes the vertical point in the sin wave graph.
	(multiply by 0.5) - Sine waves generate values from -1 to 1. We need a value from 0 to 1. This essentally squishes the wave height to -0.5 to 0.5.
	(add by 0.5) - displaces it from -0.5 to 0.5 to now 0 to 1.
	*/
	SD::FLOAT alpha = (std::sin((((RandomTimeOffset + localEngine->GetElapsedTime()) * PI_FLOAT * 2.f) / CycleTime).Value) * 0.5f) + 0.5f;

	SetTranslation(SD::Vector3::Lerp(alpha, PrimaryPosition, SecondaryPosition));
}

void OscillatingTransformComponent::HandleTick (SD::FLOAT deltaSec)
{
	CalcNewTranslation();
}

DD_END