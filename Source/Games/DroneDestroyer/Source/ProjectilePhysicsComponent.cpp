/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ProjectilePhysicsComponent.cpp
=====================================================================
*/

#include "Character.h"
#include "ProjectilePhysicsComponent.h"
#include "Projectile.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, ProjectilePhysicsComponent, SD, PhysicsComponent)

bool ProjectilePhysicsComponent::CanBlockComponent (PhysicsComponent* other, b2Contact* contact, const b2Manifold* oldManifold) const
{
	if (!Super::CanBlockComponent(other, contact, oldManifold) || other == nullptr)
	{
		return false;
	}

	return CanCollideAgainst(other->GetRootEntity());
}

bool ProjectilePhysicsComponent::CanBlockComponent (PhysicsComponent* other) const
{
	if (!Super::CanBlockComponent(other) || other == nullptr)
	{
		return false;
	}

	return CanCollideAgainst(other->GetRootEntity());
}

bool ProjectilePhysicsComponent::CanCollideAgainst (SD::Entity* rootEntity) const
{
	if (dynamic_cast<Projectile*>(rootEntity) != nullptr)
	{
		//Can't collide against other projectiles
		return false;
	}

	//Ensure the shooter cannot shoot themselves.
	if (Character* victimCharacter = dynamic_cast<Character*>(rootEntity))
	{
		if (GetRootEntity() == nullptr)
		{
			return false; //Must be a projectile that's about to be destroyed.
		}

		const Projectile* owningProjectile = dynamic_cast<const Projectile*>(GetRootEntity());
		if (owningProjectile == nullptr)
		{
#ifdef DEBUG_MODE
			DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("A ProjectilePhysicsComponent is attached to an Entity that is not a Projectile. It's attached to %s instead."), GetRootEntity()->DebugName);
#endif
			return true;
		}

		if (owningProjectile->GetShooter() == victimCharacter->GetSoul())
		{
			return false;
		}
	}

	return true;
}

DD_END