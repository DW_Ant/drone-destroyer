/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScoreComponent.cpp
=====================================================================
*/

#include "DroneDestroyerTheme.h"
#include "PlayerSoul.h"
#include "ScoreComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, ScoreComponent, SD, GuiComponent)

void ScoreComponent::InitProps ()
{
	Super::InitProps();

	FontSize = 24;
	Frame = nullptr;
	RankLabel = nullptr;
	PlayerNameLabel = nullptr;
}

void ScoreComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	Frame = SD::FrameComponent::CreateObject();
	if (AddComponent(Frame))
	{
		Frame->ApplyStyle(DroneDestroyerTheme::SCOREBOARD_STYLE_NAME);

		RankLabel = SD::LabelComponent::CreateObject();
		if (Frame->AddComponent(RankLabel))
		{
			RankLabel->SetAutoRefresh(false);
			RankLabel->SetPosition(SD::Vector2(0.f, 0.f));
			RankLabel->SetSize(SD::Vector2(0.1f, 1.f));
			RankLabel->SetWrapText(false);
			RankLabel->SetClampText(false);
			RankLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			RankLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			RankLabel->SetCharacterSize(FontSize);
			RankLabel->SetAutoRefresh(true);
		}
		else
		{
			RankLabel = nullptr;
		}

		PlayerNameLabel = SD::LabelComponent::CreateObject();
		if (Frame->AddComponent(PlayerNameLabel))
		{
			PlayerNameLabel->SetAutoRefresh(false);
			PlayerNameLabel->SetPosition(SD::Vector2(0.1f, 0.f));
			PlayerNameLabel->SetSize(SD::Vector2(0.4f, 1.f));
			PlayerNameLabel->SetWrapText(false);
			PlayerNameLabel->SetClampText(true);
			PlayerNameLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			PlayerNameLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			PlayerNameLabel->SetCharacterSize(FontSize);
			PlayerNameLabel->SetAutoRefresh(true);
		}
		else
		{
			PlayerNameLabel = nullptr;
		}
	}
	else
	{
		Frame = nullptr;
	}
}

void ScoreComponent::UpdateInformation (SD::INT rank, PlayerSoul* soul)
{
	CHECK(soul != nullptr)

	if (Frame != nullptr && Frame->RenderComponent != nullptr)
	{
		Frame->RenderComponent->FillColor = GetFrameColor(rank, soul);
	}

	if (RankLabel != nullptr)
	{
		RankLabel->SetText(rank.ToString());
	}

	if (PlayerNameLabel != nullptr)
	{
		if (PlayerNameLabel->ReadCachedAbsSize().X > 0.f && PlayerNameLabel->GetFont()->CalculateStringWidth(soul->ReadPlayerName(), FontSize) > PlayerNameLabel->ReadCachedAbsSize().X)
		{
			//Make the text smaller to fit more characters for this long name.
			PlayerNameLabel->SetCharacterSize(FontSize / 2);
		}
		else
		{
			PlayerNameLabel->SetCharacterSize(FontSize);
		}

		PlayerNameLabel->SetText(soul->ReadPlayerName());
	}
}

void ScoreComponent::SetFontSize (SD::INT newFontSize)
{
	FontSize = newFontSize;

	if (RankLabel != nullptr)
	{
		RankLabel->SetCharacterSize(FontSize);
	}

	if (PlayerNameLabel != nullptr)
	{
		PlayerNameLabel->SetCharacterSize(FontSize);
	}
}

SD::Color ScoreComponent::GetFrameColor (SD::INT rank, PlayerSoul* soul)
{
	//Give special colors if it's the player
	if (!soul->GetIsBot())
	{
		return SD::Color(150, 150, 32, 128);
	}

	//Alternate colors of varying dark shades of grey
	return (rank.IsEven()) ? SD::Color(64, 64, 64, 128) : SD::Color(32, 32, 32, 128);
}
DD_END