/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HealthPickup.cpp
=====================================================================
*/

#include "Character.h"
#include "HealthComponent.h"
#include "HealthPickup.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, HealthPickup, DD, Inventory)

void HealthPickup::InitProps ()
{
	Super::InitProps();

#ifdef DEBUG_MODE
	DebugName = TXT("HealthPickup");
#endif

	ItemNameKey = TXT("Health");
	PickupImage = TXT("DroneDestroyer.Pickups.Medkit");

	DefaultHealAmount = 25;
	HealAmount = DefaultHealAmount;
}

bool HealthPickup::CanPickupItem (Character* takerCandidate) const
{
	//Must be wounded
	if (!Super::CanPickupItem(takerCandidate))
	{
		return false;
	}

	if (takerCandidate != nullptr && takerCandidate->GetHealth() != nullptr)
	{
		return takerCandidate->GetHealth()->GetHealth() < takerCandidate->GetHealth()->GetMaxHealth();
	}

	return false;
}

SD::INT HealthPickup::GetImportanceValue (Character* possibleTaker) const
{
	CHECK(possibleTaker != nullptr)

	HealthComponent* healthComp = possibleTaker->GetHealth();

	SD::FLOAT percentHealth = healthComp->GetHealth().ToFLOAT() / healthComp->GetMaxHealth().ToFLOAT();

	percentHealth *= (HealAmount.ToFLOAT() / DefaultHealAmount.ToFLOAT());

	//Pick a value ranging from 2 to 75 based on the percentFull where full healing yields 2, nearly dead yields 75.
	//Nearly dead bots will prioritize healing over ammo.
	return SD::Utils::Lerp<SD::INT, SD::FLOAT>(percentHealth, 75, 2);
}

void HealthPickup::GiveTo (Character* taker)
{
	Super::GiveTo(taker);

	CHECK(taker != nullptr)
	HealthComponent* healthComp = taker->GetHealth();
	healthComp->SetHealth(healthComp->GetHealth() + HealAmount);

	Destroy();
}

void HealthPickup::SetHealAmount (SD::INT newHealAmount)
{
	HealAmount = SD::Utils::Clamp<SD::INT>(newHealAmount, 1, 100);
}
DD_END