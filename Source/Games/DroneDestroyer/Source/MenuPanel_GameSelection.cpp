/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanel_GameSelection.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "GameRules.h"
#include "MainMenu.h"
#include "MenuPanel_Credits.h"
#include "MenuPanel_GameRules.h"
#include "MenuPanel_GameSelection.h"
#include "MenuPanel_UserSettings.h"
#include "SoundEngineComponent.h"
#include "SoundMessenger.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, MenuPanel_GameSelection, DD, MenuPanelComponent)

void MenuPanel_GameSelection::InitProps ()
{
	Super::InitProps();

	ConfigKey = TXT("GameSelection");

	BrowseToGameTime = 0.5f;

	SelectedGame = 0;
	SelectedGameSize = SD::Vector2(0.25f, 0.75f);
	SelectedGamePosition = SD::Vector2(0.5f, 0.5f) - (SelectedGameSize * 0.5f);
	LeftFadeOutSize = SD::Vector2::ZeroVector;
	LeftFadeOutPosition = SD::Vector2(0.f, 0.5f);
	PrevGameSize = SD::Vector2(0.125f, 0.25f);
	PrevGamePosition = SD::Vector2(0.25f, 0.5f) - (PrevGameSize * 0.5f);
	NextGameSize = PrevGameSize;
	NextGamePosition = SD::Vector2(0.75f, 0.5f) - (NextGameSize * 0.5f);
	RightFadeOutSize = SD::Vector2::ZeroVector;
	RightFadeOutPosition = SD::Vector2(1.f, 0.5f);
	
	NextGame = nullptr;
	PrevGame = nullptr;
	GameDescriptionFrame = nullptr;
	GameDescriptionLabel = nullptr;
	GameDescriptionEntity = nullptr;
	Tick = nullptr;

	DesiredSelectedGame = 0;
	IsPastHalfway = false;
	IsMovingRight = false;
	GameSelectedTime = -1.f;
}

void MenuPanel_GameSelection::RestorePanel ()
{
#if 0
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(ConfigFile, false))
	{
		config->Destroy();
		return;
	}

	SD::INT prevSelectedGame = config->GetProperty<SD::INT>(ConfigKey, TXT("SelectedGame"));
	if (SD::ContainerUtils::IsValidIndex(Games, prevSelectedGame))
	{
		BrowseToGame(prevSelectedGame.ToUnsignedInt());
	}

	config->Destroy();
#endif
}

void MenuPanel_GameSelection::SavePanelState ()
{
#if 0
	if (!SD::ContainerUtils::IsValidIndex(Games, SelectedGame))
	{
		return;
	}

	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(ConfigFile, false))
	{
		config->Destroy();
		return;
	}

	config->SaveProperty<SD::INT>(ConfigKey, TXT("SelectedGame"), SelectedGame);
	config->Destroy();
#endif
}

void MenuPanel_GameSelection::InitializeComponents ()
{
	Super::InitializeComponents();

	InitializeGameSelection();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	SD::Vector2 drawPosition(SD::Vector2::ZeroVector);

	SD::FrameComponent* gameSelectionFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(gameSelectionFrame))
	{
		gameSelectionFrame->SetPosition(SD::Vector2(0.15f, 0.05f));
		gameSelectionFrame->SetSize(SD::Vector2(1.f - (gameSelectionFrame->ReadPosition().X * 2.f), 0.35f));
		drawPosition.Y += gameSelectionFrame->ReadSize().Y + gameSelectionFrame->ReadPosition().Y;

		gameSelectionFrame->SetLockedFrame(true);
		gameSelectionFrame->SetBorderThickness(2.f);
		if (gameSelectionFrame->RenderComponent != nullptr)
		{
			gameSelectionFrame->RenderComponent->ClearSpriteTexture();
			gameSelectionFrame->RenderComponent->FillColor = SD::Color(211, 122, 110, 64);
		}

		//This tick component is responsible for panning the gametype buttons left/right.
		Tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
		if (gameSelectionFrame->AddComponent(Tick))
		{
			Tick->SetTicking(false);
			Tick->SetTickHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameSelection, HandleBrowseGameTick, void, SD::FLOAT));
		}
		else
		{
			Tick = nullptr;
		}

		//Initialize each game component. At this point, every game button is created, but we need to attach them, set their positions, and set their visibility.
		for (size_t i = 0; i < Games.size(); ++i)
		{
			if (gameSelectionFrame->AddComponent(Games.at(i).Button))
			{
				if (i == SelectedGame)
				{
					//Front and center
					Games.at(i).Button->SetPosition(SelectedGamePosition);
					Games.at(i).Button->SetSize(SelectedGameSize);
					Games.at(i).Button->SetVisibility(true);
				}
				else if (i == (SelectedGame+1) % Games.size())
				{
					//Next button
					Games.at(i).Button->SetPosition(NextGamePosition);
					Games.at(i).Button->SetSize(NextGameSize);
					Games.at(i).Button->SetVisibility(true);
				}
				else if (SelectedGame == 0 && i == (Games.size() - 1))
				{
					//Previous button
					Games.at(i).Button->SetPosition(PrevGamePosition);
					Games.at(i).Button->SetSize(PrevGameSize);
					Games.at(i).Button->SetVisibility(true);
				}
				else if (i == (SelectedGame-1))
				{
					//Previous button
					Games.at(i).Button->SetPosition(PrevGamePosition);
					Games.at(i).Button->SetSize(PrevGameSize);
					Games.at(i).Button->SetVisibility(true);
				}
				else
				{
					Games.at(i).Button->SetVisibility(false);
				}

				Games.at(i).Button->ReplaceStateComponent(SD::ButtonStateComponent::SStaticClass()); //Don't change appearances when hovered or clicked.
				Games.at(i).Button->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameSelection, HandleGameButtonReleased, void, SD::ButtonComponent*));
				Games.at(i).Button->CaptionComponent->Destroy();
				if (Games.at(i).Button->GetBackground() != nullptr && Games.at(i).Button->GetBackground()->RenderComponent != nullptr)
				{
					Games.at(i).Button->GetBackground()->RenderComponent->SetDrawCoordinates(0, 0, 256, 1024);
					Games.at(i).Button->GetBackground()->RenderComponent->SetSpriteTexture(localTexturePool->FindTexture(Games.at(i).Gametype->ReadIconTextureName()));
				}
			}
		}

		if (Games.size() > 1)
		{
			SD::FLOAT buttonMargin = 0.05f;
			SD::FLOAT buttonSize = 0.1f;
			PrevGame = SD::ButtonComponent::CreateObject();
			if (gameSelectionFrame->AddComponent(PrevGame))
			{
				PrevGame->SetPosition(SD::Vector2(buttonMargin, 0.5f - (buttonSize * 0.5f)));
				PrevGame->SetSize(SD::Vector2(buttonSize, buttonSize));
				PrevGame->SetCaptionText(TXT("<"));
				PrevGame->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameSelection, HandleNextButtonReleased, void, SD::ButtonComponent*));
			}
			else
			{
				PrevGame = nullptr;
			}

			NextGame = SD::ButtonComponent::CreateObject();
			if (gameSelectionFrame->AddComponent(NextGame))
			{
				NextGame->SetPosition(SD::Vector2(1.f - buttonMargin - buttonSize, 0.5f - (buttonSize * 0.5f)));
				NextGame->SetSize(SD::Vector2(buttonSize, buttonSize));
				NextGame->SetCaptionText(TXT(">"));
				NextGame->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameSelection, HandlePrevButtonReleased, void, SD::ButtonComponent*));
			}
			else
			{
				NextGame = nullptr;
			}
		}
	}

	GameDescriptionFrame = SD::ScrollbarComponent::CreateObject();
	if (AddComponent(GameDescriptionFrame))
	{
		drawPosition.Y += 0.05f;
		GameDescriptionFrame->SetSize(SD::Vector2(0.4f, 0.15f));
		GameDescriptionFrame->SetPosition(SD::Vector2(0.5f - (GameDescriptionFrame->ReadSize().X * 0.5f), drawPosition.Y));
		drawPosition.Y += GameDescriptionFrame->ReadSize().Y;
		GameDescriptionFrame->SetHideControlsWhenFull(true);
		GameDescriptionFrame->SetZoomEnabled(false);

		SD::LabelComponent::CreateLabelWithinScrollbar(GameDescriptionFrame, OUT GameDescriptionEntity, OUT GameDescriptionLabel);
		if (GameDescriptionLabel != nullptr)
		{
			GameDescriptionLabel->SetText(Games.at(SelectedGame).Gametype->ReadDescription());
		}
	}
	else
	{
		GameDescriptionFrame = nullptr;
	}

	SD::FLOAT buttonSpacing = 0.05f;
	SD::Vector2 buttonSize(0.2f, 0.05f);
	drawPosition.Y += buttonSpacing;
	SD::ButtonComponent* userSettings = SD::ButtonComponent::CreateObject();
	if (AddComponent(userSettings))
	{
		userSettings->SetPosition(0.5f - (buttonSize.X * 0.5f), drawPosition.Y);
		userSettings->SetSize(buttonSize);
		drawPosition.Y += buttonSize.Y + buttonSpacing;
		userSettings->SetCaptionText(translator->TranslateText(TXT("UserSettings"), TRANSLATION_FILE, TXT("GameSelection")));
		userSettings->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameSelection, HandleUserSettingsReleased, void, SD::ButtonComponent*));
	}

	SD::ButtonComponent* credits = SD::ButtonComponent::CreateObject();
	if (AddComponent(credits))
	{
		credits->SetPosition(0.5f - (buttonSize.X * 0.5f), drawPosition.Y);
		credits->SetSize(buttonSize);
		drawPosition.Y += buttonSize.Y + buttonSpacing;
		credits->SetCaptionText(translator->TranslateText(TXT("CreditsButton"), TRANSLATION_FILE, TXT("GameSelection")));
		credits->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameSelection, HandleCreditsReleased, void, SD::ButtonComponent*));
	}

	SD::ButtonComponent* quitButton = SD::ButtonComponent::CreateObject();
	if (AddComponent(quitButton))
	{
		quitButton->SetPosition(0.5f - (buttonSize.X * 0.5f), drawPosition.Y);
		quitButton->SetSize(buttonSize);
		drawPosition.Y += buttonSize.Y + buttonSpacing;
		quitButton->SetCaptionText(translator->TranslateText(TXT("QuitButton"), TRANSLATION_FILE, TXT("GameSelection")));
		quitButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_GameSelection, HandleQuitReleased, void, SD::ButtonComponent*));
	}
}

void MenuPanel_GameSelection::Destroyed ()
{
	if (GameDescriptionEntity != nullptr)
	{
		GameDescriptionEntity->Destroy();
		GameDescriptionEntity = nullptr;
	}

	Super::Destroyed();
}

bool MenuPanel_GameSelection::IsAnimating () const
{
	return (Tick != nullptr && Tick->IsTicking());
}

void MenuPanel_GameSelection::InitializeGameSelection ()
{
	for (SD::ClassIterator iter(GameRules::SStaticClass()); iter.GetSelectedClass() != nullptr; ++iter)
	{
		if (const GameRules* cdo = dynamic_cast<const GameRules*>(iter.GetSelectedClass()->GetDefaultObject()))
		{
			if (cdo->CanBeSelected())
			{
				Games.push_back(SGameTypeSelection(SD::ButtonComponent::CreateObject(), cdo));
			}
		}
	}
}

void MenuPanel_GameSelection::BrowseToGame (size_t newDesiredGameIdx)
{
	if (SelectedGame != DesiredSelectedGame || SelectedGame == newDesiredGameIdx)
	{
		//I'm either already moving to a game or I'm already the new desired game idx. Either case do nothing.
		return;
	}

	if (Tick == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to browse to a new game since there isn't a TickComponent ready for the game selection."));
		return;
	}

	DesiredSelectedGame = newDesiredGameIdx;
	IsPastHalfway = false;
	IsMovingRight = ((SelectedGame+1) % Games.size() != DesiredSelectedGame);
	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	GameSelectedTime = localEngine->GetElapsedTime();

	//Figure out the new positions
	size_t leftButtonIdx = (SelectedGame == 0) ? Games.size() - 1 : SelectedGame - 1;
	size_t rightButtonIdx = (SelectedGame == Games.size() - 1) ? 0 : SelectedGame + 1;

	Games.at(leftButtonIdx).HeadedPosition = (IsMovingRight) ? SelectedGamePosition : LeftFadeOutPosition;
	Games.at(leftButtonIdx).HeadedSize = (IsMovingRight) ? SelectedGameSize : LeftFadeOutSize;
	Games.at(leftButtonIdx).PrevPosition = Games.at(leftButtonIdx).Button->ReadPosition();
	Games.at(leftButtonIdx).PrevSize = Games.at(leftButtonIdx).Button->ReadSize();
	Games.at(leftButtonIdx).ReachesInHalftime = !IsMovingRight;
	Games.at(leftButtonIdx).IsAnimating = true;

	Games.at(SelectedGame).HeadedPosition = (IsMovingRight) ? NextGamePosition : PrevGamePosition;
	Games.at(SelectedGame).HeadedSize = (IsMovingRight) ? NextGameSize : PrevGameSize;
	Games.at(SelectedGame).PrevPosition = Games.at(SelectedGame).Button->ReadPosition();
	Games.at(SelectedGame).PrevSize = Games.at(SelectedGame).Button->ReadSize();
	Games.at(SelectedGame).ReachesInHalftime = false;
	Games.at(SelectedGame).IsAnimating = true;

	Games.at(rightButtonIdx).HeadedPosition = (IsMovingRight) ? RightFadeOutPosition : SelectedGamePosition;
	Games.at(rightButtonIdx).HeadedSize = (IsMovingRight) ? RightFadeOutSize : SelectedGameSize;
	Games.at(rightButtonIdx).PrevPosition = Games.at(rightButtonIdx).Button->ReadPosition();
	Games.at(rightButtonIdx).PrevSize = Games.at(rightButtonIdx).Button->ReadSize();
	Games.at(rightButtonIdx).ReachesInHalftime = IsMovingRight;
	Games.at(rightButtonIdx).IsAnimating = true;

	Tick->SetTicking(true);
}

void MenuPanel_GameSelection::BrowseToGameFinished ()
{
	//Snap everything to their desired positions
	for (size_t i = 0; i < Games.size(); ++i)
	{
		if (Games.at(i).IsAnimating)
		{
			Games.at(i).Button->SetPosition(Games.at(i).HeadedPosition);
			Games.at(i).Button->SetSize(Games.at(i).HeadedSize);
			Games.at(i).IsAnimating = false;
		}
	}

	if (Tick != nullptr)
	{
		Tick->SetTicking(false);
	}

	SelectedGame = DesiredSelectedGame;
	if (GameDescriptionLabel != nullptr)
	{
		GameDescriptionLabel->SetText(Games.at(SelectedGame).Gametype->ReadDescription());
	}
}

void MenuPanel_GameSelection::HandleBrowseGameTick (SD::FLOAT deltaSec)
{
	if (SelectedGame == DesiredSelectedGame)
	{
		return; //already there
	}

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	SD::FLOAT alpha = (localEngine->GetElapsedTime() - GameSelectedTime) / BrowseToGameTime;

	if (alpha >= 1.f)
	{
		BrowseToGameFinished();
		return;
	}

	if (alpha >= 0.5f && !IsPastHalfway)
	{
		//Handle the swap where one button hides and another shows.
		IsPastHalfway = true;
		size_t rightButtonIdx = (SelectedGame == Games.size() - 1) ? 0 : (SelectedGame+1);
		size_t leftButtonIdx = (SelectedGame == 0) ? (Games.size() - 1) : (SelectedGame-1);

		//Hide the fade out button
		if (IsMovingRight)
		{
			Games.at(rightButtonIdx).Button->SetVisibility(false);
			Games.at(rightButtonIdx).IsAnimating = false;
		}
		else
		{
			Games.at(leftButtonIdx).Button->SetVisibility(false);
			Games.at(leftButtonIdx).IsAnimating = false;
		}

		//Show the fade in button
		size_t newButtonIdx;
		if (IsMovingRight)
		{
			newButtonIdx = (leftButtonIdx == 0) ? (Games.size() - 1) : (leftButtonIdx - 1);
			Games.at(newButtonIdx).Button->SetPosition(LeftFadeOutPosition);
			Games.at(newButtonIdx).Button->SetSize(LeftFadeOutSize);
			Games.at(newButtonIdx).HeadedPosition = PrevGamePosition;
			Games.at(newButtonIdx).HeadedSize = PrevGameSize;
		}
		else
		{
			newButtonIdx = (rightButtonIdx >= Games.size() - 1) ? 0 : (rightButtonIdx + 1);
			Games.at(newButtonIdx).Button->SetPosition(RightFadeOutPosition);
			Games.at(newButtonIdx).Button->SetSize(RightFadeOutSize);
			Games.at(newButtonIdx).HeadedPosition = NextGamePosition;
			Games.at(newButtonIdx).HeadedSize = NextGameSize;
		}

		Games.at(newButtonIdx).Button->SetVisibility(true);
		Games.at(newButtonIdx).PrevPosition = Games.at(newButtonIdx).Button->ReadPosition();
		Games.at(newButtonIdx).PrevSize = Games.at(newButtonIdx).Button->ReadSize();
		Games.at(newButtonIdx).IsAnimating = true;
		Games.at(newButtonIdx).ReachesInHalftime = true;
	}

	//Advance the buttons
	for (size_t i = 0; i < Games.size(); ++i)
	{
		if (!Games.at(i).IsAnimating)
		{
			continue;
		}

		Games.at(i).Button->SetPosition(SD::Vector2::Lerp(alpha, Games.at(i).PrevPosition, Games.at(i).HeadedPosition));
		Games.at(i).Button->SetSize(SD::Vector2::Lerp(alpha, Games.at(i).PrevSize, Games.at(i).HeadedSize));
	}
}

void MenuPanel_GameSelection::HandleGameButtonReleased (SD::ButtonComponent* uiComponent)
{
	if (IsAnimating())
	{
		//Currently in transition. Ignore button press
		return;
	}

	if (Games.at(SelectedGame).Button == uiComponent)
	{
		if (SoundMessenger* msg = SoundMessenger::GetMessenger())
		{
			msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_HEAVY);
		}

		//Initialize the game rules instance
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr)
		localDroneEngine->SetGameRules(Games.at(SelectedGame).Gametype);

		//The selected game is clicked again. Advance to that game
		MainMenu* owningMenu = GetOwningMenu();
		CHECK(owningMenu != nullptr)
		owningMenu->PushPanel(dynamic_cast<const MenuPanel_GameRules*>(MenuPanel_GameRules::SStaticClass()->GetDefaultObject()));
		return;
	}
	
	//Identify if next or previous was clicked
	size_t prevIdx = (SelectedGame == 0) ? (Games.size() - 1) : (SelectedGame - 1);
	if (Games.at(prevIdx).Button == uiComponent)
	{
		if (SoundMessenger* msg = SoundMessenger::GetMessenger())
		{
			msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_LIGHT);
		}

		BrowseToGame(prevIdx);
		return;
	}
	
	size_t nextIdx = (SelectedGame >= Games.size() - 1) ? 0 : (SelectedGame + 1);
	if (Games.at(nextIdx).Button == uiComponent)
	{
		if (SoundMessenger* msg = SoundMessenger::GetMessenger())
		{
			msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
		}

		BrowseToGame(nextIdx);
	}
	else
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Somehow the user clicked on a game button that's neither the current selected game or the adjacent game. SelectedGameIdx=%s. NextGameIdx=%s. PrevGameIdx=%s."), SD::INT(SelectedGame), SD::INT(nextIdx), SD::INT(prevIdx));
	}
}

void MenuPanel_GameSelection::HandlePrevButtonReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_LIGHT);
	}

	size_t newIdx = (SelectedGame == 0) ? (Games.size() - 1) : (SelectedGame - 1);
	BrowseToGame(newIdx);
}

void MenuPanel_GameSelection::HandleNextButtonReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	size_t newIdx = (SelectedGame == Games.size() - 1) ? 0 : (SelectedGame + 1);
	BrowseToGame(newIdx);
}

void MenuPanel_GameSelection::HandleUserSettingsReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	MainMenu* owningMenu = GetOwningMenu();
	CHECK(owningMenu != nullptr)
	owningMenu->PushPanel(dynamic_cast<const MenuPanel_UserSettings*>(MenuPanel_UserSettings::SStaticClass()->GetDefaultObject()));
}

void MenuPanel_GameSelection::HandleCreditsReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	MainMenu* owningMenu = GetOwningMenu();
	CHECK(owningMenu != nullptr)
	owningMenu->PushPanel(dynamic_cast<const MenuPanel_Credits*>(MenuPanel_Credits::SStaticClass()->GetDefaultObject()));
}

void MenuPanel_GameSelection::HandleQuitReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->Shutdown();
}
DD_END