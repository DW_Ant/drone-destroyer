/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TimeLimitHudComponent.cpp
=====================================================================
*/

#include "Hud.h"
#include "TimeLimitComponent.h"
#include "TimeLimitHudComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, TimeLimitHudComponent, SD, GuiComponent)

void TimeLimitHudComponent::InitProps ()
{
	Super::InitProps();

	ClockLabel = nullptr;
}

void TimeLimitHudComponent::InitializeComponents ()
{
	Super::InitializeComponents();

	SD::FrameComponent* frame = SD::FrameComponent::CreateObject();
	if (AddComponent(frame))
	{
		frame->SetAnchorRight(0.f);
		frame->SetAnchorTop(0.f);
		frame->SetSize(SD::Vector2(0.2f, 0.1f));
		frame->SetLockedFrame(true);
		frame->SetBorderThickness(2.f);
		if (frame->RenderComponent != nullptr)
		{
			frame->RenderComponent->ClearSpriteTexture();
			frame->RenderComponent->FillColor = SD::Color(172, 208, 120);
		}

		ClockLabel = SD::LabelComponent::CreateObject();
		if (frame->AddComponent(ClockLabel))
		{
			ClockLabel->SetAutoRefresh(false);

			SD::FLOAT anchorDist = 2.f;
			ClockLabel->SetAnchorTop(anchorDist);
			ClockLabel->SetAnchorRight(anchorDist);
			ClockLabel->SetAnchorBottom(anchorDist);
			ClockLabel->SetAnchorLeft(anchorDist);
			ClockLabel->SetWrapText(false);
			ClockLabel->SetClampText(true);
			ClockLabel->SetCharacterSize(28);
			if (ClockLabel->GetRenderComponent() != nullptr)
			{
				ClockLabel->GetRenderComponent()->SetFontColor(sf::Color::Black);
			}
			ClockLabel->SetAutoRefresh(true);
		}
	}
}

void TimeLimitHudComponent::Destroyed ()
{
	if (GameClock != nullptr)
	{
		GameClock->OnTimeChanged.UnregisterHandler(SDFUNCTION_1PARAM(this, TimeLimitHudComponent, HandleTimeChanged, void, SD::INT));
	}

	Super::Destroyed();
}

void TimeLimitHudComponent::BindClockTo (TimeLimitComponent* timeComponent)
{
	if (GameClock != nullptr)
	{
		GameClock->OnTimeChanged.UnregisterHandler(SDFUNCTION_1PARAM(this, TimeLimitHudComponent, HandleTimeChanged, void, SD::INT));
	}

	GameClock = timeComponent;
	if (GameClock != nullptr)
	{
		GameClock->OnTimeChanged.RegisterHandler(SDFUNCTION_1PARAM(this, TimeLimitHudComponent, HandleTimeChanged, void, SD::INT));
	}
}

void TimeLimitHudComponent::UpdateTime (SD::INT seconds)
{
	if (ClockLabel != nullptr)
	{
		seconds = SD::Utils::Max<SD::INT>(0, seconds);

		SD::INT minutes = seconds / 60;
		seconds %= 60;
		ClockLabel->SetText(SD::DString::CreateFormattedString(TXT("%s:%s"), minutes.ToFormattedString(2), seconds.ToFormattedString(2)));
	}
}

void TimeLimitHudComponent::HandleTimeChanged (SD::INT newSeconds)
{
	UpdateTime(newSeconds);
}

DD_END