/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Arena.cpp
=====================================================================
*/

#include "AmmoPickup.h"
#include "Arena.h"
#include "Character.h"
#include "DroneDestroyerEngineComponent.h"
#include "HandGun.h"
#include "HealthPickup.h"
#include "MachineGun.h"
#include "NavigationComponent.h"
#include "Inventory.h"
#include "PickupStation.h"
#include "PlayerStart.h"
#include "Shotgun.h"
#include "SoundMessenger.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Arena, SD, Entity)

void Arena::InitProps ()
{
	Super::InitProps();

	HumanCorpseSizeVariation.Min = 1.5f;
	HumanCorpseSizeVariation.Max = 2.5f;

	FirstNavigation = nullptr;

	HumanCorpses = nullptr;
	DroneCorpses = nullptr;
	MaxNumHumanCorpses = 50000;
	LatestHumanCorpseIndex = -1;
	LatestDroneCorpseIndex = -1;
}

void Arena::BeginObject ()
{
	Super::BeginObject();

	SD::SceneTransformComponent* transform = SD::SceneTransformComponent::CreateObject();
	if (AddComponent(transform))
	{
		transform->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_MAP + 5.f; //slightly above background
		transform->AutoComputeAbsTransform = false;
		transform->CalculateAbsoluteTransform();

		HumanCorpses = SD::InstancedSpriteComponent::CreateObject();
		if (transform->AddComponent(HumanCorpses))
		{
			SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
			CHECK(localTexturePool != nullptr)

			HumanCorpses->SetSpriteTexture(localTexturePool->FindTexture(TXT("DroneDestroyer.Player.DeadPlayer")));

			DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(HumanCorpses);
		}
		else
		{
			HumanCorpses = nullptr;
		}
	}
}

void Arena::Destroyed ()
{
	//Destroy navigation. But to prevent Navigation nodes from repairing their linked lists (that iterates through each node every time it's destroyed),
	//sever the head NavigationPoint to prevent that from happening (for a performance gain).
	NavigationComponent* nav = FirstNavigation;
	FirstNavigation = nullptr;
	while (nav != nullptr)
	{
		NavigationComponent* nextNav = nav->GetNextNavigation();
		nav->Destroy();
		nav = nextNav;
	}

	for (size_t i = 0; i < ArenaEntities.size(); ++i)
	{
		ArenaEntities.at(i)->Destroy();
	}
	SD::ContainerUtils::Empty(ArenaEntities);

	Super::Destroyed();
}

void Arena::GetArenaMetaData (const SD::DString& mapName, SD::DString& outPreviewImage, SD::Range<SD::INT>& outRecPlayers, SD::DString& outAuthorName, SD::DString& outDescriptionKey)
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile((SD::Directory::CONFIG_DIRECTORY).ToString() + TXT("Arenas.ini"), false))
	{
		config->Destroy();
		return;
	}

	outPreviewImage = config->GetPropertyText(mapName, TXT("PreviewImage"));
	outRecPlayers.Min = config->GetProperty<SD::INT>(mapName, TXT("RecommendedPlayersMin"));
	outRecPlayers.Max = config->GetProperty<SD::INT>(mapName, TXT("RecommendedPlayersMax"));
	outAuthorName = config->GetPropertyText(mapName, TXT("AuthorName"));
	outDescriptionKey = config->GetPropertyText(mapName, TXT("DescriptionKey"));

	config->Destroy();
}

bool Arena::IsAllowedFor (GameRules* rules) const
{
	return true;
}

bool Arena::ArenaTrace (const b2RayCastInput& input, b2RayCastOutput* impactPoint) const
{
	bool collided = false;
	float closestImpact = 1.f;

	for (SD::Entity* entity : ArenaEntities)
	{
		//Ignore pickups, player spawns, and characters
		if (dynamic_cast<Inventory*>(entity) != nullptr || dynamic_cast<Character*>(entity) != nullptr || dynamic_cast<PlayerStart*>(entity) != nullptr)
		{
			continue;
		}

		SD::PhysicsComponent* physComp = dynamic_cast<SD::PhysicsComponent*>(entity->FindSubComponent(SD::PhysicsComponent::SStaticClass(), true));
		if (physComp == nullptr)
		{
			continue;
		}

		b2Body* physBody = physComp->GetPhysicsBody();
		if (physBody == nullptr)
		{
			continue;
		}

		for (b2Fixture* fixture = physBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
		{
			b2RayCastOutput output;
			if (fixture->RayCast(&output, input, 0))
			{
				if (output.fraction < closestImpact)
				{
					closestImpact = output.fraction;
					impactPoint->fraction = output.fraction;
					impactPoint->normal = output.normal;
					collided = true;
				}
			}
		}
	}

	return collided;
}

bool Arena::FastTrace (const SD::Vector3& start, const SD::Vector3& end) const
{
	b2RayCastInput input;
	input.maxFraction = 1.f;
	input.p1 = SD::Box2dUtils::ToBox2dCoordinates(start);
	input.p2 = SD::Box2dUtils::ToBox2dCoordinates(end);

	for (SD::Entity* entity : ArenaEntities)
	{
		//Ignore pickups, player spawns, and characters
		if (dynamic_cast<Inventory*>(entity) != nullptr || dynamic_cast<Character*>(entity) != nullptr || dynamic_cast<PlayerStart*>(entity) != nullptr)
		{
			continue;
		}

		SD::PhysicsComponent* physComp = dynamic_cast<SD::PhysicsComponent*>(entity->FindSubComponent(SD::PhysicsComponent::SStaticClass(), true));
		if (physComp == nullptr)
		{
			continue;
		}

		b2Body* physBody = physComp->GetPhysicsBody();
		if (physBody == nullptr)
		{
			continue;
		}

		for (b2Fixture* fixture = physBody->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
		{
			b2RayCastOutput output;
			if (fixture->RayCast(&output, input, 0))
			{
				return true; //Early exit
			}
		}
	}

	return false;
}

void Arena::AddCorpse (SD::FLOAT characterRadius, const SD::Vector3& corpseLocation)
{
	if (HumanCorpses == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to add a human corpse since the Arena does not have the InstancedSpriteComponent to add them to."));
		return;
	}

	LatestHumanCorpseIndex++;
	if (MaxNumHumanCorpses > 0 && LatestHumanCorpseIndex >= MaxNumHumanCorpses)
	{
		LatestHumanCorpseIndex = 0;
	}

	AddCorpseImplementation(characterRadius, corpseLocation, HumanCorpses, LatestHumanCorpseIndex.ToUnsignedInt(), 0);
}

void Arena::AddDroneCorpse (SD::FLOAT characterRadius, const SD::Vector3& corpseLocation)
{
	if (DroneCorpses == nullptr)
	{
		SD::SceneTransformComponent* transformComp = SD::SceneTransformComponent::CreateObject();
		if (AddComponent(transformComp))
		{
			transformComp->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_MAP + 60.f; //above all map objects

			DroneCorpses = SD::InstancedSpriteComponent::CreateObject();
			if (transformComp->AddComponent(DroneCorpses))
			{
				SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
				CHECK(localTexturePool != nullptr)
				DroneCorpses->SetNumSubDivisions(5, 1);
				DroneCorpses->SetSpriteTexture(localTexturePool->FindTexture(TXT("DroneDestroyer.Enemies.DeadEnemies")));

				DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(DroneCorpses);
			}
			else
			{
				CHECK_INFO(false, "Failed to initialize DroneCorpses");
			}
		}
	}

	LatestDroneCorpseIndex++;
	if (MaxNumHumanCorpses > 0 && LatestDroneCorpseIndex >= MaxNumHumanCorpses)
	{
		LatestDroneCorpseIndex = 0;
	}

	AddCorpseImplementation(characterRadius, corpseLocation, DroneCorpses, LatestDroneCorpseIndex.ToUnsignedInt(), SD::RandomUtils::Rand(6).ToInt32());
}

void Arena::InitializeArenaObjects (const SD::DString& arenaName)
{
	CHECK(!arenaName.IsEmpty())

	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile((SD::Directory::CONFIG_DIRECTORY).ToString() + TXT("Arenas.ini"), false))
	{
		config->Destroy();
		return;
	}

	//Launch the map music
	SD::DString mapMusic = config->GetPropertyText(arenaName, TXT("Music"));
	if (!mapMusic.IsEmpty())
	{
		if (SoundMessenger* msg = SoundMessenger::GetMessenger())
		{
			msg->PlaySound(true, mapMusic, 1.f);
		}
	}

	SD::FLOAT gridSize = config->GetProperty<SD::FLOAT>(arenaName, TXT("GridSize"));
	if (gridSize <= 2.f)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Invalid grid size. %s is too small. It's pulled from the %s section in the Arenas.ini."), gridSize, arenaName);
		config->Destroy();
		return;
	}

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	const SD::Texture* groundTexture = localTexturePool->FindTexture(config->GetPropertyText(arenaName, TXT("GroundTexture")), true);
	const SD::Texture* smallDecoTexture = localTexturePool->FindTexture(config->GetPropertyText(arenaName, TXT("SmallDecoTexture")), true);

	std::vector<SD::DString> textureList;
	config->GetArrayValues<SD::DString>(arenaName, TXT("LargeDecoTextures"), OUT textureList);
	std::vector<const SD::Texture*> largeTextures;
	for (const SD::DString& textureName : textureList)
	{
		largeTextures.push_back(localTexturePool->FindTexture(textureName, true));
	}

	//Import general map data
	SD::INT numInstancesPerCluster = config->GetProperty<SD::INT>(arenaName, TXT("NumSmallDecoPerCluster"));
	SD::FLOAT clusterRadius = config->GetProperty<SD::FLOAT>(arenaName, TXT("SmallDecoClusterRadius"));
	SD::FLOAT smallDecoSizeVariation = config->GetProperty<SD::FLOAT>(arenaName, TXT("SmallDecoSizeVariation"));
	bool randomizeSmallDecoRotation = config->GetProperty<SD::BOOL>(arenaName, TXT("SmallDecoRandRotation"));
	bool randomizeLargeDecoRotation = config->GetProperty<SD::BOOL>(arenaName, TXT("LargeDecoRandRotation"));

	//Initialize ground tiles
	{
		std::vector<SD::DString> groundData;
		//The odd name LightData is intended so there's the same number of characters in ArenaData so that it's easier to overlay one array over the other without character displacement. LightData is short for lighting data.
		config->GetArrayValues(arenaName, TXT("LightData"), OUT groundData);

		if (!SD::ContainerUtils::IsEmpty(groundData))
		{
			SD::InstancedSpriteComponent* bottomFloor = SD::InstancedSpriteComponent::CreateObject();
			if (AddComponent(bottomFloor))
			{
				bottomFloor->SetSpriteTexture(groundTexture);
				bottomFloor->SetNumSubDivisions(10, 1);
				DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(bottomFloor);
			}

			SD::Vector2 curPos(0.f, SD::FLOAT::MakeFloat(groundData.size()) * -0.5f * gridSize);
			for (SD::DString& row : groundData)
			{
				row.TrimSpaces();
				curPos.X = row.Length().ToFLOAT() * -0.5f * gridSize;
				for (SD::INT colIdx = 0; colIdx < row.Length(); ++colIdx)
				{
					SD::DString charValue = row.At(colIdx);
					SD::INT lightNumber = SD::INT(std::atoi(charValue.ToCString()));

					if (lightNumber > 0 || row.At(colIdx) == '0')
					{
						SD::InstancedSpriteComponent::SSpriteInstance newSpriteInstance;
						newSpriteInstance.Position = curPos;
						newSpriteInstance.Size = SD::Vector2(gridSize, gridSize);
						newSpriteInstance.SubDivisionIdxX = lightNumber.ToInt32();
						newSpriteInstance.SubDivisionIdxY = 0;
						bottomFloor->EditSpriteInstances().push_back(newSpriteInstance);
					}

					curPos.X += gridSize;
				}

				curPos.Y += gridSize;
			}

			bottomFloor->RegenerateVertices();
		}
	}

	//Initialize deco layer
	SD::FLOAT decoScale = 0.25f; //Quarter the size of a grid tile
	SD::InstancedSpriteComponent* decoLayer = nullptr;
	{
		SD::SceneTransformComponent* decoTransform = SD::SceneTransformComponent::CreateObject();
		if (AddComponent(decoTransform))
		{
			decoTransform->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_MAP + 1.f;
			decoTransform->AutoComputeAbsTransform = false;
			decoTransform->CalculateAbsoluteTransform();

			decoLayer = SD::InstancedSpriteComponent::CreateObject();
			if (decoTransform->AddComponent(decoLayer))
			{
				decoLayer->SetSpriteTexture(smallDecoTexture);
				SD::INT subDivideX = config->GetProperty<SD::INT>(arenaName, TXT("NumDecoTexturesX"));
				SD::INT subDivideY = config->GetProperty<SD::INT>(arenaName, TXT("NumDecoTexturesY"));
				decoLayer->SetNumSubDivisions(subDivideX, subDivideY);
				DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(decoLayer);
			}
			else
			{
				decoLayer = nullptr;
			}
		}
	}
	CHECK(decoLayer != nullptr)

	std::vector<SD::DString> mapData;
	config->GetArrayValues(arenaName, TXT("ArenaData"), OUT mapData);

	if (SD::ContainerUtils::IsEmpty(mapData))
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to populate Entities for the arena since the ArenaData in section %s is empty."), arenaName);
		config->Destroy();
		return;
	}

	SD::RandomEngineComponent* randEngine = SD::RandomEngineComponent::Find();
	CHECK(randEngine != nullptr)
	unsigned int oldSeed = randEngine->GetSeed();
	SD::INT seed = config->GetProperty<SD::INT>(arenaName, TXT("Seed"));
	if (seed != 0)
	{
		randEngine->SetSeed(seed.ToUnsignedInt32());
	}
	else
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Seed is not specified in %s section. The generated map Entities are going to be inconsistent each game session."), arenaName);
	}

	std::function<const SD::Texture*()> pickRandTexture([&largeTextures]()
	{
		return largeTextures.at(SD::RandomUtils::Rand(SD::INT(largeTextures.size())).ToUnsignedInt());
	});

	SD::Vector3 spawnLocation(0.f, SD::FLOAT::MakeFloat(mapData.size()) * gridSize * -0.5f, DroneDestroyerEngineComponent::DRAW_ORDER_MAP);
	SD::Vector3 depth(0.f, 0.f, 0.f);
	for (const SD::DString& row : mapData)
	{
		spawnLocation.X = row.Length().ToFLOAT() * gridSize * -0.5f;
		for (SD::INT i = 0; i < row.Length(); ++i)
		{
			switch(row.At(i))
			{
				case('T'): //Spawn big tree
				{
					depth.Z = 50.f; //Draw over other map objects
					SD::FLOAT yaw = (randomizeLargeDecoRotation) ? SD::RandomUtils::RandRange(-180.f, 180.f) : 0.f;
					CreateStaticObject(spawnLocation + depth, SD::Vector3(1.f, 1.f, 1.f) * gridSize, yaw, pickRandTexture());
					break;
				}

				case('t'): //Spawn smaller tree
				{
					depth.Z = 45.f;
					SD::Vector3 scaleMultiplier;
					scaleMultiplier.X = SD::RandomUtils::RandRange(0.5f, 0.75f);
					scaleMultiplier.Y = SD::RandomUtils::RandRange(0.5f, 0.75f);
					scaleMultiplier.Z = 1.f;
					SD::FLOAT yaw = (randomizeLargeDecoRotation) ? SD::RandomUtils::RandRange(-180.f, 180.f) : 0.f;
					CreateStaticObject(spawnLocation + depth, scaleMultiplier * gridSize, yaw, pickRandTexture());
					break;
				}

				case('B'): //Spawn full sized box
				{
					depth.Z = 40.f;
					CreateBlackBox(spawnLocation + depth, SD::Vector3(1.f, 1.f, 1.f) * gridSize, 0.f);
					break;
				}

				case('b'): //Spawn a half grid box
				{
					depth.Z = 40.f;
					CreateBlackBox(spawnLocation + depth, SD::Vector3(0.5f, 0.5f, 1.f) * gridSize, 0.f);
					break;
				}

				case('D'): //Spawn full sized / 45 degree box
				{
					depth.Z = 40.f;
					CreateBlackBox(spawnLocation + depth, SD::Vector3(1.f, 1.f, 1.f) * gridSize, 45.f);
					break;
				}

				case('d'): //Spawn half grid / 45 degree box
				{
					depth.Z = 40.f;
					CreateBlackBox(spawnLocation + depth, SD::Vector3(0.5f, 0.5f, 1.f) * gridSize, 45.f);
					break;
				}

				case('v'): //spawn a single small decoration at this tile
				{
					CreateSmallDeco(decoLayer, spawnLocation.ToVector2(), gridSize * decoScale, gridSize * decoScale, smallDecoSizeVariation, randomizeSmallDecoRotation);
					break;
				}

				case('V'): //Spawn a half cluster around this tile
				{
					SD::INT numInstances = (numInstancesPerCluster / 2);
					for (int i = 0; i < numInstances; ++i)
					{
						CreateSmallDeco(decoLayer, spawnLocation.ToVector2(), gridSize * clusterRadius * 0.5f, gridSize * decoScale, smallDecoSizeVariation, randomizeSmallDecoRotation);
					}

					break;
				}

				case('w'): //Spawn a cluster around this tile
				{
					for (SD::INT i = 0; i < numInstancesPerCluster; ++i)
					{
						CreateSmallDeco(decoLayer, spawnLocation.ToVector2(), gridSize * clusterRadius, gridSize * decoScale, smallDecoSizeVariation, randomizeSmallDecoRotation);
					}

					break;
				}

				case('W'): //Spawn a cluster with twice the number and range
				{
					for (SD::INT i = 0; i < numInstancesPerCluster * 2; ++i)
					{
						CreateSmallDeco(decoLayer, spawnLocation.ToVector2(), gridSize * clusterRadius * 2.f, gridSize * decoScale, smallDecoSizeVariation, randomizeSmallDecoRotation);
					}

					break;
				}

				case('X'): //Spawn a player start
				{
					PlayerStart* playerStart = PlayerStart::CreateObject();
					playerStart->SetTranslation(spawnLocation);
					playerStart->CalculateAbsoluteTransform(); //Since these Entities do not auto calc transforms.
					if (playerStart->GetNavigation() != nullptr)
					{
						playerStart->GetNavigation()->SetMaxConnectionRadius(gridSize * 3.f);
						playerStart->GetNavigation()->SetWanderRadius(gridSize);
					}

					ArenaEntities.push_back(playerStart);
					break;
				}

				case('p'): //Spawn a pathnode in a tight spot
				{
					CreatePathNode(spawnLocation, gridSize * 3.f, gridSize * 0.5f);
					break;
				}

				case('P'): //Spawn a pathnode in a wide space
				{
					CreatePathNode(spawnLocation, gridSize * 5.f, gridSize);
					break;
				}

				case('q'): //Spawn a path node with reach up to 10 tiles away.
				{
					CreatePathNode(spawnLocation, gridSize * 10.f, gridSize * 2.f);
					break;
				}

				case('Q'): //Spawn a path node with reach up to 20 tiles away and a wide wander radius.
				{
					CreatePathNode(spawnLocation, gridSize * 20.f, gridSize * 4.f);
					break;
				}

				case('A'):
				case('a'): //Spawn ammo pickup
				{
					depth.Z = DroneDestroyerEngineComponent::DRAW_ORDER_PICKUPS;
					CreatePickup(spawnLocation + depth, dynamic_cast<const AmmoPickup*>(AmmoPickup::SStaticClass()->GetDefaultObject()));
					break;
				}

				case('h'):
				case('H'): //Spawn a health pickup
				{
					depth.Z = DroneDestroyerEngineComponent::DRAW_ORDER_PICKUPS;
					CreatePickup(spawnLocation + depth, dynamic_cast<const HealthPickup*>(HealthPickup::SStaticClass()->GetDefaultObject()));
					break;
				}

				case('1'): //Spawn a handgun pickup
				{
					depth.Z = DroneDestroyerEngineComponent::DRAW_ORDER_PICKUPS;
					CreatePickup(spawnLocation + depth, dynamic_cast<const HandGun*>(HandGun::SStaticClass()->GetDefaultObject()));
					break;
				}

				case('2'): //Spawn a minigun pickup
				{
					depth.Z = DroneDestroyerEngineComponent::DRAW_ORDER_PICKUPS;
					CreatePickup(spawnLocation + depth, dynamic_cast<const MachineGun*>(MachineGun::SStaticClass()->GetDefaultObject()));
					break;
				}

				case('3'): //Spawn a shotgun pickup
				{
					depth.Z = DroneDestroyerEngineComponent::DRAW_ORDER_PICKUPS;
					CreatePickup(spawnLocation + depth, dynamic_cast<const Shotgun*>(Shotgun::SStaticClass()->GetDefaultObject()));
					break;
				}
			}

			spawnLocation.X += gridSize;
		}

		spawnLocation.Y += gridSize;
	}

	decoLayer->RegenerateVertices();

	if (seed != 0)
	{
		randEngine->SetSeed(oldSeed);
	}

	config->Destroy();

	//Iterate through the navigation list to construct connections.
	for (NavigationComponent* nav = FirstNavigation; nav != nullptr; nav = nav->GetNextNavigation())
	{
		nav->BuildPathing(false);
	}
}

void Arena::SetFirstNavigation (NavigationComponent* newFirstNavigation)
{
	FirstNavigation = newFirstNavigation;
}

void Arena::SetMaxNumHumanCorpses (SD::INT newMaxNumHumanCorpses)
{
	MaxNumHumanCorpses = newMaxNumHumanCorpses;
	if (MaxNumHumanCorpses >= 0 && HumanCorpses != nullptr && HumanCorpses->ReadSpriteInstances().size() > MaxNumHumanCorpses)
	{
		HumanCorpses->EditSpriteInstances().resize(MaxNumHumanCorpses.ToUnsignedInt());
		HumanCorpses->RegenerateVertices();
		LatestHumanCorpseIndex = 0;
		LatestDroneCorpseIndex = 0;
	}
}

SD::SceneEntity* Arena::CreatePathNode (const SD::Vector3& nodeLocation, SD::FLOAT connectionRadius, SD::FLOAT wanderRadius)
{
	SD::SceneEntity* pathNode = SD::SceneEntity::CreateObject();
	CHECK(pathNode != nullptr)
	pathNode->SetTranslation(nodeLocation);
	pathNode->AutoComputeAbsTransform = false;
	pathNode->CalculateAbsoluteTransform();

	NavigationComponent* nav = NavigationComponent::CreateObject();
	if (pathNode->AddComponent(nav))
	{
#ifdef DEBUG_MODE
		nav->DebugName = TXT("PathNode_NavComponent");
#endif
		if (connectionRadius > 0.f)
		{
			nav->SetMaxConnectionRadius(connectionRadius);
		}

		if (wanderRadius > 0.f)
		{
			nav->SetWanderRadius(wanderRadius);
#ifdef DEBUG_MODE
			bool debugWanderRadius = false;
			if (debugWanderRadius)
			{
				SD::SolidColorRenderComponent* wanderColor = SD::SolidColorRenderComponent::CreateObject();
				if (pathNode->AddComponent(wanderColor))
				{
					wanderColor->SolidColor = SD::Color(48, 255, 255, 64);
					wanderColor->Shape = SD::SolidColorRenderComponent::ST_Circle;
					wanderColor->CenterOrigin = true;
					wanderColor->BaseSize = wanderRadius * 2.f;
					DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(wanderColor);
				}
			}
#endif
		}
	}

	ArenaEntities.push_back(pathNode);
	return pathNode;
}

void Arena::CreatePickup (const SD::Vector3& location, const Inventory* pickupCdo)
{
	PickupStation* pickup = PickupStation::CreateObject();
	pickup->SetTranslation(location);
	pickup->CalculateAbsoluteTransform();
	pickup->SetInventoryClass(pickupCdo);
	pickup->ApplyGameModifications();
	ArenaEntities.push_back(pickup);
}

SD::SceneEntity* Arena::CreateStaticObject (const SD::Vector3& location, const SD::Vector3& size, SD::FLOAT yaw, const SD::Texture* spriteTexture)
{
	SD::SceneEntity* newObj = SD::SceneEntity::CreateObject();
	newObj->SetTranslation(location);
	newObj->SetScale(size);
	newObj->EditRotation().SetYaw(yaw, SD::Rotator::RU_Degrees);
	newObj->AutoComputeAbsTransform = false;
	newObj->CalculateAbsoluteTransform();

	SD::SpriteComponent* sprite = SD::SpriteComponent::CreateObject();
	if (newObj->AddComponent(sprite))
	{
		sprite->SetSpriteTexture(spriteTexture);
		sprite->CenterOrigin = true;

		DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(sprite);
	}

	SD::PhysicsComponent::SComponentInitData initData;
	initData.BodyInit.PhysicsType = SD::PhysicsComponent::PT_Static;
	initData.CollisionGroup = 1;
	b2PolygonShape rect = SD::Box2dUtils::GetBox2dRectangle(SD::Rectangle<SD::FLOAT>(size.X * -0.5f, size.Y * -0.5f, size.X * 0.5f, size.Y * 0.5f, SD::Rectangle<SD::FLOAT>::CS_XRightYDown));
	initData.CollisionShape = &rect;

	SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateAndInitializeComponent<SD::PhysicsComponent>(newObj, initData);

#ifdef DEBUG_MODE
	bool debugCollision = false;
	if (debugCollision)
	{
		SD::PhysicsRenderer* renderComp = SD::PhysicsRenderer::CreateObject();
		if (newObj->AddComponent(renderComp))
		{
			renderComp->FillColor = SD::Color(0, 128, 128, 96);
			DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(renderComp);
		}
	}
#endif

	ArenaEntities.push_back(newObj);
	return newObj;
}

SD::SceneEntity* Arena::CreateBlackBox (const SD::Vector3& location, const SD::Vector3& size, SD::FLOAT yaw)
{
	SD::SceneEntity* newObj = SD::SceneEntity::CreateObject();
	newObj->SetTranslation(location);
	newObj->SetScale(size);
	newObj->EditRotation().SetYaw(yaw, SD::Rotator::RU_Degrees);
	newObj->AutoComputeAbsTransform = false;
	newObj->CalculateAbsoluteTransform();

	SD::SolidColorRenderComponent* colorComp = SD::SolidColorRenderComponent::CreateObject();
	if (newObj->AddComponent(colorComp))
	{
		colorComp->CenterOrigin = true;
		colorComp->SolidColor = SD::Color::BLACK;

		DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(colorComp);
	}

	SD::PhysicsComponent::SComponentInitData initData;
	initData.BodyInit.PhysicsType = SD::PhysicsComponent::PT_Static;
	initData.CollisionGroup = 1;
	b2PolygonShape rect = SD::Box2dUtils::GetBox2dRectangle(SD::Rectangle<SD::FLOAT>(size.X * -0.5f, size.Y * -0.5f, size.X * 0.5f, size.Y * 0.5f, SD::Rectangle<SD::FLOAT>::CS_XRightYDown));
	initData.CollisionShape = &rect;

	SD::PhysicsComponent* physComp = SD::PhysicsComponent::CreateAndInitializeComponent<SD::PhysicsComponent>(newObj, initData);

	ArenaEntities.push_back(newObj);
	return newObj;
}

void Arena::CreateSmallDeco (SD::InstancedSpriteComponent* spriteComp, const SD::Vector2& location, SD::FLOAT locationVariation, SD::FLOAT size, SD::FLOAT sizeVariation, bool randRotation) const
{
	CHECK(spriteComp != nullptr)

	SD::InstancedSpriteComponent::SSpriteInstance newSprite;
	newSprite.Position = location;
	newSprite.Position.X += SD::RandomUtils::RandRange(locationVariation * -0.5f, locationVariation * 0.5f);
	newSprite.Position.Y += SD::RandomUtils::RandRange(locationVariation * -0.5f, locationVariation * 0.5f);
	newSprite.Size = SD::Vector2(size, size);

	unsigned int yaw = (randRotation) ? SD::RandomUtils::Rand(SD::Rotator::FULL_REVOLUTION).ToUnsignedInt32() : 0;
	newSprite.Rotation.Yaw = yaw;

	newSprite.SubDivisionIdxX = SD::RandomUtils::Rand(spriteComp->GetNumSubDivisionsX()).ToInt32();
	newSprite.SubDivisionIdxY = SD::RandomUtils::Rand(spriteComp->GetNumSubDivisionsY()).ToInt32();

	spriteComp->EditSpriteInstances().push_back(newSprite);
}

void Arena::AddCorpseImplementation (SD::FLOAT characterRadius, const SD::Vector3& corpseLocation, SD::InstancedSpriteComponent* spriteComp, size_t corpseIdx, int corpseSubDivideX) const
{
	CHECK(spriteComp != nullptr)

	SD::InstancedSpriteComponent::SSpriteInstance newSprite;
	newSprite.Position = corpseLocation.ToVector2();
	newSprite.Rotation.Yaw = SD::RandomUtils::Rand(SD::Rotator::FULL_REVOLUTION).ToUnsignedInt();
	newSprite.Size.X = characterRadius * SD::RandomUtils::RandRange(HumanCorpseSizeVariation.Min, HumanCorpseSizeVariation.Max);
	newSprite.Size.Y = characterRadius * SD::RandomUtils::RandRange(HumanCorpseSizeVariation.Min, HumanCorpseSizeVariation.Max);
	newSprite.SubDivisionIdxX = corpseSubDivideX;

	if (corpseIdx >= spriteComp->ReadSpriteInstances().size())
	{
		spriteComp->EditSpriteInstances().push_back(newSprite);
	}
	else
	{
		spriteComp->EditSpriteInstances().at(corpseIdx) = newSprite;
	}

	spriteComp->RegenerateVertices({corpseIdx});
}

DD_END