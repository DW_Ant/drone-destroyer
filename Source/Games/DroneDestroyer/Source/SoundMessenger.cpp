/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SoundMessenger.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "SoundEngineComponent.h"
#include "SoundMessenger.h"
#include "SoundPlayer.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, SoundMessenger, SD, Entity)

void SoundMessenger::InitProps ()
{
	Super::InitProps();

	Sender = nullptr;
	VolumeLevelSender = nullptr;

	PendingGameVolume = -1.f;
	PendingMusicVolume = -1.f;
}

void SoundMessenger::BeginObject ()
{
	Super::BeginObject();

	Sender = SD::ThreadedComponent::CreateObject();
	if (AddComponent(Sender))
	{
		Sender->InstantiateExternalComp(SoundEngineComponent::SOUND_ENGINE_IDX, []()
		{
			SoundEngineComponent* externalSoundEngine = SoundEngineComponent::Find();
			CHECK(externalSoundEngine != nullptr)

			SoundPlayer* player = externalSoundEngine->GetPlayer();
			CHECK(player != nullptr)

			SD::ThreadedComponent* externalThread = SD::ThreadedComponent::CreateObject();
			if (player->AddComponent(externalThread))
			{
				player->SetReceiver(externalThread);
			}
			else
			{
				externalThread = nullptr;
			}

			return externalThread;
		});

		Sender->SetCopyMethod(SDFUNCTION_1PARAM(this, SoundMessenger, HandleCopyData, void, SD::DataBuffer&));
	}
	else
	{
		Sender = nullptr;
	}

	VolumeLevelSender = SD::ThreadedComponent::CreateObject();
	if (AddComponent(VolumeLevelSender))
	{
		VolumeLevelSender->InstantiateExternalComp(SoundEngineComponent::SOUND_ENGINE_IDX, []()
		{
			SoundEngineComponent* externalSoundEngine = SoundEngineComponent::Find();
			CHECK(externalSoundEngine != nullptr)

			SoundPlayer* player = externalSoundEngine->GetPlayer();
			CHECK(player != nullptr)

			SD::ThreadedComponent* externalThread = SD::ThreadedComponent::CreateObject();
			if (player->AddComponent(externalThread))
			{
				player->SetVolLevelReceiver(externalThread);
			}
			else
			{
				externalThread = nullptr;
			}

			return externalThread;
		});

		VolumeLevelSender->SetCopyMethod(SDFUNCTION_1PARAM(this, SoundMessenger, HandleTransferVolLevel, void, SD::DataBuffer&));
	}
	else
	{
		VolumeLevelSender = nullptr;
	}
}

SoundMessenger* SoundMessenger::GetMessenger ()
{
	DroneDestroyerEngineComponent* droneEngine = DroneDestroyerEngineComponent::Find();
	if (droneEngine == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("SoundMessenger::GetMessenger is called from the wrong thread. This thread does not have an instance of the DroneDestroyerEngineComponent."));
		return nullptr;
	}

	return droneEngine->GetSoundSender();
}

void SoundMessenger::PlaySound (bool isMusic, const SD::DString& soundName, SD::FLOAT volMultiplier)
{
	if (Sender == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to play sound since the messenger does not have a connected ThreadedComponent."));
		return;
	}

	SSoundData newSound;
	newSound.IsMusic = isMusic;
	newSound.SoundName = soundName;
	newSound.SoundVolume = volMultiplier;
	PendingSounds.push_back(newSound);
	Sender->RequestDataTransfer(true); //Send data as soon as possible
}

void SoundMessenger::SetVolumeLevel (bool music, SD::FLOAT newVolume)
{
	if (VolumeLevelSender == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to adjust volume levels since the messenger is missing the threaded component to transfer volume levels."));
		return;
	}

	if (music)
	{
		PendingMusicVolume = SD::Utils::Clamp<SD::FLOAT>(newVolume, 0.f, 1.f);
	}
	else
	{
		PendingGameVolume = SD::Utils::Clamp<SD::FLOAT>(newVolume, 0.f, 1.f);
	}

	VolumeLevelSender->RequestDataTransfer(1.f);
}

void SoundMessenger::HandleCopyData (SD::DataBuffer& outData)
{
	for (size_t i = 0; i < PendingSounds.size(); ++i)
	{
		outData << PendingSounds.at(i).IsMusic;
		outData << PendingSounds.at(i).SoundName;
		outData << PendingSounds.at(i).SoundVolume;
	}
	SD::ContainerUtils::Empty(OUT PendingSounds);
}

void SoundMessenger::HandleTransferVolLevel (SD::DataBuffer& outData)
{
	if (PendingMusicVolume >= 0.f)
	{
		outData << SD::BOOL(true); //Flag this data as music channel
		outData << PendingMusicVolume;
		PendingMusicVolume = -1.f;
	}

	if (PendingGameVolume >= 0.f)
	{
		outData << SD::BOOL(false); //Flag this data as game volume
		outData << PendingGameVolume;
		PendingGameVolume = -1.f;
	}
}
DD_END