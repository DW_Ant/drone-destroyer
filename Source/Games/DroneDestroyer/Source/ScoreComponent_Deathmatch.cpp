/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScoreComponent_Deathmatch.cpp
=====================================================================
*/

#include "PlayerSoul.h"
#include "ScoreComponent_Deathmatch.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, ScoreComponent_Deathmatch, DD, ScoreComponent)

void ScoreComponent_Deathmatch::InitProps ()
{
	Super::InitProps();

	KillsLabel = nullptr;
	DeathsLabel = nullptr;
}

void ScoreComponent_Deathmatch::UpdateInformation (SD::INT rank, PlayerSoul* soul)
{
	Super::UpdateInformation(rank, soul);
	CHECK(soul != nullptr)

	if (KillsLabel != nullptr)
	{
		KillsLabel->SetText(soul->GetNumKills().ToString());
	}

	if (DeathsLabel != nullptr)
	{
		DeathsLabel->SetText(soul->GetNumDeaths().ToString());
	}
}

void ScoreComponent_Deathmatch::SetFontSize (SD::INT newFontSize)
{
	Super::SetFontSize(newFontSize);

	if (KillsLabel != nullptr)
	{
		KillsLabel->SetCharacterSize(FontSize);
	}

	if (DeathsLabel != nullptr)
	{
		DeathsLabel->SetCharacterSize(FontSize);
	}
}

void ScoreComponent_Deathmatch::InitializeComponents ()
{
	Super::InitializeComponents();

	if (Frame != nullptr)
	{
		KillsLabel = SD::LabelComponent::CreateObject();
		if (Frame->AddComponent(KillsLabel))
		{
			KillsLabel->SetAutoRefresh(false);
			KillsLabel->SetPosition(SD::Vector2(0.6f, 0.f));
			KillsLabel->SetSize(SD::Vector2(0.2f, 1.f));
			KillsLabel->SetWrapText(false);
			KillsLabel->SetClampText(false);
			KillsLabel->SetCharacterSize(FontSize);
			KillsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			KillsLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			KillsLabel->SetAutoRefresh(true);
		}
		else
		{
			KillsLabel = nullptr;
		}

		DeathsLabel = SD::LabelComponent::CreateObject();
		if (Frame->AddComponent(DeathsLabel))
		{
			DeathsLabel->SetAutoRefresh(false);
			DeathsLabel->SetPosition(SD::Vector2(0.8f, 0.f));
			DeathsLabel->SetSize(SD::Vector2(0.2f, 1.f));
			DeathsLabel->SetWrapText(false);
			DeathsLabel->SetClampText(false);
			DeathsLabel->SetCharacterSize(FontSize);
			DeathsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			DeathsLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			DeathsLabel->SetAutoRefresh(true);
		}
		else
		{
			DeathsLabel = nullptr;
		}
	}
}
DD_END