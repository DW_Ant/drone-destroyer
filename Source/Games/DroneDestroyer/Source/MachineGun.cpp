/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MachineGun.cpp
=====================================================================
*/

#include "MachineGun.h"
#include "MachineGunBullet.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, MachineGun, DD, Weapon)

void MachineGun::InitProps ()
{
	Super::InitProps();

#ifdef DEBUG_MODE
	DebugName = TXT("MachineGun");
#endif

	ItemNameKey = TXT("MachineGun");
	ShortNameKey = TXT("MachineGunShort");
	PickupImage = TXT("DroneDestroyer.Pickups.Machinegun");
	PossessedImage = TXT("DroneDestroyer.Weapons.Shotgun");
	StartingAmmo = 100;
	Ammo = StartingAmmo;
	MaxAmmo = 800;
	FireRate = 0.1f;
	FireSpread = 5.f;
	ProjectileClass = MachineGunBullet::SStaticClass();
	ShootSound = TXT("W_MachineGun");
	ShootSoundVolume = 0.125f;
	WeaponPriority = 2;
	WeaponSwitchBinds.push_back(sf::Keyboard::Num2);
	WeaponSwitchBinds.push_back(sf::Keyboard::Numpad2);

	SwitchToTime = 0.5f;
	SwitchOutTime = 0.5f;
}
DD_END