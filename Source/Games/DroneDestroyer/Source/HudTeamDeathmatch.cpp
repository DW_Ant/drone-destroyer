/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HudTeamDeathmatch.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "HudTeamDeathmatch.h"
#include "PlayerSoul.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, HudTeamDeathmatch, SD, GuiComponent)

void HudTeamDeathmatch::InitProps ()
{
	Super::InitProps();

	KillsLabel = nullptr;
	DeathsLabel = nullptr;
}

void HudTeamDeathmatch::BeginObject ()
{
	Super::BeginObject();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	KillsPrefix = translator->TranslateText(TXT("Kills"), TRANSLATION_FILE, TXT("Hud"));
	DeathsPrefix = translator->TranslateText(TXT("Deaths"), TRANSLATION_FILE, TXT("Hud"));
}

void HudTeamDeathmatch::InitializeComponents ()
{
	Super::InitializeComponents();

	SD::FrameComponent* frame = SD::FrameComponent::CreateObject();
	if (AddComponent(frame))
	{
		frame->SetPosition(SD::Vector2(0.f, 0.825f));
		frame->SetSize(SD::Vector2(0.2f, 0.075f));
		frame->SetBorderThickness(0.f);
		frame->SetLockedFrame(true);
		if (frame->RenderComponent != nullptr)
		{
			frame->RenderComponent->TopBorder = nullptr;
			frame->RenderComponent->RightBorder = nullptr;
			frame->RenderComponent->BottomBorder = nullptr;
			frame->RenderComponent->LeftBorder = nullptr;
			frame->RenderComponent->TopRightCorner = nullptr;
			frame->RenderComponent->BottomRightCorner = nullptr;
			frame->RenderComponent->BottomLeftCorner = nullptr;
			frame->RenderComponent->TopLeftCorner = nullptr;
			frame->RenderComponent->ClearSpriteTexture();
			frame->RenderComponent->FillColor = SD::Color(64, 64, 64, 150);
		}

		KillsLabel = SD::LabelComponent::CreateObject();
		if (frame->AddComponent(KillsLabel))
		{
			KillsLabel->SetAutoRefresh(false);
			KillsLabel->SetPosition(SD::Vector2(0.f, 0.f));
			KillsLabel->SetSize(SD::Vector2(1.f, 0.5f));
			KillsLabel->SetWrapText(false);
			KillsLabel->SetClampText(true);
			KillsLabel->SetAutoRefresh(true);
		}
		else
		{
			KillsLabel = nullptr;
		}

		DeathsLabel = SD::LabelComponent::CreateObject();
		if (frame->AddComponent(DeathsLabel))
		{
			DeathsLabel->SetAutoRefresh(false);
			DeathsLabel->SetPosition(SD::Vector2(0.f, 0.5f));
			DeathsLabel->SetSize(SD::Vector2(1.f, 0.5f));
			DeathsLabel->SetWrapText(false);
			DeathsLabel->SetClampText(true);
			DeathsLabel->SetAutoRefresh(true);
		}
		else
		{
			DeathsLabel = nullptr;
		}
	}
}

void HudTeamDeathmatch::InitializeTeams (const std::vector<TeamDeathmatch::STeam>& teams)
{
	PlayerSoul* human = nullptr;

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)

	//First find the player to figure out which of the team labels will have the gold outline.
	for (PlayerSoul* soul = localDroneEngine->GetGame()->GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
	{
		if (!soul->GetIsBot())
		{
			if (!soul->GetIsSpectator())
			{
				human = soul;
			}

			break;
		}
	}

	SD::FLOAT spacing = 0.05f;
	SD::FLOAT drawPosY = spacing + 0.1f;
	for (size_t i = 0; i < teams.size(); ++i)
	{
		SD::FrameComponent* teamBackground = SD::FrameComponent::CreateObject();
		if (AddComponent(teamBackground))
		{
			teamBackground->SetSize(SD::Vector2(0.15f, 0.05f));
			teamBackground->SetPosition(SD::Vector2(1.f - teamBackground->ReadSize().X, drawPosY));
			drawPosY += spacing + teamBackground->ReadSize().Y;

			teamBackground->SetLockedFrame(true);
			teamBackground->SetBorderThickness(3.f);
			if (teamBackground->RenderComponent != nullptr)
			{
				teamBackground->RenderComponent->ClearSpriteTexture();
				teamBackground->RenderComponent->FillColor = teams.at(i).TeamColor;
				teamBackground->RenderComponent->FillColor.Source.a = 165;

				if (human != nullptr && human->GetTeamNumber() != teams.at(i).TeamNum)
				{
					teamBackground->RenderComponent->TopBorder = nullptr;
					teamBackground->RenderComponent->RightBorder = nullptr;
					teamBackground->RenderComponent->BottomBorder = nullptr;
					teamBackground->RenderComponent->LeftBorder = nullptr;
					teamBackground->RenderComponent->TopRightCorner = nullptr;
					teamBackground->RenderComponent->BottomRightCorner = nullptr;
					teamBackground->RenderComponent->BottomLeftCorner = nullptr;
					teamBackground->RenderComponent->TopLeftCorner = nullptr;
				}
			}

			SD::LabelComponent* teamScore = SD::LabelComponent::CreateObject();
			if (teamBackground->AddComponent(teamScore))
			{
				teamScore->SetAutoRefresh(false);
				teamScore->SetPosition(SD::Vector2::ZeroVector);
				teamScore->SetSize(SD::Vector2(1.f, 1.f));
				teamScore->SetWrapText(false);
				teamScore->SetClampText(false);
				teamScore->SetCharacterSize(24);
				teamScore->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
				teamScore->SetVerticalAlignment(SD::LabelComponent::VA_Center);
				teamScore->SetAutoRefresh(true);
				TeamLabels.push_back(teamScore);
			}
		}
	}
}

void HudTeamDeathmatch::UpdateScores (const std::vector<TeamDeathmatch::STeam>& teams, PlayerSoul* player)
{
	for (size_t i = 0; i < TeamLabels.size() && i < teams.size(); ++i)
	{
		TeamLabels.at(i)->SetText(teams.at(i).TeamScore.ToString());
	}

	if (player != nullptr)
	{
		if (KillsLabel != nullptr)
		{
			KillsLabel->SetText(KillsPrefix + player->GetNumKills().ToString());
		}

		if (DeathsLabel != nullptr)
		{
			DeathsLabel->SetText(DeathsPrefix + player->GetNumDeaths().ToString());
		}
	}
}

DD_END