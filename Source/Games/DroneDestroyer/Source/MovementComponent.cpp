/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MovementComponent.cpp
=====================================================================
*/

#include "MovementComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, MovementComponent, SD, EntityComponent)

const SD::INT MovementComponent::INPUT_PRIORITY(1000); //Higher than PlayerSoul

void MovementComponent::InitProps ()
{
	Super::InitProps();

	IsActive = true;
	LockedRotation = false;
	Direction = SD::Vector3::ZeroVector;
	DesiredDirection = SD::Vector3::ZeroVector;
	VelocityMagnitude = 1000.f; //10 meters per second
	Acceleration = 0.1f;

	OwningPhysComp = nullptr;
	OwningTransform = nullptr;
	Tick = nullptr;

	MovingUp = false;
	MovingRight = false;
	MovingDown = false;
	MovingLeft = false;
	IsWalking = false;
}

void MovementComponent::BeginObject ()
{
	Super::BeginObject();

	Tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(Tick))
	{
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, MovementComponent, HandleTick, void, SD::FLOAT));
		Tick->SetTicking(false); //Not attached to a PhysicsComponent yet
	}
	else
	{
		Tick = nullptr;
	}
}

bool MovementComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (dynamic_cast<SD::PhysicsComponent*>(ownerCandidate) == nullptr && dynamic_cast<SD::SceneTransform*>(ownerCandidate) == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("MovementCompnents can only be attached to PhysicsComponents and SceneTransform Entities."));
		return false;
	}

	return Super::CanBeAttachedTo(ownerCandidate);
}

void MovementComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwningPhysComp = dynamic_cast<SD::PhysicsComponent*>(newOwner);
	OwningTransform = dynamic_cast<SD::SceneTransform*>(newOwner);
	CHECK(OwningPhysComp != nullptr || OwningTransform != nullptr)

	if (Tick != nullptr)
	{
		Tick->SetTicking(true);
	}
}

void MovementComponent::ComponentDetached ()
{
	OwningPhysComp = nullptr;
	OwningTransform = nullptr;
	if (Tick != nullptr)
	{
		Tick->SetTicking(false);
	}

	Super::ComponentDetached();
}

void MovementComponent::InitializeInputComponent ()
{
	//Ensure there is only one InputComponent
	if (dynamic_cast<SD::InputComponent*>(FindSubComponent(SD::InputComponent::SStaticClass(), false)) != nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to initialize InputComponent to the MovementComponent since this MovementComponent already has an InputComponent."));
		return;
	}

	SD::InputComponent* input = SD::InputComponent::CreateObject();
	if (AddComponent(input))
	{
		input->SetInputPriority(INPUT_PRIORITY);
		input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, MovementComponent, HandleInput, bool, const sf::Event&);
		input->MouseMoveDelegate = SDFUNCTION_3PARAM(this, MovementComponent, HandleMouseMove, void, SD::MousePointer*, const sf::Event::MouseMoveEvent&, const SD::Vector2&);
	}
}

void MovementComponent::SetActive (bool newActive)
{
	IsActive = newActive;
	if (Tick != nullptr)
	{
		Tick->SetTicking(IsActive);
	}

	if (!IsActive)
	{
		MovingUp = false;
		MovingRight = false;
		MovingDown = false;
		MovingLeft = false;
		IsWalking = false;
		DesiredDirection = SD::Vector3::ZeroVector;
	}
}

void MovementComponent::SetLockedRotation (bool newLockedRotation)
{
	LockedRotation = newLockedRotation;
}

void MovementComponent::SetDesiredDirection (const SD::Vector3& newDesiredDirection)
{
	DesiredDirection = newDesiredDirection;
}

void MovementComponent::SetVelocityMagnitude (SD::FLOAT newVelocityMagnitude)
{
	VelocityMagnitude = newVelocityMagnitude;
}

void MovementComponent::SetAcceleration (SD::FLOAT newAcceleration)
{
	Acceleration = newAcceleration;
}

void MovementComponent::ApplyVelocity (SD::FLOAT deltaSec)
{
	CHECK(OwningPhysComp != nullptr || OwningTransform != nullptr)

	SD::Vector3 appliedVelocity(Direction);

	if (appliedVelocity.VSize() > 1.f)
	{
		//If we're moving in multiple directions, ensure we don't run faster than the magnitude.
		appliedVelocity.Normalize();
	}

	SD::Vector3 curVel = appliedVelocity * VelocityMagnitude;

	if (OwningPhysComp != nullptr)
	{
		OwningPhysComp->SetVelocity(curVel);
	}
	else
	{
		OwningTransform->EditTranslation() += (curVel * deltaSec);
	}
}

void MovementComponent::HandleTick (SD::FLOAT deltaSec)
{
	if (DesiredDirection != Direction)
	{
		//Move direction towards DesiredDirection
		SD::Vector3 deltaDir = (DesiredDirection - Direction);

		//Use Utils::Min to ensure we don't overshoot our target.
		deltaDir.SetLengthTo(SD::Utils::Min(deltaSec / Acceleration, deltaDir.VSize()));

		Direction += deltaDir;
	}

	ApplyVelocity(deltaSec);
}

void MovementComponent::ComputeDesiredDirection ()
{
	SD::Vector3 newDirection(SD::Vector3::ZeroVector);

	if (MovingUp)
	{
		newDirection.Y -= 1.f;
	}

	if (MovingRight)
	{
		newDirection.X += 1.f;
	}

	if (MovingDown)
	{
		newDirection.Y += 1.f;
	}

	if (MovingLeft)
	{
		newDirection.X -= 1.f;
	}

	if (IsWalking)
	{
		newDirection *= 0.5f;
	}

	SetDesiredDirection(newDirection);
}

bool MovementComponent::HandleInput (const sf::Event& evnt)
{
	if (!IsActive)
	{
		return false;
	}

	bool consumeInput = false;

	if (evnt.type == sf::Event::EventType::KeyPressed || evnt.type == sf::Event::EventType::KeyReleased)
	{
		bool isPressed = (evnt.type == sf::Event::EventType::KeyPressed);

		switch (evnt.key.code)
		{
			case(sf::Keyboard::Key::W):
			case(sf::Keyboard::Key::Up):
				MovingUp = isPressed;
				consumeInput = true;
				break;

			case(sf::Keyboard::Key::D):
			case(sf::Keyboard::Key::Right):
				MovingRight = isPressed;
				consumeInput = true;
				break;

			case(sf::Keyboard::Key::S):
			case(sf::Keyboard::Key::Down):
				MovingDown = isPressed;
				consumeInput = true;
				break;

			case(sf::Keyboard::Key::A):
			case(sf::Keyboard::Key::Left):
				MovingLeft = isPressed;
				consumeInput = true;
				break;

			case(sf::Keyboard::Key::LShift):
			case(sf::Keyboard::Key::RShift):
				IsWalking = isPressed;
				consumeInput = true;
				break;
		}

		ComputeDesiredDirection();
	}

	return consumeInput;
}

void MovementComponent::HandleMouseMove (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const SD::Vector2& deltaMove)
{
	if (!IsActive || LockedRotation)
	{
		return;
	}

	SD::GraphicsEngineComponent* localGraphicsEngine = SD::GraphicsEngineComponent::Find();
	CHECK(localGraphicsEngine != nullptr)

	SD::Window* primaryWindow = localGraphicsEngine->GetPrimaryWindow();
	if (primaryWindow == nullptr)
	{
		return;
	}

	//Assume the camera is centered around the player (so that aim is not messed up during camera shake).
	SD::Vector2 deltaFromCenter = mouse->GetPosition() - (primaryWindow->GetSize() * 0.5f);
	if (!deltaFromCenter.IsEmpty())
	{
		SD::SceneTransform* rootTransform = dynamic_cast<SD::SceneTransform*>(GetRootEntity());
		if (rootTransform != nullptr)
		{
			rootTransform->SetRotation(SD::Rotator(deltaFromCenter.ToVector3()));
		}
	}
}

DD_END