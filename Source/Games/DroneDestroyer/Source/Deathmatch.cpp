/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Deathmatch.cpp
=====================================================================
*/

#include "Deathmatch.h"
#include "Hud.h"
#include "HudDeathmatch.h"
#include "PlayerSoul.h"
#include "Scoreboard_Deathmatch.h"
#include "TimeLimitComponent.h"
#include "TimeLimitHudComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Deathmatch, DD, GameRules)

void Deathmatch::InitProps ()
{
	Super::InitProps();

	KillLimit = 10;
	TimeLimit = -1.f;
	InSuddenDeath = false;
	TimeComponent = nullptr;

	IconTextureName = TXT("DroneDestroyer.Interface.Deathmatch");
	DescriptionKey = TXT("Deathmatch");
	ConfigName = TXT("Deathmatch");

	GameSettings.push_back(SGameRulesOption(TXT("KillLimit"), KillLimit));
	GameSettings.push_back(SGameRulesOption(TXT("TimeLimit"), 5));
}

bool Deathmatch::CanBeSelected () const
{
	return true;
}

void Deathmatch::ApplyGameSettings (const std::vector<SD::DString>& newSettings)
{
	CHECK(newSettings.size() == 2)
	KillLimit = std::atoi(newSettings.at(0).ToCString());
	TimeLimit = std::stof(newSettings.at(1).ToCString()) * 60.f;
}

void Deathmatch::InitializeGame ()
{
	Super::InitializeGame();

	//Hack: Set everyone's team number to be negative to make it a free for all
	for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
	{
		soul->SetTeamNumber(-1);
	}

	TimeComponent = TimeLimitComponent::CreateObject();
	if (AddComponent(TimeComponent))
	{
		TimeComponent->OnExpired = SDFUNCTION(this, Deathmatch, HandleTimeLimitExpired, void);
		TimeComponent->SetTimeLimit(TimeLimit);
	}
	else
	{
		TimeComponent = nullptr;
	}
}

void Deathmatch::AddComponentsToHud (Hud* playerHud)
{
	Super::AddComponentsToHud(playerHud);

	if (playerHud != nullptr)
	{
		if (TimeComponent != nullptr)
		{
			TimeLimitHudComponent* timeHud = TimeLimitHudComponent::CreateObject();
			if (playerHud->AddComponent(timeHud))
			{
				timeHud->BindClockTo(TimeComponent);
			}
		}

		HudScore = HudDeathmatch::CreateObject();
		if (playerHud->AddComponent(HudScore.Get()))
		{
			//Place above the health
			HudScore->SetPosition(SD::Vector2(0.f, 0.775f));
			HudScore->SetSize(SD::Vector2(0.2f, 0.125f));
			HudScore->SetVisibility(false); //Hides from spectators and start of the match. This will become visible as soon as there's an update.
		}
	}
}

void Deathmatch::ScoreKill (Character* killed, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg)
{
	Super::ScoreKill(killed, killer, killedBy, deathMsg);

	if (killer != nullptr && (InSuddenDeath || killer->GetNumKills() >= KillLimit))
	{
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr);

		SD::DString translationKey = (InSuddenDeath) ? TXT("SuddenDeathKill") : TXT("ReachedKillLimit");
		SD::DString endGameMsg = translator->TranslateText(translationKey, TRANSLATION_FILE, TXT("Deathmatch"));
		endGameMsg.ReplaceInline(TXT("%s"), killer->ReadPlayerName(), SD::DString::CC_IgnoreCase);

		SD::Vector3 camLocation(SD::Vector3::ZeroVector);
		if (killer->GetBody() != nullptr)
		{
			camLocation = killer->GetBody()->ReadAbsTranslation();
		}
		else if (killed != nullptr)
		{
			camLocation = killed->ReadAbsTranslation();
		}

		ExecuteVictorySequence(endGameMsg, camLocation);
	}
}

void Deathmatch::UpdateScores ()
{
	Super::UpdateScores();

	if (HudScore != nullptr)
	{
		HudScore->UpdateScores(ReadRankedPlayers());
	}
}

void Deathmatch::SortScores ()
{
	Super::SortScores();

	std::sort(RankedPlayers.begin(), RankedPlayers.end(), [](const SD::DPointer<PlayerSoul>& a, const SD::DPointer<PlayerSoul>& b)
	{
		//Prioritize on kills
		if (a->GetNumKills() != b->GetNumKills())
		{
			return (a->GetNumKills() > b->GetNumKills());
		}
		
		//Otherwise sort by number of deaths
		return (a->GetNumDeaths() < b->GetNumDeaths());
	});
}

const Scoreboard* Deathmatch::GetScoreboardClass () const
{
	return dynamic_cast<const Scoreboard_Deathmatch*>(Scoreboard_Deathmatch::SStaticClass()->GetDefaultObject());
}

bool Deathmatch::CanCreateBody (PlayerSoul* requester)
{
	if (!Super::CanCreateBody(requester))
	{
		return false;
	}

	return !InSuddenDeath;
}

void Deathmatch::SuddenDeath ()
{
	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	//Handle case if there are zero or one players
	if (RankedPlayers.size() < 2)
	{
		SD::DString endGameMsg;
		SD::Vector3 camLocation(SD::Vector3::ZeroVector);
		if (RankedPlayers.size() == 0)
		{
			endGameMsg = translator->TranslateText(TXT("SuddenDeathNoVictor"), TRANSLATION_FILE, TXT("Deathmatch"));
		}
		else if (RankedPlayers.size() == 1)
		{
			endGameMsg = translator->TranslateText(TXT("SuddenDeathOneVictor"), TRANSLATION_FILE, TXT("Deathmatch"));
			endGameMsg.ReplaceInline(TXT("%s"), RankedPlayers.at(0)->ReadPlayerName(), SD::DString::CC_IgnoreCase);

			if (RankedPlayers.at(0)->GetBody() != nullptr)
			{
				camLocation = RankedPlayers.at(0)->GetBody()->ReadAbsTranslation();
			}
		}

		ExecuteVictorySequence(endGameMsg, camLocation);
		return;
	}

	bool tieGame = (RankedPlayers.at(0)->GetNumKills() == RankedPlayers.at(1)->GetNumKills());
	if (!tieGame)
	{
		SD::DString endGameMsg = translator->TranslateText(TXT("SuddenDeathVictor"), TRANSLATION_FILE, TXT("Deathmatch"));
		endGameMsg.ReplaceInline(TXT("%s"), RankedPlayers.at(0)->ReadPlayerName(), SD::DString::CC_IgnoreCase);
		SD::Vector3 camLocation = (RankedPlayers.at(0)->GetBody() != nullptr) ? RankedPlayers.at(0)->GetBody()->ReadAbsTranslation() : SD::Vector3::ZeroVector;
		ExecuteVictorySequence(endGameMsg, camLocation);
		return;
	}

	//There's a tie game. First eliminate everyone that doesn't have the highest score. 
	SD::INT highScore = RankedPlayers.at(0)->GetNumKills();

	//Iterate through the soul linked list instead of RankedPlayers since we may kill players (which may sort the RankedPlayers vector)
	for (PlayerSoul* soul = GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
	{
		if (soul->GetNumKills() < highScore && soul->IsAlive())
		{
			soul->GetBody()->Kill(999, nullptr, nullptr, TXT("SuddenDeath"));
		}
	}

	InSuddenDeath = true; //Disables respawning and sets the victory condition to be whoever makes the next kill wins.
}

void Deathmatch::HandleTimeLimitExpired ()
{
	SuddenDeath();
}

DD_END