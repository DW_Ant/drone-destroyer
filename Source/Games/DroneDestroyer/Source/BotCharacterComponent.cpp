/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BotCharacterComponent.cpp
=====================================================================
*/

#include "Arena.h"
#include "BotCharacterComponent.h"
#include "Character.h"
#include "Drone.h"
#include "DroneDestroyerEngineComponent.h"
#include "Extinction.h"
#include "GameRules.h"
#include "HealthComponent.h"
#include "MovementComponent.h"
#include "NavigationComponent.h"
#include "PlayerSoul.h"
#include "Projectile.h"
#include "Weapon.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, BotCharacterComponent, SD, EntityComponent)

const SD::INT BotCharacterComponent::DODGE_PRIORITY(-512);

void BotCharacterComponent::InitProps ()
{
	Super::InitProps();

	SightRadius = 1200.f; //A little over 1 screen radius
	CombatSightRadius = 1500.f;
	NavigationSightRadius = 2500.f; //A little beyond 1 screen width
	RotationRate = 90.f;

	SkillLevel = GameRules::EGameDifficulty::Unknown;
	BotState = BS_Roaming;
	PrioritizePickupsWhenRoaming = true;

	SD::FLOAT processingVariation = 0.25f;

	//Ensure the navigation is fast enough where they can't overshoot their destination between ticks
	ProcessNavigationInterval = 0.1f;
	ProcessCombatInterval = 0.5f + SD::RandomUtils::RandRange(-processingVariation, processingVariation);
	ProcessPerceptionInterval = 2.5f + SD::RandomUtils::RandRange(-processingVariation, processingVariation);

	ObscuredEnemyBeforeForgetting = 3;
	CheckForObscuredEnemies = true;
	CombatDodgeInterval.Min = 0.5f;
	CombatDodgeInterval.Max = 1.25f;
	DesiredDestination = SD::Vector3::ZeroVector;
	NavigationReach = 50.f;
	DestinationPriority = 0;
	RoamSpeedMultiplier = 1.f;
	CombatSpeedMultiplier = 1.f;
#ifdef DEBUG_MODE
	DesiredLocationDebugger = nullptr;
#endif

	for (size_t i = 0; i < 3; ++i)
	{
		PastDestinations.push_back(SD::Vector3::ZeroVector);
	}
	PastDestinationIdx = PastDestinations.size() - 1;
	LookAtThreshold = 5.f;
	ObscuredEnemyCounter = 0;
	CombatNextDodgeTime = 0.f;
	StuckCounter = 0;
	MaxStuckCounter = 2;
	StuckCheckInterval = SD::RandomUtils::RandRange(0.25f, 0.75f);
}

void BotCharacterComponent::BeginObject ()
{
	Super::BeginObject();

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame())
	SetSkillLevel(localDroneEngine->GetGame()->GetGameDifficulty());

	SD::TickComponent* navigationTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(navigationTick))
	{
		navigationTick->SetTickInterval(ProcessNavigationInterval);
		navigationTick->SetTickHandler(SDFUNCTION_1PARAM(this, BotCharacterComponent, HandleNavigationTick, void, SD::FLOAT));
	}

	SD::TickComponent* stuckTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(stuckTick))
	{
		stuckTick->SetTickInterval(StuckCheckInterval);
		stuckTick->SetTickHandler(SDFUNCTION_1PARAM(this, BotCharacterComponent, HandleStuckTick, void, SD::FLOAT));
	}

	SD::TickComponent* combatTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(combatTick))
	{
		combatTick->SetTickInterval(ProcessCombatInterval);
		combatTick->SetTickHandler(SDFUNCTION_1PARAM(this, BotCharacterComponent, HandleCombatTick, void, SD::FLOAT));
	}

	SD::TickComponent* perceptionTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(perceptionTick))
	{
		perceptionTick->SetTickInterval(ProcessPerceptionInterval);
		perceptionTick->SetTickHandler(SDFUNCTION_1PARAM(this, BotCharacterComponent, HandlePerceptionTick, void, SD::FLOAT));
	}

	SD::TickComponent* rotationTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(rotationTick))
	{
		rotationTick->SetTickHandler(SDFUNCTION_1PARAM(this, BotCharacterComponent, HandleRotationTick, void, SD::FLOAT));
	}

	if (dynamic_cast<Extinction*>(localDroneEngine->GetGame()) != nullptr)
	{
		//Drones and projectiles in extinction mode ignore geometry. Ignore obscured enemies to permit bots to see through walls.
		CheckForObscuredEnemies = false;
	}

#ifdef DEBUG_MODE
	bool debugDesiredLocation = false;
	if (debugDesiredLocation)
	{
		DesiredLocationDebugger = SD::SceneEntity::CreateObject();
		DesiredLocationDebugger->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_DEBUG_SHAPES;

		SD::SolidColorRenderComponent* locationDebugger = SD::SolidColorRenderComponent::CreateObject();
		if (DesiredLocationDebugger->AddComponent(locationDebugger))
		{
			locationDebugger->SolidColor = SD::Color::WHITE;
			locationDebugger->BaseSize = 50.f;
			locationDebugger->Shape = SD::SolidColorRenderComponent::ST_Circle;
			locationDebugger->CenterOrigin = true;
			DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(locationDebugger);
		}
	}
#endif
}

bool BotCharacterComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	return (dynamic_cast<Character*>(ownerCandidate) != nullptr);
}

void BotCharacterComponent::AttachTo (Entity* newOwner)
{
	Super::AttachTo(newOwner);

	OwningBody = dynamic_cast<Character*>(newOwner);
	CHECK(OwningBody != nullptr)

	if (HealthComponent* bodyHealth = OwningBody->GetHealth())
	{
		bodyHealth->OnTakeDamage.RegisterHandler(SDFUNCTION_3PARAM(this, BotCharacterComponent, HandleTakeDamage, void, SD::INT, PlayerSoul*, Projectile*));
	}
}

void BotCharacterComponent::ComponentDetached ()
{
	if (OwningBody != nullptr)
	{
		if (HealthComponent* bodyHealth = OwningBody->GetHealth())
		{
			bodyHealth->OnTakeDamage.UnregisterHandler(SDFUNCTION_3PARAM(this, BotCharacterComponent, HandleTakeDamage, void, SD::INT, PlayerSoul*, Projectile*));
		}

		OwningBody = nullptr;
	}

	Super::ComponentDetached();
}

void BotCharacterComponent::Destroyed ()
{
#ifdef DEBUG_MODE
	if (DesiredLocationDebugger != nullptr)
	{
		DesiredLocationDebugger->Destroy();
		DesiredLocationDebugger = nullptr;
	}
#endif

	Super::Destroyed();
}

bool BotCharacterComponent::IsAbleToFight () const
{
	return (OwningBody != nullptr && OwningBody->GetDrawnWeapon() != nullptr && OwningBody->GetDrawnWeapon()->GetAmmo() > 0);
}

void BotCharacterComponent::Attack (Character* newEnemy)
{
	if (newEnemy != nullptr && newEnemy->IsAlive() && newEnemy != OwningBody && IsAbleToFight())
	{
		Enemy = newEnemy;
		ObscuredEnemyCounter = 0;
		BotState = BS_Combat;
	}
}

void BotCharacterComponent::SetSkillLevel (GameRules::EGameDifficulty newSkillLevel)
{
	if (SkillLevel == newSkillLevel)
	{
		return;
	}

	const BotCharacterComponent* cdo = dynamic_cast<const BotCharacterComponent*>(StaticClass()->GetDefaultObject());
	CHECK(cdo != nullptr);

	//Reset multipliers to defaults before applying skill modifiers in case difficulty was set multiple times.
	SightRadius = cdo->SightRadius;
	CombatSightRadius = cdo->CombatSightRadius;
	RoamSpeedMultiplier = cdo->RoamSpeedMultiplier;
	CombatSpeedMultiplier = cdo->CombatSpeedMultiplier;
	RotationRate = cdo->RotationRate;
	CombatDodgeInterval = cdo->CombatDodgeInterval;
	NavigationReach = cdo->NavigationReach;

	SkillLevel = newSkillLevel;

	switch (SkillLevel)
	{
		case(GameRules::Pacifist):
			SightRadius *= 0.1f; //Blind
			CombatSightRadius = 0.f; //Don't look for items during combat
			RoamSpeedMultiplier = 0.5f; //Always walk
			CombatSpeedMultiplier = 0.5f; //Always walk
			break;

		case(GameRules::Novice):
			SightRadius *= 0.5f; //pretty blind
			CombatSightRadius = 0.f; //Don't look for items during combat
			RotationRate *= 0.75f; //Slow rotation
			RoamSpeedMultiplier = 0.5f; //Always walk
			CombatSpeedMultiplier = 0.f; //Stand still when fighting
			break;

		case(GameRules::Easy):
			SightRadius *= 0.75f;
			CombatSightRadius = 0.f; //Don't look for items during combat
			RotationRate *= 0.75f;
			RoamSpeedMultiplier = 0.5f;
			CombatSpeedMultiplier = 0.5f;
			CombatDodgeInterval.Min /= CombatSpeedMultiplier;
			CombatDodgeInterval.Max /= CombatSpeedMultiplier;
			break;

		case(GameRules::Medium):
			RotationRate *= 0.9f;
			RoamSpeedMultiplier = 0.75f;
			CombatSpeedMultiplier = 0.75f;
			CombatDodgeInterval.Min /= CombatSpeedMultiplier;
			CombatDodgeInterval.Max /= CombatSpeedMultiplier;
			NavigationReach *= 2.f;
			break;

		case(GameRules::Hard):
			SightRadius *= 1.5f; //Cheaters. They can see beyond screen width a bit.
			RotationRate *= 1.5f;
			RoamSpeedMultiplier = 1.f; //Always running
			CombatSpeedMultiplier = 1.f; //Always running
			NavigationReach *= 4.f; //Prevent fast moving bots from 'orbiting' around a navigation point
			break;
	}
}

void BotCharacterComponent::ProcessNavigation ()
{
	if (OwningBody == nullptr)
	{
		//This can happen if OwningBody was destroyed on the same frame as ProcessNavigation was called.
		return;
	}

	if (DestinationPriority == DODGE_PRIORITY)
	{
		//Doing a dodge. Don't interrupt.
		return;
	}

	if (DesiredDestination.IsEmpty() || (DesiredDestination - OwningBody->GetAbsTranslation()).VSize(false) < NavigationReach)
	{
		ReachDestination();

		if (Enemy == nullptr)
		{
			//If idling, do something else
			FindRoamDestination(true);
		}
	}

	//Update MovementComponent's to head towards navigation
	MovementComponent* movement = OwningBody->GetMovement();
	if (movement != nullptr)
	{
		SD::Vector3 newDirection = DesiredDestination - OwningBody->GetAbsTranslation();
		newDirection.Z = 0.f;
		if (!newDirection.IsEmpty())
		{
			newDirection.Normalize();
		}

		newDirection *= (Enemy != nullptr) ? CombatSpeedMultiplier : RoamSpeedMultiplier;
		movement->SetDesiredDirection(newDirection);
	}
}

void BotCharacterComponent::ProcessStuckCheck ()
{
	if (OwningBody == nullptr)
	{
		return;
	}

	SD::Vector3 curLocation = OwningBody->GetTranslation();
	if ((curLocation - LastStuckLocation).VSize() <= OwningBody->GetCollisionRadius() * 2.f)
	{
		StuckCounter++;
		if (StuckCounter >= MaxStuckCounter)
		{
			ReachDestination();
			FindRoamDestination(false);
			StuckCounter = 0;
		}
	}
	else
	{
		StuckCounter = 0;
		LastStuckLocation = curLocation;
	}
}

void BotCharacterComponent::ProcessCombat ()
{
	if (Enemy == nullptr || OwningBody == nullptr)
	{
		if (BotState == BS_Combat)
		{
			//Either this bot died or someone else killed their Enemy. Inform this bot that they're no longer fighting.
			EndCombat();
		}

		return;
	}

	if (!Enemy->IsAlive())
	{
		EndCombat();
		return;
	}

	if (SkillLevel == GameRules::Pacifist)
	{
		//Throw weapon
		bool throwLeft = (SD::RandomUtils::fRand() > 0.5f);
		if (throwLeft)
		{
			OwningBody->EditRotation().Yaw += SD::Rotator::QUARTER_REVOLUTION;
		}
		else
		{
			OwningBody->EditRotation().Yaw -= SD::Rotator::QUARTER_REVOLUTION;
		}

		OwningBody->ThrowWeapon();

		//Fix rotation
		if (throwLeft)
		{
			OwningBody->EditRotation().Yaw -= SD::Rotator::QUARTER_REVOLUTION;
		}
		else
		{
			OwningBody->EditRotation().Yaw += SD::Rotator::QUARTER_REVOLUTION;
		}

		//Forget the enemy
		EndCombat();
		return;
	}

	if (!IsAbleToFight())
	{
		//Forget the enemy. Go back to roaming. We need to find a new pickup.
		EndCombat();
		return;
	}

	Weapon* currentWeapon = OwningBody->GetDrawnWeapon();

	//Check if the Enemy is still in sight
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetEnvironment() != nullptr)
	if (CheckForObscuredEnemies && localDroneEngine->GetEnvironment()->FastTrace(OwningBody->ReadAbsTranslation(), Enemy->ReadAbsTranslation()))
	{
		ObscuredEnemyCounter++;
		if (currentWeapon != nullptr)
		{
			currentWeapon->StopFiring();
		}

		if (ObscuredEnemyCounter >= ObscuredEnemyBeforeForgetting)
		{
			//The enemy is hidden for long enough. Time to roam around.
			ObscuredEnemyCounter = 0;
			EndCombat();
			return;
		}
	}
	else
	{
		if (currentWeapon != nullptr && currentWeapon->CanShoot() && !currentWeapon->IsShooting())
		{
			currentWeapon->StartFiring();
		}

		ObscuredEnemyCounter = 0;
	}

	// If I'm not heading towards a desired item, perform a dodge.
	if (OwningBody->GetMovement() != nullptr && DestinationPriority <= 1)
	{
		if (CombatSpeedMultiplier > 0.f)
		{
			SD::Engine* localEngine = SD::Engine::FindEngine();
			CHECK(localEngine != nullptr)

			if (localEngine->GetElapsedTime() >= CombatNextDodgeTime)
			{
				SD::Vector3 newDirection = PerformDodge();
				newDirection.Z = 0.f;
				if (!newDirection.IsEmpty())
				{
					newDirection.Normalize();
					newDirection *= CombatSpeedMultiplier;
				}

				OwningBody->GetMovement()->SetDesiredDirection(newDirection);
				DestinationPriority = DODGE_PRIORITY;
				CombatNextDodgeTime = localEngine->GetElapsedTime() + SD::RandomUtils::RandRange(CombatDodgeInterval.Min, CombatDodgeInterval.Max);
			}
		}
		else
		{
			//Stand still
			OwningBody->GetMovement()->SetDesiredDirection(SD::Vector3::ZeroVector);
			DestinationPriority = DODGE_PRIORITY;
		}
	}
}

void BotCharacterComponent::ProcessPerception ()
{
	//Iterate through the Character list to identify the closest hostile.
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)

	if (OwningBody == nullptr)
	{
		return;
	}

	if (Enemy != nullptr)
	{
		//Check if the enemy travelled too far away
		SD::FLOAT enemyDist = (Enemy->ReadAbsTranslation() - OwningBody->ReadAbsTranslation()).VSize(false);
		if (enemyDist > SightRadius * 1.25f)
		{
			//Enemy must have travelled too far
			EndCombat();
		}
		else if (CombatSightRadius > 0.f && DestinationPriority <= 1)
		{
			//Look around for any goodies
			CHECK(localDroneEngine->GetEnvironment() != nullptr);

			SD::INT bestInvPriority = 1; //Must be better than a useless pickup
			NavigationComponent* bestInv = nullptr;

			//Although we're still in combat, look for any valuable items nearby
			for (NavigationComponent* nav = localDroneEngine->GetEnvironment()->GetFirstNavigation(); nav != nullptr; nav = nav->GetNextNavigation())
			{
				if (Inventory* inv = dynamic_cast<Inventory*>(nav->GetOwner()))
				{
					SD::FLOAT dist = (nav->GetSceneTransform()->GetTranslation() - OwningBody->ReadAbsTranslation()).VSize(false);
					if (dist < CombatSightRadius && inv->CanPickupItem(OwningBody.Get()))
					{
						SD::INT priority = inv->GetImportanceValue(OwningBody.Get());
						if (priority > bestInvPriority)
						{
							bestInvPriority = priority;
							bestInv = nav;
						}
					}
				}
			}

			if (bestInv != nullptr)
			{
				DesiredDestination = SD::RandomUtils::RandPointWithinCircle(bestInv->GetWanderRadius()).ToVector3() + bestInv->GetSceneTransform()->ReadTranslation();
				DestinationPriority = bestInvPriority;
			}
		}
	}

	if (!IsAbleToFight())
	{
		//Don't bother looking for new enemies if we can't fight them.
		return;
	}

	SD::FLOAT bestDistSquared;
	Character* closestCharacter = FindEnemy(localDroneEngine->GetGame(), OUT bestDistSquared);
	if (Enemy != nullptr && Enemy->IsAlive())
	{
		if (closestCharacter == Enemy)
		{
			//Already fighting the closest character
			return;
		}

		SD::FLOAT enemyDistSquared = (Enemy->GetAbsTranslation() - OwningBody->GetAbsTranslation()).CalcDistSquared(false);
		if (bestDistSquared * 2.f < enemyDistSquared)
		{
			//The new character must be significantly closer compared to the current enemy to consider switching targets.
			Attack(closestCharacter);
		}
	}
	else
	{
		Attack(closestCharacter);
	}
}

Character* BotCharacterComponent::FindEnemy (GameRules* game, SD::FLOAT& outBestDistSquared)
{
	Character* result = nullptr;
	SD::FLOAT bestDistSquared = SightRadius * SightRadius;

	bool searchForDrones = false;
	Extinction* extinction = nullptr;
	if (extinction = dynamic_cast<Extinction*>(game))
	{
		//If I'm a drone, still search for humans. Otherwise search for drones.
		searchForDrones = (dynamic_cast<Drone*>(OwningBody.Get()) == nullptr);
	}

	//I know this is bad, but I'm in crunch. I got one more day to finish this so I'm adding some bad hacks to get this done quickly!
	if (searchForDrones)
	{
		CHECK(extinction != nullptr)

		//Search for a random drone instead of other players (Drones don't have souls so they cannot be found through the player linked list).
		for (Drone* drone : extinction->ReadDrones())
		{
			if (!drone->IsAlive())
			{
				continue;
			}

			SD::FLOAT distSquared = (drone->ReadAbsTranslation() - OwningBody->ReadAbsTranslation()).CalcDistSquared(false);
			if (distSquared < SightRadius * SightRadius)
			{
				bestDistSquared = distSquared;
				result = drone;
			}
		}
	}
	else
	{
		for (PlayerSoul* soul = game->GetFirstPlayer(); soul != nullptr; soul = soul->GetNextSoul())
		{
			if (!soul->IsAlive() || soul->GetBody() == OwningBody || OwningBody->IsFriendlyTo(soul->GetBody()))
			{
				continue;
			}

			SD::FLOAT distSquared = (soul->GetBody()->ReadAbsTranslation() - OwningBody->ReadAbsTranslation()).CalcDistSquared(false);
			if (distSquared < bestDistSquared)
			{
				bestDistSquared = distSquared;
				result = soul->GetBody();
			}
		}
	}

	return result;
}

void BotCharacterComponent::ReachDestination ()
{
	//Record that this bot reached their destination
	++PastDestinationIdx;
	PastDestinationIdx %= PastDestinations.size();
	PastDestinations.at(PastDestinationIdx) = DesiredDestination;

	//Permit dodging if this bot was obtaining an item during combat.
	DesiredDestination = SD::Vector3::ZeroVector;
	DestinationPriority = 0;
}

void BotCharacterComponent::FindRoamDestination (bool prioritizePickups)
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetEnvironment() != nullptr)

	if (OwningBody == nullptr)
	{
		//Could have been called from EndCombat (bot and its Enemy killed each other at the same time).
		return;
	}

	if (!PrioritizePickupsWhenRoaming)
	{
		prioritizePickups = false;
	}

	NavigationComponent* closestPoint = nullptr;
	SD::FLOAT closestDist = NavigationSightRadius;

	for (NavigationComponent* nav = localDroneEngine->GetEnvironment()->GetFirstNavigation(); nav != nullptr; nav = nav->GetNextNavigation())
	{
		SD::FLOAT dist = (OwningBody->GetAbsTranslation() - nav->GetSceneTransform()->ReadTranslation()).VSize(false);
		if (dist < closestDist)
		{
			closestDist = dist;
			closestPoint = nav;
		}
	}

	if (closestPoint != nullptr)
	{
		std::vector<SNavigationCandidateInfo> candidates;

		//If priority items are not found, find a NavigationPoint this bot did not traverse to recently.
		for (NavigationComponent* connectedPoint : closestPoint->ReadConnectedPoints())
		{
			SD::INT priority = 1;

			if (Inventory* inv = dynamic_cast<Inventory*>(connectedPoint->GetOwner()))
			{
				if (prioritizePickups)
				{
					if (inv->CanPickupItem(OwningBody.Get()) && SkillLevel > GameRules::Pacifist) /* Pacifists should ignore items */
					{
						//Inspect the inventory entity to see if this is something the bot should desire.
						priority = inv->GetImportanceValue(OwningBody.Get());
					}
					else
					{
						priority = -1; //Treat as if this is not a desired path to run to since we can't take this item.
					}
				}
			}
			else if (prioritizePickups) //This Navigation point does not lead to an inventory item, treat it as an ordinary roaming point.
			{
				for (const SD::Vector3& destHistory : PastDestinations)
				{
					SD::FLOAT dist = (connectedPoint->GetSceneTransform()->ReadTranslation() - destHistory).VSize(false);
					if (dist <= connectedPoint->GetWanderRadius())
					{
						//This past destination is too close to this navigation point. Move to low priority
						priority = 0;
						break;
					}
				}
			}

			candidates.push_back(SNavigationCandidateInfo(priority, connectedPoint));
		}

		if (!SD::ContainerUtils::IsEmpty(candidates))
		{
			SD::INT bestPriority = -1;
			for (size_t i = 0; i < candidates.size(); ++i)
			{
				bestPriority = SD::Utils::Max(candidates.at(i).Priority, bestPriority);
			}

			//Drop any candidates that do not match this priority
			size_t i = 0;
			while (i < candidates.size())
			{
				if (candidates.at(i).Priority < bestPriority)
				{
					candidates.erase(candidates.begin() + i);
					continue;
				}

				++i;
			}
			
			CHECK(!SD::ContainerUtils::IsEmpty(candidates)) //should never happen

			//Pick a random node
			NavigationComponent* chosenNode = candidates.at(SD::RandomUtils::Rand(SD::INT(candidates.size())).ToUnsignedInt()).NavPoint;
			DestinationPriority = bestPriority;
			CHECK(chosenNode != nullptr); //should never happen
			DesiredDestination = SD::RandomUtils::RandPointWithinCircle(chosenNode->GetWanderRadius()).ToVector3() + chosenNode->GetSceneTransform()->ReadTranslation();

			const SD::Vector3& nodeLocation = chosenNode->GetSceneTransform()->ReadTranslation();
			b2RayCastInput rayCast;
			rayCast.maxFraction = 1.f;
			rayCast.p1 = SD::Box2dUtils::ToBox2dCoordinates(nodeLocation);
			rayCast.p2 = SD::Box2dUtils::ToBox2dCoordinates(DesiredDestination);
			b2RayCastOutput hitLocation;
			if (localDroneEngine->GetEnvironment()->ArenaTrace(rayCast, &hitLocation))
			{
				//We collided against the geometry, figure out the impact point
				SD::Vector3 destDir = (DesiredDestination - nodeLocation);
				destDir.Normalize();
				destDir *= hitLocation.fraction * 0.9f;
				DesiredDestination = destDir + nodeLocation;
			}
		}
	}

	if (DesiredDestination.IsEmpty())
	{
		//No valid spots? The bot is lost now
		DesiredDestination = NavigateFromLost();
	}

	DesiredDestination.Z = OwningBody->ReadAbsTranslation().Z;
}

SD::Vector3 BotCharacterComponent::NavigateFromLost ()
{
	//Pick a random area around self.
	return SD::RandomUtils::RandPointWithinCircle(NavigationSightRadius).ToVector3() + OwningBody->ReadAbsTranslation();
}

SD::Vector3 BotCharacterComponent::PerformDodge () const
{
	CHECK(Enemy != nullptr && OwningBody != nullptr && OwningBody->GetMovement() != nullptr)

	SD::Rotator enemyDir(OwningBody->ReadAbsTranslation() - Enemy->ReadAbsTranslation());
	unsigned int originalYaw = enemyDir.Yaw;
	SD::FLOAT dodgeProjection = OwningBody->GetMovement()->GetVelocityMagnitude() * CombatDodgeInterval.Max;

	/*
	The algorithm will look at six different points that are flanking this bot to figure out possible dodge directions.
	The bot will favor points that are more away from walls.
	    Enemy
	      X
	  P   |   P
	P     Y     P
	  P  Bot  P

	If Enemy is at X and Bot is at Y, this algorithm will look at the points at P.
	*/

	std::vector<SD::Vector3> dodgePositions;
	enemyDir.Yaw += SD::Rotator::FULL_REVOLUTION / 6; //60 degrees
	dodgePositions.push_back(enemyDir.GetDirectionalVector() * dodgeProjection);
	enemyDir.Yaw = originalYaw + SD::Rotator::QUARTER_REVOLUTION; //90 degrees
	dodgePositions.push_back(enemyDir.GetDirectionalVector() * dodgeProjection);
	enemyDir.Yaw = originalYaw + (SD::Rotator::FULL_REVOLUTION / 3); //120 degrees
	dodgePositions.push_back(enemyDir.GetDirectionalVector() * dodgeProjection);
	enemyDir.Yaw += SD::Rotator::QUARTER_REVOLUTION + (SD::Rotator::FULL_REVOLUTION / 12); //240 degrees
	dodgePositions.push_back(enemyDir.GetDirectionalVector() * dodgeProjection);
	enemyDir.Yaw = originalYaw + (SD::Rotator::QUARTER_REVOLUTION * 3); //270 degrees
	dodgePositions.push_back(enemyDir.GetDirectionalVector() * dodgeProjection);
	enemyDir.Yaw = originalYaw - (SD::Rotator::FULL_REVOLUTION / 6); //300 degrees
	dodgePositions.push_back(enemyDir.GetDirectionalVector() * dodgeProjection);

#ifdef DEBUG_MODE
	const bool debugDodgePoints = false;
	if (debugDodgePoints)
	{
		//Temporary display dots that shows where the dodge candidates are.
		for (const SD::Vector3& dodgePosition : dodgePositions)
		{
			SD::SceneEntity* spot = SD::SceneEntity::CreateObject();
			spot->SetTranslation((dodgePosition * 0.2f) + OwningBody->ReadTranslation());
			spot->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_DEBUG_SHAPES;

			SD::SolidColorRenderComponent* spotRender = SD::SolidColorRenderComponent::CreateObject();
			if (spot->AddComponent(spotRender))
			{
				spotRender->SolidColor = SD::Color(200, 200, 200, 128);
				spotRender->CenterOrigin = true;
				spotRender->BaseSize = 32.f;
				spotRender->RectHeight = 32.f;
				spotRender->Shape = SD::SolidColorRenderComponent::ST_Circle;
				DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(spotRender);
			}

			SD::LifeSpanComponent* lifeSpan = SD::LifeSpanComponent::CreateObject();
			if (spot->AddComponent(lifeSpan))
			{
				lifeSpan->LifeSpan = 5.f;
			}
		}
	}
#endif

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetEnvironment() != nullptr)

	//Conduct a trace towards each direction and favor the ones that are not going to collide
	SD::FLOAT bestTrace = 0.2f; //Must be at least 20% of the direction in order to consider a dodge direction
	std::vector<SD::Vector3> candidates;
	b2RayCastInput traceInput;
	b2RayCastOutput traceOutput;
	traceInput.maxFraction = 1.f;
	for (const SD::Vector3& dodgePosition : dodgePositions)
	{
		traceInput.p1 = SD::Box2dUtils::ToBox2dCoordinates(OwningBody->ReadAbsTranslation());
		traceInput.p2 = SD::Box2dUtils::ToBox2dCoordinates(dodgePosition + OwningBody->ReadAbsTranslation());
		if (!localDroneEngine->GetEnvironment()->ArenaTrace(traceInput, &traceOutput))
		{
			if (bestTrace < 1.f)
			{
				//First time finding a direction that did not collide with anything. Dismiss other candidates.
				SD::ContainerUtils::Empty(OUT candidates);
				bestTrace = 1.f;
			}

			candidates.push_back(dodgePosition);
			continue;
		}
		
		if (traceOutput.fraction > bestTrace)
		{
			//Although we collided, this is a better candidate than the others.
			SD::ContainerUtils::Empty(OUT candidates);
			bestTrace = traceOutput.fraction;
			candidates.push_back(dodgePosition);
		}
	}

	if (!SD::ContainerUtils::IsEmpty(candidates))
	{
		return candidates.at(SD::RandomUtils::Rand(candidates.size()).ToUnsignedInt());
	}
	
	//None of the dodge directions are valid. This bot must cornered themselves.
	return SD::Vector3::ZeroVector;
}

void BotCharacterComponent::EndCombat ()
{
	Enemy = nullptr;
	if (OwningBody != nullptr && OwningBody->GetDrawnWeapon() != nullptr)
	{
		OwningBody->GetDrawnWeapon()->StopFiring();
	}

	if (DestinationPriority == DODGE_PRIORITY)
	{
		//Stop dodging
		DestinationPriority = 0;
	}

	BotState = BS_Roaming;
	FindRoamDestination(true);
}

void BotCharacterComponent::CalcRotation (SD::FLOAT deltaSec)
{
	if (OwningBody == nullptr)
	{
		//Body was killed on same frame as this tick
		return;
	}

	SD::Vector3 lookAt;
	if (Enemy == nullptr)
	{
		lookAt = DesiredDestination;
	}
	else
	{
		lookAt = Enemy->ReadAbsTranslation();
		if (SkillLevel > GameRules::Easy && OwningBody->GetDrawnWeapon() != nullptr)
		{
			//If medium or hard, try to lead projectiles
			MovementComponent* enemyMovement = Enemy->GetMovement();
			const Projectile* projectileClass = OwningBody->GetDrawnWeapon()->GetProjectileClass();

			//If this weapon has a projectile and the enemy is moving
			if (projectileClass != nullptr && projectileClass->GetVelocityMagnitude() > 0.f &&
				enemyMovement != nullptr && !enemyMovement->ReadDesiredDirection().IsEmpty())
			{
				//Compute the time it takes for the projectile to reach from self to Enemy
				const SD::FLOAT travelTime = (Enemy->ReadAbsTranslation() - OwningBody->ReadAbsTranslation()).VSize(false) / projectileClass->GetVelocityMagnitude();

				//Figure out the point in space where the enemy would be if they continued going straight for travelTime seconds.
				lookAt += (enemyMovement->ReadDesiredDirection() * enemyMovement->GetVelocityMagnitude() * travelTime);
			}
		}
	}

	lookAt = (lookAt - OwningBody->ReadAbsTranslation());
	if (lookAt.IsEmpty())
	{
		return; //Look at the origin? Nah.
	}

	SD::Rotator desiredRotation(lookAt);

	SD::FLOAT rotationDelta = (OwningBody->ReadRotation().GetYaw(SD::Rotator::RU_Degrees) - desiredRotation.GetYaw(SD::Rotator::RU_Degrees));
	if (SD::FLOAT::Abs(rotationDelta) <= LookAtThreshold)
	{
		//Already looking at desired rotation
		return;
	}

	//Null out any directional changes due to Z-translation
	desiredRotation.Pitch = 0;

	SD::Rotator newRotation(OwningBody->ReadRotation());
	SD::FLOAT multiplier = 1.f;

	//Increment and decrement rotation to figure out which way is faster
	SD::FLOAT newRotationYaw = newRotation.GetYaw(SD::Rotator::RU_Radians);
	SD::FLOAT desiredYaw = desiredRotation.GetYaw(SD::Rotator::RU_Radians);
	if (std::sin((desiredYaw - newRotationYaw).Value) < 0.f)
	{
		//It'll be faster to spin counterclockwise.
		multiplier = -1.f;
	}

	//Add rotation
	newRotation.SetYaw(newRotation.GetYaw(SD::Rotator::RU_Degrees) + (RotationRate * deltaSec * multiplier), SD::Rotator::RU_Degrees);
	OwningBody->SetRotation(newRotation);
}

void BotCharacterComponent::HandleNavigationTick (SD::FLOAT deltaSec)
{
	ProcessNavigation();

#ifdef DEBUG_MODE
	if (DesiredLocationDebugger != nullptr)
	{
		DesiredLocationDebugger->EditTranslation().X = DesiredDestination.X;
		DesiredLocationDebugger->EditTranslation().Y = DesiredDestination.Y;
	}
#endif
}

void BotCharacterComponent::HandleStuckTick (SD::FLOAT deltaSec)
{
	ProcessStuckCheck();
}

void BotCharacterComponent::HandleCombatTick (SD::FLOAT deltaSec)
{
	ProcessCombat();
}

void BotCharacterComponent::HandlePerceptionTick (SD::FLOAT deltaSec)
{
	ProcessPerception();
}

void BotCharacterComponent::HandleRotationTick (SD::FLOAT deltaSec)
{
	CalcRotation(deltaSec);
}

void BotCharacterComponent::HandleTakeDamage (SD::INT damage, PlayerSoul* damagedBy, Projectile* damagedWith)
{
	//Can't retaliate if we can't fight
	if (!IsAbleToFight())
	{
		return;
	}

	CHECK(OwningBody != nullptr) //IsAbleToFight should have covered this

	if (Enemy != nullptr && damagedBy != nullptr && damagedBy->GetBody() != nullptr && Enemy != damagedBy->GetBody() && !OwningBody->IsFriendlyTo(damagedBy->GetBody()))
	{
		SD::FLOAT damagedByDist = (damagedBy->GetBody()->ReadAbsTranslation() - OwningBody->ReadAbsTranslation()).VSize(false);
		SD::FLOAT enemyDist = (Enemy->ReadAbsTranslation() - OwningBody->ReadAbsTranslation()).VSize(false);
		if (damagedByDist * 1.25f < enemyDist)
		{
			//The new assailant is significantly closer. Switch targets
			Attack(damagedBy->GetBody());
		}
	}
	else if (Enemy == nullptr && damagedBy != nullptr && damagedBy->IsAlive() && !OwningBody->IsFriendlyTo(damagedBy->GetBody()))
	{
		Attack(damagedBy->GetBody());
	}
}

DD_END