/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Scoreboard_TeamDeathmatch.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "Scoreboard_TeamDeathmatch.h"
#include "ScoreComponent_TeamTable.h"
#include "TeamDeathmatch.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Scoreboard_TeamDeathmatch, DD, Scoreboard)

void Scoreboard_TeamDeathmatch::InitProps ()
{
	Super::InitProps();

	ShouldInitializeHeaders = false;
}

void Scoreboard_TeamDeathmatch::InitializeGameLimitLabel ()
{
	Super::InitializeGameLimitLabel();

	if (GameLimitLabel != nullptr)
	{
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr)

		if (TeamDeathmatch* deathmatch = dynamic_cast<TeamDeathmatch*>(localDroneEngine->GetGame()))
		{
			SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
			CHECK(translator != nullptr)
			SD::DString summaryText = translator->TranslateText(TXT("TeamDeathmatchSummary"), TRANSLATION_FILE, TranslationSectionName);
			summaryText.ReplaceInline(TXT("%s"), deathmatch->GetKillLimit().ToString(), SD::DString::CC_IgnoreCase);
			GameLimitLabel->SetText(summaryText);
		}
	}
}

void Scoreboard_TeamDeathmatch::UpdatePlayerFields (GameRules* rules)
{
	TeamDeathmatch* teamGame = dynamic_cast<TeamDeathmatch*>(rules);
	if (teamGame == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to update the team deathmatch scoreboard's player fields since the game is not running a team game."));
		return;
	}

	CHECK(TeamTables.size() == teamGame->ReadTeams().size())

	for (size_t i = 0; i < TeamTables.size(); ++i)
	{
		TeamTables.at(i)->UpdateTable(teamGame->ReadTeams().at(i));
	}
}

void Scoreboard_TeamDeathmatch::InitializeHeaderLabel (SD::FrameComponent* headerFrame)
{
	//Don't call super. Instead the subtables will have the headers
}

void Scoreboard_TeamDeathmatch::InitializeEntries (const SD::Vector2& startingPosition, const SD::Vector2& allowedSpace)
{
	//Extend the permitted borders since we'll need the space.
	SD::Vector2 exStartingPosition(SD::Vector2(startingPosition.X * 0.75f, startingPosition.Y));
	SD::Vector2 exAllowedSpace(SD::Vector2(1.f - (exStartingPosition.X*2.f), allowedSpace.Y));
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	TeamDeathmatch* teamGame = dynamic_cast<TeamDeathmatch*>(localDroneEngine->GetGame());
	if (teamGame == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to update the team deathmatch scoreboard's player fields since the game is not running a team game."));
		return;
	}

	size_t numTeams = teamGame->ReadTeams().size();
	if (numTeams <= 0)
	{
		return;
	}

	SD::FLOAT tableWidth;
	SD::FLOAT tableSpacing = 0.02f;
	if (numTeams <= 2 || numTeams == 4)
	{
		tableWidth = (exAllowedSpace.X - tableSpacing) / 2.f;
	}
	else
	{
		tableWidth = (exAllowedSpace.X - (tableSpacing*2)) / 3.f;
	}
	SD::FLOAT tableHeight = (numTeams < 4) ? 1.f : (0.5f - tableSpacing);
	tableHeight *= exAllowedSpace.Y;	

	std::vector<SD::Vector2> tablePos;
	tablePos.resize(6);
	tablePos.at(0) = exStartingPosition;
	tablePos.at(1) = SD::Vector2(exStartingPosition.X + tableWidth + tableSpacing, exStartingPosition.Y);
	tablePos.at(2) = (numTeams == 4) ? SD::Vector2(exStartingPosition.X, exStartingPosition.Y + tableHeight + tableSpacing) : SD::Vector2(exStartingPosition.X + ((tableWidth + tableSpacing) * 2), exStartingPosition.Y);
	tablePos.at(3) = SD::Vector2(0.f, exStartingPosition.Y + tableSpacing + tableHeight);
	switch (numTeams)
	{
		case(4):
			tablePos.at(3).X = exStartingPosition.X + (tableWidth + tableSpacing);
			break;
		case(5):
			tablePos.at(3).X = exStartingPosition.X + (tableWidth * 0.5f);
			break;
		case(6):
			tablePos.at(3).X = exStartingPosition.X;
			break;
	}

	tablePos.at(4) = (numTeams == 5) ? SD::Vector2(exStartingPosition.X + (tableWidth * 1.5f) + tableSpacing, exStartingPosition.Y + tableHeight + tableSpacing) : SD::Vector2(exStartingPosition.X + tableWidth + tableSpacing, exStartingPosition.Y + tableHeight + tableSpacing);
	tablePos.at(5) = SD::Vector2(exStartingPosition.X + (tableWidth * 2.f) + (tableSpacing * 2.f), exStartingPosition.Y + tableHeight + tableSpacing);

	for (size_t i = 0; i < teamGame->ReadTeams().size() && i < tablePos.size(); ++i)
	{
		ScoreComponent_TeamTable* table = ScoreComponent_TeamTable::CreateObject();
		if (AddComponent(table))
		{
			table->SetSize(tableWidth, tableHeight);
			table->SetPosition(tablePos.at(i));
			table->InitializeComponent(teamGame->ReadTeams().at(i));
			TeamTables.push_back(table);
		}
	}
}

DD_END