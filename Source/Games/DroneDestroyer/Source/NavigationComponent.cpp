/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Arena.cpp
=====================================================================
*/

#include "Arena.h"
#include "DebugNavLine.h"
#include "DroneDestroyerEngineComponent.h"
#include "NavigationComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, NavigationComponent, SD, EntityComponent)

const SD::Color NavigationComponent::PATH_LINE_COLOR(255, 255, 64, 128);
bool NavigationComponent::DebugLinesVisible = false;

void NavigationComponent::InitProps ()
{
	Super::InitProps();

	MaxConnectionRadius = 2100.f; //21 meters (about 1 screen width)
	WanderRadius = 500.f;

	NextNavigation = nullptr;
}

void NavigationComponent::BeginObject ()
{
	Super::BeginObject();

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	Arena* environment = localDroneEngine->GetEnvironment();
	CHECK(environment != nullptr)

	NextNavigation = environment->GetFirstNavigation();
	environment->SetFirstNavigation(this);

	//Ensure debugging lines are invisible by default
	SetVisibility(DebugLinesVisible);
}

bool NavigationComponent::CanBeAttachedTo (Entity* ownerCandidate) const
{
	if (!Super::CanBeAttachedTo(ownerCandidate))
	{
		return false;
	}

	return (dynamic_cast<SD::SceneTransform*>(ownerCandidate) != nullptr);
}

void NavigationComponent::Destroyed ()
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetEnvironment() != nullptr)

	SeverConnections(localDroneEngine->GetEnvironment(), true);

	if (localDroneEngine->GetEnvironment()->GetFirstNavigation() == this)
	{
		localDroneEngine->GetEnvironment()->SetFirstNavigation(NextNavigation);
		NextNavigation = nullptr;
	}
	else
	{
		for (NavigationComponent* nav = localDroneEngine->GetEnvironment()->GetFirstNavigation(); nav != nullptr; nav = nav->NextNavigation)
		{
			if (nav->NextNavigation == this)
			{
				nav->NextNavigation = NextNavigation;
				NextNavigation = nullptr;
				break;
			}
		}
	}

	Super::Destroyed();
}

void NavigationComponent::BuildPathing (bool appendingConnections)
{
	if (GetOwner() == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot build navigation component's pathing if it doesn't have a SceneTransformation. It must attach to an Entity with a SceneTransformation before it can build its navigation."));
		return;
	}

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetEnvironment() != nullptr)
	SD::SceneTransform* transform = GetSceneTransform();

	if (!appendingConnections && !SD::ContainerUtils::IsEmpty(ConnectedPoints))
	{
		SeverConnections(localDroneEngine->GetEnvironment(), false);
	}

#ifdef DEBUG_MODE
	if (dynamic_cast<SD::SolidColorRenderComponent*>(FindSubComponent(SD::SolidColorRenderComponent::SStaticClass(), true)) == nullptr)
	{
		//Add a dot visualizer for this node to signal that it has built paths
		SD::SceneTransformComponent* dotTransform = SD::SceneTransformComponent::CreateObject();
		if (AddComponent(dotTransform))
		{
			dotTransform->SetRelativeTo(transform);
			dotTransform->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_DEBUG_SHAPES;
			dotTransform->AutoComputeAbsTransform = false;
			dotTransform->CalculateAbsoluteTransform();

			SD::SolidColorRenderComponent* dotComp = SD::SolidColorRenderComponent::CreateObject();
			if (dotTransform->AddComponent(dotComp))
			{
				dotComp->SolidColor = PATH_LINE_COLOR;
				dotComp->Shape = SD::SolidColorRenderComponent::ShapeType::ST_Circle;
				dotComp->BaseSize = 64.f;
				dotComp->CenterOrigin = true;

				DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(dotComp);
			}
		}
	}
#endif

	for (NavigationComponent* nav = localDroneEngine->GetEnvironment()->GetFirstNavigation(); nav != nullptr; nav = nav->NextNavigation)
	{
		if (nav->GetOwner() == nullptr)
		{
			//Ignore components that are not attached to anything (needs a scene transform)
			continue;
		}

		if (nav == this)
		{
			//Don't connect to self
			continue;
		}

		SD::SceneTransform* otherTransform = nav->GetSceneTransform();
		SD::FLOAT dist = (otherTransform->ReadTranslation() - transform->ReadTranslation()).VSize(false);
		if (dist > MaxConnectionRadius)
		{
			//Too far away to connect
			continue;
		}

		if (localDroneEngine->GetEnvironment()->FastTrace(transform->ReadTranslation(), otherTransform->ReadTranslation()))
		{
			//Something is obscuring this node
			continue;
		}

		ConnectedPoints.push_back(nav);
		if (appendingConnections)
		{
			//Notify the other navigation point that there is a connection between this and that (since we're not telling the other navigation point to rebuild pathing).
			nav->ConnectedPoints.push_back(this);
		}

#ifdef DEBUG_MODE
		//Add debugging line to represent a two-way connection
		if (IsConnectedToThis(nav))
		{
			SD::SceneTransformComponent* transformComp = SD::SceneTransformComponent::CreateObject();
			if (AddComponent(transformComp))
			{
				SD::Vector3 dir(otherTransform->ReadTranslation() - transform->ReadTranslation());
				transformComp->SetRotation(SD::Rotator(dir));
				transformComp->SetRelativeTo(transform);
				transformComp->SetTranslation(SD::Vector3(0.f, 0.f, DroneDestroyerEngineComponent::DRAW_ORDER_DEBUG_SHAPES));
				transformComp->AutoComputeAbsTransform = false;
				transformComp->CalculateAbsoluteTransform();

				DebugNavLine* lineComp = DebugNavLine::CreateObject();
				if (transformComp->AddComponent(lineComp))
				{
					lineComp->NavPointA = this;
					lineComp->NavPointB = nav;
					lineComp->SolidColor = PATH_LINE_COLOR;
					lineComp->Shape = SD::SolidColorRenderComponent::ShapeType::ST_Rectangle;
					lineComp->BaseSize = dist;
					lineComp->RectHeight = 6.f;

					DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(lineComp);
				}
			}
		}
#endif
	}
}

void NavigationComponent::SetMaxConnectionRadius (SD::FLOAT newMaxConnectionRadius)
{
	MaxConnectionRadius = newMaxConnectionRadius;
}

void NavigationComponent::SetWanderRadius (SD::FLOAT newWanderRadius)
{
	WanderRadius = newWanderRadius;
}

SD::SceneTransform* NavigationComponent::GetSceneTransform () const
{
	return dynamic_cast<SD::SceneTransform*>(GetOwner());
}

bool NavigationComponent::IsConnectedToThis (NavigationComponent* other) const
{
	for (NavigationComponent* connectedPoint : other->ConnectedPoints)
	{
		if (connectedPoint == this)
		{
			return true;
		}
	}

	return false;
}

void NavigationComponent::SeverConnections (Arena* networkOwner, bool pendingDestruction)
{
	CHECK(networkOwner != nullptr)

	//Iterate through the navigation list, and if anything is connected to this, remove it.
	for (NavigationComponent* nav = networkOwner->GetFirstNavigation(); nav != nullptr; nav = nav->GetNextNavigation())
	{
		if (nav == this)
		{
			continue;
		}

		for (size_t i = 0; i < nav->ConnectedPoints.size(); ++i)
		{
			if (nav->ConnectedPoints.at(i) == this)
			{
				nav->ConnectedPoints.erase(nav->ConnectedPoints.begin() + i);

#ifdef DEBUG_MODE
				std::vector<SD::Entity*> destructionList; //Don't purge components in a middle of a component iterator

				//Destroy the line that draws between this navigation component and the other.
				for (SD::ComponentIterator iter(nav, true); iter.GetSelectedComponent() != nullptr; ++iter)
				{
					if (DebugNavLine* lineComp = dynamic_cast<DebugNavLine*>(iter.GetSelectedComponent()))
					{
						//Check if the line's end point
						if (lineComp->NavPointA == this || lineComp->NavPointB == this)
						{
							//Destroy the owning transform associated with this lineComp
							destructionList.push_back(lineComp->GetOwner());
						}
					}
				}

				//Now destroy them
				while (!SD::ContainerUtils::IsEmpty(destructionList))
				{
					SD::ContainerUtils::GetLast(destructionList)->Destroy();
					destructionList.pop_back();
				}
#endif

				//Assume each NavigationComponent as at most one connection to this component
				break;
			}
		}
	}

	if (!pendingDestruction)
	{
		//Need to clean up its own connections since this component is about to be repurposed.
		SD::ContainerUtils::Empty(OUT ConnectedPoints);

#ifdef DEBUG_MODE
		//Destroy all debugging line render components
		while (true)
		{
			SD::SceneTransformComponent* transformComp = dynamic_cast<SD::SceneTransformComponent*>(FindSubComponent(SD::SceneTransformComponent::SStaticClass(), false));
			if (transformComp == nullptr)
			{
				break;
			}

			//Destroys the transform and the DebugNavLine attached to it.
			transformComp->Destroy();
		}
#endif
	}
}

DD_END