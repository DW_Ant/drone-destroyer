/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TimeLimitComponent.cpp
=====================================================================
*/

#include "TimeLimitComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, TimeLimitComponent, SD, EntityComponent)

void TimeLimitComponent::InitProps ()
{
	Super::InitProps();

	GameTime = 0.f;
	OriginalTimeLimit = 0.f;
	Tick = nullptr;
}

void TimeLimitComponent::BeginObject ()
{
	Super::BeginObject();

	Tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(Tick))
	{
		Tick->SetTickInterval(1.f);
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, TimeLimitComponent, HandleTick, void, SD::FLOAT));
	}
}

void TimeLimitComponent::SetTimeLimit (SD::FLOAT newTimeLimit)
{
	OriginalTimeLimit = newTimeLimit;
	if (OriginalTimeLimit > 0)
	{
		GameTime = OriginalTimeLimit;
	}
	else
	{
		GameTime = 0.f;
	}

	if (!Tick->IsTicking())
	{
		Tick->SetTicking(true);
	}
}

void TimeLimitComponent::HandleTick (SD::FLOAT deltaSec)
{
	if (OriginalTimeLimit > 0.f)
	{
		GameTime -= deltaSec;
		if (GameTime < 0.f)
		{
			if (OnExpired.IsBounded())
			{
				OnExpired.Execute();
			}

			if (Tick != nullptr)
			{
				Tick->SetTicking(false);
			}
		}
	}
	else
	{
		GameTime += deltaSec;
	}

	OnTimeChanged.Broadcast(GetGameTime());
}

DD_END