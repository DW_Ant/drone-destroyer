/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HudLastManStanding.cpp
=====================================================================
*/

#include "HudLastManStanding.h"
#include "PlayerSoul.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, HudLastManStanding, SD, GuiComponent)

void HudLastManStanding::InitProps ()
{
	Super::InitProps();

	PositionLabel = nullptr;
	SpreadLabel = nullptr;
	LivesRemainingLabel = nullptr;
}

void HudLastManStanding::BeginObject ()
{
	Super::BeginObject();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	PositionRatio = translator->TranslateText(TXT("PositionRatio"), TRANSLATION_FILE, TXT("Hud"));
	PositionPostfix = translator->TranslateText(TXT("Position"), TRANSLATION_FILE, TXT("Hud"));
	SpreadPrefix = translator->TranslateText(TXT("Spread"), TRANSLATION_FILE, TXT("Hud"));
	LivesRemainingPrefix = translator->TranslateText(TXT("LivesRemaining"), TRANSLATION_FILE, TXT("Hud"));
}

void HudLastManStanding::InitializeComponents ()
{
	Super::InitializeComponents();

	SD::FrameComponent* frame = SD::FrameComponent::CreateObject();
	if (AddComponent(frame))
	{
		frame->SetPosition(SD::Vector2::ZeroVector);
		frame->SetSize(SD::Vector2(1.f, 1.f));
		frame->SetBorderThickness(0.f);
		frame->SetLockedFrame(true);
		if (frame->RenderComponent != nullptr)
		{
			frame->RenderComponent->TopBorder = nullptr;
			frame->RenderComponent->RightBorder = nullptr;
			frame->RenderComponent->BottomBorder = nullptr;
			frame->RenderComponent->LeftBorder = nullptr;
			frame->RenderComponent->TopRightCorner = nullptr;
			frame->RenderComponent->BottomRightCorner = nullptr;
			frame->RenderComponent->BottomLeftCorner = nullptr;
			frame->RenderComponent->TopLeftCorner = nullptr;
			frame->RenderComponent->ClearSpriteTexture();
			frame->RenderComponent->FillColor = SD::Color(64, 64, 64, 150);
		}

		PositionLabel = SD::LabelComponent::CreateObject();
		if (frame->AddComponent(PositionLabel))
		{
			PositionLabel->SetAutoRefresh(false);
			PositionLabel->SetPosition(SD::Vector2(0.f, 0.f));
			PositionLabel->SetSize(SD::Vector2(1.f, 0.33f));
			PositionLabel->SetWrapText(false);
			PositionLabel->SetClampText(true);
			PositionLabel->SetAutoRefresh(true);
		}
		else
		{
			PositionLabel = nullptr;
		}

		SpreadLabel = SD::LabelComponent::CreateObject();
		if (frame->AddComponent(SpreadLabel))
		{
			SpreadLabel->SetAutoRefresh(false);
			SpreadLabel->SetPosition(SD::Vector2(0.f, 0.33f));
			SpreadLabel->SetSize(SD::Vector2(1.f, 0.33f));
			SpreadLabel->SetWrapText(false);
			SpreadLabel->SetClampText(true);
			SpreadLabel->SetAutoRefresh(true);
		}
		else
		{
			SpreadLabel = nullptr;
		}

		LivesRemainingLabel = SD::LabelComponent::CreateObject();
		if (frame->AddComponent(LivesRemainingLabel))
		{
			LivesRemainingLabel->SetAutoRefresh(false);
			LivesRemainingLabel->SetPosition(SD::Vector2(0.f, 0.67f));
			LivesRemainingLabel->SetSize(SD::Vector2(1.f, 0.33f));
			LivesRemainingLabel->SetWrapText(false);
			LivesRemainingLabel->SetClampText(true);
			LivesRemainingLabel->SetAutoRefresh(true);
		}
		else
		{
			LivesRemainingLabel = nullptr;
		}
	}
}

void HudLastManStanding::UpdateScores (const std::vector<SD::DPointer<PlayerSoul>>& rankedPlayers)
{
	size_t playerRank = 0;
	SD::INT spread = 0;
	bool foundPlayer = false;

	//Figure out where the player is positioned in the scoreboard
	for (size_t i = 0; i < rankedPlayers.size(); ++i)
	{
		if (!rankedPlayers.at(i)->GetIsBot())
		{
			foundPlayer = true;
			playerRank = i;
			if (i > 0)
			{
				spread = rankedPlayers.at(i)->GetLivesRemaining() - rankedPlayers.at(0)->GetLivesRemaining();
			}
			else if (rankedPlayers.size() > 1)
			{
				spread = rankedPlayers.at(0)->GetLivesRemaining() - rankedPlayers.at(1)->GetLivesRemaining();
			}
			break;
		}
	}

	if (!foundPlayer)
	{
		return; //Player must be a spectator
	}

	SetVisibility(true);
	if (PositionLabel != nullptr)
	{
		if (PositionLabel->GetRenderComponent() != nullptr)
		{
			PositionLabel->GetRenderComponent()->SetFontColor(GetPositionColor(playerRank));
		}

		SD::DString posStr = SD::INT(playerRank+1).ToString();
		if (((playerRank + 1) % 10) == 1 && playerRank != 11)
		{
			posStr += TXT("st");
		}
		else if (((playerRank + 1) % 10) == 2 && playerRank != 12)
		{
			posStr += TXT("nd");
		}
		else if (((playerRank + 1) % 10) == 3 && playerRank != 13)
		{
			posStr += TXT("rd");
		}
		else
		{
			posStr += TXT("th");
		}

		posStr += PositionRatio;
		posStr += SD::INT(rankedPlayers.size()).ToString();
		posStr += PositionPostfix;

		PositionLabel->SetText(posStr);
	}

	if (SpreadLabel != nullptr)
	{
		if (SpreadLabel->GetRenderComponent() != nullptr)
		{
			SpreadLabel->GetRenderComponent()->SetFontColor(GetSpreadColor(spread));
		}

		SD::DString numberSign = (spread > 0) ? TXT("+") : SD::DString::EmptyString;
		SpreadLabel->SetText(SpreadPrefix + numberSign + spread.ToString());
	}

	if (LivesRemainingLabel != nullptr)
	{
		LivesRemainingLabel->SetText(LivesRemainingPrefix + rankedPlayers.at(playerRank)->GetLivesRemaining().ToString());
	}
}

sf::Color HudLastManStanding::GetPositionColor (size_t rank)
{
	switch(rank)
	{
		case (0): //1st place
			return sf::Color(128, 128, 255, 255);
		case (1): //2nd place
			return sf::Color(222, 222, 32, 255);
		case (2): //3rd place
			return sf::Color(128, 128, 32, 255);
	}

	return sf::Color(255, 255, 255, 255);
}

sf::Color HudLastManStanding::GetSpreadColor (SD::INT spread)
{
	if (spread > 0)
	{
		return sf::Color(128, 128, 255, 255);
	}
	else if (spread < 0)
	{
		return sf::Color(255, 128, 128, 255);
	}
	else
	{
		return sf::Color(255, 255, 255, 255);
	}
}

DD_END