/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GameModifier_UnlimitedAmmo.cpp
=====================================================================
*/

#include "AmmoPickup.h"
#include "GameModifier_UnlimitedAmmo.h"
#include "PickupStation.h"
#include "Weapon.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, GameModifier_UnlimitedAmmo, DD, GameModifier)

void GameModifier_UnlimitedAmmo::InitProps ()
{
	Super::InitProps();

	FriendlyNameKey = TXT("UnlimitedAmmo");
}

void GameModifier_UnlimitedAmmo::ModifyInventory (Inventory* newInv)
{
	if (Weapon* weap = dynamic_cast<Weapon*>(newInv))
	{
		weap->SetInfiniteAmmo(true);
	}
}

void GameModifier_UnlimitedAmmo::ModifyPickupStation (PickupStation* station)
{
	if (station != nullptr && station->GetInventoryClass() != nullptr)
	{
		bool isAmmoStation = (station->GetInventoryClass()->StaticClass()->IsParentOf(AmmoPickup::SStaticClass()));
		if (isAmmoStation)
		{
			//Don't destroy since it's an Arena Entity. Arenas have their own list of Entities to purge during clean up. Instead disable it.
			station->SetInventoryClass(nullptr);
		}
	}
}

DD_END