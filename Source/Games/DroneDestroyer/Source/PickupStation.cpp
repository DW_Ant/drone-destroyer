/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PickupStation.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "GameModifier.h"
#include "GameRules.h"
#include "NavigationComponent.h"
#include "PickupStation.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, PickupStation, SD, SceneEntity)

void PickupStation::InitProps ()
{
	Super::InitProps();

	AutoComputeAbsTransform = false;

	InventoryClass = nullptr;
	SpawnedItem = nullptr;
	RespawnInterval = 15.f;
	Tick = nullptr;
}

void PickupStation::BeginObject ()
{
	Super::BeginObject();

	Tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(Tick))
	{
		Tick->SetTickInterval(RespawnInterval);
		Tick->SetTickHandler(SDFUNCTION_1PARAM(this, PickupStation, HandleTick, void, SD::FLOAT));
	}
	else
	{
		Tick = nullptr;
	}
}

void PickupStation::Destroyed ()
{
	if (SpawnedItem != nullptr)
	{
		SpawnedItem->Destroy();
		SpawnedItem = nullptr;
	}

	Super::Destroyed();
}

void PickupStation::ApplyGameModifications ()
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)

	for (GameModifier* gameMod : localDroneEngine->GetGame()->ReadGameModifiers())
	{
		gameMod->ModifyPickupStation(this);
	}
}

void PickupStation::SetInventoryClass (const Inventory* newInventoryClass)
{
	//Destroy old item
	if (SpawnedItem != nullptr)
	{
		SpawnedItem->Destroy();
		SpawnedItem = nullptr;
	}

	InventoryClass = newInventoryClass;
	
	//Spawn the new item immediately
	SpawnItem();
}

void PickupStation::SetRespawnInterval (SD::FLOAT newRespawnInterval)
{
	RespawnInterval = newRespawnInterval;
	if (Tick != nullptr)
	{
		Tick->SetTickInterval(RespawnInterval);
	}
}

void PickupStation::SpawnItem ()
{
	CHECK(SpawnedItem == nullptr);
	if (InventoryClass == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("No Inventory class is specified for Pickup Station."));
		return;
	}

	SpawnedItem = InventoryClass->CreateObjectOfMatchingClass();
	CHECK(SpawnedItem != nullptr);

	SpawnedItem->SetTranslation(ReadTranslation());
	SpawnedItem->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_PICKUPS;
	SpawnedItem->OnTaken = SDFUNCTION_2PARAM(this, PickupStation, HandleItemTaken, void, Inventory*, Character*);
	SpawnedItem->InitializePhysics();

	if (Tick != nullptr)
	{
		Tick->SetTicking(false);
	}
}

void PickupStation::HandleTick (SD::FLOAT deltaSec)
{
	SpawnItem();
}

void PickupStation::HandleItemTaken (Inventory* itemTaken, Character* takenBy)
{
	CHECK(itemTaken != nullptr && itemTaken == SpawnedItem);

	itemTaken->OnTaken.ClearFunction();
	SpawnedItem = nullptr; //No longer responsible for item. 
	if (Tick != nullptr)
	{
		Tick->SetTicking(true);
		Tick->ResetAccumulatedTickTime();
	}
}

DD_END