/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanel_Roster.cpp
=====================================================================
*/

#include <regex>
#include "DroneDestroyerEngineComponent.h"
#include "GameRules.h"
#include "MenuPanel_Roster.h"
#include "PlayerSoul.h"
#include "SoundEngineComponent.h"
#include "SoundMessenger.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, MenuPanel_Roster, DD, MenuPanelComponent)

const SD::INT MenuPanel_Roster::PLAYER_NAME_LIMIT(32);

void MenuPanel_Roster::InitProps ()
{
	Super::InitProps();

	NeutralColor = SD::Color(64, 64, 64);

	TeamColors.push_back(SD::Color(128, 0, 0));
	TeamColors.push_back(SD::Color(0, 0, 128));
	TeamColors.push_back(SD::Color(0, 128, 0));
	TeamColors.push_back(SD::Color(128, 128, 0));
	TeamColors.push_back(SD::Color(128, 0, 128));
	TeamColors.push_back(SD::Color(0, 128, 128));
	FullBotNameChance = 0.1f;

	PlayerVerticalSpacing = 8.f;
	PlayerVerticalSize = 64.f;

	RosterHorizontalScaling = -1.f;
}

void MenuPanel_Roster::RestorePanel ()
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(ConfigFile, false))
	{
		config->Destroy();
		return;
	}
	
	std::vector<SD::DString> savedNames;
	config->GetArrayValues(TXT("Roster"), TXT("BotNames"), OUT savedNames);

	for (size_t i = 0; i < savedNames.size() && i < Players.size(); ++i)
	{
		Players.at(i).PlayerName->SetText(savedNames.at(i));

		if (Players.at(i).IsPlayer && Players.at(i).SpectatorCheckbox != nullptr)
		{
			bool isSpectator = config->GetProperty<SD::BOOL>(TXT("Roster"), TXT("Spectator"));
			Players.at(i).SpectatorCheckbox->SetChecked(isSpectator);
			Players.at(i).IsSpectator = isSpectator;
		}
	}

	config->Destroy();
}

void MenuPanel_Roster::SavePanelState ()
{
	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(ConfigFile, false))
	{
		config->Destroy();
		return;
	}

	std::vector<SD::DString> botNames;
	//Load array from config so that we don't lose the bot names in case there are fewer bots in current game compared to what was saved before.
	config->GetArrayValues(TXT("Roster"), TXT("SavedBotNames"), OUT botNames);

	for (size_t i = 0; i < Players.size(); ++i)
	{
		if (i >= botNames.size())
		{
			botNames.push_back(Players.at(i).PlayerName->GetContent());
		}
		else
		{
			botNames.at(i) = Players.at(i).PlayerName->GetContent();
		}

		if (Players.at(i).IsPlayer)
		{
			config->SaveProperty<SD::BOOL>(TXT("Roster"), TXT("Spectator"), Players.at(i).IsSpectator);
		}
	}

	config->SaveArray(TXT("Roster"), TXT("BotNames"), botNames);
	config->Destroy();
}

void MenuPanel_Roster::InitializeComponents ()
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	//Create at least one team category even for free for all games.
	SD::INT numCategories = SD::Utils::Max<SD::INT>(1, localDroneEngine->GetGame()->GetNumTeams()); 
	for (size_t i = 0; i < numCategories; ++i)
	{
		Teams.push_back(STeamData());
		STeamData& newTeam = Teams.at(i);

		newTeam.Scrollbar = SD::ScrollbarComponent::CreateObject();
		if (AddComponent(newTeam.Scrollbar))
		{
			SD::FLOAT teamMargin = 0.025f / SD::INT(numCategories).ToFLOAT();
			//Ugh! These formulae are ugly. I really need to get my UI Editor implemented
			newTeam.Scrollbar->SetSize(SD::Vector2((1.f - (teamMargin + (teamMargin * numCategories.ToFLOAT()))) / numCategories.ToFLOAT(), 0.85f));
			newTeam.Scrollbar->SetPosition(SD::Vector2(teamMargin + ((newTeam.Scrollbar->ReadSize().X + teamMargin) * SD::FLOAT::MakeFloat(i)), 0.05f));
			RosterHorizontalScaling = newTeam.Scrollbar->ReadSize().X;
			newTeam.Scrollbar->SetZoomEnabled(false);
			newTeam.Scrollbar->SetHideControlsWhenFull(true);

			newTeam.ViewedEntity = SD::GuiEntity::CreateObject();
			newTeam.ViewedEntity->SetAutoSizeVertical(true);
			newTeam.ViewedEntity->SetAutoSizeHorizontal(false);
			newTeam.ViewedEntity->SetGuiSizeToOwningScrollbar(SD::Vector2(1.f, -1.f));

			newTeam.Scrollbar->SetViewedObject(newTeam.ViewedEntity);
		}

		if (numCategories <= 1 || i >= TeamColors.size())
		{
			newTeam.TeamColor = NeutralColor;
		}
		else
		{
			newTeam.TeamColor = TeamColors.at(i);
		}

		newTeam.NumPlayers = 0;
		newTeam.TeamNumber = SD::INT(i);
	}

	//Add one to insert the player.
	for (size_t i = 0; i < localDroneEngine->GetGame()->GetNumBots() + 1; ++i)
	{
		Players.push_back(SPlayerEntry());
		SPlayerEntry& curPlayer = Players.at(Players.size() - 1);
		curPlayer.IsPlayer = (i == 0);
		
		STeamData& teamCategory = Teams.at(i % numCategories.ToUnsignedInt());
		CHECK(teamCategory.ViewedEntity != nullptr)

		curPlayer.Background = SD::FrameComponent::CreateObject();
		if (teamCategory.ViewedEntity->AddComponent(curPlayer.Background))
		{
			curPlayer.Background->SetSize(SD::Vector2(0.9f, PlayerVerticalSize));
			curPlayer.Background->SetPosition(SD::Vector2(0.05f, PlayerVerticalSpacing + (teamCategory.NumPlayers.ToFLOAT() * (PlayerVerticalSpacing + curPlayer.Background->ReadSize().Y))));
			curPlayer.Background->SetLockedFrame(true);
			curPlayer.Background->SetBorderThickness(2.f);

			if (curPlayer.Background->RenderComponent != nullptr)
			{
				curPlayer.Background->RenderComponent->ClearSpriteTexture();
				curPlayer.Background->RenderComponent->FillColor = teamCategory.TeamColor;
			}

			SD::FrameComponent* playerNameBackground = SD::FrameComponent::CreateObject();
			if (curPlayer.Background->AddComponent(playerNameBackground))
			{
				playerNameBackground->SetLockedFrame(true);
				playerNameBackground->SetPosition(SD::Vector2(0.25f / SD::FLOAT::MakeFloat(Teams.size()), 0.1f)); //We move the name field closer and closer to the borders based on the team size to make more space for text field.
				playerNameBackground->SetSize(SD::Vector2(1.f - (playerNameBackground->ReadPosition().X * 2.f), 0.4f));
				playerNameBackground->SetBorderThickness(2.f);
				if (playerNameBackground->RenderComponent != nullptr)
				{
					playerNameBackground->RenderComponent->ClearSpriteTexture();
					playerNameBackground->RenderComponent->FillColor = SD::Color::BLACK;
				}

				curPlayer.PlayerName = SD::TextFieldComponent::CreateObject();
				if (playerNameBackground->AddComponent(curPlayer.PlayerName))
				{
					curPlayer.PlayerName->SetAnchorTop(4.f);
					curPlayer.PlayerName->SetAnchorRight(4.f);
					curPlayer.PlayerName->SetAnchorBottom(4.f);
					curPlayer.PlayerName->SetAnchorLeft(4.f);
					curPlayer.PlayerName->MaxNumCharacters = PLAYER_NAME_LIMIT;
					curPlayer.PlayerName->SetClampText(false);
					curPlayer.PlayerName->SetText(GenerateRandomBotName());
					curPlayer.PlayerName->OnAllowTextInput = SDFUNCTION_1PARAM(this, MenuPanel_Roster, HandleAllowTextInput, bool, const SD::DString&);

					if (Teams.size() > 2)
					{
						//Reduce font size to fit more characters.
						curPlayer.PlayerName->SetCharacterSize(12);
					}
				}
			}

			curPlayer.PrevTeam = SD::ButtonComponent::CreateObject();
			if (curPlayer.Background->AddComponent(curPlayer.PrevTeam))
			{
				curPlayer.PrevTeam->SetPosition(SD::Vector2(0.05f, 0.5f));
				curPlayer.PrevTeam->SetSize(SD::Vector2(0.15f, 0.45f));

				curPlayer.PrevTeam->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_Roster, HandleMoveEntryLeftReleased, void, SD::ButtonComponent*));
				curPlayer.PrevTeam->SetCaptionText(TXT("<"));
				curPlayer.PrevTeam->SetEnabled(teamCategory.TeamNumber > 0);
				curPlayer.PrevTeam->SetVisibility(Teams.size() > 1);
			}

			if (!curPlayer.IsPlayer)
			{
				curPlayer.RandomName = SD::ButtonComponent::CreateObject();
				if (curPlayer.Background->AddComponent(curPlayer.RandomName))
				{
					curPlayer.RandomName->SetSize(SD::Vector2(0.45f, 0.45f));
					// Horizontally center this button based on the size of the next and prev team buttons.
					curPlayer.RandomName->SetPosition(SD::Vector2(0.5f - (curPlayer.RandomName->ReadSize().X * 0.5f), 0.5f));

					curPlayer.RandomName->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_Roster, HandleRandomNameReleased, void, SD::ButtonComponent*));
					curPlayer.RandomName->SetCaptionText(translator->TranslateText(TXT("RandomName"), TRANSLATION_FILE, TXT("Roster")));
				}
			}
			else
			{
				//Create the spectator checkbox instead
				curPlayer.SpectatorCheckbox = SD::CheckboxComponent::CreateObject();
				if (curPlayer.Background->AddComponent(curPlayer.SpectatorCheckbox))
				{
					curPlayer.SpectatorCheckbox->SetSize(SD::Vector2(0.45f, 0.45f));
					curPlayer.SpectatorCheckbox->SetPosition(SD::Vector2(0.5f - (curPlayer.SpectatorCheckbox->ReadSize().X * 0.5f), 0.5f));
					curPlayer.SpectatorCheckbox->SetChecked(curPlayer.IsSpectator);
					curPlayer.SpectatorCheckbox->OnChecked = SDFUNCTION_1PARAM(this, MenuPanel_Roster, HandleSetSpectatorChecked, void, SD::CheckboxComponent*);

					if (curPlayer.SpectatorCheckbox->CaptionComponent != nullptr)
					{
						curPlayer.SpectatorCheckbox->CaptionComponent->SetText(translator->TranslateText(TXT("SpectatorCheckbox"), TRANSLATION_FILE, TXT("Roster")));
					}
				}
				else
				{
					curPlayer.SpectatorCheckbox = nullptr;
				}
			}

			curPlayer.NextTeam = SD::ButtonComponent::CreateObject();
			if (curPlayer.Background->AddComponent(curPlayer.NextTeam))
			{
				curPlayer.NextTeam->SetPosition(SD::Vector2(1.f - (curPlayer.PrevTeam->ReadPosition().X + curPlayer.PrevTeam->ReadSize().X), 0.5f));
				curPlayer.NextTeam->SetSize(SD::Vector2(0.15f, 0.45f));

				curPlayer.NextTeam->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_Roster, HandleMoveEntryRightReleased, void, SD::ButtonComponent*));
				curPlayer.NextTeam->SetCaptionText(TXT(">"));
				curPlayer.NextTeam->SetEnabled(teamCategory.TeamNumber < Teams.size() - 1);
				curPlayer.NextTeam->SetVisibility(Teams.size() > 1);
			}

			teamCategory.NumPlayers++;
			curPlayer.TeamNumber = teamCategory.TeamNumber;
		}
	}

	//Buttons at the bottom
	{
		SD::FLOAT margins = 12.f;

		SD::ButtonComponent* backButton = SD::ButtonComponent::CreateObject();
		if (AddComponent(backButton))
		{
			backButton->SetSize(SD::Vector2(0.2f, 0.05f));
			backButton->SetAnchorLeft(margins);
			backButton->SetAnchorBottom(margins);
			backButton->SetCaptionText(translator->TranslateText(TXT("Back"), TRANSLATION_FILE, TXT("Roster")));
			backButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_Roster, HandleBackReleased, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* randomAll = SD::ButtonComponent::CreateObject();
		if (AddComponent(randomAll))
		{
			randomAll->SetPosition(SD::Vector2(0.4f, 0.f));
			randomAll->SetSize(SD::Vector2(0.2f, 0.05f));
			randomAll->SetAnchorBottom(margins);
			randomAll->SetCaptionText(translator->TranslateText(TXT("RandomAll"), TRANSLATION_FILE, TXT("Roster")));
			randomAll->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_Roster, HandleRandomAllReleased, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* launchButton = SD::ButtonComponent::CreateObject();
		if (AddComponent(launchButton))
		{
			launchButton->SetSize(SD::Vector2(0.2f, 0.05f));
			launchButton->SetAnchorRight(margins);
			launchButton->SetAnchorBottom(margins);
			launchButton->SetCaptionText(translator->TranslateText(TXT("Launch"), TRANSLATION_FILE, TXT("Roster")));
			launchButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_Roster, HandleLaunchGameReleased, void, SD::ButtonComponent*));

			//Give this button unique colors
			if (SD::ColorButtonState* colorState = dynamic_cast<SD::ColorButtonState*>(launchButton->GetButtonStateComponent()))
			{
				colorState->SetDefaultColor(SD::Color(128, 128, 0, 255));
				colorState->SetHoverColor(SD::Color(150, 150, 0, 255));
				colorState->SetDownColor(SD::Color(96, 96, 0, 255));
			}
		}
	}

	SD::TickComponent* scrollbarTick = SD::TickComponent::CreateObject(TICK_GROUP_DD_NO_PAUSE);
	if (AddComponent(scrollbarTick))
	{
		scrollbarTick->SetTickHandler(SDFUNCTION_1PARAM(this, MenuPanel_Roster, HandleClampScrollbarTick, void, SD::FLOAT));
		scrollbarTick->SetTickInterval(0.15f);
	}
}

void MenuPanel_Roster::Destroyed ()
{
	for (STeamData& team : Teams)
	{
		if (team.ViewedEntity != nullptr)
		{
			team.ViewedEntity->Destroy();
			team.ViewedEntity = nullptr;
		}
	}

	Super::Destroyed();
}

SD::DString MenuPanel_Roster::GenerateRandomBotName ()
{
	LoadDefaultBotNames();

	if (SD::ContainerUtils::IsEmpty(DefaultFullBotNames) || SD::ContainerUtils::IsEmpty(DefaultBotPrefixes) || SD::ContainerUtils::IsEmpty(DefaultBotPostfixes))
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to generate random bot name since the roster configuration file is missing fields. There aren't any bot names to pull from."));
		return TXT("Player");
	}

	if (SD::RandomUtils::fRand() <= FullBotNameChance)
	{
		return DefaultFullBotNames.at(SD::RandomUtils::Rand(SD::INT(DefaultFullBotNames.size())).Value);
	}

	SD::DString botName = DefaultBotPrefixes.at(SD::RandomUtils::Rand(SD::INT(DefaultBotPrefixes.size())).Value);
	botName += DefaultBotPostfixes.at(SD::RandomUtils::Rand(SD::INT(DefaultBotPostfixes.size())).Value);

	return botName;
}

void MenuPanel_Roster::LoadDefaultBotNames ()
{
	if (!SD::ContainerUtils::IsEmpty(DefaultFullBotNames))
	{
		//If this vector is already populated, then assume that the names are already loaded.
		return;
	}

	SD::ConfigWriter* config = SD::ConfigWriter::CreateObject();
	if (!config->OpenFile(SD::Directory::CONFIG_DIRECTORY.ReadDirectoryPath() + TXT("DefaultDroneDestroyer.ini"), false))
	{
		config->Destroy();
		return;
	}

	FullBotNameChance = config->GetProperty<SD::FLOAT>(TXT("Roster"), TXT("ChanceToUseFullName"));

	SD::ContainerUtils::Empty(OUT DefaultFullBotNames);
	config->GetArrayValues(TXT("Roster"), TXT("FullName"), OUT DefaultFullBotNames);

	SD::ContainerUtils::Empty(OUT DefaultBotPrefixes);
	config->GetArrayValues(TXT("Roster"), TXT("Prefix"), OUT DefaultBotPrefixes);

	SD::ContainerUtils::Empty(OUT DefaultBotPostfixes);
	config->GetArrayValues(TXT("Roster"), TXT("Postfix"), OUT DefaultBotPostfixes);

	config->Destroy();
}

void MenuPanel_Roster::UpdateBackgroundColor (SPlayerEntry& outPlayer, SD::INT teamNum) const
{
	if (outPlayer.Background != nullptr && outPlayer.Background->RenderComponent != nullptr)
	{
		outPlayer.Background->RenderComponent->FillColor = (Teams.size() > 1 && SD::ContainerUtils::IsValidIndex(TeamColors, teamNum)) ? TeamColors.at(teamNum.ToUnsignedInt()) : NeutralColor;
	}
}

void MenuPanel_Roster::MovePlayer (SPlayerEntry& outPlayer, STeamData& outOldTeam, STeamData& outNewTeam)
{
	UpdateBackgroundColor(OUT outPlayer, outNewTeam.TeamNumber);
	outPlayer.PrevTeam->SetEnabled(outNewTeam.TeamNumber > 0);
	outPlayer.NextTeam->SetEnabled(outNewTeam.TeamNumber.ToUnsignedInt() < Teams.size() - 1);

	//Move the entry from old to new
	outPlayer.Background->DetachSelfFromOwner();
	outNewTeam.ViewedEntity->AddComponent(outPlayer.Background);

	outOldTeam.NumPlayers--;
	outNewTeam.NumPlayers++;
	outPlayer.TeamNumber = outNewTeam.TeamNumber;

	RepairRosterPositions(OUT outOldTeam);
	RepairRosterPositions(OUT outNewTeam);
}

void MenuPanel_Roster::RepairRosterPositions (STeamData& outTeamCategory)
{
	SD::FLOAT curPosY = PlayerVerticalSpacing;
	for (SD::ComponentIterator iter(outTeamCategory.ViewedEntity, false); iter.GetSelectedComponent() != nullptr; ++iter)
	{
		if (SD::GuiComponent* gui = dynamic_cast<SD::GuiComponent*>(iter.GetSelectedComponent()))
		{
			gui->SetPosition(SD::Vector2(gui->ReadPosition().X, curPosY));
			curPosY += PlayerVerticalSpacing + PlayerVerticalSize;
		}
	}
}

bool MenuPanel_Roster::FindTeamAndPlayer (SD::ButtonComponent* button, STeamData*& outTeamOwner, SPlayerEntry*& outPlayer)
{
	//Find the player
	for (SPlayerEntry& player : Players)
	{
		if (button->GetOwner() != player.Background)
		{
			continue;
		}

		if (SD::GuiEntity* uiOwner = dynamic_cast<SD::GuiEntity*>(button->GetRootEntity()))
		{
			//Find the team
			for (STeamData& team : Teams)
			{
				if (team.ViewedEntity == uiOwner)
				{
					outPlayer = &player;
					outTeamOwner = &team;
					return true;
				}
			}
		}
	}

	return false;
}

bool MenuPanel_Roster::HandleAllowTextInput (const SD::DString& txt)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_TYPING);
	}

	std::regex regex("[^\\r\\n\\t\\f\\v%]"); //Don't allow tabs, new line, or other white space characters characters (spaces permitted). Also don't allow % since that would break some str replace functions.
	std::cmatch match;

	return std::regex_match(txt.ReadString().c_str(), OUT match, regex);
}

void MenuPanel_Roster::HandleClampScrollbarTick (SD::FLOAT deltaSec)
{
	if (RosterHorizontalScaling <= 0.f)
	{
		return;
	}

	//The Engine assumes the scrollbars will not be larger than 1024x1024 textures. This assumption introduces visual bugs when the rosters expand beyond the texture limits (smears the edge).
	//This function is a workaround to that limitation where it prevents the scrollbars from going beyond the width.
	SD::FLOAT newSize = SD::Utils::Min<SD::FLOAT>(ReadCachedAbsSize().X, 1024.f) * RosterHorizontalScaling;
	
	for (STeamData& team : Teams)
	{
		if (team.Scrollbar != nullptr)
		{
			team.Scrollbar->SetSize(newSize, team.Scrollbar->ReadSize().Y);
		}
	}
}

void MenuPanel_Roster::HandleSetSpectatorChecked (SD::CheckboxComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, (uiComponent->IsChecked()) ? SoundEngineComponent::SOUND_CLICK_NORMAL : SoundEngineComponent::SOUND_CLICK_LIGHT);
	}

	for (SPlayerEntry& player : Players)
	{
		if (player.IsPlayer)
		{
			//Assume there is only one human player and bots cannot be spectators
			player.IsSpectator = uiComponent->GetChecked();
			break;
		}
	}
}

void MenuPanel_Roster::HandleRandomNameReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	for (SPlayerEntry& player : Players)
	{
		if (player.RandomName == uiComponent)
		{
			player.PlayerName->SetText(GenerateRandomBotName());
			break;
		}
	}
}

void MenuPanel_Roster::HandleRandomAllReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_HEAVY);
	}

	for (SPlayerEntry& player : Players)
	{
		if (!player.IsPlayer)
		{
			player.PlayerName->SetText(GenerateRandomBotName());
		}
	}
}

void MenuPanel_Roster::HandleMoveEntryLeftReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	STeamData* oldTeam = nullptr;
	SPlayerEntry* player = nullptr;
	if (FindTeamAndPlayer(uiComponent, OUT oldTeam, OUT player))
	{
		CHECK(oldTeam != nullptr && player != nullptr && oldTeam->TeamNumber > 0) //User should not be able to click Left button for team zero
		STeamData& newTeam = Teams.at((oldTeam->TeamNumber - 1).ToUnsignedInt());
		MovePlayer(OUT *player, OUT *oldTeam, OUT newTeam);
	}
}

void MenuPanel_Roster::HandleMoveEntryRightReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	STeamData* oldTeam = nullptr;
	SPlayerEntry* player = nullptr;
	if (FindTeamAndPlayer(uiComponent, OUT oldTeam, OUT player))
	{
		CHECK(oldTeam != nullptr && player != nullptr && oldTeam->TeamNumber < Teams.size() - 1) //User should not be able to click the right button if this is the right-most team.
		STeamData& newTeam = Teams.at((oldTeam->TeamNumber + 1).ToUnsignedInt());
		MovePlayer(OUT *player, OUT *oldTeam, OUT newTeam);
	}
}

void MenuPanel_Roster::HandleBackReleased (SD::ButtonComponent* uiComponent)
{
	MainMenu* owningMenu = GetOwningMenu();
	CHECK(owningMenu != nullptr)
	owningMenu->NavigateBack();
}

void MenuPanel_Roster::HandleLaunchGameReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, TXT("Game_Start"));
	}

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	for (SPlayerEntry& player : Players)
	{
		localDroneEngine->CreateSoul(player.PlayerName->GetContent(), player.TeamNumber, player.IsSpectator, player.IsPlayer);
	}

	localDroneEngine->LaunchGame();
}

DD_END