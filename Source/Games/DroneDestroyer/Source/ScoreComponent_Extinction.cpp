/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScoreComponent_Extinction.cpp
=====================================================================
*/

#include "PlayerSoul.h"
#include "ScoreComponent_Extinction.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, ScoreComponent_Extinction, DD, ScoreComponent)

void ScoreComponent_Extinction::InitProps ()
{
	Super::InitProps();

	KillsLabel = nullptr;
	DeathsLabel = nullptr;
	LivesLabel = nullptr;
}

void ScoreComponent_Extinction::UpdateInformation (SD::INT rank, PlayerSoul* soul)
{
	Super::UpdateInformation(rank, soul);
	CHECK(soul != nullptr)

	if (KillsLabel != nullptr)
	{
		KillsLabel->SetText(soul->GetNumKills().ToString());
	}

	if (DeathsLabel != nullptr)
	{
		DeathsLabel->SetText(soul->GetNumDeaths().ToString());
	}

	if (LivesLabel != nullptr)
	{
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		LivesLabel->SetText((soul->GetIsEliminated()) ? translator->TranslateText(TXT("Out"), TRANSLATION_FILE, TXT("Scoreboard")) : soul->GetLivesRemaining().ToString());
	}
}

void ScoreComponent_Extinction::SetFontSize (SD::INT newFontSize)
{
	Super::SetFontSize(newFontSize);

	if (KillsLabel != nullptr)
	{
		KillsLabel->SetCharacterSize(FontSize);
	}

	if (DeathsLabel != nullptr)
	{
		DeathsLabel->SetCharacterSize(FontSize);
	}

	if (LivesLabel != nullptr)
	{
		LivesLabel->SetCharacterSize(FontSize);
	}
}

void ScoreComponent_Extinction::InitializeComponents ()
{
	Super::InitializeComponents();

	if (Frame != nullptr)
	{
		KillsLabel = SD::LabelComponent::CreateObject();
		if (Frame->AddComponent(KillsLabel))
		{
			KillsLabel->SetAutoRefresh(false);
			KillsLabel->SetPosition(SD::Vector2(0.6f, 0.f));
			KillsLabel->SetSize(SD::Vector2(0.133f, 1.f));
			KillsLabel->SetWrapText(false);
			KillsLabel->SetClampText(false);
			KillsLabel->SetCharacterSize(FontSize);
			KillsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			KillsLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			KillsLabel->SetAutoRefresh(true);
		}
		else
		{
			KillsLabel = nullptr;
		}

		DeathsLabel = SD::LabelComponent::CreateObject();
		if (Frame->AddComponent(DeathsLabel))
		{
			DeathsLabel->SetAutoRefresh(false);
			DeathsLabel->SetPosition(SD::Vector2(0.733f, 0.f));
			DeathsLabel->SetSize(SD::Vector2(0.133f, 1.f));
			DeathsLabel->SetWrapText(false);
			DeathsLabel->SetClampText(false);
			DeathsLabel->SetCharacterSize(FontSize);
			DeathsLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			DeathsLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			DeathsLabel->SetAutoRefresh(true);
		}
		else
		{
			DeathsLabel = nullptr;
		}

		LivesLabel = SD::LabelComponent::CreateObject();
		if (Frame->AddComponent(LivesLabel))
		{
			LivesLabel->SetAutoRefresh(false);
			LivesLabel->SetPosition(SD::Vector2(0.866f, 0.f));
			LivesLabel->SetSize(SD::Vector2(0.133f, 1.f));
			LivesLabel->SetWrapText(false);
			LivesLabel->SetClampText(false);
			LivesLabel->SetCharacterSize(FontSize);
			LivesLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			LivesLabel->SetVerticalAlignment(SD::LabelComponent::VA_Center);
			LivesLabel->SetAutoRefresh(true);
		}
		else
		{
			LivesLabel = nullptr;
		}
	}
}
DD_END