/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  AmmoPickup.cpp
=====================================================================
*/

#include "AmmoPickup.h"
#include "Character.h"
#include "Weapon.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, AmmoPickup, DD, Inventory)

void AmmoPickup::InitProps ()
{
	Super::InitProps();

#ifdef DEBUG_MODE
	DebugName = TXT("AmmoPickup");
#endif

	ItemNameKey = TXT("Ammo");
	PickupImage = TXT("DroneDestroyer.Pickups.Ammo");

	DefaultRefillAmount = 0.2f;
	AmmoRefillAmount = DefaultRefillAmount;
}

bool AmmoPickup::CanPickupItem (Character* takerCandidate) const
{
	//Must possess at least one weapon
	return (Super::CanPickupItem(takerCandidate) && takerCandidate != nullptr && takerCandidate->GetDrawnWeapon() != nullptr);
}

SD::INT AmmoPickup::GetImportanceValue (Character* possibleTaker) const
{
	CHECK(possibleTaker != nullptr)

	SD::INT result = 2; //Default to a slight higher value than normal pathing. Even if we don't need this weapon, taking this will prevent opponents from taking it.

	//Check the taker's inventory list to assess how loaded they are with their weapons
	SD::FLOAT numWeapons = 0.f;
	SD::FLOAT totalPercent = 0.f;
	for (Inventory* inv = possibleTaker->GetFirstInventory(); inv != nullptr; inv = inv->GetNextInventory())
	{
		if (Weapon* weap = dynamic_cast<Weapon*>(inv))
		{
			++numWeapons;
			totalPercent += (weap->GetAmmo().ToFLOAT() / weap->GetMaxAmmo().ToFLOAT());
		}
	}

	if (numWeapons <= 0)
	{
		//This should never happen since CanPickupItem would check this first
		return 0;
	}

	SD::FLOAT avgFull = totalPercent / numWeapons;

	avgFull *= (AmmoRefillAmount / DefaultRefillAmount); //Adjust priority if this is a super ammo pickup or a weak one.

	//Pick a value ranging from 2 to 50 based on the percentFull where full ammo yields 2, empty yields 50.
	return SD::Utils::Lerp<SD::INT, SD::FLOAT>(avgFull, 50, 2);
}

void AmmoPickup::GiveTo (Character* taker)
{
	Super::GiveTo(taker);

	for (Inventory* inv = taker->GetFirstInventory(); inv != nullptr; inv = inv->GetNextInventory())
	{
		if (Weapon* weap = dynamic_cast<Weapon*>(inv))
		{
			weap->SetAmmo(weap->GetAmmo() + SD::FLOAT::Round(weap->GetMaxAmmo().ToFLOAT() * AmmoRefillAmount).ToINT());
		}
	}

	Destroy();
}

void AmmoPickup::SetAmmoRefillAmount (SD::FLOAT newAmmoRefillAmount)
{
	AmmoRefillAmount = SD::Utils::Clamp<SD::FLOAT>(newAmmoRefillAmount, 0.0001f, 1.f);
}
DD_END