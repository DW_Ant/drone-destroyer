/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Inventory.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "Character.h"
#include "GameModifier.h"
#include "HealthComponent.h"
#include "HudMessage.h"
#include "Inventory.h"
#include "Msg_PickupItem.h"
#include "NavigationComponent.h"
#include "OscillatingTransformComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Inventory, SD, Entity)

void Inventory::InitProps ()
{
	Super::InitProps();

	ItemNameKey = SD::DString::EmptyString;
	ItemName = SD::DString::EmptyString;
	PickupRadius = 100.f;
	PickupImage = SD::DString::EmptyString;
	PossessedImage = SD::DString::EmptyString;
	PickupSound = TXT("Player_Low Health");
	IsDropped = false;
	Carrier = nullptr;

	PickupRenderTransform = nullptr;
	PickupRenderComponent = nullptr;
	PossessedTransformComponent = nullptr;
	PossessedRenderComponent = nullptr;
	PickupCollision = nullptr;
	Navigation = nullptr;
	PickupTicker = nullptr;
	PickupTickerInterval = 0.5f;
}

void Inventory::BeginObject ()
{
	Super::BeginObject();

	EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_PICKUPS;

	SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
	CHECK(localTexturePool != nullptr)

	if (!PickupImage.IsEmpty())
	{
		PickupRenderTransform = OscillatingTransformComponent::CreateObject();
		if (AddComponent(PickupRenderTransform))
		{
			PickupRenderTransform->SetCycleTime(2.f);
			PickupRenderTransform->PrimaryPosition = SD::Vector3(0.f, PickupRadius * 0.25f, 0.f);
			PickupRenderTransform->PrimaryPosition = SD::Vector3(0.f, PickupRadius * -0.25f, 0.f);

			PickupRenderComponent = SD::SpriteComponent::CreateObject();
			if (PickupRenderTransform->AddComponent(PickupRenderComponent))
			{
				PickupRenderComponent->SetSpriteTexture(localTexturePool->FindTexture(PickupImage));
				PickupRenderComponent->CenterOrigin = true;
				PickupRenderComponent->EditBaseSize() = SD::Vector2(PickupRadius, PickupRadius);
				PickupRenderComponent->SetVisibility(false);
				DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(PickupRenderComponent);
			}
			else
			{
				PickupRenderComponent = nullptr;
			}
		}
		else
		{
			PickupRenderTransform = nullptr;
		}
	}

	if (!PossessedImage.IsEmpty())
	{
		PossessedTransformComponent = SD::SceneTransformComponent::CreateObject();
		if (AddComponent(PossessedTransformComponent))
		{
			PossessedTransformComponent->SetScale(SD::Vector3(4.f, 4.f, 1.f));

			PossessedRenderComponent = SD::SpriteComponent::CreateObject();
			if (PossessedTransformComponent->AddComponent(PossessedRenderComponent))
			{
				PossessedRenderComponent->SetSpriteTexture(localTexturePool->FindTexture(PossessedImage));
				PossessedRenderComponent->CenterOrigin = true;
				PossessedRenderComponent->SetVisibility(false);
				DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(PossessedRenderComponent);
			}
			else
			{
				PossessedRenderComponent = nullptr;
			}
		}
		else
		{
			PossessedTransformComponent = nullptr;
		}
	}

	//HACK: Create a dummy render component (directly on this transform) so that this Inventory's AbsoluteTransform is computed every frame. The correct fix is to overhaul how the engine computes transforms.
	SD::SolidColorRenderComponent* dummyColor = SD::SolidColorRenderComponent::CreateObject();
	if (AddComponent(dummyColor))
	{
		dummyColor->SetVisibility(false);
		DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(dummyColor);
	}

	/*
	The PickupTicker accomplishes the following:
	- Gives the function that created this Inventory to immediately give to some Entity before pathing is provided.
	- Initializes the NavigationComponent after this Inventory item has come to a complete stop.
	*/
	PickupTicker = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(PickupTicker))
	{
		PickupTicker->SetTickInterval(PickupTickerInterval);
		PickupTicker->SetTicking(false);
		PickupTicker->SetTickHandler(SDFUNCTION_1PARAM(this, Inventory, HandlePickupTick, void, SD::FLOAT));
	}
	else
	{
		PickupTicker = nullptr;
	}

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetGame() != nullptr)
	for (GameModifier* gameMod : localDroneEngine->GetGame()->ReadGameModifiers())
	{
		gameMod->ModifyInventory(this);
	}

	EnterPickupState();
}

void Inventory::Destroyed ()
{
	if (Carrier != nullptr)
	{
		if (Carrier->GetFirstInventory() == this)
		{
			Carrier->FirstInventory = NextInventory;
		}
		else
		{
			for (Inventory* inv = Carrier->GetFirstInventory(); inv != nullptr; inv = inv->NextInventory.Get())
			{
				if (inv->NextInventory == this)
				{
					//Sever self from the inventory linked list
					inv->NextInventory = NextInventory;
					NextInventory = nullptr;
					break;
				}
			}
		}
	}

	Super::Destroyed();
}

void Inventory::InitializePhysics ()
{
	SD::PhysicsComponent::SComponentInitData initData;
	initData.BodyInit.PhysicsType = SD::PhysicsComponent::PT_Dynamic;
	initData.BodyInit.EnableContinuousCollision = false;
	initData.BodyInit.LinearDamping = 1.f; //slides across the floor until it comes to a stop.
	initData.BodyInit.IsFixedRotation = true;

	b2CircleShape circle;
	circle.m_radius = SD::Box2dUtils::ToBox2dDist(PickupRadius);
	initData.CollisionShape = &circle;
	initData.IsBlockingComponent = false;
	initData.CollisionGroup = 1;
	PickupCollision = SD::PhysicsComponent::CreateAndInitializeComponent<SD::PhysicsComponent>(this, initData);
	CHECK(PickupCollision != nullptr)

	PickupCollision->OnBeginContact = SDFUNCTION_2PARAM(this, Inventory, HandleBeginContact, void, SD::PhysicsComponent*, b2Contact*);

#ifdef DEBUG_MODE
	SD::PhysicsRenderer* renderComp = SD::PhysicsRenderer::CreateObject();
	if (AddComponent(renderComp))
	{
		renderComp->FillColor = SD::Color(0, 255, 0, DroneDestroyerEngineComponent::DebugPhysicsRenderAlpha);
		renderComp->SetVisibility(IsDropped);
		DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(renderComp);
	}
#endif
}

bool Inventory::CanPickupItem (Character* takerCandidate) const
{
	return (takerCandidate != nullptr && takerCandidate->IsAlive());
}

SD::INT Inventory::GetImportanceValue (Character* possibleTaker) const
{
	return 1;
}

void Inventory::GiveTo (Character* taker)
{
	ExitPickupState();
	EnterPossessedState(taker);

	Carrier = taker;

	if (taker != nullptr)
	{
		NextInventory = taker->GetFirstInventory();
		taker->FirstInventory = this;
	}
}

bool Inventory::CanBeDropped (Character* dropFrom) const
{
	return true;
}

void Inventory::DropItem (Character* dropFrom, const SD::Vector3& tossVelocity)
{
	ExitPossessedState(dropFrom);
	EnterPickupState();

	//Snap location ahead so the owner doesn't drop the item on themselves.
	if (!tossVelocity.IsEmpty() && dropFrom != nullptr)
	{
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr && localDroneEngine->GetEnvironment() != nullptr)

		SD::FLOAT collisionRadius = (dropFrom->GetCollisionRadius() + PickupRadius) * 1.1f;
		SD::Vector3 normalizedTossVelocity(tossVelocity);
		normalizedTossVelocity.Normalize();

		SetTranslation(dropFrom->ReadAbsTranslation() + (normalizedTossVelocity * collisionRadius));
		if (localDroneEngine->GetEnvironment()->FastTrace(dropFrom->ReadTranslation(), ReadTranslation()))
		{
			//I bumped into a wall, snap to the current position
			SetTranslation(dropFrom->ReadTranslation());
		}

		EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_PICKUPS;
		PickupCollision->OverwritePosition = true;

		if (!localDroneEngine->GetEnvironment()->FastTrace(ReadTranslation(), ReadTranslation() + (tossVelocity * 1.5f)))
		{
			//If I'm not going to bump into a wall, add velocity
			PickupCollision->SetVelocity(tossVelocity);
			PickupCollision->OverwriteVelocity = true;
		}
	}
}

void Inventory::RemoveInventory (Character* removeFrom)
{
	if (removeFrom == nullptr)
	{
		return;
	}

	if (removeFrom->GetFirstInventory() == this)
	{
		removeFrom->FirstInventory = NextInventory;
		NextInventory = nullptr;
		return;
	}

	for (Inventory* inv = removeFrom->GetFirstInventory(); inv != nullptr; inv = inv->NextInventory.Get())
	{
		if (inv->NextInventory == this)
		{
			inv->NextInventory = NextInventory;
			NextInventory = nullptr;
			break;
		}
	}

	removeFrom->DeleteInventory(this);
}

SD::DString Inventory::GetItemName () const
{
	if (!ItemName.IsEmpty())
	{
		return ItemName;
	}

	if (!ItemNameKey.IsEmpty())
	{
		//This item is not yet translated. Translate this item
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)
		ItemName = translator->TranslateText(ItemNameKey, TRANSLATION_FILE, TXT("Items"));
	}

	return ItemName;
}

void Inventory::EnterPickupState ()
{
	if (PickupRenderTransform != nullptr)
	{
		PickupRenderTransform->SetOscillatingActive(true);
	}

	if (PickupRenderComponent != nullptr)
	{
		PickupRenderComponent->SetVisibility(true);
	}

	if (PickupTicker != nullptr)
	{
		PickupTicker->SetTicking(true);
	}

	IsDropped = true;
	UpdatePhysicsVisualizerVisibility();
}

void Inventory::ExitPickupState ()
{
	if (PickupRenderTransform != nullptr)
	{
		PickupRenderTransform->SetOscillatingActive(false);
	}

	if (PickupRenderComponent != nullptr)
	{
		PickupRenderComponent->SetVisibility(false);
	}

	if (Navigation != nullptr)
	{
		Navigation->Destroy();
		Navigation = nullptr;
	}

	if (PickupTicker != nullptr)
	{
		PickupTicker->SetTicking(false);
	}

	IsDropped = false;
	UpdatePhysicsVisualizerVisibility();
}

void Inventory::EnterPossessedState (Character* possesser)
{
	SetRelativeTo(possesser);
	EditTranslation().X = 0.f;
	EditTranslation().Y = 0.f;

	if (PickupCollision != nullptr)
	{
		//Notify the PhysicsComponent that we've overwrote the position
		PickupCollision->OverwritePosition = true;

		//Null velocity incase this item was collected while drifting as a pickup
		PickupCollision->SetVelocity(SD::Vector3::ZeroVector);
		PickupCollision->OverwriteVelocity = true;
	}

	if (PossessedRenderComponent != nullptr)
	{
		PossessedRenderComponent->SetVisibility(true);
		if (possesser != nullptr)
		{
			SD::FLOAT radius = possesser->GetCollisionRadius();
			PossessedRenderComponent->EditBaseSize() = SD::Vector2(radius, radius);
		}
	}
}

void Inventory::ExitPossessedState (Character* oldOwner)
{
	RemoveInventory(oldOwner);
	SetRelativeTo(nullptr);
	Carrier = nullptr;

	//Null out rotation so that NavigationPoints built relative to this point are not rotated.
	SetRotation(SD::Rotator::ZERO_ROTATOR);

	if (PossessedRenderComponent != nullptr)
	{
		PossessedRenderComponent->SetVisibility(false);
	}
}

void Inventory::InitializeNavigationComponent ()
{
	if (Navigation != nullptr)
	{
		return; //already initialized
	}

	Navigation = NavigationComponent::CreateObject();
	if (AddComponent(Navigation))
	{
#ifdef DEBUG_MODE
		Navigation->DebugName = DebugName + TXT("_NavigationComponent");
#endif
		Navigation->SetWanderRadius(PickupRadius * 0.5f);
		Navigation->BuildPathing(true);
	}
	else
	{
		Navigation = nullptr;
	}
}

void Inventory::UpdatePhysicsVisualizerVisibility ()
{
#ifdef DEBUG_MODE
	SD::PhysicsRenderer* physicsVisualizer = dynamic_cast<SD::PhysicsRenderer*>(FindSubComponent(SD::PhysicsRenderer::SStaticClass(), false));
	if (physicsVisualizer != nullptr)
	{
		physicsVisualizer->SetVisibility(IsDropped);
	}
#endif
}

void Inventory::HandlePickupTick (SD::FLOAT deltaSec)
{
	if (Navigation == nullptr && IsDropped && PickupCollision != nullptr)
	{
		//Create the NavigationComponent after this Inventory Entity has come to a near stop.
		if (PickupCollision->ReadVelocity().IsNearlyEqual(SD::Vector3::ZeroVector, 32.f))
		{
			InitializeNavigationComponent();

			//Stop sliding
			PickupCollision->SetVelocity(SD::Vector3::ZeroVector);
		}
	}
}

void Inventory::HandleBeginContact (SD::PhysicsComponent* otherComp, b2Contact* contactData)
{
	if (!IsDropped)
	{
		return;
	}

	Character* bumpedInto = dynamic_cast<Character*>(otherComp->GetRootEntity());
	if (bumpedInto != nullptr && CanPickupItem(bumpedInto))
	{
		if (OnTaken.IsBounded())
		{
			OnTaken.Execute(this, bumpedInto);
		}

		GiveTo(bumpedInto);

		if (!PickupSound.IsEmpty())
		{
			DroneDestroyerEngineComponent::SPlayLocalSound(PickupSound, ReadAbsTranslation(), 1.f);
		}

		if (bumpedInto->IsHuman())
		{
			//Only send this message on collision. It's not in GiveTo, itself, so that this message is not sent when the item is directly given to the player (eg: default inventory).
			HudMessage::CreateAndRegisterMsgToHud(Msg_PickupItem::SStaticClass(), GetItemName());
		}
	}
}

DD_END