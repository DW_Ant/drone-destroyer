/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanel_UserSettings.cpp
=====================================================================
*/

#include <regex>

#include "MainMenu.h"
#include "MenuPanel_UserSettings.h"
#include "SoundEngineComponent.h"
#include "SoundMessenger.h"
#include "UserSettings.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, MenuPanel_UserSettings, DD, MenuPanelComponent)

void MenuPanel_UserSettings::InitProps ()
{
	Super::InitProps();

	ConfigKey = TXT("UserSettings");

	ControlsUi = nullptr;
	DynamicCamCheckbox = nullptr;
	GameVolumeField = nullptr;
	MusicVolumeField = nullptr;
}

void MenuPanel_UserSettings::RestorePanel ()
{
	UserSettings* settings = UserSettings::GetUserSettings();
	if (settings == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to restore user settings. Could not find the object instance responsible for loading the configuration file."));
		return;
	}

	const SD::DString& cursorName = settings->ReadCursorName();
	if (!cursorName.IsEmpty())
	{
		for (const SMouseIconData& mouseIcon : MouseIcons)
		{
			if (mouseIcon.IconName.Compare(cursorName, SD::DString::CC_CaseSensitive) == 0)
			{
				//Not only this sets the mouse icon (in case the user canceled UserSettings), this also sets the mouse button enabledness from returning to this menu.
				SelectMouseIcon(mouseIcon.Button);
				break;
			}
		}
	}

	if (DynamicCamCheckbox != nullptr)
	{
		DynamicCamCheckbox->SetChecked(settings->GetDynamicCam());
	}

	if (GameVolumeField != nullptr)
	{
		GameVolumeField->SetText(settings->GetGameVolume().ToString());
	}

	if (MusicVolumeField != nullptr)
	{
		MusicVolumeField->SetText(settings->GetMusicVolume().ToString());
	}

	//Reapply volume settings in case the player cancelled out of this menu.
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		SD::FLOAT gameVolume = settings->GetGameVolume().ToFLOAT();
		gameVolume *= 0.01f;
		msg->SetVolumeLevel(false, gameVolume);

		SD::FLOAT musicVolume = settings->GetMusicVolume().ToFLOAT();
		musicVolume *= 0.01f;
		msg->SetVolumeLevel(true, musicVolume);
	}
}

void MenuPanel_UserSettings::SavePanelState ()
{
	UserSettings* settings = UserSettings::GetUserSettings();
	if (settings == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to save user settings. Could not obtain the object instance responsible for storing the information to a configuration file."));
		return;
	}

	for (const SMouseIconData& mouseIcon : MouseIcons)
	{
		//Assume the disabled button is the selected mouse icon
		if (!mouseIcon.Button->GetEnabled())
		{
			settings->SetCursorName(mouseIcon.IconName);
			break;
		}
	}

	if (DynamicCamCheckbox != nullptr)
	{
		settings->SetDynamicCam(DynamicCamCheckbox->GetChecked());
	}

	if (GameVolumeField != nullptr)
	{
		SD::INT gameVolume = std::atoi(GameVolumeField->GetContent().ToCString());
		settings->SetGameVolume(gameVolume);
	}
	
	if (MusicVolumeField != nullptr)
	{
		SD::INT musicVolume = std::atoi(MusicVolumeField->GetContent().ToCString());
		settings->SetMusicVolume(musicVolume);
	}

	settings->SaveUserSettings();
}

void MenuPanel_UserSettings::InitializeComponents ()
{
	Super::InitializeComponents();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	//Add a almost full frame behind this since this panel is reused in escape menu (hides the game behind it)
	SD::FrameComponent* fadeOutBorders = SD::FrameComponent::CreateObject();
	if (AddComponent(fadeOutBorders))
	{
		fadeOutBorders->SetPosition(SD::Vector2::ZeroVector);
		fadeOutBorders->SetSize(SD::Vector2(1.f, 1.f));
		fadeOutBorders->SetBorderThickness(1.f);
		fadeOutBorders->SetLockedFrame(true);
		if (fadeOutBorders->RenderComponent != nullptr)
		{
			fadeOutBorders->RenderComponent->ClearSpriteTexture();
			fadeOutBorders->RenderComponent->FillColor = SD::Color(32, 32, 32, 160);
		}

		//completely opaque background
		SD::FrameComponent* background = SD::FrameComponent::CreateObject();
		if (fadeOutBorders->AddComponent(background))
		{
			background->SetPosition(SD::Vector2(0.15f, 0.1f));
			background->SetSize(SD::Vector2(0.7f, 0.8f));
			background->SetBorderThickness(1.f);
			background->SetLockedFrame(true);
			if (background->RenderComponent != nullptr)
			{
				background->RenderComponent->ClearSpriteTexture();
				background->RenderComponent->FillColor = SD::Color(96, 96, 96, 255);
			}

			SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
			CHECK(localTexturePool != nullptr)

			//Initialize Mouse Icon buttons
			{
				std::vector<SD::DString> colors({TXT("Red"), TXT("Yellow"), TXT("Green"), TXT("Cyan"), TXT("Blue"), TXT("Magenta"), TXT("Black"), TXT("White")});
				SD::FLOAT topMargin = 0.01f;
				SD::FLOAT iconSpacing = 0.05f;
				SD::FLOAT iconSize = (1.f - (SD::FLOAT::MakeFloat(colors.size() + 1) * iconSpacing)) / SD::FLOAT::MakeFloat(colors.size());
				SD::FLOAT drawPosX = iconSpacing;
				for (size_t i = 0; i < colors.size(); ++i)
				{
					SD::ButtonComponent* iconButton = SD::ButtonComponent::CreateObject();
					if (background->AddComponent(iconButton))
					{
						iconButton->SetPosition(SD::Vector2(drawPosX, topMargin));
						iconButton->SetSize(SD::Vector2(iconSize, iconSize * 1.7f));
						drawPosX += iconSize + iconSpacing;
						SD::DString iconName = TXT("DroneDestroyer.Interface.Crosshair_") + colors.at(i);
						if (iconButton->GetButtonStateComponent() != nullptr)
						{
							iconButton->GetButtonStateComponent()->Destroy();
						}

						if (iconButton->CaptionComponent != nullptr)
						{
							iconButton->CaptionComponent->Destroy();
						}

						if (iconButton->GetRenderComponent() != nullptr)
						{
							iconButton->GetRenderComponent()->SetSpriteTexture(localTexturePool->FindTexture(iconName, true));
							iconButton->GetRenderComponent()->SetDrawCoordinatesMultipliers(1.f, 1.f);
						}
						iconButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_UserSettings, HandleMouseIconReleased, void, SD::ButtonComponent*));
						MouseIcons.push_back(SMouseIconData(iconName, iconButton));
					}
				}
			}

			DynamicCamCheckbox = SD::CheckboxComponent::CreateObject();
			if (background->AddComponent(DynamicCamCheckbox))
			{
				DynamicCamCheckbox->SetPosition(SD::Vector2(0.05f, 0.15f));
				DynamicCamCheckbox->SetSize(SD::Vector2(0.4f, 0.05f));
				DynamicCamCheckbox->CaptionComponent->SetText(translator->TranslateText(TXT("DynamicCam"), TRANSLATION_FILE, TXT("UserSettings")));
				DynamicCamCheckbox->OnChecked = SDFUNCTION_1PARAM(this, MenuPanel_UserSettings, HandleDynamicCamChecked, void, SD::CheckboxComponent*);
			}

			SD::LabelComponent* gameVolLabel = SD::LabelComponent::CreateObject();
			if (background->AddComponent(gameVolLabel))
			{
				gameVolLabel->SetAutoRefresh(false);
				gameVolLabel->SetPosition(SD::Vector2(0.05f, 0.25f));
				gameVolLabel->SetSize(SD::Vector2(0.3f, 0.05f));
				gameVolLabel->SetWrapText(false);
				gameVolLabel->SetClampText(true);
				gameVolLabel->SetText(translator->TranslateText(TXT("GameVolume"), TRANSLATION_FILE, TXT("UserSettings")));
				gameVolLabel->SetAutoRefresh(true);

				SD::FrameComponent* textBackground = SD::FrameComponent::CreateObject();
				if (background->AddComponent(textBackground))
				{
					textBackground->SetPosition(SD::Vector2(0.4f, gameVolLabel->ReadPosition().Y));
					textBackground->SetSize(SD::Vector2(0.075f, gameVolLabel->ReadSize().Y));
					textBackground->SetLockedFrame(true);
					textBackground->SetBorderThickness(1.f);
					if (textBackground->RenderComponent != nullptr)
					{
						textBackground->RenderComponent->ClearSpriteTexture();
						textBackground->RenderComponent->FillColor = SD::Color::BLACK;
					}

					GameVolumeField = SD::TextFieldComponent::CreateObject();
					if (textBackground->AddComponent(GameVolumeField))
					{
						GameVolumeField->MaxNumCharacters = 3;
						GameVolumeField->OnAllowTextInput = SDFUNCTION_1PARAM(this, MenuPanel_UserSettings, HandleAllowVolumeInput, bool, const SD::DString&);
						GameVolumeField->OnReturn = SDFUNCTION_1PARAM(this, MenuPanel_UserSettings, HandleApplyGameVolume, void, SD::TextFieldComponent*);
					}
					else
					{
						GameVolumeField = nullptr;
					}
				}
			}

			SD::LabelComponent* musicVolLabel = SD::LabelComponent::CreateObject();
			if (background->AddComponent(musicVolLabel))
			{
				musicVolLabel->SetAutoRefresh(false);
				musicVolLabel->SetPosition(SD::Vector2(0.55f, 0.25f));
				musicVolLabel->SetSize(SD::Vector2(0.3f, 0.05f));
				musicVolLabel->SetWrapText(false);
				musicVolLabel->SetClampText(true);
				musicVolLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Right);
				musicVolLabel->SetText(translator->TranslateText(TXT("MusicVolume"), TRANSLATION_FILE, TXT("UserSettings")));
				musicVolLabel->SetAutoRefresh(true);

				SD::FrameComponent* textBackground = SD::FrameComponent::CreateObject();
				if (background->AddComponent(textBackground))
				{
					textBackground->SetPosition(SD::Vector2(0.9f, musicVolLabel->ReadPosition().Y));
					textBackground->SetSize(SD::Vector2(0.075f, musicVolLabel->ReadSize().Y));
					textBackground->SetLockedFrame(true);
					textBackground->SetBorderThickness(1.f);
					if (textBackground->RenderComponent != nullptr)
					{
						textBackground->RenderComponent->ClearSpriteTexture();
						textBackground->RenderComponent->FillColor = SD::Color::BLACK;
					}

					MusicVolumeField = SD::TextFieldComponent::CreateObject();
					if (textBackground->AddComponent(MusicVolumeField))
					{
						MusicVolumeField->MaxNumCharacters = 3;
						MusicVolumeField->OnAllowTextInput = SDFUNCTION_1PARAM(this, MenuPanel_UserSettings, HandleAllowVolumeInput, bool, const SD::DString&);
						MusicVolumeField->OnReturn = SDFUNCTION_1PARAM(this, MenuPanel_UserSettings, HandleApplyMusicVolume, void, SD::TextFieldComponent*);
					}
					else
					{
						MusicVolumeField = nullptr;
					}
				}
			}

			SD::ScrollbarComponent* controlsScroll = SD::ScrollbarComponent::CreateObject();
			if (background->AddComponent(controlsScroll))
			{
				controlsScroll->SetPosition(SD::Vector2(0.2f, 0.35f));
				controlsScroll->SetSize(SD::Vector2(0.6f, 0.5f));
				controlsScroll->SetHideControlsWhenFull(true);
				controlsScroll->SetZoomEnabled(false);

				SD::LabelComponent* controlsHelp = nullptr;
				SD::LabelComponent::CreateLabelWithinScrollbar(controlsScroll, OUT ControlsUi, OUT controlsHelp);
				if (ControlsUi != nullptr && controlsHelp != nullptr)
				{
					controlsHelp->SetAutoRefresh(false);
					controlsHelp->SetWrapText(false);
					controlsHelp->SetCharacterSize(12);
					controlsHelp->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
					controlsHelp->SetText(translator->TranslateText(TXT("Controls"), TRANSLATION_FILE, TXT("UserSettings")));
					controlsHelp->SetAutoRefresh(true);
				}
			}

			SD::ButtonComponent* backButton = SD::ButtonComponent::CreateObject();
			if (background->AddComponent(backButton))
			{
				backButton->SetPosition(SD::Vector2(0.1f, 0.9f));
				backButton->SetSize(SD::Vector2(0.15f, 0.05f));
				backButton->SetCaptionText(translator->TranslateText(TXT("Back"), TRANSLATION_FILE, TXT("UserSettings")));
				backButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_UserSettings, HandleBackReleased, void, SD::ButtonComponent*));
			}

			SD::ButtonComponent* applyButton = SD::ButtonComponent::CreateObject();
			if (background->AddComponent(applyButton))
			{
				applyButton->SetPosition(SD::Vector2(0.8f, 0.9f));
				applyButton->SetSize(SD::Vector2(0.15f, 0.05f));
				applyButton->SetCaptionText(translator->TranslateText(TXT("Apply"), TRANSLATION_FILE, TXT("UserSettings")));
				applyButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, MenuPanel_UserSettings, HandleApplyReleased, void, SD::ButtonComponent*));
			}
		}
	}
}

void MenuPanel_UserSettings::Destroyed ()
{
	if (OnClosed.IsBounded())
	{
		OnClosed();
	}

	if (ControlsUi != nullptr)
	{
		ControlsUi->Destroy();
		ControlsUi = nullptr;
	}

	Super::Destroyed();
}

void MenuPanel_UserSettings::SelectMouseIcon (SD::ButtonComponent* associatedUiComponent)
{
	for (const SMouseIconData& mouseData : MouseIcons)
	{
		if (mouseData.Button == associatedUiComponent)
		{
			mouseData.Button->SetEnabled(false);

			SD::InputEngineComponent* localInputEngine = SD::InputEngineComponent::Find();
			CHECK(localInputEngine != nullptr)
			if (SD::MousePointer* mouse = localInputEngine->GetMouse())
			{
				SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
				CHECK(localTexturePool != nullptr)
				mouse->SetDefaultIcon(localTexturePool->FindTexture(mouseData.IconName));
			}
		}
		else
		{
			mouseData.Button->SetEnabled(true);
		}
	}
}

void MenuPanel_UserSettings::ClampTextField (SD::TextFieldComponent* uiComponent, SD::Range<SD::INT> clamp) const
{
	CHECK(uiComponent != nullptr)

	SD::INT originalNum = SD::INT(std::atoi(uiComponent->GetContent().ToCString()));
	SD::INT clampedNum = SD::Utils::Clamp<SD::INT>(originalNum, clamp.Min, clamp.Max);
	if (originalNum != clampedNum)
	{
		uiComponent->SetText(clampedNum.ToString());
	}
}

void MenuPanel_UserSettings::HandleMouseIconReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	SelectMouseIcon(uiComponent);
}

void MenuPanel_UserSettings::HandleDynamicCamChecked (SD::CheckboxComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, (uiComponent->IsChecked()) ? SoundEngineComponent::SOUND_CLICK_NORMAL : SoundEngineComponent::SOUND_CLICK_LIGHT);
	}
}

bool MenuPanel_UserSettings::HandleAllowVolumeInput (const SD::DString& txt)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_TYPING);
	}

	std::regex regex("[0-9]"); //Only accept numbers (not even decimals or negative values)
	std::cmatch match;

	return std::regex_match(txt.ReadString().c_str(), OUT match, regex);
}

void MenuPanel_UserSettings::HandleApplyGameVolume (SD::TextFieldComponent* uiComponent)
{
	ClampTextField(uiComponent, SD::Range<SD::INT>(0, 100));

	//Play a sample sound
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		SD::FLOAT newVol = std::stof(uiComponent->GetContent().ToCString());
		newVol *= 0.01f;
		msg->SetVolumeLevel(false, newVol);
		msg->PlaySound(false, TXT("PlayerDeathShort"), 1.f);
	}
}

void MenuPanel_UserSettings::HandleApplyMusicVolume (SD::TextFieldComponent* uiComponent)
{
	ClampTextField(uiComponent, SD::Range<SD::INT>(0, 100));

	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		SD::FLOAT newVol = std::stof(uiComponent->GetContent().ToCString());
		newVol *= 0.01f;
		msg->SetVolumeLevel(true, newVol);
	}
}

void MenuPanel_UserSettings::HandleBackReleased (SD::ButtonComponent* uiComponent)
{
	RestorePanel();

	MainMenu* owningMenu = GetOwningMenu();
	if (owningMenu != nullptr)
	{
		owningMenu->NavigateBack();
	}
	else
	{
		if (SoundMessenger* msg = SoundMessenger::GetMessenger())
		{
			msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_LIGHT);
		}

		Destroy();
	}
}

void MenuPanel_UserSettings::HandleApplyReleased (SD::ButtonComponent* uiComponent)
{
	//Apply audio settings (since users are not required to hit enter to apply).
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		if (GameVolumeField != nullptr)
		{
			ClampTextField(GameVolumeField, SD::Range<SD::INT>(0, 100));
			SD::FLOAT newVol = std::stof(GameVolumeField->GetContent().ToCString());
			newVol *= 0.01f;
			msg->SetVolumeLevel(false, newVol);
		}

		if (MusicVolumeField != nullptr)
		{
			ClampTextField(MusicVolumeField, SD::Range<SD::INT>(0, 100));
			SD::FLOAT newVol = std::stof(MusicVolumeField->GetContent().ToCString());
			newVol *= 0.01f;
			msg->SetVolumeLevel(true, newVol);
		}
	}

	MainMenu* owningMenu = GetOwningMenu();
	if (owningMenu != nullptr)
	{
		owningMenu->NavigateBack();
	}
	else
	{
		if (SoundMessenger* msg = SoundMessenger::GetMessenger())
		{
			msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_LIGHT);
		}

		SavePanelState();
		Destroy();
	}
}

DD_END