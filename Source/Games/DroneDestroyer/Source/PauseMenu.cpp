/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PauseMenu.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "MenuPanel_UserSettings.h"
#include "PauseMenu.h"
#include "SoundEngineComponent.h"
#include "SoundMessenger.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, PauseMenu, SD, GuiEntity)

void PauseMenu::InitProps ()
{
	Super::InitProps();

	MainFrame = nullptr;
	UserSettings = nullptr;
}

void PauseMenu::ConstructUI ()
{
	Super::ConstructUI();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)

	MainFrame = SD::FrameComponent::CreateObject();
	if (AddComponent(MainFrame))
	{
		MainFrame->SetPosition(SD::Vector2(0.25f, 0.25f));
		MainFrame->SetSize(SD::Vector2(0.5f, 0.5f));
		MainFrame->SetLockedFrame(true);

		SD::FLOAT buttonSpacing = 0.1f;
		SD::FLOAT buttonHeight = (1.f - (buttonSpacing * 4.f)) / 3;
		SD::FLOAT drawPosY = buttonSpacing;
		SD::ButtonComponent* resumeButton = SD::ButtonComponent::CreateObject();
		if (MainFrame->AddComponent(resumeButton))
		{
			resumeButton->SetPosition(SD::Vector2(buttonSpacing, drawPosY));
			resumeButton->SetSize(SD::Vector2(1.f - (buttonSpacing * 2.f), buttonHeight));
			drawPosY += buttonSpacing + buttonHeight;
			resumeButton->SetCaptionText(translator->TranslateText(TXT("Resume"), TRANSLATION_FILE, TXT("PauseMenu")));
			resumeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PauseMenu, HandleResumeReleased, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* settingsButton = SD::ButtonComponent::CreateObject();
		if (MainFrame->AddComponent(settingsButton))
		{
			settingsButton->SetPosition(SD::Vector2(buttonSpacing, drawPosY));
			settingsButton->SetSize(SD::Vector2(1.f - (buttonSpacing * 2.f), buttonHeight));
			drawPosY += buttonSpacing + buttonHeight;
			settingsButton->SetCaptionText(translator->TranslateText(TXT("UserSettings"), TRANSLATION_FILE, TXT("PauseMenu")));
			settingsButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PauseMenu, HandleUserSettingsReleased, void, SD::ButtonComponent*));
		}

		SD::ButtonComponent* quitButton = SD::ButtonComponent::CreateObject();
		if (MainFrame->AddComponent(quitButton))
		{
			quitButton->SetPosition(SD::Vector2(buttonSpacing, drawPosY));
			quitButton->SetSize(SD::Vector2(1.f - (buttonSpacing * 2.f), buttonHeight));
			drawPosY += buttonSpacing + buttonHeight;
			quitButton->SetCaptionText(translator->TranslateText(TXT("Quit"), TRANSLATION_FILE, TXT("PauseMenu")));
			quitButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, PauseMenu, HandleQuitReleased, void, SD::ButtonComponent*));
		}
	}
	else
	{
		MainFrame = nullptr;
	}
}

void PauseMenu::Destroyed ()
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	if (localDroneEngine->GetGame() != nullptr && !localDroneEngine->GetGame()->GetHasEnded())
	{
		localDroneEngine->SetPausedGame(false);
	}

	Super::Destroyed();
}

bool PauseMenu::HandleInput (const sf::Event& evnt)
{
	bool consumeEvent = false;

	if (evnt.type == sf::Event::EventType::KeyPressed || evnt.type == sf::Event::EventType::KeyReleased)
	{
		if (evnt.key.code == sf::Keyboard::Escape)
		{
			consumeEvent = true;
			if (evnt.type == sf::Event::EventType::KeyReleased)
			{
				if (UserSettings != nullptr)
				{
					if (SoundMessenger* msg = SoundMessenger::GetMessenger())
					{
						msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_LIGHT);
					}

					UserSettings->RestorePanel();
					UserSettings->Destroy();
					UserSettings = nullptr;
				}
				else
				{
					if (SoundMessenger* msg = SoundMessenger::GetMessenger())
					{
						msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
					}

					Destroy();
				}
			}
		}
	}

	return consumeEvent;
}

void PauseMenu::HandleResumeReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	Destroy();
}

void PauseMenu::HandleUserSettingsReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_NORMAL);
	}

	UserSettings = MenuPanel_UserSettings::CreateObject();
	if (AddComponent(UserSettings))
	{
		UserSettings->SetPosition(SD::Vector2::ZeroVector);
		UserSettings->SetSize(SD::Vector2(1.f, 1.f));
		UserSettings->OnClosed = SDFUNCTION(this, PauseMenu, HandleUserSettingsClosed, void);
		UserSettings->RestorePanel();

		if (MainFrame != nullptr)
		{
			MainFrame->SetVisibility(false);
		}
	}
	else
	{
		UserSettings = nullptr;
	}
}

void PauseMenu::HandleQuitReleased (SD::ButtonComponent* uiComponent)
{
	if (SoundMessenger* msg = SoundMessenger::GetMessenger())
	{
		msg->PlaySound(false, SoundEngineComponent::SOUND_CLICK_HEAVY);
	}

	Destroy();

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	localDroneEngine->ReturnToTitleScreen();
}

void PauseMenu::HandleUserSettingsClosed ()
{
	if (UserSettings != nullptr)
	{
		UserSettings = nullptr;
	}

	if (MainFrame != nullptr)
	{
		MainFrame->SetVisibility(true);
	}
}

DD_END