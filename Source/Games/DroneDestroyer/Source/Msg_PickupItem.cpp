/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Msg_PickupItem.cpp
=====================================================================
*/

#include "Msg_PickupItem.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Msg_PickupItem, DD, HudMessage)

void Msg_PickupItem::InitProps ()
{
	Super::InitProps();

	DisplayTime = 2.f;
	StartFadeTime = 0.5f;
	TextKey = TXT("PickedUp");
}

void Msg_PickupItem::BeginObject ()
{
	Super::BeginObject();

	SetAutoRefresh(false);
	SetPosition(SD::Vector2(0.f, 0.8f));
	SetAnchorRight(0.f);
	SetAnchorLeft(0.f);
	SetHorizontalAlignment(eHorizontalAlignment::HA_Center);
	SetCharacterSize(20);
	SetWrapText(false);
	SetClampText(false);
	SetAutoRefresh(true);
}

DD_END