/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Weapon.cpp
=====================================================================
*/

#include "Character.h"
#include "DroneDestroyerEngineComponent.h"
#include "Extinction.h"
#include "Projectile.h"
#include "Weapon.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Weapon, DD, Inventory)

const SD::INT Weapon::MAX_WEAPON_PRIORITY(100);

void Weapon::InitProps ()
{
	Super::InitProps();

	PickupSound = TXT("Pickup_Weapon");

	ShortNameKey = TXT("Unknown");
	StartingAmmo = 10;
	MaxAmmo = 25;
	Ammo = StartingAmmo;
	InfiniteAmmo = false;
	FireRate = 1.f;
	RecoilDistance = -25.f;
	FireSpread = 0.f;
	ProjectileClass = nullptr;
	ShootSoundVolume = 0.33f;
	WeaponPriority = 0;
	WeaponState = WS_NotCarried;
	SwitchToTime = 0.25f;
	SwitchOutTime = 0.25f;
	IsExtinctionMode = false;
	IsDroneWeapon = false;
	DropExpirationTime = 60.f;
	PurgeTimer = nullptr;
	WeaponAnimTick = nullptr;
	WeaponFireTick = nullptr;

	LastFireTime = 0.f;
	SwitchTime = 0.f;
}

void Weapon::BeginObject ()
{
	Super::BeginObject();

	WeaponAnimTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(WeaponAnimTick))
	{
		WeaponAnimTick->SetTicking(false);
		WeaponAnimTick->SetTickHandler(SDFUNCTION_1PARAM(this, Weapon, HandleWeaponAnimTick, void, SD::FLOAT));
	}
	else
	{
		WeaponAnimTick = nullptr;
	}

	WeaponFireTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(WeaponFireTick))
	{
		WeaponFireTick->SetTicking(false);
		WeaponFireTick->SetTickInterval(FireRate);
		WeaponFireTick->SetTickHandler(SDFUNCTION_1PARAM(this, Weapon, HandleWeaponFireTick, void, SD::FLOAT));
	}
	else
	{
		WeaponFireTick = nullptr;
	}

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)
	IsExtinctionMode = (dynamic_cast<Extinction*>(localDroneEngine->GetGame()) != nullptr);
}

SD::INT Weapon::GetImportanceValue (Character* possibleTaker) const
{
	CHECK(possibleTaker != nullptr)

	SD::INT result = 2; //Default to a slight higher value than normal pathing. Even if we don't need this weapon, taking this will prevent opponents from taking it.

	//Check the taker's inventory list to see if they have this weapon.
	bool alreadyHasWeapon = false;
	for (Inventory* inv = possibleTaker->GetFirstInventory(); inv != nullptr; inv = inv->GetNextInventory())
	{
		if (inv->StaticClass() == StaticClass()) //Found a matching weapon type
		{
			Weapon* weap = dynamic_cast<Weapon*>(inv);
			CHECK(weap != nullptr);

			//Check if they need ammo and adjust priority based on how badly they need it.
			SD::FLOAT percentFull = (weap->Ammo.ToFLOAT() / weap->MaxAmmo.ToFLOAT());

			//Pick a value ranging from 2 to 50 based on the percentFull where full ammo yields 2, empty yields 50.
			result = SD::Utils::Lerp<SD::INT, SD::FLOAT>(percentFull, 50, 2);
			alreadyHasWeapon = true;
			break;
		}
	}

	if (!alreadyHasWeapon)
	{
		result = 50; //High priority!
	}

	return result;
}

void Weapon::GiveTo (Character* taker)
{
	Super::GiveTo(taker);
	CHECK(taker != nullptr)

	//If the taker already has this weapon. Simply give ammo instead.
	for (Inventory* inv = taker->GetFirstInventory(); inv != nullptr; inv = inv->GetNextInventory())
	{
		if (inv != this && inv->StaticClass() == StaticClass())
		{
			Weapon* otherWeap = dynamic_cast<Weapon*>(inv);
			CHECK(otherWeap != nullptr) //Should never happen since we compared classes earlier
			otherWeap->SetAmmo(otherWeap->GetAmmo() + Ammo);

			//The user's current weapon is out of ammo. Switch to this weapon that just replenished ammo.
			if (taker->GetDrawnWeapon() != nullptr && taker->GetDrawnWeapon()->Ammo <= 0)
			{
				taker->DrawWeaponTo(otherWeap);
			}

			Destroy();
			return;
		}
	}

	WeaponState = WS_Stowed;

	//TODO: Add a config variable for auto weapon switching
	if (taker->GetDrawnWeapon() == nullptr || taker->GetDrawnWeapon()->WeaponPriority < WeaponPriority)
	{
		taker->DrawWeaponTo(this);
	}
}

bool Weapon::CanBeDropped (Character* dropFrom) const
{
	if (!Super::CanBeDropped(dropFrom))
	{
		return false;
	}

	return (Ammo > 0);
}

void Weapon::RemoveInventory (Character* removeFrom)
{
	if (IsShooting())
	{
		StopFiring();
	}

	WeaponState = WS_NotCarried;

	//Removes this weapon from the character's inventory list
	Super::RemoveInventory(removeFrom);
}

void Weapon::EnterPossessedState (Character* possesser)
{
	Super::EnterPossessedState(possesser);

	if (PossessedRenderComponent != nullptr)
	{
		//This weapon is only visible when it's the character's held weapon
		PossessedRenderComponent->SetVisibility(false);
	}

	if (PurgeTimer != nullptr)
	{
		PurgeTimer->Destroy();
		PurgeTimer = nullptr;
	}
}

bool Weapon::CanSwitchTo () const
{
	return (Ammo > 0);
}

void Weapon::StartSwitchTo ()
{
	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)

	SwitchTime = localEngine->GetElapsedTime();

	if (PossessedRenderComponent != nullptr)
	{
		PossessedRenderComponent->SetVisibility(true);
	}

	if (WeaponAnimTick != nullptr)
	{
		WeaponAnimTick->SetTicking(true);
	}

	WeaponState = WS_PullingOut;
}

void Weapon::EndSwitchTo ()
{
	WeaponState = WS_Drawn;

	if (OnWeaponSwitchIn.IsBounded())
	{
		OnWeaponSwitchIn.Execute(this);
	}
}

void Weapon::StartSwitchOut ()
{
	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)

	SwitchTime = localEngine->GetElapsedTime();
	WeaponState = WS_StowingAway;
	StopFiring();

	//If we're pulling the weapon away in a middle of shoot animation, reset its animation.
	if (PossessedTransformComponent != nullptr)
	{
		PossessedTransformComponent->SetTranslation(SD::Vector3::ZeroVector);
	}
}

void Weapon::EndSwitchOut ()
{
	//Hide the weapon
	if (PossessedRenderComponent != nullptr)
	{
		PossessedRenderComponent->SetVisibility(false);
	}

	if (WeaponAnimTick != nullptr)
	{
		WeaponAnimTick->SetTicking(false);
	}

	WeaponState = WS_Stowed;

	if (OnWeaponSwitchOut.IsBounded())
	{
		OnWeaponSwitchOut.Execute(this);
	}
}

bool Weapon::CanShoot () const
{
	return (WeaponState == WS_Drawn);
}

void Weapon::StartFiring ()
{
	//Figure out the delta time between last fired and now.
	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)

	SD::FLOAT deltaFireTime = localEngine->GetElapsedTime() - LastFireTime;
	if (deltaFireTime >= FireRate)
	{
		//Enough time elapsed to fire immediately
		if (Ammo > 0)
		{
			Fire();
		}

		if (WeaponFireTick != nullptr)
		{
			WeaponFireTick->SetTicking(true);
			WeaponFireTick->SetTickInterval(FireRate);
			WeaponFireTick->ResetAccumulatedTickTime();
		}
	}
	else if (WeaponFireTick != nullptr)
	{
		WeaponFireTick->SetTicking(true);
		WeaponFireTick->SetTickInterval(FireRate - deltaFireTime); //Wait for the remaining time before shooting.
		WeaponFireTick->ResetAccumulatedTickTime();
	}
}

bool Weapon::IsShooting () const
{
	return (WeaponFireTick != nullptr && WeaponFireTick->IsTicking());
}

void Weapon::StopFiring ()
{
	if (WeaponFireTick != nullptr)
	{
		WeaponFireTick->SetTicking(false);
	}
}

void Weapon::StartPurgeTimer ()
{
	if (PurgeTimer != nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to properly start the weapon purge timer for %s since there is already weapon purge timer active."), ToString());
		return;
	}

	PurgeTimer = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(PurgeTimer))
	{
		PurgeTimer->SetTickInterval(DropExpirationTime);
		PurgeTimer->SetTickHandler(SDFUNCTION_1PARAM(this, Weapon, HandlePurgeWeapon, void, SD::FLOAT));
	}
	else
	{
		PurgeTimer = nullptr;
	}
}

void Weapon::SetAmmo (SD::INT newAmmo)
{
	Ammo = SD::Utils::Min(newAmmo, MaxAmmo);

	OnAmmoChanged.Broadcast(Ammo);
}

void Weapon::SetInfiniteAmmo (bool newInfiniteAmmo)
{
	InfiniteAmmo = newInfiniteAmmo;
	if (InfiniteAmmo)
	{
		SetAmmo(MaxAmmo);
	}
}

SD::DString Weapon::GetShortName () const
{
	if (!ShortName.IsEmpty())
	{
		return ShortName;
	}

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	ShortName = translator->TranslateText(ShortNameKey, TRANSLATION_FILE, TXT("Items"));
	return ShortName;
}

const Projectile* Weapon::GetProjectileClass () const
{
	if (ProjectileClass != nullptr)
	{
		return dynamic_cast<const Projectile*>(ProjectileClass->GetDefaultObject());
	}

	return nullptr;
}

void Weapon::Fire ()
{
	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	LastFireTime = localEngine->GetElapsedTime();
	if (!InfiniteAmmo)
	{
		SetAmmo(Ammo - 1);
	}

	FireProjectile();

	if (!ShootSound.IsEmpty())
	{
		DroneDestroyerEngineComponent::SPlayLocalSound(ShootSound, ReadAbsTranslation(), ShootSoundVolume);
	}

	if (Ammo <= 0)
	{
		StopFiring();
		if (Carrier != nullptr)
		{
			Carrier->SwitchToBestWeapon();
		}
	}
}

void Weapon::FireProjectile ()
{
	if (ProjectileClass != nullptr)
	{
		Projectile* proj = dynamic_cast<Projectile*>(ProjectileClass->GetDefaultObject()->CreateObjectOfMatchingClass());
		if (proj != nullptr)
		{
			ModifyProjectile(proj);
		}
	}
}

void Weapon::ModifyProjectile (Projectile* proj) const
{
	CHECK(proj != nullptr)

	SD::Vector3 spawnOffset = ReadAbsRotation().GetDirectionalVector();
	if (Carrier != nullptr)
	{
		spawnOffset *= (Carrier->GetCollisionRadius() + proj->GetCollisionRadius()) * 1.1f;
	}
	spawnOffset += ReadAbsTranslation();
	spawnOffset.Z = DroneDestroyerEngineComponent::DRAW_ORDER_PROJECTILES;
	proj->SetTranslation(spawnOffset);
	
	SD::Rotator projRotation(ReadAbsRotation());
	if (FireSpread > 0)
	{
		//Apply FireSpread by randomizing the direction.
		SD::FLOAT yaw = projRotation.GetYaw(SD::Rotator::ERotationUnit::RU_Degrees);
		yaw += SD::RandomUtils::RandRange(-FireSpread, FireSpread);
		projRotation.SetYaw(yaw, SD::Rotator::ERotationUnit::RU_Degrees);
	}

	proj->SetRotation(projRotation);
	if (Carrier != nullptr)
	{
		proj->SetShooter(Carrier->GetSoul());
		proj->SetWeap(this);
	}

	if (IsExtinctionMode)
	{
		proj->InitializeExtinctionPhysics(!IsDroneWeapon);
	}
	else
	{
		proj->InitializePhysics();
	}
}

void Weapon::HandleWeaponAnimTick (SD::FLOAT deltaSec)
{
	if (PossessedTransformComponent == nullptr)
	{
		//Can't animate without this transform. Instantly snap to next state.
		if (WeaponState == WS_PullingOut)
		{
			EndSwitchTo();
		}
		else if (WeaponState == WS_StowingAway)
		{
			EndSwitchOut();
		}

		return;
	}

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)

	switch (WeaponState)
	{
		case(WS_PullingOut):
		{
			SD::FLOAT percentComplete = (localEngine->GetElapsedTime() - SwitchTime) / SwitchToTime;
			
			if (percentComplete >= 1.f)
			{
				PossessedTransformComponent->EditRotation().Yaw = 0;
				EndSwitchTo();
			}
			else
			{
				unsigned int rotationAmount = SD::Utils::Lerp<unsigned int>((1.f - percentComplete.Value), 0, SD::Rotator::QUARTER_REVOLUTION);
				PossessedTransformComponent->EditRotation().Yaw = rotationAmount;
			}

			break;
		}
		case(WS_StowingAway):
		{
			SD::FLOAT percentComplete = (localEngine->GetElapsedTime() - SwitchTime) / SwitchOutTime;

			if (percentComplete >= 1.f)
			{
				EndSwitchOut();
			}
			else
			{
				unsigned int rotationAmount = SD::Utils::Lerp<unsigned int>(percentComplete.Value, 0, SD::Rotator::QUARTER_REVOLUTION);
				PossessedTransformComponent->EditRotation().Yaw = rotationAmount;
			}

			break;
		}
		case(WS_Drawn):
		{
			if (localEngine->GetElapsedTime() - LastFireTime < FireRate) //If firing
			{
				SD::FLOAT translationOffset = SD::Utils::Lerp(((localEngine->GetElapsedTime() - LastFireTime) / FireRate).Value, RecoilDistance.Value, 0.f);
				PossessedTransformComponent->EditTranslation().X = translationOffset;
			}
			else
			{
				//Not shooting
				PossessedTransformComponent->SetTranslation(SD::Vector3::ZeroVector);
			}
		}
	}
}

void Weapon::HandleWeaponFireTick (SD::FLOAT deltaSec)
{
	if (Ammo > 0)
	{
		Fire();
	}
	else if (Carrier != nullptr)
	{
		Carrier->SwitchToBestWeapon();
	}

	//Reset interval in case it was adjusted to handle tapping fire modes (quickly stop firing in middle of fire anim).
	CHECK(WeaponFireTick != nullptr)
	WeaponFireTick->SetTickInterval(FireRate);
}

void Weapon::HandlePurgeWeapon (SD::FLOAT deltaSec)
{
	Destroy();
}

DD_END