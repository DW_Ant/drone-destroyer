/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Shotgun.cpp
=====================================================================
*/

#include "Shotgun.h"
#include "ShotgunPellet.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Shotgun, DD, Weapon)

void Shotgun::InitProps ()
{
	Super::InitProps();

	NumPelletsPerShot = 12;

#ifdef DEBUG_MODE
	DebugName = TXT("Shotgun");
#endif

	ItemNameKey = TXT("Shotgun");
	ShortNameKey = TXT("ShotgunShort");
	PickupImage = TXT("DroneDestroyer.Pickups.Shotgun");
	PossessedImage = TXT("DroneDestroyer.Weapons.RocketLauncher");
	StartingAmmo = 16;
	Ammo = StartingAmmo;
	MaxAmmo = 64;
	FireRate = 0.8f;
	FireSpread = 20.f;
	ProjectileClass = ShotgunPellet::SStaticClass();
	ShootSound = TXT("W_Shotgun");
	ShootSoundVolume = 0.5f;
	WeaponPriority = 3;
	WeaponSwitchBinds.push_back(sf::Keyboard::Num3);
	WeaponSwitchBinds.push_back(sf::Keyboard::Numpad3);

	SwitchToTime = 0.4f;
	SwitchOutTime = 0.4f;
}

void Shotgun::FireProjectile ()
{
	for (SD::INT i = 0; i < NumPelletsPerShot; ++i)
	{
		Super::FireProjectile();
	}
}
DD_END