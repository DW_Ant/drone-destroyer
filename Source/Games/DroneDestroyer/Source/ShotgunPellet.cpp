/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ShotgunPellet.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "ProjectilePhysicsComponent.h"
#include "ShotgunPellet.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, ShotgunPellet, DD, Projectile)

void ShotgunPellet::InitProps ()
{
	Super::InitProps();

	MaxVelocityMultiplier = 1.5f;
	MaxRandLifeSpan = 1.25f;

	Damage = 5; //Assuming all 12 pellets hit, this is 75 dps
	VelocityMagnitude = 2500.f; //25 meters per second
	//AllowContinuousCollision = false; //Disable continuous collision for performance reasons (since there are multiple pellets that are close together each shot)
	LifeSpan = 2.5f;
	ImpactSoundVolume = 0.01f; //This weapon is spammy. Reduce volume
	DeathMessageKey = TXT("Shotgun");
}

void ShotgunPellet::BeginObject ()
{
	Super::BeginObject();

	SD::LifeSpanComponent* lifeSpan = dynamic_cast<SD::LifeSpanComponent*>(FindSubComponent(SD::LifeSpanComponent::SStaticClass(), false));
	if (lifeSpan != nullptr)
	{
		lifeSpan->LifeSpan += SD::RandomUtils::RandRange(0.f, MaxRandLifeSpan);
	}
}

void ShotgunPellet::InitializePhysics ()
{
	Super::InitializePhysics();

	if (Physics != nullptr)
	{
		SD::Vector3 vel = Physics->GetVelocity();
		vel *= SD::RandomUtils::RandRange(1.f, 1.f + MaxVelocityMultiplier);
		Physics->SetVelocity(vel);
	}
}

void ShotgunPellet::InitializeRenderComponents ()
{
	SD::SpriteComponent* sprite = SD::SpriteComponent::CreateObject();
	if (AddComponent(sprite))
	{
		SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)

		sprite->SetSpriteTexture(localTexturePool->FindTexture(TXT("DroneDestroyer.Projectiles.Bullet")));
		sprite->CenterOrigin = true;
		sprite->EditBaseSize() = SD::Vector2(CollisionRadius * 15.f, CollisionRadius);
		DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(sprite);
	}
}
DD_END