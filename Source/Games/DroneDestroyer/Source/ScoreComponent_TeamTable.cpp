/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScoreComponent_TeamTable.cpp
=====================================================================
*/

#include "PlayerSoul.h"
#include "ScoreComponent.h"
#include "ScoreComponent_Deathmatch.h"
#include "ScoreComponent_TeamTable.h"
#include "TeamDeathmatch.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, ScoreComponent_TeamTable, SD, GuiComponent)

void ScoreComponent_TeamTable::InitProps ()
{
	Super::InitProps();

	Background = nullptr;
	TeamScoreLabel = nullptr;
}

void ScoreComponent_TeamTable::BeginObject ()
{
	Super::BeginObject();

	SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
	CHECK(translator != nullptr)
	TeamScorePrefix = translator->TranslateText(TXT("TeamScore"), TRANSLATION_FILE, TXT("Scoreboard"));
}

void ScoreComponent_TeamTable::InitializeComponents ()
{
	Background = SD::FrameComponent::CreateObject();
	if (AddComponent(Background))
	{
		Background->SetPosition(SD::Vector2::ZeroVector);
		Background->SetSize(SD::Vector2(1.f, 1.f));
		Background->SetLockedFrame(true);
		Background->SetBorderThickness(1.f);

		SD::INT headerCharSize = 14;
		TeamScoreLabel = SD::LabelComponent::CreateObject();
		if (Background->AddComponent(TeamScoreLabel))
		{
			TeamScoreLabel->SetAutoRefresh(false);
			TeamScoreLabel->SetPosition(SD::Vector2::ZeroVector);
			TeamScoreLabel->SetSize(SD::Vector2(1.f, 0.1f));
			TeamScoreLabel->SetWrapText(false);
			TeamScoreLabel->SetClampText(false);
			TeamScoreLabel->SetCharacterSize(headerCharSize);
			TeamScoreLabel->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			TeamScoreLabel->SetAutoRefresh(true);
		}
		else
		{
			TeamScoreLabel = nullptr;
		}

		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

#if 0
		SD::LabelComponent* rankHeader = SD::LabelComponent::CreateObject();
		if (Background->AddComponent(rankHeader))
		{
			rankHeader->SetAutoRefresh(false);
			rankHeader->SetPosition(SD::Vector2(0.f, 0.1f));
			rankHeader->SetSize(SD::Vector2(0.1f, 0.1f));
			rankHeader->SetWrapText(false);
			rankHeader->SetClampText(false);
			rankHeader->SetCharacterSize(headerCharSize);
			rankHeader->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			rankHeader->SetText(translator->TranslateText(TXT("Rank"), TRANSLATION_FILE, TXT("Scoreboard")));
			rankHeader->SetAutoRefresh(true);
		}
#endif

		SD::LabelComponent* playerNameHeader = SD::LabelComponent::CreateObject();
		if (Background->AddComponent(playerNameHeader))
		{
			playerNameHeader->SetAutoRefresh(false);
			playerNameHeader->SetPosition(SD::Vector2(0.1f, 0.1f));
			playerNameHeader->SetSize(SD::Vector2(0.4f, 0.1f));
			playerNameHeader->SetWrapText(false);
			playerNameHeader->SetClampText(false);
			playerNameHeader->SetCharacterSize(headerCharSize);
			playerNameHeader->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			playerNameHeader->SetText(translator->TranslateText(TXT("PlayerName"), TRANSLATION_FILE, TXT("Scoreboard")));
			playerNameHeader->SetAutoRefresh(true);
		}

		SD::LabelComponent* killsHeader = SD::LabelComponent::CreateObject();
		if (Background->AddComponent(killsHeader))
		{
			killsHeader->SetAutoRefresh(false);
			killsHeader->SetPosition(SD::Vector2(0.5f, 0.1f));
			killsHeader->SetSize(SD::Vector2(0.25f, 0.1f));
			killsHeader->SetWrapText(false);
			killsHeader->SetClampText(false);
			killsHeader->SetCharacterSize(headerCharSize);
			killsHeader->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			killsHeader->SetText(translator->TranslateText(TXT("Kills"), TRANSLATION_FILE, TXT("Scoreboard")));
			killsHeader->SetAutoRefresh(true);
		}

		SD::LabelComponent* deathsHeader = SD::LabelComponent::CreateObject();
		if (Background->AddComponent(deathsHeader))
		{
			deathsHeader->SetAutoRefresh(false);
			deathsHeader->SetPosition(SD::Vector2(0.75f, 0.1f));
			deathsHeader->SetSize(SD::Vector2(0.25f, 0.1f));
			deathsHeader->SetWrapText(false);
			deathsHeader->SetClampText(false);
			deathsHeader->SetCharacterSize(headerCharSize);
			deathsHeader->SetHorizontalAlignment(SD::LabelComponent::HA_Center);
			deathsHeader->SetText(translator->TranslateText(TXT("Deaths"), TRANSLATION_FILE, TXT("Scoreboard")));
			deathsHeader->SetAutoRefresh(true);
		}
	}
	else
	{
		Background = nullptr;
	}
}

void ScoreComponent_TeamTable::InitializeComponent (const TeamDeathmatch::STeam& team)
{
	CHECK(Background != nullptr)
	if (Background->RenderComponent != nullptr)
	{
		Background->RenderComponent->ClearSpriteTexture();
		Background->RenderComponent->FillColor = team.TeamColor;
		Background->RenderComponent->FillColor.Source.a = 128;
	}

	SD::FLOAT drawPosY = 0.2f;
	SD::FLOAT entryHeight = 0.075f;
	SD::FLOAT spacing = 0.025f;
	size_t maxEntries = SD::FLOAT::RoundDown((1.f - drawPosY) / (entryHeight + spacing)).ToINT().ToUnsignedInt();

	bool halfSize = (team.Members.size() > maxEntries);
	if (halfSize)
	{
		entryHeight *= 0.5f;
		spacing *= 0.5f;
		maxEntries *= 2;
	}

	for (size_t i = 0; i < team.Members.size() && i < maxEntries; ++i)
	{
		ScoreComponent* newMember = GetScoreComponentClass()->CreateObjectOfMatchingClass();
		if (Background->AddComponent(newMember))
		{
			newMember->SetPosition(SD::Vector2(0.f, drawPosY));
			newMember->SetSize(SD::Vector2(1.f, entryHeight));
			drawPosY += spacing + entryHeight;
			newMember->SetFontSize(12);
			Members.push_back(newMember);
		}
	}
}

void ScoreComponent_TeamTable::UpdateTable (const TeamDeathmatch::STeam& team)
{
	if (TeamScoreLabel != nullptr)
	{
		TeamScoreLabel->SetText(TeamScorePrefix + team.TeamScore.ToString());
	}

	bool playerFound = false;
	for (size_t i = 0; i < team.Members.size(); ++i)
	{
		if (i >= Members.size())
		{
			break;
		}

		Members.at(i)->UpdateInformation(SD::INT(i+1), team.Members.at(i).Get());
	}
	
	if (!playerFound)
	{
		for (size_t i = Members.size(); i < team.Members.size(); ++i)
		{
			if (team.Members.at(i)->GetIsBot())
			{
				continue;
			}

			//Found the player. Replace the last entry with this one.
			SD::ContainerUtils::GetLast(Members)->UpdateInformation(SD::INT(i+1), team.Members.at(i).Get());
			break;
		}
	}
}

const ScoreComponent* ScoreComponent_TeamTable::GetScoreComponentClass () const
{
	return dynamic_cast<const ScoreComponent_Deathmatch*>(ScoreComponent_Deathmatch::SStaticClass()->GetDefaultObject());
}

DD_END