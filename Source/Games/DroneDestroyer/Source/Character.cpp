/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Character.cpp
=====================================================================
*/

#include "BotCharacterComponent.h"
#include "Character.h"
#include "DroneDestroyerEngineComponent.h"
#include "HealthComponent.h"
#include "Hud.h"
#include "HudMessage.h"
#include "MovementComponent.h"
#include "Msg_Killed.h"
#include "Msg_KilledBy.h"
#include "Msg_OutOfAmmo.h"
#include "Msg_WeaponSwitch.h"
#include "PlayerSoul.h"
#include "Projectile.h"
#include "Weapon.h"
#include "UserSettings.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Character, SD, Entity)

const SD::INT Character::INPUT_PRIORITY(750); //Higher than soul, lower than Gui

void Character::InitProps ()
{
	Super::InitProps();

	Radius = 100.f;
	ThrowWeaponSpeed = 200.f;

	DrawnWeapon = nullptr;
	IsShooting = false;

	Soul = nullptr;
	RenderComponent = nullptr;
	Physics = nullptr;
	BotComponentCdo = dynamic_cast<const BotCharacterComponent*>(BotCharacterComponent::SStaticClass()->GetDefaultObject());
	Health = nullptr;
	DeathSounds.push_back(TXT("PlayerDeathShort"));
	DeathSoundVolume = 1.f;
	AttachedCameraTransform = nullptr;

	PainSoundInterval = 0.1f;
	LastPainSound = -1.f;

	InvokedKill = false;
	CachedCollisionRadius = -1.f;
	DesiredWeapon = nullptr;
}

void Character::BeginObject ()
{
	Super::BeginObject();

	RenderComponent = SD::SpriteComponent::CreateObject();
	if (AddComponent(RenderComponent))
	{
		SD::TexturePool* texturePool = SD::TexturePool::FindTexturePool();
		CHECK(texturePool != nullptr)

		SD::Texture* characterTexture = texturePool->FindTexture(TXT("DroneDestroyer.Player.BrightPlayer"));
		RenderComponent->SetSpriteTexture(characterTexture);
		RenderComponent->EditBaseSize() = SD::Vector2(Radius, Radius);
		RenderComponent->CenterOrigin = true;

		DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(RenderComponent);
	}

	Health = HealthComponent::CreateObject();
	if (AddComponent(Health))
	{
		Health->OnTakeDamage.RegisterHandler(SDFUNCTION_3PARAM(this, Character, HandleTakeDamage, void, SD::INT, PlayerSoul*, Projectile*));
	}

	AttachedCameraTransform = SD::SceneTransformComponent::CreateObject();
	if (AddComponent(AttachedCameraTransform))
	{
		//Noop
	}
}

void Character::Destroyed ()
{
	//Any cameras relative to this character should be detached before that component is destroyed.
	if (AttachedCameraTransform != nullptr)
	{
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr)

		if (SD::SceneCamera* cam = localDroneEngine->GetMainCamera())
		{
			if (cam->GetRelativeTo() == AttachedCameraTransform)
			{
				//Detach camera but have it remember what it's looking at
				cam->SetRelativeTo(nullptr);
				cam->SetTranslation(ReadAbsTranslation());
				cam->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_CAMERA;
			}
		}
	}

	//Destroy all inventory associated with this character. The Inventory would try to repair themselves every time one gets destroyed.
	//For a performance gain, sever the head. The sub inventory Entities are going to be destroyed anyways.
	Inventory* inv = FirstInventory.Get();
	FirstInventory = nullptr;

	while (inv != nullptr)
	{
		Inventory* nextInv = inv->GetNextInventory();
		inv->Destroy();
		inv = nextInv;
	}

	Super::Destroyed();
}

void Character::InitializePhysics ()
{
	SD::PhysicsComponent::SComponentInitData initData;
	initData.BodyInit.PhysicsType = SD::PhysicsComponent::PT_Dynamic;
	initData.BodyInit.EnableContinuousCollision = false;
	initData.BodyInit.IsFixedRotation = true;

	b2CircleShape circle;
	circle.m_radius = SD::Box2dUtils::ToBox2dDist(Radius * 0.5f);
	initData.CollisionShape = &circle;
	initData.Friction = 0.75f;
	initData.Restitution = 0.4f;
	initData.Density = 1.f;
	initData.IsBlockingComponent = true;
	initData.CollisionGroup = 1;
	Physics = SD::PhysicsComponent::CreateAndInitializeComponent<SD::PhysicsComponent>(this, initData);
	CHECK(Physics != nullptr)

	Movement = MovementComponent::CreateObject();
	if (Physics->AddComponent(Movement.Get()))
	{
		//Noop
	}

#ifdef DEBUG_MODE
	bool debugCharacterPhysics = false;
	if (debugCharacterPhysics)
	{
		SD::SceneTransformComponent* transform = SD::SceneTransformComponent::CreateObject();
		if (AddComponent(transform))
		{
			transform->EditTranslation().Z = DroneDestroyerEngineComponent::DRAW_ORDER_DEBUG_SHAPES;

			SD::PhysicsRenderer* renderComp = SD::PhysicsRenderer::CreateObject();
			if (AddComponent(renderComp))
			{
				renderComp->FillColor = SD::Color(128, 128, 0, DroneDestroyerEngineComponent::DebugPhysicsRenderAlpha);
				DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer(renderComp);
			}
		}
	}
#endif
}

void Character::InitializeInputComponent ()
{
	SD::InputComponent* input = SD::InputComponent::CreateObject();
	if (AddComponent(input))
	{
		input->SetInputPriority(INPUT_PRIORITY);
		input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, Character, HandleInput, bool, const sf::Event&);
		input->MouseMoveDelegate = SDFUNCTION_3PARAM(this, Character, HandleMouseMove, void, SD::MousePointer*, const sf::Event::MouseMoveEvent&, const SD::Vector2&);
		input->MouseClickDelegate = SDFUNCTION_3PARAM(this, Character, HandleMouseClick, bool, SD::MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
		input->MouseWheelScrollDelegate = SDFUNCTION_2PARAM(this, Character, HandleMouseWheel, bool, SD::MousePointer*, const sf::Event::MouseWheelScrollEvent&);
	}
}

void Character::InitializeBotComponent ()
{
	if (BotComponentCdo != nullptr)
	{
		BotCharacterComponent* botComp = BotComponentCdo->CreateObjectOfMatchingClass();
		AddComponent(botComp);
	}
}

bool Character::IsFriendlyTo (Character* other) const
{
	if (other == this)
	{
		//Don't hate yourself
		return true;
	}

	//Hate everyone not on your team
	if (Soul != nullptr && Soul->GetTeamNumber() >= 0)
	{
		//Running a team game. Check other's team
		return (other->Soul != nullptr && other->Soul->GetTeamNumber() == Soul->GetTeamNumber());
	}

	return false;
}

bool Character::IsAlive () const
{
	return (Health != nullptr && Health->GetHealth() > 0);
}

bool Character::IsHuman () const
{
	return (Soul != nullptr && !Soul->GetIsBot());
}

void Character::SwitchToBestWeapon ()
{
	SD::INT bestWeaponNumber = 0;

	SelectWeaponImplementation([&](Weapon* weap)
	{
		if (weap->GetWeaponPriority() > bestWeaponNumber)
		{
			bestWeaponNumber = weap->GetWeaponPriority();
			return true;
		}

		return false;
	});
}

void Character::SelectNextWeapon ()
{
	if (DrawnWeapon == nullptr)
	{
		SwitchToBestWeapon();
		return;
	}

	SD::INT bestDelta = INT_MAX;
	Weapon* curWeapon = (DesiredWeapon != nullptr) ? DesiredWeapon : DrawnWeapon; //Only consider the weapon I'm switching to. If not, then consider my drawn weapon

	SelectWeaponImplementation([&](Weapon* weap)
	{
		if (curWeapon == weap)
		{
			return false;
		}

		SD::INT delta = weap->GetWeaponPriority() - DrawnWeapon->GetWeaponPriority();
		if (delta < 0)
		{
			//Loop around the weapon list
			delta = Weapon::MAX_WEAPON_PRIORITY + weap->GetWeaponPriority();
		}

		if (delta < bestDelta)
		{
			bestDelta = delta;
			return true;
		}

		return false;
	});
}

void Character::SelectPrevWeapon ()
{
	if (DrawnWeapon == nullptr)
	{
		SwitchToBestWeapon();
		return;
	}

	SD::INT bestDelta = INT_MAX;
	Weapon* curWeapon = (DesiredWeapon != nullptr) ? DesiredWeapon : DrawnWeapon; //Only consider the weapon I'm switching to. If not, then consider my drawn weapon

	SelectWeaponImplementation([&](Weapon* weap)
	{
		if (curWeapon == weap)
		{
			return false;
		}

		SD::INT delta = DrawnWeapon->GetWeaponPriority() - weap->GetWeaponPriority();
		if (delta < 0)
		{
			//Loop around the weapon list
			delta = Weapon::MAX_WEAPON_PRIORITY - weap->GetWeaponPriority();
		}

		if (delta < bestDelta)
		{
			bestDelta = delta;
			return true;
		}

		return false;
	});
}

void Character::ThrowWeapon ()
{
	if (DrawnWeapon != nullptr && DrawnWeapon->CanBeDropped(this))
	{
		SD::Vector3 throwVel = ReadRotation().GetDirectionalVector();
		throwVel *= ThrowWeaponSpeed;
		DrawnWeapon->StartPurgeTimer();
		DrawnWeapon->DropItem(this, throwVel);
		SetDrawnWeapon(nullptr);
		SwitchToBestWeapon();
	}
}

void Character::DeleteInventory (Inventory* oldInventory)
{
	if (oldInventory != nullptr && DrawnWeapon == oldInventory)
	{
		/*
		Setting DrawnWeapon to null fixes the following:
		* Removes reference to old weapon immediately (which could be destroyed if someone else takes it)
		* This allows this character to immediately switch in to the next weapon (no latent actions from old weapon which could be in someone elses possession by the time it finishes).
		* Nulls it out in case SwitchToBestWeapon doesn't choose a new DrawnWeapon (eg: this is the Character's last weapon).
		*/
		SetDrawnWeapon(nullptr);
		SwitchToBestWeapon();
	}
}

bool Character::DrawWeaponTo (Weapon* weaponToDrawTo)
{
	if (weaponToDrawTo == nullptr || !weaponToDrawTo->CanSwitchTo() || !IsAlive())
	{
		return false;
	}

	if (IsHuman())
	{
		HudMessage::CreateAndRegisterMsgToHud(Msg_WeaponSwitch::SStaticClass(), weaponToDrawTo->GetItemName());
	}

	if (weaponToDrawTo == DrawnWeapon && DrawnWeapon->GetWeaponState() == Weapon::WS_Drawn)
	{
		//Already have this weapon drawn. Do nothing since the task is already done.
		return true;
	}
	else if (weaponToDrawTo == DesiredWeapon)
	{
		//Already switching towards this weapon (eg: Player is spamming the weapon key).
		return true;
	}

	if (DrawnWeapon != nullptr)
	{
		//Must pull away old weapon first
		DesiredWeapon = weaponToDrawTo;

		if (DrawnWeapon->GetWeaponState() == Weapon::WS_Drawn) //If it's not in the middle of pull out or stowing away
		{
			DrawnWeapon->OnWeaponSwitchOut = SDFUNCTION_1PARAM(this, Character, HandleWeaponSwitchOut, void, Weapon*);
			DrawnWeapon->StartSwitchOut();
		}
	}
	else
	{
		//Switch to weapon immediately if we don't have a weapon
		weaponToDrawTo->OnWeaponSwitchIn = SDFUNCTION_1PARAM(this, Character, HandleWeaponSwitchIn, void, Weapon*);
		SetDrawnWeapon(weaponToDrawTo);
		DrawnWeapon->StartSwitchTo();
	}

	return true;
}

SD::FLOAT Character::GetCollisionRadius () const
{
	if (CachedCollisionRadius > 0.f)
	{
		return CachedCollisionRadius;
	}

	if (Physics != nullptr)
	{
		b2Body* body = Physics->GetPhysicsBody();
		if (body != nullptr)
		{
			b2Fixture* firstFixture = body->GetFixtureList();
			if (firstFixture != nullptr)
			{
				b2Shape* shape = firstFixture->GetShape();
				if (shape != nullptr)
				{
					CachedCollisionRadius = SD::Box2dUtils::ToSdDist(shape->m_radius);
					return CachedCollisionRadius;
				}
			}
		}
	}

	return 0.f;
}

void Character::Kill (SD::INT damageAmount, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg)
{
	InvokedKill = true;
	ThrowWeapon();

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr && localDroneEngine->GetEnvironment() != nullptr && localDroneEngine->GetGame() != nullptr)

	if (!SD::ContainerUtils::IsEmpty(DeathSounds))
	{
		SD::DString& deathSound = DeathSounds.at(SD::RandomUtils::Rand(SD::INT(DeathSounds.size())).ToUnsignedInt());
		localDroneEngine->PlayLocalSound(deathSound, ReadAbsTranslation(), DeathSoundVolume);
	}

	//Notify the game to give points
	localDroneEngine->GetGame()->ScoreKill(this, killer, killedBy, deathMsg);
	LeaveCorpse(localDroneEngine->GetEnvironment());

	if (Soul != nullptr)
	{
		Soul->DetachFromBody();
	}
	Soul = nullptr;

	//Destroys inventory, input, and bot components
	Destroy();
}

void Character::LeaveCorpse (Arena* environment)
{
	environment->AddCorpse(GetCollisionRadius(), ReadAbsTranslation());
}

void Character::SetSoul (PlayerSoul* newSoul)
{
	Soul = newSoul;

	if (!newSoul->GetIsBot() && Health != nullptr)
	{
		//The player is controlling this body, bind delegates to update the hud.
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr && localDroneEngine->GetPlayerHud() != nullptr)

		Health->OnHealthChanged.RegisterHandler(SDFUNCTION_1PARAM(localDroneEngine->GetPlayerHud(), Hud, HandleNewHealth, void, SD::INT)); 
		localDroneEngine->GetPlayerHud()->SetHealthAmount(Health->GetHealth());

		if (DrawnWeapon != nullptr)
		{
			//HACK! 
			//It's possible to obtain weapons before a player possesses this (eg: default inventory).
			//In order to refresh the hud, lets null out this weapon (to prevent that log error about unable to unregister old callback), then reassign it to itself to register hud callbacks.
			Weapon* drawWeaponHack = DrawnWeapon;
			DrawnWeapon = nullptr;
			SetDrawnWeapon(drawWeaponHack);
		}
	}

#ifdef DEBUG_MODE
	if (newSoul != nullptr)
	{
		DebugName = newSoul->GetPlayerName() + TXT("_Body");
	}
#endif
}

void Character::SetDrawnWeapon (Weapon* newWeapon)
{
	if (IsHuman())
	{
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr)
		Hud* playerHud = localDroneEngine->GetPlayerHud();
		CHECK(playerHud != nullptr)

		if (DrawnWeapon != nullptr)
		{
			DrawnWeapon->OnAmmoChanged.UnregisterHandler(SDFUNCTION_1PARAM(playerHud, Hud, HandleNewAmmo, void, SD::INT));
		}
		
		if (newWeapon != nullptr)
		{
			newWeapon->OnAmmoChanged.RegisterHandler(SDFUNCTION_1PARAM(playerHud, Hud, HandleNewAmmo, void, SD::INT));
			playerHud->SetAmmoAmount(newWeapon->GetAmmo());
		}
		else
		{
			playerHud->SetAmmoAmount(0);
		}
	}

	DrawnWeapon = newWeapon;
}

void Character::SelectWeaponImplementation (const std::function<bool(Weapon*)>& isBetterWeaponCandidate)
{
	Weapon* bestWeapon = nullptr;
	for (Inventory* inv = FirstInventory.Get(); inv != nullptr; inv = inv->GetNextInventory())
	{
		if (Weapon* weap = dynamic_cast<Weapon*>(inv))
		{
			if (!weap->CanSwitchTo())
			{
				continue;
			}

			if (isBetterWeaponCandidate(weap))
			{
				bestWeapon = weap;
			}
		}
	}

	if (bestWeapon != nullptr)
	{
		DrawWeaponTo(bestWeapon);
	}
}

bool Character::HandleInput (const sf::Event& inputEvent)
{
	bool consumeEvent = false;

	if (inputEvent.type == sf::Event::KeyPressed || inputEvent.type == sf::Event::KeyReleased)
	{
		switch(inputEvent.key.code)
		{
			case(sf::Keyboard::BackSlash):
			case(sf::Keyboard::Slash):
				consumeEvent = true;
				if (inputEvent.type == sf::Event::KeyPressed)
				{
					ThrowWeapon();
				}
				break;

			case(sf::Keyboard::Space):
			case(sf::Keyboard::Num0):
			case(sf::Keyboard::Numpad0):
				consumeEvent = true;
				if (inputEvent.type == sf::Event::KeyPressed)
				{
					SwitchToBestWeapon();
				}
				break;
		}

		//Iterate through the weapon list to see if the player selected a particular weapon
		for (Inventory* inv = FirstInventory.Get(); inv != nullptr && !consumeEvent; inv = inv->GetNextInventory())
		{
			Weapon* weap = dynamic_cast<Weapon*>(inv);
			if (weap == nullptr)
			{
				//Not a weapon ignore
				continue;
			}

			for (sf::Keyboard::Key keyCode : weap->ReadWeaponSwitchBinds())
			{
				if (keyCode == inputEvent.key.code)
				{
					consumeEvent = true; //Aborts inv loop and consumes input event.
					if (inputEvent.type == sf::Event::KeyPressed)
					{
						if (weap->GetAmmo() <= 0 && IsHuman())
						{
							DroneDestroyerEngineComponent::SPlayLocalSound(TXT("W_NoAmmo"), ReadAbsTranslation(), 1.f);
							HudMessage::CreateAndRegisterMsgToHud(Msg_OutOfAmmo::SStaticClass(), weap->GetItemName());
						}

						if (weap->CanSwitchTo())
						{
							DrawWeaponTo(weap);
							break;
						}
					}
				}
			}
		}
	}

	return consumeEvent;
}

void Character::HandleMouseMove (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const SD::Vector2& deltaMove)
{
	if (AttachedCameraTransform != nullptr)
	{
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr && localDroneEngine->GetMainCamera() != nullptr && localDroneEngine->GetSettings() != nullptr)
		if (localDroneEngine->IsPausingGame() || !localDroneEngine->GetSettings()->GetDynamicCam())
		{
			AttachedCameraTransform->EditTranslation().X = 0.f; //Snap to look at the player.
			return;
		}

		if (MainViewport == nullptr)
		{
			SD::GraphicsEngineComponent* localGraphics = SD::GraphicsEngineComponent::Find();
			CHECK(localGraphics != nullptr && localGraphics->GetPrimaryWindow() != nullptr)
			MainViewport = localGraphics->GetPrimaryWindow();
		}

#if 0
		//The problem with this method is that the AttachedCameraTransform is relative to the character which rotates towards the mouse cursor (which in turn modifies the result transform).
		SD::Rotator rotation;
		SD::Vector3 location;
		AttachedCamera->CalculatePixelProjection(MainViewport->GetSize(), mouse->ReadPosition(), OUT rotation, OUT location);
		DroneDestroyerLog.Log(SD::LogCategory::LL_Debug, TXT("Cam projection %s"), location.ToVector2());
		location -= ReadAbsTranslation();

		AttachedCameraTransform->SetTranslation(location * 0.5f);
		AttachedCameraTransform->EditTranslation().Z = 0.f;
#else
		//Better solution. Since the character is always rotated towards the mouse pointer, simply edit the translation.X (forward) based on mouse cursor to the center.
		SD::Vector2 viewportSize = MainViewport->GetSize();
		SD::FLOAT distToCenter = (mouse->ReadPosition() - (viewportSize * 0.5f)).VSize();
		distToCenter /= localDroneEngine->GetMainCamera()->ReadProjectionData().Scale.x; //Apply camera zoom and dpi settings
		AttachedCameraTransform->EditTranslation().X = distToCenter;
#endif
	}
}

bool Character::HandleMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& mouseEvent, sf::Event::EventType eventType)
{
	bool consumeEvent = false;

	if (mouseEvent.button == sf::Mouse::Left)
	{
		consumeEvent = true;
		IsShooting = (eventType == sf::Event::MouseButtonPressed);

		if (DrawnWeapon != nullptr)
		{
			if (eventType == sf::Event::MouseButtonPressed && DrawnWeapon->CanShoot())
			{
				DrawnWeapon->StartFiring();
			}
			else if (eventType == sf::Event::MouseButtonReleased)
			{
				DrawnWeapon->StopFiring();
			}
		}
	}

	return consumeEvent;
}

bool Character::HandleMouseWheel (SD::MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& wheelEvent)
{
	bool consumeEvent = false;

	if (wheelEvent.wheel == sf::Mouse::Wheel::VerticalWheel && wheelEvent.delta != 0.f)
	{
		consumeEvent = true;
		if (wheelEvent.delta > 0.f)
		{
			//Scrolling up
			SelectNextWeapon();
		}
		else
		{
			//Scrolling down
			SelectPrevWeapon();
		}
	}

	return consumeEvent;
}

void Character::HandleWeaponSwitchOut (Weapon* switchedFrom)
{
	if (switchedFrom != nullptr)
	{
		switchedFrom->OnWeaponSwitchOut.ClearFunction();
	}

	if (DesiredWeapon != nullptr && IsAlive())
	{
		SetDrawnWeapon(DesiredWeapon);
		DrawnWeapon->OnWeaponSwitchIn = SDFUNCTION_1PARAM(this, Character, HandleWeaponSwitchIn, void, Weapon*);
		DrawnWeapon->StartSwitchTo();
		DesiredWeapon = nullptr;
	}
}

void Character::HandleWeaponSwitchIn (Weapon* switchedTo)
{
	if (switchedTo == nullptr)
	{
		return;
	}
	switchedTo->OnWeaponSwitchIn.ClearFunction();

	if (!IsAlive())
	{
		return;
	}

	if (DesiredWeapon == DrawnWeapon)
	{
		//The player must have switched to one weapon then back to the original weapon. Treat this as a cancel action and don't switch out.
		DesiredWeapon = nullptr;
	}

	if (DesiredWeapon != nullptr)
	{
		//Switched to a new weapon while the current DrawnWeapon is being pulled out. Automatically draw to that new weapon.
		DrawnWeapon->OnWeaponSwitchOut = SDFUNCTION_1PARAM(this, Character, HandleWeaponSwitchOut, void, Weapon*);
		DrawnWeapon->StartSwitchOut();
		return;
	}

	if (IsShooting)
	{
		switchedTo->StartFiring();
	}
}

void Character::HandleTakeDamage (SD::INT damageAmount, PlayerSoul* damagedBy, Projectile* damagedWith)
{
	SD::Engine* engine = SD::Engine::FindEngine();
	CHECK(engine != nullptr)
	if (engine->GetElapsedTime() - LastPainSound >= PainSoundInterval)
	{
		LastPainSound = engine->GetElapsedTime();
		if (IsHuman())
		{
			DroneDestroyerEngineComponent::SPlayLocalSound(TXT("Player_Damage"), ReadAbsTranslation(), 1.f);
		}
		else
		{
			DroneDestroyerEngineComponent::SPlayLocalSound(TXT("Enemy_Damage"), ReadAbsTranslation(), 0.75f);
		}
	}

	if (!InvokedKill && Health->GetHealth() <= 0)
	{
		SD::DString deathMsg = (damagedWith != nullptr) ? damagedWith->ReadDeathMessageKey() : TXT("Unknown");
		Kill(damageAmount, damagedBy, damagedWith, deathMsg);
	}
}

DD_END