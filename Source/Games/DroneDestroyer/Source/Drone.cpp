/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Drone.cpp
=====================================================================
*/

#include "BotCharacterComponent.h"
#include "BotDroneComponent.h"
#include "Drone.h"
#include "DroneDestroyerEngineComponent.h"
#include "DroneWeapon.h"
#include "Extinction.h"
#include "HealthComponent.h"
#include "MovementComponent.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, Drone, DD, Character)

void Drone::InitProps ()
{
	Super::InitProps();

	Radius = 100.f;
	BotComponentCdo = dynamic_cast<const BotDroneComponent*>(BotDroneComponent::SStaticClass()->GetDefaultObject());

	SD::ContainerUtils::Empty(OUT DeathSounds);
	DeathSounds.push_back(TXT("Enemy_Death1"));
	DeathSounds.push_back(TXT("Enemy_Death2"));
	DeathSounds.push_back(TXT("Enemy_Death3"));
	DeathSounds.push_back(TXT("Enemy_Death4"));
	DeathSounds.push_back(TXT("Enemy_Death5"));
	DeathSoundVolume = 0.67f;

	SpriteIdx = 0;
}

void Drone::BeginObject ()
{
	Super::BeginObject();

	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	//Replace inefficient RenderComponent with InstancedSpriteComponent in the game
	if (RenderComponent != nullptr)
	{
		RenderComponent->Destroy();
		RenderComponent = nullptr;
	}
}

void Drone::InitializePhysics ()
{
	//Don't call super. This drone is not suppose to use Box2D physics

	SD::TickComponent* droneTick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
	if (AddComponent(droneTick))
	{
		droneTick->SetTickHandler(SDFUNCTION_1PARAM(this, Drone, HandleTick, void, SD::FLOAT));

		Movement = MovementComponent::CreateObject();
		if (AddComponent(Movement.Get()))
		{
			Movement->SetAcceleration(0.25f); //Slow down acceleration to make this drone "slide" as if they're flying.
		}
	}
}

bool Drone::IsFriendlyTo (Character* other) const
{
	//Hate everyone that's not a drone
	return (dynamic_cast<Drone*>(other) != nullptr);
}

SD::FLOAT Drone::GetCollisionRadius () const
{
	return Radius;
}

void Drone::Kill (SD::INT damageAmount, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg)
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)

	if (Extinction* game = dynamic_cast<Extinction*>(localDroneEngine->GetGame()))
	{
		game->RemoveDrone(this);
	}

	Super::Kill(damageAmount, killer, killedBy, deathMsg);
}

void Drone::LeaveCorpse (Arena* environment)
{
	environment->AddDroneCorpse(GetCollisionRadius() * 0.5f, ReadAbsTranslation());
}

void Drone::AdjustDroneStrength (GameRules::EGameDifficulty skillLevel, SD::INT droneHealth, SD::FLOAT maxVelocity, const SD::DClass* weaponClass)
{
	if (Health != nullptr)
	{
		Health->SetMaxHealth(droneHealth);
		Health->SetHealth(droneHealth);
	}

	if (Movement != nullptr)
	{
		Movement->SetVelocityMagnitude(maxVelocity);
	}

	if (weaponClass != nullptr)
	{
		DroneWeapon* newWeapon = dynamic_cast<DroneWeapon*>(weaponClass->GetDefaultObject()->CreateObjectOfMatchingClass());
		CHECK(newWeapon != nullptr)
		newWeapon->GiveTo(this);
	}

	if (BotCharacterComponent* botComp = dynamic_cast<BotCharacterComponent*>(FindSubComponent(BotCharacterComponent::SStaticClass(), false)))
	{
		botComp->SetSkillLevel(skillLevel);
	}
}

void Drone::SetSpriteIdx (size_t newSpriteIdx)
{
	SpriteIdx = newSpriteIdx;
}

void Drone::SetDroneSprite (SD::InstancedSpriteComponent* newDroneSprite)
{
	DroneSprite = newDroneSprite;
}

void Drone::UpdateVertices ()
{
	sf::VertexArray& vertArray = DroneSprite->EditVertices();
	CHECK(DroneSprite != nullptr && (SpriteIdx+5) < vertArray.getVertexCount())

	size_t baseIdx = SpriteIdx*6;
	vertArray[baseIdx+0].position = sf::Vector2f(Radius.Value * 0.5f, Radius.Value * -0.5f);
	vertArray[baseIdx+1].position = sf::Vector2f(Radius.Value * -0.5f, Radius.Value * 0.5f);
	vertArray[baseIdx+2].position = sf::Vector2f(Radius.Value * -0.5f, Radius.Value * -0.5f);
	vertArray[baseIdx+3].position = sf::Vector2f(Radius.Value * 0.5f, Radius.Value * -0.5f);
	vertArray[baseIdx+4].position = sf::Vector2f(Radius.Value * 0.5f, Radius.Value * 0.5f);
	vertArray[baseIdx+5].position = sf::Vector2f(Radius.Value * -0.5f, Radius.Value * 0.5f);

	const float radians = -ReadAbsRotation().GetYaw(SD::Rotator::RU_Radians).Value;

	//Rotate each vertex about the center. Then displace it by the sprite's position offset.
	for (size_t i = 0; i < 6; ++i)
	{
		size_t curIdx = i+baseIdx;
		float newXPos = (vertArray[curIdx].position.x * std::cos(radians)) - (vertArray[curIdx].position.y * std::sin(radians));
		vertArray[curIdx].position.y = (vertArray[curIdx].position.x * std::sin(radians)) + (vertArray[curIdx].position.y * std::cos(radians));
		vertArray[curIdx].position.x = newXPos;
		vertArray[curIdx].position += SD::Vector2::SDtoSFML(ReadAbsTranslation().ToVector2());
	}
}

void Drone::HandleTick (SD::FLOAT deltaSec)
{
	if (DroneSprite != nullptr)
	{
		//Bug in the engine where Entities without RenderComponents will not have their absolute transforms computed. Do it here.
		CalculateAbsoluteTransform();
		UpdateVertices();
	}
}
DD_END