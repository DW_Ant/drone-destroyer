/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanelComponent.cpp
=====================================================================
*/

#include "MainMenu.h"
#include "MenuPanelComponent.h"

DD_BEGIN
IMPLEMENT_ABSTRACT_CLASS(DD, MenuPanelComponent, SD, GuiComponent)

void MenuPanelComponent::InitProps ()
{
	Super::InitProps();

	ConfigFile = (SD::Directory::CONFIG_DIRECTORY / TXT("Generated")).ReadDirectoryPath() + TXT("DroneDestroyerMenus.ini");
	ConfigKey = SD::DString::EmptyString;
}

MainMenu* MenuPanelComponent::GetOwningMenu () const
{
	return dynamic_cast<MainMenu*>(GetRootEntity());
}

void MenuPanelComponent::RemoveThisPanel ()
{
	MainMenu* owningMenu = GetOwningMenu();
	if (owningMenu == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to remove MenuPanel with config key %s since it's not a component of a MainMenu."), ConfigKey);
		return;
	}

	if (owningMenu->GetCurrentPanel() != this)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Unable to remove MenuPanel with config key %s since the main menu is currently not viewing it. It's viewing %s instead."), ConfigKey, owningMenu->GetCurrentPanel()->ConfigKey);
		return;
	}

	owningMenu->NavigateBack();
}

DD_END