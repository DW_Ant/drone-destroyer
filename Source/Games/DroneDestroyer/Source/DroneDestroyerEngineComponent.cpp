/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DroneDestroyerEngineComponent.cpp
=====================================================================
*/

#include "DroneDestroyerEngineComponent.h"
#include "DroneDestroyerTheme.h"
#include "GameRules.h"
#include "Hud.h"
#include "Inventory.h"
#include "MainMenu.h"
#include "PlayerSoul.h"
#include "SoundEngineComponent.h"
#include "SoundMessenger.h"
#include "UserSettings.h"

DD_BEGIN
SD_THREAD_FUNCTION(InitializeSoundThread);

IMPLEMENT_ENGINE_COMPONENT(DD, DroneDestroyerEngineComponent)

#ifdef DEBUG_MODE
const int DroneDestroyerEngineComponent::DebugPhysicsRenderAlpha(96);
#endif

const SD::FLOAT DroneDestroyerEngineComponent::DRAW_ORDER_MAP(0.f);
const SD::FLOAT DroneDestroyerEngineComponent::DRAW_ORDER_PICKUPS(1000.f);
const SD::FLOAT DroneDestroyerEngineComponent::DRAW_ORDER_CHARACTERS(1250.f);
const SD::FLOAT DroneDestroyerEngineComponent::DRAW_ORDER_DRONES(1500.f);
const SD::FLOAT DroneDestroyerEngineComponent::DRAW_ORDER_PROJECTILES(2000.f);
const SD::FLOAT DroneDestroyerEngineComponent::DRAW_ORDER_DEBUG_SHAPES(2500.f);
const SD::FLOAT DroneDestroyerEngineComponent::DRAW_ORDER_CAMERA(50000.f);

DroneDestroyerEngineComponent::DroneDestroyerEngineComponent () : EngineComponent(),
	DefaultCamZoom(-1.f),
	PausingGame(false),
	InnerHearingRadius(500.f),
	OuterHearingRadius(2500.f)
{
	//Noop
}

void DroneDestroyerEngineComponent::PreInitializeComponent ()
{
	Super::PreInitializeComponent();

	ImportGameAssets();

	SD::GuiEngineComponent* guiEngine = SD::GuiEngineComponent::Find();
	CHECK(guiEngine != nullptr)
	guiEngine->SetDefaultThemeClass(DroneDestroyerTheme::SStaticClass());
}

void DroneDestroyerEngineComponent::InitializeComponent ()
{
	Super::InitializeComponent();

	SD::Engine* localEngine = SD::Engine::FindEngine();
	CHECK(localEngine != nullptr)
	localEngine->CreateTickGroup(TICK_GROUP_DD, TICK_GROUP_PRIORITY_DD);
	localEngine->CreateTickGroup(TICK_GROUP_DD_NO_PAUSE, TICK_GROUP_PRIORITY_DD_NO_PAUSE);

	SD::GraphicsEngineComponent* graphicsEngine = SD::GraphicsEngineComponent::Find();
	CHECK(graphicsEngine != nullptr)

	MainCamera = SD::TopDownCamera::CreateObject();
	MainCamera->EditTranslation().Z = DRAW_ORDER_CAMERA;
	DefaultCamZoom = MainCamera->Zoom;

	MainDrawLayer = SD::SceneDrawLayer::CreateObject();
	MainDrawLayer->SetDrawPriority(SD::GuiDrawLayer::MAIN_OVERLAY_PRIORITY / 2); //Draw behind the UI
	graphicsEngine->GetPrimaryWindow()->RegisterDrawLayer(MainDrawLayer.Get(), MainCamera.Get());

	SD::GuiEngineComponent* guiEngine = SD::GuiEngineComponent::Find();
	CHECK(guiEngine != nullptr)

	SD::GuiTheme* theme = guiEngine->GetGuiTheme();
	CHECK(theme != nullptr)
	theme->SetDefaultStyle(DroneDestroyerTheme::DRONE_DESTROYER_STYLE_NAME);

	Settings = UserSettings::CreateObject();
	CHECK(Settings != nullptr)
	Settings->LoadUserSettings();
}

void DroneDestroyerEngineComponent::PostInitializeComponent ()
{
	Super::PostInitializeComponent();

	if (LaunchSoundThread())
	{
		SoundSender = SoundMessenger::CreateObject();
		CHECK(SoundSender != nullptr)
	}

	Menu = MainMenu::CreateObject();
	CHECK(Menu != nullptr)
	Menu->RegisterToMainWindow();

	if (Settings != nullptr)
	{
		ApplyUserSettings();
	}
}

void DroneDestroyerEngineComponent::ShutdownComponent ()
{
	if (Menu != nullptr)
	{
		Menu->Destroy();
	}

	if (SoundSender != nullptr)
	{
		SoundSender->Destroy();
	}

	SetPausedGame(false);
	CleanupGameInstance();

	if (Settings != nullptr)
	{
		Settings->SaveUserSettings();
		Settings->Destroy();
	}

	Super::ShutdownComponent();
}

std::vector<const SD::DClass*> DroneDestroyerEngineComponent::GetPreInitializeDependencies () const
{
	std::vector<const SD::DClass*> dependencies = Super::GetPreInitializeDependencies();
	dependencies.push_back(SD::GuiEngineComponent::SStaticClass());
	dependencies.push_back(SD::GraphicsEngineComponent::SStaticClass());

	return dependencies;
}

std::vector<const SD::DClass*> DroneDestroyerEngineComponent::GetInitializeDependencies () const
{
	std::vector<const SD::DClass*> dependencies = Super::GetInitializeDependencies();
	dependencies.push_back(SD::GuiEngineComponent::SStaticClass());

	return dependencies;
}

void DroneDestroyerEngineComponent::RegisterComponentToMainDrawLayer (SD::RenderComponent* component)
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	CHECK(localDroneEngine != nullptr)
	SD::SceneDrawLayer* drawLayer = localDroneEngine->GetMainDrawLayer();
	CHECK(drawLayer != nullptr)

	drawLayer->RegisterSingleComponent(component);
}

void DroneDestroyerEngineComponent::CreateSoul (const SD::DString& playerName, SD::INT teamNum, bool isSpectator, bool isPlayer)
{
	PlayerSoul* newSoul = PlayerSoul::CreateObject();
	CHECK(newSoul != nullptr)

	//TODO: check for name conflicts
	newSoul->SetPlayerName(playerName);
	newSoul->SetTeamNumber(teamNum);
	newSoul->SetIsSpectator(isSpectator);

	if (isPlayer)
	{
		newSoul->InitializeInputComponent();
	}
	else
	{
		newSoul->InitializeBotComponent();
	}
}

void DroneDestroyerEngineComponent::LaunchGame ()
{
	CHECK_INFO(!PendingArenaName.IsEmpty(), "Arena is not specified")

	Environment = Arena::CreateObject();

	//Ensure the Environment variable is set before InitializingArenaObjects since some Entities (like the NavigationComponent) needs the engineComponent to assign this variable.
	Environment->InitializeArenaObjects(PendingArenaName);

	PlayerHud = Hud::CreateObject();
	CHECK(PlayerHud != nullptr)
	PlayerHud->RegisterToMainWindow();
	PlayerHud->SetPauseMode(PausingGame);

	Game->InitializeGame();
	Game->AddComponentsToHud(PlayerHud.Get());

	if (Menu != nullptr)
	{
		Menu->Destroy();
	}
}

void DroneDestroyerEngineComponent::ReturnToTitleScreen ()
{
	SetPausedGame(false);
	CleanupGameInstance();

	//Return to title screen
	CHECK(Menu == nullptr)
	Menu = MainMenu::CreateObject();
	Menu->RegisterToMainWindow();
}

void DroneDestroyerEngineComponent::SetPausedGame (bool newPaused)
{
	if (newPaused == PausingGame)
	{
		return;
	}

	PausingGame = newPaused;
	if (PlayerHud != nullptr)
	{
		PlayerHud->SetPauseMode(PausingGame);
	}

	SD::Engine* localEngine = GetOwningEngine();
	CHECK(localEngine != nullptr)

	SD::TickGroup* physicsTick = localEngine->FindTickGroup(TICK_GROUP_PHYSICS);
	if (physicsTick != nullptr)
	{
		physicsTick->SetTicking(!PausingGame);
	}

	SD::TickGroup* droneTickGroup = localEngine->FindTickGroup(TICK_GROUP_DD);
	if (droneTickGroup != nullptr)
	{
		droneTickGroup->SetTicking(!PausingGame);
	}
}

void DroneDestroyerEngineComponent::TogglePause ()
{
	SetPausedGame(!PausingGame);
}

void DroneDestroyerEngineComponent::PlayLocalSound (const SD::DString& soundName, const SD::Vector3& soundOrigin, SD::FLOAT volMultiplier)
{
	if (SoundSender == nullptr || MainCamera == nullptr)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Cannot play %s since the engine component does not have a camera or sound messenger ready for it."), soundName);
		return;
	}

	SD::FLOAT zoomMultiplier = 1.f;
	if (DefaultCamZoom > MainCamera->Zoom && DefaultCamZoom > 0.f)
	{
		//Zoom multiplier increases hearing radius and fades the sound based on how zoomed out the camera is.
		zoomMultiplier = DefaultCamZoom / MainCamera->Zoom;
	}

	SD::FLOAT curInnerRadius = InnerHearingRadius * zoomMultiplier;
	SD::FLOAT curOuterRadius = OuterHearingRadius * zoomMultiplier;

	SD::FLOAT dist = (MainCamera->ReadAbsTranslation() - soundOrigin).VSize(false);
	if (dist >= curOuterRadius)
	{
		//Too far away to hear
		return;
	}

	SD::FLOAT finalVol = 1.f;
	if (dist > curInnerRadius)
	{
		finalVol = SD::Utils::Lerp<SD::FLOAT>((dist - curInnerRadius) / curOuterRadius, 1.f, 0.f);
	}

	//Fade the sound volume based on the zoom value
	if (zoomMultiplier != 1.f)
	{
		finalVol /= zoomMultiplier;
	}

	finalVol *= SD::Utils::Clamp<SD::FLOAT>(volMultiplier, 0.001f, 1.f);

	SoundSender->PlaySound(false, soundName, finalVol);
}

void DroneDestroyerEngineComponent::SPlayLocalSound (const SD::DString& soundName, const SD::Vector3& soundOrigin, SD::FLOAT volMultiplier)
{
	DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
	if (localDroneEngine != nullptr)
	{
		localDroneEngine->PlayLocalSound(soundName, soundOrigin, volMultiplier);
	}
}

void DroneDestroyerEngineComponent::SetGameRules (const GameRules* cdo)
{
	if (Game != nullptr)
	{
		Game->Destroy();
	}

	CHECK(cdo != nullptr)
	Game = cdo->CreateObjectOfMatchingClass();
}

void DroneDestroyerEngineComponent::SetPendingArena (const SD::DString& newPendingArenaName)
{
	PendingArenaName = newPendingArenaName;
}

void DroneDestroyerEngineComponent::ImportGameAssets ()
{
	//Fonts
	{
		SD::FontPool* localFontPool = SD::FontPool::FindFontPool();
		CHECK(localFontPool != nullptr)

		localFontPool->CreateAndImportFont(TXT("PixelDigivolve.ttf"), TXT("PixelDigivolve"));
	}

	//Textures
	{
		SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)

		//Characters
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Enemies"), TXT("DeadEnemies.png")), TXT("DroneDestroyer.Enemies.DeadEnemies"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Enemies"), TXT("Enemies_V2.png")), TXT("DroneDestroyer.Enemies.Enemies_V2"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Enemies"), TXT("Enemies5.png")), TXT("DroneDestroyer.Enemies.Enemies5"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Player"), TXT("BrightPlayer.png")), TXT("DroneDestroyer.Player.BrightPlayer"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Player"), TXT("DeadPlayer.png")), TXT("DroneDestroyer.Player.DeadPlayer"));

		//Pickups
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Pickups"), TXT("Ammo.png")), TXT("DroneDestroyer.Pickups.Ammo"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Pickups"), TXT("Handgun.png")), TXT("DroneDestroyer.Pickups.Handgun"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Pickups"), TXT("Machinegun.png")), TXT("DroneDestroyer.Pickups.Machinegun"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Pickups"), TXT("Medkit.png")), TXT("DroneDestroyer.Pickups.Medkit"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Pickups"), TXT("RocketLauncher.png")), TXT("DroneDestroyer.Pickups.RocketLauncher"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Pickups"), TXT("Shotgun.png")), TXT("DroneDestroyer.Pickups.Shotgun"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Pickups"), TXT("SniperRifle.png")), TXT("DroneDestroyer.Pickups.SniperRifle"));

		//Projectiles
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Enemies"), TXT("Enemies4.png")), TXT("DroneDestroyer.Enemies.Enemies4"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Enemies"), TXT("Enemies6.png")), TXT("DroneDestroyer.Enemies.Enemies6"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Projectiles"), TXT("Bullet.png")), TXT("DroneDestroyer.Projectiles.Bullet"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Projectiles"), TXT("Rocket.png")), TXT("DroneDestroyer.Projectiles.Rocket"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Projectiles"), TXT("SniperBullet.png")), TXT("DroneDestroyer.Projectiles.SniperBullet"));

		//Weapons
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Weapons"), TXT("Handgun.png")), TXT("DroneDestroyer.Weapons.Handgun"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Weapons"), TXT("MachineGun.png")), TXT("DroneDestroyer.Weapons.MachineGun"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Weapons"), TXT("RocketLauncher.png")), TXT("DroneDestroyer.Weapons.RocketLauncher"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Weapons"), TXT("Shotgun.png")), TXT("DroneDestroyer.Weapons.Shotgun"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Weapons"), TXT("Sniper.png")), TXT("DroneDestroyer.Weapons.Sniper"));

		//Interface
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Checkbox.png")), TXT("DroneDestroyer.Interface.Checkbox"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Crosshair_Black.png")), TXT("DroneDestroyer.Interface.Crosshair_Black"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Crosshair_White.png")), TXT("DroneDestroyer.Interface.Crosshair_White"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Crosshair_Red.png")), TXT("DroneDestroyer.Interface.Crosshair_Red"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Crosshair_Yellow.png")), TXT("DroneDestroyer.Interface.Crosshair_Yellow"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Crosshair_Green.png")), TXT("DroneDestroyer.Interface.Crosshair_Green"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Crosshair_Cyan.png")), TXT("DroneDestroyer.Interface.Crosshair_Cyan"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Crosshair_Blue.png")), TXT("DroneDestroyer.Interface.Crosshair_Blue"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Crosshair_Magenta.png")), TXT("DroneDestroyer.Interface.Crosshair_Magenta"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Deathmatch.png")), TXT("DroneDestroyer.Interface.Deathmatch"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("Extinction.png")), TXT("DroneDestroyer.Interface.Extinction"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("LastManStanding.png")), TXT("DroneDestroyer.Interface.LastManStanding"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("Interface"), TXT("TeamDeathmatch.png")), TXT("DroneDestroyer.Interface.TeamDeathmatch"));

		//Misc
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer"), TXT("Box2dLogo.png")), TXT("DroneDestroyer.Box2dLogo"));
		localTexturePool->CreateAndImportTexture(SD::PrimitiveFileAttributes(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer"), TXT("SfmlLogo.png")), TXT("DroneDestroyer.SfmlLogo"));

		//Dynamically import all images in case there are level designers who want to add their own images.
		for (SD::FileIterator fileIter(SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer") / TXT("AutoImported"), SD::FileIterator::INCLUDE_FILES | SD::FileIterator::ITERATE_SUBDIRECTORIES); !fileIter.FinishedIterating(); ++fileIter)
		{
			CHECK(!fileIter.IsDirectorySelected())

			SD::PrimitiveFileAttributes fileAttr = fileIter.GetSelectedAttributes();

			SD::DString textureName = fileAttr.GetName(true, false);

			//Remove the texture directory prefix
			SD::DString::SubString(OUT textureName, (SD::Texture::TEXTURE_DIRECTORY / TXT("DroneDestroyer")).ToString().Length());

			//Replace slashes with dots
			textureName.ReplaceInline(SD::Directory::DIRECTORY_SEPARATOR, TXT("."), SD::DString::CC_CaseSensitive);

			localTexturePool->CreateAndImportTexture(fileAttr, textureName);
		}
	}
}

void DroneDestroyerEngineComponent::ApplyUserSettings ()
{
	CHECK(Settings != nullptr)

	SD::InputEngineComponent* localInputEngine = SD::InputEngineComponent::Find();
	CHECK(localInputEngine != nullptr)
	if (SD::MousePointer* mouse = localInputEngine->GetMouse())
	{
		SD::TexturePool* localTexturePool = SD::TexturePool::FindTexturePool();
		CHECK(localTexturePool != nullptr)
		SD::Texture* mouseIcon = localTexturePool->FindTexture(Settings->ReadCursorName(), true);
		if (mouseIcon == nullptr)
		{
			//Whatever was in the config has failed. Fallback to hard coded defaults.
			mouseIcon = localTexturePool->FindTexture(Settings->ReadDefaultCursorName(), true);
		}

		mouse->SetDefaultIcon(mouseIcon);
	}

	if (SoundSender != nullptr)
	{
		SD::FLOAT gameVolume = Settings->GetGameVolume().ToFLOAT();
		gameVolume *= 0.01f;
		SoundSender->SetVolumeLevel(false, gameVolume);

		SD::FLOAT musicVolume = Settings->GetMusicVolume().ToFLOAT();
		musicVolume *= 0.01f;
		SoundSender->SetVolumeLevel(true, musicVolume);
	}
}

bool DroneDestroyerEngineComponent::LaunchSoundThread ()
{
	//Create a thread that launches the Debug Engine
	SD::SDThread newThread;
	THREAD_INIT_TYPE threadInitData;
	threadInitData.ThreadName = TXT("Sound Thread");
	int exitCode = OS_CreateThread(newThread, InitializeSoundThread, nullptr, &threadInitData);
	if (exitCode != 0)
	{
		DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to launch sound thread. This game instance is now muted."));
		return false;
	}

	//In milliseconds
	SD::INT timeoutTime = 5000;
	SD::INT totalWaitTime = 0;
	SD::INT sleepInterval = 250;
	//Wait for the DebugEngine to finish initializing before kicking off the ThreadTester test
	while (SD::Engine::GetEngine(SoundEngineComponent::SOUND_ENGINE_IDX) == nullptr || !SD::Engine::GetEngine(SoundEngineComponent::SOUND_ENGINE_IDX)->IsInitialized())
	{
		totalWaitTime += sleepInterval;
		if (totalWaitTime >= timeoutTime)
		{
			DroneDestroyerLog.Log(SD::LogCategory::LL_Warning, TXT("Failed to launch sound thread. Waiting for the sound engine to initialize has timed out. The game has waited for %s milliseconds"), timeoutTime);
			return false;
		}

		OS_Sleep(sleepInterval);
	}

	DroneDestroyerLog.Log(SD::LogCategory::LL_Log, TXT("The Sound Engine thread has successfully initialized!"));
	return true;
}

void DroneDestroyerEngineComponent::CleanupGameInstance ()
{
	if (Environment != nullptr)
	{
		//Destroys all environmental Entities such as pickup stations, path nodes, static entities
		Environment->Destroy();
	}

	//Destroy all souls
	if (Game != nullptr)
	{
		PlayerSoul* soul = Game->GetFirstPlayer();
		while (soul != nullptr)
		{
			PlayerSoul* nextSoul = soul->GetNextSoul();
			soul->Destroy(); //Destroys the character they posses. Destroying the character also destroys the inventory associated with them.
			soul = nextSoul;
		}
	}

	//Destroy all Inventory items since those spawn dynamically and are not associated with the Arena.
	std::vector<Inventory*> purgeList;
	for (SD::ObjectIterator iter(GetOwningEngine()->GetEntityHashNumber(), true); iter.SelectedObject != nullptr; ++iter)
	{
		if (Inventory* inv = dynamic_cast<Inventory*>(iter.SelectedObject.Get()))
		{
			purgeList.push_back(inv); //Don't purge in a middle of an object iterator
		}
	}

	for (Inventory* inv : purgeList)
	{
		inv->Destroy();
	}

	//No need to purge projectiles since they will automatically destroy themselves due to life span components.

	if (PlayerHud != nullptr)
	{
		PlayerHud->Destroy();
	}

	//Finally destroy the GameRules itself.
	if (Game != nullptr)
	{
		Game->Destroy();
	}

	if (MainCamera != nullptr)
	{
		//Reset camera back to defaults in case another game launches
		MainCamera->SetTranslation(SD::Vector3(0.f, 0.f, DRAW_ORDER_CAMERA));
		if (const SD::TopDownCamera* cdo = dynamic_cast<const SD::TopDownCamera*>(MainCamera->GetDefaultObject()))
		{
			MainCamera->Zoom = cdo->Zoom;
		}

		//Remove the camera movement component if there is one
		if (MovementComponent* moveComp = dynamic_cast<MovementComponent*>(MainCamera->FindSubComponent(MovementComponent::SStaticClass(), true)))
		{
			moveComp->Destroy();
		}
	}
}

SD_THREAD_FUNCTION(InitializeSoundThread)
{
	SD::Engine* soundEngine = new SD::Engine();
	CHECK(soundEngine != nullptr)

	std::vector<SD::EngineComponent*> engineComponents;
	engineComponents.push_back(new SD::MultiThreadEngineComponent());
	engineComponents.push_back(new SoundEngineComponent());
	soundEngine->InitializeEngine(SoundEngineComponent::SOUND_ENGINE_IDX, engineComponents);
#ifdef DEBUG_MODE
	soundEngine->SetDebugName(TXT("Sound Engine"));
#endif

	while (soundEngine != nullptr)
	{
		soundEngine->Tick();

		if (soundEngine->IsShuttingDown())
		{
			delete soundEngine;
			soundEngine = nullptr;
		}
	}

	SD_EXIT_THREAD
}
DD_END