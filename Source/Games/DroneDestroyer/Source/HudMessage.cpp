/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HudMessage.cpp
=====================================================================
*/

#include "HudMessage.h"

DD_BEGIN
IMPLEMENT_CLASS(DD, HudMessage, SD, LabelComponent)

void HudMessage::InitProps ()
{
	Super::InitProps();

	DisplayTime = 3.f;
	StartFadeTime = 1.f;
}

void HudMessage::BeginObject ()
{
	Super::BeginObject();

	if (StartFadeTime > 0.f && StartFadeTime < DisplayTime)
	{
		TimeRemaining = DisplayTime;

		SD::TickComponent* tick = SD::TickComponent::CreateObject(TICK_GROUP_DD);
		if (AddComponent(tick))
		{
			tick->SetTickHandler(SDFUNCTION_1PARAM(this, HudMessage, HandleTick, void, SD::FLOAT));
		}
	}
	else
	{
		SD::LifeSpanComponent* timeLimit = SD::LifeSpanComponent::CreateObject();
		if (AddComponent(timeLimit))
		{
			//Simple pop out instead of fading
			timeLimit->LifeSpan = DisplayTime;
		}
	}
}

void HudMessage::Destroyed ()
{
	if (OnExpiration.IsBounded())
	{
		OnExpiration(this);
	}

	Super::Destroyed();
}

void HudMessage::HandleTick (SD::FLOAT deltaSec)
{
	TimeRemaining -= deltaSec;
	if (TimeRemaining <= 0.f)
	{
		Destroy();
		return;
	}

	if (TimeRemaining < (DisplayTime - StartFadeTime) && RenderComponent != nullptr)
	{
		SD::FLOAT alpha = TimeRemaining / (DisplayTime - StartFadeTime);
		sf::Color newColor = RenderComponent->GetFontColor();
		newColor.a = SD::Utils::Lerp<sf::Uint8, float>(alpha.Value, 0, 255);
		RenderComponent->SetFontColor(newColor);
	}
}

DD_END