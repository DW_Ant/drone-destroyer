/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Hud.h

  A GuiEntity that displays UI components over the screen during gameplay.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class HudMessage;

class Hud : public SD::GuiEntity
{
	DECLARE_CLASS(Hud)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SStackMessage
	{
		SStackMessage (const SD::DString& inText, SD::FLOAT inDisplayedTimestamp) :
			Text(inText),
			DisplayedTimestamp(inDisplayedTimestamp)
		{
			//Noop
		}

		SD::DString Text;
		SD::FLOAT DisplayedTimestamp;
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines how long a message can be displayed on the feed before it pops out. */
	SD::FLOAT MessageFeedDisplayTime;

	/* Event invoked whenever the EndGame messages fade out completely. */
	SD::SDFunction<void> OnEndGameMsgFadeOut;

protected:
	SD::LabelComponent* AmmoDisplay;
	SD::LabelComponent* HealthDisplay;
	SD::LabelComponent* ViewedSoulDisplay;
	SD::FrameComponent* PauseFrame;

	SD::FrameComponent* EndGameFrame;
	SD::LabelComponent* EndGameLabel;

	/* Different from DisplayedMessages. This component displays a stack of game events. No fading and it allows up multiple messages. */
	SD::LabelComponent* MessageFeed;

	/* List of stacked messages that'll appear in the MessageFeed component. This list is sorted by timestamp where
	younger messages appear at the end of the list. */
	std::vector<SStackMessage> StackedMessages;

	/* List of Messages that are currently being displayed on the HUD. Only one message of each class can be active at once. */
	std::vector<HudMessage*> DisplayedMessages;

	/* List of components to set to visible whenever the player becomes an active participant. */
	std::vector<SD::GuiComponent*> PlayerOnlyComponents;

	/* List of components to set to visible whenever the player becomes a spectator. */
	std::vector<SD::GuiComponent*> SpectatorOnlyComponents;

	/* Total time it takes for the EndGame components to fade out. */
	SD::FLOAT EndGameFadeTime;

private:
	/* Timestamp when the end game messages appeared. */
	SD::FLOAT EndGameTimeStamp;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Registers the given HudMessage to the list for ticking.
	 * The Hud components will automatically be destroyed once the fade out completes.
	 * Adding multiple huds of the same class will destroy and replace the others of matching class.
	 */
	virtual void AddHudMessage (HudMessage* newMsg);

	/**
	 * Sends a new message to be displayed on the message feed. If there are too many messages, this
	 * function may purge some of the older messages.
	 */
	virtual void AddMessageFeed (const SD::DString& newMsg);

	/**
	 * Changes display mode to show components depending on the mode.
	 * @param playerMode - if true it'll hide all spectator only components while revealing player only components.
	 * If false it'll show all spectator only components while hiding all player only components.
	 */
	virtual void SetHudMode (bool playerMode);

	/**
	 * Informs the Hud to enter pause mode or not.
	 */
	virtual void SetPauseMode (bool isPauseMode);

	/**
	 * Creates components that flashes a temporary end game message on the hud.
	 */
	virtual void FlashEndGameMessage (const SD::DString& endGameMsg);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetAmmoAmount (SD::INT newAmmo);
	virtual void SetHealthAmount (SD::INT newHealth);
	virtual void SetViewedPlayer (const SD::DString& nameOfViewedPlayer);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Goes through the StackedMessages to populate the MessageFeed component.
	 */
	virtual void UpdateMessageFeed ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

public:
	virtual void HandleTick (SD::FLOAT deltaSec);
	virtual void HandleNewAmmo (SD::INT newAmmoAmount);
	virtual void HandleNewHealth (SD::INT newHealthAmount);

protected:
	virtual void HandleMsgExpired (HudMessage* oldMsg);
	virtual void HandleEndGameTick (SD::FLOAT deltaSec);
};
DD_END