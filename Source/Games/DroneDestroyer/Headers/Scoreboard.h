/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Scoreboard.h

  A transparent GuiEntity responsible for displaying the scoreboard of the active participants.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class GameRules;
class ScoreComponent;

class Scoreboard : public SD::GuiEntity
{
	DECLARE_CLASS(Scoreboard)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Section name where all translated text is found in the localization file. */
	SD::DString TranslationSectionName;

	/* List of individual players listed on the scoreboard. */
	std::vector<ScoreComponent*> Entries;

	/* Panel that lists the game limits (eg: score limit) */
	SD::LabelComponent* GameLimitLabel;

	/* If true, then this Entity will create and initialize the table headers. */
	bool ShouldInitializeHeaders;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void ConstructUI () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Iterates through each entry in the scoreboard to update their values based on their soul's current status.
	 */
	virtual void UpdatePlayerFields (GameRules* rules);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Hook to modify the summary text of the game limits. At this point, the GameLimitLabel should be allocated and attached to a component.
	 * It only needs text to be set.
	 */
	virtual void InitializeGameLimitLabel ();

	/**
	 * Hook to create and initialize a label component within the given headerFrame. The created components should describe
	 * what each number means in the player entries.
	 */
	virtual void InitializeHeaderLabel (SD::FrameComponent* headerFrame);

	/**
	 * Returns the ScoreComponent class to use when instantiating entries.
	 */
	virtual const ScoreComponent* GetScoreComponentClass () const;

	/**
	 * Creates and initializes player entries with information populated.
	 * The allowedSpace is a normalized value that informs how much space is permitted for the player entries.
	 */
	virtual void InitializeEntries (const SD::Vector2& startingPosition, const SD::Vector2& allowedSpace);
};
DD_END