/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MusicPlayer.h

  A simple Entity that plays a continuous music stream.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class MusicPlayer : public SD::Object
{
	DECLARE_CLASS(MusicPlayer)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	sf::Music Music;


	/*
	=====================
	  Inherited
	=====================
	*/

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Replaces the current music playback with the given music file name.
	 * Music files are expected to reside in the Sounds directory.
	 * Pass in an empty string to stop the music.
	 */
	virtual void PlayMusic (const SD::DString& musicName);

	virtual void SetMusicVolume (SD::FLOAT newVolume);
};
DD_END