/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SoundPlayer.h

  The Entity that resides in the SoundEngineComponent, this Entity can receive a ThreadedComponent,
  which can be used to communicate in the main thread. As soon as its component receives information
  to play a sound, it'll attempt to play the sound associated with the name.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class SoundPlayer : public SD::Entity
{
	DECLARE_CLASS(SoundPlayer)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Maximum number of sounds that can play simultaneously before it starts recycling sound instances. */
	static const size_t MAX_SOUND_INSTANCES;


protected:
	/* Volume multipliers to apply to all game/music sounds. */
	SD::FLOAT GameVolumeMultiplier;
	SD::FLOAT MusicVolumeMultiplier;

	SD::ThreadedComponent* Receiver;
	SD::ThreadedComponent* VolLevelReceiver;

	std::vector<sf::Sound> Sounds;

	/* Index in the Sounds vector that will play the next sound. */
	size_t NextSoundIndex;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetReceiver (SD::ThreadedComponent* newReceiver);
	virtual void SetVolLevelReceiver (SD::ThreadedComponent* newVolLevelReceiver);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * The receiver has data in it. This function processes that data to play sounds.
	 */
	virtual void ProcessNewData ();

	/**
	 * The volume level receiver component has data in it. This function processes that buffer to figure out what the new global volumes should be.
	 */
	virtual void ProcessNewVolumeLevels ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::FLOAT deltaSec);
};
DD_END