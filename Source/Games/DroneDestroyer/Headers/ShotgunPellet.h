/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ShotgunPellet.h

  Simple fast moving bullet that does weak damage on impact with variation to velocity magnitude and lifespan.
=====================================================================
*/

#pragma once

#include "Projectile.h"

DD_BEGIN
class ShotgunPellet : public Projectile
{
	DECLARE_CLASS(ShotgunPellet)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The maximum variation in velocity mangitude. The velocity can be randomly multiplied up to this value. */
	SD::FLOAT MaxVelocityMultiplier;

	/* The variation in this projectile's life span. It may randomly gain up to this many seconds. */
	SD::FLOAT MaxRandLifeSpan;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void InitializePhysics () override;

protected:
	virtual void InitializeRenderComponents () override;
};
DD_END