/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanel_Roster.h

  A panel that allows the user to configure bot names and teams.
=====================================================================
*/

#pragma once

#include "MenuPanelComponent.h"

DD_BEGIN
class MenuPanel_Roster : public MenuPanelComponent
{
	DECLARE_CLASS(MenuPanel_Roster)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct STeamData
	{
		SD::ScrollbarComponent* Scrollbar;

		/* The GuiEntity the scrollbar component views. This Entity will have SPlayerEntry components to represent which players are part of this team. */
		SD::GuiEntity* ViewedEntity;

		SD::Color TeamColor;

		/* Number of SPlayerEntries registered in this team. */
		SD::INT NumPlayers;

		/* The number that uniquely identifies this team where 0 is the left most team category, and max is the right most team. */
		SD::INT TeamNumber;
	};

	struct SPlayerEntry
	{
		SD::FrameComponent* Background;
		SD::TextFieldComponent* PlayerName;
		SD::CheckboxComponent* SpectatorCheckbox;

		/* Only applicable if there are multiple teams. This button moves this entry to the column on the left. */
		SD::ButtonComponent* PrevTeam;

		/* Only applicable if there are multiple teams. This button moves this entry to the column on the right. */
		SD::ButtonComponent* NextTeam;

		/* When clicked, it'll choose a random default bot name. */
		SD::ButtonComponent* RandomName;

		SD::INT TeamNumber;
		bool IsPlayer;
		bool IsSpectator;

		SPlayerEntry () :
			Background(nullptr),
			PlayerName(nullptr),
			SpectatorCheckbox(nullptr),
			PrevTeam(nullptr),
			NextTeam(nullptr),
			TeamNumber(-1),
			IsPlayer(false),
			IsSpectator(false)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The maximum number of allowed characters for a player name. */
	static const SD::INT PLAYER_NAME_LIMIT;

protected:
	/* Fallback color to use when there are more teams than colors. This is also used when this is not a team game. */
	SD::Color NeutralColor;

	/* Colors to use for the background for each team. */
	std::vector<SD::Color> TeamColors;

	std::vector<STeamData> Teams;

	/* List of players and bots that are on stage, getting ready to join the game. */
	std::vector<SPlayerEntry> Players;

	/* When generating a random bot name, this is the percent chance it'll use one of the names from the full name list. */
	SD::FLOAT FullBotNameChance;

	/* List of default full bot names pulled from the config file. */
	std::vector<SD::DString> DefaultFullBotNames;

	/* List of prefix/postfix names that may be combined to form a bot name. */
	std::vector<SD::DString> DefaultBotPrefixes;
	std::vector<SD::DString> DefaultBotPostfixes;

	/* The horizontal scaling used to apply on the scrollbar components for each team category. */
	SD::FLOAT RosterHorizontalScaling;

private:
	/* The vertical spacing of each player entry in the roster. */
	SD::FLOAT PlayerVerticalSpacing;

	/* The vertical size of each player entry. */
	SD::FLOAT PlayerVerticalSize;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void RestorePanel () override;
	virtual void SavePanelState () override;

protected:
	virtual void InitializeComponents () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns a randomly generated name that's either one of the full names or a combination between pre and postfixes.
	 */
	virtual SD::DString GenerateRandomBotName ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the config file to populate a list of bot names this component may use to generate random names.
	 */
	virtual void LoadDefaultBotNames ();

	/**
	 * Updates the player background color based on the team they belong to.
	 */
	virtual void UpdateBackgroundColor (SPlayerEntry& outPlayer, SD::INT teamNum) const;

	/**
	 * Moves the given player from the oldTeam to the newTeam.
	 */
	virtual void MovePlayer (SPlayerEntry& outPlayer, STeamData& outOldTeam, STeamData& outNewTeam);

	/**
	 * Iterates through all panels registered to the specified TeamCategory entity, and repositions
	 * them all so that there are no blank spaces between them.
	 */
	virtual void RepairRosterPositions (STeamData& outTeamCategory);

	/**
	 * Searches through the roster and returns the team and player that owns the given button.
	 * Returns true if the given belongs to a team and player.
	 */
	virtual bool FindTeamAndPlayer (SD::ButtonComponent* button, STeamData*& outTeamOwner, SPlayerEntry*& outPlayer);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleAllowTextInput (const SD::DString& txt);
	virtual void HandleClampScrollbarTick (SD::FLOAT deltaSec);
	virtual void HandleSetSpectatorChecked (SD::CheckboxComponent* uiComponent);
	virtual void HandleRandomNameReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleRandomAllReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleMoveEntryLeftReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleMoveEntryRightReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleBackReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleLaunchGameReleased (SD::ButtonComponent* uiComponent);
};
DD_END