/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Projectile.h

  Parent class of an Entity that moves forward and damages whomever gets in its way.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"
#include "Character.h"

DD_BEGIN
class ProjectilePhysicsComponent;
class Weapon;

class Projectile : public SD::Entity, public SD::SceneTransform
{
	DECLARE_CLASS(Projectile)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The collision group all projectiles will be under. */
	static const uint16 COLLISION_CATEGORY;
	
protected:
	/* The amount of damage this projectile will do when it directly strikes something with a HealthComponent. */
	SD::INT Damage;

	/* Determines how long this projectile should fly to until it explodes (doesn't fly forever). This should be set in InitProps. In seconds. */
	SD::FLOAT LifeSpan;

	SD::FLOAT CollisionRadius;

	/* The character responsible for shooting this projectile. */
	SD::DPointer<PlayerSoul> Shooter;

	/* The weapon that shot this projectile. */
	SD::DPointer<const Weapon> Weap;

	/* Deterines the velocity this projectile moves. Must be set before its physics component is initialized. */
	SD::FLOAT VelocityMagnitude;

	/* If true, then this projectile will have continuous collision where it cannot jump across thin targets between frames. */
	bool AllowContinuousCollision;

	/* Sound to play whenever this projectile perishes. */
	SD::DString ImpactSound;
	SD::FLOAT ImpactSoundVolume;

	/* Translation key that'll be used to for the death message should this projectile hit something. */
	SD::DString DeathMessageKey;

	/* For Extinction mode only: If true then this projectile can only detect drones. Otherwise, this projectile can only detect humans. */
	bool DamageDronesOnly;

	ProjectilePhysicsComponent* Physics;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Initializes this component to use a very dumb simple algorithm where it'll only either damage players or drones.
	 * If damageDrones is true, it'll only damage the drones and go through everything else. Otherwise, it'll only damage players.
	 */
	virtual void InitializeExtinctionPhysics (bool damageDrones);

	/**
	 * Creates and initializes the physics component responsible for handling collision and movement.
	 */
	virtual void InitializePhysics ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetShooter (PlayerSoul* newShooter);
	virtual void SetWeap (const Weapon* newWeap);
	

	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(SD::INT, Damage);
	DEFINE_ACCESSOR(SD::FLOAT, CollisionRadius);

	inline PlayerSoul* GetShooter () const
	{
		return Shooter.Get();
	}

	inline const Weapon* GetWeap () const
	{
		return Weap.Get();
	}

	DEFINE_ACCESSOR(SD::FLOAT, VelocityMagnitude);

	const SD::DString& ReadDeathMessageKey () const
	{
		return DeathMessageKey;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates and initializes any RenderComponents used to visually display this projectile.
	 */
	virtual void InitializeRenderComponents () = 0;

	/**
	 * A cheap substitute for the PhysicsComponent where this function processes this projectile's velocity and
	 * determines if it collides with anything.
	 */
	virtual void ProcessCheapPhysics (SD::FLOAT deltaSec);

	/**
	 * Invoked whenever this projectile directly collided with an Entity owning the given HealthComponent.
	 * Essentially this is where the projectile does direct damage.
	 * This is called prior to Explode.
	 */
	virtual void DoDamage (HealthComponent* victim);

	/**
	 * Invoked whenever this projectile collided with something.
	 */
	virtual void Explode ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCollision (SD::PhysicsComponent* otherComp, b2Contact* contactData, const b2ContactImpulse* impulse);
	virtual void HandleCheapPhysicsTick (SD::FLOAT deltaSec);
};
DD_END