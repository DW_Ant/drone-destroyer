/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HudDeathmatch.h

  A component that displays the player's status in a Deathmatch game.
  -Position/rank in the scoreboard
  -Spread/distance from 1st place
  -Kills
  -Deaths
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class PlayerSoul;

class HudDeathmatch : public SD::GuiComponent
{
	DECLARE_CLASS(HudDeathmatch)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::LabelComponent* PositionLabel;
	SD::LabelComponent* SpreadLabel;
	SD::LabelComponent* KillsLabel;
	SD::LabelComponent* DeathsLabel;

	/* Translated prefixes for each label (to avoid having to translate these variables every time the scores are updated). */
	SD::DString PositionRatio;
	SD::DString PositionPostfix;
	SD::DString SpreadPrefix;
	SD::DString KillsPrefix;
	SD::DString DeathsPrefix;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void InitializeComponents () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Updates the label components to reflect the current status of the player.
	 */
	virtual void UpdateScores (const std::vector<SD::DPointer<PlayerSoul>>& rankedPlayers);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual sf::Color GetPositionColor (size_t rank);
	virtual sf::Color GetSpreadColor (SD::INT spread);
};
DD_END