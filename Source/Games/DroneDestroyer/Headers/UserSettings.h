/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  UserSettings.h

  A collection of variables intended to be saved and accessed in various places in the game.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class UserSettings : public SD::Object
{
	DECLARE_CLASS(UserSettings)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* If true, then the camera will pan towards the crosshair. Otherwise, it'll remain locked on the character. */
	bool DynamicCam;
	bool DefaultDynamicCam;

	/* Name of the image that should be usedfor the cursor. */
	SD::DString CursorName;
	SD::DString DefaultCursorName;

	SD::INT GameVolume;
	SD::INT DefaultGameVolume;

	SD::INT MusicVolume;
	SD::INT DefaultMusicVolume;

	/* Name of the configuration file the data are saved in. */
	SD::DString ConfigFile;
	SD::DString ConfigSection;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	static UserSettings* GetUserSettings ();

	/**
	 * Opens the config file and intiailizes various properties based on what's there. If there is no data, it'll refer to the defaults instead.
	 */
	virtual void LoadUserSettings ();

	/**
	 * Resets all settings back to their default values.
	 */
	virtual void ResetToDefaults ();

	virtual void SaveUserSettings ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetDynamicCam (bool newDynamicCam);
	virtual void SetCursorName (const SD::DString& newCursorName);
	virtual void SetGameVolume (SD::INT newGameVolume);
	virtual void SetMusicVolume (SD::INT newMusicVolume);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(bool, DynamicCam)
	DEFINE_ACCESSOR(bool, DefaultDynamicCam)

	const SD::DString& ReadCursorName () const
	{
		return CursorName;
	}

	const SD::DString& ReadDefaultCursorName () const
	{
		return CursorName;
	}

	DEFINE_ACCESSOR(SD::INT, GameVolume);
	DEFINE_ACCESSOR(SD::INT, DefaultGameVolume);
	DEFINE_ACCESSOR(SD::INT, MusicVolume);
	DEFINE_ACCESSOR(SD::INT, DefaultMusicVolume);
};
DD_END