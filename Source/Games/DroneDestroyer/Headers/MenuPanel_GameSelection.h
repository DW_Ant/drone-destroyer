/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanel_GameSelection.h

  The panel that allows the user to choose the game mode, go to user settings, or exit.
=====================================================================
*/

#pragma once

#include "MenuPanelComponent.h"

DD_BEGIN
class GameRules;

class MenuPanel_GameSelection : public MenuPanelComponent
{
	DECLARE_CLASS(MenuPanel_GameSelection)


	/*
	=====================
	  Struct
	=====================
	*/

protected:
	struct SGameTypeSelection
	{
		SD::ButtonComponent* Button;
		const GameRules* Gametype;

		/* The size and position this button is lerping towards. */
		SD::Vector2 HeadedPosition;
		SD::Vector2 HeadedSize;

		/* The size and position this button is lerping from. */
		SD::Vector2 PrevPosition;
		SD::Vector2 PrevSize;

		/* If true, then the button component may reach to the HeadedPosition and HeadedSize in half the amount of time. */
		bool ReachesInHalftime;

		/* Becomes true if this button is currently lerping towards HeadedPosition. */
		bool IsAnimating;

		SGameTypeSelection(SD::ButtonComponent* inButton, const GameRules* inGametype) :
			Button(inButton),
			Gametype(inGametype)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* How much time it takes to browse to another game type (in seconds). */
	SD::FLOAT BrowseToGameTime;

protected:
	/* List of buttons that allows the user to choose the game to run. */
	std::vector<SGameTypeSelection> Games;

	/* Index in the games list that is currently front and center. */
	size_t SelectedGame;

	/* The size and position of the selected game button. */
	SD::Vector2 SelectedGameSize;
	SD::Vector2 SelectedGamePosition;

	/* The size and position of the left buttons that are not visible in the menu. */
	SD::Vector2 LeftFadeOutSize;
	SD::Vector2 LeftFadeOutPosition;

	/* The size and position of previous game button. */
	SD::Vector2 PrevGameSize;
	SD::Vector2 PrevGamePosition;

	/* The size and positio of the next game button. */
	SD::Vector2 NextGameSize;
	SD::Vector2 NextGamePosition;

	/* The size and position of the right buttons that are not visible in the menu. */
	SD::Vector2 RightFadeOutSize;
	SD::Vector2 RightFadeOutPosition;

	SD::ButtonComponent* NextGame;
	SD::ButtonComponent* PrevGame;
	SD::ScrollbarComponent* GameDescriptionFrame;
	SD::LabelComponent* GameDescriptionLabel;
	SD::GuiEntity* GameDescriptionEntity;
	SD::TickComponent* Tick;

private:
	/* Index of the games vector the user is heading towards. */
	size_t DesiredSelectedGame;

	/* Becomes true when the current transition has past its halfway point. */
	bool IsPastHalfway;

	/* Becomes true if the buttons are panning left. Otherwise they're panning right. Only applicable when DesiredSelectedGame is not equal to the Selected Game. */
	bool IsMovingRight;

	/* Timestamp when the user last clicked on the game. */
	SD::FLOAT GameSelectedTime;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void RestorePanel () override;
	virtual void SavePanelState () override;

protected:
	virtual void InitializeComponents () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this component is currently animating the game type selection buttons.
	 */
	virtual bool IsAnimating () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through the games and dynamically populates the list of game types based on what it finds.
	 */
	virtual void InitializeGameSelection ();

	/**
	 * Begins the sequence to browse towards the selected game.
	 */
	virtual void BrowseToGame (size_t newDesiredGameIdx);

	/**
	 * Invoked whenever the desired game is now front and center.
	 */
	virtual void BrowseToGameFinished ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleBrowseGameTick (SD::FLOAT deltaSec);
	virtual void HandleGameButtonReleased (SD::ButtonComponent* uiComponent);
	virtual void HandlePrevButtonReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleNextButtonReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleUserSettingsReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleCreditsReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleQuitReleased (SD::ButtonComponent* uiComponent);
};
DD_END