/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MovementComponent.h

  A component that continuously modifies its owning PhysicsComponent of its velocity based on this component's normalized direction.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN

class MovementComponent : public SD::EntityComponent
{
	DECLARE_CLASS(MovementComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const SD::INT INPUT_PRIORITY;

protected:
	/* If true, then this component is listening for input events to apply velocity to the owner. */
	bool IsActive;

	/* If true, then this component will not adjust the owner's rotation. */
	bool LockedRotation;

	/* Movement in all axis that range from -1 to 1. This direction lerps from current to DesiredDirection based on the Acceleration. */
	SD::Vector3 Direction;

	/* The desired Movement direction this component is headed. */
	SD::Vector3 DesiredDirection;

	/* The maximum velocity this component can achieve in any direction (in centimeters per second). */
	SD::FLOAT VelocityMagnitude;

	/* Determines the number of seconds for Direction to reach from 0 to 1. */
	SD::FLOAT Acceleration;

	SD::PhysicsComponent* OwningPhysComp;

	/* If the owning component is not a Physics Component, it'll then attempt to move the owner by transform without running any physics collision. */
	SD::SceneTransform* OwningTransform;
	SD::TickComponent* Tick;

private:
	/* Various flags used to record multiple key presses for computing the movement state. */
	bool MovingUp : 1;
	bool MovingRight : 1;
	bool MovingDown : 1;
	bool MovingLeft : 1;
	bool IsWalking : 4;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initialize an Input component that'll send events to manipulate the desired direction.
	 */
	virtual void InitializeInputComponent ();
	

	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetActive (bool newActive);
	virtual void SetLockedRotation (bool newLockedRotation);
	virtual void SetDesiredDirection (const SD::Vector3& newDesiredDirection);
	virtual void SetVelocityMagnitude (SD::FLOAT newVelocityMagnitude);
	virtual void SetAcceleration (SD::FLOAT newAcceleration);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline SD::Vector3 GetDesiredDirection () const
	{
		return DesiredDirection;
	}

	inline const SD::Vector3& ReadDesiredDirection () const
	{
		return DesiredDirection;
	}

	inline SD::FLOAT GetVelocityMagnitude () const
	{
		return VelocityMagnitude;
	}

	inline SD::FLOAT GetAcceleration () const
	{
		return Acceleration;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Looks at the owning component's velocity and 'adds' this component's movement velocity up
	 * to the magnitude.
	 */
	virtual void ApplyVelocity (SD::FLOAT deltaSec);

	/**
	 * Looks at the various movement states to calculate the DesiredDirection.
	 */
	virtual void ComputeDesiredDirection ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::FLOAT deltaSec);
	virtual bool HandleInput (const sf::Event& evnt);
	virtual void HandleMouseMove (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const SD::Vector2& deltaMove);
};
DD_END