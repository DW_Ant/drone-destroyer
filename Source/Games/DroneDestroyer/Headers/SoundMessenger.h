/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SoundMessenger.h

  An Entity for sending messages across threads to play sounds on the sound thread.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class SoundMessenger : public SD::Entity
{
	DECLARE_CLASS(SoundMessenger)

	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct SSoundData
	{
		SD::BOOL IsMusic;
		SD::DString SoundName;
		SD::FLOAT SoundVolume;
	};


	/*
	=====================
	  Properties
	=====================
	*/


protected:
	SD::ThreadedComponent* Sender;

	/* ThreadedComponent responsible for sending changes to the global audio volume levels. */
	SD::ThreadedComponent* VolumeLevelSender;

	/* List of sounds to transfer over on the next transfer cycle. */
	std::vector<SSoundData> PendingSounds;

private:
	/* Data waiting to be transfered to the other thread. If negative, then there's no new data to send. */
	SD::FLOAT PendingGameVolume;
	SD::FLOAT PendingMusicVolume;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Quick accessor to obtain the sound messenger.
	 */
	static SoundMessenger* GetMessenger ();

	/**
	 * Sends the given string to the sound thread and finds the sound file associated with that string.
	 * If there is an association, it'll attempt to play that sound as soon as possible.
	 */
	virtual void PlaySound (bool isMusic, const SD::DString& soundName, SD::FLOAT volMultiplier = 0.75f);

	virtual void SetVolumeLevel (bool music, SD::FLOAT newVolume);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCopyData (SD::DataBuffer& outData);
	virtual void HandleTransferVolLevel (SD::DataBuffer& outData);
};
DD_END