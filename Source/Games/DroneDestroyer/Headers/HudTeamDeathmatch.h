/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HudTeamDeathmatch.h

  A component that displays the team scores on the right side.
  And the player status above the health.
  The status only displays kills and deaths.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"
#include "TeamDeathmatch.h"

DD_BEGIN
class PlayerSoul;

class HudTeamDeathmatch : public SD::GuiComponent
{
	DECLARE_CLASS(HudTeamDeathmatch)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::LabelComponent* KillsLabel;
	SD::LabelComponent* DeathsLabel;

	/* Translated prefixes for each label (to avoid having to translate these variables every time the scores are updated). */
	SD::DString KillsPrefix;
	SD::DString DeathsPrefix;

	/* List of labels corresponding to each team. The ordering of this vector must align with the TeamDeathmatch team vector. */
	std::vector<SD::LabelComponent*> TeamLabels;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void InitializeComponents () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes various UI components to for each team in the given game.
	 */
	virtual void InitializeTeams (const std::vector<TeamDeathmatch::STeam>& teams);

	/**
	 * Updates the label components to reflect the team scores.
	 * If the player is provided, it'll update the status labels for the player as well.
	 */
	virtual void UpdateScores (const std::vector<TeamDeathmatch::STeam>& teams, PlayerSoul* player);
};
DD_END