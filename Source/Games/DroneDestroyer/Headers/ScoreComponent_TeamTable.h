/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScoreComponent_TeamTable.h

  A scoreboard component that manages player entries for a single team.
=====================================================================
*/

#pragma once

#include "Scoreboard.h"
#include "TeamDeathmatch.h"

DD_BEGIN
class ScoreComponent;

class ScoreComponent_TeamTable : public SD::GuiComponent
{
	DECLARE_CLASS(ScoreComponent_TeamTable)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::FrameComponent* Background;
	SD::LabelComponent* TeamScoreLabel;
	std::vector<ScoreComponent*> Members;

	SD::DString TeamScorePrefix;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void InitializeComponents () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Instructs this component to populate its member data for the given team.
	 */
	virtual void InitializeComponent (const TeamDeathmatch::STeam& team);

	/**
	 * Updates all members in this component to reflect the team's current state.
	 * This also updates the team score label.
	 */
	virtual void UpdateTable (const TeamDeathmatch::STeam& team);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns the component to instantiate for each member.
	 */
	virtual const ScoreComponent* GetScoreComponentClass () const;
};
DD_END