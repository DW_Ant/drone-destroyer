/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BotSoulComponent.h

  Component that helps bots continue in the game without controlling a body.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"
#include "PlayerSoul.h"

DD_BEGIN

class BotSoulComponent : public SD::EntityComponent
{
	DECLARE_CLASS(BotSoulComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* How frequently should this bot evaluate the situation (in seconds). */
	SD::FLOAT ProcessInterval;

	SD::DPointer<PlayerSoul> OwningSoul;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void AttachTo (Entity* newOwner);
	virtual void ComponentDetached ();


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void EvaluateSituation ();
	

	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::FLOAT deltaSec);
};
DD_END