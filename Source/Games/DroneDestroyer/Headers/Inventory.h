/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Inventory.h

  Entity that has two states (Pickup and Possessed).
  When in pick up state, it'll adjust its components to be a detached Entity in the stage,
  waiting for a Character to walk over it to obtain it. This will also handle navigation,
  and provides bots hints in how important this pickup is to them.

  When in the possessed state, it'll power the owning character with the subclass
  defining the implementation.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class Character;
class NavigationComponent;
class OscillatingTransformComponent;

class Inventory : public SD::Entity, public SD::SceneTransform
{
	DECLARE_CLASS(Inventory)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Invoked whenever this inventory item is picked up. */
	SD::SDFunction<void, Inventory* /*takenItem*/, Character* /*taker*/> OnTaken;

protected:
	/* Key to translate when identifying the item name. */
	SD::DString ItemNameKey;

	/* The translated name from ItemNameKey. This is only set when GetItemName is called. */
	mutable SD::DString ItemName;

	/* Determines how close a Character must be in order to pick up this item. This must be set before Physics is initialized. */
	SD::FLOAT PickupRadius;

	/* Image to use to represent the pickup item for this inventory. If empty then this Entity will not have a PickupRenderComponent. This must be set before BeginObject. */
	SD::DString PickupImage;

	/* Image to use when this item is carried. If empty then this Entity will not have a PossessedRenderComponent. This must be set before BeginObject. */
	SD::DString PossessedImage;

	/* Sound to play whenever someone picks up this item. */
	SD::DString PickupSound;

	/* Becomes true if this inventory item is dropped and can be picked up. */
	bool IsDropped;

	/* The character that's holding this inventory item. */
	Character* Carrier;

	/* Only applicable if there's a carrier. This is the pointer to the inventory linked list the carrier possesses. */
	SD::DPointer<Inventory> NextInventory;

	/* Transform responsible for causing the pickup render component to bob up and down. */
	OscillatingTransformComponent* PickupRenderTransform;

	/* Render component to draw whenever this inventory item is in the Pickup state. */
	SD::SpriteComponent* PickupRenderComponent;

	/* Transform component used to animate the pickup whenever a carrier is holding it. */
	SD::SceneTransformComponent* PossessedTransformComponent;

	/* Render component to draw whenever this inventory item is possessed. */
	SD::SpriteComponent* PossessedRenderComponent;

	/* Physics component that detects overlap events to determine if a character can pick up this item. Disabled when possessed. */
	SD::PhysicsComponent* PickupCollision;

	NavigationComponent* Navigation;

	/* Tick component that ticks while this Inventory Entity is in a pickup state. */
	SD::TickComponent* PickupTicker;
	SD::FLOAT PickupTickerInterval;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Creates and initializes the PhysicsComponent responsible for pickup collision.
	 * This should be called after the Inventory translation is established since there's a performance hit
	 * if the physics component is initialized to origin (due to multiple collisions on start).
	 */
	virtual void InitializePhysics ();


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the specified character can pick up this inventory item.
	 */
	virtual bool CanPickupItem (Character* takerCandidate) const;

	/**
	 * Returns a number that suggests how desireable this item is for the given character.
	 * Typically non positive values are not desireable and should be avoided.
	 * A value of 1 means to treat this inventory item as a normal NavigationPoint the bot may randomly stumble into.
	 * Anything greater than 1 means there is some value to go here.
	 */
	virtual SD::INT GetImportanceValue (Character* possibleTaker) const;

	/**
	 * The specified character is about to take this item. What does this inventory item do
	 * to power up the character. 
	 */
	virtual void GiveTo (Character* taker);

	/**
	 * Returns true if this item can be tossed from its owner. 
	 */
	virtual bool CanBeDropped (Character* dropFrom) const;

	/**
	 * Called whenever the character has dropped this item with the given directional velocity.
	 * This removes self from dropFrom and initialize the Pickup state.
	 */
	virtual void DropItem (Character* dropFrom, const SD::Vector3& tossVelocity);

	/**
	 * Powers down the character to remove self from the owner.
	 */
	virtual void RemoveInventory (Character* removeFrom);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual SD::DString GetItemName () const;

	virtual Inventory* GetNextInventory () const
	{
		return NextInventory.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Invoked whenever this pickup is transitioning to pickup state.
	 */
	virtual void EnterPickupState ();

	/**
	 * Invoked whenever this pickup is exiting pickup state.
	 */
	virtual void ExitPickupState ();

	/**
	 * Invoked whenever this pickup is transitioning to possessed state where
	 * the given character is possessing this item.
	 */
	virtual void EnterPossessedState (Character* possesser);

	/**
	 * Invoked whenever this pickup is leaving the possessed state where the
	 * old owner is the character that use to carry this item.
	 */
	virtual void ExitPossessedState (Character* oldOwner);

	/**
	 * Creates and initializes an NavigationComponent to inform the bots to obtain this item when dropped.
	 */
	virtual void InitializeNavigationComponent ();

	/**
	 * Refreshes the debugging visualizer components' visibility based on this Entity's Physics state.
	 */
	virtual void UpdatePhysicsVisualizerVisibility ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandlePickupTick (SD::FLOAT deltaSec);
	virtual void HandleBeginContact (SD::PhysicsComponent* otherComp, b2Contact* contactData);
};
DD_END