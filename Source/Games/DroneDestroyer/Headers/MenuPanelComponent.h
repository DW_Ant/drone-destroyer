/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanelComponent.h

  A GuiComonent that provides an interface the MainMenu may invoke to handle
  pushing/popping menu panels.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class MainMenu;

class MenuPanelComponent : public SD::GuiComponent
{
	DECLARE_CLASS(MenuPanelComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The name of the config file to store menu states in. */
	SD::DString ConfigFile;

	/* Key to use when saving data to the config file. */
	SD::DString ConfigKey;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Checks each item in the config file to initialize its internal components.
	 */
	virtual void RestorePanel () = 0;

	/**
	 * Saves all essential data for this component so that when the user comes back to it,
	 * the menu is restored to where it was before.
	 */
	virtual void SavePanelState () = 0;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual MainMenu* GetOwningMenu () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Quick utility to access the main menu and instructs it to navigate backwards.
	 */
	virtual void RemoveThisPanel ();
};
DD_END