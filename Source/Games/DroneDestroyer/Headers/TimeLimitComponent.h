/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TimeLimitComponent.h

  A simple timer that runs a countdown, and invokes a function upon expiration.
  This also has a few milestone markers to give warnings of the time remaining.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN

class TimeLimitComponent : public SD::EntityComponent
{
	DECLARE_CLASS(TimeLimitComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The function to invoke whenever the time limit is reached. */
	SD::SDFunction<void> OnExpired;

	/* Event to broadcast whenever the GameTime changed. */
	SD::MulticastDelegate<void, SD::INT> OnTimeChanged;

protected:
	/* If there's a time limit, this number decrements to 0 before expiration.
	Otherwise this number simply increments (in seconds). */
	SD::FLOAT GameTime;

	/* The initial time limit value. If positive, then the GameTime will decrement. Otherwise, it'll increment. */
	SD::FLOAT OriginalTimeLimit;

	SD::TickComponent* Tick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this function is counting up. Otherwise it's counting down.
	 */
	inline bool IsCountingUp () const
	{
		return (OriginalTimeLimit > 0.f);
	}


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	void SetTimeLimit (SD::FLOAT newTimeLimit);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	/** 
	 * Get the rounded down verson of GameTime.
	 */
	inline SD::INT GetGameTime () const
	{
		return SD::INT(SD::FLOAT::RoundDown(GameTime));
	}

	inline SD::TickComponent* GetTick () const
	{
		return Tick;
	}


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::FLOAT deltaSec);
};
DD_END