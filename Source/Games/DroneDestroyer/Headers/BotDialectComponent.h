/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BotDialectComponent.h

  A component that broadcast bot messages on the hud whenever a particular event happened.

  Note: Although it makes sense to attach this to the BotCharacterComponents, it's actually
  attached to the GameRules so that the dialogue options are shared, and the GameRules already
  has hooks for death events.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class PlayerSoul;
class Weapon;

class BotDialectComponent : public SD::EntityComponent
{
	DECLARE_CLASS(BotDialectComponent)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SPendingMessage
	{
		SD::DString Message;

		/* How much time remaining before this message is sent (in seconds). */
		SD::FLOAT TimeRemaining;

		SPendingMessage(const SD::DString& inMessage, SD::FLOAT inTimeRemaining) :
			Message(inMessage),
			TimeRemaining(inTimeRemaining)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines how fast a bot can 'type' where the PendingMessage duration is determined based on the length of the message. */
	SD::FLOAT LettersPerSecond;

protected:
	/* List of greetings a bot may use. */
	std::vector<SD::DString> Greetings;

	/* A list of messages to state whenever a bot is killed. */
	std::vector<SD::DString> DeathMessages;

	/* A list of taunts to use whenever a bot makes a kill. */
	std::vector<SD::DString> KillMessages;

	std::vector<SD::DString> EliminationMessages;

	std::vector<SPendingMessage> PendingMessages;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Have the given bot announce one of the greetings.
	 */
	virtual void SayGreetingFrom (PlayerSoul* soul, PlayerSoul* humanSoul);

	/**
	 * Pick either a death or taunt message based on the interaction.
	 */
	virtual void SayDeathMessage (PlayerSoul* killed, PlayerSoul* killer, const Weapon* killedBy);

	/**
	 * Say a message related to the given soul that was just eliminated.
	 */
	virtual void SayEliminationMessage (PlayerSoul* killed);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void AddGreetings (const std::vector<SD::DString>& newGreetings);
	virtual void AddDeaths (const std::vector<SD::DString>& newDeaths);
	virtual void AddTaunts (const std::vector<SD::DString>& newTaunts);
	virtual void AddEliminationMsgs (const std::vector<SD::DString>& newEliminationMsgs);


	/*
	=====================
	  Implemenation
	=====================
	*/

protected:
	/**
	 * Decorates the given message to insert the author's name at the beginning.
	 */
	virtual void InsertAuthorName (PlayerSoul* author, SD::DString& outMsg);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::FLOAT deltaSec);
};
DD_END