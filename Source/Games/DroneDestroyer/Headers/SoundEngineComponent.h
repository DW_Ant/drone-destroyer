/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SoundEngineComponent.h
  An Engine component responsible for setting up and playing sounds.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class MusicPlayer;
class SoundPlayer;

class SoundEngineComponent : public SD::EngineComponent
{
	DECLARE_ENGINE_COMPONENT(SoundEngineComponent)
		

	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The SD::Engine index where this component resides in. */
	static const size_t SOUND_ENGINE_IDX;

	/* Various unique names for the sounds imported. Used for quickly accessing the sounds that are reused across multiple Entities in this game. */
	static const SD::DString SOUND_CLICK_HEAVY;
	static const SD::DString SOUND_CLICK_LIGHT;
	static const SD::DString SOUND_CLICK_NORMAL;
	static const SD::DString SOUND_TYPING;


protected:
	/* The player responsible for playing all requested sounds. */
	SD::DPointer<SoundPlayer> Player;

	SD::DPointer<MusicPlayer> Music;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	SoundEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void ShutdownComponent () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	SoundPlayer* GetPlayer () const;
	MusicPlayer* GetMusic () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ImportGameSounds ();
};
DD_END