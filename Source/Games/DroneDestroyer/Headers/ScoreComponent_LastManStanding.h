/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScoreComponent_LastManStanding.h

  A component that adds lives and kills to the score components.
=====================================================================
*/

#pragma once

#include "ScoreComponent.h"

DD_BEGIN
class PlayerSoul;

class ScoreComponent_LastManStanding : public ScoreComponent
{
	DECLARE_CLASS(ScoreComponent_LastManStanding)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::LabelComponent* LivesLabel;
	SD::LabelComponent* KillsLabel;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void UpdateInformation (SD::INT rank, PlayerSoul* soul) override;
	virtual void SetFontSize (SD::INT newFontSize) override;

protected:
	virtual void InitializeComponents () override;
};
DD_END