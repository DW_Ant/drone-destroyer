/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScoreComponent.h

  A component responsible for displaying the player's name and game relevant score attributes.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class PlayerSoul;

class ScoreComponent : public SD::GuiComponent
{
	DECLARE_CLASS(ScoreComponent)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Font size to use for all labels for this component. */
	SD::INT FontSize;

	SD::FrameComponent* Frame;
	SD::LabelComponent* RankLabel;
	SD::LabelComponent* PlayerNameLabel;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void InitializeComponents () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Updates all internal label components to reflect the given soul's current information.
	 */
	virtual void UpdateInformation (SD::INT rank, PlayerSoul* soul);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetFontSize (SD::INT newFontSize);


	/*
	=====================
	  Accessor
	=====================
	*/

public:
	DEFINE_ACCESSOR(SD::INT, FontSize);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Determines the background color based on the soul this component is displaying.
	 */
	virtual SD::Color GetFrameColor (SD::INT rank, PlayerSoul* soul);
};
DD_END