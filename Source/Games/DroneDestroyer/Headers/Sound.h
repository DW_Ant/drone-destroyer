/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Sound.h
  An object responsible for storing a reference to a sfml sound resource.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class Sound : public SD::Object
{
	DECLARE_CLASS(Sound)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Directory where sound files are found. */
	static const SD::Directory SOUND_DIRECTORY;

	SD::DString SoundName;

protected:
	sf::SoundBuffer* Buffer;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual SD::DString GetFriendlyName () const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	virtual void Release ();
	virtual void SetResource (sf::SoundBuffer* newBuffer);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual SD::DString GetSoundName () const;

	/**
	 * Returns the SFML's sound resource.
	 */
	virtual const sf::SoundBuffer* GetBuffer () const;
};
DD_END