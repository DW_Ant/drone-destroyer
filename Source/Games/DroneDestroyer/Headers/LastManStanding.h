/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LastManStanding.h

  Game where everyone fights to the death. Players can continue to respawn until they are
  out of lives. As the name implies, the last player with lives is the victor.
=====================================================================
*/

#pragma once

#include "GameRules.h"

DD_BEGIN
class HudLastManStanding;
class TimeLimitComponent;

class LastManStanding : public GameRules
{
	DECLARE_CLASS(LastManStanding)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The maximum number of lives all new players start with. */
	SD::INT NumLives;

	/* The maximum duration this match could last before it goes into sudden death (in seconds).
	If not positive, then there is no limit. */
	SD::FLOAT TimeLimit;

	/* Component responsible for keeping time. */
	TimeLimitComponent* TimeComponent;

	/* Component on the hud that will be updated whenever the scores have changed.
	A pub/sub module would be nice to update this rather than having a cached pointer. */
	SD::DPointer<HudLastManStanding> HudScore;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool CanBeSelected () const override;
	virtual void ApplyGameSettings (const std::vector<SD::DString>& newSettings) override;
	virtual void InitializeGame () override;
	virtual void AddComponentsToHud (Hud* playerHud) override;
	virtual void ScoreKill (Character* killed, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg) override;
	virtual void UpdateScores () override;
	virtual void SortScores () override;

protected:
	virtual const Scoreboard* GetScoreboardClass () const override;
	virtual bool CanCreateBody (PlayerSoul* requester) override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through all players. Anyone that's not tied with the lead are automatically eliminated.
	 * The survivors are narrowed down to their last life.
	 */
	virtual void SuddenDeath ();

	/**
	 * Executes the end game sequence whenever a victor is determined.
	 */
	virtual void CheckEndGame ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTimeLimitExpired ();
};
DD_END