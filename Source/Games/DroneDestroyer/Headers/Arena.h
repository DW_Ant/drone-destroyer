/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Arena.h

  Entity responsible for instantiating and managing a list of Entities that make up the environment.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class GameRules;
class NavigationComponent;
class Inventory;

class Arena : public SD::Entity, public SD::SceneTransform
{
	DECLARE_CLASS(Arena)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The size range randomization variation for corpses relative to the base character size where minimum is the smallest possible multiplier to apply, and
	max is the largest possible multiplier. */
	SD::Range<SD::FLOAT> HumanCorpseSizeVariation;

protected:
	/* The first NavigationComponent in the Navigation linked list. */
	NavigationComponent* FirstNavigation;

	/* List of environmental entities to instantiate in the map. */
	std::vector<SD::Entity*> ArenaEntities;

	/* Component responsible for rendering human corpses. */
	SD::InstancedSpriteComponent* HumanCorpses;
	SD::InstancedSpriteComponent* DroneCorpses;

	/* Maximum number of corpses allowed before it starts clearing old corpses. If negative, then there isn't a limit. */
	SD::INT MaxNumHumanCorpses;

	/* The index value of the human corpse vector that informs the Arena which corpse is the most recent. */
	SD::INT LatestHumanCorpseIndex;
	SD::INT LatestDroneCorpseIndex;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Obtains numerous properties from the config fie based on the configuration key passed into it.
	 * This essentially pulls map meta data without loading a map instance.
	 */
	static void GetArenaMetaData (const SD::DString& mapName, SD::DString& outPreviewImage, SD::Range<SD::INT>& outRecPlayers, SD::DString& outAuthorName, SD::DString& outDescriptionKey);

	/**
	 * Returns true if this Arena is allowed to be played in the given GameRules.
	 */
	virtual bool IsAllowedFor (GameRules* rules) const;

	/**
	 * Traces against all fixtures from world geometry objects (ignores projectiles, pickups, and character collision).
	 * Returns true if the line collided against something.
	 */
	virtual bool ArenaTrace (const b2RayCastInput& input, b2RayCastOutput* impactPoint) const;

	/**
	 * Returns true if the line segment collided with something against the world geometry.
	 * This function is faster since it doesn't need to obtain the closest impact point.
	 */
	virtual bool FastTrace (const SD::Vector3& start, const SD::Vector3& end) const;

	/**
	 * Adds a corpse at the specified location.
	 */
	virtual void AddCorpse (SD::FLOAT characterRadius, const SD::Vector3& corpseLocation);
	virtual void AddDroneCorpse (SD::FLOAT characterRadius, const SD::Vector3& corpseLocation);

	/**
	 * Creates a series of Entities that make up the environment. Each instantiated object is expected to
	 * register themselves to the ArenaEntities vector for clean up purposes.
	 */
	virtual void InitializeArenaObjects (const SD::DString& arenaName);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetFirstNavigation (NavigationComponent* newFirstNavigation);
	virtual void SetMaxNumHumanCorpses (SD::INT newMaxNumHumanCorpses);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(NavigationComponent*, FirstNavigation);

	const std::vector<SD::Entity*>& ReadArenaEntities () const
	{
		return ArenaEntities;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Creates and initializes an Entity that acts like a path node for the navigation network.
	 * This will also automatically register the node to the vector of ArenaEntities.
	 *
	 * @param nodeLocation - The absolute translation where the center of this node is found.
	 * @param connectionRadius - Specifies how far this node may reach when attempting to make connections with other nodes. Uses defaults if negative.
	 * @param wanderRadius - Informs the bots how close they need to stand in order to consider they reached this node's destination. Uses defaults if negative.
	 * Returns the newly created Entity.
	 */
	virtual SD::SceneEntity* CreatePathNode (const SD::Vector3& nodeLocation, SD::FLOAT connectionRadius, SD::FLOAT wanderRadius);

	/**
	 * Creates a pickup station at the specified location where it spawns the given item.
	 */
	virtual void CreatePickup (const SD::Vector3& location, const Inventory* pickupCdo);

	/**
	 * Utility to create a rectangular static entity with a collision and render component.
	 * Rotation yaw is in degrees.
	 */
	virtual SD::SceneEntity* CreateStaticObject (const SD::Vector3& location, const SD::Vector3& size, SD::FLOAT yaw, const SD::Texture* spriteTexture);

	/**
	 * Creates a static entity that's simply a black box.
	 */
	virtual SD::SceneEntity* CreateBlackBox (const SD::Vector3& location, const SD::Vector3& size, SD::FLOAT yaw);

	/**
	 * Utility to create a single small noncolliding decorative object to the given instanced sprite component.
	 */
	virtual void CreateSmallDeco (SD::InstancedSpriteComponent* spriteComp, const SD::Vector2& location, SD::FLOAT locationVariation, SD::FLOAT size, SD::FLOAT sizeVariation, bool randRotation) const;

	virtual void AddCorpseImplementation (SD::FLOAT characterRadius, const SD::Vector3& corpseLocation, SD::InstancedSpriteComponent* spriteComp, size_t corpseIdx, int corpseSubDivideX) const;
};
DD_END