/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanel_GameRules.h

  A panel that allows the user to specify the score limits, map, and modifiers
  for the next game.
=====================================================================
*/

#pragma once

#include "MenuPanelComponent.h"

DD_BEGIN
class Arena;
class GameModifier;

class MenuPanel_GameRules : public MenuPanelComponent
{
	DECLARE_CLASS(MenuPanel_GameRules)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SMapSelection
	{
		SD::ButtonComponent* Button;
		SD::DString MapName;

		SMapSelection (SD::ButtonComponent* inButton, const SD::DString& inMapName) :
			Button(inButton),
			MapName(inMapName)
		{
			//Noop
		}
	};

	struct SGameModifier
	{
		SD::CheckboxComponent* Checkbox;
		const GameModifier* Modifier;

		SGameModifier (SD::CheckboxComponent* inCheckbox, const GameModifier* inModifier) :
			Checkbox(inCheckbox),
			Modifier(inModifier)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const SD::INT MAX_NUM_BOTS;

protected:
	/* Index of the MapSelection vector that points to the current selected map. */
	size_t SelectedMapIdx;

	/* Index of the DifficultyButtons that points to the current selected difficulty. */
	size_t SelectedDifficultyIdx;

	std::vector<SMapSelection> MapSelection;

	SD::SpriteComponent* MapPreview;
	SD::LabelComponent* MapAuthor;
	SD::LabelComponent* RecommendedNumPlayers;
	SD::LabelComponent* MapDescription;
	
	SD::GuiEntity* MapListEntity;
	SD::GuiEntity* MapDescriptionEntity;
	SD::GuiEntity* GameRulesEntity;

	SD::FrameComponent* RulesSection;
	SD::FLOAT RulesSectionAbsHeight;

	/* List of text field components that correspond to a particular game rule specific setting. The order of the fields must be the same as the GameRules option list. */
	std::vector<SD::LabelComponent*> GameRulesPrompts;
	std::vector<SD::TextFieldComponent*> GameRulesOptions;
	SD::ScrollbarComponent* GameRulesOptionsScrollbar;

	SD::TextFieldComponent* NumBots;

	/* List of buttons that specifies the selected difficulty. Sorted from Pacifist to hard. */
	std::vector<SD::ButtonComponent*> DifficultyButtons;

	std::vector<SGameModifier> Modifiers;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void RestorePanel () override;
	virtual void SavePanelState () override;

protected:
	virtual void InitializeComponents () override;
	virtual bool ProcessMouseClick_Implementation (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType, bool consumedEvent) override;
	virtual void Destroyed () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Populates the vector with all available maps for the chosen game type.
	 * The list is sorted by map name alphabetical order.
	 */
	virtual void GenerateMapList (std::vector<SD::DString>& outMapList) const;

	/**
	 * Populates the vector with all available GameModifiers then sorts them in alphabetical order.
	 */
	virtual void GenerateModifierList (std::vector<const GameModifier*>& outModifiers) const;

	/**
	 * Changes the preview and description to display the map information.
	 */
	virtual void SelectMap (const SD::DString& selectedMapName);

	virtual void SelectDifficulty (SD::ButtonComponent* uiComponent);

	/**
	 * Iterates through all game modifiers to see if they are permitted based on the game configuration.
	 */
	virtual void EvaluateGameModifierEnableness ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleAllowTextInput (const SD::DString& txt);
	virtual void HandleTextReturn (SD::TextFieldComponent* uiComponent);
	virtual void HandleMapButtonReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleDifficultyButtonReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleGameModChecked (SD::CheckboxComponent* uiComponent);
	virtual void HandleBackButtonReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleNextButtonReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleTickCheckSectionHeight (SD::FLOAT deltaSec);
};
DD_END