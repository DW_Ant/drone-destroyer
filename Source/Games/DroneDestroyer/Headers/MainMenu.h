/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MainMenu.h

  A GuiEntity that handles the menu flow (pushing and removing panels).
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class MenuPanelComponent;

class MainMenu : public SD::GuiEntity
{
	DECLARE_CLASS(MainMenu)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of MenuPanels that are pushed to the stack. Pressing the back or escape key destroys the previous
	panel and brings the last item in the stack to be displayed. */
	std::vector<MenuPanelComponent*> Panels;


	/*
	=====================
	  Inherited
	=====================
	*/

protected:
	virtual void ConstructUI () override;
	virtual bool HandleInput (const sf::Event& evnt) override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Removes the last panel on the stack and displays the panel the user visited most recently.
	 */
	virtual void NavigateBack ();

	/**
	 * Hides the current panel. Then it will create a panel of matching class, and initialize that panel.
	 * @param menuPanelCdo - The class default object of the menu panel class to create a copy of for this menu.
	 * Returns the instance of the panel that was created.
	 */
	virtual MenuPanelComponent* PushPanel (const MenuPanelComponent* menuPanelCdo);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline MenuPanelComponent* GetCurrentPanel () const
	{
		if (!SD::ContainerUtils::IsEmpty(Panels))
		{
			return SD::ContainerUtils::GetLast(Panels);
		}

		return nullptr;
	}
};
DD_END