/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HealthPickup.h

  A simple pickup that heals the taker a bit.
=====================================================================
*/

#pragma once

#include "Inventory.h"

DD_BEGIN
class HealthPickup : public Inventory
{
	DECLARE_CLASS(HealthPickup)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The total amount of health this pickup may provide. */
	SD::INT HealAmount;
	SD::INT DefaultHealAmount;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool CanPickupItem (Character* takerCandidate) const override;
	virtual SD::INT GetImportanceValue (Character* possibleTaker) const override;
	virtual void GiveTo (Character* taker) override;


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetHealAmount (SD::INT newHealAmount);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(SD::INT, HealAmount);
};
DD_END