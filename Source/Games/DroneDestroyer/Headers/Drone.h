/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Drone.h

  A Character that overrides much of the Character components so that they consume less resources.
  This Character does not update its SceneTransform. Instead it uses is simplified transforms.
  This Character does not have a render component. Instead, it's merely a particle in the Drone SpriteInstances.
  This Character does not have a PhysicsComponent. It uses its simplified math for its physics where it has no collision,
	nor does it use Box2D.

  Drones don't have souls.
=====================================================================
*/

#pragma once

#include "Character.h"
#include "GameRules.h"

DD_BEGIN
class Drone : public Character
{
	DECLARE_CLASS(Drone)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* SubDivisionX index in the drone sprite to use when rendering this Drone. This must be initialized before BeginObject is called. */
	SD::INT SpriteSubDivisionIdx;

	/* The index to the Sprite Instance in the Extinction DroneSprites component that represents this drone. */
	size_t SpriteIdx;

private:
	/* Quick access to the sprite instance component so that the drones don't have to find this every frame. */
	SD::DPointer<SD::InstancedSpriteComponent> DroneSprite;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual void InitializePhysics () override;
	virtual bool IsFriendlyTo (Character* other) const override;
	virtual SD::FLOAT GetCollisionRadius () const override;
	virtual void Kill (SD::INT damageAmount, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg) override;
	virtual void LeaveCorpse (Arena* environment) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Adjusts various parameters to adjust this drone's strength.
	 */
	virtual void AdjustDroneStrength (GameRules::EGameDifficulty skillLevel, SD::INT droneHealth, SD::FLOAT maxVelocity, const SD::DClass* weaponClass);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSpriteIdx (size_t newSpriteIdx);
	virtual void SetDroneSprite (SD::InstancedSpriteComponent* newDroneSprite);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(size_t, SpriteIdx);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Updates the vertices to reflect the drone's current rotation and location.
	 */
	virtual void UpdateVertices ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::FLOAT deltaSec);
};
DD_END