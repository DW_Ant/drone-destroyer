/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DroneDestroyerEngineComponent.h
  The EngineComponent that manages game-specific entities and art assets.
=====================================================================
*/

#pragma once

#include "Arena.h"
#include "DroneDestroyer.h"
#include "GameRules.h"
#include "MainMenu.h"

DD_BEGIN
class Character;
class Hud;
class SoundMessenger;
class UserSettings;

class DroneDestroyerEngineComponent : public SD::EngineComponent
{
	DECLARE_ENGINE_COMPONENT(DroneDestroyerEngineComponent)
		

	/*
	=====================
	  Properties
	=====================
	*/

public:
#ifdef DEBUG_MODE
	/* The color transparency for the debugging PhysicsRender components. */
	static const int DebugPhysicsRenderAlpha;
#endif

	/* Various Z-position constants used for determining draw order. */
	static const SD::FLOAT DRAW_ORDER_MAP;
	static const SD::FLOAT DRAW_ORDER_PICKUPS;
	static const SD::FLOAT DRAW_ORDER_CHARACTERS;
	static const SD::FLOAT DRAW_ORDER_DRONES;
	static const SD::FLOAT DRAW_ORDER_PROJECTILES;
	static const SD::FLOAT DRAW_ORDER_DEBUG_SHAPES;
	static const SD::FLOAT DRAW_ORDER_CAMERA;

protected:
	/* Collection of settings that are stored in a config file. Cached here to be easily accessed by any Entity in this game. */
	SD::DPointer<UserSettings> Settings;

	/* The UI menu that's responsible for setting up the game. */
	SD::DPointer<MainMenu> Menu;

	SD::DPointer<Hud> PlayerHud;

	/* The Draw Layer used to render the environment and Entities in that environment. */
	SD::DPointer<SD::SceneDrawLayer> MainDrawLayer;
	SD::DPointer<SD::TopDownCamera> MainCamera;

	/* The default zoom multiplier when MainCamera was original instantiated. */
	SD::FLOAT DefaultCamZoom;

	/* The name of the arena to load whenever the game launches. */
	SD::DString PendingArenaName;

	/* The actual instance of the Arena being played. */
	SD::DPointer<Arena> Environment;

	/* Current game this is running. Becomes null if a game is not running. */
	SD::DPointer<GameRules> Game;

	/* Becomes true if the game is paused. */
	bool PausingGame;

	/* Entity responsible for sending sound requests to the external thread. */
	SD::DPointer<SoundMessenger> SoundSender;

	/* Hearing radius of the camera where any sound played within the inner radius is at max volume. The volume lerps to zero the closer it approaches to the outer radius. */
	SD::FLOAT InnerHearingRadius;
	SD::FLOAT OuterHearingRadius;


	/*
	=====================
	  Constructors
	=====================
	*/

public:
	DroneDestroyerEngineComponent ();


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void PreInitializeComponent () override;
	virtual void InitializeComponent () override;
	virtual void PostInitializeComponent () override;
	virtual void ShutdownComponent () override;

protected:
	virtual std::vector<const SD::DClass*> GetPreInitializeDependencies () const override;
	virtual std::vector<const SD::DClass*> GetInitializeDependencies () const override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Quick utility function to register a render component to the MainDrawLayer in the local thread.
	 */
	static void RegisterComponentToMainDrawLayer (SD::RenderComponent* component);

	/**
	 * Creates and initializes a soul using those properties. The newly created soul will be stored in the game's soul linked list.
	 */
	virtual void CreateSoul (const SD::DString& playerName, SD::INT teamNum, bool isSpectator, bool isPlayer);

	/**
	 * Transitions the game state from the menu to gameplay using the current game settings.
	 */
	virtual void LaunchGame ();

	/**
	 * Destroys all gameplay Entities and the Arena before it returns back to the title screen.
	 */
	virtual void ReturnToTitleScreen ();

	/**
	 * Either pauses or unpauses the game based on the flag passed in.
	 */
	virtual void SetPausedGame (bool newPaused);
	virtual void TogglePause ();

	/**
	 * Plays the given sound file and adjusts its volume multiplier based on the distance from the main camera.
	 */
	virtual void PlayLocalSound (const SD::DString& soundName, const SD::Vector3& soundOrigin, SD::FLOAT volMultiplier);
	static void SPlayLocalSound (const SD::DString& soundName, const SD::Vector3& soundOrigin, SD::FLOAT volMultiplier);


	/*
	=====================
	  Mutators
	=====================
	*/

	/**
	 * Creates an instance of the game rules of matching class. It does not initialize it yet since
	 * it's expecting the menu to process through the game settings first.
	 */
	virtual void SetGameRules (const GameRules* cdo);

	/**
	 * Sets the arena to be constructed whenever the game launches.
	 */
	virtual void SetPendingArena (const SD::DString& newMapName);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual UserSettings* GetSettings () const
	{
		return Settings.Get();
	}

	virtual SD::TopDownCamera* GetMainCamera () const
	{
		return MainCamera.Get();
	}

	inline Hud* GetPlayerHud () const
	{
		return PlayerHud.Get();
	}

	virtual SD::SceneDrawLayer* GetMainDrawLayer () const
	{
		return MainDrawLayer.Get();
	}

	virtual Arena* GetEnvironment () const
	{
		return Environment.Get();
	}

	virtual GameRules* GetGame () const
	{
		return Game.Get();
	}

	inline bool IsPausingGame () const
	{
		return PausingGame;
	}

	inline SoundMessenger* GetSoundSender () const
	{
		return SoundSender.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void ImportGameAssets ();

	virtual void ApplyUserSettings ();

	/**
	 * Creates and initializes the external thread responsible for running the SoundEngineComponent.
	 * Returns true if the external Engine is successfully created and ready to go!
	 */
	virtual bool LaunchSoundThread ();

	/**
	 * Destroys all gameplay related Entities.
	 */
	virtual void CleanupGameInstance ();
};
DD_END