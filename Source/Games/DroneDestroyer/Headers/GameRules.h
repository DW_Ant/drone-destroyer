/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GameRules.h

  Entity that determines how the game proceeds. Manages player souls and win/loss conditions.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class BotDialectComponent;
class Character;
class GameModifier;
class Hud;
class PlayerSoul;
class Projectile;
class Scoreboard;

class GameRules : public SD::Entity
{
	DECLARE_CLASS(GameRules)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	enum EGameDifficulty
	{
		Unknown, //Not set
		Pacifist, //Throws their weapons away. Always walking. Ignores enemies and pickups.
		Novice, //Bots run at half speed (always walking). Bots are stationary when attacking. They spin slowly.
		Easy, //Bots always walk, but they move around when attacking. They spin slowly.
		Medium, //Bots move at moderate speed (jogging). They spin moderately. Looks for items during combat. Leads projectiles.
		Hard //Bots move as fast as the player. They spin very fast. Looks for items during combat. Leads projectiles. Extended vision.
	};


	/*
	=====================
	  Structs
	=====================
	*/

public:
	/* Data structure that has the information the menu panel needs to construct the Game Rules option section. */
	struct SGameRulesOption
	{
		SD::DString PromptKey;
		SD::INT DefaultValue;

		SGameRulesOption (const SD::DString& inPromptKey, SD::INT inDefaultValue) :
			PromptKey(inPromptKey),
			DefaultValue(inDefaultValue)
		{
			//Noop
		}
	};

private:
	/* Collection of variables used for calculating the end game camera zoom. */
	struct SCamZoomData
	{
		SD::FLOAT StartZoom;
		SD::FLOAT EndZoom;

		/* How long the game should wait before it starts zooming out. */
		SD::FLOAT ZoomDelay;

		/* How long it takes to zoom from start to end (in seconds). */
		SD::FLOAT ZoomTime;
		
		/* Timestamp when the game has finished. */
		SD::FLOAT EndGameTime;

		/* Tick component that's responsible for updating the camera zoom continuously. */
		SD::TickComponent* Tick;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Name of the image to use when displaying the icon of this gametype in the menu. */
	SD::DString IconTextureName;

	/* Localization key to set the game description. */
	SD::DString DescriptionKey;
	mutable SD::DString Description;

	/* Configuration key used to obtain and save menu states that are specific to these GameRules. */
	SD::DString ConfigName;

	/* First player in the linked list to the player list. */
	PlayerSoul* FirstPlayer;

	/* List of souls in this game that are sorted by scoring rank (where element 0 is the leading player).
	This list does not include everyone (excludes spectators for example). */
	std::vector<SD::DPointer<PlayerSoul>> RankedPlayers;

	EGameDifficulty GameDifficulty;

	/* Specifies the number of bots that should run in this game instance. */
	SD::INT NumBots;

	/* Component that'll be adding extra bot interaction messages to the player's hud. */
	BotDialectComponent* BotDialect;

	/* Maximum number of bots that can be present in the game before the BotDialect component is not added. It's not added for crowded games. */
	SD::INT MaxBotsForDialect;

	/* If true, then the bots will taunt or reply when making kills. */
	bool UseStandardBotDialectKillMsgs;

	/* If true, then the bots will use the standard greetings at map start. */
	bool UseStandardGreetings;

	/* If true, then all players are requires to respawn after slain. */
	bool ForceRespawn;

	/* List of settings to display in the game rules menu. */
	std::vector<SGameRulesOption> GameSettings;

	std::vector<GameModifier*> GameModifiers;

	Scoreboard* ScoreboardInstance;

	/* Becomes true if the match has ended. */
	bool HasEnded;

private:
	SCamZoomData EndGameCamZoom;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns the human readable name for the given difficulty setting.
	 */
	static SD::DString GetDifficultyName (EGameDifficulty difficulty);

	/**
	 * Returns true if this GameRules class can appear in the menu selection.
	 */
	virtual bool CanBeSelected () const;

	/**
	 * Returns the number of active teams that are configured for this game.
	 * If less than 1, then it assumes a free for all game.
	 */
	virtual SD::INT GetNumTeams () const;

	/**
	 * Invoked from the menu panel, this actually applies this game instance's settings.
	 * The order of the strings are in the same order as the GameSettings vector.
	 */
	virtual void ApplyGameSettings (const std::vector<SD::DString>& newSettings);

	/**
	 * Creates an instance of the given game modifier CDO, and registers it to the game modifier list.
	 */
	virtual void AddGameModifier (const GameModifier* gameModCdo);

	virtual void DestroyGameModifiers ();

	/**
	 * Invoked after the variables are set. This function launches the game functionality.
	 */
	virtual void InitializeGame ();

	/**
	 * Allows the game to add specific components to the given hud.
	 */
	virtual void AddComponentsToHud (Hud* playerHud);

	/**
	 * Registers the new soul to the soul linked list.
	 */
	virtual void RegisterNewSoul (PlayerSoul* newSoul);

	virtual void AddSpectator (PlayerSoul* newSpectator);

	/**
	 * Iterates through the linked list to remove this soul from it.
	 */
	virtual void RemoveSoul (PlayerSoul* oldSoul);

	/**
	 * Creates and initializes a new body for the soul to possess.
	 */
	virtual Character* CreateCharacter (PlayerSoul* soul);

	/**
	 * Informs the game that there was a kill.
	 */
	virtual void ScoreKill (Character* killed, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetGameDifficulty (EGameDifficulty newGameDifficulty);
	virtual void SetNumBots (SD::INT newNumBots);

	inline const std::vector<GameModifier*>& ReadGameModifiers () const
	{
		return GameModifiers;
	}


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	const SD::DString& ReadIconTextureName () const
	{
		return IconTextureName;
	}

	virtual const SD::DString& ReadDescription () const;

	const SD::DString& ReadConfigName () const
	{
		return ConfigName;
	}

	PlayerSoul* GetFirstPlayer () const
	{
		return FirstPlayer;
	}

	const std::vector<SD::DPointer<PlayerSoul>>& ReadRankedPlayers () const
	{
		return RankedPlayers;
	}

	DEFINE_ACCESSOR(EGameDifficulty, GameDifficulty);
	DEFINE_ACCESSOR(SD::INT, NumBots);
	DEFINE_ACCESSOR(bool, ForceRespawn);

	const std::vector<SGameRulesOption>& ReadGameSettings () const
	{
		return GameSettings;
	}

	DEFINE_ACCESSOR(Scoreboard*, ScoreboardInstance);
	DEFINE_ACCESSOR(bool, HasEnded);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Scoreboard UI class to use when this game is launched.
	 */
	virtual const Scoreboard* GetScoreboardClass () const;

	/** 
	 * Populates the bot dialect options.
	 */
	virtual void InitializeBotDialect ();

	/**
	 * Returns true if the specified soul is allowed to possess a new body.
	 */
	virtual bool CanCreateBody (PlayerSoul* requester);

	/**
	 * Finds the appropriate location and rotation to spawn the character in.
	 * Returns true if there's a valid location to place a character.
	 */
	virtual bool FindSpawnLocation (Character* body, PlayerSoul* soul, SD::Vector3& outLocation, SD::Rotator& outRotation) const;

	/**
	 * Initializes the character, and gives them starting equipment.
	 */
	virtual void GiveDefaultInventoryTo (Character* newCharacter);

	/**
	 * Returns true if the game should broadcast a death message for the given character.
	 */
	virtual bool AllowDeathMessageFor (Character* killed) const;

	/**
	 * Updates the RankedPlayer list and updates the scoreboard if any.
	 */
	virtual void UpdateScores ();

	/**
	 * Iterates through each active participant and sorts them by their scores.
	 * The subclasses are expected to store the results of the sorted list in the RankedPlayers vector.
	 */
	virtual void SortScores ();

	/**
	 * Executes the victory sequence where it pauses the game, splashes the winner's name, then shows the scoreboard.
	 * If the camera location is not at the origin, it'll snap the main camera at that location.
	 */
	virtual void ExecuteVictorySequence (const SD::DString& endGameMsg, const SD::Vector3& camLocation);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleCamZoomTick (SD::FLOAT deltaSec);
	virtual void HandleEndGameMsgFadeOutFinished ();
};
DD_END