/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BotCharacterComponent.h

  Component that informs the bots how to navigate around, shoot, and move while shooting.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"
#include "Character.h"
#include "GameRules.h"

DD_BEGIN
class NavigationComponent;
class Projectile;

class BotCharacterComponent : public SD::EntityComponent
{
	DECLARE_CLASS(BotCharacterComponent)


	/*
	=====================
	  Enums
	=====================
	*/

protected:
	enum eBotState
	{
		BS_Roaming,
		BS_Combat
	};


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SNavigationCandidateInfo
	{
		/* Candidates of higher priority are chosen over others. */
		SD::INT Priority;

		NavigationComponent* NavPoint;

		SNavigationCandidateInfo (SD::INT inPriority, NavigationComponent* inNavPoint) :
			Priority(inPriority),
			NavPoint(inNavPoint)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Determines how far this bot can see when detecting hostiles. */
	SD::FLOAT SightRadius;

	/* Determines how far this bot can see when looking for important pickups during combat. */
	SD::FLOAT CombatSightRadius;

	/* Determines how far this bot can see when looking for places to go. */
	SD::FLOAT NavigationSightRadius;

	/* How fast may this bot spin (in degrees per second). */
	SD::FLOAT RotationRate;

protected:
	/* Game difficulty that determines how well this bot performs. */
	GameRules::EGameDifficulty SkillLevel;

	eBotState BotState;

	/* If true, then this bot will prioritize pickups when looking for a new destination. */
	bool PrioritizePickupsWhenRoaming;

	/* How frequently should this bot evaluate the direction they're headed (in seconds). */
	SD::FLOAT ProcessNavigationInterval;

	/* When engaged in combat, the bot will evaluate their movement and target this many seconds. */
	SD::FLOAT ProcessCombatInterval;

	/* How frequently should this bot check their visual range for hostiles (in seconds). */
	SD::FLOAT ProcessPerceptionInterval;

	/* Number of consecutive times for the Enemy to remain obscure before this bot forgets about them. */
	SD::INT ObscuredEnemyBeforeForgetting;

	/* If true, then this bot would conduct a line trace from self to their current enemy to see if they're obscured or not.
	Otherwise, this bot will attempt to attack the enemy regardless if they're behind cover or not. */
	bool CheckForObscuredEnemies;

	/* When strafing while fighting someone, how often should this bot change directions (in seconds). */
	SD::Range<SD::FLOAT> CombatDodgeInterval;

	SD::DPointer<Character> OwningBody;

	/* The point in the map this bot is heading towards. Falls back to this destination if ImmediateDestination is empty. */
	SD::Vector3 DesiredDestination;

	/* How close the bot must stand close to the destination before it considers that it reached that spot. */
	SD::FLOAT NavigationReach;

	/* The priority value of the current NavigationComponent this bot is headed towards. If zero, then this bot is not running towards a NavigationComponent. */
	SD::INT DestinationPriority;

	/* The DestinationPriority value when this bot is performing a dodge. This should be something distinct that inventory pickups won't set to. */
	static const SD::INT DODGE_PRIORITY;

	/* The character this bot is currently fighting. */
	SD::DPointer<Character> Enemy;

	/* How fast should this bot move when roaming. */
	SD::FLOAT RoamSpeedMultiplier;

	/* How fast should this bot move when fighting. */
	SD::FLOAT CombatSpeedMultiplier;

#ifdef DEBUG_MODE
	SD::SceneEntity* DesiredLocationDebugger;
#endif

	/* List of destinations this bot traversed to recently (prevent bots from going back and forth). */
	std::vector<SD::Vector3> PastDestinations;

	/* Index in PastDestinations that points to the most recent destination. */
	size_t PastDestinationIdx;

private:
	/* Maximum yaw difference in order for this bot to look at a target to be considered that they're actually looking at it in degrees. */
	SD::FLOAT LookAtThreshold;

	/* The number of consecutive times the Enemy was obscured. */
	SD::INT ObscuredEnemyCounter;

	/* Earliest possible timestamp when this bot can change directions to dodge while fighting something. */
	SD::FLOAT CombatNextDodgeTime;

	/* The bot location last time the stuck check was called. */
	SD::Vector3 LastStuckLocation;

	/* Current counter how many times this bot was found in the stuck location. */
	SD::INT StuckCounter;

	/* Maximum number the stuck counter much reach to this value before it'll interrupt destinations. */
	SD::INT MaxStuckCounter;

	/* How frequently this bot will check if they're stuck (in seconds). The bot must fail multiple times in a row to find a new spot. */
	SD::FLOAT StuckCheckInterval;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

	/**
	 * Returns true if this bot has the resources to fight others (eg: Must have a weapon with ammo).
	 */
	virtual bool IsAbleToFight () const;

	/**
	 * Tells this bot to start attacking the specified target regardless if the bot is roaming or fighting someone else.
	 */
	virtual void Attack (Character* newEnemy);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSkillLevel (GameRules::EGameDifficulty newSkillLevel);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	inline Character* GetEnemy () const
	{
		return Enemy.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Checks the bot's movement direction and adjusts the direction in case their destination or their current position has changed.
	 */
	virtual void ProcessNavigation ();

	/**
	 * Checks if the bot is currently stuck or not. If the bot failed multiple times in a row, their current destination will change to something else.
	 */
	virtual void ProcessStuckCheck ();

	/**
	 * Evaluates the state of the current enemy. It figures out if it should forget the enemy, shoot, and dodge.
	 */
	virtual void ProcessCombat ();

	/**
	 * Looks at the characters around this bot to assess the situation. If there's another hostile more threatening
	 * to this bot, the bot may change targets to the greater threat.
	 */
	virtual void ProcessPerception ();

	/**
	 * Iterates through the roster and chooses a target that's within range.
	 */
	virtual Character* FindEnemy (GameRules* game, SD::FLOAT& outBestDistSquared);

	/**
	 * Invoked whenever this bot reached their DesiredDestination.
	 */
	virtual void ReachDestination ();

	/**
	 * Iterates through the navigation list to figure out where this bot should roam next.
	 * If prioritizePickups is true, then the bot will prioritize picking up items rather than picking a random spot.
	 */
	virtual void FindRoamDestination (bool prioritizePickups);

	/**
	 * Invoked whenever the bot is lost and they need to choose a new destination.
	 * This function must return a location where this bot should head.
	 */
	virtual SD::Vector3 NavigateFromLost ();

	/**
	 * Looks at the area around the bot and picks a direction that is somewhat perpendicular from
	 * Enemy's direction.
	 */
	virtual SD::Vector3 PerformDodge () const;

	/**
	 * Tells this bot to forget who they are attacking and return to roaming state.
	 */
	virtual void EndCombat ();

	/**
	 * Calculates the desired rotation for aiming and looking purposes.
	 */
	virtual void CalcRotation (SD::FLOAT deltaSec);
	

	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleNavigationTick (SD::FLOAT deltaSec);
	virtual void HandleStuckTick (SD::FLOAT deltaSec);
	virtual void HandleCombatTick (SD::FLOAT deltaSec);
	virtual void HandlePerceptionTick (SD::FLOAT deltaSec);
	virtual void HandleRotationTick (SD::FLOAT deltaSec);
	virtual void HandleTakeDamage (SD::INT damage, PlayerSoul* damagedBy, Projectile* damagedWith);
};
DD_END