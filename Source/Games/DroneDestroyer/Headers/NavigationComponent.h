/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  NavigationComponent.h

  A component that registers itself to the navigation list in the Arena.
  It maintains its own network of navigation components to help bots navigate around the environment.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class Arena;

class NavigationComponent : public SD::EntityComponent
{
	DECLARE_CLASS(NavigationComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Color to use when visualizing pathing lines. */
	static const SD::Color PATH_LINE_COLOR;

	/* If true, then the player can see debug pathing lines. */
	static bool DebugLinesVisible;

protected:
	/* The maximum distance this NavigationPoint may connect to. */
	SD::FLOAT MaxConnectionRadius;

	/* Bots don't have to strictly stand on this NavigationComponent. They can choose anywhere around this point's region.
	The circle radius of that region is determined by this value. */
	SD::FLOAT WanderRadius;

	/* List of NavigationComponents bots may traverse to from this component. */
	std::vector<NavigationComponent*> ConnectedPoints;

	/* Used for quickly iterating through all NavigationComponents in the linked list. The Arena holds the head point. */
	NavigationComponent* NextNavigation;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Iterates through the NavigationComponent list to figure out which components this component may link to.
	 * @param appendingConnections - If true, then this is a node that's simply adding itself to an existing network. It'll reciprocate the connections and it will not remove existing connections.
	 */
	virtual void BuildPathing (bool appendingConnections);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetMaxConnectionRadius (SD::FLOAT newMaxConnectionRadius);
	virtual void SetWanderRadius (SD::FLOAT newWanderRadius);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(SD::FLOAT, WanderRadius);
	DEFINE_ACCESSOR(NavigationComponent*, NextNavigation);

	const std::vector<NavigationComponent*>& ReadConnectedPoints () const
	{
		return ConnectedPoints;
	}

	virtual SD::SceneTransform* GetSceneTransform () const;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Returns true if the other NavigationComponent already has a connection to this component.
	 */
	virtual bool IsConnectedToThis (NavigationComponent* other) const;

	/**
	 * Severs this NavigationComponent from the network so that no NavigationComponent points to this.
	 * If pendingDestruction is true, this will not bother its own connections since this function assumes,
	 * this component is about to be destroyed anyways.
	 */
	virtual void SeverConnections (Arena* networkOwner, bool pendingDestruction);
};
DD_END