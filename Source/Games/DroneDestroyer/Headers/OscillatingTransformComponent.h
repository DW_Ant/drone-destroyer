/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  OscillatingTransformComponent.h

  A simple transform component that simply oscillates between two different translations.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class OscillatingTransformComponent : public SD::SceneTransformComponent
{
	DECLARE_CLASS(OscillatingTransformComponent)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The two translation points this component will oscillate between. */
	SD::Vector3 PrimaryPosition;
	SD::Vector3 SecondaryPosition;

protected:
	/* How many seconds it takes to complete a full wave/cycle. */
	SD::FLOAT CycleTime;

	SD::TickComponent* Tick;

private:
	/* To prevent every pickup from being in sync, this offset will randomize and distinguish one pickup from another. */
	SD::FLOAT RandomTimeOffset;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Starts/stops animating/adjusting this component's translation.
	 */
	virtual void SetOscillatingActive (bool isActive);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetCycleTime (SD::FLOAT newCycleTime);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(SD::FLOAT, CycleTime);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Calculates this transform's translation based on the engine's time.
	 */
	virtual void CalcNewTranslation ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::FLOAT deltaSec);
};
DD_END