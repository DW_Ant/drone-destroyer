/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Scoreboard_TeamDeathmatch.h

  A scoreboard entity that displays information specific for the TeamDeathmatch Game Rules.
=====================================================================
*/

#pragma once

#include "Scoreboard.h"

DD_BEGIN
class ScoreComponent_TeamTable;

class Scoreboard_TeamDeathmatch : public Scoreboard
{
	DECLARE_CLASS(Scoreboard_TeamDeathmatch)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* List of tables for each team. This vector assumes the same order as the game's team vector. */
	std::vector<ScoreComponent_TeamTable*> TeamTables;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void UpdatePlayerFields (GameRules* rules) override;

protected:
	virtual void InitializeGameLimitLabel () override;
	virtual void InitializeHeaderLabel (SD::FrameComponent* headerFrame) override;
	virtual void InitializeEntries (const SD::Vector2& startingPosition, const SD::Vector2& allowedSpace) override;
};
DD_END