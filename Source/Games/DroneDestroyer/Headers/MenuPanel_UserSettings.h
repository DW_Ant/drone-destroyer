/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MenuPanel_UserSettings.h

  A panel that allows the player to save game preferences.
=====================================================================
*/

#pragma once

#include "MenuPanelComponent.h"

DD_BEGIN

class MenuPanel_UserSettings : public MenuPanelComponent
{
	DECLARE_CLASS(MenuPanel_UserSettings)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	struct SMouseIconData
	{
		SD::DString IconName;
		SD::ButtonComponent* Button;

		SMouseIconData (const SD::DString& inIconName, SD::ButtonComponent* inButton) :
			IconName(inIconName),
			Button(inButton)
		{
			//Noop
		}
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Invoked whenever this panel closes. */
	SD::SDFunction<void> OnClosed;

protected:
	SD::GuiEntity* ControlsUi;

	/* List of selectable mouse icons the user may choose from. */
	std::vector<SMouseIconData> MouseIcons;

	SD::CheckboxComponent* DynamicCamCheckbox;
	SD::TextFieldComponent* GameVolumeField;
	SD::TextFieldComponent* MusicVolumeField;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void RestorePanel () override;
	virtual void SavePanelState () override;

protected:
	virtual void InitializeComponents () override;
	virtual void Destroyed () override;


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Toggles button enabledness based on the mouse icon, then it will then overwrite the default mouse icon.
	 */
	virtual void SelectMouseIcon (SD::ButtonComponent* associatedUiComponent);

	/**
	 * Clamps the given TextFieldComponent to ensure its content is within the given range.
	 */
	virtual void ClampTextField (SD::TextFieldComponent* uiComponent, SD::Range<SD::INT> clamp) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleMouseIconReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleDynamicCamChecked (SD::CheckboxComponent* uiComponent);
	virtual bool HandleAllowVolumeInput (const SD::DString& txt);
	virtual void HandleApplyGameVolume (SD::TextFieldComponent* uiComponent);
	virtual void HandleApplyMusicVolume (SD::TextFieldComponent* uiComponent);
	virtual void HandleBackReleased (SD::ButtonComponent* uiComponent);
	virtual void HandleApplyReleased (SD::ButtonComponent* uiComponent);
};
DD_END