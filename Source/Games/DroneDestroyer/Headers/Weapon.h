/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Weapon.h

  Inventory item that supplies the character a weapon.
=====================================================================
*/

#pragma once

#include "Inventory.h"

DD_BEGIN
class Projectile;

class Weapon : public Inventory
{
	DECLARE_CLASS(Weapon)


	/*
	=====================
	  Enums
	=====================
	*/

public:
	/**
	 * Various states for quickly accessing which state this weapon is in.
	 */
	enum EWeaponState
	{
		WS_Stowed, //Not drawn
		WS_PullingOut, //In between StartSwitchIn and EndSwitchIn
		WS_Drawn,
		WS_StowingAway, //In between StartSwitchOut and EndSwitchOut
		WS_NotCarried
	};


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* The highest priority value WeaponPriority can reach. */
	static const SD::INT MAX_WEAPON_PRIORITY;

	/* Called whenever this weapon finished pulling away. */
	SD::SDFunction<void, Weapon*> OnWeaponSwitchOut;

	/* Called whenever this weapon finished pulling out. */
	SD::SDFunction<void, Weapon*> OnWeaponSwitchIn;

	/* Broadcasted whenever the ammo amount has changed. */
	SD::MulticastDelegate<void, SD::INT /*newAmmoAmount*/> OnAmmoChanged;

protected:
	/* Short name of this current weapon that the bots will refer this weapon as. */
	mutable SD::DString ShortName;
	SD::DString ShortNameKey;

	/* The default ammo amount when picking up this weapon for the first time. */
	SD::INT StartingAmmo;

	/* The total amount of ammo this weapon can hold. */
	SD::INT MaxAmmo;

	/* Number of shots remaining before this weapon is empty. */
	SD::INT Ammo;

	/* If true, then this weapon does not consume ammo. */
	bool InfiniteAmmo;

	/* Determines how often this weapon may fire a single projectile (in seconds). */
	SD::FLOAT FireRate;

	/* The distance (in centimeters) this weapon kicks back from recoil. */
	SD::FLOAT RecoilDistance;

	/* Randomizes the projectile rotation by this amount from the center in degrees. */
	SD::FLOAT FireSpread;

	/* Projectile class to launch whenever this weapon fires. */
	const SD::DClass* ProjectileClass;

	/* Sound to play when this weapon is fired. */
	SD::DString ShootSound;
	SD::FLOAT ShootSoundVolume;

	/* Determines the order this weapon appears in the scroll list (whenever the carrier uses mouse wheel to scroll through weapons). */
	SD::INT WeaponPriority;

	/* List of sf keys to listen to in order for this weapon to request to switch to. */
	std::vector<sf::Keyboard::Key> WeaponSwitchBinds;

	EWeaponState WeaponState;

	/* Time it takes to switch to this weapon (in seconds). */
	SD::FLOAT SwitchToTime;

	/* Time it takes to switch out of this weapon (in seconds). */
	SD::FLOAT SwitchOutTime;

	/* Becomes true if this weapon is running in extinction mode where it'll change the projectile physics to shoot at drones instead of humans. */
	bool IsExtinctionMode;

	/* If true, then a drone is carrying this weapon. */
	bool IsDroneWeapon;

	/* Number of seconds to elapsed after this weapon was dropped before it vanishes. */
	SD::FLOAT DropExpirationTime;

	/* The Tick Component that will purge this weapon if it was not picked up in time. */
	SD::TickComponent* PurgeTimer;

	/* Tick Component responsible for handling weapon animations such as switching and shooting. */
	SD::TickComponent* WeaponAnimTick;

	/* Tick Component responsible for handling fire rate. */
	SD::TickComponent* WeaponFireTick;

private:
	/* Timestamp when this weapon last fired (used to handle toggling fire states and 'accumulate' deltaTime even when switching weapons (in seconds). */
	SD::FLOAT LastFireTime;

	/* Timestamp when the weapon started to switch in/out. Used for lerping. */
	SD::FLOAT SwitchTime;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual SD::INT GetImportanceValue (Character* possibleTaker) const override;
	virtual void GiveTo (Character* taker) override;
	virtual bool CanBeDropped (Character* dropFrom) const override;
	virtual void RemoveInventory (Character* removeFrom) override;

protected:
	virtual void EnterPossessedState (Character* possesser) override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the carrier is allowed to switch to this weapon.
	 */
	virtual bool CanSwitchTo () const;

	/**
	 * Called whenever the carrier started to pull out this weapon (the weapon is typically not yet ready to fire at this time).
	 */
	virtual void StartSwitchTo ();

	/**
	 * Called whenever the carrier finished pulling out this weapon.
	 */
	virtual void EndSwitchTo ();

	/**
	 * Called whenever the carrier started to switch away from this weapon.
	 */
	virtual void StartSwitchOut ();

	virtual void EndSwitchOut ();

	/**
	 * Returns true if this weapon is out and ready to shoot.
	 */
	virtual bool CanShoot () const;

	/**
	 * Informs this weapon to enter the shooting state where it'll shoot a single projectile every
	 * FireRate seconds.
	 */
	virtual void StartFiring ();

	virtual bool IsShooting () const;

	virtual void StopFiring ();

	/**
	 * Starts the timer that will purge this weapon if it is not picked up before expiration.
	 */
	virtual void StartPurgeTimer ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetAmmo (SD::INT newAmmo);
	virtual void SetInfiniteAmmo (bool newInfiniteAmmo);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual SD::DString GetShortName () const;
	DEFINE_ACCESSOR(SD::INT, StartingAmmo);
	DEFINE_ACCESSOR(SD::INT, MaxAmmo);
	DEFINE_ACCESSOR(SD::INT, Ammo);
	inline bool HasInfiniteAmmo () const
	{
		return InfiniteAmmo;
	}

	const Projectile* GetProjectileClass () const;

	DEFINE_ACCESSOR(SD::INT, WeaponPriority);
	DEFINE_ACCESSOR(EWeaponState, WeaponState);

	const std::vector<sf::Keyboard::Key>& ReadWeaponSwitchBinds () const
	{
		return WeaponSwitchBinds;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Fires a single shot, and sets the weapon state accordingly considering it just shot.
	 */
	virtual void Fire ();

	/**
	 * Fires a single instance of the projectile (if any).
	 */
	virtual void FireProjectile ();

	/**
	 * If the default projectile is spawned, this function is called to alter its behavior.
	 */
	virtual void ModifyProjectile (Projectile* proj) const;


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleWeaponAnimTick (SD::FLOAT deltaSec);
	virtual void HandleWeaponFireTick (SD::FLOAT deltaSec);
	virtual void HandlePurgeWeapon (SD::FLOAT deltaSec);
};
DD_END