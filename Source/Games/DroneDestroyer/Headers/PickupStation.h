/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PickupStation.h

  An Entity that periodically spawns pickups of a particular class.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"
#include "Inventory.h"

DD_BEGIN
class PickupStation : public SD::SceneEntity
{
	DECLARE_CLASS(PickupStation)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The inventory class this station will spawn. This is the class default object. */
	const Inventory* InventoryClass;

	/* The most recent item this station has spawned. This station is responsible for this item until someone takes it. */
	Inventory* SpawnedItem;

	/* Determines the interval in how frequently it'll respawn an item after the previous item was taken. */
	SD::FLOAT RespawnInterval;

	SD::TickComponent* Tick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Usually called after everything is initialized. This function visits the game modifications to allow
	 * them to modify this pickup station.
	 */
	virtual void ApplyGameModifications ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetInventoryClass (const Inventory* newInventoryClass);
	virtual void SetRespawnInterval (SD::FLOAT newRespawnInterval);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(const Inventory*, InventoryClass);
	DEFINE_ACCESSOR(Inventory*, SpawnedItem);
	DEFINE_ACCESSOR(SD::FLOAT, RespawnInterval);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Spawns a new item over this weapon base.
	 */
	virtual void SpawnItem ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::FLOAT deltaSec);
	virtual void HandleItemTaken (Inventory* itemTaken, Character* takenBy);
};
DD_END