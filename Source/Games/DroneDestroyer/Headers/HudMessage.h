/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HudMessage.h

  A Label Component that automaticaly registers itself to the game's hud. This handles positioning and fading.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"
#include "DroneDestroyerEngineComponent.h"
#include "Hud.h"

DD_BEGIN
class HudMessage : public SD::LabelComponent
{
	DECLARE_CLASS(HudMessage)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	/* Callback used whenever this message expired. */
	SD::SDFunction<void, HudMessage*> OnExpiration;

protected:
	/* Configures how long this message is displayed for (in seconds). */
	SD::FLOAT DisplayTime;

	/* Specifies how long before this message must last until it begins fading out (in seconds).
	If negative or greater than DisplayTime, then this message will pop out instead of fading out. */
	SD::FLOAT StartFadeTime;

	/* Localization key to use to populate the text. */
	SD::DString TextKey;

private:
	/* Time that's left before this component expires. */
	SD::FLOAT TimeRemaining;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;
	

	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTick (SD::FLOAT deltaSec);


	/*
	=====================
	  Templates
	=====================
	*/

public:
	/**
	 * Quick utility function that'll instantiate an instance of a HudMessage class, translate the text,
	 * and register the newly created instance to the PlayerHud.
	 */
	template <class ... T>
	static void CreateAndRegisterMsgToHud (const SD::DClass* msgClass, const T& ... params)
	{
		DroneDestroyerEngineComponent* localDroneEngine = DroneDestroyerEngineComponent::Find();
		CHECK(localDroneEngine != nullptr && localDroneEngine->GetPlayerHud() != nullptr)

		HudMessage* newMsg = dynamic_cast<HudMessage*>(msgClass->GetDefaultObject()->CreateObjectOfMatchingClass());
		CHECK(newMsg != nullptr)

		newMsg->InitializeText(params ...);
		localDroneEngine->GetPlayerHud()->AddHudMessage(newMsg);
	}

	/**
	 * Initializes the text content of the LabelComponent.
	 * @param params - Additional string to replace '%s' with these values.
	 */
	template <class ... T>
	void InitializeText (const T& ... params)
	{
		SD::TextTranslator* translator = SD::TextTranslator::GetTranslator();
		CHECK(translator != nullptr)

		SD::DString completeMsg = SD::DString::CreateFormattedString(translator->TranslateText(TextKey, TRANSLATION_FILE, TXT("HudMessage")), params ...);
		SetText(completeMsg);
	}
};
DD_END