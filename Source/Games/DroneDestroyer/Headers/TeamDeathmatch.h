/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TeamDeathmatch.h

  Players are grouped into teams to pool their kills together.
  The first team to reach the score limit are victorious.
=====================================================================
*/

#pragma once

#include "GameRules.h"

DD_BEGIN
class HudTeamDeathmatch;
class TimeLimitComponent;

class TeamDeathmatch : public GameRules
{
	DECLARE_CLASS(TeamDeathmatch)


	/*
	=====================
	  Structs
	=====================
	*/

public:
	struct STeam
	{
		SD::INT TeamNum;

		/* List of players in this team. These are sorted based on kills where player 0 is the highest scorer. */
		std::vector<SD::DPointer<PlayerSoul>> Members;

		/* Total number of kills everyone in this team team. */
		SD::INT TeamScore;

		SD::Color TeamColor;
		SD::DString TeamName;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* The kill limit to reach to in order to win. */
	SD::INT KillLimit;

	/* The maximum duration this match could last before it goes into sudden death (in seconds).
	If not positive, then there is no limit. */
	SD::FLOAT TimeLimit;

	/* Becomes true when this game as entered sudden death mode. */
	bool InSuddenDeath;

	/* Component responsible for keeping time. */
	TimeLimitComponent* TimeComponent;

	/* Component on the hud that will be updated whenever the scores have changed.
	A pub/sub module would be nice to update this rather than having a cached pointer. */
	SD::DPointer<HudTeamDeathmatch> HudScore;

	std::vector<STeam> Teams;

private:
	std::vector<SD::Color> TeamColors;
	std::vector<SD::DString> TeamNames;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool CanBeSelected () const override;
	virtual SD::INT GetNumTeams () const override;
	virtual void ApplyGameSettings (const std::vector<SD::DString>& newSettings) override;
	virtual void InitializeGame () override;
	virtual void AddSpectator (PlayerSoul* newSpectator) override;
	virtual Character* CreateCharacter (PlayerSoul* soul) override;
	virtual void AddComponentsToHud (Hud* playerHud) override;
	virtual void ScoreKill (Character* killed, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg) override;
	virtual void SortScores () override;

protected:
	virtual const Scoreboard* GetScoreboardClass () const override;
	virtual bool CanCreateBody (PlayerSoul* requester) override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(SD::INT, KillLimit);
	DEFINE_ACCESSOR(SD::FLOAT, TimeLimit);

	inline const std::vector<STeam>& ReadTeams () const
	{
		return Teams;
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Iterates through all players. Anyone that's not tied with the lead are automatically eliminated.
	 * The survivors are narrowed down to their last life.
	 */
	virtual void SuddenDeath ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleTimeLimitExpired ();
};
DD_END