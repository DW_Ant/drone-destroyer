/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Extinction.h

  A game mode where all players are on the same team to fight off the swarms of Drones.
=====================================================================
*/

#pragma once

#include "GameRules.h"

DD_BEGIN
class Drone;
class HudExtinction;
class NavigationComponent;
class PlayerSoul;
class TimeHudComponent;
class TimeLimitComponent;
class Weapon;

class Extinction : public GameRules
{
	DECLARE_CLASS(Extinction)


	/*
	=====================
	  Structs
	=====================
	*/

protected:
	/**
	 * A collection of properties pulled from a configuration file to configure a drone template.
	 */
	struct SDroneConfiguration
	{
		int SpriteIdx;
		EGameDifficulty SkillLevel;
		SD::INT MaxHealth;
		SD::FLOAT VelocityMagnitude;
		const SD::DClass* WeaponClass;
	};


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	SD::INT Wave;

	/* The wave the game starts in. */
	SD::INT StartingWave;

	/* The players are victorious if they can survive through the wave limits. */
	SD::INT MaxWave;

	/* Determines how many times a player can die before they're out of the waves. */
	SD::INT NumLivesPerWave;

	/* Base number of drones to spawn per wave. */
	SD::INT BaseNumDrones;

	/* Number of extra drones to spawn every wave. */
	SD::INT NumExtraDronesPerWave;

	/* Maximum number of drones that can be alive at once. */
	SD::INT MaxDronesAtOnce;

	/* Number of drones remaining in the current wave before the game mode stops spawning them. */
	SD::INT NumDronesRemaining;

	/* Number of seconds in each wave before drones start expiring. */
	SD::INT WaveTimeLimit;

	/* Rate how often a new drone spawns if it's permitted to spawn more drones for this wave (in seconds). */
	SD::FLOAT DroneSpawnInterval;

	/* Time since it spawned a drone. */
	SD::FLOAT LastDroneSpawnTime;

	/* The number of seconds between each wave before it starts spawning drones. */
	SD::INT WaveInterval;

	/* Number of seconds remaining in the WaveInterval before it'll begin spawning drones. */
	SD::INT WaveIntervalTimeRemaining;

	/* List of drone templates this game mode will use to initialize drones for this wave. */
	std::vector<SDroneConfiguration> DroneTemplates;

	/* List of drones that are currently alive. Drones do not have souls so they are registered to this list. */
	std::vector<Drone*> Drones;

	TimeHudComponent* TimeComponent;
	TimeLimitComponent* WaveTimeLimitComp;
	HudExtinction* GameHud;
	SD::TickComponent* WaveIntervalTick;
	SD::TickComponent* SpawnDroneTick;
	SD::TickComponent* PurgeDroneTick;

	/* The SpriteComponent responsible for efficiently rendering all Drones. */
	SD::InstancedSpriteComponent* DroneSprites;

	/* Slots in the DroneSprites sprite vector that cleared up because a drone perished. The six vertices associated with that drone didn't vanish. It's just invisible. */
	std::vector<size_t> EmptyDroneSpriteIndices;

private:
	/* Maximum number of drones to kill every second when the wave expired. */
	SD::INT DronePurgeLimit;

	/* Cached list of all NavigationComponents associated with the map (not temporary ones). These are used to determine where the drones may spawn.
	This is used for quick access since there are going to be an excessively large number of drones spawning. */
	mutable std::vector<NavigationComponent*> DroneSpawnLocations;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual bool CanBeSelected () const override;
	virtual SD::INT GetNumTeams () const override;
	virtual void ApplyGameSettings (const std::vector<SD::DString>& newSettings) override;
	virtual void InitializeGame () override;
	virtual void AddComponentsToHud (Hud* playerHud) override;
	virtual void ScoreKill (Character* killed, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg) override;
	virtual bool AllowDeathMessageFor (Character* killed) const override;

protected:
	virtual void Destroyed () override;
	virtual const Scoreboard* GetScoreboardClass () const override;
	virtual void InitializeBotDialect () override;
	virtual bool CanCreateBody (PlayerSoul* requester) override;
	virtual bool FindSpawnLocation (Character* body, PlayerSoul* soul, SD::Vector3& outLocation, SD::Rotator& outRotation) const override;
	virtual void SortScores () override;


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(SD::INT, Wave);
	DEFINE_ACCESSOR(SD::INT, MaxWave);
	DEFINE_ACCESSOR(SD::INT, BaseNumDrones);
	DEFINE_ACCESSOR(SD::INT, NumExtraDronesPerWave);
	DEFINE_ACCESSOR(SD::INT, MaxDronesAtOnce);
	DEFINE_ACCESSOR(SD::INT, WaveTimeLimit);

	inline const std::vector<Drone*>& ReadDrones () const
	{
		return Drones;
	}


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Removes the given drone from the sprite instance component and checks if it should progress to the next wave.
	 */
	virtual void RemoveDrone (Drone* oldDrone);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Advances the game to the next wave.
	 */
	virtual void NextWave ();

	/**
	 * Checks the configuration file for any templates to pull for this wave.
	 */
	virtual void LoadTemplates ();

	/**
	 * Spawns a single drone instance. This function will determine the drone type and its strength based on the wave.
	 */
	virtual void SpawnDrone ();

	/**
	 * Creates a sprite instance for the new Drone.
	 */
	virtual void AddDroneSprite (Drone* newDrone, SD::INT subDivionsIdx);

	/**
	 * Checks if there are any survivors. If everyone is dead, then this game is over.
	 * If the game ends, it'll use the given location as the end game camera location.
	 */
	virtual void CheckLossCondition (const SD::Vector3& camLocation);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleWaveInterval (SD::FLOAT deltaSec);
	virtual void HandleSpawnDrone (SD::FLOAT deltaSec);
	virtual void HandleWaveLimitExpired ();
	virtual void HandlePurgeDrones (SD::FLOAT deltaSec);
};
DD_END