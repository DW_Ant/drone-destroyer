/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Character.h

  Entity that represents a physical character in the scene that either the bots or the player may control.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class Arena;
class BotCharacterComponent;
class HealthComponent;
class Inventory;
class MovementComponent;
class PlayerSoul;
class Projectile;
class Weapon;

class Character : public SD::Entity, public SD::SceneTransform
{
	DECLARE_CLASS(Character)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const SD::INT INPUT_PRIORITY;

protected:
	/* Determines the rendered and collision radius of this character (in centimeters). Must be set in InitProps. */
	SD::FLOAT Radius;

	/* When throwing weapons, this is how fast the weapon is initially tossed from this character. */
	SD::FLOAT ThrowWeaponSpeed;

	/* The weapon this character is holding out. */
	Weapon* DrawnWeapon;

	/* Becomes true if this character is actively shooting (fire button is held down). */
	bool IsShooting;

	/* Inventory item this character carries that's the start of the inventory linked list. */
	SD::DPointer<Inventory> FirstInventory;

	/* The soul that is possessing this character. */
	PlayerSoul* Soul;

	SD::SpriteComponent* RenderComponent;
	SD::PhysicsComponent* Physics;

	/* The CDO of the bot component class to spawn when initializing this Character. */
	const BotCharacterComponent* BotComponentCdo;

	HealthComponent* Health;

	std::vector<SD::DString> DeathSounds;
	SD::FLOAT DeathSoundVolume;

	/* Component cameras attach to when watching this character. */
	SD::SceneTransformComponent* AttachedCameraTransform;

	SD::DPointer<MovementComponent> Movement;

	/* Frequency how often this character can grunt in pain. */
	SD::FLOAT PainSoundInterval;
	SD::FLOAT LastPainSound;

private:
	/* Becomes false if this character is still alive. Used to detect when this once living player is killed.
	For example: Don't call Kill if someone damaged this corpse. */
	bool InvokedKill;

	mutable SD::FLOAT CachedCollisionRadius;

	/* Current weapon this character is switching to. */
	Weapon* DesiredWeapon;

	/* Quick references to the viewport (used for fast access on mouse move). */
	SD::DPointer<SD::Window> MainViewport;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * There's a performance hit if the physics component is initialized before the character position is specified.
	 * After establishing the character position, call this function to initialize the physics and movement component.
	 */
	virtual void InitializePhysics ();

	/**
	 * Initialize InputComponent that'll be responsible for handling this character's inventory.
	 */
	virtual void InitializeInputComponent ();

	/**
	 * Creates and initializes a bot component that will be used to control this character.
	 */
	virtual void InitializeBotComponent ();

	/**
	 * Returns true if this character is friendly towards the other character.
	 * This character should not attack their friends.
	 */
	virtual bool IsFriendlyTo (Character* other) const;

	/**
	 * Returns true if this character's current health is positive.
	 */
	virtual bool IsAlive () const;

	/**
	 * Returns true if a human is controlling this character.
	 */
	virtual bool IsHuman () const;

	/**
	 * Iterates through this character's inventory list to figure out which is the best weapon to switch to.
	 */
	virtual void SwitchToBestWeapon ();

	/**
	 * Iterates through this character's inventory list to figure out the weapon that has the next priority value from
	 * their current drawn weapon. If the user already has the highest weapon drawn, it'll draw the weakest weapon.
	 */
	virtual void SelectNextWeapon ();
	virtual void SelectPrevWeapon ();

	/**
	 * Throws this character's current weapon away.
	 */
	virtual void ThrowWeapon ();

	/**
	 * Instructs this character that the given inventory is no longer under their possession.
	 * At this point, the given Inventory is already severed from the linked list.
	 */
	virtual void DeleteInventory (Inventory* oldInventory);

	/**
	 * Instructs this character to switch to the specified weapon.
	 * Returns true on success.
	 */
	virtual bool DrawWeaponTo (Weapon* weaponToDrawTo);

	/**
	 * Quick accessor that returns the collision radius of this character based on the physics component.
	 */
	virtual SD::FLOAT GetCollisionRadius () const;

	/**
	 * Called whenever this character's health reaches 0 or below. Handles the logic for killing this character.
	 */
	virtual void Kill (SD::INT damageAmount, PlayerSoul* killer, Projectile* killedBy, const SD::DString& deathMsg);

	/**
	 * Leaves a mark in the given arena to signal that this character died there.
	 */
	virtual void LeaveCorpse (Arena* environment);


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetSoul (PlayerSoul* newSoul);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	Weapon* GetDrawnWeapon () const
	{
		return DrawnWeapon;
	}

	Inventory* GetFirstInventory () const
	{
		return FirstInventory.Get();
	}

	PlayerSoul* GetSoul () const
	{
		return Soul;
	}

	HealthComponent* GetHealth () const
	{
		return Health;
	}

	SD::SceneTransformComponent* GetAttachedCameraTransform () const
	{
		return AttachedCameraTransform;
	}

	MovementComponent* GetMovement () const
	{
		return Movement.Get();
	}


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	virtual void SetDrawnWeapon (Weapon* newWeapon);

	/**
	 * Determines what kind of weapon to select based on the lambda passed to it.
	 * The character will then switch to the latest weapon where isBetterWeaponCandidate returned true.
	 */
	virtual void SelectWeaponImplementation (const std::function<bool(Weapon*)>& isBetterWeaponCandidate);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual bool HandleInput (const sf::Event& inputEvent);
	virtual void HandleMouseMove (SD::MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const SD::Vector2& deltaMove);
	virtual bool HandleMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& mouseEvent, sf::Event::EventType eventType);
	virtual bool HandleMouseWheel (SD::MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& wheelEvent);
	virtual void HandleWeaponSwitchOut (Weapon* switchedFrom);
	virtual void HandleWeaponSwitchIn (Weapon* switchedTo);
	virtual void HandleTakeDamage (SD::INT damageAmount, PlayerSoul* damagedBy, Projectile* damagedWith);

	friend Inventory;
};
DD_END