/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  PlayerSoul.h

  Entity that contains player information that persists across multiple bodies.
  Each participant will have one of these including bots.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"
#include "Character.h"
#include "GameRules.h"
#include "MovementComponent.h"

DD_BEGIN
class HealthComponent;
class PauseMenu;

class PlayerSoul : public SD::Entity
{
	DECLARE_CLASS(PlayerSoul)


	/*
	=====================
	  Properties
	=====================
	*/

public:
	static const SD::INT INPUT_PRIORITY;
	static const SD::INT PAUSED_INPUT_PRIORITY;

	/* How long should it wait in between deaths before it can request another body to possess. */
	SD::FLOAT RespawnTime;

protected:
	/* Callback to invoke whenever this soul possessed a new body. This is strictly used to notify the soul spectating this soul that this soul spawned a new character. */
	SD::SDFunction<void, Character* /*newBody*/> OnPossessBody;

	/* Displayed name on the scoreboard and nameplates. */
	SD::DString PlayerName;

	/* Only applicable for team games, this determines which team this player belongs to. */
	SD::INT TeamNumber;

	/* Number of characters this player killed. */
	SD::INT NumKills;

	/* Number of times this player has died. */
	SD::INT NumDeaths;

	/* Number of times this player can respawn. */
	SD::INT LivesRemaining;

	/* If true, then this player is an active participant, but they're out of lives and characters. */
	bool IsEliminated;

	/* Becomes true if this player is not a participant but rather an observer. */
	bool IsSpectator;

	/* Becomes true if an AI is controlling this soul. */
	bool IsBot;

	/* The Character this soul is controlling. */
	SD::DPointer<Character> Body;

	/* Pointer to the next soul in the linked list. If null, then this is at the end of the list. */
	PlayerSoul* NextSoul;

	/* The soul this player is watching. If null, then the player is using a free camera. */
	PlayerSoul* SpectatedSoul;

	/* Component used to move the main camera whenever the player is in free fly mode. */
	SD::DPointer<MovementComponent> CamMovementComp;

	SD::DPointer<PauseMenu> PauseUi;

private:
	/* Timestamp when this soul lost a body. */
	SD::FLOAT LastKilledTime;

	/* The default velocity magnitude for the camera movement component before any multipliers based on camera zoom were applied. */
	SD::FLOAT DefaultCamPanSpeed;

	/* The default zoom value of the camera before this soul manipulated it. */
	SD::FLOAT DefaultCamZoom;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;

protected:
	virtual void Destroyed () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if this soul is controlling a living body.
	 */
	virtual bool IsAlive () const;

	/**
	 * Makes a request for the game to create a new body for this soul to posess.
	 * Returns true if this soul is now possessing a new body.
	 */
	virtual bool RequestBody ();

	/**
	 * Notifies this soul to possess an InputComponent that'll process commands that are
	 * not associated with a body (such as requesting a body, viewing score board, and
	 * popping up game menus.
	 */
	virtual void InitializeInputComponent ();

	/**
	 * Creates and initializes components responsible for requesting bodies to possess.
	 */
	virtual void InitializeBotComponent ();

	/**
	 * Returns true if this soul is allowed to damage the given health component.
	 */
	virtual bool CanHarm (HealthComponent* health) const;

	//Should I just expose these variables?
	virtual void IncrementKillCount ();
	virtual void DecrementKillCount ();
	virtual void IncrementDeathCount ();
	virtual void DecrementLivesCount ();

	/**
	 * Informs this soul that it's no longer in charge of its current possessed body.
	 */
	virtual void DetachFromBody ();


	/*
	=====================
	  Mutators
	=====================
	*/

public:
	virtual void SetPlayerName (const SD::DString& newPlayerName);
	virtual void SetTeamNumber (SD::INT newTeamNumber)
	{
		TeamNumber = newTeamNumber;
	}

	virtual void SetLivesRemaining (SD::INT newLivesRemaining)
	{
		LivesRemaining = newLivesRemaining;
	}

	virtual void SetIsEliminated (bool newIsEliminated)
	{
		IsEliminated = newIsEliminated;
	}

	virtual void SetIsSpectator (bool newIsSpectator);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	DEFINE_ACCESSOR(SD::DString, PlayerName);

	const SD::DString& ReadPlayerName () const
	{
		return PlayerName;
	}

	DEFINE_ACCESSOR(SD::INT, TeamNumber);
	DEFINE_ACCESSOR(SD::INT, NumKills);
	DEFINE_ACCESSOR(SD::INT, NumDeaths);
	DEFINE_ACCESSOR(SD::INT, LivesRemaining);
	DEFINE_ACCESSOR(bool, IsEliminated);
	DEFINE_ACCESSOR(bool, IsSpectator);
	DEFINE_ACCESSOR(bool, IsBot);

	Character* GetBody () const
	{
		return Body.Get();
	}

	DEFINE_ACCESSOR(PlayerSoul*, NextSoul);


	/*
	=====================
	  Implementation
	=====================
	*/

protected:
	/**
	 * Moves to the next soul in the list and snaps the camera to follow that character.
	 */
	virtual void ViewNextPlayer ();

	/**
	 * Attaches the main camera to view this particular character.
	 */
	virtual void AttachCameraTo (Character* target);

	/**
	 * Forgets whatever this soul is viewing and moves to its free camera mode.
	 */
	virtual void BeginFreeCamMode ();


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleForceRespawnTick (SD::FLOAT deltaSec);
	virtual bool HandleInput (const sf::Event& evnt);
	virtual bool HandleMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual bool HandlePausedMouseClick (SD::MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
	virtual bool HandleMouseWheel (SD::MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& wheelEvent);
	virtual void HandleSoulPossessBody (Character* newBody);

	friend GameRules;
};
DD_END