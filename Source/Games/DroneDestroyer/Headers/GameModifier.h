/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GameModifier.h

  The base entity that changes the rules of the game.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class Character;
class GameRules;
class Inventory;
class PickupStation;

class GameModifier : public SD::Entity
{
	DECLARE_CLASS(GameModifier)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	/* Translation key used to set FriendlyName. */
	SD::DString FriendlyNameKey;
	mutable SD::DString FriendlyName;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;


	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Returns true if the selected game modifier is permitted based on the given game rules' state.
	 */
	virtual bool IsAllowed (GameRules* game) const;

	/**
	 * A hook that allows the game modifier to modify the newly spawned player.
	 */
	virtual void ModifyPlayer (Character* newPlayer);

	virtual void ModifyInventory (Inventory* newInv);

	virtual void ModifyPickupStation (PickupStation* station);


	/*
	=====================
	  Accessors
	=====================
	*/

public:
	virtual const SD::DString& ReadFriendlyName () const;
};
DD_END