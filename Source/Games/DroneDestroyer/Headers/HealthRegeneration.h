/*
=====================================================================
  MIT License

  Copyright (c) 2016-2020 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  HealthRegeneration.h

  A component that attaches to the HealthComponent that gives it passive healing over time.
=====================================================================
*/

#pragma once

#include "DroneDestroyer.h"

DD_BEGIN
class HealthComponent;

class HealthRegeneration : public SD::EntityComponent
{
	DECLARE_CLASS(HealthRegeneration)


	/*
	=====================
	  Properties
	=====================
	*/

protected:
	HealthComponent* OwningHealthComp;
	SD::TickComponent* Tick;


	/*
	=====================
	  Inherited
	=====================
	*/

public:
	virtual void InitProps () override;
	virtual void BeginObject () override;
	virtual bool CanBeAttachedTo (Entity* ownerCandidate) const override;

protected:
	virtual void AttachTo (Entity* newOwner) override;
	virtual void ComponentDetached () override;
	

	/*
	=====================
	  Methods
	=====================
	*/

public:
	/**
	 * Sets this component to ensure this heals the owning health component by the given amount per second.
	 */
	virtual void SetHealPerSecond (SD::FLOAT healPerSec);


	/*
	=====================
	  Event Handlers
	=====================
	*/

protected:
	virtual void HandleHealingTick (SD::FLOAT deltaSec);
};
DD_END